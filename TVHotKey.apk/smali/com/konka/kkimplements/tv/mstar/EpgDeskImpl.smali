.class public Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "EpgDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/EpgDesk;


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I = null

.field private static final TAG:Ljava/lang/String; = "EpgDeskImpl"

.field private static epgMgrImpl:Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;


# instance fields
.field private final TIME_OFFSET:I

.field private _mCurKonkaProgInfo:Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

.field private _mCurServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

.field private _mEventDiffTime:I

.field private _mEventList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private _mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

.field private _mProgList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation
.end field

.field private channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

.field private mLock:Ljava/lang/Object;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_UNITED_TV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgrImpl:Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurKonkaProgInfo:Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    iput v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    iput v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->TIME_OFFSET:I

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->mLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvService"

    const-string v3, "EpgManagerImpl constructor!!"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getEpgManager()Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkIsScheduledInTwoMinutes(I)Z
    .locals 7
    .param p1    # I

    const/4 v4, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerEventCount()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    const/4 v3, 0x0

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v2, :cond_2

    :cond_1
    :goto_2
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    :goto_3
    iget v5, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v5, v5, 0x0

    iput v5, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v5, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v6, p1, -0x78

    if-le v5, v6, :cond_3

    iget v5, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v6, p1, 0x78

    if-ge v5, v6, :cond_3

    const/4 v4, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :cond_3
    iget v5, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v6, p1, 0x78

    if-ge v5, v6, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private delEpgTimerEventByEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Z
    .locals 9
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventsWithoutDelPast()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerIndex(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)I

    move-result v1

    const/4 v6, -0x1

    if-ne v1, v6, :cond_0

    :goto_0
    return v5

    :cond_0
    iget v5, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    sub-int v2, v5, v6

    invoke-virtual {p0, v2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->deleteScheduleInfoByStartTime(I)V

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgEvent(I)Z

    const/4 v5, 0x0

    iput-boolean v5, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v5, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v6, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    if-le v6, v2, :cond_1

    iget-object v6, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget-object v7, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v8, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    invoke-virtual {p0, v6, v7, v8}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2
.end method

.method private delEpgTimerEventByIndex(I)Z
    .locals 8
    .param p1    # I

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventsWithoutDelPast()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_2

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {p0, v4}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->deleteScheduleInfoByStartTime(I)V

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgEvent(I)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v4, 0x1

    :goto_3
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v5, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-le v5, v6, :cond_0

    iget-object v5, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget-object v6, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v7, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    invoke-virtual {p0, v5, v6, v7}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public static getEpgMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgrImpl:Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgrImpl:Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgrImpl:Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    return-object v0
.end method

.method private getEpgTimerEventsWithoutDelPast()Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;",
            ">;"
        }
    .end annotation

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerEventCount()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :cond_0
    :goto_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_1
    if-lt v2, v3, :cond_1

    return-object v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    new-instance v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    invoke-direct {v5}, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;-><init>()V

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_2

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventId:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v9, v9, 0x0

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryEventName(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryServiceName(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->serviceType:I

    iget-short v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    iget-short v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->repeatMode:I

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "timer start time is ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    int-to-long v9, v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    const-wide/16 v11, 0x0

    add-long/2addr v9, v11

    iget v11, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    mul-int/lit16 v11, v11, 0x3e8

    int-to-long v11, v11

    add-long v7, v9, v11

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "timer start time is ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    invoke-virtual {v0, v7, v8}, Landroid/text/format/Time;->set(J)V

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v10, "EpgDeskImpl"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u52a0\u5165\u9884\u7ea6\u65f6\u95f4============"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v0, Landroid/text/format/Time;->year:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->month:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->hour:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->minute:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method private getEpgTimerIndex(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)I
    .locals 7
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventCount()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v3, :cond_0

    :goto_2
    if-eqz v0, :cond_3

    :goto_3
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    :goto_4
    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x0

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-eqz v4, :cond_2

    iget v5, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v6, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-ne v5, v6, :cond_1

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iget v6, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    if-ne v5, v6, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :cond_1
    iget v5, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v6, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-ge v5, v6, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, -0x1

    goto :goto_3
.end method

.method private getHourOffset()I
    .locals 7

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    const v6, 0x36ee80

    div-int v2, v4, v6

    return v2
.end method

.method private getMinuteOffset()I
    .locals 7

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    const v6, 0xea60

    div-int v3, v4, v6

    rem-int/lit8 v3, v3, 0x3c

    return v3
.end method

.method private getProgramInfoByIndex(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_DATABASE_INDEX:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {v2, p1, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    const-string v4, "getProgramInfoByNumber ========="

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getProgramInfoByNumber(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {v2, p1, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    const-string v4, "TvProgInfo is ------> null"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TvProgInfo is ------>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    const-string v4, "getProgramInfoByNumber ========="

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getServiceTypeByShort(S)Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .locals 2
    .param p1    # S

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-lt p1, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-le p1, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v1

    aget-object v0, v1, p1

    goto :goto_0
.end method

.method private isEventScheduled(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;I)I
    .locals 5
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;
    .param p2    # I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x0

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-eqz v1, :cond_0

    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    if-lt v2, v3, :cond_0

    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    if-ge v2, v3, :cond_0

    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iget v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    if-ne v2, v3, :cond_0

    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    iget v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mServiceType:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    :goto_1
    return v2

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    const-string v4, "getEpgTimerEventByIndex error"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    if-le v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x2

    goto :goto_1
.end method

.method private isProgCountEqualToZeroByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getDtvProgCount()Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    :cond_0
    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->isNoProgInServiceType(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->isNoProgInServiceType(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->isNoProgInServiceType(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private queryEventName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryEpg(ZI)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "sEventName"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n=====>>eventName "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " @starttime "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method

.method private queryServiceName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryEpg(ZI)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "sServiceName"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n=====>>serviceName "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " @starttime "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v1
.end method


# virtual methods
.method public ProgramSel(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V
    .locals 5
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    iget v2, p1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    iget-short v3, p1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgType:S

    iget v4, p1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgIndex:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurKonkaProgInfo:Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "EpgDeskImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "program name sis ---------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public UpdateEpgTimer(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "sEventName"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "sServiceName"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n########### getDataBaseMgrInstance \n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    invoke-virtual {v2, v1, p3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateChannelNameAndEventName(Landroid/content/ContentValues;I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method public addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addEpgTimerEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;
    .locals 13
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventList()Ljava/util/ArrayList;

    move-result-object v8

    new-instance v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct {v5}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v0

    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    long-to-int v4, v9

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v10, "EpgDeskImpl"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "================ibaseTime"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v10, "EpgDeskImpl"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "================_mStartTime"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v10, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    add-int/2addr v10, v4

    add-int/lit8 v10, v10, 0x3e

    if-gt v9, v10, :cond_0

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_TIMEOUT:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    :goto_0
    return-object v9

    :cond_0
    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    add-int/lit8 v10, v4, 0x78

    if-ge v9, v10, :cond_1

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OF_ATHAND:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto :goto_0

    :cond_1
    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v10, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    sub-int/2addr v9, v10

    invoke-direct {p0, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->checkIsScheduledInTwoMinutes(I)Z

    move-result v9

    if-eqz v9, :cond_2

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OF_EXIST:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v10, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x0

    iput v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventId:I

    iput v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    iget v10, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    sub-int/2addr v9, v10

    iput v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_REMINDER:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->ordinal()I

    move-result v9

    int-to-short v9, v9

    iput-short v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    iput v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iget v9, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mServiceType:I

    iput v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    const/16 v9, 0x81

    iput-short v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    const/4 v9, 0x0

    iput-boolean v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->isEndTimeBeforeStart:Z

    :try_start_0
    invoke-virtual {p0, v5}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v10, "EpgDeskImpl"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "add event result is ["

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] here "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v9

    sget-object v10, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v10

    if-ne v9, v10, :cond_3

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_FULL:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v9

    sget-object v10, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v10

    if-ne v9, v10, :cond_6

    new-instance v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v6}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    const/4 v7, 0x0

    iget v9, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iput v9, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->number:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v9

    iget v10, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    aget-object v9, v9, v10

    invoke-virtual {v6, v9}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->setServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    invoke-direct {p0, v6}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getProgramInfoByNumber(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v7

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "the name is "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "---------1-------"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v9, v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    iget-object v10, p1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    iget v11, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {p0, v9, v10, v11}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lt v3, v9, :cond_4

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_SUCCESSS:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v9, v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v10, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    if-le v9, v10, :cond_5

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v10, v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v11, v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v9, v9, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    invoke-virtual {p0, v10, v11, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "---------11------"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v9

    sget-object v10, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_OVERLAY:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v10

    if-ne v9, v10, :cond_7

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OVERLAY:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto/16 :goto_0

    :cond_7
    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;->E_ADDEVENT_FAIL_OTHER_EXCEPTION:Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    goto/16 :goto_0
.end method

.method public cancelEpgTimerEvent(I)V
    .locals 6
    .param p1    # I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventList()Ljava/util/ArrayList;

    move-result-object v2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, p1, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->cancelEpgTimerEvent(IZ)V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "------------cancelIlefttime"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v4, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v5, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v3, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    invoke-virtual {p0, v4, v5, v3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public cancelEpgTimerEvent(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager;->cancelEpgTimerEvent(IZ)V

    :cond_0
    return-void
.end method

.method public closeDB()V
    .locals 0

    return-void
.end method

.method public delAllEpgEvent()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->delAllEpgEvent()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public delEpgEvent(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->delEpgEvent(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public delEpgTimerEvent(IZ)Z
    .locals 1
    .param p1    # I
    .param p2    # Z

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    invoke-direct {p0, v0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgTimerEventByEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgTimerEventByIndex(I)Z

    move-result v0

    goto :goto_0
.end method

.method public deletePastEpgTimer()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->deletePastEpgTimer()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deletePastScheduleInfo()V
    .locals 12

    const/4 v9, 0x0

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v8}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v8

    invoke-virtual {v8, v9, v9}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryEpg(ZI)Landroid/database/Cursor;

    move-result-object v0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v1

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v8, v8

    add-int/lit8 v8, v8, 0x0

    iget v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    sub-int/2addr v8, v9

    add-int/lit8 v3, v8, -0xa

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "u32StartTime"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    :cond_0
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-ge v6, v3, :cond_1

    new-instance v8, Ljava/lang/Integer;

    invoke-direct {v8, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    :cond_2
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v5, :cond_4

    :cond_3
    invoke-interface {v7}, Ljava/util/List;->clear()V

    const/4 v7, 0x0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void

    :cond_4
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v8}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->deleteEpg(I)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public deleteScheduleInfoByStartTime(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->deleteEpg(I)I

    return-void
.end method

.method public disableEpgBarkerChannel()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->disableEpgBarkerChannel()Z

    move-result v0

    return v0
.end method

.method public enableEpgBarkerChannel()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->enableEpgBarkerChannel()Z

    move-result v0

    return v0
.end method

.method public execEpgTimerAction()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->execEpgTimerAction()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execEpgTimerEvent()V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->execEpgTimerAction()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCridAlternateList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getCridAlternateList(SILandroid/text/format/Time;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCridSeriesList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getCridSeriesList(SILandroid/text/format/Time;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCridSplitList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getCridSplitList(SILandroid/text/format/Time;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getCridStatus(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getCridStatus(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;

    move-result-object v0

    return-object v0
.end method

.method public getCurClkTime()Landroid/text/format/Time;
    .locals 5

    const/4 v0, 0x0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    return-object v0
.end method

.method public getCurInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurKonkaProgInfo()Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;
    .locals 6

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    invoke-direct {v1}, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;-><init>()V

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->progId:I

    iput v2, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgDbId:I

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->queryIndex:I

    iput v2, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgIndex:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    iput-object v2, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "initPosInChannelList Kpi ProgName is ------>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iput v2, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "EpgDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "initPosInChannelList Kpi ProgNumber is ------>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-short v2, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iput-short v2, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgType:S

    :cond_0
    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurKonkaProgInfo:Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    return-object v1
.end method

.method public getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 7

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "EpgDeskImpl"

    const-string v5, "getCurProgramInfo"

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_CURRENT:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {v3, v2, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v3

    iget-short v4, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "EpgDeskImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cur ProgNo is -------->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ProgramInfo ServiceType is"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-short v6, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "EpgDeskImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "CurServiceType is -------->"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mCurServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDtvProgCount()Lcom/konka/kkimplements/tv/vo/KonkaProgCount;
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    :goto_0
    new-instance v5, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    invoke-direct {v5, v3, v4, v1, v2}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;-><init>(IIII)V

    iput-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    return-object v5

    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v3

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v4

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEitInfo(Z)Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEitInfo(Z)Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEpgEventOffsetTime(Landroid/text/format/Time;Z)I
    .locals 1
    .param p1    # Landroid/text/format/Time;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEpgEventOffsetTime(Landroid/text/format/Time;Z)I

    move-result v0

    return v0
.end method

.method public getEpgSearchReslut(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEpgTimerEventCount()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerEventCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEpgTimerEventList()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;",
            ">;"
        }
    .end annotation

    const/4 v13, 0x1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerEventCount()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :cond_0
    :goto_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_1
    if-lt v2, v3, :cond_1

    return-object v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    new-instance v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    invoke-direct {v5}, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;-><init>()V

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    :goto_2
    if-eqz v4, :cond_2

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventId:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v9, v9, 0x0

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryEventName(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryServiceName(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->serviceType:I

    iget-short v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    iget-short v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    iput v9, v5, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->repeatMode:I

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "timer start time is ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    iget v9, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    int-to-long v9, v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    const-wide/16 v11, 0x0

    add-long/2addr v9, v11

    iget v11, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    mul-int/lit16 v11, v11, 0x3e8

    int-to-long v11, v11

    add-long v7, v9, v11

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "timer start time is ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    invoke-virtual {v0, v7, v8}, Landroid/text/format/Time;->set(J)V

    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v10, "EpgDeskImpl"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "\u52a0\u5165\u9884\u7ea6\u65f6\u95f4============"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v12, v0, Landroid/text/format/Time;->year:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->month:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->hour:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v0, Landroid/text/format/Time;->minute:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v9

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v11

    invoke-virtual {v11, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v11

    cmp-long v9, v9, v11

    if-gtz v9, :cond_3

    const/4 v9, 0x0

    invoke-virtual {p0, v2, v9}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgTimerEvent(IZ)Z

    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method

.method public getEpgTimerRecordingProgram()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getEpgTimerRecordingProgram()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEvent1stMatchHdBroadcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEvent1stMatchHdBroadcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;

    move-result-object v0

    return-object v0
.end method

.method public getEvent1stMatchHdSimulcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEvent1stMatchHdSimulcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;

    move-result-object v0

    return-object v0
.end method

.method public getEventCount(SILandroid/text/format/Time;)I
    .locals 2
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventCount(SILandroid/text/format/Time;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEventDescriptor(SILandroid/text/format/Time;Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/lang/String;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventDescriptor(SILandroid/text/format/Time;Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEventDetailDescriptor(I)Ljava/lang/String;
    .locals 8
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    add-int/lit8 v4, p1, 0x0

    int-to-long v4, v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v5, "EpgDeskImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u83b7\u53d6\u8282\u76ee\u4fe1\u606f=======\u5f53\u524d\u9891\u9053\u4fe1\u606f"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->queryIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-short v4, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    sget-object v6, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;->E_DETAIL_DESCRIPTION:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;

    invoke-virtual {p0, v4, v5, v0, v6}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEventDescriptor(SILandroid/text/format/Time;Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v5, "EpgDeskImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u83b7\u53d6\u8282\u76ee\u4fe1\u606f=======\u5f53\u524d\u8282\u76ee"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    return-object v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEventHdSimulcast(SILandroid/text/format/Time;S)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "S)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventHdSimulcast(SILandroid/text/format/Time;S)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getEventInfoById(SIS)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventInfoById(SIS)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEventInfoByRctLink(Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventInfoByRctLink(Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEventList(SI)Ljava/util/ArrayList;
    .locals 21
    .param p1    # S
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation

    new-instance v3, Landroid/text/format/Time;

    const-string v17, "GMT+0"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    new-instance v16, Landroid/text/format/Time;

    const-string v17, "GMT+0"

    invoke-direct/range {v16 .. v17}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getHourOffset()I

    move-result v17

    move/from16 v0, v17

    mul-int/lit16 v0, v0, 0xe10

    move/from16 v17, v0

    invoke-direct/range {p0 .. p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getMinuteOffset()I

    move-result v18

    mul-int/lit8 v18, v18, 0x3c

    add-int v15, v17, v18

    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    const-wide/16 v17, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/text/format/Time;->set(J)V

    invoke-direct/range {p0 .. p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getServiceTypeByShort(S)Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v17

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    const/16 v17, 0x0

    :goto_0
    return-object v17

    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    const/16 v17, 0x0

    goto :goto_0

    :cond_1
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v9, 0x0

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v17

    const-wide/16 v19, 0x0

    add-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEventCount(SILandroid/text/format/Time;)I

    move-result v8

    if-gtz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    move-object/from16 v17, v0

    const-string v18, "EpgDeskImpl"

    const-string v19, "errrrrrrrrrrroooooo"

    invoke-interface/range {v17 .. v19}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v17, 0x0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v3, v8}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    :goto_1
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v17

    mul-int/lit16 v0, v15, 0x3e8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    add-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v9, v3, Landroid/text/format/Time;->weekDay:I

    if-eqz v13, :cond_b

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-lez v17, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->mLock:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    if-nez v17, :cond_3

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    :goto_2
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v12, 0x0

    const/4 v7, 0x0

    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventCount()I
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v7

    :goto_3
    const/4 v5, 0x0

    :goto_4
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v5, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    monitor-enter v18

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    monitor-exit v18

    goto/16 :goto_0

    :catchall_0
    move-exception v17

    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v17

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    goto :goto_2

    :catchall_1
    move-exception v17

    monitor-exit v18
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v17

    :catch_1
    move-exception v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    move-object/from16 v17, v0

    const-string v18, "EpgDeskImpl"

    const-string v19, "getEpgTimerEventCount error"

    invoke-interface/range {v17 .. v19}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    new-instance v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    invoke-direct {v14}, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;-><init>()V

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x0

    move/from16 v0, v17

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    iget v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    move/from16 v17, v0

    iget v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    iget v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventId:I

    iget-object v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    move/from16 v0, p2

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mProgNo:I

    iput v5, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIndex:I

    move/from16 v0, p1

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mServiceType:I

    iget-short v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mContentDescriptor:I

    iget-short v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mParentalRating:I

    iget v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    move/from16 v17, v0

    add-int v17, v17, v15

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    iget v0, v3, Landroid/text/format/Time;->weekDay:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v9, v0, :cond_5

    add-int/lit8 v6, v6, 0x1

    iget v9, v3, Landroid/text/format/Time;->weekDay:I

    :cond_5
    iput v6, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    if-ge v12, v7, :cond_a

    const/4 v10, 0x2

    :cond_6
    const-string v17, "EPG"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "===========>>>> 222 Info.startTime["

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "] Info.endTime["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "] tempInfo._mStartTime["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "] tempInfo._mEndTime["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEndTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->isEventScheduled(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;I)I

    move-result v10

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v10, v0, :cond_8

    :goto_5
    const/16 v17, 0x2

    move/from16 v0, v17

    if-ne v10, v0, :cond_7

    if-lt v12, v7, :cond_6

    :cond_7
    if-nez v10, :cond_9

    const/16 v17, 0x1

    move/from16 v0, v17

    iput-boolean v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    monitor-enter v18

    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v14}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    monitor-exit v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_8
    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    :cond_9
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    goto :goto_6

    :cond_a
    const/16 v17, 0x0

    move/from16 v0, v17

    iput-boolean v0, v14, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mIsScheduled:Z

    goto :goto_6

    :catchall_2
    move-exception v17

    :try_start_6
    monitor-exit v18
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v17

    :cond_b
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method public getEventListByDay(I)Ljava/util/ArrayList;
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget-short v3, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {p0, v3, v4}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEventList(SI)Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->mLock:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_1

    :cond_0
    monitor-exit v4

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    monitor-enter v4

    const/4 v0, 0x0

    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_2
    :try_start_3
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget v3, v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    if-ne v3, p1, :cond_4

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget v3, v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    if-gt v3, p1, :cond_3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget v3, v3, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ge v3, p1, :cond_3

    goto :goto_2
.end method

.method public getNvodTimeShiftEventCount(SI)I
    .locals 1
    .param p1    # S
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getNvodTimeShiftEventCount(SI)I

    move-result v0

    return v0
.end method

.method public getNvodTimeShiftEventInfo(SIILcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # S
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SII",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getNvodTimeShiftEventInfo(SIILcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPresentFollowingEventInfo(SIZLcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
    .locals 3
    .param p1    # S
    .param p2    # I
    .param p3    # Z
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getPresentFollowingEventInfo(SIZLcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const-string v1, "TvApp"

    const-string v2, "!!!getPresentFollowingEventInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public declared-synchronized getProgListByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Ljava/util/ArrayList;
    .locals 10
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mstar/android/tvapi/common/vo/EnumServiceType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v7, "EpgDeskImpl"

    const-string v8, "End getCurProgramInfo()"

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v6, v7}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->isNoProgInServiceType(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v7, "EpgDeskImpl"

    const-string v8, "NoProgInServiceType"

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v5

    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v7, "EpgDeskImpl"

    const-string v8, "HasProgInServiceType"

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->isProgCountEqualToZeroByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v7, "EpgDeskImpl"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ProgCount is Zero in ------>"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    invoke-virtual {v5, p1}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->getProgCountByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)I

    move-result v1

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "ProgCount is ------>"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v4}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    new-instance v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v3}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    if-nez v5, :cond_2

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    const-string v7, "ProgList is ------> a new one"

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "the type count is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_2
    if-lt v0, v1, :cond_3

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    goto/16 :goto_0

    :cond_2
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    const-string v7, "ProgList is ------> clear"

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    if-ne p1, v5, :cond_5

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgCount:Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v5, v6}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->getProgCountByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)I

    move-result v5

    add-int/2addr v5, v0

    iput v5, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->queryIndex:I

    :goto_3
    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->setServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    invoke-direct {p0, v3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getProgramInfoByIndex(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v4

    if-nez v4, :cond_6

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    const-string v7, "TvProgInfo is ------> null"

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    if-eqz v4, :cond_4

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    const-string v7, "TvProgInfo is ------> not null"

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    invoke-direct {v2}, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;-><init>()V

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->progId:I

    iput v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgDbId:I

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->queryIndex:I

    iput v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgIndex:I

    iget-object v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    iput-object v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iput v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    iget-short v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iput-short v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgType:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "name["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]no["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mProgList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    const-string v7, "_mProgList.add(kkProgInfo)"

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_5
    iput v0, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->queryIndex:I

    goto :goto_3

    :cond_6
    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v6, "EpgDeskImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "TvProgInfo is ------>"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4
.end method

.method public getProgListByServiceType(S)Ljava/util/ArrayList;
    .locals 3
    .param p1    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getServiceTypeByShort(S)Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getProgListByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_0
.end method

.method public getRctTrailerLink()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getRctTrailerLink()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getScheduleInfoByIndex(I)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;-><init>()V

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    iput v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    add-int/lit8 v3, v3, 0x0

    iput v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    iput v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventId:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryEventName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-direct {p0, v3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->queryServiceName(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    int-to-short v3, v3

    iput v3, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->serviceType:I

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\n EpgEventTimerInfo programNum "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "[eventName] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "[ProgName] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->deletePastScheduleInfo()V

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "sEventName"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "u32StartTime"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sServiceName"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    invoke-virtual {v2, v1, p3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->insertEpg(Landroid/content/ContentValues;I)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized modifyTimerNotifyTime(I)V
    .locals 11
    .param p1    # I

    monitor-enter p0

    const/4 v4, 0x0

    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "timer iDiffTime is ["

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] ============="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventCount()I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    :goto_0
    const/4 v2, 0x0

    :goto_1
    if-lt v2, v4, :cond_1

    if-lez v4, :cond_0

    const/4 v2, 0x0

    :goto_2
    if-lt v2, v4, :cond_3

    :cond_0
    :try_start_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    long-to-int v3, v7

    const/4 v2, 0x0

    :goto_3
    if-lt v2, v4, :cond_4

    monitor-exit p0

    return-void

    :catch_0
    move-exception v1

    :try_start_3
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_1
    :try_start_4
    invoke-virtual {p0, v2}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v5

    :goto_4
    if-eqz v5, :cond_2

    :try_start_5
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :cond_3
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->delEpgTimerEvent(IZ)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v7, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sub-int/2addr v7, p1

    iput v7, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget v7, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    add-int/lit8 v8, v3, 0x78

    if-ge v7, v8, :cond_5

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    :try_start_6
    invoke-virtual {p0, v5}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    :catch_2
    move-exception v1

    :try_start_7
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_5
.end method

.method public openDB()V
    .locals 0

    return-void
.end method

.method public resetEpgProgramPriority()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->resetEpgProgramPriority()V

    return-void
.end method

.method public setDispalyWindow(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput p1, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput p2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput p3, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput p4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDtvInputSource()V
    .locals 3

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getCurInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEpgProgramPriority(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->setEpgProgramPriority(I)V

    return-void
.end method

.method public setEpgProgramPriority(SI)V
    .locals 1
    .param p1    # S
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->epgMgr:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->setEpgProgramPriority(SI)V

    return-void
.end method

.method public setScheduleNofityTime(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "timer iNowNotifyTime is ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    iput p1, p0, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->_mEventDiffTime:I

    return-void
.end method

.method public updateTimerInfo(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p0, p1, p2, p3}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->insertScheduleInfo(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
