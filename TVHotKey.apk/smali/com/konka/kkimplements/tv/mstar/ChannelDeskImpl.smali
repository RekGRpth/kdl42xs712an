.class public Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "ChannelDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/ChannelDesk;


# static fields
.field private static channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;


# instance fields
.field private context:Landroid/content/Context;

.field private curChannelNumber:I

.field private curDtvRoute:S

.field dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

.field private prevChannelNumber:I

.field sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

.field private tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

.field private tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->prevChannelNumber:I

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    iput-short v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curDtvRoute:S

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    new-instance v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;-><init>()V

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    const/16 v3, 0x1adb

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iput v4, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v2

    iget-object v0, v2, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, "teac"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    return-object v0
.end method


# virtual methods
.method public DtvStopScan()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "DtvStopScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->stopScan()Z

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    return-object v0
.end method

.method public addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .param p2    # I
    .param p3    # S
    .param p4    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvGetCurrentFrequency()I
    .locals 4

    const-string v2, "TuningService"

    const-string v3, "atvGetCurrentFrequency"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getCurrentFrequency()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvGetProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;IILjava/lang/StringBuffer;)I
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvGetSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;
    .locals 5

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrentChannelNumber()I

    move-result v0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v4

    aget-object v4, v4, v3

    return-object v4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvGetVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 5

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrentChannelNumber()I

    move-result v0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    invoke-interface {v3, v4, v0}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v3

    aget-object v3, v3, v2

    return-object v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetAutoTuningEnd()Z
    .locals 4

    const-string v2, "TuningService"

    const-string v3, "atvSetAutoTuningStart"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningEnd()Z

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetAutoTuningPause()Z
    .locals 4

    const-string v2, "TuningService"

    const-string v3, "atvSetAutoTuningPause"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningPause()Z

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetAutoTuningResume()Z
    .locals 4

    const-string v2, "TuningService"

    const-string v3, "atvSetAutoTuningResume"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningResume()Z

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetAutoTuningStart(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v2, "TuningService"

    const-string v3, "atvSetAutoTuningStart"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceAtv()V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NONE_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    invoke-interface {v2, p1, p2, p3, v3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningStart(IIILcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;)Z

    move-result v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public atvSetChannel(SZ)I
    .locals 1
    .param p1    # S
    .param p2    # Z

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {p0, p1, v0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    const/4 v0, 0x0

    return v0
.end method

.method public atvSetForceSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrentChannelNumber()I

    move-result v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getValue()I

    move-result v2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_AUDIO_STANDARD:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    invoke-interface {v3, v4, v0, v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetForceVedioSystem(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrentChannelNumber()I

    move-result v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->forceVideoStandard(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    invoke-interface {v3, v4, v0, v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetFrequency(I)Z
    .locals 2
    .param p1    # I

    const-string v0, "TuningService"

    const-string v1, "atvSetFrequency!!!!!!! no such api "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public atvSetManualTuningEnd()V
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "atvSetManualTuningEnd"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setManualTuningEnd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceAtv()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-virtual {p0, v2, v3}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    move-result v1

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    if-ne p3, v2, :cond_2

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_2
    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    if-ne p3, v2, :cond_1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public atvSetProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const/4 v3, 0x0

    const-string v1, "TuningService"

    const-string v2, "ChangeToFirstService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/ChannelManager;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public closeSubtitle()Z
    .locals 2

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->closeSubtitle()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public closeTeletext()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->closeTeletext()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .param p2    # I
    .param p3    # S
    .param p4    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvAutoScan()Z
    .locals 3

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    const-string v1, "Tvapp"

    const-string v2, "use dvbt\'s scaning method"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbtScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DvbtScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DvbtScanManager;->startAutoScan()V

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvChangeManualScanRF(S)Z
    .locals 1
    .param p1    # S

    invoke-virtual {p0, p1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dtvManualScanRF(S)Z

    const/4 v0, 0x0

    return v0
.end method

.method public dtvFullScan()Z
    .locals 2

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startFullScan()Z

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;
    .locals 2

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getMSrvDtvRoute()S

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;
    .param p2    # I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->getRfInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvManualScanFreq(I)Z
    .locals 4
    .param p1    # I

    const-string v1, "TuningService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dtvManualScanFreq:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setManualTuneByFreq(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvManualScanRF(S)Z
    .locals 4
    .param p1    # S

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    const-string v1, "===========================dtvManualScanRF"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RFNum:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setManualTuneByRf(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvPauseScan()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "dtvPauseScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->pauseScan()Z

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvResumeScan()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "dtvResumeScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->resumeScan()Z

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->switchMSrvDtvRouteCmd(S)Z

    return-void
.end method

.method public dtvStartManualScan()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "dtvStartManualScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startManualScan()Z

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dtvplayCurrentProgram()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "dtvplayCurrentProgram"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->playCurrentProgram()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public dvbcgetScanParam(Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iput-short v0, p1, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iput-object v0, p1, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget v0, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    iput v0, p1, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    const/4 v0, 0x0

    return v0
.end method

.method public dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z
    .locals 9
    .param p1    # S
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;
    .param p3    # I
    .param p4    # I
    .param p5    # S

    const/4 v8, 0x0

    const-string v0, "TuningService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dvbcsetScanParam:S_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Q_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iput-object p2, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iput p3, v0, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v0

    const/4 v6, 0x0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v6}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->setScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IISZ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v8

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvStationName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {p0, v3}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v0

    if-eqz v0, :cond_0

    if-lt p1, v0, :cond_1

    :cond_0
    const-string v3, "TvApp"

    const-string v4, "getAtvStationName null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, " "

    :goto_0
    return-object v3

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvStationName(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public getAtvVolumeCompensation(I)I
    .locals 3
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramMiscInfo(I)Lcom/mstar/android/tvapi/common/vo/AtvProgramData;

    move-result-object v2

    iget-object v2, v2, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    iget-byte v1, v2, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVolumeCompensation:B
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getChinaDvbcRegion()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/mstar/android/tvapi/common/TvPlayer;->getChinaDvbcRegion()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_OTHERS:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    goto :goto_0
.end method

.method public getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_CURRENT:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {p0, v0, v1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    return-object v1
.end method

.method public getCurrentChannelNumber()I
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrChannelNumber()I

    move-result v1

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_3

    const/16 v2, 0xff

    if-gt v1, v2, :cond_1

    if-gez v1, :cond_2

    :cond_1
    const-string v2, "Mapp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getatvCurrentChannelNumber error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xff

    :cond_2
    :goto_0
    return v1

    :cond_3
    const/16 v2, 0x3e8

    if-gt v1, v2, :cond_4

    if-gez v1, :cond_2

    :cond_4
    const-string v2, "Mapp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getdtvCurrentChannelNumber error:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x3e8

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0
.end method

.method public getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentProgramSpecificInfo(Lcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;)Z
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;

    const-string v0, "TuningService"

    const-string v1, "getCurrentProgramSpecificInfo!!!!!!! no such api "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMSrvDtvRoute()S
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentDtvRoute()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curDtvRoute:S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v1, "TuningService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getMSrvDtvRoute:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v3, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curDtvRoute:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-short v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curDtvRoute:S

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getNitFrequencyByDtvRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)[I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->getNitFrequencyByDtvRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPogramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;
    .param p2    # I
    .param p3    # S
    .param p4    # I
    .param p5    # Z

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISI)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TuningService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getProgramCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;IILjava/lang/String;)I
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;II)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    const-string v2, "TuningService"

    const-string v3, "getdtvProgramInfo"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 3
    .param p1    # I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    iput p1, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->queryIndex:I

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_DATABASE_INDEX:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {p0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    return-object v0
.end method

.method public getProgramName(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;S)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .param p2    # Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;
    .param p3    # S

    const-string v2, "TuningService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getProgramName:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {p2}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {v2, p1, v3, p3}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramName(ISS)Ljava/lang/String;

    move-result-object v1

    :cond_0
    const-string v2, "TuningService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getProgramName:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramSpecificInfoByIndex(ILcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;)Z
    .locals 2
    .param p1    # I
    .param p2    # Lcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;

    const-string v0, "TuningService"

    const-string v1, "getCurrentProgramSpecificInfo!!!!!!! no such api "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public getSIFMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    goto :goto_0
.end method

.method public getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
    .locals 2

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-object v0
.end method

.method public getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    return-object v0
.end method

.method public getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasTeletextClockSignal()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->hasTeletextClockSignal()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public hasTeletextSignal()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->hasTeletextSignal()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isSignalStabled()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isTeletextDisplayed()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isTeletextDisplayed()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isTeletextSubtitleChannel()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isTeletextSubtitleChannel()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isTtxChannel()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->hasTeletextSignal()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public makeSourceAtv()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    const-string v0, "Tvapp"

    const-string v1, "makeSourceAtv"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_0
    return-void
.end method

.method public makeSourceDtv()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    const-string v0, "Tvapp"

    const-string v1, "makeSourceDtv"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_0
    return-void
.end method

.method public moveProgram(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/ChannelManager;->moveProgram(II)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public openSubtitle(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->openSubtitle(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programDown()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "programDown"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->programDown(Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programReturn()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "programReturn"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->prevChannelNumber:I

    iput v1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->returnToPreviousProgram()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z
    .locals 5
    .param p1    # I
    .param p2    # Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    const/4 v4, 0x0

    const-string v1, "TuningService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "programSel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "u8ServiceType"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {p2}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v2

    int-to-short v2, v2

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programUp()Z
    .locals 3

    const-string v1, "TuningService"

    const-string v2, "programUp"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->programUp(Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public readTurnChannelInterval(Landroid/content/Context;)I
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const-string v2, "content://endy.eyeguard.provider.confprovider/t_system_config/3000"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "item_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "item_name"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "field_value1"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "field_value2"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "field_value3"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "field_value4"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "field_value5"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "field_value6"

    aput-object v5, v2, v4

    const/16 v4, 0x8

    const-string v5, "field_value7"

    aput-object v5, v2, v4

    const-string v5, "item_id"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "field_value4"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v8
.end method

.method public saveAtvProgram(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->saveAtvProgram(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendAtvScaninfo()V
    .locals 0

    return-void
.end method

.method public sendDtvScaninfo()V
    .locals 0

    return-void
.end method

.method public sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvVolumeCompensation(II)Z
    .locals 7
    .param p1    # I
    .param p2    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    invoke-interface {v4, p1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramMiscInfo(I)Lcom/mstar/android/tvapi/common/vo/AtvProgramData;

    move-result-object v0

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/AtvProgramData;->misc:Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;

    int-to-byte v5, p2

    iput-byte v5, v4, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVolumeCompensation:B

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    invoke-interface {v4, p1, v0}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAtvProgramMiscInfo(ILcom/mstar/android/tvapi/common/vo/AtvProgramData;)Z

    move-result v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_COMPENSATION:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v6, p2

    invoke-virtual {v4, v5, v6}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V

    :cond_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->getSoundMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->getVolume()S

    move-result v3

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->getSoundMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    move-result-object v4

    int-to-short v5, v3

    invoke-virtual {v4, v5}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setVolume(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCableOperator(Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->setCableOperator(Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelChangeFreezeMode(Z)V
    .locals 2
    .param p1    # Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChinaDvbcRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setChinaDvbcRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V

    :cond_0
    return-void
.end method

.method public setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V
    .locals 7
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;
    .param p2    # I
    .param p3    # S
    .param p4    # I
    .param p5    # Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/tvapi/common/ChannelManager;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramName(ISLjava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # S
    .param p3    # Ljava/lang/String;

    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setProgramName:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2, p3}, Lcom/mstar/android/tvapi/common/ChannelManager;->setProgramName(ISILjava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSystemCountry(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v2, "TuningService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mem_country :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v2

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateCurCountry(I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    move-result-object v3

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    invoke-interface {v2, v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setCountry(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V
    .locals 0
    .param p1    # Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    return-void
.end method

.method public startAutoUpdateScan()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startAutoUpdateScan()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public startQuickScan()Z
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startQuickScan()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public switchAudioTrack(I)V
    .locals 2
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->switchAudioTrack(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchMSrvDtvRouteCmd(S)Z
    .locals 4
    .param p1    # S

    const-string v1, "TuningService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "switchMSrvDtvRouteCmd:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->makeSourceDtv()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->switchDtvRoute(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
