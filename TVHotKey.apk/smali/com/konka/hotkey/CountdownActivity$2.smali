.class Lcom/konka/hotkey/CountdownActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "CountdownActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/CountdownActivity;->registerBroadcastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/CountdownActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/CountdownActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/CountdownActivity$2;->this$0:Lcom/konka/hotkey/CountdownActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "===============receive broadcast ======"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity$2;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->isNoSignalStandby:Z
    invoke-static {v0}, Lcom/konka/hotkey/CountdownActivity;->access$3(Lcom/konka/hotkey/CountdownActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity$2;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/hotkey/CountdownActivity;->access$5(Lcom/konka/hotkey/CountdownActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity$2;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/hotkey/CountdownActivity;->access$5(Lcom/konka/hotkey/CountdownActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
