.class Lcom/konka/hotkey/ZoomActivity$ClickListener;
.super Ljava/lang/Object;
.source "ZoomActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/ZoomActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/ZoomActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/ZoomActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/ZoomActivity$ClickListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    move-object v1, p1

    check-cast v1, Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ClickListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/ZoomActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ClickListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity;->recoverItemNorBg()V

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ClickListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;
    invoke-static {v2}, Lcom/konka/hotkey/ZoomActivity;->access$2(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "picture skin is not null"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->doUpdate()Z

    :cond_1
    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ClickListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity;->finish()V

    goto :goto_0
.end method
