.class Lcom/konka/hotkey/volumeViewHolder$MyHandler;
.super Landroid/os/Handler;
.source "volumeViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/volumeViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/volumeViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/hotkey/volumeViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/hotkey/volumeViewHolder;Lcom/konka/hotkey/volumeViewHolder$MyHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;-><init>(Lcom/konka/hotkey/volumeViewHolder;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x6f

    if-ne v0, v1, :cond_1

    const-string v0, "VOLUME_INCREASE"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    const/4 v1, 0x1

    # invokes: Lcom/konka/hotkey/volumeViewHolder;->adjustVolume(Z)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/volumeViewHolder;->access$3(Lcom/konka/hotkey/volumeViewHolder;Z)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xde

    if-ne v0, v1, :cond_2

    const-string v0, "VOLUME_DECREASE"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    const/4 v1, 0x0

    # invokes: Lcom/konka/hotkey/volumeViewHolder;->adjustVolume(Z)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/volumeViewHolder;->access$3(Lcom/konka/hotkey/volumeViewHolder;Z)V

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x14d

    if-ne v0, v1, :cond_3

    const-string v0, "VOLUME_SET"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/konka/hotkey/volumeViewHolder;->setVolumeValue(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/volumeViewHolder;->access$4(Lcom/konka/hotkey/volumeViewHolder;I)V

    goto :goto_0

    :cond_3
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x3e9

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    invoke-virtual {v0}, Lcom/konka/hotkey/volumeViewHolder;->closeView()V

    goto :goto_0
.end method
