.class Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;
.super Landroid/widget/LinearLayout;
.source "Video3DAutoActivity.java"

# interfaces
.implements Lcom/konka/hotkey/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "S3DAutoView"
.end annotation


# instance fields
.field private SIZE_NOR:F

.field private final STATUS_OFF:I

.field private final STATUS_ON:I

.field private final TEXT1_NOR_LM:I

.field private WIDTH_TEXTVIEW1:I

.field private WIDTH_TEXTVIEW2:I

.field public final imgNextIdNor:I

.field public final imgNextIdSel:I

.field public final imgPreIdNor:I

.field public final imgPreIdSel:I

.field public mViewIvNext:Landroid/widget/ImageView;

.field public mViewIvPrev:Landroid/widget/ImageView;

.field public mViewTvContent:Landroid/widget/TextView;

.field public mViewTvState:Landroid/widget/TextView;

.field public miCurrState:I

.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;


# direct methods
.method public constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;Landroid/content/Context;I)V
    .locals 12
    .param p2    # Landroid/content/Context;
    .param p3    # I

    const/4 v11, -0x2

    const/4 v7, 0x1

    const/16 v10, 0xa

    const/4 v9, -0x1

    const/4 v8, 0x0

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/16 v5, 0xf

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->TEXT1_NOR_LM:I

    const/16 v5, 0xfa

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW1:I

    const/16 v5, 0x8c

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW2:I

    iput v8, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->STATUS_OFF:I

    iput v7, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->STATUS_ON:I

    const/high16 v5, 0x41880000    # 17.0f

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->SIZE_NOR:F

    const v5, 0x7f02002f    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_nor

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->imgPreIdNor:I

    const v5, 0x7f020030    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_sel

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->imgPreIdSel:I

    const v5, 0x7f02002c    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_nor

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->imgNextIdNor:I

    const v5, 0x7f02002d    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_sel

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->imgNextIdSel:I

    iput p3, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070002    # com.konka.hotkey.R.dimen.TVHotkeyS3dEnItemText1_Width

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW1:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070003    # com.konka.hotkey.R.dimen.TVHotkeyS3dEnItemText2_Width

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW2:I

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_WIDTH:I
    invoke-static {p1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$1(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)I

    move-result v5

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_HEIGHT:I
    invoke-static {p1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$2(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)I

    move-result v6

    invoke-direct {v1, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v7}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->setFocusable(Z)V

    invoke-virtual {p0, v7}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, v8}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->setOrientation(I)V

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW1:I

    invoke-direct {v0, v5, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0xf

    invoke-virtual {v0, v5, v8, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    const/16 v6, 0x10

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    const v6, 0x7f080023    # com.konka.hotkey.R.string.str_hk_3d_menu_auto

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    iget v6, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->SIZE_NOR:F

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    invoke-virtual {v5, v10, v8, v10, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvContent:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->addView(Landroid/view/View;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v11, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    const v6, 0x7f020030    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_sel

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    const v6, 0x7f02002d    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_sel

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    new-instance v6, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView$1;

    invoke-direct {v6, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView$1;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->addView(Landroid/view/View;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    iget v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->WIDTH_TEXTVIEW2:I

    invoke-direct {v4, v5, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    const/16 v6, 0x11

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    iget v6, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->SIZE_NOR:F

    const/high16 v7, 0x40000000    # 2.0f

    sub-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->StrValueIds:[I
    invoke-static {p1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$3(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)[I

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    invoke-virtual {v5, v10, v8, v10, v8}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v11, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    new-instance v6, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView$2;

    invoke-direct {v6, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView$2;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public KeyListener(ILandroid/view/KeyEvent;)V
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v4, 0x7f020030    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_sel

    const v3, 0x7f02002d    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_sel

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "in original"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v0, 0x15

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    if-nez v0, :cond_1

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->doUpdate()Z

    return-void

    :cond_1
    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    goto :goto_0

    :cond_2
    const/16 v0, 0x16

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    if-ne v1, v0, :cond_3

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    :goto_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvPrev:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewIvNext:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    :cond_3
    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    goto :goto_2
.end method

.method public doUpdate()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->mViewTvState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->StrValueIds:[I
    invoke-static {v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$3(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)[I

    move-result-object v1

    iget v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public getCurrState()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->miCurrState:I

    return v0
.end method
