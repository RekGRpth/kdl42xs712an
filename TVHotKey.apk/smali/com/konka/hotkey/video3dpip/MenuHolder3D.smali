.class public Lcom/konka/hotkey/video3dpip/MenuHolder3D;
.super Lcom/konka/hotkey/interfaces/V3DMenuBase;
.source "MenuHolder3D.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I


# instance fields
.field private ImgFbdId:[I

.field private ImgNorId:[I

.field private ImgRunId:[I

.field private ImgSelId:[I

.field private ItemTextId:[I

.field private mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

.field private mIsMVC:Z

.field public mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

.field private mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

.field public mItemsContainer:Landroid/widget/LinearLayout;

.field private mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private m_bIsMenuForbidden:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;IIFF)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/video3dpip/Video3DActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # F
    .param p5    # F

    const/16 v1, 0x8

    invoke-direct {p0}, Lcom/konka/hotkey/interfaces/V3DMenuBase;-><init>()V

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    const-string v0, "MenuHolder3D==>onCreate begin! =================================="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iput p2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iput p3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iput p4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iput p5, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    const/16 v0, 0x51

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getIsMVCFromSupernova()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->findViews()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D==>onCreate, width="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", norTextSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selTextSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v0, "MenuHolder3D==>onCreate done! =================================="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020036    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_sel
        0x7f02003e    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_sel
        0x7f02003a    # com.konka.hotkey.R.drawable.hk_3d_menu_off_sel
        0x7f02000b    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_sel
        0x7f020007    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_sel
        0x7f02001b    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_sel
        0x7f020013    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_sel
        0x7f020032    # com.konka.hotkey.R.drawable.hk_3d_menu_en_sel
    .end array-data

    :array_1
    .array-data 4
        0x7f020034    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_nor
        0x7f02003c    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_nor
        0x7f020038    # com.konka.hotkey.R.drawable.hk_3d_menu_off_nor
        0x7f020009    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_nor
        0x7f020005    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_nor
        0x7f020019    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_nor
        0x7f020011    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_nor
        0x7f02002e    # com.konka.hotkey.R.drawable.hk_3d_menu_en_nor
    .end array-data

    :array_2
    .array-data 4
        0x7f020035    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_run
        0x7f02003d    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_run
        0x7f020039    # com.konka.hotkey.R.drawable.hk_3d_menu_off_run
        0x7f02000a    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_run
        0x7f020006    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_run
        0x7f02001a    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_run
        0x7f020012    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_run
        0x7f020031    # com.konka.hotkey.R.drawable.hk_3d_menu_en_run
    .end array-data

    :array_3
    .array-data 4
        0x7f020033    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_fbd
        0x7f02003b    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_fbd
        0x7f020037    # com.konka.hotkey.R.drawable.hk_3d_menu_off_fbd
        0x7f020008    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_fbd
        0x7f020004    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_fbd
        0x7f020018    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_fbd
        0x7f020010    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_fbd
        0x7f02002a    # com.konka.hotkey.R.drawable.hk_3d_menu_en_fbd
    .end array-data

    :array_4
    .array-data 4
        0x7f08001d    # com.konka.hotkey.R.string.str_hk_3d_menu_lr
        0x7f08001e    # com.konka.hotkey.R.string.str_hk_3d_menu_ud
        0x7f08001f    # com.konka.hotkey.R.string.str_hk_3d_menu_off
        0x7f080020    # com.konka.hotkey.R.string.str_hk_3d_menu_3t2
        0x7f080021    # com.konka.hotkey.R.string.str_hk_3d_menu_2t3
        0x7f080022    # com.konka.hotkey.R.string.str_hk_3d_menu_clar
        0x7f080023    # com.konka.hotkey.R.string.str_hk_3d_menu_auto
        0x7f080024    # com.konka.hotkey.R.string.str_hk_3d_menu_en
    .end array-data
.end method

.method static synthetic access$1(Lcom/konka/hotkey/video3dpip/MenuHolder3D;)Lcom/konka/hotkey/video3dpip/Video3DActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    return-object v0
.end method

.method private addItem3D2To3()V
    .locals 10

    const/4 v9, 0x4

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$5;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$5;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_2To3"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3D2To3"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3D3To2()V
    .locals 10

    const/4 v9, 0x3

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$4;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$4;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_3To2"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3D3To2"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DAuto()V
    .locals 10

    const/4 v9, 0x6

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_Auto"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DAuto"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DClar()V
    .locals 10

    const/4 v9, 0x5

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$6;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$6;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_Clar"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DClar"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DEn()V
    .locals 10

    const/4 v9, 0x7

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_En"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DEn"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DLR()V
    .locals 10

    const/4 v9, 0x0

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$1;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$1;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_LR"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DLR"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DOff()V
    .locals 10

    const/4 v9, 0x2

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$3;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$3;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_Off"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DOff"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private addItem3DTB()V
    .locals 10

    const/4 v9, 0x1

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$2;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ItemTextId:[I

    aget v1, v1, v9

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getText(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$2;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_TB"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgSelId:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgRunId:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgFbdId:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ImgNorId:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string v0, "MenuHolder3D===>addItem3DTB"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private getText(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0, p1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isReadyShow3D(Landroid/content/Context;)Z
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v2, "activity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "The package of top task is ===="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "browser"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "launcher"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "avenger"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "metrolauncher"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "epg"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "karaoke"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "doublechannelentry"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_0
.end method


# virtual methods
.method public checkItem3DAutoState()V
    .locals 4

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-static {}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    :goto_0
    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DClar()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getIsMVC()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    :cond_2
    const-string v2, "MenuHolder3D===>checkItem3DAutoState done."

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "MenuHolder3D===>checkItem3DAutoState done."

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x1d -> :sswitch_0
    .end sparse-switch
.end method

.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->finish()V

    return-void
.end method

.method public display3D2To3()V
    .locals 9

    const/16 v8, 0x18

    const/16 v7, 0x12

    const/16 v6, 0xc

    const/4 v5, 0x0

    const/4 v3, 0x4

    iput v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "GetGainAndOffsetMode"

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    array-length v3, v1

    if-nez v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    new-array v1, v3, [S

    const-string v3, "========================pq has not data"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    aget-short v3, v1, v5

    packed-switch v3, :pswitch_data_0

    :goto_2
    const-string v3, "MenuHolder3D==>display3D2To3"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "========================pq index"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-short v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_0
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto :goto_2

    :pswitch_1
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v8}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v8}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto :goto_2

    :pswitch_2
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v3, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto :goto_2

    :pswitch_3
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DDepthMode()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DOffsetMode()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public display3D3To2()V
    .locals 5

    const/4 v3, 0x3

    iput v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "=======3to2===============get current 3d mode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v3

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v4

    aget v3, v3, v4
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v3, :pswitch_data_0

    :goto_0
    const-string v3, "MenuHolder3D==>display3D3To2"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    const-string v3, "===========================3d item 3to2 lf=============="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_2
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    const-string v3, "===========================3d item 3to2 tb=============="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIs3DClar(Z)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    const-string v3, "===========================3d item 3to2 clar FRAME_PACKING_1080P=============="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIs3DClar(Z)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    const-string v3, "===========================3d item 3to2 clar FRAME_PACKING_720P=============="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public display3DAuto()V
    .locals 3

    const-string v1, "start the menu 3d auto"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "IS_DOBULE_CHANNEL"

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isDoubleChannel()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const-class v2, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->startActivity(Landroid/content/Intent;)V

    const-string v1, "MenuHolder3D==>display3DAuto"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public display3DClar()V
    .locals 3

    const/4 v1, 0x5

    iput v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIs3DClar(Z)V

    const-string v1, "MenuHolder3D==>display3DClar"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public display3DEn()V
    .locals 5

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->getRight()I

    move-result v4

    sub-int v4, v3, v4

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v3}, Lcom/konka/hotkey/view/Com3DPIPItem;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v3

    sub-int v3, v4, v3

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_WIDTH:I

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->ITEM_HEIGHT:I

    add-int/lit8 v0, v3, 0x32

    const-string v3, "RightMargin"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "BottomMargin"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "CURR_MODE"

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "IS_DOBULE_CHANNEL"

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v4}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isDoubleChannel()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->startActivity(Landroid/content/Intent;)V

    const-string v3, "MenuHolder3D==>display3DEn"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public display3DLR()V
    .locals 3

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const-string v1, "MenuHolder3D==>display3DLR"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public display3DOff()V
    .locals 3

    const/4 v1, 0x2

    iput v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const-string v1, "MenuHolder3D==>display3DOff"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public display3DTB()V
    .locals 3

    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const-string v1, "MenuHolder3D==>display3DTB"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public findViews()V
    .locals 3

    const-string v1, "MenuHolder3D==>findViews start."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const v2, 0x7f0a0029    # com.konka.hotkey.R.id.hk_3d_menu_container

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DLR()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DTB()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DOff()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3D3To2()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3D2To3()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DClar()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DAuto()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DEn()V

    const-string v1, "MenuHolder3D==>findViews done."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public getIsMVC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z

    return v0
.end method

.method public getIsMVCFromSupernova()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-array v1, v2, [S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const-string v3, "GetMVC3DFlag"

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z

    const-string v2, "========================GetIsMVCFormat not data"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "========================GetIsMVCFormat:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    aget-short v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v2, 0x0

    aget-short v2, v1, v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mIsMVC:Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getLastFocusItem()Lcom/konka/hotkey/view/Com3DPIPItem;
    .locals 1

    const-string v0, "MenuHolder3D==>getLastFocusItem"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    return-object v0
.end method

.method public isMenuForbidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    return v0
.end method

.method public loadDataToUI()V
    .locals 11

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v9, "MenuHolder3D===>loadDataToUI start."

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v9, -0x1

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v4

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mS3DDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v3

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI======get display format: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI======get display 3d to 2d: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_a

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI========get current 3d mode: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_4

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_4

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getIsMVC()Z

    move-result v9

    if-nez v9, :cond_4

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v6, v9, :cond_0

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v3, v9, :cond_4

    :cond_0
    move v0, v7

    :goto_0
    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_5

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getIsMVC()Z

    move-result v9

    if-nez v9, :cond_5

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_5

    move v2, v7

    :goto_1
    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_1

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v6, v9, :cond_1

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v6, v9, :cond_6

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v3, v9, :cond_6

    :cond_1
    move v1, v7

    :goto_2
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    const/4 v1, 0x0

    :cond_3
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v9, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIs3DClar(Z)V

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v9, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIsFramePacking(Z)V

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v9, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->set3DEnabled(Z)V

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI: m_bIs3DEnabled = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI: m_bIs3DClar = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "MenuHolder3D===>loadDataToUI: m_bIsFramePacking = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v9

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v3, v9, :cond_8

    const/4 v9, 0x2

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v9

    if-nez v9, :cond_b

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v9

    if-nez v9, :cond_b

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v9

    if-nez v9, :cond_b

    :goto_4
    iput-boolean v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    iget-object v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-boolean v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    invoke-virtual {v7, v8}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iput v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_MODE:I

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "MenuHolder3D===>loadDataToUI: m_bIsMenuForbidden = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v7, "MenuHolder3D===>loadDataToUI done."

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_4
    move v0, v8

    goto/16 :goto_0

    :cond_5
    move v2, v8

    goto/16 :goto_1

    :cond_6
    move v1, v8

    goto/16 :goto_2

    :pswitch_1
    const/4 v9, 0x0

    :try_start_1
    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_2
    :try_start_2
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getIsMVC()Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x5

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :cond_7
    const/4 v9, 0x1

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :pswitch_3
    const/4 v9, 0x4

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :pswitch_4
    const/4 v9, 0x5

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :cond_8
    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v3, v9, :cond_9

    const/4 v9, 0x3

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :cond_9
    const/4 v9, 0x3

    iput v9, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    goto :goto_3

    :cond_a
    const-string v9, "TvManger.getInstance() == null"

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :cond_b
    move v7, v8

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setCurrMode(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x7

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    return-void
.end method

.method public setMenuForbidden()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    const-string v1, "MenuHolder3D==>setMenuForbidden, m_bIsMenuForbidden:false>>>true! All items forbidden."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "MenuHolder3D==>setMenuForbidden, m_bIsMenuForbidden:true >>>true! Did nothing, already forbidden!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuResume()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    if-eqz v2, :cond_0

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemStates(Z)V

    const-string v1, "MenuHolder3D==>setMenuResume, m_bIsMenuForbidden:true >>>false! Done."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v0, "MenuHolder3D==>setMenuResume, m_bIsMenuForbidden:false>>>false! Did nothing, already resume!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public updateItemState3D2To3(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3D2To3, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbiddenPIPMenu()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    const-string v0, "MenuHolder3D===>updateItemState3D2To3 done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemState3D3To2(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3D3To2, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DClar()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbiddenPIPMenu()V

    const-string v0, "MenuHolder3D===>updateItemState3D3To2 done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0
.end method

.method public updateItemState3DAuto()V
    .locals 1

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v0, "MenuHolder3D===>updateItemState3DAuto done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemState3DClar(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3DClar, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->checkItem3DAutoState()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbiddenPIPMenu()V

    const-string v0, "MenuHolder3D===>updateItemState3DClar done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemState3DEn()V
    .locals 1

    const/4 v0, 0x7

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v0, "MenuHolder3D===>updateItemState3DEn done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemState3DLR(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3DLR, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbiddenPIPMenu()V

    const-string v0, "MenuHolder3D===>updateItemState3DLR done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemState3DOff(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3DOff, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DClar()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    const-string v0, "MenuHolder3D===>updateItemState3DOff, mItem3dClar requestFocus."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->resumePIPMenu()V

    const-string v0, "MenuHolder3D===>updateItemState3DOff done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    const-string v0, "MenuHolder3D===>updateItemState3DOff, mItem3dLR requestFocus."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0
.end method

.method public updateItemState3DTB(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolder3D===>updateItemState3DTB, start. bSendFocusToNearItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dLR:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dTB:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dOff:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d3To2:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3d2To3:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dClar:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbiddenPIPMenu()V

    const-string v0, "MenuHolder3D===>updateItemState3DTB done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemStates(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MenuHolder3D==>updateItemStates, start, bSendFocusToNearItem:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->isReadyShow3D(Landroid/content/Context;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MenuHolder3D==>updateItemStates, isPIPEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isPIPEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MenuHolder3D==>updateItemStates, isReadyShow3D(mActivity)="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MenuHolder3D==>updateItemStates, is3DVisible(mActivity)="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2, v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DVisible(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isPIPEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DVisible(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->m_bIsMenuForbidden:Z

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v4}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->set3DEnabled(Z)V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->setMenuForbidden()Z

    :goto_0
    const-string v1, "MenuHolder3D==>updateItemStates, done."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->CURR_ITEM:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_LR"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DLR(Z)V

    goto :goto_0

    :pswitch_1
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_TB"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DTB(Z)V

    goto :goto_0

    :pswitch_2
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_OFF"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DOff(Z)V

    goto :goto_0

    :pswitch_3
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_3TO2"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3D3To2(Z)V

    goto :goto_0

    :pswitch_4
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_2TO3"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3D2To3(Z)V

    goto :goto_0

    :pswitch_5
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_CLAR"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DClar(Z)V

    goto :goto_0

    :pswitch_6
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_AUTO"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dAuto:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    goto :goto_0

    :pswitch_7
    const-string v1, "MenuHolder3D==>updateItemStates==>CURR_ITEM : HK3D_EN"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mItem3dEn:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
