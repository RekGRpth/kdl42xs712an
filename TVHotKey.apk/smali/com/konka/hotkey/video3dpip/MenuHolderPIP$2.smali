.class Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;
.super Lcom/konka/hotkey/view/Com3DPIPItem;
.source "MenuHolderPIP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemPOP()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V
    .locals 8
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # F
    .param p8    # I

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/konka/hotkey/view/Com3DPIPItem;-><init>(Landroid/content/Context;IIILjava/lang/String;FI)V

    return-void
.end method


# virtual methods
.method public doReturn()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->dismiss()V

    return-void
.end method

.method public doUpdate()Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->displayPOP()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    # getter for: Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;
    invoke-static {v1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->access$0(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;)Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStatePOP(Z)V

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    # getter for: Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->access$0(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;)Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->popToast()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget-object v2, v2, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doUpdateMenuStateNotSupport(Lcom/konka/hotkey/view/Com3DPIPItem;)V

    move v0, v1

    goto :goto_0
.end method
