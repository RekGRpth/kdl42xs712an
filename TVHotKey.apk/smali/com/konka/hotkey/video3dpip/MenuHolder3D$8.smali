.class Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;
.super Lcom/konka/hotkey/view/Com3DPIPItem;
.source "MenuHolder3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DEn()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V
    .locals 8
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # F
    .param p8    # I

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/konka/hotkey/view/Com3DPIPItem;-><init>(Landroid/content/Context;IIILjava/lang/String;FI)V

    return-void
.end method


# virtual methods
.method public doReturn()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->dismiss()V

    return-void
.end method

.method public doUpdate()Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    # getter for: Lcom/konka/hotkey/video3dpip/MenuHolder3D;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->access$1(Lcom/konka/hotkey/video3dpip/MenuHolder3D;)Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->set3DEnabled(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DEn()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->display3DEn()V

    return v1
.end method

.method public setItemStateInUse()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$8;->setItemStateInUseWithoutDisabled()V

    return-void
.end method
