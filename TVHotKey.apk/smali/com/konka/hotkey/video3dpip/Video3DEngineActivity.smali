.class public Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;
.super Landroid/app/Activity;
.source "Video3DEngineActivity.java"


# static fields
.field private static final ACTION_3DEN_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_3DEN_RETURN"

.field public static bCheckS3dEngineFirstCreate:Z


# instance fields
.field private final STATE_OFF:I

.field private final STATE_ON:I

.field private final STATE_SEQ_NEG:I

.field private final STATE_SEQ_POS:I

.field private itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

.field private itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

.field private mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private miClarity:[S

.field private miDepth:I

.field private miEffect:[S

.field private miOffset:I

.field private miPq:[S

.field private miSequence:I

.field private miWhich3DMode:I

.field private myHandler:Landroid/os/Handler;

.field private final strStateEffect:[I

.field private final strStateOffOn:[I

.field private final strStateSequence:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->bCheckS3dEngineFirstCreate:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    iput v4, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miDepth:I

    iput v4, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miOffset:I

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miWhich3DMode:I

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->STATE_OFF:I

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->STATE_ON:I

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->STATE_SEQ_POS:I

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->STATE_SEQ_NEG:I

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->strStateOffOn:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->strStateSequence:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->strStateEffect:[I

    new-instance v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$1;-><init>(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->myHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x7f080002    # com.konka.hotkey.R.string.str_hk_state_off
        0x7f080001    # com.konka.hotkey.R.string.str_hk_state_on
    .end array-data

    :array_1
    .array-data 4
        0x7f080030    # com.konka.hotkey.R.string.str_hk_3d_en_sequence_state_positive
        0x7f080031    # com.konka.hotkey.R.string.str_hk_3d_en_sequence_state_negative
    .end array-data

    :array_2
    .array-data 4
        0x7f08002c    # com.konka.hotkey.R.string.str_hk_3d_en_effect_state_standard
        0x7f08002d    # com.konka.hotkey.R.string.str_hk_3d_en_effect_state_strong
        0x7f08002e    # com.konka.hotkey.R.string.str_hk_3d_en_effect_state_weak
        0x7f08002f    # com.konka.hotkey.R.string.str_hk_3d_en_effect_state_custom
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/hotkey/view/S3dEnItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method private findFocus()V
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miWhich3DMode:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setStatusFbd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateMenuPosition(Z)V
    .locals 4
    .param p1    # Z

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "POPPIP==>Video3DEngine==>IS_DOBULD_CHANNEL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v2, 0x7f0a0015    # com.konka.hotkey.R.id.hk_3d_menu_en_layout

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_3

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x97

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v2, "DoubleChannel==>V3DEngine==>updateMenuPosition: success!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/16 v2, 0xdd

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0xea

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_2
    const/16 v2, 0x166

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_3
    const-string v2, "DoubleChannel==>V3DEngine==>updateMenuPosition: fail!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public LoadData()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/S3DDesk;->getLRViewSwitch()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v2

    if-ne v1, v2, :cond_3

    iput v4, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miSequence:I

    :goto_0
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DDepthMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miDepth:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DOffsetMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miOffset:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "3d depth============================================================"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miDepth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "3d offset============================================================"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miOffset:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetGainAndOffsetMode"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetPicQualityImproved"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetLOW3dQualityStatus"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miClarity:[S

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    array-length v1, v1

    if-nez v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    const-string v1, "========================pq has not data"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    array-length v1, v1

    if-nez v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    const-string v1, "========================pq has not data"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_2
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miClarity:[S

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miClarity:[S

    array-length v1, v1

    if-nez v1, :cond_6

    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miClarity:[S

    const-string v1, "========================pq has not data"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    return-void

    :cond_3
    iput v3, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miSequence:I

    goto/16 :goto_0

    :cond_4
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========================pq index"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :cond_5
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========================pq index"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miPq:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========================iClar index"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miClarity:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    const-string v1, "TvManger.getInstance() == null"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3
.end method

.method public addItemEffect()V
    .locals 7

    new-instance v0, Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miEffect:[S

    const/4 v2, 0x0

    aget-short v3, v1, v2

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miOffset:I

    iget v5, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miDepth:I

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->strStateEffect:[I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/hotkey/view/S3dEnEffectItem;-><init>(Landroid/app/Activity;Landroid/content/Context;III[I)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

    return-void
.end method

.method public addItemSequence()V
    .locals 7

    new-instance v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miSequence:I

    const v5, 0x7f0a0027    # com.konka.hotkey.R.id.hk_3d_en_sequence_container

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->strStateSequence:[I

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;-><init>(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;Landroid/app/Activity;Landroid/content/Context;II[I)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030006    # com.konka.hotkey.R.layout.hk_video3d_engine_menu

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    const/4 v2, 0x1

    sput-boolean v2, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->bCheckS3dEngineFirstCreate:Z

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "CURR_MODE"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miWhich3DMode:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "===========================3d mode =============="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->miWhich3DMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->LoadData()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->viewInit()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->findFocus()V

    const-string v2, "IS_DOBULE_CHANNEL"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->updateMenuPosition(Z)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->getInstance()Lcom/konka/hotkey/LittleDownTimer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/hotkey/LittleDownTimer;->start()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    if-ne v1, p1, :cond_0

    const-string v1, "on key down"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->myHandler:Landroid/os/Handler;

    const/16 v2, 0x40a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/S3dEnItem;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemEffect:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/S3dEnItem;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/high16 v0, 0x7f040000    # com.konka.hotkey.R.anim.anim_zoom_in

    const v1, 0x7f040001    # com.konka.hotkey.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->finish()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public viewInit()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->addItemEffect()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->addItemSequence()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->bCheckS3dEngineFirstCreate:Z

    return-void
.end method
