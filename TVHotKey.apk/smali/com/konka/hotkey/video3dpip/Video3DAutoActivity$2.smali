.class Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;
.super Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;
.source "Video3DAutoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->viewInit()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;Landroid/content/Context;I)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;)Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    return-object v0
.end method


# virtual methods
.method public doUpdate()Z
    .locals 2

    invoke-super {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->doUpdate()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$4(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->getCurrState()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Close"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2$1;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2$1;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->access$0(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "Open"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2$2;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2$2;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
