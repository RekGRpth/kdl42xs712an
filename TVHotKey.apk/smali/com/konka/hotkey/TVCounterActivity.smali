.class public Lcom/konka/hotkey/TVCounterActivity;
.super Landroid/app/Activity;
.source "TVCounterActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/TVCounterActivity$RootReceiver;
    }
.end annotation


# instance fields
.field private linearLayoutPopupOffTime:Landroid/widget/LinearLayout;

.field private linearLayoutPopupOnTime:Landroid/widget/LinearLayout;

.field private linearLayoutReadyOff:Landroid/widget/LinearLayout;

.field private linearLayoutReadySwitch:Landroid/widget/LinearLayout;

.field public rootReceiver:Lcom/konka/hotkey/TVCounterActivity$RootReceiver;

.field private textViewOffTimeSecond:Landroid/widget/TextView;

.field private textViewOnTimeSecond:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->textViewOffTimeSecond:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOnTime:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->textViewOnTimeSecond:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutReadySwitch:Landroid/widget/LinearLayout;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const-string v1, "TVCounterActivity"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03000d    # com.konka.hotkey.R.layout.tv_counter

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->setContentView(I)V

    const v1, 0x7f0a0034    # com.konka.hotkey.R.id.linear_layout_popup_ontime

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOnTime:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0035    # com.konka.hotkey.R.id.text_view_second

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->textViewOnTimeSecond:Landroid/widget/TextView;

    const v1, 0x7f0a0037    # com.konka.hotkey.R.id.linear_layout_popup_offtime

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0038    # com.konka.hotkey.R.id.text_view_offsecond

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->textViewOffTimeSecond:Landroid/widget/TextView;

    const v1, 0x7f0a0039    # com.konka.hotkey.R.id.linear_layout_popup_ready_switch

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutReadySwitch:Landroid/widget/LinearLayout;

    const v1, 0x7f0a003a    # com.konka.hotkey.R.id.linear_layout_popup_ready_off

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/TVCounterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutReadyOff:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;-><init>(Lcom/konka/hotkey/TVCounterActivity;Lcom/konka/hotkey/TVCounterActivity$RootReceiver;)V

    iput-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->rootReceiver:Lcom/konka/hotkey/TVCounterActivity$RootReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.android.server.tv.TIME_EVENT_DESTROY_COUNT_DOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_WARN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/TVCounterActivity;->rootReceiver:Lcom/konka/hotkey/TVCounterActivity$RootReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/hotkey/TVCounterActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->rootReceiver:Lcom/konka/hotkey/TVCounterActivity$RootReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/TVCounterActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvTimerManager;->setOffTimerEnable(Z)Z

    invoke-virtual {p0}, Lcom/konka/hotkey/TVCounterActivity;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/TVCounterActivity;->finish()V

    const-string v0, "ActivityXXXXXXXXXX========>>>onStop!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "onUserInteraction"

    const-string v1, "setOffTimerEnable(false)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mstar/android/tv/TvTimerManager;->setOffTimerEnable(Z)Z

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvTimerManager;->setSleepMode(Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;)Z

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOnTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "onUserInteraction"

    const-string v1, "setOnTimerEnable(false)"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mstar/android/tv/TvTimerManager;->setOnTimerEnable(Z)Z

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/TVCounterActivity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
