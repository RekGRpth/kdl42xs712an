.class public Lcom/konka/hotkey/EpgOrderInfoActivity;
.super Landroid/app/Activity;
.source "EpgOrderInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;
    }
.end annotation


# static fields
.field private static final CMD_CANCEL_TIMER:I = 0x2887

.field private static final CMD_REFRESH_TEXT:I = 0x2885

.field private static final CMD_TIME_OUT:I = 0x2886

.field private static final TIME_OFFSET:I


# instance fields
.field private final DELAY_TIME:I

.field private final PERIOD_TIME:I

.field private bCheckFinishByTimeOut:Z

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

.field private mViewBtnCancel:Landroid/widget/Button;

.field private mViewBtnOk:Landroid/widget/Button;

.field private mViewTvCountdown:Landroid/widget/TextView;

.field private mbTimer:Z

.field private myHandler:Landroid/os/Handler;

.field private sec:I

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private strChannelName:Ljava/lang/String;

.field private strChannelTime:Ljava/lang/String;

.field private strProgramName:Ljava/lang/String;

.field private timer:Ljava/util/Timer;

.field timerSkin:Lcom/mstar/android/tv/TvTimerManager;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0x3e8

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->DELAY_TIME:I

    iput v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->PERIOD_TIME:I

    iput v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->sec:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->strChannelName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->strProgramName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->strChannelTime:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;

    iput-boolean v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    iput-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iput-boolean v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->bCheckFinishByTimeOut:Z

    new-instance v0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/EpgOrderInfoActivity$1;-><init>(Lcom/konka/hotkey/EpgOrderInfoActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/EpgOrderInfoActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewTvCountdown:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/EpgOrderInfoActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->sec:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/EpgOrderInfoActivity;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/EpgOrderInfoActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->bCheckFinishByTimeOut:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/hotkey/EpgOrderInfoActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    return-void
.end method

.method static synthetic access$7(Lcom/konka/hotkey/EpgOrderInfoActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/hotkey/EpgOrderInfoActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    return v0
.end method

.method static synthetic access$9(Lcom/konka/hotkey/EpgOrderInfoActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->sec:I

    return-void
.end method

.method private getEpgTimeDiffTime()I
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    const-string v4, "com.konka.cloudsearch"

    const/4 v5, 0x3

    invoke-virtual {p0, v4, v5}, Lcom/konka/hotkey/EpgOrderInfoActivity;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const-string v4, "cloudsearchInfo"

    const/4 v5, 0x7

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "cloudsearchSettingTime"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_1
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    const/16 v2, 0x78

    goto :goto_1

    :pswitch_1
    const/16 v2, 0x12c

    goto :goto_1

    :pswitch_2
    const/16 v2, 0x258

    goto :goto_1

    :pswitch_3
    const/16 v2, 0x708

    goto :goto_1

    :pswitch_4
    const/4 v2, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public loadEpgData()V
    .locals 7

    const/16 v6, 0xa

    iget-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/EpgDesk;->getScheduleInfoByIndex(I)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    if-eqz v4, :cond_0

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    iget-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v4, v4, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    int-to-long v0, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/text/format/Time;->set(J)V

    new-instance v2, Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iget v4, v3, Landroid/text/format/Time;->hour:I

    if-ge v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Landroid/text/format/Time;->hour:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget v4, v3, Landroid/text/format/Time;->minute:I

    if-ge v4, v6, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Landroid/text/format/Time;->minute:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v4, v3, Landroid/text/format/Time;->hour:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Landroid/text/format/Time;->minute:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const-string v1, "here to call EpgOrderInfo Activity onCreate"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v1, "here to call EpgOrderInfo Activity onCreate"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030002    # com.konka.hotkey.R.layout.hk_order_info_menu_simple

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/EpgOrderInfoActivity;->setContentView(I)V

    iput-boolean v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->bCheckFinishByTimeOut:Z

    const v1, 0x7f0a0005    # com.konka.hotkey.R.id.hk_order_info_tv_countdown

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/EpgOrderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewTvCountdown:Landroid/widget/TextView;

    const v1, 0x7f0a000d    # com.konka.hotkey.R.id.hk_order_info_btn_ok

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/EpgOrderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewBtnOk:Landroid/widget/Button;

    const v1, 0x7f0a000e    # com.konka.hotkey.R.id.hk_order_info_btn_cancel

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/EpgOrderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewBtnCancel:Landroid/widget/Button;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/EpgDesk;->openDB()V

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    const/16 v1, 0xa

    iput v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->sec:I

    new-instance v0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;-><init>(Lcom/konka/hotkey/EpgOrderInfoActivity;)V

    iget-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewBtnOk:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewBtnCancel:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/EpgOrderInfoActivity;->loadEpgData()V

    iput-boolean v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/16 v1, 0x6f

    if-ne p1, v1, :cond_1

    :cond_0
    iput-boolean v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;

    const/16 v1, 0x2887

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected onResume()V
    .locals 6

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "EpgOrderInfoActivity Resume "

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;

    invoke-virtual {p0}, Lcom/konka/hotkey/EpgOrderInfoActivity;->sendMsg()Ljava/util/TimerTask;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->mbTimer:Z

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-boolean v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->bCheckFinishByTimeOut:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;

    const/16 v1, 0x2887

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const-string v0, "ActivityXXXXXXXXXX========>>>onStop!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public sendMsg()Ljava/util/TimerTask;
    .locals 1

    new-instance v0, Lcom/konka/hotkey/EpgOrderInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/EpgOrderInfoActivity$2;-><init>(Lcom/konka/hotkey/EpgOrderInfoActivity;)V

    return-object v0
.end method
