.class Lcom/konka/hotkey/PictureActivity$FocusChangeListener;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/PictureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FocusChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/PictureActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/PictureActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/PictureActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    move-object v0, p1

    check-cast v0, Lcom/konka/hotkey/PictureActivity$PictureItem;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    # getter for: Lcom/konka/hotkey/PictureActivity;->SIZE_SEL:F
    invoke-static {v1}, Lcom/konka/hotkey/PictureActivity;->access$0(Lcom/konka/hotkey/PictureActivity;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/PictureActivity$PictureItem;->getFocus(F)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/konka/hotkey/PictureActivity$PictureItem;->mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    # getter for: Lcom/konka/hotkey/PictureActivity;->SIZE_NOR:F
    invoke-static {v1}, Lcom/konka/hotkey/PictureActivity;->access$1(Lcom/konka/hotkey/PictureActivity;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/PictureActivity$PictureItem;->lostFocus(F)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lost: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/konka/hotkey/PictureActivity$PictureItem;->mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
