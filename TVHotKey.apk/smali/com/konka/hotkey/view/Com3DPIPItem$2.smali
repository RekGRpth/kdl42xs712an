.class Lcom/konka/hotkey/view/Com3DPIPItem$2;
.super Ljava/lang/Object;
.source "Com3DPIPItem.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/view/Com3DPIPItem;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/Com3DPIPItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/Com3DPIPItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem$2;->this$0:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    sparse-switch p2, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem$2;->this$0:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->onItemClick()Z

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem$2;->this$0:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->doReturn()V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
        0x49 -> :sswitch_1
        0x6f -> :sswitch_1
    .end sparse-switch
.end method
