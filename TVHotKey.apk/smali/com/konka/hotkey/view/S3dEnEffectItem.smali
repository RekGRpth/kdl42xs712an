.class public Lcom/konka/hotkey/view/S3dEnEffectItem;
.super Ljava/lang/Object;
.source "S3dEnEffectItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/view/S3dEnEffectItem$ArrowsClickListener;,
        Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;,
        Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;,
        Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;,
        Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;
    }
.end annotation


# instance fields
.field private final STATE_EFF_NOR:I

.field private final STATE_EFF_STRG:I

.field private final STATE_EFF_USER:I

.field private final STATE_EFF_WEAK:I

.field private bFocuse:Z

.field mActivity:Landroid/app/Activity;

.field private mArrowsNext:Landroid/widget/ImageView;

.field private mArrowsPre:Landroid/widget/ImageView;

.field mContext:Landroid/content/Context;

.field private mDepthValue:I

.field private mEffectIndex:I

.field mEffectStatus:[I

.field private mLayoutDepth:Landroid/widget/LinearLayout;

.field private mLayoutDepthText:Landroid/widget/LinearLayout;

.field private mLayoutEffect:Landroid/widget/LinearLayout;

.field private mLayoutOffset:Landroid/widget/LinearLayout;

.field private mLayoutOffsetText:Landroid/widget/LinearLayout;

.field private mOffsetValue:I

.field private mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private mSbDepth:Landroid/widget/SeekBar;

.field private mSbOffset:Landroid/widget/SeekBar;

.field private mViewDepth:Landroid/widget/TextView;

.field private mViewDepthContent:Landroid/widget/TextView;

.field private mViewEffect:Landroid/widget/TextView;

.field private mViewEffectContent:Landroid/widget/TextView;

.field private mViewOffset:Landroid/widget/TextView;

.field private mViewOffsetContent:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;III)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_NOR:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_STRG:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_WEAK:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_USER:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iput-boolean v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->bFocuse:Z

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    iput p4, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iput p5, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iput p3, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    invoke-virtual {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->findViewById()V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->updateUI()V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->listenerInit()V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetDepthVisibility(I)V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->refreshArrowsImg(I)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;III[I)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # [I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_NOR:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_STRG:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_WEAK:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->STATE_EFF_USER:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    iput v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iput-boolean v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->bFocuse:Z

    iput-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    iput p4, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iput p5, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iput p3, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    iput-object p6, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-virtual {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->findViewById()V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->updateUI()V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->listenerInit()V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetDepthVisibility(I)V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->refreshArrowsImg(I)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    return-void
.end method

.method static synthetic access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    return v0
.end method

.method static synthetic access$21(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    return-void
.end method

.method static synthetic access$22(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->refreshArrowsImg(I)V

    return-void
.end method

.method static synthetic access$23(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetDepthVisibility(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/hotkey/view/S3dEnEffectItem;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private listenerInit()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ArrowsClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ArrowsClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$ArrowsClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$ArrowsClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;-><init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method private refreshArrowsImg(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    const v1, 0x7f020030    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_sel

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    const v1, 0x7f02002d    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_sel

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method private setOffsetDepthVisibility(I)V
    .locals 3
    .param p1    # I

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUI()V
    .locals 3

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    iget v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "=======Update UI Depth["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]  offset["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setDepthValue(I)V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetValue(I)V

    return-void
.end method


# virtual methods
.method public findViewById()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0016    # com.konka.hotkey.R.id.hk_3d_en_effect_container

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0017    # com.konka.hotkey.R.id.hk_3d_en_effect_content

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0019    # com.konka.hotkey.R.id.hk_3d_en_effect

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0018    # com.konka.hotkey.R.id.hk_3d_en_effect_arrows_pre

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a001a    # com.konka.hotkey.R.id.hk_3d_en_effect_arrows_next

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a001b    # com.konka.hotkey.R.id.hk_3d_en_depth_container

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a001c    # com.konka.hotkey.R.id.hk_3d_en_depth_text_container

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a001d    # com.konka.hotkey.R.id.hk_3d_en_depth_content

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a001e    # com.konka.hotkey.R.id.hk_3d_en_depth

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0020    # com.konka.hotkey.R.id.hk_3d_en_seekbar_depth

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0021    # com.konka.hotkey.R.id.hk_3d_en_offset_container

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0022    # com.konka.hotkey.R.id.hk_3d_en_offset_text_container

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0023    # com.konka.hotkey.R.id.hk_3d_en_offset_content

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0024    # com.konka.hotkey.R.id.hk_3d_en_offset

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0a0026    # com.konka.hotkey.R.id.hk_3d_en_seekbar_offset

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    return-void
.end method

.method public getDepthValue()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    return v0
.end method

.method public getEffectIndex()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    return v0
.end method

.method public getOffsetValue()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    return v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->bFocuse:Z

    return v0
.end method

.method public requestFocus()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    move-result v0

    return v0
.end method

.method public setDepthValue(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    sget-boolean v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->bCheckS3dEngineFirstCreate:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    mul-int/lit8 v1, p1, 0x3

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    :cond_0
    return-void
.end method

.method public setEffectIndex(I)V
    .locals 6
    .param p1    # I

    const/16 v5, 0x18

    const/16 v4, 0x12

    const/16 v3, 0xc

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Normal"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mstar/android/tv/TvS3DManager;->set3DDepthMode(I)Z

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mstar/android/tv/TvS3DManager;->set3DOffsetMode(I)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Strong"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/mstar/android/tv/TvS3DManager;->set3DDepthMode(I)Z

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/mstar/android/tv/TvS3DManager;->set3DOffsetMode(I)Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Weak"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tv/TvS3DManager;->set3DDepthMode(I)Z

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tv/TvS3DManager;->set3DOffsetMode(I)Z

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_User"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DDepthMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DOffsetMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iget v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetValue(I)V

    iget v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setDepthValue(I)V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setOffsetValue(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    sget-boolean v0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->bCheckS3dEngineFirstCreate:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    mul-int/lit8 v1, p1, 0x3

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    :cond_0
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->bFocuse:Z

    return-void
.end method

.method public setStatusFbd()V
    .locals 4

    const v3, 0x7f060003    # com.konka.hotkey.R.color.text_forbidden_col

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method
