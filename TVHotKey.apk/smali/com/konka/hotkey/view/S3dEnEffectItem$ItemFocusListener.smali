.class Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;
.super Ljava/lang/Object;
.source "S3dEnEffectItem.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnEffectItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemFocusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/16 v6, 0x8

    const/4 v2, 0x4

    const v5, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    const v4, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    const/4 v3, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$9(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$0(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$10(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000e    # com.konka.hotkey.R.dimen.TVHotkeyS3dDepthOffsetTextSel_Height

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$1(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$11(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v0, v3}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setSelected(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$12(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$5(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$13(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000e    # com.konka.hotkey.R.dimen.TVHotkeyS3dDepthOffsetTextSel_Height

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$6(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$14(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v0, v3}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setSelected(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$15(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$16(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$17(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$18(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$19(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setSelected(Z)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepth:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$9(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$0(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutDepthText:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$10(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000d    # com.konka.hotkey.R.dimen.TVHotkeyS3dDepthOffsetTextNor_Height

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$1(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepthContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$11(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-virtual {v0, v3}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setSelected(Z)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffset:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$12(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$5(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutOffsetText:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$13(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000d    # com.konka.hotkey.R.dimen.TVHotkeyS3dDepthOffsetTextNor_Height

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$6(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffsetContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$14(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mLayoutEffect:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$15(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsPre:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$16(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mArrowsNext:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$17(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffect:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$18(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewEffectContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$19(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method
