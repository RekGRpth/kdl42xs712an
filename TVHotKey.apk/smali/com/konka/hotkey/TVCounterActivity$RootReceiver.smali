.class Lcom/konka/hotkey/TVCounterActivity$RootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TVCounterActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/TVCounterActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RootReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/TVCounterActivity;


# direct methods
.method private constructor <init>(Lcom/konka/hotkey/TVCounterActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/hotkey/TVCounterActivity;Lcom/konka/hotkey/TVCounterActivity$RootReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;-><init>(Lcom/konka/hotkey/TVCounterActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v7, 0x8

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "com.android.server.tv.TIME_EVENT_DESTROY_COUNT_DOWN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$0(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/TVCounterActivity;->finish()V

    :cond_0
    const-string v2, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_WARN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$0(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    const-string v2, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_UPDATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "LeftTime"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOffTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$0(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->textViewOffTimeSecond:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$1(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    invoke-virtual {v4}, Lcom/konka/hotkey/TVCounterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08003f    # com.konka.hotkey.R.string.str_root_popup_off_after

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez v1, :cond_2

    const-string v2, "TvCounterActivitity"

    const-string v3, "=================== SET TO STANDBY =============="

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v2, "???"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOnTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$2(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->textViewOnTimeSecond:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$3(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const-string v2, "???"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutPopupOnTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$2(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/hotkey/TVCounterActivity$RootReceiver;->this$0:Lcom/konka/hotkey/TVCounterActivity;

    # getter for: Lcom/konka/hotkey/TVCounterActivity;->linearLayoutReadySwitch:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/hotkey/TVCounterActivity;->access$4(Lcom/konka/hotkey/TVCounterActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_4
    return-void
.end method
