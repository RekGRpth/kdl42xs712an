.class public Lcom/konka/hotkey/service/PipService;
.super Landroid/app/Service;
.source "PipService.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static PIPView:Landroid/view/View; = null

.field private static final ServiceTag:Ljava/lang/String; = "PipService"


# instance fields
.field private PIPwm:Landroid/view/WindowManager;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/service/PipService;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iput-object v0, p0, Lcom/konka/hotkey/service/PipService;->PIPwm:Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/konka/hotkey/service/PipService;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-void
.end method

.method private showPip(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    const-string v9, "pipdata"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v9, "x"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v9, "y"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v9, "width"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v9, "height"

    invoke-virtual {v1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    new-instance v5, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v5}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v9, 0x7d6

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    add-int/lit8 v9, v7, -0x2

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v8, v5, Landroid/view/WindowManager$LayoutParams;->y:I

    add-int/lit8 v9, v6, 0x2

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/lit8 v9, v3, 0x2

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v9, 0x33

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/16 v9, 0x18

    iput v9, v5, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v9, p0, Lcom/konka/hotkey/service/PipService;->PIPwm:Landroid/view/WindowManager;

    if-nez v9, :cond_0

    invoke-virtual {p0}, Lcom/konka/hotkey/service/PipService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "window"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    iput-object v9, p0, Lcom/konka/hotkey/service/PipService;->PIPwm:Landroid/view/WindowManager;

    :cond_0
    :try_start_0
    sget-object v9, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;

    if-nez v9, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/service/PipService;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const v9, 0x7f03000c    # com.konka.hotkey.R.layout.pipsurface

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    sput-object v9, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;

    sget-object v9, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;

    const v10, 0x7f0a0032    # com.konka.hotkey.R.id.PIP_Surface

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/hotkey/service/PipService;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v9, p0, Lcom/konka/hotkey/service/PipService;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v9, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v9, p0, Lcom/konka/hotkey/service/PipService;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v10, 0x3

    invoke-interface {v9, v10}, Landroid/view/SurfaceHolder;->setType(I)V

    iget-object v9, p0, Lcom/konka/hotkey/service/PipService;->PIPwm:Landroid/view/WindowManager;

    sget-object v10, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;

    invoke-interface {v9, v10, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    if-nez p1, :cond_0

    const/4 v2, 0x2

    :goto_0
    return v2

    :cond_0
    const-string v2, "PipService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onBind.  arg.getAction():"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/hotkey/service/PipService;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    if-nez v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/hotkey/service/PipService;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    :cond_1
    const-string v2, "cmd"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "showpip"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, Lcom/konka/hotkey/service/PipService;->showPip(Landroid/content/Intent;)V

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v2

    goto :goto_0

    :cond_3
    const-string v2, "removepip"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    iget-object v2, p0, Lcom/konka/hotkey/service/PipService;->PIPwm:Landroid/view/WindowManager;

    sget-object v3, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/4 v2, 0x0

    sput-object v2, Lcom/konka/hotkey/service/PipService;->PIPView:Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PipService"

    const-string v1, "surfaceChanged."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v1, "PipService"

    const-string v2, "surfaceCreated."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v1, p0, Lcom/konka/hotkey/service/PipService;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iget-object v2, p0, Lcom/konka/hotkey/service/PipService;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "PipService"

    const-string v1, "surfaceDestroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
