.class public final Lcom/konka/hotkey/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f080000

.field public static final baofeng_goldeneye_close:I = 0x7f080063

.field public static final baofeng_goldeneye_open:I = 0x7f080064

.field public static final baofeng_notsupport_state:I = 0x7f080065

.field public static final baofeng_title:I = 0x7f080062

.field public static final dc_doublechannel:I = 0x7f080061

.field public static final dc_normalscreen:I = 0x7f080049

.field public static final dc_pip:I = 0x7f080046

.field public static final dc_pop:I = 0x7f080045

.field public static final dc_subchannel:I = 0x7f080047

.field public static final dc_subfullscreen:I = 0x7f080048

.field public static final dc_syncwatch:I = 0x7f08004a

.field public static final notsupport_3d:I = 0x7f080041

.field public static final str_3d_setting:I = 0x7f080060

.field public static final str_factory_index:I = 0x7f080056

.field public static final str_factory_macaddr:I = 0x7f080055

.field public static final str_factory_offset:I = 0x7f080057

.field public static final str_factory_upgradedataover:I = 0x7f08005b

.field public static final str_factory_upgradenofile:I = 0x7f080059

.field public static final str_factory_upgradeok:I = 0x7f080058

.field public static final str_factory_upgradereadfilefail:I = 0x7f08005a

.field public static final str_factory_usbreadhdcpkey:I = 0x7f080054

.field public static final str_factory_usbwritehdcpkey:I = 0x7f080053

.field public static final str_factory_usbwritemacaddr:I = 0x7f080052

.field public static final str_factory_usbwritesn:I = 0x7f080051

.field public static final str_hk_3d_auto_wait:I = 0x7f080032

.field public static final str_hk_3d_en_clarity_intell:I = 0x7f080029

.field public static final str_hk_3d_en_effect:I = 0x7f080025

.field public static final str_hk_3d_en_effect_deepth:I = 0x7f08002a

.field public static final str_hk_3d_en_effect_state_custom:I = 0x7f08002f

.field public static final str_hk_3d_en_effect_state_standard:I = 0x7f08002c

.field public static final str_hk_3d_en_effect_state_strong:I = 0x7f08002d

.field public static final str_hk_3d_en_effect_state_weak:I = 0x7f08002e

.field public static final str_hk_3d_en_effect_viewpoint:I = 0x7f08002b

.field public static final str_hk_3d_en_mot_intell:I = 0x7f080028

.field public static final str_hk_3d_en_pic_qual:I = 0x7f080027

.field public static final str_hk_3d_en_sequence:I = 0x7f080026

.field public static final str_hk_3d_en_sequence_state_negative:I = 0x7f080031

.field public static final str_hk_3d_en_sequence_state_positive:I = 0x7f080030

.field public static final str_hk_3d_menu_2t3:I = 0x7f080021

.field public static final str_hk_3d_menu_3t2:I = 0x7f080020

.field public static final str_hk_3d_menu_auto:I = 0x7f080023

.field public static final str_hk_3d_menu_clar:I = 0x7f080022

.field public static final str_hk_3d_menu_en:I = 0x7f080024

.field public static final str_hk_3d_menu_lr:I = 0x7f08001d

.field public static final str_hk_3d_menu_off:I = 0x7f08001f

.field public static final str_hk_3d_menu_ud:I = 0x7f08001e

.field public static final str_hk_countdown_menu_content:I = 0x7f080004

.field public static final str_hk_order_info_btn_cancel:I = 0x7f080039

.field public static final str_hk_order_info_btn_yes:I = 0x7f080038

.field public static final str_hk_order_info_channel:I = 0x7f080036

.field public static final str_hk_order_info_program:I = 0x7f080037

.field public static final str_hk_order_info_time:I = 0x7f080035

.field public static final str_hk_order_info_tips:I = 0x7f080033

.field public static final str_hk_order_info_wram:I = 0x7f080034

.field public static final str_hk_picture_menu_dynamic:I = 0x7f080005

.field public static final str_hk_picture_menu_normal:I = 0x7f080006

.field public static final str_hk_picture_menu_soft:I = 0x7f080007

.field public static final str_hk_picture_menu_user:I = 0x7f080009

.field public static final str_hk_picture_menu_vivid:I = 0x7f080008

.field public static final str_hk_sleep_menu_str1:I = 0x7f08000f

.field public static final str_hk_sleep_menu_str2:I = 0x7f080010

.field public static final str_hk_sleep_menu_str3:I = 0x7f080011

.field public static final str_hk_sleep_menu_str4:I = 0x7f080012

.field public static final str_hk_sleep_menu_str5:I = 0x7f080013

.field public static final str_hk_sleep_menu_str6:I = 0x7f080014

.field public static final str_hk_sound_menu_movie:I = 0x7f08000c

.field public static final str_hk_sound_menu_music:I = 0x7f08000b

.field public static final str_hk_sound_menu_normal:I = 0x7f08000a

.field public static final str_hk_sound_menu_sport:I = 0x7f08000d

.field public static final str_hk_sound_menu_user:I = 0x7f08000e

.field public static final str_hk_state_demonstrate:I = 0x7f08005e

.field public static final str_hk_state_off:I = 0x7f080002

.field public static final str_hk_state_on:I = 0x7f080001

.field public static final str_hk_zoom_menu_16x9:I = 0x7f080015

.field public static final str_hk_zoom_menu_4x3:I = 0x7f080016

.field public static final str_hk_zoom_menu_Fullview:I = 0x7f08001c

.field public static final str_hk_zoom_menu_Subtitle:I = 0x7f08001a

.field public static final str_hk_zoom_menu_dotbydot:I = 0x7f08001b

.field public static final str_hk_zoom_menu_intell:I = 0x7f080017

.field public static final str_hk_zoom_menu_movie:I = 0x7f080019

.field public static final str_hk_zoom_menu_panorama:I = 0x7f080018

.field public static final str_input_menu_atv:I = 0x7f08004c

.field public static final str_input_menu_av:I = 0x7f08004d

.field public static final str_input_menu_dtv:I = 0x7f08004b

.field public static final str_input_menu_hdmi:I = 0x7f08004e

.field public static final str_input_menu_vga:I = 0x7f080050

.field public static final str_input_menu_ypbpr:I = 0x7f08004f

.field public static final str_root_popup_left:I = 0x7f08003a

.field public static final str_root_popup_off_after:I = 0x7f08003f

.field public static final str_root_popup_ready_off:I = 0x7f08003e

.field public static final str_root_popup_ready_to_switch:I = 0x7f08003d

.field public static final str_root_popup_second:I = 0x7f08003b

.field public static final str_root_popup_switch_to:I = 0x7f08003c

.field public static final str_sys_language:I = 0x7f080003

.field public static final warning_4k2knotsupport:I = 0x7f08005c

.field public static final warning_exitthemenu:I = 0x7f08005d

.field public static final warning_mic_status:I = 0x7f080044

.field public static final warning_notsupport_3d:I = 0x7f080040

.field public static final warning_notsupport_3d_function:I = 0x7f08005f

.field public static final warning_notsupport_3d_pip:I = 0x7f080042

.field public static final warning_notsupport_func:I = 0x7f080043


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
