.class public final Lcom/konka/hotkey/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final PIP_Surface:I = 0x7f0a0032

.field public static final hk_3d_en_depth:I = 0x7f0a001e

.field public static final hk_3d_en_depth_container:I = 0x7f0a001b

.field public static final hk_3d_en_depth_content:I = 0x7f0a001d

.field public static final hk_3d_en_depth_sb_container:I = 0x7f0a001f

.field public static final hk_3d_en_depth_text_container:I = 0x7f0a001c

.field public static final hk_3d_en_effect:I = 0x7f0a0019

.field public static final hk_3d_en_effect_arrows_next:I = 0x7f0a001a

.field public static final hk_3d_en_effect_arrows_pre:I = 0x7f0a0018

.field public static final hk_3d_en_effect_container:I = 0x7f0a0016

.field public static final hk_3d_en_effect_content:I = 0x7f0a0017

.field public static final hk_3d_en_offset:I = 0x7f0a0024

.field public static final hk_3d_en_offset_container:I = 0x7f0a0021

.field public static final hk_3d_en_offset_content:I = 0x7f0a0023

.field public static final hk_3d_en_offset_sb_container:I = 0x7f0a0025

.field public static final hk_3d_en_offset_text_container:I = 0x7f0a0022

.field public static final hk_3d_en_seekbar_depth:I = 0x7f0a0020

.field public static final hk_3d_en_seekbar_offset:I = 0x7f0a0026

.field public static final hk_3d_en_sequence_container:I = 0x7f0a0027

.field public static final hk_3d_menu_auto_container:I = 0x7f0a0014

.field public static final hk_3d_menu_auto_layout:I = 0x7f0a0012

.field public static final hk_3d_menu_container:I = 0x7f0a0029

.field public static final hk_3d_menu_en_head:I = 0x7f0a0013

.field public static final hk_3d_menu_en_layout:I = 0x7f0a0015

.field public static final hk_countdown_menu_ll_before:I = 0x7f0a0000

.field public static final hk_countdown_menu_note:I = 0x7f0a0001

.field public static final hk_countdown_menu_sec:I = 0x7f0a0002

.field public static final hk_order_info_btn_cancel:I = 0x7f0a000e

.field public static final hk_order_info_btn_ok:I = 0x7f0a000d

.field public static final hk_order_info_menu:I = 0x7f0a0003

.field public static final hk_order_info_menu_simple:I = 0x7f0a000f

.field public static final hk_order_info_tips:I = 0x7f0a0004

.field public static final hk_order_info_tv_channel:I = 0x7f0a000a

.field public static final hk_order_info_tv_channel_title:I = 0x7f0a0009

.field public static final hk_order_info_tv_countdown:I = 0x7f0a0005

.field public static final hk_order_info_tv_program:I = 0x7f0a000c

.field public static final hk_order_info_tv_program_title:I = 0x7f0a000b

.field public static final hk_order_info_tv_time:I = 0x7f0a0008

.field public static final hk_order_info_tv_time_title:I = 0x7f0a0007

.field public static final hk_order_info_tv_wram:I = 0x7f0a0006

.field public static final hk_picture_menu_container:I = 0x7f0a0010

.field public static final hk_sound_menu_container:I = 0x7f0a0011

.field public static final hk_sound_menu_seekbar:I = 0x7f0a002b

.field public static final hk_sound_menu_tv_volum:I = 0x7f0a002d

.field public static final hk_sound_menu_volume100:I = 0x7f0a002c

.field public static final hk_zoom_menu_container:I = 0x7f0a002e

.field public static final input_item_container:I = 0x7f0a002f

.field public static final iv_freeze:I = 0x7f0a0030

.field public static final iv_mute:I = 0x7f0a0031

.field public static final linear_layout_popup_offtime:I = 0x7f0a0037

.field public static final linear_layout_popup_ontime:I = 0x7f0a0034

.field public static final linear_layout_popup_ready_off:I = 0x7f0a003a

.field public static final linear_layout_popup_ready_switch:I = 0x7f0a0039

.field public static final linear_layout_root:I = 0x7f0a0033

.field public static final menu_doublechannel:I = 0x7f0a002a

.field public static final pip3d_menu_container:I = 0x7f0a0028

.field public static final text_view_offsecond:I = 0x7f0a0038

.field public static final text_view_second:I = 0x7f0a0035

.field public static final text_view_target:I = 0x7f0a0036


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
