.class Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;
.super Ljava/lang/Thread;
.source "volumeViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/volumeViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateAudioVolumeThread"
.end annotation


# instance fields
.field public starting:Z

.field final synthetic this$0:Lcom/konka/hotkey/volumeViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/hotkey/volumeViewHolder;)V
    .locals 1

    iput-object p1, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->starting:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/hotkey/volumeViewHolder;Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;-><init>(Lcom/konka/hotkey/volumeViewHolder;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->starting:Z

    if-nez v1, :cond_1

    const-string v1, "UpdateAudioVolumeThread"

    const-string v2, "++++++++UpdateAudioVolumeThread end"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    # getter for: Lcom/konka/hotkey/volumeViewHolder;->iPreVolumeValue:I
    invoke-static {v1}, Lcom/konka/hotkey/volumeViewHolder;->access$0(Lcom/konka/hotkey/volumeViewHolder;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    # getter for: Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I
    invoke-static {v2}, Lcom/konka/hotkey/volumeViewHolder;->access$1(Lcom/konka/hotkey/volumeViewHolder;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    # getter for: Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I
    invoke-static {v2}, Lcom/konka/hotkey/volumeViewHolder;->access$1(Lcom/konka/hotkey/volumeViewHolder;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/hotkey/volumeViewHolder;->access$2(Lcom/konka/hotkey/volumeViewHolder;I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    iget-object v3, p0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->this$0:Lcom/konka/hotkey/volumeViewHolder;

    # getter for: Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I
    invoke-static {v3}, Lcom/konka/hotkey/volumeViewHolder;->access$1(Lcom/konka/hotkey/volumeViewHolder;)I

    move-result v3

    int-to-byte v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
