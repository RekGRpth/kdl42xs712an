.class public abstract Lcom/android/magicsmoke/RenderScriptScene;
.super Ljava/lang/Object;
.source "RenderScriptScene.java"


# instance fields
.field protected mHeight:I

.field protected mIsStarted:Z

.field protected mPreview:Z

.field protected mRS:Landroid/renderscript/RenderScriptGL;

.field protected mResources:Landroid/content/res/Resources;

.field protected mScript:Landroid/renderscript/ScriptC;

.field protected mWidth:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/magicsmoke/RenderScriptScene;->mWidth:I

    iput p2, p0, Lcom/android/magicsmoke/RenderScriptScene;->mHeight:I

    return-void
.end method


# virtual methods
.method protected abstract createScript()Landroid/renderscript/ScriptC;
.end method

.method public init(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;Z)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScriptGL;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # Z

    iput-object p1, p0, Lcom/android/magicsmoke/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iput-object p2, p0, Lcom/android/magicsmoke/RenderScriptScene;->mResources:Landroid/content/res/Resources;

    iput-boolean p3, p0, Lcom/android/magicsmoke/RenderScriptScene;->mPreview:Z

    invoke-virtual {p0}, Lcom/android/magicsmoke/RenderScriptScene;->createScript()Landroid/renderscript/ScriptC;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/RenderScriptScene;->mScript:Landroid/renderscript/ScriptC;

    return-void
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public resize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/android/magicsmoke/RenderScriptScene;->mWidth:I

    iput p2, p0, Lcom/android/magicsmoke/RenderScriptScene;->mHeight:I

    return-void
.end method

.method public setOffset(FFFFII)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/magicsmoke/RenderScriptScene;->mScript:Landroid/renderscript/ScriptC;

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/magicsmoke/RenderScriptScene;->mIsStarted:Z

    return-void
.end method

.method public stop(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/android/magicsmoke/RenderScriptScene;->mRS:Landroid/renderscript/RenderScriptGL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/renderscript/RenderScriptGL;->bindRootScript(Landroid/renderscript/Script;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/magicsmoke/RenderScriptScene;->mIsStarted:Z

    return-void
.end method
