.class public Lcom/android/smspush/WapPushManager;
.super Landroid/app/Service;
.source "WapPushManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/smspush/WapPushManager$IWapPushManagerStub;,
        Lcom/android/smspush/WapPushManager$WapPushManDBHelper;
    }
.end annotation


# instance fields
.field private final mBinder:Lcom/android/smspush/WapPushManager$IWapPushManagerStub;

.field private mDbHelper:Lcom/android/smspush/WapPushManager$WapPushManDBHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/android/smspush/WapPushManager$IWapPushManagerStub;

    invoke-direct {v0, p0}, Lcom/android/smspush/WapPushManager$IWapPushManagerStub;-><init>(Lcom/android/smspush/WapPushManager;)V

    iput-object v0, p0, Lcom/android/smspush/WapPushManager;->mBinder:Lcom/android/smspush/WapPushManager$IWapPushManagerStub;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/smspush/WapPushManager;->mDbHelper:Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    iget-object v0, p0, Lcom/android/smspush/WapPushManager;->mBinder:Lcom/android/smspush/WapPushManager$IWapPushManagerStub;

    iput-object p0, v0, Lcom/android/smspush/WapPushManager$IWapPushManagerStub;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected getDatabase(Landroid/content/Context;)Lcom/android/smspush/WapPushManager$WapPushManDBHelper;
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/android/smspush/WapPushManager;->mDbHelper:Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    invoke-direct {v0, p0, p1}, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;-><init>(Lcom/android/smspush/WapPushManager;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/smspush/WapPushManager;->mDbHelper:Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    :cond_0
    iget-object v0, p0, Lcom/android/smspush/WapPushManager;->mDbHelper:Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    return-object v0
.end method

.method public isDataExist(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-virtual {p0, p0}, Lcom/android/smspush/WapPushManager;->getDatabase(Landroid/content/Context;)Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v1, v0, p1, p2}, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;->queryLastApp(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/smspush/WapPushManager;->mBinder:Lcom/android/smspush/WapPushManager$IWapPushManagerStub;

    return-object v0
.end method

.method public verifyData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Z
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Z
    .param p7    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, p0}, Lcom/android/smspush/WapPushManager;->getDatabase(Landroid/content/Context;)Lcom/android/smspush/WapPushManager$WapPushManDBHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v1, v0, p1, p2}, Lcom/android/smspush/WapPushManager$WapPushManDBHelper;->queryLastApp(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, v2, Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;->packageName:Ljava/lang/String;

    invoke-virtual {v5, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v2, Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;->className:Ljava/lang/String;

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget v5, v2, Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;->appType:I

    if-ne v5, p5, :cond_0

    iget v6, v2, Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;->needSignature:I

    if-eqz p6, :cond_2

    move v5, v4

    :goto_1
    if-ne v6, v5, :cond_0

    iget v6, v2, Lcom/android/smspush/WapPushManager$WapPushManDBHelper$queryData;->furtherProcessing:I

    if-eqz p7, :cond_3

    move v5, v4

    :goto_2
    if-ne v6, v5, :cond_0

    move v3, v4

    goto :goto_0

    :cond_2
    move v5, v3

    goto :goto_1

    :cond_3
    move v5, v3

    goto :goto_2
.end method
