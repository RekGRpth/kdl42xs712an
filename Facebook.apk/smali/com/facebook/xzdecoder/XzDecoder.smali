.class public Lcom/facebook/xzdecoder/XzDecoder;
.super Ljava/lang/Object;
.source "XzDecoder.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "LogUse"
    }
.end annotation


# static fields
.field private static a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;


# instance fields
.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->NOT_LOADED:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sput-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    return-void
.end method

.method private static a(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 10

    const/4 v2, 0x0

    const-string v0, "XZ"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Decompressing XZ stream into "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    const-wide/16 v4, 0x0

    :try_start_0
    new-instance v1, Lorg/tukaani/xz/SingleXZInputStream;

    invoke-direct {v1, p0}, Lorg/tukaani/xz/SingleXZInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/16 v0, 0x2000

    :try_start_2
    new-array v0, v0, [B

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v8, -0x1

    if-eq v2, v8, :cond_2

    int-to-long v8, v2

    add-long/2addr v4, v8

    const/4 v8, 0x0

    invoke-virtual {v3, v0, v8, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_1
    if-eqz v1, :cond_0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_1
    :goto_2
    throw v0

    :cond_2
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-string v2, "XZ"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " output bytes written in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long/2addr v0, v6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    :try_start_0
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/xzdecoder/XzDecoder;->decompressFile(JLjava/lang/String;JLjava/lang/String;)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string v4, "XZ"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " output bytes written in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sub-long v1, v2, v6

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/facebook/xzdecoder/XzDecoder;->a()V

    throw v0
.end method

.method private b()Lcom/facebook/xzdecoder/XzDecoder$LibraryState;
    .locals 4

    const-class v1, Lcom/facebook/xzdecoder/XzDecoder;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sget-object v2, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->NOT_LOADED:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_0

    :try_start_1
    const-string v0, "fb_xzdecoder"

    invoke-static {v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/xzdecoder/XzDecoder;->initializeLibrary()V

    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->NATIVE:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sput-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sget-object v1, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->NATIVE:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    invoke-static {}, Lcom/facebook/xzdecoder/XzDecoder;->initializeState()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    :cond_1
    :goto_1
    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "XZ"

    const-string v3, "Unable to initialize native library, using Java fallback."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->JAVA:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sput-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    invoke-static {v0, v1}, Lcom/facebook/xzdecoder/XzDecoder;->reset(J)V

    goto :goto_1
.end method

.method private static native decompressFile(JLjava/lang/String;JLjava/lang/String;)J
.end method

.method private static native end(J)V
.end method

.method private static native getFileCrc32(JLjava/lang/String;JJ)I
.end method

.method private static native initializeLibrary()V
.end method

.method private static native initializeState()J
.end method

.method private static native reset(J)V
.end method

.method private static native syncInternal()V
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    const-wide/16 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/xzdecoder/XzDecoder;->a:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    sget-object v1, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->NATIVE:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J

    invoke-static {v0, v1}, Lcom/facebook/xzdecoder/XzDecoder;->end(J)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/xzdecoder/XzDecoder;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzDecoder;->b()Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    move-result-object v0

    sget-object v1, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->JAVA:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/xzdecoder/XzDecoder;->a(Ljava/io/InputStream;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v2

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/facebook/xzdecoder/XzDecoder;->a(Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "XZ"

    const-string v2, "File not found while opening asset. Trying Java implementation."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/xzdecoder/XzDecoder;->a(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_2
    throw v0
.end method

.method public final a(Ljava/io/File;Ljava/io/File;)V
    .locals 4

    invoke-direct {p0}, Lcom/facebook/xzdecoder/XzDecoder;->b()Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    move-result-object v0

    sget-object v1, Lcom/facebook/xzdecoder/XzDecoder$LibraryState;->JAVA:Lcom/facebook/xzdecoder/XzDecoder$LibraryState;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/xzdecoder/XzDecoder;->a(Ljava/io/InputStream;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/xzdecoder/XzDecoder;->a(Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_0
.end method
