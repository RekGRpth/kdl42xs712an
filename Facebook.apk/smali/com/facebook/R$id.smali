.class public Lcom/facebook/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ButtonLayout:I = 0x7f090a66

.field public static final about_button:I = 0x7f090925

.field public static final about_collection_item_icon:I = 0x7f0902d0

.field public static final about_collection_item_subtitle:I = 0x7f0902d3

.field public static final about_collection_item_title:I = 0x7f0902d2

.field public static final about_collection_text_container:I = 0x7f0902d1

.field public static final above:I = 0x7f090086

.field public static final aboveAnchor:I = 0x7f09006e

.field public static final accept_guardrail_button:I = 0x7f091186

.field public static final accessory_text:I = 0x7f091271

.field public static final account_with_pin_section:I = 0x7f090455

.field public static final accounts_on_device:I = 0x7f09045c

.field public static final accounts_on_device_container:I = 0x7f09045d

.field public static final action:I = 0x7f09086e

.field public static final action_bar:I = 0x7f0900f9

.field public static final action_bar_activity_content:I = 0x7f090051

.field public static final action_bar_container:I = 0x7f0900f8

.field public static final action_bar_overlay_layout:I = 0x7f0900fc

.field public static final action_bar_root:I = 0x7f0900f7

.field public static final action_bar_subtitle:I = 0x7f0900ff

.field public static final action_bar_tab_scroller:I = 0x7f090054

.field public static final action_bar_title:I = 0x7f0900fe

.field public static final action_button:I = 0x7f0902b6

.field public static final action_button_container:I = 0x7f090b6c

.field public static final action_buttons_left_wrapper:I = 0x7f091276

.field public static final action_buttons_wrapper:I = 0x7f091278

.field public static final action_context_bar:I = 0x7f0900fa

.field public static final action_forward:I = 0x7f091380

.field public static final action_help:I = 0x7f09138d

.field public static final action_icon:I = 0x7f090b33

.field public static final action_menu_divider:I = 0x7f090052

.field public static final action_menu_presenter:I = 0x7f090053

.field public static final action_mode_bar:I = 0x7f09010c

.field public static final action_mode_bar_stub:I = 0x7f09010b

.field public static final action_mode_close_button:I = 0x7f090100

.field public static final action_search:I = 0x7f091389

.field public static final action_send:I = 0x7f09138e

.field public static final action_wrapper:I = 0x7f090914

.field public static final activate_feed_button:I = 0x7f090632

.field public static final activate_feed_button_container:I = 0x7f090631

.field public static final activity_chooser_view_content:I = 0x7f090101

.field public static final activity_intent:I = 0x7f0900a2

.field public static final activity_url:I = 0x7f090077

.field public static final activity_view_container:I = 0x7f091232

.field public static final actor_image:I = 0x7f090481

.field public static final actor_name:I = 0x7f090482

.field public static final adapter_tag_item_data:I = 0x7f0900f6

.field public static final add_a_place_image:I = 0x7f09014a

.field public static final add_button:I = 0x7f090b0b

.field public static final add_checkin:I = 0x7f090f38

.field public static final add_comment_bar:I = 0x7f0906ca

.field public static final add_contributor_view:I = 0x7f090494

.field public static final add_cover_feed_shortcut:I = 0x7f091110

.field public static final add_friend_button_text:I = 0x7f0906d3

.field public static final add_friends_friends_table:I = 0x7f0906e3

.field public static final add_friends_main_layout:I = 0x7f0906e1

.field public static final add_members_button:I = 0x7f090b13

.field public static final add_members_contact_picker:I = 0x7f090b12

.field public static final add_members_root_layout:I = 0x7f090b11

.field public static final add_new_address:I = 0x7f090199

.field public static final add_new_address_divider:I = 0x7f09019a

.field public static final add_payment_button:I = 0x7f0911b5

.field public static final add_payment_methods_activity:I = 0x7f090157

.field public static final add_people:I = 0x7f09139e

.field public static final add_photo:I = 0x7f090333

.field public static final add_photo_button:I = 0x7f0912bb

.field public static final add_photos:I = 0x7f090179

.field public static final add_photos_text:I = 0x7f09017a

.field public static final add_pin:I = 0x7f090454

.field public static final add_place_row_view:I = 0x7f09014b

.field public static final add_profile_pic_btn:I = 0x7f091246

.field public static final add_profile_pic_stub:I = 0x7f091245

.field public static final add_recommendation_link_title:I = 0x7f09023b

.field public static final add_reply:I = 0x7f090761

.field public static final add_tag_button:I = 0x7f0911fb

.field public static final add_to_album:I = 0x7f090334

.field public static final add_tracking_codes_to_extras:I = 0x7f0900ba

.field public static final address_city:I = 0x7f090161

.field public static final address_details:I = 0x7f090f11

.field public static final address_entry_form:I = 0x7f09015d

.field public static final address_entry_form_dialog:I = 0x7f090164

.field public static final address_name:I = 0x7f09015e

.field public static final address_privacy_notice:I = 0x7f090197

.field public static final address_selection_container:I = 0x7f090198

.field public static final address_state:I = 0x7f090162

.field public static final address_street:I = 0x7f09015f

.field public static final address_street2:I = 0x7f090160

.field public static final address_zipcode:I = 0x7f090163

.field public static final admin_images:I = 0x7f090b19

.field public static final admin_message_frame:I = 0x7f090b16

.field public static final admin_message_rounded:I = 0x7f090b17

.field public static final admin_panel_publisher:I = 0x7f090e50

.field public static final admin_rounded_img:I = 0x7f090b1a

.field public static final admin_text:I = 0x7f090b15

.field public static final album_art:I = 0x7f090a33

.field public static final album_art_fader:I = 0x7f090a34

.field public static final album_audience:I = 0x7f09018c

.field public static final album_contributor_header:I = 0x7f09048f

.field public static final album_cover:I = 0x7f090ece

.field public static final album_cover_image:I = 0x7f090186

.field public static final album_creator_text_view:I = 0x7f09037b

.field public static final album_creator_text_view_stub:I = 0x7f09037a

.field public static final album_date_and_privacy:I = 0x7f090176

.field public static final album_description:I = 0x7f090178

.field public static final album_description_text:I = 0x7f090169

.field public static final album_description_text_separator:I = 0x7f090168

.field public static final album_header:I = 0x7f09016a

.field public static final album_header_text:I = 0x7f09016b

.field public static final album_list:I = 0x7f090185

.field public static final album_location:I = 0x7f0903be

.field public static final album_location_root_view:I = 0x7f09048c

.field public static final album_name:I = 0x7f09018a

.field public static final album_name_left:I = 0x7f090ecb

.field public static final album_name_right:I = 0x7f090ecd

.field public static final album_options:I = 0x7f090189

.field public static final album_photo_count:I = 0x7f09018b

.field public static final album_privacy_icon:I = 0x7f090177

.field public static final album_selected_mark:I = 0x7f090188

.field public static final album_selector_view:I = 0x7f090393

.field public static final album_selector_view_stub:I = 0x7f090392

.field public static final album_title:I = 0x7f090175

.field public static final album_title_text:I = 0x7f090167

.field public static final album_visibility:I = 0x7f0903c0

.field public static final albums_header:I = 0x7f090191

.field public static final alert_dialog_row:I = 0x7f09019c

.field public static final alignBottom:I = 0x7f090066

.field public static final alignBounds:I = 0x7f0900ed

.field public static final alignHorizontalCenter:I = 0x7f090062

.field public static final alignLeft:I = 0x7f090061

.field public static final alignMargins:I = 0x7f0900ee

.field public static final alignRight:I = 0x7f090063

.field public static final alignTop:I = 0x7f090064

.field public static final alignVerticalCenter:I = 0x7f090065

.field public static final alignWithBottomOfAnchor:I = 0x7f090070

.field public static final alignWithLeftOfAnchor:I = 0x7f090073

.field public static final alignWithRightOfAnchor:I = 0x7f090074

.field public static final alignWithTopOfAnchor:I = 0x7f09006f

.field public static final all_button:I = 0x7f0909ec

.field public static final all_selected_group:I = 0x7f0909eb

.field public static final allow_contributors_indicator:I = 0x7f090492

.field public static final allow_contributors_rootview:I = 0x7f090490

.field public static final allow_contributors_textview:I = 0x7f090491

.field public static final always:I = 0x7f090049

.field public static final alwaysExpandSingleButtonInSplitBar:I = 0x7f090046

.field public static final analytics_tag:I = 0x7f090000

.field public static final anchorable_toast_text:I = 0x7f090c03

.field public static final android_notification_app_icon:I = 0x7f09019e

.field public static final android_notification_timestamp_text:I = 0x7f0901a3

.field public static final android_notification_title:I = 0x7f0901a0

.field public static final android_notification_title2:I = 0x7f0901a1

.field public static final android_notification_title3:I = 0x7f0901a2

.field public static final angora_attachment_bottom_view:I = 0x7f090a28

.field public static final angora_event_attachment_leftsection:I = 0x7f0904d1

.field public static final answer_call_button:I = 0x7f091341

.field public static final app_feed_pager_container:I = 0x7f0901ae

.field public static final app_feed_store_content_container:I = 0x7f0901ac

.field public static final app_feed_store_fragment:I = 0x7f0903f8

.field public static final app_feed_store_loading_spinner:I = 0x7f0901ab

.field public static final app_icon_image:I = 0x7f090ac4

.field public static final app_icons_text_1:I = 0x7f090f78

.field public static final app_icons_text_2:I = 0x7f090f79

.field public static final app_info_group:I = 0x7f091119

.field public static final app_info_group_title:I = 0x7f09111a

.field public static final app_name:I = 0x7f090a37

.field public static final app_rating:I = 0x7f090ac3

.field public static final appinvites_fragment:I = 0x7f0901b8

.field public static final appirater_comment_edittext:I = 0x7f0901ba

.field public static final appirater_comment_text:I = 0x7f0901b9

.field public static final appirater_current_rating_description:I = 0x7f0901be

.field public static final appirater_message_icon:I = 0x7f0901bb

.field public static final appirater_message_text:I = 0x7f0901bc

.field public static final appirater_rating_bar:I = 0x7f0901bf

.field public static final appirater_rating_text:I = 0x7f0901bd

.field public static final apps_popup_webview:I = 0x7f0901c0

.field public static final article_chaining_item:I = 0x7f0901c1

.field public static final article_chaining_item_image:I = 0x7f0901c2

.field public static final article_chaining_item_subtitle:I = 0x7f0901c4

.field public static final article_chaining_item_title:I = 0x7f0901c3

.field public static final artist:I = 0x7f090c4a

.field public static final artist_info:I = 0x7f090a3f

.field public static final artist_name:I = 0x7f090a3d

.field public static final artist_profile:I = 0x7f090a3e

.field public static final artist_title:I = 0x7f090a36

.field public static final association_icon:I = 0x7f090edc

.field public static final at_tags_empty:I = 0x7f091102

.field public static final at_tags_empty_msg:I = 0x7f091103

.field public static final attachment_action_button_container:I = 0x7f0901a7

.field public static final attachment_action_icon_divider:I = 0x7f090646

.field public static final attachment_audio_stub:I = 0x7f090be2

.field public static final attachment_base_container:I = 0x7f0905ac

.field public static final attachment_call_to_action_button_field:I = 0x7f09064f

.field public static final attachment_call_to_action_icon:I = 0x7f0905a1

.field public static final attachment_call_to_action_text_button:I = 0x7f0905a0

.field public static final attachment_context_text:I = 0x7f0905a8

.field public static final attachment_cover_image:I = 0x7f0905aa

.field public static final attachment_cover_image_container:I = 0x7f0905a9

.field public static final attachment_cover_image_play_button:I = 0x7f0905ab

.field public static final attachment_cta_star_rating_stub:I = 0x7f090650

.field public static final attachment_download:I = 0x7f090bfa

.field public static final attachment_event_icon_date:I = 0x7f0904cf

.field public static final attachment_event_icon_plus:I = 0x7f0904d0

.field public static final attachment_icon:I = 0x7f090cb0

.field public static final attachment_image:I = 0x7f09105d

.field public static final attachment_name:I = 0x7f090bfb

.field public static final attachment_operation_container:I = 0x7f090319

.field public static final attachment_preview_cancel:I = 0x7f090f7b

.field public static final attachment_preview_continue:I = 0x7f090f7d

.field public static final attachment_preview_edit:I = 0x7f090f7c

.field public static final attachment_preview_picture:I = 0x7f090f7a

.field public static final attachment_product_info_container:I = 0x7f090652

.field public static final attachment_profile_image:I = 0x7f0905a2

.field public static final attachment_profile_picture_play_button:I = 0x7f0905a3

.field public static final attachment_size:I = 0x7f090bfc

.field public static final attachment_target_star_rating:I = 0x7f0905a7

.field public static final attachment_target_star_rating_stub:I = 0x7f0905a6

.field public static final attachment_text_container:I = 0x7f0905a4

.field public static final attachment_title_text:I = 0x7f0905a5

.field public static final attachment_type_logo:I = 0x7f090b92

.field public static final attachment_type_text:I = 0x7f090b93

.field public static final attachments_section_stub:I = 0x7f090b96

.field public static final audience_call_to_action:I = 0x7f0901c9

.field public static final audience_educator_icon:I = 0x7f0901c8

.field public static final audience_picker_autocomplete_container:I = 0x7f090323

.field public static final audience_picker_autocomplete_input:I = 0x7f090328

.field public static final audience_picker_fixed_privacy:I = 0x7f090325

.field public static final audience_picker_fixed_target:I = 0x7f090327

.field public static final audience_picker_fixed_target_pic:I = 0x7f090326

.field public static final audience_picker_heading:I = 0x7f090324

.field public static final audience_picker_loading_indicator:I = 0x7f09032a

.field public static final audience_picker_members_divider:I = 0x7f09032c

.field public static final audience_picker_right_arrow:I = 0x7f09032b

.field public static final audience_prompt:I = 0x7f0901c7

.field public static final audience_typeahead_frame:I = 0x7f09060d

.field public static final audio_composer:I = 0x7f090b4b

.field public static final audio_composer_content_view:I = 0x7f090b1b

.field public static final audio_composer_record_button:I = 0x7f090b1f

.field public static final audio_composer_tooltip_stub:I = 0x7f090b24

.field public static final audio_composer_topbar_stub:I = 0x7f090b23

.field public static final audio_configurator:I = 0x7f0901d0

.field public static final audio_hint_text:I = 0x7f090b20

.field public static final audio_play_button:I = 0x7f090b28

.field public static final audio_player_bubble_highlighted:I = 0x7f090b27

.field public static final audio_player_bubble_normal:I = 0x7f090b25

.field public static final audio_player_highlighted_wrapper:I = 0x7f090b26

.field public static final audio_record_button:I = 0x7f090b1e

.field public static final audio_timer:I = 0x7f090b2a

.field public static final audio_timer_container:I = 0x7f090b21

.field public static final audio_timer_text:I = 0x7f090b22

.field public static final audio_tooltip_text:I = 0x7f090b2b

.field public static final audio_volume_indicator:I = 0x7f090b1d

.field public static final audio_wave_form:I = 0x7f090b29

.field public static final author:I = 0x7f090edb

.field public static final authorisation_webview:I = 0x7f0901af

.field public static final authorisation_webview_progressbar:I = 0x7f0901b0

.field public static final auto_photos_download_enable_checkbox:I = 0x7f090c1f

.field public static final available_tab:I = 0x7f090c5f

.field public static final awayFromText:I = 0x7f0900db

.field public static final back_album_art:I = 0x7f090a42

.field public static final back_album_art_fader:I = 0x7f090a43

.field public static final back_artist_title:I = 0x7f090a44

.field public static final back_button:I = 0x7f090340

.field public static final back_chevron:I = 0x7f091270

.field public static final back_track_title:I = 0x7f090a45

.field public static final background:I = 0x7f0909c8

.field public static final background_container:I = 0x7f091123

.field public static final background_darken:I = 0x7f090b3b

.field public static final background_image_back:I = 0x7f0903fc

.field public static final background_image_container:I = 0x7f0903fb

.field public static final background_image_front:I = 0x7f0903fd

.field public static final background_location_inline_turn_on:I = 0x7f0901d8

.field public static final background_location_inline_upsell_facepile:I = 0x7f0901d7

.field public static final background_location_inline_upsell_facepile_text:I = 0x7f0901d4

.field public static final background_location_inline_upsell_subtitle:I = 0x7f0901d3

.field public static final background_location_inline_upsell_title:I = 0x7f0901d2

.field public static final background_location_invite_icon:I = 0x7f0901fd

.field public static final background_location_invite_row:I = 0x7f0901fc

.field public static final background_location_nux_activity_fragment_container:I = 0x7f090ae1

.field public static final background_location_nux_intro_chat_head_1:I = 0x7f0901dd

.field public static final background_location_nux_intro_chat_head_1_mask:I = 0x7f0901dc

.field public static final background_location_nux_intro_chat_head_2:I = 0x7f0901e0

.field public static final background_location_nux_intro_chat_head_2_mask:I = 0x7f0901df

.field public static final background_location_nux_intro_message_1:I = 0x7f0901db

.field public static final background_location_nux_intro_message_2:I = 0x7f0901de

.field public static final background_location_nux_intro_notification:I = 0x7f0901d9

.field public static final background_location_nux_intro_phone:I = 0x7f0901da

.field public static final background_location_nux_privacy_card:I = 0x7f0901d1

.field public static final background_location_nux_privacy_facepile:I = 0x7f0901e7

.field public static final background_location_nux_privacy_facepile_text:I = 0x7f0901e6

.field public static final background_location_nux_privacy_learn_more:I = 0x7f0901e5

.field public static final background_location_nux_privacy_progress_row:I = 0x7f0901d5

.field public static final background_location_nux_privacy_spinner:I = 0x7f0901d6

.field public static final background_location_nux_timeline_card:I = 0x7f0901ea

.field public static final background_location_nux_timeline_cover_photo:I = 0x7f0901eb

.field public static final background_location_nux_timeline_profile_photo:I = 0x7f0901ed

.field public static final background_location_nux_timeline_text:I = 0x7f0901ec

.field public static final background_location_privacy_settings_row_icon:I = 0x7f090207

.field public static final background_location_privacy_settings_row_right_image:I = 0x7f090209

.field public static final background_location_privacy_settings_row_text:I = 0x7f090208

.field public static final background_location_report_bug_icon:I = 0x7f090200

.field public static final background_location_report_bug_row:I = 0x7f0901ff

.field public static final background_location_report_bug_separator:I = 0x7f0901fe

.field public static final background_location_settings_content:I = 0x7f0901ee

.field public static final background_location_settings_error:I = 0x7f090201

.field public static final background_location_settings_error_subtitle:I = 0x7f090203

.field public static final background_location_settings_error_title:I = 0x7f090202

.field public static final background_location_settings_history_description:I = 0x7f0901f9

.field public static final background_location_settings_icon:I = 0x7f0901fb

.field public static final background_location_settings_loading:I = 0x7f090204

.field public static final background_location_settings_manage_location:I = 0x7f0901f8

.field public static final background_location_settings_row:I = 0x7f0901fa

.field public static final background_location_settings_sharing_control_icon:I = 0x7f0901ef

.field public static final background_location_settings_sharing_control_switch:I = 0x7f0901f0

.field public static final background_location_settings_sharing_description:I = 0x7f0901f6

.field public static final background_location_settings_sharing_divider:I = 0x7f0901f1

.field public static final background_location_settings_sharing_learn_more:I = 0x7f0901f7

.field public static final background_location_settings_sharing_status_chevron:I = 0x7f0901f5

.field public static final background_location_settings_sharing_status_icon:I = 0x7f0901f3

.field public static final background_location_settings_sharing_status_label:I = 0x7f0901f4

.field public static final background_location_settings_sharing_status_row:I = 0x7f0901f2

.field public static final background_overlay_full:I = 0x7f090400

.field public static final background_overlay_partial:I = 0x7f090401

.field public static final backgroundlocation_settings_sharing_audience_error:I = 0x7f090206

.field public static final backgroundlocation_settings_sharing_audience_list:I = 0x7f090205

.field public static final badge_count:I = 0x7f0902bd

.field public static final badgeable_image_button:I = 0x7f09032e

.field public static final banner_border:I = 0x7f09029f

.field public static final banner_close_button:I = 0x7f090907

.field public static final banner_content:I = 0x7f090909

.field public static final banner_image:I = 0x7f090485

.field public static final banner_notification_placeholder:I = 0x7f090ca8

.field public static final banner_thread_tile:I = 0x7f090a22

.field public static final banner_title:I = 0x7f090908

.field public static final bar_chart_item_bar:I = 0x7f09020b

.field public static final bar_chart_item_label:I = 0x7f09020a

.field public static final base_overlay_layout:I = 0x7f091210

.field public static final basic_share_layout:I = 0x7f0906f5

.field public static final beeper_close_button:I = 0x7f090215

.field public static final beeper_image:I = 0x7f090213

.field public static final beeper_text:I = 0x7f090214

.field public static final beginning:I = 0x7f09004e

.field public static final below:I = 0x7f090087

.field public static final belowAnchor:I = 0x7f09006d

.field public static final betterswitch:I = 0x7f090bc3

.field public static final billing_zip:I = 0x7f09015c

.field public static final birthdate_text:I = 0x7f090196

.field public static final birthday_date_picker:I = 0x7f091078

.field public static final birthday_date_picker_overlay:I = 0x7f091079

.field public static final birthday_icon:I = 0x7f090b62

.field public static final birthday_item_action_icon:I = 0x7f09021b

.field public static final birthday_item_description:I = 0x7f090219

.field public static final birthday_item_image:I = 0x7f090217

.field public static final birthday_item_name:I = 0x7f090218

.field public static final birthday_item_selfie_cam_btn:I = 0x7f09021a

.field public static final black:I = 0x7f090027

.field public static final blackout:I = 0x7f0903fe

.field public static final blue:I = 0x7f090060

.field public static final blue_touch:I = 0x7f090af1

.field public static final blue_touch_shadow:I = 0x7f090af0

.field public static final blurred_image:I = 0x7f090bc9

.field public static final body_text:I = 0x7f090942

.field public static final bold:I = 0x7f090006

.field public static final book_now:I = 0x7f0900cb

.field public static final bookmark_bottom_empty:I = 0x7f0904a1

.field public static final bookmark_creative_container:I = 0x7f090228

.field public static final bookmark_creative_image:I = 0x7f090229

.field public static final bookmark_divider_holder:I = 0x7f090220

.field public static final bookmark_edit_item_button:I = 0x7f0904a0

.field public static final bookmark_edit_item_icon:I = 0x7f09049f

.field public static final bookmark_hide:I = 0x7f090236

.field public static final bookmark_item_body_text:I = 0x7f090226

.field public static final bookmark_item_holder:I = 0x7f090222

.field public static final bookmark_item_icon:I = 0x7f090225

.field public static final bookmark_item_label:I = 0x7f090221

.field public static final bookmark_move_icon:I = 0x7f090230

.field public static final bookmark_star_rating:I = 0x7f09022a

.field public static final bookmark_sub_item:I = 0x7f090227

.field public static final bookmark_top_empty:I = 0x7f09049e

.field public static final bookmark_x:I = 0x7f090235

.field public static final bookmark_x_container:I = 0x7f090234

.field public static final bookmarks_bottom_divider:I = 0x7f090233

.field public static final bookmarks_button:I = 0x7f090920

.field public static final bookmarks_edit_fragment:I = 0x7f090948

.field public static final bookmarks_list:I = 0x7f09022f

.field public static final bookmarks_menu_back_button:I = 0x7f090232

.field public static final bookmarks_menu_back_button_container:I = 0x7f090231

.field public static final bookmarks_menu_done_button:I = 0x7f09022e

.field public static final bookmarks_menu_done_button_container:I = 0x7f09022c

.field public static final bookmarks_refreshable_container:I = 0x7f090ab4

.field public static final bookmarks_sync_progress_bar:I = 0x7f090224

.field public static final bookmarks_tab:I = 0x7f09007d

.field public static final bookmarks_top_divider:I = 0x7f090223

.field public static final boost_button:I = 0x7f090339

.field public static final boost_post_duration_spinner:I = 0x7f0911c3

.field public static final boost_post_duration_title:I = 0x7f0911c2

.field public static final border:I = 0x7f090eba

.field public static final bottom:I = 0x7f09000b

.field public static final bottom_bar:I = 0x7f0911f9

.field public static final bottom_buttons:I = 0x7f0909e8

.field public static final bottom_divider:I = 0x7f090918

.field public static final bottom_half:I = 0x7f090279

.field public static final bottom_row_buttons:I = 0x7f091346

.field public static final bottom_text:I = 0x7f09090e

.field public static final browser_chrome:I = 0x7f090243

.field public static final browser_pivots_link_pic:I = 0x7f09106a

.field public static final browser_pivots_main_text:I = 0x7f09106c

.field public static final browser_pivots_overlay:I = 0x7f090248

.field public static final browser_pivots_sub_text:I = 0x7f09106d

.field public static final browser_pivots_text_holder:I = 0x7f09106b

.field public static final browser_pivots_vert_separator:I = 0x7f09106e

.field public static final btn_back:I = 0x7f091202

.field public static final btn_border:I = 0x7f091007

.field public static final btn_install_app:I = 0x7f0908ff

.field public static final btn_install_new_build:I = 0x7f090902

.field public static final btn_install_new_build_remind:I = 0x7f090903

.field public static final btn_not_now:I = 0x7f0908fe

.field public static final btn_primary:I = 0x7f091002

.field public static final btn_secondary:I = 0x7f091001

.field public static final btn_x_out:I = 0x7f091003

.field public static final btn_yes:I = 0x7f091203

.field public static final bubble_view:I = 0x7f090b3d

.field public static final bubble_view_container:I = 0x7f090b3c

.field public static final budget_slider:I = 0x7f0911b9

.field public static final buffering_spinner:I = 0x7f090a38

.field public static final bug_report_disclaimer:I = 0x7f090257

.field public static final builtin:I = 0x7f090020

.field public static final buttonDivider:I = 0x7f090a67

.field public static final button_add_card:I = 0x7f090155

.field public static final button_bar:I = 0x7f090437

.field public static final button_buy:I = 0x7f090655

.field public static final button_camera_done:I = 0x7f091312

.field public static final button_camera_reject:I = 0x7f091313

.field public static final button_camera_video_play:I = 0x7f0905b5

.field public static final button_container:I = 0x7f09086b

.field public static final button_continue:I = 0x7f090836

.field public static final button_divider:I = 0x7f09090f

.field public static final button_execute_order:I = 0x7f090cee

.field public static final button_gifts_close:I = 0x7f090ced

.field public static final button_give_gift:I = 0x7f090f87

.field public static final button_holder:I = 0x7f09071f

.field public static final button_inline_video_play:I = 0x7f0908ed

.field public static final button_inline_video_play_stub:I = 0x7f0908ec

.field public static final button_list_container:I = 0x7f090332

.field public static final button_next:I = 0x7f090ab7

.field public static final button_pay_now:I = 0x7f090ce1

.field public static final button_post:I = 0x7f0909ee

.field public static final button_remove:I = 0x7f090f88

.field public static final button_review_order:I = 0x7f091135

.field public static final button_rotate:I = 0x7f0909e9

.field public static final button_save_card:I = 0x7f0903dd

.field public static final button_see_all_gifts:I = 0x7f090f8f

.field public static final button_top_highlight:I = 0x7f09022d

.field public static final button_video_item_play:I = 0x7f090f61

.field public static final button_view_profile:I = 0x7f090ce2

.field public static final buttons:I = 0x7f091000

.field public static final buttons_divider:I = 0x7f090add

.field public static final buttons_layout:I = 0x7f090adb

.field public static final buyButton:I = 0x7f0900c7

.field public static final buy_now:I = 0x7f0900ca

.field public static final buy_with_google:I = 0x7f0900c9

.field public static final call_quick_action_item:I = 0x7f090091

.field public static final call_status:I = 0x7f09133d

.field public static final call_status_stub:I = 0x7f09132e

.field public static final call_status_text:I = 0x7f090cb2

.field public static final call_status_text_view:I = 0x7f09132d

.field public static final call_to_action:I = 0x7f09025a

.field public static final call_to_action_button_on_full_screen_player:I = 0x7f090258

.field public static final call_to_action_click_tag:I = 0x7f0900b8

.field public static final call_to_action_title:I = 0x7f09025b

.field public static final camera_button:I = 0x7f090c35

.field public static final camera_button_activity_photo_capture:I = 0x7f09026d

.field public static final camera_button_activity_video_capture:I = 0x7f09026e

.field public static final camera_button_flash:I = 0x7f090267

.field public static final camera_button_group:I = 0x7f091145

.field public static final camera_button_switch_camera:I = 0x7f090268

.field public static final camera_clipper:I = 0x7f090c30

.field public static final camera_container:I = 0x7f090c2a

.field public static final camera_flash:I = 0x7f090c33

.field public static final camera_fragment_container:I = 0x7f090260

.field public static final camera_gallery_image:I = 0x7f09026b

.field public static final camera_launch_indicator:I = 0x7f090277

.field public static final camera_or_grid_button:I = 0x7f0909ea

.field public static final camera_preview:I = 0x7f090265

.field public static final camera_preview_stub:I = 0x7f090c31

.field public static final camera_reveal_animation:I = 0x7f09027c

.field public static final camera_rotateable_gallery_dialog:I = 0x7f090272

.field public static final camera_ui_container:I = 0x7f090261

.field public static final cancel:I = 0x7f0911d7

.field public static final cancelButton:I = 0x7f090a68

.field public static final cancel_button:I = 0x7f0903d1

.field public static final cancel_confirm_button:I = 0x7f090b00

.field public static final cancel_separator:I = 0x7f090afd

.field public static final caption:I = 0x7f090ff4

.field public static final caption_box:I = 0x7f090497

.field public static final caption_text:I = 0x7f090498

.field public static final card_background:I = 0x7f09081e

.field public static final card_background_photo:I = 0x7f090626

.field public static final card_button:I = 0x7f091106

.field public static final card_check_in:I = 0x7f090f12

.field public static final card_entry_point_wrapper:I = 0x7f091374

.field public static final card_fronts_pager:I = 0x7f090288

.field public static final card_info:I = 0x7f090f0b

.field public static final card_info_button:I = 0x7f091107

.field public static final card_info_entry_point_wrapper:I = 0x7f09136a

.field public static final card_map:I = 0x7f090f0f

.field public static final card_number:I = 0x7f090159

.field public static final cards_container_separator:I = 0x7f090998

.field public static final carrier_bottom_banner_close:I = 0x7f090290

.field public static final carrier_bottom_banner_content:I = 0x7f09028f

.field public static final carrier_bottom_banner_title:I = 0x7f09028e

.field public static final carrier_content_container:I = 0x7f090294

.field public static final carrier_header:I = 0x7f090295

.field public static final carrier_logo:I = 0x7f090296

.field public static final carrier_top_content:I = 0x7f090298

.field public static final category_name:I = 0x7f0902af

.field public static final category_remove:I = 0x7f090f2e

.field public static final celebration_description:I = 0x7f0902b5

.field public static final center:I = 0x7f090009

.field public static final centerCrop:I = 0x7f09001e

.field public static final centerHorizontallyOnAnchor:I = 0x7f090076

.field public static final centerInside:I = 0x7f09001f

.field public static final centerVerticallyOnAnchor:I = 0x7f090075

.field public static final center_horizontal:I = 0x7f090010

.field public static final center_point:I = 0x7f0909a6

.field public static final center_vertical:I = 0x7f09000e

.field public static final change_contactpoint_button:I = 0x7f090124

.field public static final change_contactpoint_type_button:I = 0x7f090125

.field public static final change_interval:I = 0x7f09134e

.field public static final change_pin:I = 0x7f090456

.field public static final change_wallpaper:I = 0x7f09110f

.field public static final chat_availability_turn_on_button:I = 0x7f090b89

.field public static final chat_head_text_bubble_container:I = 0x7f0902be

.field public static final chat_head_text_bubble_text:I = 0x7f0902bf

.field public static final chat_heads_container:I = 0x7f090b3e

.field public static final check:I = 0x7f0901e9

.field public static final check_icon:I = 0x7f0909e0

.field public static final checkbox:I = 0x7f090108

.field public static final checkbox_post_to_timeline:I = 0x7f091132

.field public static final checkbox_stub:I = 0x7f090fc6

.field public static final checkin:I = 0x7f090f2f

.field public static final checkin_content_view:I = 0x7f091104

.field public static final checkin_details:I = 0x7f090f14

.field public static final checkmark:I = 0x7f0906d1

.field public static final choose_audience_arrow:I = 0x7f090e1e

.field public static final choose_media_button:I = 0x7f090aae

.field public static final classic:I = 0x7f0900cc

.field public static final clear_all_tags:I = 0x7f0907a3

.field public static final clear_button:I = 0x7f09049a

.field public static final clear_image_cache_button:I = 0x7f090417

.field public static final clear_search_text_button:I = 0x7f0910fd

.field public static final clear_text:I = 0x7f090873

.field public static final clear_view:I = 0x7f091351

.field public static final clickable_container:I = 0x7f090ba6

.field public static final clip_horizontal:I = 0x7f090014

.field public static final clip_vertical:I = 0x7f090013

.field public static final clock_card_settings_button:I = 0x7f0902c3

.field public static final clock_text_view:I = 0x7f0902c2

.field public static final close_bauble:I = 0x7f090b2c

.field public static final close_bauble_base:I = 0x7f090b2d

.field public static final close_bauble_icon:I = 0x7f090b2e

.field public static final close_button:I = 0x7f0901b2

.field public static final close_button_parent:I = 0x7f0903e9

.field public static final close_target:I = 0x7f090b3a

.field public static final code_generator_activate_auto_provision:I = 0x7f0902cc

.field public static final code_generator_activate_manual_provision:I = 0x7f0902cf

.field public static final code_generator_manual_provision_url:I = 0x7f0902cd

.field public static final code_generator_not_working:I = 0x7f0902ca

.field public static final code_generator_secret_key_manual:I = 0x7f0902ce

.field public static final code_generator_timer:I = 0x7f0902cb

.field public static final code_input:I = 0x7f090121

.field public static final code_resent_message:I = 0x7f09011f

.field public static final collapseActionView:I = 0x7f09004b

.field public static final collection_content_container:I = 0x7f0902fb

.field public static final collection_curate_icon:I = 0x7f09030e

.field public static final collection_curate_icon_divider:I = 0x7f09030d

.field public static final collection_item_divider:I = 0x7f0902d4

.field public static final collection_items_container:I = 0x7f090310

.field public static final collection_link_item_container:I = 0x7f0902e6

.field public static final collection_link_item_icon:I = 0x7f0902e7

.field public static final collection_link_item_icon_facepile:I = 0x7f0902e8

.field public static final collection_link_item_navigate_icon:I = 0x7f0902e9

.field public static final collection_link_item_title:I = 0x7f0902ea

.field public static final collection_navigate_icon:I = 0x7f09030c

.field public static final collection_outer_frame:I = 0x7f0902d6

.field public static final collection_rating_section_list_button:I = 0x7f0902fd

.field public static final collection_rating_section_navigate_icon:I = 0x7f090307

.field public static final collection_rating_section_star_icon:I = 0x7f090306

.field public static final collection_rating_section_title:I = 0x7f090308

.field public static final collection_save_button:I = 0x7f090302

.field public static final collection_subtitle_text:I = 0x7f09030b

.field public static final collection_suggestions_header:I = 0x7f0902fa

.field public static final collection_title_bar:I = 0x7f0902d7

.field public static final collection_title_section:I = 0x7f090309

.field public static final collection_title_text:I = 0x7f09030a

.field public static final collections_container:I = 0x7f09030f

.field public static final collections_facepile_1:I = 0x7f0902e2

.field public static final collections_facepile_2:I = 0x7f0902e3

.field public static final collections_facepile_3:I = 0x7f0902e4

.field public static final collections_facepile_4:I = 0x7f0902e5

.field public static final collections_grid_item_image:I = 0x7f0902db

.field public static final collections_grid_item_label:I = 0x7f0902dc

.field public static final collections_photo_image:I = 0x7f0902fc

.field public static final colour_indicator:I = 0x7f090c11

.field public static final colour_picker:I = 0x7f090c12

.field public static final columnWidth:I = 0x7f09003a

.field public static final comment_annotations:I = 0x7f0905e6

.field public static final comment_annotations_separator:I = 0x7f0905e5

.field public static final comment_attachments:I = 0x7f0905e2

.field public static final comment_author:I = 0x7f0905e0

.field public static final comment_body:I = 0x7f0905e1

.field public static final comment_button:I = 0x7f09017e

.field public static final comment_content_without_image:I = 0x7f0905de

.field public static final comment_edit_updating:I = 0x7f0905ee

.field public static final comment_like:I = 0x7f0905e8

.field public static final comment_like_count:I = 0x7f0905ec

.field public static final comment_like_count_container:I = 0x7f0905ea

.field public static final comment_like_count_icon:I = 0x7f0905eb

.field public static final comment_like_count_separator:I = 0x7f0905e9

.field public static final comment_like_separator:I = 0x7f0905e7

.field public static final comment_metadata:I = 0x7f0905e3

.field public static final comment_profile_picture:I = 0x7f0905dd

.field public static final comment_reply_box_stub:I = 0x7f0905df

.field public static final comment_reply_link_stub:I = 0x7f0905ed

.field public static final comment_retry:I = 0x7f0905ef

.field public static final comment_sample_text:I = 0x7f090726

.field public static final comment_sticker_image:I = 0x7f090661

.field public static final comment_text:I = 0x7f090182

.field public static final comment_time:I = 0x7f0905e4

.field public static final commenter_add_photo_button:I = 0x7f0912ab

.field public static final commenter_frame:I = 0x7f0912a5

.field public static final commenter_image_preview_badge:I = 0x7f0912a9

.field public static final commenter_image_preview_frame:I = 0x7f0912a7

.field public static final commenter_image_preview_image:I = 0x7f0912a8

.field public static final commenter_reply_box:I = 0x7f0905dc

.field public static final commenter_send_button:I = 0x7f0905da

.field public static final commit_password:I = 0x7f090463

.field public static final compose_attachment_container:I = 0x7f090bd2

.field public static final compose_attachment_scroll:I = 0x7f090b49

.field public static final compose_attachments:I = 0x7f090b4a

.field public static final compose_button_like:I = 0x7f090317

.field public static final compose_button_location:I = 0x7f090bd8

.field public static final compose_button_send:I = 0x7f090318

.field public static final compose_button_send_text:I = 0x7f090316

.field public static final compose_button_stickers:I = 0x7f090bd7

.field public static final compose_chars_left:I = 0x7f090bd9

.field public static final compose_container:I = 0x7f090b4c

.field public static final compose_edit:I = 0x7f090bd6

.field public static final compose_edit_container:I = 0x7f090bd5

.field public static final compose_emoji_attachments:I = 0x7f090bd3

.field public static final compose_location_disabled_nux:I = 0x7f090b82

.field public static final compose_location_nux:I = 0x7f090b81

.field public static final compose_location_nux_close_button:I = 0x7f090bb4

.field public static final compose_location_nux_instructions:I = 0x7f090bb3

.field public static final compose_location_nux_text:I = 0x7f090bb5

.field public static final compose_location_nux_text1:I = 0x7f090bb2

.field public static final compose_vertical_divider_emoji_attachments:I = 0x7f090bd4

.field public static final compose_vertical_divider_send:I = 0x7f0906cb

.field public static final composer:I = 0x7f09072e

.field public static final composer_actions_container:I = 0x7f090cca

.field public static final composer_attachment_item_cover:I = 0x7f090b50

.field public static final composer_attachment_item_image:I = 0x7f090345

.field public static final composer_attachment_item_image_frame:I = 0x7f090344

.field public static final composer_attachment_item_play_logo:I = 0x7f090b51

.field public static final composer_attachment_item_progress:I = 0x7f090b52

.field public static final composer_attachment_item_remove:I = 0x7f090349

.field public static final composer_attachment_item_tag_indicator:I = 0x7f090348

.field public static final composer_attachment_item_thumbnail:I = 0x7f090b4f

.field public static final composer_attachment_preview_attachment_container:I = 0x7f090321

.field public static final composer_attachment_preview_remove_button:I = 0x7f090322

.field public static final composer_attachment_preview_root:I = 0x7f090320

.field public static final composer_audience_typeahead_frame:I = 0x7f0900d2

.field public static final composer_audience_typeahead_padding:I = 0x7f09038b

.field public static final composer_bar:I = 0x7f09120e

.field public static final composer_edit_dropdown_anchor:I = 0x7f0900d3

.field public static final composer_footer:I = 0x7f0900d1

.field public static final composer_footer_stub:I = 0x7f090391

.field public static final composer_frame:I = 0x7f09031a

.field public static final composer_layout:I = 0x7f09031c

.field public static final composer_page_selector_layout:I = 0x7f09036d

.field public static final composer_popup_content_container:I = 0x7f090b94

.field public static final composer_titlebar:I = 0x7f090331

.field public static final composer_titlebar_stub:I = 0x7f09031d

.field public static final condensed_feedback_action_callout:I = 0x7f0905c3

.field public static final condensed_feedback_comment_count:I = 0x7f0905c2

.field public static final condensed_feedback_like_count:I = 0x7f0905c1

.field public static final confirm_button:I = 0x7f090552

.field public static final confirm_friend_request_button:I = 0x7f09078c

.field public static final confirmation_message_text:I = 0x7f090397

.field public static final confirmation_negative_text:I = 0x7f090398

.field public static final confirmation_positive_text:I = 0x7f090399

.field public static final confirmation_root_view:I = 0x7f090396

.field public static final confirmation_text:I = 0x7f090cec

.field public static final connect_apps:I = 0x7f09110e

.field public static final connect_as_text:I = 0x7f09062c

.field public static final connect_button:I = 0x7f0901b7

.field public static final connect_disable:I = 0x7f09042e

.field public static final connect_disable_checkbox:I = 0x7f09042f

.field public static final connect_enable:I = 0x7f09042c

.field public static final connect_enable_checkbox:I = 0x7f09042d

.field public static final connect_without_sso_text:I = 0x7f090630

.field public static final connection_banner:I = 0x7f091337

.field public static final connection_spinner:I = 0x7f091338

.field public static final connection_text:I = 0x7f091339

.field public static final connectivity_banner:I = 0x7f090471

.field public static final connectivity_banner_text:I = 0x7f09039a

.field public static final contact_collection_item_subtitle:I = 0x7f0902da

.field public static final contact_collection_item_title:I = 0x7f0902d9

.field public static final contact_container:I = 0x7f090b67

.field public static final contact_display_name:I = 0x7f09133b

.field public static final contact_divider:I = 0x7f090b5f

.field public static final contact_group_presence_indicator:I = 0x7f090b5c

.field public static final contact_group_tile_image:I = 0x7f090b0c

.field public static final contact_info:I = 0x7f090b60

.field public static final contact_name:I = 0x7f090b10

.field public static final contact_photo:I = 0x7f091336

.field public static final contact_picker_autocomplete_container:I = 0x7f090329

.field public static final contact_picker_autocomplete_input:I = 0x7f091284

.field public static final contact_picker_expanded_scroll_section:I = 0x7f090b57

.field public static final contact_picker_expanded_section:I = 0x7f090b58

.field public static final contact_picker_heading:I = 0x7f091283

.field public static final contact_picker_members_divider:I = 0x7f091285

.field public static final contact_picker_search_magnifier:I = 0x7f090b6a

.field public static final contact_picker_search_progress:I = 0x7f090b70

.field public static final contact_picker_search_section:I = 0x7f090b69

.field public static final contact_picker_section_header:I = 0x7f090b07

.field public static final contact_picker_section_header_action_button:I = 0x7f090b09

.field public static final contact_picker_section_header_text:I = 0x7f090b08

.field public static final contact_picker_warning:I = 0x7f090b59

.field public static final contact_presence_indicator:I = 0x7f090b63

.field public static final contact_row_extras_holder:I = 0x7f090b5b

.field public static final contact_status:I = 0x7f090b61

.field public static final contact_user_tile_image:I = 0x7f090b0f

.field public static final contact_voip_tile_image:I = 0x7f09133a

.field public static final contactpoint_input:I = 0x7f09014c

.field public static final container:I = 0x7f090254

.field public static final container_layout:I = 0x7f0909aa

.field public static final content:I = 0x7f090ffd

.field public static final content_1:I = 0x7f09112a

.field public static final content_2:I = 0x7f091129

.field public static final content_container:I = 0x7f09019f

.field public static final context_items_view_position:I = 0x7f0900e3

.field public static final context_items_view_tag:I = 0x7f0900e2

.field public static final context_layout:I = 0x7f090a3b

.field public static final context_menu_placeholder:I = 0x7f090f23

.field public static final context_sentence:I = 0x7f090483

.field public static final contextual_dialog_tag_people:I = 0x7f09033c

.field public static final contextual_dialog_tag_place_left:I = 0x7f09033e

.field public static final contextual_dialog_tag_place_right:I = 0x7f09033d

.field public static final contributor_digest_textview:I = 0x7f090496

.field public static final contributor_first:I = 0x7f09016e

.field public static final contributor_image:I = 0x7f090174

.field public static final contributor_name:I = 0x7f0903b9

.field public static final contributor_overlay:I = 0x7f090173

.field public static final contributor_profile_pic:I = 0x7f0903b8

.field public static final contributor_root_view:I = 0x7f090493

.field public static final contributor_second:I = 0x7f09016f

.field public static final contributor_third:I = 0x7f090170

.field public static final contributors_extra_count:I = 0x7f090172

.field public static final contributors_list:I = 0x7f09016c

.field public static final contributors_section:I = 0x7f09016d

.field public static final control_bar:I = 0x7f090a39

.field public static final control_view:I = 0x7f091353

.field public static final controlwidget_checkin_button:I = 0x7f091356

.field public static final controlwidget_composer_button:I = 0x7f091354

.field public static final controlwidget_photo_button:I = 0x7f091355

.field public static final cookies:I = 0x7f0903f6

.field public static final copyrights:I = 0x7f090c6c

.field public static final count:I = 0x7f0903ba

.field public static final count_badge_bg:I = 0x7f0909ef

.field public static final count_container:I = 0x7f090ac1

.field public static final counter_text:I = 0x7f090216

.field public static final country_code_text:I = 0x7f0903bb

.field public static final country_spinner:I = 0x7f09014d

.field public static final cover_feed_notification_container:I = 0x7f0902c7

.field public static final cover_feed_notification_text_view:I = 0x7f0902c8

.field public static final cover_photo:I = 0x7f0902b1

.field public static final coverfeed_dialog:I = 0x7f090aed

.field public static final coverfeed_dialog_continue_button:I = 0x7f090aef

.field public static final coverfeed_dialog_text:I = 0x7f090aee

.field public static final create_dummy_focus_elt:I = 0x7f090ca7

.field public static final create_shortcut:I = 0x7f0913a6

.field public static final create_thread_compose:I = 0x7f090b80

.field public static final create_thread_layout:I = 0x7f090b7e

.field public static final create_thread_overlay_container:I = 0x7f090b7d

.field public static final creative_pyml_horizontal_divider:I = 0x7f0903c9

.field public static final creative_pyml_item:I = 0x7f0903ca

.field public static final creative_pyml_unit_container:I = 0x7f0903c6

.field public static final creative_pyml_unit_title:I = 0x7f0903c7

.field public static final cropDoneButton:I = 0x7f0903d4

.field public static final crop_image:I = 0x7f090553

.field public static final crop_image_layout:I = 0x7f0903d5

.field public static final crop_overlay:I = 0x7f0903d0

.field public static final current_filter_container:I = 0x7f090768

.field public static final current_filter_container_divider:I = 0x7f090767

.field public static final current_filter_image:I = 0x7f090769

.field public static final current_filter_remove_button:I = 0x7f09076a

.field public static final current_filter_value:I = 0x7f09076b

.field public static final customInsideUp:I = 0x7f090044

.field public static final custom_keyboard_layout:I = 0x7f0903de

.field public static final custom_menu_table:I = 0x7f090a11

.field public static final custom_title_wrapper:I = 0x7f09127e

.field public static final cw_0:I = 0x7f09002c

.field public static final cw_180:I = 0x7f09002e

.field public static final cw_270:I = 0x7f09002f

.field public static final cw_90:I = 0x7f09002d

.field public static final dark:I = 0x7f090032

.field public static final dash_auth_fragment:I = 0x7f0903e3

.field public static final dash_edge:I = 0x7f090449

.field public static final dash_empty_story_view_text:I = 0x7f0903ec

.field public static final dash_fb4a_login_button:I = 0x7f0903f3

.field public static final dash_fb4a_login_logo:I = 0x7f0903ee

.field public static final dash_fragment:I = 0x7f0903e2

.field public static final dash_fragment_container:I = 0x7f0903f9

.field public static final dash_fragment_pager_frame:I = 0x7f090405

.field public static final dash_gating_button:I = 0x7f090410

.field public static final dash_gating_footer:I = 0x7f090412

.field public static final dash_gating_logo:I = 0x7f09040d

.field public static final dash_gating_root:I = 0x7f09040c

.field public static final dash_gating_text:I = 0x7f09040f

.field public static final dash_gating_text_container:I = 0x7f09040e

.field public static final dash_gating_use_home_button:I = 0x7f090411

.field public static final dash_loading_debug_listview:I = 0x7f090419

.field public static final dash_loading_spinner:I = 0x7f0903ff

.field public static final dash_loading_stream_layout:I = 0x7f090414

.field public static final dash_login_disclosure_text1:I = 0x7f0903f0

.field public static final dash_login_disclosure_text2:I = 0x7f0903f1

.field public static final dash_login_disclosure_text3:I = 0x7f0903f2

.field public static final dash_nag_ignore_button:I = 0x7f090421

.field public static final dash_nag_upgrade_button:I = 0x7f090420

.field public static final dash_nux_loading_spinner:I = 0x7f090ae3

.field public static final dash_ood_descrip_first:I = 0x7f090423

.field public static final dash_ood_descrip_second:I = 0x7f090424

.field public static final dash_ood_no_data_icon:I = 0x7f090422

.field public static final dash_ood_view_setting_button:I = 0x7f090425

.field public static final dash_prefs_header_icon:I = 0x7f090434

.field public static final dash_root:I = 0x7f0903e1

.field public static final dash_silent_login_loading_view:I = 0x7f09041d

.field public static final dash_story_debug_view_image:I = 0x7f09043e

.field public static final dash_story_debug_view_text:I = 0x7f09043f

.field public static final dash_story_text_container:I = 0x7f09043d

.field public static final dash_time_view:I = 0x7f0903e4

.field public static final data_budget_expiration_close_message:I = 0x7f09044e

.field public static final data_use_policy:I = 0x7f0903f5

.field public static final data_use_setting:I = 0x7f091118

.field public static final date_text_view:I = 0x7f0902c4

.field public static final dbl_accounts_on_device:I = 0x7f09045a

.field public static final dbl_main_group:I = 0x7f090136

.field public static final dbl_off:I = 0x7f090137

.field public static final dbl_on:I = 0x7f090138

.field public static final debug_info_stub:I = 0x7f09134b

.field public static final decline_call_button:I = 0x7f091340

.field public static final deep_link_stub:I = 0x7f0903aa

.field public static final default_activity_button:I = 0x7f090104

.field public static final delete:I = 0x7f09137f

.field public static final delete_button:I = 0x7f090416

.field public static final delete_picture:I = 0x7f09138c

.field public static final desc:I = 0x7f090bbc

.field public static final description:I = 0x7f090c4c

.field public static final description_text:I = 0x7f0903bc

.field public static final detail_text_view:I = 0x7f0910d8

.field public static final details:I = 0x7f090a1e

.field public static final dialog:I = 0x7f09004c

.field public static final dialog_button:I = 0x7f09137d

.field public static final dialog_button_bar:I = 0x7f090474

.field public static final dialog_button_index:I = 0x7f090088

.field public static final dialog_cancel_button:I = 0x7f0903ea

.field public static final dialog_icon:I = 0x7f0903e7

.field public static final dialog_item:I = 0x7f09047a

.field public static final dialog_item_checkbox:I = 0x7f09047e

.field public static final dialog_item_contents:I = 0x7f09047b

.field public static final dialog_item_list:I = 0x7f0903eb

.field public static final dialog_item_separator:I = 0x7f09047d

.field public static final dialog_item_text:I = 0x7f09047c

.field public static final dialog_negative_button:I = 0x7f090475

.field public static final dialog_neutral_button:I = 0x7f090476

.field public static final dialog_positive_button:I = 0x7f090477

.field public static final dialog_text:I = 0x7f090550

.field public static final dialog_title:I = 0x7f0903e8

.field public static final dialog_title_container:I = 0x7f0903e6

.field public static final dialog_view:I = 0x7f0903e5

.field public static final digital_good_inline_video_root:I = 0x7f09047f

.field public static final digital_good_inline_video_view:I = 0x7f090480

.field public static final disableHome:I = 0x7f090043

.field public static final disable_reminder_checkbox:I = 0x7f090551

.field public static final disabled_overlay:I = 0x7f091291

.field public static final discard:I = 0x7f090b84

.field public static final disclaimer:I = 0x7f0911d8

.field public static final disclosure_arrow:I = 0x7f090eb9

.field public static final disclosure_text_container:I = 0x7f0903ef

.field public static final disconnect:I = 0x7f090430

.field public static final disconnect_disable_checkbox:I = 0x7f090431

.field public static final dismiss_button:I = 0x7f0903bd

.field public static final display_name:I = 0x7f090158

.field public static final disturbing_video_message:I = 0x7f0908eb

.field public static final divebar_availability_warning:I = 0x7f090b72

.field public static final divebar_container:I = 0x7f090b85

.field public static final divebar_header_view_container:I = 0x7f090b71

.field public static final divebar_nearby_friends_back_button:I = 0x7f090b73

.field public static final divebar_nearby_friends_row:I = 0x7f090b8a

.field public static final divebar_nearby_friends_row_arrow:I = 0x7f090b8c

.field public static final divebar_nearby_friends_row_divider:I = 0x7f090b8f

.field public static final divebar_nearby_friends_row_icon:I = 0x7f090b8b

.field public static final divebar_nearby_friends_row_subtitle:I = 0x7f090b8e

.field public static final divebar_nearby_friends_row_title:I = 0x7f090b8d

.field public static final divider:I = 0x7f090342

.field public static final divider_discount_block:I = 0x7f090cf5

.field public static final divider_fig:I = 0x7f09128a

.field public static final divider_text:I = 0x7f091101

.field public static final does_have_physical_address:I = 0x7f091391

.field public static final done_button:I = 0x7f0903d2

.field public static final doodle_bottom_tab_bar:I = 0x7f090c13

.field public static final doodle_buttons_layout:I = 0x7f090c0d

.field public static final doodle_cancel_button:I = 0x7f090c15

.field public static final doodle_caption_button:I = 0x7f090c0f

.field public static final doodle_caption_text_view:I = 0x7f090c0c

.field public static final doodle_draw_button:I = 0x7f090c10

.field public static final doodle_drawing_view:I = 0x7f090c0b

.field public static final doodle_photo_frame_layout:I = 0x7f090c0a

.field public static final doodle_send_button:I = 0x7f090c16

.field public static final doodle_top_tab_bar:I = 0x7f090c0e

.field public static final doodle_undo_button:I = 0x7f090c14

.field public static final dot_1:I = 0x7f090cd0

.field public static final dot_2:I = 0x7f090cd1

.field public static final dot_3:I = 0x7f090cd2

.field public static final down:I = 0x7f090056

.field public static final download_button:I = 0x7f090c4e

.field public static final drag_down_container:I = 0x7f090409

.field public static final drag_frame:I = 0x7f090b48

.field public static final drawers_draggable_content_root:I = 0x7f0900df

.field public static final drawers_left_root:I = 0x7f0900dd

.field public static final drawers_right_root:I = 0x7f0900de

.field public static final drawers_root:I = 0x7f0900dc

.field public static final dropdown:I = 0x7f09004d

.field public static final dummy_focus_elt:I = 0x7f09041a

.field public static final dummy_focus_view:I = 0x7f090766

.field public static final dummy_full_screen_view:I = 0x7f09090a

.field public static final dummy_non_harrison_titlebar_container:I = 0x7f0909c2

.field public static final dup_content_view:I = 0x7f0903c5

.field public static final duplicates_fragment:I = 0x7f0909c5

.field public static final edit_button:I = 0x7f09128f

.field public static final edit_caption_box:I = 0x7f0912dd

.field public static final edit_caption_view:I = 0x7f0903ab

.field public static final edit_cover_photo_button:I = 0x7f09049c

.field public static final edit_cover_photo_button_container:I = 0x7f09049b

.field public static final edit_cover_photo_button_stub:I = 0x7f090f59

.field public static final edit_favorites_cancel_button:I = 0x7f090b75

.field public static final edit_favorites_cancel_button_neue:I = 0x7f090b78

.field public static final edit_favorites_done_button:I = 0x7f090b74

.field public static final edit_favorites_done_button_neue:I = 0x7f090b79

.field public static final edit_history_list:I = 0x7f09068c

.field public static final edit_history_progress:I = 0x7f09068b

.field public static final edit_photo:I = 0x7f091378

.field public static final edit_photo_attachments:I = 0x7f090382

.field public static final edit_photo_icon:I = 0x7f091230

.field public static final edit_photo_icon_stub:I = 0x7f09122f

.field public static final edit_query:I = 0x7f090113

.field public static final edit_text:I = 0x7f090cc4

.field public static final edit_text_card_billing_zip:I = 0x7f090154

.field public static final edit_text_card_exp_month:I = 0x7f090151

.field public static final edit_text_card_exp_year:I = 0x7f090152

.field public static final edit_text_card_number:I = 0x7f090150

.field public static final edit_text_card_security_code:I = 0x7f090153

.field public static final edit_text_message:I = 0x7f0903da

.field public static final edit_text_recipient_name:I = 0x7f0903d9

.field public static final edit_text_search_query:I = 0x7f09092a

.field public static final edit_text_sender_name:I = 0x7f0903dc

.field public static final edittext_search_input:I = 0x7f091065

.field public static final education_collapsed_tip_icon:I = 0x7f0904a9

.field public static final education_collapsed_title:I = 0x7f0904aa

.field public static final education_expanded_action_link:I = 0x7f0904a6

.field public static final education_expanded_collapse_button:I = 0x7f0904a7

.field public static final education_expanded_text:I = 0x7f0904a5

.field public static final education_expanded_tip_icon:I = 0x7f0904a3

.field public static final education_expanded_title:I = 0x7f0904a4

.field public static final ego_item:I = 0x7f0904ab

.field public static final ego_item_action_icon:I = 0x7f0904be

.field public static final ego_item_actor:I = 0x7f0904af

.field public static final ego_item_bottom:I = 0x7f0904b3

.field public static final ego_item_bottom_text:I = 0x7f0904b4

.field public static final ego_item_bottom_titles:I = 0x7f0904b5

.field public static final ego_item_extra:I = 0x7f0904bd

.field public static final ego_item_header:I = 0x7f0904ae

.field public static final ego_item_image:I = 0x7f0904ba

.field public static final ego_item_inner:I = 0x7f0904b2

.field public static final ego_item_social_header_divider:I = 0x7f0904ad

.field public static final ego_item_social_header_title:I = 0x7f0904ac

.field public static final ego_item_sponsored_text:I = 0x7f0904b1

.field public static final ego_item_subtitle:I = 0x7f0904bc

.field public static final ego_item_title:I = 0x7f0904b0

.field public static final ego_item_vertical_divider:I = 0x7f0904bb

.field public static final ego_like_button:I = 0x7f0904b8

.field public static final ego_like_sentence:I = 0x7f0904b6

.field public static final ego_swipe_unit_end_view_stub:I = 0x7f0904b9

.field public static final email:I = 0x7f09042a

.field public static final embeddable_map_frame:I = 0x7f0904bf

.field public static final emoji_back_container:I = 0x7f090b9d

.field public static final emoji_back_stub:I = 0x7f090b9c

.field public static final emoji_backspace:I = 0x7f090b9b

.field public static final emoji_button_inline:I = 0x7f090cc5

.field public static final emoji_category_container:I = 0x7f090c79

.field public static final emoji_custom_keyboard_container:I = 0x7f090092

.field public static final emoji_front_container:I = 0x7f090b95

.field public static final emoji_front_content:I = 0x7f090b97

.field public static final emoji_front_content_first_row:I = 0x7f090b98

.field public static final emoji_front_content_second_row:I = 0x7f090b99

.field public static final emoji_show_back:I = 0x7f090b9a

.field public static final empty:I = 0x7f090b86

.field public static final empty_item:I = 0x7f091200

.field public static final empty_item_progress:I = 0x7f090b9e

.field public static final empty_item_text:I = 0x7f090b9f

.field public static final empty_item_view:I = 0x7f09036e

.field public static final empty_layout:I = 0x7f0904c2

.field public static final empty_layout_progress_bar:I = 0x7f0904eb

.field public static final empty_list_view:I = 0x7f090192

.field public static final empty_state_image:I = 0x7f090869

.field public static final empty_state_text:I = 0x7f09086a

.field public static final empty_story_view:I = 0x7f09040b

.field public static final empty_view:I = 0x7f090289

.field public static final enable_home_wallpaper:I = 0x7f09089e

.field public static final enable_home_wallpaper_bottom_divider:I = 0x7f09089f

.field public static final enable_launcher_toggle:I = 0x7f0909a4

.field public static final enable_lockscreen_toggle:I = 0x7f0909a0

.field public static final enable_notifications:I = 0x7f09139a

.field public static final end:I = 0x7f090016

.field public static final end_call_button:I = 0x7f091344

.field public static final entity_card_container:I = 0x7f0904c7

.field public static final entity_cards_background:I = 0x7f0904c9

.field public static final entity_cards_dismissible_container:I = 0x7f0904cb

.field public static final entity_cards_title:I = 0x7f0904ca

.field public static final entity_cards_view_pager:I = 0x7f0904cc

.field public static final error_alert_stub:I = 0x7f090f22

.field public static final error_announcement_megaphone:I = 0x7f09096c

.field public static final error_banner:I = 0x7f090297

.field public static final error_card_retry_button:I = 0x7f0904c8

.field public static final error_container:I = 0x7f09096b

.field public static final error_image:I = 0x7f090968

.field public static final error_message:I = 0x7f090c2b

.field public static final error_stub:I = 0x7f090bf4

.field public static final error_text:I = 0x7f09041f

.field public static final error_text_view:I = 0x7f090969

.field public static final error_toast_dock:I = 0x7f090c87

.field public static final error_view:I = 0x7f090359

.field public static final error_view_stub:I = 0x7f091231

.field public static final estimated_reach:I = 0x7f0911bf

.field public static final event_admin_post_only:I = 0x7f0904e7

.field public static final event_card_action_button:I = 0x7f0904dc

.field public static final event_card_action_button_divider:I = 0x7f0904d8

.field public static final event_card_bottom_view:I = 0x7f0904d6

.field public static final event_card_cover_photo:I = 0x7f0904d4

.field public static final event_card_location_etc:I = 0x7f0904da

.field public static final event_card_social_context:I = 0x7f0904db

.field public static final event_card_time_etc:I = 0x7f0904d9

.field public static final event_card_title:I = 0x7f0904d5

.field public static final event_cover_photo_stub:I = 0x7f0904dd

.field public static final event_cover_photo_view:I = 0x7f09050d

.field public static final event_cover_pic:I = 0x7f0904de

.field public static final event_dashboard_row_divider:I = 0x7f09053d

.field public static final event_dashboard_row_inline_rsvp_view:I = 0x7f090545

.field public static final event_dashboard_row_main_content:I = 0x7f09053e

.field public static final event_dashboard_row_rsvp_status_view:I = 0x7f090544

.field public static final event_dashboard_row_social_context_text_view:I = 0x7f090543

.field public static final event_date:I = 0x7f0904d3

.field public static final event_date_day_text_view:I = 0x7f09052b

.field public static final event_date_month_text_view:I = 0x7f09052a

.field public static final event_description:I = 0x7f0904e0

.field public static final event_feeds_container:I = 0x7f0904e8

.field public static final event_guestlist:I = 0x7f0904fb

.field public static final event_guestlist_empty_text_view:I = 0x7f0904fc

.field public static final event_guestlist_fragment_container:I = 0x7f0904f3

.field public static final event_guestlist_going_count:I = 0x7f0904f8

.field public static final event_guestlist_invited_count:I = 0x7f0904fa

.field public static final event_guestlist_loading_progress_bar:I = 0x7f0904fd

.field public static final event_guestlist_maybe_count:I = 0x7f0904f9

.field public static final event_guestlist_type_tabbed_view_pager_indicator:I = 0x7f0904fe

.field public static final event_guestlist_view_pager:I = 0x7f0904ff

.field public static final event_host_image:I = 0x7f0908b3

.field public static final event_host_name:I = 0x7f0908b4

.field public static final event_hosts_fragment_container:I = 0x7f090500

.field public static final event_invite:I = 0x7f0904e3

.field public static final event_location:I = 0x7f0904e1

.field public static final event_location_text_view:I = 0x7f090542

.field public static final event_month:I = 0x7f0904d2

.field public static final event_name:I = 0x7f0904df

.field public static final event_permalink_action_bar:I = 0x7f090503

.field public static final event_permalink_container:I = 0x7f09050b

.field public static final event_permalink_details_view:I = 0x7f09050a

.field public static final event_permalink_details_view_about:I = 0x7f090507

.field public static final event_permalink_details_view_lineup:I = 0x7f090509

.field public static final event_permalink_details_view_text:I = 0x7f090508

.field public static final event_permalink_disable_action_bar_view:I = 0x7f090504

.field public static final event_permalink_disabling_action_bar_text:I = 0x7f090505

.field public static final event_permalink_fragment_container:I = 0x7f090502

.field public static final event_permalink_header:I = 0x7f090506

.field public static final event_permalink_pinned_post_story_view:I = 0x7f090510

.field public static final event_permalink_pinned_post_view:I = 0x7f09050f

.field public static final event_permalink_posting_story:I = 0x7f090511

.field public static final event_permalink_posting_story_loading_text:I = 0x7f090512

.field public static final event_permalink_summary_view:I = 0x7f090528

.field public static final event_permalink_summary_view_address_icon:I = 0x7f090516

.field public static final event_permalink_summary_view_address_subtitle:I = 0x7f090519

.field public static final event_permalink_summary_view_address_title:I = 0x7f090518

.field public static final event_permalink_summary_view_address_wrapper:I = 0x7f090517

.field public static final event_permalink_summary_view_invited_by_icon:I = 0x7f090513

.field public static final event_permalink_summary_view_invited_by_row:I = 0x7f090527

.field public static final event_permalink_summary_view_invited_by_title:I = 0x7f090514

.field public static final event_permalink_summary_view_location_row:I = 0x7f090525

.field public static final event_permalink_summary_view_ticket_action_icon:I = 0x7f09051f

.field public static final event_permalink_summary_view_ticket_action_label:I = 0x7f09051c

.field public static final event_permalink_summary_view_ticket_icon:I = 0x7f09051a

.field public static final event_permalink_summary_view_ticket_row:I = 0x7f090526

.field public static final event_permalink_summary_view_ticket_subtitle:I = 0x7f09051e

.field public static final event_permalink_summary_view_ticket_title:I = 0x7f09051d

.field public static final event_permalink_summary_view_ticket_wrapper:I = 0x7f09051b

.field public static final event_permalink_summary_view_time_icon:I = 0x7f090520

.field public static final event_permalink_summary_view_time_row:I = 0x7f090524

.field public static final event_permalink_summary_view_time_subtitle:I = 0x7f090523

.field public static final event_permalink_summary_view_time_title:I = 0x7f090522

.field public static final event_permalink_summary_view_time_wrapper:I = 0x7f090521

.field public static final event_profile_photo:I = 0x7f090529

.field public static final event_profile_picture_view:I = 0x7f09053f

.field public static final event_start_and_end_time_picker:I = 0x7f0904e2

.field public static final event_subtitle:I = 0x7f09050e

.field public static final event_time_frame:I = 0x7f09082f

.field public static final event_time_text_view:I = 0x7f090541

.field public static final event_title_text_view:I = 0x7f090540

.field public static final events_birthdays_linear_layout:I = 0x7f09052d

.field public static final events_birthdays_loading_progress_bar:I = 0x7f09052e

.field public static final events_dashboard_birthday_compose_button:I = 0x7f090531

.field public static final events_dashboard_birthday_selfiecam_button:I = 0x7f090530

.field public static final events_dashboard_card_title_text_view:I = 0x7f09052c

.field public static final events_dashboard_card_view_all_text_view:I = 0x7f09052f

.field public static final events_dashboard_container:I = 0x7f090534

.field public static final events_dashboard_create_button:I = 0x7f090537

.field public static final events_dashboard_empty_list_image_view:I = 0x7f090538

.field public static final events_dashboard_empty_list_text_view:I = 0x7f090539

.field public static final events_dashboard_filter_picker:I = 0x7f090536

.field public static final events_dashboard_filter_picker_text_view:I = 0x7f090532

.field public static final events_dashboard_list_view:I = 0x7f090535

.field public static final events_dashboard_row_bottom_divider:I = 0x7f090546

.field public static final events_dashboard_row_inline_going_button:I = 0x7f09053a

.field public static final events_dashboard_row_inline_maybe_button:I = 0x7f09053b

.field public static final events_dashboard_row_inline_not_going_button:I = 0x7f09053c

.field public static final events_dashboard_separator_bottom_divider:I = 0x7f090548

.field public static final events_dashboard_separator_top_divider:I = 0x7f090547

.field public static final events_dashboard_sticky_section_header_text:I = 0x7f090549

.field public static final events_dashboard_view_all_bottom_divider:I = 0x7f09054b

.field public static final events_dashboard_view_all_row_text:I = 0x7f09054a

.field public static final events_feed_list:I = 0x7f0904ea

.field public static final events_feed_list_container:I = 0x7f0904e9

.field public static final events_launcher_megaphone:I = 0x7f090533

.field public static final events_suggestions_loading_progress_bar:I = 0x7f09054c

.field public static final events_suggestions_view_pager:I = 0x7f09054d

.field public static final expand_activities_button:I = 0x7f090102

.field public static final expandable_photo:I = 0x7f09081d

.field public static final expanded_menu:I = 0x7f090107

.field public static final experiment_group:I = 0x7f090ff9

.field public static final experiment_name:I = 0x7f090ff6

.field public static final expiration_date:I = 0x7f09015a

.field public static final expire:I = 0x7f090432

.field public static final expired_checkbox:I = 0x7f090433

.field public static final expired_text:I = 0x7f09062e

.field public static final explicit_location_share_menu_item:I = 0x7f0913a9

.field public static final extra_action_frame:I = 0x7f090187

.field public static final face_image:I = 0x7f090a32

.field public static final face_image_frame:I = 0x7f090a31

.field public static final facebook_tab:I = 0x7f090093

.field public static final faceboxes_view:I = 0x7f0912d8

.field public static final facepile:I = 0x7f090fff

.field public static final facepile_photos:I = 0x7f091026

.field public static final facepile_view:I = 0x7f09020c

.field public static final faces_gallery:I = 0x7f0911ed

.field public static final faceweb_error_retry_text:I = 0x7f09055a

.field public static final faceweb_error_view:I = 0x7f090559

.field public static final faceweb_placeholder:I = 0x7f09072f

.field public static final faceweb_progress:I = 0x7f09055b

.field public static final fail_button:I = 0x7f090afb

.field public static final failed_button:I = 0x7f090afe

.field public static final failure_text:I = 0x7f0910d3

.field public static final fat_title_bar_button:I = 0x7f090560

.field public static final fat_title_bar_image:I = 0x7f09055d

.field public static final fat_title_bar_layout:I = 0x7f09055c

.field public static final fat_title_bar_subtitle:I = 0x7f09055f

.field public static final fat_title_bar_title:I = 0x7f09055e

.field public static final fb_logo_badge_count:I = 0x7f09127b

.field public static final fb_logo_up_button:I = 0x7f09127a

.field public static final fb_message_container:I = 0x7f090a17

.field public static final fb_profile:I = 0x7f09009f

.field public static final fb_profile_extras:I = 0x7f0900a0

.field public static final fbui_content_view:I = 0x7f09057b

.field public static final fbui_content_view_image_button:I = 0x7f09057c

.field public static final fbui_content_view_meta:I = 0x7f09057a

.field public static final fbui_content_view_subtitle:I = 0x7f090579

.field public static final fbui_content_view_thumbnail:I = 0x7f0900bc

.field public static final fbui_content_view_title:I = 0x7f090578

.field public static final fbui_dialog_buttonbar:I = 0x7f090585

.field public static final fbui_dialog_choice_item_checked_text_view:I = 0x7f090580

.field public static final fbui_dialog_content:I = 0x7f090584

.field public static final fbui_dialog_icon:I = 0x7f090582

.field public static final fbui_dialog_message:I = 0x7f090586

.field public static final fbui_dialog_negative_button:I = 0x7f09057d

.field public static final fbui_dialog_neutral_button:I = 0x7f09057e

.field public static final fbui_dialog_positive_button:I = 0x7f09057f

.field public static final fbui_dialog_title:I = 0x7f090583

.field public static final fbui_dialog_title_container:I = 0x7f090581

.field public static final fbui_megaphone_close:I = 0x7f09058c

.field public static final fbui_megaphone_image_block:I = 0x7f090587

.field public static final fbui_megaphone_primary_button:I = 0x7f09058b

.field public static final fbui_megaphone_secondary_button:I = 0x7f09058a

.field public static final fbui_megaphone_subtitle:I = 0x7f090589

.field public static final fbui_megaphone_title:I = 0x7f090588

.field public static final fbui_popover_list_item_checkbox:I = 0x7f090595

.field public static final fbui_popover_list_item_description:I = 0x7f090594

.field public static final fbui_popover_list_item_icon:I = 0x7f090592

.field public static final fbui_popover_list_item_title:I = 0x7f090593

.field public static final fbui_popover_view_flipper:I = 0x7f090596

.field public static final fbui_progressdialog_message:I = 0x7f090598

.field public static final fbui_progressdialog_progressbar:I = 0x7f090597

.field public static final fbui_section_header_view_divider:I = 0x7f090599

.field public static final fbui_section_header_view_title:I = 0x7f09059a

.field public static final fbui_tooltip_description:I = 0x7f09059c

.field public static final fbui_tooltip_nub_above:I = 0x7f09059e

.field public static final fbui_tooltip_nub_below:I = 0x7f09059d

.field public static final fbui_tooltip_title:I = 0x7f09059b

.field public static final featured_tab:I = 0x7f090c5e

.field public static final feed_angora_attachment_style_cta_button:I = 0x7f09059f

.field public static final feed_app_ad_banner:I = 0x7f0905b4

.field public static final feed_app_ad_description:I = 0x7f0905b2

.field public static final feed_app_ad_header_spacer:I = 0x7f0905ae

.field public static final feed_app_ad_install_button:I = 0x7f0905b9

.field public static final feed_app_ad_item_name:I = 0x7f0905b6

.field public static final feed_app_ad_item_name_header:I = 0x7f0905b0

.field public static final feed_app_ad_rating:I = 0x7f0905b7

.field public static final feed_app_ad_social_context:I = 0x7f0905b8

.field public static final feed_app_ad_sponsored_context:I = 0x7f0905b1

.field public static final feed_app_ad_unit_item_container:I = 0x7f0905bb

.field public static final feed_app_ad_unit_item_header_container:I = 0x7f0905bc

.field public static final feed_app_collection_confirmation_text:I = 0x7f0905bd

.field public static final feed_app_collection_privacy_scope:I = 0x7f0905bf

.field public static final feed_app_collection_separator:I = 0x7f0905be

.field public static final feed_attached_story:I = 0x7f09066f

.field public static final feed_attached_story_view_stub:I = 0x7f09066e

.field public static final feed_attachment_event_icon:I = 0x7f0904ce

.field public static final feed_bling_bar_dummy_expand_view:I = 0x7f0906c8

.field public static final feed_bling_bar_spacing_view:I = 0x7f0906b4

.field public static final feed_comment_edit_attachments:I = 0x7f0905c0

.field public static final feed_discover_unit_title:I = 0x7f090488

.field public static final feed_end_container:I = 0x7f0905c4

.field public static final feed_error_icon:I = 0x7f0905c6

.field public static final feed_error_retry:I = 0x7f0905c8

.field public static final feed_error_retry_icon:I = 0x7f0905c9

.field public static final feed_error_retry_text:I = 0x7f0905ca

.field public static final feed_error_text:I = 0x7f0905c7

.field public static final feed_error_view:I = 0x7f090293

.field public static final feed_error_view_contents:I = 0x7f0905c5

.field public static final feed_error_view_stub:I = 0x7f090292

.field public static final feed_event:I = 0x7f09009e

.field public static final feed_expandable_video_container:I = 0x7f09009d

.field public static final feed_feedback_bling_bar:I = 0x7f0906c6

.field public static final feed_feedback_bling_bar_container:I = 0x7f09021c

.field public static final feed_feedback_bottom_divider:I = 0x7f090470

.field public static final feed_feedback_comment_container:I = 0x7f09046e

.field public static final feed_feedback_commenters_text:I = 0x7f09021e

.field public static final feed_feedback_container:I = 0x7f09046c

.field public static final feed_feedback_divider:I = 0x7f090237

.field public static final feed_feedback_like_container:I = 0x7f09046d

.field public static final feed_feedback_likers_text:I = 0x7f09021d

.field public static final feed_feedback_play_count_text:I = 0x7f0906c7

.field public static final feed_feedback_share_container:I = 0x7f09046f

.field public static final feed_feedback_share_divider:I = 0x7f0904b7

.field public static final feed_feedback_spacing:I = 0x7f090683

.field public static final feed_feedback_top_divider:I = 0x7f09046b

.field public static final feed_flyout_background:I = 0x7f090728

.field public static final feed_flyout_bubble:I = 0x7f090727

.field public static final feed_flyout_comment_attachments:I = 0x7f090749

.field public static final feed_flyout_comment_attachments_container:I = 0x7f090748

.field public static final feed_flyout_comment_box:I = 0x7f0912aa

.field public static final feed_flyout_comment_box_divider:I = 0x7f0912a6

.field public static final feed_flyout_comments_group:I = 0x7f09129f

.field public static final feed_flyout_comments_list:I = 0x7f0912a4

.field public static final feed_flyout_drag_frame:I = 0x7f09072b

.field public static final feed_flyout_drag_handle:I = 0x7f09072c

.field public static final feed_flyout_edit_history_arrow:I = 0x7f090712

.field public static final feed_flyout_edit_history_header_text:I = 0x7f090713

.field public static final feed_flyout_edit_history_separator:I = 0x7f090714

.field public static final feed_flyout_edit_history_switch_view_holder:I = 0x7f090711

.field public static final feed_flyout_no_comments_placeholder:I = 0x7f0912a0

.field public static final feed_flyout_no_comments_placeholder_image_view:I = 0x7f0912a2

.field public static final feed_flyout_no_comments_text_view:I = 0x7f0912a3

.field public static final feed_flyout_nub:I = 0x7f090729

.field public static final feed_gallery_item_image:I = 0x7f090668

.field public static final feed_hidden_story_afro_question:I = 0x7f0905ce

.field public static final feed_hidden_story_container:I = 0x7f0905cb

.field public static final feed_hidden_story_divider:I = 0x7f0905cd

.field public static final feed_hidden_story_dummy_expand_view:I = 0x7f0905cf

.field public static final feed_hidden_story_optional_followup_text:I = 0x7f0905cc

.field public static final feed_hide_story_menu_item:I = 0x7f09009c

.field public static final feed_loading_more:I = 0x7f0905d0

.field public static final feed_page_story_underlying_admin_creator_view:I = 0x7f090689

.field public static final feed_permalink_more_comments_container:I = 0x7f0905f7

.field public static final feed_permalink_more_comments_icon:I = 0x7f0905f8

.field public static final feed_permalink_more_comments_spacer:I = 0x7f0905f6

.field public static final feed_permalink_more_comments_spinner:I = 0x7f0905f9

.field public static final feed_permalink_more_comments_text:I = 0x7f0905fa

.field public static final feed_permalink_report_menu_item:I = 0x7f09009b

.field public static final feed_premium_video_feedback_bar:I = 0x7f090f62

.field public static final feed_recyclable_pager_tag:I = 0x7f0900b7

.field public static final feed_settings_content:I = 0x7f090614

.field public static final feed_settings_empty_view:I = 0x7f090622

.field public static final feed_settings_follow_button:I = 0x7f09061a

.field public static final feed_settings_item:I = 0x7f090615

.field public static final feed_settings_item_text_container:I = 0x7f090619

.field public static final feed_settings_list_view:I = 0x7f090621

.field public static final feed_settings_pic:I = 0x7f090616

.field public static final feed_settings_profile_pic:I = 0x7f090618

.field public static final feed_settings_refreshable_container:I = 0x7f090620

.field public static final feed_settings_row_subtitle:I = 0x7f09061c

.field public static final feed_settings_row_title:I = 0x7f09061b

.field public static final feed_settings_section_header_name:I = 0x7f09061e

.field public static final feed_settings_section_title_bottom_border:I = 0x7f09061d

.field public static final feed_story:I = 0x7f0900ab

.field public static final feed_story_attached_story_divider:I = 0x7f090669

.field public static final feed_story_attachment:I = 0x7f0900af

.field public static final feed_story_attachment_rating_bar:I = 0x7f090638

.field public static final feed_story_attachment_style_cta:I = 0x7f09064e

.field public static final feed_story_attachments:I = 0x7f09066c

.field public static final feed_story_default_feedback_bar:I = 0x7f090685

.field public static final feed_story_explanation:I = 0x7f0905ad

.field public static final feed_story_explanation_divider:I = 0x7f09067c

.field public static final feed_story_explanation_menu_button:I = 0x7f090676

.field public static final feed_story_explanation_text:I = 0x7f090675

.field public static final feed_story_fallback_warning_text:I = 0x7f09067d

.field public static final feed_story_feedback_divider:I = 0x7f0905ba

.field public static final feed_story_footer:I = 0x7f090673

.field public static final feed_story_freshness_overlay:I = 0x7f091369

.field public static final feed_story_header:I = 0x7f09066a

.field public static final feed_story_header_friending_button:I = 0x7f0907ad

.field public static final feed_story_header_like_button:I = 0x7f090958

.field public static final feed_story_image_attachment:I = 0x7f09065f

.field public static final feed_story_image_attachment_container:I = 0x7f090656

.field public static final feed_story_image_attachment_progress_overlay:I = 0x7f090660

.field public static final feed_story_image_attachments:I = 0x7f09063e

.field public static final feed_story_info:I = 0x7f09068a

.field public static final feed_story_insights_bar:I = 0x7f090684

.field public static final feed_story_large_image_attachment:I = 0x7f090635

.field public static final feed_story_location:I = 0x7f09066d

.field public static final feed_story_location_action_divider:I = 0x7f09069d

.field public static final feed_story_location_more_text:I = 0x7f090694

.field public static final feed_story_location_place_category:I = 0x7f09069a

.field public static final feed_story_location_place_details:I = 0x7f090696

.field public static final feed_story_location_place_name:I = 0x7f090697

.field public static final feed_story_location_place_rating_bar:I = 0x7f090699

.field public static final feed_story_location_place_rating_bar_content_desc:I = 0x7f090698

.field public static final feed_story_location_place_save_icon:I = 0x7f09069e

.field public static final feed_story_location_place_save_view:I = 0x7f09069c

.field public static final feed_story_location_place_thumbnail:I = 0x7f09069f

.field public static final feed_story_location_place_visits:I = 0x7f09069b

.field public static final feed_story_location_profile_image:I = 0x7f0906a0

.field public static final feed_story_location_profile_pic:I = 0x7f090695

.field public static final feed_story_map:I = 0x7f0906a2

.field public static final feed_story_map_dot:I = 0x7f090693

.field public static final feed_story_map_image:I = 0x7f090691

.field public static final feed_story_map_image_border:I = 0x7f090692

.field public static final feed_story_map_label_container:I = 0x7f0906a3

.field public static final feed_story_menu_button:I = 0x7f0903c8

.field public static final feed_story_message:I = 0x7f09066b

.field public static final feed_story_page_feedback_container:I = 0x7f0906ac

.field public static final feed_story_page_like_button:I = 0x7f090687

.field public static final feed_story_page_like_icon:I = 0x7f0906ad

.field public static final feed_story_page_like_text:I = 0x7f0906ae

.field public static final feed_story_place_info:I = 0x7f0906a1

.field public static final feed_story_place_info_stub:I = 0x7f0906a4

.field public static final feed_story_postpost_badge_icon:I = 0x7f090657

.field public static final feed_story_postpost_badge_text:I = 0x7f090658

.field public static final feed_story_postpost_badge_wrapper:I = 0x7f0906af

.field public static final feed_story_primary_actor:I = 0x7f090686

.field public static final feed_story_subattachment_container:I = 0x7f0906b3

.field public static final feed_story_subattachment_divider:I = 0x7f0906b0

.field public static final feed_story_title:I = 0x7f090688

.field public static final feed_story_topic_pivot:I = 0x7f090677

.field public static final feed_story_topic_pivot_header_icon:I = 0x7f090678

.field public static final feed_story_topic_pivot_right_chevron:I = 0x7f09067b

.field public static final feed_story_topic_pivot_sub_text:I = 0x7f090679

.field public static final feed_story_topic_pivot_text:I = 0x7f09067a

.field public static final feed_story_total_reach:I = 0x7f09068f

.field public static final feed_story_view_insights:I = 0x7f090690

.field public static final feed_storyset_item_attachments:I = 0x7f0906ba

.field public static final feed_storyset_item_card:I = 0x7f0906b5

.field public static final feed_storyset_item_footer:I = 0x7f0906bc

.field public static final feed_storyset_item_footer_horizontal_divider:I = 0x7f0906bb

.field public static final feed_storyset_item_header:I = 0x7f0906b8

.field public static final feed_storyset_item_message:I = 0x7f0906b9

.field public static final feed_storyset_item_social_context:I = 0x7f0906b6

.field public static final feed_storyset_item_social_context_divider:I = 0x7f0906b7

.field public static final feed_substories_section:I = 0x7f090670

.field public static final feed_substory_attachments:I = 0x7f0906bf

.field public static final feed_substory_gallery:I = 0x7f090672

.field public static final feed_substory_gallery_attachment_bling_bar_divider:I = 0x7f0906c1

.field public static final feed_substory_gallery_attachment_bling_bar_placeholder_view:I = 0x7f0906c0

.field public static final feed_substory_gallery_attachment_feedback_bar:I = 0x7f0906c2

.field public static final feed_substory_gallery_attachment_view:I = 0x7f0906be

.field public static final feed_substory_gallery_stub:I = 0x7f090671

.field public static final feed_survey_question_id:I = 0x7f0900b0

.field public static final feed_survey_response_text:I = 0x7f0900b1

.field public static final feed_top_padding_banner_spacer:I = 0x7f0906c4

.field public static final feed_top_padding_first_story_spacer:I = 0x7f0906c5

.field public static final feed_top_padding_ptr_spacer:I = 0x7f0906c3

.field public static final feed_ufi_bling_bar_extra_text:I = 0x7f0906c9

.field public static final feed_unit_cache_id:I = 0x7f0900aa

.field public static final feed_unit_menu_story:I = 0x7f0900ac

.field public static final feed_unit_menu_story_view:I = 0x7f0900ad

.field public static final feedback:I = 0x7f0903a3

.field public static final feedback_buttons_layout:I = 0x7f0903a4

.field public static final feedback_logging_params:I = 0x7f0900a6

.field public static final feedback_tap_point:I = 0x7f0900a7

.field public static final feedback_text:I = 0x7f090180

.field public static final fill:I = 0x7f090012

.field public static final fill_horizontal:I = 0x7f090011

.field public static final fill_vertical:I = 0x7f09000f

.field public static final film_strip_clips:I = 0x7f09131a

.field public static final film_strip_left_handle:I = 0x7f09131e

.field public static final film_strip_mask:I = 0x7f09131b

.field public static final film_strip_right_handle:I = 0x7f09131f

.field public static final film_strip_scrubber_handle:I = 0x7f09131d

.field public static final film_strip_selected_bar:I = 0x7f09131c

.field public static final filter_fragment:I = 0x7f090139

.field public static final filter_text:I = 0x7f09085e

.field public static final filter_value:I = 0x7f09085d

.field public static final filter_value_holder:I = 0x7f09085b

.field public static final filter_value_list:I = 0x7f09076c

.field public static final filters_collection_container:I = 0x7f09013a

.field public static final find_friend_bar:I = 0x7f0906d7

.field public static final find_friend_button:I = 0x7f0906d8

.field public static final find_friend_text_view:I = 0x7f0906d9

.field public static final find_friends_add_friends_header:I = 0x7f0906e4

.field public static final find_friends_bottom_button:I = 0x7f0906e6

.field public static final find_friends_button:I = 0x7f09067f

.field public static final find_friends_candidate_avatar:I = 0x7f0906cd

.field public static final find_friends_friend_candidate_list:I = 0x7f0906e5

.field public static final find_friends_how_it_works_button:I = 0x7f0906ec

.field public static final find_friends_how_it_works_button_layout:I = 0x7f0906e9

.field public static final find_friends_how_it_works_text:I = 0x7f0906eb

.field public static final find_friends_how_it_works_title:I = 0x7f0906ea

.field public static final find_friends_how_it_works_view:I = 0x7f0906e8

.field public static final find_friends_intro_button:I = 0x7f0906db

.field public static final find_friends_intro_footer:I = 0x7f0906ed

.field public static final find_friends_intro_footer_text:I = 0x7f0906ee

.field public static final find_friends_invite_all_button:I = 0x7f0906f1

.field public static final find_friends_progress:I = 0x7f0906e7

.field public static final find_friends_tab:I = 0x7f09007f

.field public static final find_pages_button:I = 0x7f090682

.field public static final first_child:I = 0x7f090447

.field public static final first_photo_pivot:I = 0x7f091166

.field public static final fitCenter:I = 0x7f09001c

.field public static final fitEnd:I = 0x7f09001d

.field public static final fitStart:I = 0x7f09001b

.field public static final fitXY:I = 0x7f09001a

.field public static final fitness_card_image:I = 0x7f09094b

.field public static final fitness_card_map:I = 0x7f09094c

.field public static final fitness_map:I = 0x7f0906f4

.field public static final fitness_pager:I = 0x7f09094a

.field public static final fixed:I = 0x7f0900e4

.field public static final flickr_button:I = 0x7f0901b6

.field public static final flip_camera:I = 0x7f090c2e

.field public static final flipper:I = 0x7f0910cb

.field public static final floating_image:I = 0x7f0908d6

.field public static final flyout_aggregated_profile_list_view:I = 0x7f090166

.field public static final flyout_attachment_url_link:I = 0x7f090097

.field public static final flyout_background:I = 0x7f090732

.field public static final flyout_background_mask:I = 0x7f090733

.field public static final flyout_bubble:I = 0x7f090165

.field public static final flyout_cached_bitmap_view:I = 0x7f09072d

.field public static final flyout_click_source:I = 0x7f09009a

.field public static final flyout_comment_attachment_fallback:I = 0x7f0906fd

.field public static final flyout_comment_attachments_container:I = 0x7f0906fc

.field public static final flyout_content_container:I = 0x7f09072a

.field public static final flyout_edit_comment_arrow:I = 0x7f09070d

.field public static final flyout_edit_comment_header_text:I = 0x7f09070e

.field public static final flyout_edit_comment_separator:I = 0x7f09070f

.field public static final flyout_edit_comment_switch_view_holder:I = 0x7f09070c

.field public static final flyout_edit_comment_view:I = 0x7f090710

.field public static final flyout_feedback:I = 0x7f0900a3

.field public static final flyout_should_show_keyboard_on_first_load:I = 0x7f0900a4

.field public static final flyout_should_show_likers:I = 0x7f0900a5

.field public static final focus_indicator:I = 0x7f090724

.field public static final focus_indicator_rotate_layout:I = 0x7f090723

.field public static final footer_1:I = 0x7f091127

.field public static final footer_2:I = 0x7f091128

.field public static final footer_bar:I = 0x7f09038a

.field public static final footer_bottom:I = 0x7f090ed6

.field public static final footer_container:I = 0x7f090f5c

.field public static final footer_section:I = 0x7f0912a1

.field public static final footer_text:I = 0x7f090238

.field public static final footer_top:I = 0x7f090ed1

.field public static final footer_view:I = 0x7f090357

.field public static final forceEmbeddedTabs:I = 0x7f090045

.field public static final force_crash_menu_item:I = 0x7f09008e

.field public static final form_header:I = 0x7f090193

.field public static final forward:I = 0x7f09137e

.field public static final forward_button:I = 0x7f09091b

.field public static final forward_stub:I = 0x7f090be3

.field public static final fps_counter:I = 0x7f0900e8

.field public static final fqc_bottom_bar:I = 0x7f090604

.field public static final fqc_bottom_container:I = 0x7f090606

.field public static final fqc_bottom_message:I = 0x7f090607

.field public static final fqc_camera_clipper:I = 0x7f09060e

.field public static final fqc_camera_freeze_frame:I = 0x7f090610

.field public static final fqc_camera_play_button:I = 0x7f090612

.field public static final fqc_camera_preview_stub:I = 0x7f09060f

.field public static final fqc_inline_video_player:I = 0x7f090611

.field public static final fqc_pause_button:I = 0x7f09060c

.field public static final fqc_play_button:I = 0x7f09060b

.field public static final fqc_processing_button:I = 0x7f09060a

.field public static final fqc_progress_bar:I = 0x7f090605

.field public static final fqc_record_button:I = 0x7f090608

.field public static final fqc_recording_button:I = 0x7f090609

.field public static final fqc_video_resizing_progress:I = 0x7f090613

.field public static final fragmentContainer:I = 0x7f090725

.field public static final fragment_container:I = 0x7f090078

.field public static final fragment_nux_step_profile_info_item_clear_icon:I = 0x7f090751

.field public static final fragment_nux_step_profile_info_questions_section:I = 0x7f09074d

.field public static final fragment_nux_step_profile_info_save_button:I = 0x7f09074e

.field public static final fragment_nux_step_profile_picture_choose_button:I = 0x7f090757

.field public static final fragment_nux_step_profile_picture_silhouette:I = 0x7f090755

.field public static final fragment_nux_step_profile_picture_take_button:I = 0x7f090756

.field public static final frame:I = 0x7f090b01

.field public static final frame_card_fronts:I = 0x7f090287

.field public static final frame_content:I = 0x7f09024e

.field public static final frame_continue_button:I = 0x7f090848

.field public static final frame_description:I = 0x7f090f8a

.field public static final frame_error:I = 0x7f090847

.field public static final frame_extra_details:I = 0x7f090f89

.field public static final frame_full_screen_video:I = 0x7f090244

.field public static final frame_gift_card_options:I = 0x7f09083f

.field public static final frame_loading:I = 0x7f091066

.field public static final frame_recipient_header:I = 0x7f09084c

.field public static final frame_root:I = 0x7f090846

.field public static final free_services_image:I = 0x7f090913

.field public static final free_services_install_button:I = 0x7f090916

.field public static final free_services_list:I = 0x7f090910

.field public static final free_services_row_description_text:I = 0x7f090915

.field public static final free_services_row_text:I = 0x7f090911

.field public static final friction_label:I = 0x7f09117a

.field public static final friction_seekbar:I = 0x7f091179

.field public static final friend_candidate_checked:I = 0x7f0906d0

.field public static final friend_candidate_friending_button:I = 0x7f0906d4

.field public static final friend_candidate_name:I = 0x7f0906d2

.field public static final friend_candidate_native_name:I = 0x7f0906d5

.field public static final friend_candidate_unchecked:I = 0x7f0906ce

.field public static final friend_finder_container:I = 0x7f090774

.field public static final friend_finder_first_credential:I = 0x7f090780

.field public static final friend_finder_get_started_button:I = 0x7f09077b

.field public static final friend_finder_intro_view:I = 0x7f09077a

.field public static final friend_finder_invite:I = 0x7f09077e

.field public static final friend_finder_invite_image:I = 0x7f09077d

.field public static final friend_finder_invite_progress_bar:I = 0x7f090783

.field public static final friend_finder_invite_progress_bar_container:I = 0x7f090782

.field public static final friend_finder_invite_search_bar:I = 0x7f090784

.field public static final friend_finder_invite_search_text:I = 0x7f090785

.field public static final friend_finder_learn_more:I = 0x7f090786

.field public static final friend_finder_legal_note_text:I = 0x7f09077c

.field public static final friend_finder_name:I = 0x7f09077f

.field public static final friend_finder_progress_bar:I = 0x7f090776

.field public static final friend_finder_progress_bar_container:I = 0x7f090775

.field public static final friend_finder_progress_text:I = 0x7f090777

.field public static final friend_finder_search_bar:I = 0x7f090778

.field public static final friend_finder_search_text:I = 0x7f090779

.field public static final friend_finder_second_credential:I = 0x7f090781

.field public static final friend_finder_see_how_it_works:I = 0x7f0906ef

.field public static final friend_name:I = 0x7f09114b

.field public static final friend_radar_start_button:I = 0x7f0907ca

.field public static final friend_request_accepted:I = 0x7f09078e

.field public static final friend_request_accepted_ack:I = 0x7f09078f

.field public static final friend_request_bottom_buffer:I = 0x7f090792

.field public static final friend_request_button_container:I = 0x7f090f56

.field public static final friend_request_divider:I = 0x7f0906d6

.field public static final friend_request_ignored:I = 0x7f090790

.field public static final friend_request_ignored_ack:I = 0x7f090791

.field public static final friend_request_make_ref_tag:I = 0x7f0900b9

.field public static final friend_request_needs_response:I = 0x7f09078b

.field public static final friend_request_top_buffer:I = 0x7f090787

.field public static final friend_requestlist_container:I = 0x7f090795

.field public static final friend_requests_tab:I = 0x7f09007a

.field public static final friend_selector_autocomplete_container:I = 0x7f0907a6

.field public static final friend_selector_autocomplete_input:I = 0x7f0907a8

.field public static final friend_selector_filter_text_hint:I = 0x7f0907a7

.field public static final friend_selector_list_empty_text:I = 0x7f0907ab

.field public static final friend_selector_list_view:I = 0x7f0907aa

.field public static final friend_selector_loading_indicator:I = 0x7f0907a9

.field public static final friend_selector_main_view:I = 0x7f0907a5

.field public static final friend_social_context:I = 0x7f09114c

.field public static final friend_tag_picker_autocomplete_container:I = 0x7f09079f

.field public static final friend_tag_picker_autocomplete_input:I = 0x7f0907a1

.field public static final friend_tag_picker_loading_indicator:I = 0x7f0907a2

.field public static final friend_user_image:I = 0x7f0909ab

.field public static final friending_codes_background:I = 0x7f0907af

.field public static final friending_codes_expiration:I = 0x7f0907b7

.field public static final friending_codes_my_code:I = 0x7f0907b6

.field public static final friending_codes_my_code_container:I = 0x7f0907b5

.field public static final friending_codes_my_code_progress_circle:I = 0x7f0907b4

.field public static final friending_codes_search_box:I = 0x7f0907b1

.field public static final friending_codes_search_box_container:I = 0x7f0907b0

.field public static final friending_codes_search_icon:I = 0x7f0907b2

.field public static final friending_codes_search_progress_circle:I = 0x7f0907b3

.field public static final friending_codes_try_again:I = 0x7f0907b8

.field public static final friending_icon_button:I = 0x7f09115e

.field public static final friending_loading_more:I = 0x7f0907b9

.field public static final friending_radar_accuracy_text:I = 0x7f0907c9

.field public static final friending_radar_blurred_cover_pic:I = 0x7f0907c3

.field public static final friending_radar_close_button:I = 0x7f0907bf

.field public static final friending_radar_debug_views_container:I = 0x7f0907c6

.field public static final friending_radar_error_header:I = 0x7f0907bb

.field public static final friending_radar_error_subject:I = 0x7f0907bc

.field public static final friending_radar_fragment_container:I = 0x7f0907ba

.field public static final friending_radar_friend_codes_entry_point:I = 0x7f0907c0

.field public static final friending_radar_latitude_text:I = 0x7f0907c7

.field public static final friending_radar_location_settings_button:I = 0x7f0907bd

.field public static final friending_radar_longitude_text:I = 0x7f0907c8

.field public static final friending_radar_nux_codes_entry_point:I = 0x7f0907c2

.field public static final friending_radar_profile_pic:I = 0x7f0907c4

.field public static final friending_radar_search_again_button:I = 0x7f0907be

.field public static final friending_radar_start_button:I = 0x7f0907c1

.field public static final friending_radar_status_text:I = 0x7f0907c5

.field public static final friending_text_button:I = 0x7f09115d

.field public static final friendlist_activity_circle:I = 0x7f0907ce

.field public static final friendlist_fragment_frame:I = 0x7f0907cb

.field public static final friendlist_listview:I = 0x7f0907cd

.field public static final friendlist_search:I = 0x7f0907cc

.field public static final friendpage_tabbed_view_pager_indicator:I = 0x7f0907cf

.field public static final friendpage_view_pager:I = 0x7f0907d0

.field public static final friends_center_extra_text:I = 0x7f0907d6

.field public static final friends_center_friends_panel:I = 0x7f0907d2

.field public static final friends_center_friends_text_button:I = 0x7f0907d7

.field public static final friends_center_home_fragment_container:I = 0x7f0907d1

.field public static final friends_center_list_divider:I = 0x7f0907d8

.field public static final friends_center_suggestions_panel:I = 0x7f0907db

.field public static final friends_center_tab_slider:I = 0x7f0907d9

.field public static final friends_center_user_image:I = 0x7f0907d4

.field public static final friends_center_user_name:I = 0x7f0907d5

.field public static final friends_center_view_pager:I = 0x7f0907da

.field public static final friends_info_list:I = 0x7f09013d

.field public static final friends_list:I = 0x7f090a40

.field public static final friends_list_empty_item:I = 0x7f090b6e

.field public static final friends_list_mask:I = 0x7f090b6f

.field public static final friends_list_search:I = 0x7f090b6b

.field public static final friends_list_search_divider:I = 0x7f090b6d

.field public static final friends_nearby_bottom_divider:I = 0x7f090806

.field public static final friends_nearby_empty_list:I = 0x7f0907e3

.field public static final friends_nearby_empty_text:I = 0x7f0907e8

.field public static final friends_nearby_error:I = 0x7f0907e7

.field public static final friends_nearby_error_button_primary:I = 0x7f0907ed

.field public static final friends_nearby_error_button_secondary:I = 0x7f0907ec

.field public static final friends_nearby_error_buttons:I = 0x7f0907e9

.field public static final friends_nearby_error_subtitle:I = 0x7f0907eb

.field public static final friends_nearby_error_title:I = 0x7f0907ea

.field public static final friends_nearby_feed_unit_header_view:I = 0x7f0907f5

.field public static final friends_nearby_feed_unit_row_view:I = 0x7f0907f6

.field public static final friends_nearby_feed_unit_subtitle_text:I = 0x7f0907ef

.field public static final friends_nearby_feed_unit_title_text:I = 0x7f0907ee

.field public static final friends_nearby_invite_footer_button:I = 0x7f0907f8

.field public static final friends_nearby_invite_toast_content:I = 0x7f090800

.field public static final friends_nearby_invite_toast_title:I = 0x7f0907ff

.field public static final friends_nearby_list:I = 0x7f0907e2

.field public static final friends_nearby_loading:I = 0x7f0907e5

.field public static final friends_nearby_loading_text:I = 0x7f0907e6

.field public static final friends_nearby_map_container:I = 0x7f0907dc

.field public static final friends_nearby_refreshable_container:I = 0x7f0907e1

.field public static final friends_nearby_row_action_button:I = 0x7f0907fe

.field public static final friends_nearby_row_ping_button:I = 0x7f0907f1

.field public static final friends_nearby_search:I = 0x7f0907df

.field public static final friends_nearby_search_and_list:I = 0x7f0907dd

.field public static final friends_nearby_search_bar_container:I = 0x7f0907de

.field public static final friends_nearby_search_invite_button:I = 0x7f0907e0

.field public static final friends_nearby_see_all_button:I = 0x7f0907f7

.field public static final friends_nearby_top_divider:I = 0x7f090802

.field public static final friends_nearby_upsell_container:I = 0x7f0907e4

.field public static final friends_nearby_upsell_icon:I = 0x7f090803

.field public static final friends_nearby_upsell_row_container:I = 0x7f090801

.field public static final friends_nearby_upsell_text:I = 0x7f090804

.field public static final friends_nearby_upsell_title:I = 0x7f090805

.field public static final friends_nearby_user_context:I = 0x7f0907f4

.field public static final friends_nearby_user_image:I = 0x7f0907f2

.field public static final friends_nearby_user_info_container:I = 0x7f0907f0

.field public static final friends_nearby_user_name:I = 0x7f0907f3

.field public static final friends_on_chat_text:I = 0x7f090939

.field public static final friends_on_chat_title:I = 0x7f090938

.field public static final friends_pics:I = 0x7f090937

.field public static final friends_privacy_button:I = 0x7f0901cc

.field public static final friends_selector_divider:I = 0x7f0907a4

.field public static final friends_selector_layout:I = 0x7f090807

.field public static final friends_who_have_visited_facepile:I = 0x7f090f15

.field public static final front:I = 0x7f090625

.field public static final front_album_art:I = 0x7f090a48

.field public static final front_album_art_fader:I = 0x7f090a49

.field public static final front_app_icon:I = 0x7f090a4c

.field public static final front_artist_title:I = 0x7f090a4a

.field public static final front_track_title:I = 0x7f090a4b

.field public static final full_screen:I = 0x7f090c2d

.field public static final full_screen_controls_wrapper:I = 0x7f090813

.field public static final full_screen_paused_image:I = 0x7f090814

.field public static final full_screen_placeholder_image:I = 0x7f090811

.field public static final full_screen_player_progress:I = 0x7f090819

.field public static final full_screen_player_progress_container:I = 0x7f090818

.field public static final full_screen_spec_display:I = 0x7f090817

.field public static final full_screen_video_player:I = 0x7f090cb1

.field public static final full_screen_video_player_container:I = 0x7f09080f

.field public static final full_screen_video_wrapper:I = 0x7f09080e

.field public static final fullscreen_container:I = 0x7f090291

.field public static final fullscreen_current_time:I = 0x7f09080b

.field public static final fullscreen_duration_time:I = 0x7f09080d

.field public static final fullscreen_video_control_pause_button:I = 0x7f09080a

.field public static final fullscreen_video_control_play_button:I = 0x7f090809

.field public static final fullscreen_video_control_progress_bar:I = 0x7f09080c

.field public static final fullscreen_video_controls:I = 0x7f090810

.field public static final fullscreen_video_controls_container:I = 0x7f090808

.field public static final fullscreen_video_feedback:I = 0x7f09081a

.field public static final fullscreen_video_subtitles:I = 0x7f090816

.field public static final gallery:I = 0x7f090c18

.field public static final gallery_button:I = 0x7f090ccb

.field public static final gallery_launcher:I = 0x7f0908d8

.field public static final gallery_launcher_overlay:I = 0x7f09081c

.field public static final gallery_options:I = 0x7f090273

.field public static final gallery_picker_dialog:I = 0x7f090271

.field public static final gdp_layout:I = 0x7f090a5f

.field public static final general_group:I = 0x7f091115

.field public static final general_group_title:I = 0x7f091116

.field public static final generate_meme:I = 0x7f0913a8

.field public static final generic:I = 0x7f090037

.field public static final generic_back:I = 0x7f090634

.field public static final generic_error_retry_image:I = 0x7f090823

.field public static final generic_error_view:I = 0x7f090822

.field public static final get_google_fix:I = 0x7f090f35

.field public static final get_google_last_known:I = 0x7f090f34

.field public static final get_gps_fix:I = 0x7f090f33

.field public static final get_wifi_fix:I = 0x7f090f32

.field public static final gift_card_price_selector:I = 0x7f090f86

.field public static final gift_details_header:I = 0x7f090ce3

.field public static final global_alert_message:I = 0x7f090ba0

.field public static final glyph_checkin:I = 0x7f090f13

.field public static final glyph_placepin:I = 0x7f090f10

.field public static final go_back:I = 0x7f091383

.field public static final go_forward:I = 0x7f091384

.field public static final gone:I = 0x7f090085

.field public static final google_map:I = 0x7f090f17

.field public static final goto_settings_button:I = 0x7f090c75

.field public static final grab_bag_list:I = 0x7f090849

.field public static final grabber_icon:I = 0x7f090b77

.field public static final gradient_left:I = 0x7f090eca

.field public static final gradient_right:I = 0x7f090ecc

.field public static final graph_search_content_view:I = 0x7f090734

.field public static final graph_search_filters_loading:I = 0x7f09013b

.field public static final graph_search_game_rating_bar:I = 0x7f090858

.field public static final graph_search_keyword_results_list_view:I = 0x7f09073d

.field public static final graph_search_middle_dot1:I = 0x7f090861

.field public static final graph_search_photo_results_empty_view:I = 0x7f090738

.field public static final graph_search_photo_results_list_view:I = 0x7f090737

.field public static final graph_search_place_category:I = 0x7f090862

.field public static final graph_search_place_distance:I = 0x7f09085f

.field public static final graph_search_place_rating_bar:I = 0x7f090860

.field public static final graph_search_results_load_more_placeholder:I = 0x7f090864

.field public static final graph_search_results_load_more_progress_bar:I = 0x7f090866

.field public static final graph_search_results_load_more_text:I = 0x7f090865

.field public static final graph_search_text_results_empty_view:I = 0x7f09073c

.field public static final graph_search_text_results_list_view:I = 0x7f09073b

.field public static final gray:I = 0x7f09005f

.field public static final gray_bar:I = 0x7f091311

.field public static final grayscale:I = 0x7f0900cd

.field public static final greeting_text:I = 0x7f0901c6

.field public static final grid:I = 0x7f0912f2

.field public static final grid_wrapper:I = 0x7f0912f1

.field public static final group_button:I = 0x7f09120c

.field public static final group_button_text:I = 0x7f09120d

.field public static final group_description:I = 0x7f090b0e

.field public static final group_facepile:I = 0x7f090617

.field public static final group_facepile_1:I = 0x7f090874

.field public static final group_facepile_2:I = 0x7f090875

.field public static final group_facepile_3:I = 0x7f090876

.field public static final group_facepile_4:I = 0x7f090877

.field public static final group_images_empty_item:I = 0x7f090ba2

.field public static final group_images_gridview:I = 0x7f090ba1

.field public static final group_members_list_view:I = 0x7f090ba4

.field public static final group_name:I = 0x7f090b0d

.field public static final group_thread_action_button:I = 0x7f090ba5

.field public static final group_visibility_description:I = 0x7f090882

.field public static final groups_feed_approve_bar_button:I = 0x7f090878

.field public static final groups_feed_composer_action_bar:I = 0x7f09087a

.field public static final groups_feed_composer_photo:I = 0x7f09137b

.field public static final groups_feed_composer_status:I = 0x7f09137a

.field public static final groups_feed_delete_bar_button:I = 0x7f090879

.field public static final groups_feed_header_chevron:I = 0x7f090883

.field public static final groups_feed_header_cover_photo:I = 0x7f09087f

.field public static final groups_feed_header_facepile:I = 0x7f090880

.field public static final groups_feed_header_group_name:I = 0x7f090881

.field public static final groups_feed_join_bar_text:I = 0x7f090884

.field public static final groups_feed_list:I = 0x7f09087d

.field public static final groups_feed_list_container:I = 0x7f09087c

.field public static final groups_feed_members_bar:I = 0x7f090885

.field public static final groups_feed_pending_bar_button:I = 0x7f090886

.field public static final groups_feed_pinned_post_bar_button:I = 0x7f090887

.field public static final groups_feed_root_container:I = 0x7f09087b

.field public static final guest_friends:I = 0x7f0904ef

.field public static final guest_friends_divider:I = 0x7f0904f1

.field public static final guest_profile_image:I = 0x7f0904ed

.field public static final guest_rsvp_badge:I = 0x7f0904ee

.field public static final guestlist_counts:I = 0x7f0904f2

.field public static final guests_count:I = 0x7f0904f5

.field public static final guests_count_container:I = 0x7f0904f4

.field public static final guests_count_name:I = 0x7f0904f7

.field public static final guests_count_progress_bar:I = 0x7f0904f6

.field public static final happy_birthday_feed_unit_profile_pic_1:I = 0x7f09088a

.field public static final happy_birthday_feed_unit_profile_pic_2:I = 0x7f09088b

.field public static final happy_birthday_feed_unit_profile_pic_3:I = 0x7f09088c

.field public static final happy_birthday_feed_unit_profile_pic_4:I = 0x7f09088d

.field public static final happy_birthday_feed_unit_profile_pic_5:I = 0x7f09088e

.field public static final happy_birthday_feed_unit_profile_pic_6:I = 0x7f09088f

.field public static final happy_birthday_feed_unit_profile_pic_7:I = 0x7f090890

.field public static final happy_birthday_feed_unit_profile_pic_8:I = 0x7f090891

.field public static final happy_birthday_feed_unit_profile_pics:I = 0x7f090889

.field public static final happy_birthday_feed_unit_title:I = 0x7f090888

.field public static final harrison_publisher_bar:I = 0x7f090731

.field public static final harrison_search_button:I = 0x7f091280

.field public static final head_segment:I = 0x7f0903df

.field public static final header_chrome_container:I = 0x7f0909b9

.field public static final header_container:I = 0x7f091163

.field public static final header_description:I = 0x7f091161

.field public static final header_icon:I = 0x7f091057

.field public static final header_metadata:I = 0x7f091162

.field public static final header_overlay:I = 0x7f090ef5

.field public static final header_owner:I = 0x7f091160

.field public static final header_text:I = 0x7f090764

.field public static final header_title:I = 0x7f09115f

.field public static final header_ufi:I = 0x7f091164

.field public static final header_view_actor:I = 0x7f0907ac

.field public static final header_view_menu_button:I = 0x7f09048a

.field public static final header_view_sub_title:I = 0x7f0907ae

.field public static final header_view_title:I = 0x7f090489

.field public static final headers:I = 0x7f090435

.field public static final help_center_setting:I = 0x7f09111b

.field public static final help_text:I = 0x7f09019b

.field public static final helvetica_neue:I = 0x7f090021

.field public static final hide_button:I = 0x7f090440

.field public static final hide_button_divider:I = 0x7f090441

.field public static final high_data_use:I = 0x7f09044f

.field public static final highlights_container:I = 0x7f091281

.field public static final hint_bar:I = 0x7f0903d3

.field public static final history_button:I = 0x7f090923

.field public static final holo_dark:I = 0x7f0900c2

.field public static final holo_light:I = 0x7f0900c3

.field public static final home:I = 0x7f090050

.field public static final homeAsUp:I = 0x7f090040

.field public static final home_friend_requests_panel:I = 0x7f090794

.field public static final home_notification_view:I = 0x7f090893

.field public static final home_notifications:I = 0x7f0903c2

.field public static final home_notifications_bar:I = 0x7f090acb

.field public static final home_notifications_panel:I = 0x7f090adf

.field public static final homeintent_configure_dialog_container:I = 0x7f0908a9

.field public static final honey_client_event:I = 0x7f0900a1

.field public static final horizontal:I = 0x7f090028

.field public static final horizontalPan:I = 0x7f090034

.field public static final horizontal_bar:I = 0x7f090a78

.field public static final horizontal_divider_1:I = 0x7f09091e

.field public static final horizontal_divider_2:I = 0x7f09091f

.field public static final horizontal_divider_3:I = 0x7f090921

.field public static final horizontal_divider_4:I = 0x7f090922

.field public static final horizontal_divider_5:I = 0x7f090924

.field public static final horizontal_divider_6:I = 0x7f090926

.field public static final horizontal_option_selector:I = 0x7f090835

.field public static final horizontal_prices:I = 0x7f090851

.field public static final horizontal_scroll_card_categories:I = 0x7f09028a

.field public static final horizontal_scroll_chaining_remove_button:I = 0x7f0908ad

.field public static final hosts_info_list:I = 0x7f090501

.field public static final hot_like_icon:I = 0x7f090ba7

.field public static final hscroll_pager:I = 0x7f09102d

.field public static final hscroll_single_unit:I = 0x7f0900a8

.field public static final hybrid:I = 0x7f0900c1

.field public static final ic_search:I = 0x7f090a8f

.field public static final icon:I = 0x7f090106

.field public static final icon_image:I = 0x7f09090c

.field public static final icon_item:I = 0x7f091352

.field public static final icon_name_1:I = 0x7f090f69

.field public static final icon_name_2:I = 0x7f090f6a

.field public static final icon_name_3:I = 0x7f090f6b

.field public static final icon_name_4:I = 0x7f090f6c

.field public static final icon_picker_view:I = 0x7f09035a

.field public static final icon_with_text_view_icon:I = 0x7f0908b6

.field public static final icon_with_text_view_title:I = 0x7f0908b7

.field public static final icon_wrapper:I = 0x7f09128d

.field public static final identity_growth_megaphone_buttons_line:I = 0x7f0908c5

.field public static final identity_growth_megaphone_buttons_line_border:I = 0x7f0908c4

.field public static final identity_growth_megaphone_left_button:I = 0x7f0908c7

.field public static final identity_growth_megaphone_option_checkbox:I = 0x7f0908ba

.field public static final identity_growth_megaphone_option_image:I = 0x7f0908b8

.field public static final identity_growth_megaphone_option_subtitle_text:I = 0x7f0908bb

.field public static final identity_growth_megaphone_option_title_text:I = 0x7f0908b9

.field public static final identity_growth_megaphone_options:I = 0x7f0908c3

.field public static final identity_growth_megaphone_privacy_image:I = 0x7f0908be

.field public static final identity_growth_megaphone_privacy_selector:I = 0x7f0908c6

.field public static final identity_growth_megaphone_right_button:I = 0x7f0908c8

.field public static final identity_growth_megaphone_show_more_image:I = 0x7f0908bc

.field public static final identity_growth_megaphone_show_more_text:I = 0x7f0908bd

.field public static final identity_growth_megaphone_spinner_block:I = 0x7f0908c9

.field public static final identity_growth_megaphone_subtitle_text:I = 0x7f0908c2

.field public static final identity_growth_megaphone_title_text:I = 0x7f0908c0

.field public static final identity_growth_megaphone_typeahead_checkbox:I = 0x7f0908cb

.field public static final identity_growth_megaphone_typeahead_text:I = 0x7f0908ca

.field public static final identity_growth_megaphone_x_button:I = 0x7f0908c1

.field public static final identity_growth_progress_circle:I = 0x7f0908bf

.field public static final ifRoom:I = 0x7f090048

.field public static final ig_ego_like:I = 0x7f0908f6

.field public static final ig_ego_photo:I = 0x7f0908f3

.field public static final ig_ego_subtitle:I = 0x7f0908f5

.field public static final ig_ego_title:I = 0x7f0908f4

.field public static final ignore_friend_request_button:I = 0x7f09078d

.field public static final image:I = 0x7f090103

.field public static final image_arrow:I = 0x7f090834

.field public static final image_bottom_arrow:I = 0x7f09024f

.field public static final image_button:I = 0x7f090469

.field public static final image_button_one:I = 0x7f090a15

.field public static final image_button_overflow:I = 0x7f09023e

.field public static final image_button_two:I = 0x7f090a16

.field public static final image_camera_icon:I = 0x7f091146

.field public static final image_check:I = 0x7f090250

.field public static final image_clear:I = 0x7f090929

.field public static final image_contents:I = 0x7f0912d7

.field public static final image_credit_card:I = 0x7f0903cd

.field public static final image_disclosure:I = 0x7f09083b

.field public static final image_downloading:I = 0x7f09130e

.field public static final image_fowarding_button:I = 0x7f090bf2

.field public static final image_overflow:I = 0x7f090a12

.field public static final image_overflow_button:I = 0x7f090c1c

.field public static final image_picker_add_image:I = 0x7f0908d0

.field public static final image_picker_container:I = 0x7f0908cf

.field public static final image_picker_fragment:I = 0x7f090256

.field public static final image_picker_remove_image:I = 0x7f0908d2

.field public static final image_picker_thumbnail_image:I = 0x7f0908d1

.field public static final image_price_tag_corner:I = 0x7f090f80

.field public static final image_product:I = 0x7f090651

.field public static final image_search:I = 0x7f090928

.field public static final image_search_button:I = 0x7f090ab0

.field public static final image_search_empty_item:I = 0x7f090baa

.field public static final image_search_grid:I = 0x7f090ba9

.field public static final image_search_page_range:I = 0x7f090bac

.field public static final image_search_page_range_container:I = 0x7f090bab

.field public static final image_search_page_range_next:I = 0x7f090bae

.field public static final image_search_page_range_prev:I = 0x7f090bad

.field public static final image_search_query:I = 0x7f090ba8

.field public static final image_selector:I = 0x7f090850

.field public static final image_set_bottom_left_image:I = 0x7f09058f

.field public static final image_set_bottom_right_image:I = 0x7f090590

.field public static final image_set_container:I = 0x7f09058d

.field public static final image_set_top_image:I = 0x7f09058e

.field public static final image_shadow:I = 0x7f090ce9

.field public static final image_sku_1:I = 0x7f090f9a

.field public static final image_sku_2:I = 0x7f090f9f

.field public static final image_text_view:I = 0x7f09086c

.field public static final image_top_arrow:I = 0x7f09024d

.field public static final image_view:I = 0x7f090448

.field public static final image_white_gradient:I = 0x7f090f8d

.field public static final images_container:I = 0x7f0908d5

.field public static final images_reorder_view:I = 0x7f09031b

.field public static final imageview_camera_review:I = 0x7f09130f

.field public static final impermanent_cards_container:I = 0x7f09099b

.field public static final impermanent_cards_container_bottom_space:I = 0x7f09099c

.field public static final impermanent_cards_container_top_space:I = 0x7f09099a

.field public static final impermanent_cards_scrollview:I = 0x7f090999

.field public static final implicit_location:I = 0x7f090338

.field public static final implicit_location_to_be_deleted:I = 0x7f09038f

.field public static final import_button:I = 0x7f090ff3

.field public static final import_contacts_bottom_instructions:I = 0x7f0906dd

.field public static final import_contacts_kddi_bottom_instructions:I = 0x7f0906e0

.field public static final import_contacts_kddi_button:I = 0x7f0906df

.field public static final import_contacts_kddi_layout:I = 0x7f0906de

.field public static final import_contacts_stretcher:I = 0x7f0906dc

.field public static final inappropriate:I = 0x7f091396

.field public static final incall_button_background:I = 0x7f09134a

.field public static final incall_button_mute:I = 0x7f091348

.field public static final incall_button_speaker:I = 0x7f091347

.field public static final incall_button_video:I = 0x7f091349

.field public static final incall_buttons:I = 0x7f091343

.field public static final incoming_call_buttons:I = 0x7f09133f

.field public static final indicator_stub:I = 0x7f0908da

.field public static final indicator_titlebar:I = 0x7f091275

.field public static final indicator_titlebar_stub:I = 0x7f091274

.field public static final info_request_ask:I = 0x7f0902de

.field public static final info_request_cancel:I = 0x7f0902e1

.field public static final info_request_progress_bar:I = 0x7f0908df

.field public static final info_request_sent:I = 0x7f0902df

.field public static final info_request_sent_text:I = 0x7f0902e0

.field public static final info_request_status_container:I = 0x7f0908de

.field public static final info_request_status_title:I = 0x7f0908e0

.field public static final info_request_title:I = 0x7f0902dd

.field public static final info_request_view:I = 0x7f0902d5

.field public static final initial_loading_bar:I = 0x7f0907d3

.field public static final inline_attachment_sound_wave:I = 0x7f0908ea

.field public static final inline_attachment_video_player:I = 0x7f09065a

.field public static final inline_invite_button:I = 0x7f090b66

.field public static final inline_pivot_view_item:I = 0x7f0908e2

.field public static final inline_player_progress:I = 0x7f0908ee

.field public static final inline_video:I = 0x7f090bca

.field public static final inline_video_cover_image:I = 0x7f0908e8

.field public static final inline_video_end_screen:I = 0x7f0908e9

.field public static final inline_video_pivots_author:I = 0x7f091303

.field public static final inline_video_pivots_likecommentshare_info:I = 0x7f091304

.field public static final inline_video_pivots_title:I = 0x7f091302

.field public static final inline_video_play_count:I = 0x7f0908f1

.field public static final inline_video_player_story_attachment:I = 0x7f090659

.field public static final inline_video_status:I = 0x7f0908f0

.field public static final inline_video_subtitle_text:I = 0x7f0908e7

.field public static final inline_video_view:I = 0x7f0908e5

.field public static final inner_circle:I = 0x7f0912bc

.field public static final inner_container:I = 0x7f090479

.field public static final inner_feed_story_attachments:I = 0x7f0901c5

.field public static final inner_scroll:I = 0x7f09034a

.field public static final insight_flyout_background:I = 0x7f091195

.field public static final insight_flyout_bubble:I = 0x7f091194

.field public static final insight_flyout_loading_progress_bar:I = 0x7f091197

.field public static final insight_flyout_loading_view:I = 0x7f091196

.field public static final insights_and_promotion_scroll_view:I = 0x7f091199

.field public static final insights_bar:I = 0x7f09118f

.field public static final insights_label:I = 0x7f091192

.field public static final insights_list:I = 0x7f091193

.field public static final insights_note:I = 0x7f091190

.field public static final insights_number:I = 0x7f091191

.field public static final insights_pager:I = 0x7f09119a

.field public static final insights_pager_indicator:I = 0x7f09119b

.field public static final instagram_button:I = 0x7f0901b3

.field public static final install_button:I = 0x7f090484

.field public static final install_messenger_fragment:I = 0x7f0908f7

.field public static final install_messenger_fragment_scroll_view:I = 0x7f0908f8

.field public static final install_messenger_promotion_button:I = 0x7f090900

.field public static final install_prompt_content:I = 0x7f0908fa

.field public static final install_prompt_header:I = 0x7f0908f9

.field public static final instructions:I = 0x7f090120

.field public static final internal_settings_button:I = 0x7f090927

.field public static final internal_settings_group:I = 0x7f09111e

.field public static final internal_settings_group_title:I = 0x7f09111f

.field public static final internal_settings_setting:I = 0x7f091120

.field public static final interstitial_legal_disclaimer:I = 0x7f0906da

.field public static final invisible:I = 0x7f090084

.field public static final invitable_contact_list:I = 0x7f0906f2

.field public static final invite_button:I = 0x7f091211

.field public static final invite_friends_button:I = 0x7f090b5a

.field public static final iorg_menu:I = 0x7f09090b

.field public static final iorg_saved_pages_list_time_unique_id_tag_key:I = 0x7f0900ec

.field public static final is_a_facebox:I = 0x7f0900d5

.field public static final is_available_for_chat_toggle:I = 0x7f090b7c

.field public static final is_picked_checkbox:I = 0x7f090b5d

.field public static final is_picked_secondary_checkbox:I = 0x7f090b64

.field public static final is_sponsored:I = 0x7f0900b5

.field public static final italic:I = 0x7f090007

.field public static final item_action_chevron:I = 0x7f090960

.field public static final item_action_icon:I = 0x7f09082b

.field public static final item_action_link:I = 0x7f09095f

.field public static final item_action_text_button:I = 0x7f090963

.field public static final item_action_text_button_icon:I = 0x7f090962

.field public static final item_action_text_button_icon_container:I = 0x7f090961

.field public static final item_container:I = 0x7f090824

.field public static final item_details_container:I = 0x7f09095d

.field public static final item_facepile:I = 0x7f09095c

.field public static final item_horizontal_divider:I = 0x7f090825

.field public static final item_image:I = 0x7f090826

.field public static final item_image_container:I = 0x7f09095b

.field public static final item_label:I = 0x7f0910d5

.field public static final item_list_feed_unit_item:I = 0x7f0900b4

.field public static final item_pager:I = 0x7f0908ae

.field public static final item_privacy_icon:I = 0x7f091292

.field public static final item_rating:I = 0x7f09095e

.field public static final item_root:I = 0x7f0910d4

.field public static final item_secondary_subtitle:I = 0x7f09082a

.field public static final item_subtitle:I = 0x7f090829

.field public static final item_text_frame:I = 0x7f091293

.field public static final item_title:I = 0x7f090828

.field public static final item_vertical_divider:I = 0x7f090827

.field public static final jewel_button_count:I = 0x7f090936

.field public static final jewel_button_image:I = 0x7f090935

.field public static final just_use_text_location_image:I = 0x7f09093c

.field public static final just_use_text_location_name:I = 0x7f09093d

.field public static final keyword_module_central_entity_cover_photo:I = 0x7f090941

.field public static final keyword_module_central_entity_profile_picture:I = 0x7f090940

.field public static final keyword_search_action_button:I = 0x7f09093f

.field public static final keyword_search_content_view:I = 0x7f09093e

.field public static final keyword_search_host_tabbed_view_pager_indicator:I = 0x7f09073e

.field public static final keyword_search_host_view_pager:I = 0x7f09073f

.field public static final ksgallery_product_images:I = 0x7f090f84

.field public static final ksimage_let_them_choose_icon:I = 0x7f090fa4

.field public static final ksimage_recipient_avatar:I = 0x7f090fb1

.field public static final ksimage_sender_avatar:I = 0x7f091136

.field public static final label:I = 0x7f0901e8

.field public static final large:I = 0x7f09005a

.field public static final last_contributor:I = 0x7f090171

.field public static final last_updated:I = 0x7f09029c

.field public static final launchable_wrapper:I = 0x7f0903ed

.field public static final launcher_action_container:I = 0x7f09041e

.field public static final launcher_checkmark:I = 0x7f0909a5

.field public static final launcher_wf:I = 0x7f090f77

.field public static final layout:I = 0x7f090ac0

.field public static final layout_card_categories_holder:I = 0x7f09028b

.field public static final layout_card_cell:I = 0x7f090ce6

.field public static final layout_card_data:I = 0x7f0903d8

.field public static final layout_cell_1:I = 0x7f090f92

.field public static final layout_cell_2:I = 0x7f090f93

.field public static final layout_cell_3:I = 0x7f090f94

.field public static final layout_content:I = 0x7f0903cb

.field public static final layout_credit_cards:I = 0x7f0903cc

.field public static final layout_custom_card_root:I = 0x7f0903d6

.field public static final layout_gallery_button:I = 0x7f09026a

.field public static final layout_gift_post_example:I = 0x7f091133

.field public static final layout_header:I = 0x7f09084b

.field public static final layout_helper:I = 0x7f0911af

.field public static final layout_image_gallery:I = 0x7f090f83

.field public static final layout_main_content:I = 0x7f090286

.field public static final layout_media_picker:I = 0x7f0909e2

.field public static final layout_more_gift_cards:I = 0x7f090831

.field public static final layout_more_information:I = 0x7f090837

.field public static final layout_more_products:I = 0x7f090832

.field public static final layout_more_products_list:I = 0x7f090838

.field public static final layout_option_list:I = 0x7f090253

.field public static final layout_option_scroll:I = 0x7f090252

.field public static final layout_order_totals:I = 0x7f090cef

.field public static final layout_order_totals_discount:I = 0x7f090cf6

.field public static final layout_other_gift_cards_list:I = 0x7f090840

.field public static final layout_payment_information:I = 0x7f090ce8

.field public static final layout_price_tag:I = 0x7f090f7f

.field public static final layout_product:I = 0x7f090fa3

.field public static final layout_product1_price_tag:I = 0x7f090fad

.field public static final layout_product2_price_tag:I = 0x7f090faf

.field public static final layout_product_1:I = 0x7f090852

.field public static final layout_product_1_info:I = 0x7f090fac

.field public static final layout_product_2:I = 0x7f090854

.field public static final layout_product_2_info:I = 0x7f090fae

.field public static final layout_product_3:I = 0x7f090855

.field public static final layout_product_cell:I = 0x7f090ce5

.field public static final layout_recipient_cell:I = 0x7f090ce4

.field public static final layout_recipient_header:I = 0x7f09084d

.field public static final layout_recipientless_header:I = 0x7f09084f

.field public static final layout_recommend_list:I = 0x7f090932

.field public static final layout_recommended_cell:I = 0x7f090830

.field public static final layout_recommended_header:I = 0x7f090842

.field public static final layout_recommended_products:I = 0x7f090f8e

.field public static final layout_recommended_products_content:I = 0x7f090f91

.field public static final layout_scroll:I = 0x7f090195

.field public static final layout_see_all_gifts:I = 0x7f090833

.field public static final layout_share_gift_choice:I = 0x7f091131

.field public static final layout_sku1_price_tag:I = 0x7f090f9c

.field public static final layout_sku2_price_tag:I = 0x7f090fa1

.field public static final layout_sku_1:I = 0x7f090f98

.field public static final layout_sku_1_info:I = 0x7f090f99

.field public static final layout_sku_2:I = 0x7f090f9d

.field public static final layout_sku_2_info:I = 0x7f090f9e

.field public static final layout_sku_price_tag:I = 0x7f090fa6

.field public static final layout_title_and_up:I = 0x7f09023f

.field public static final lbl_install_buttons:I = 0x7f090901

.field public static final lbl_install_new_build_app:I = 0x7f090904

.field public static final lbl_install_new_build_notes_text:I = 0x7f090906

.field public static final lbl_install_new_build_version:I = 0x7f090905

.field public static final learn_more:I = 0x7f0903f7

.field public static final learn_more_link:I = 0x7f0901ce

.field public static final leave_conversation:I = 0x7f0913a1

.field public static final left:I = 0x7f09000c

.field public static final leftOfAnchor:I = 0x7f090071

.field public static final left_action_button:I = 0x7f0902b7

.field public static final left_action_button_icon:I = 0x7f0902b8

.field public static final left_action_button_label:I = 0x7f0902b9

.field public static final left_button:I = 0x7f090c76

.field public static final left_button_badge:I = 0x7f090c77

.field public static final left_column:I = 0x7f090aac

.field public static final left_divider:I = 0x7f090f07

.field public static final left_icon:I = 0x7f09010e

.field public static final left_margin:I = 0x7f090912

.field public static final left_origin_mask:I = 0x7f090b34

.field public static final left_origin_text_view:I = 0x7f090b35

.field public static final left_pager_button:I = 0x7f0907fc

.field public static final left_side_menu:I = 0x7f090944

.field public static final left_side_menu_fragment_container:I = 0x7f090945

.field public static final left_side_shadow:I = 0x7f090946

.field public static final left_text:I = 0x7f0902a7

.field public static final life_event_attachment:I = 0x7f090954

.field public static final life_event_bottom_padding:I = 0x7f090957

.field public static final life_event_footer_section:I = 0x7f090956

.field public static final life_event_icon:I = 0x7f09094d

.field public static final life_event_location:I = 0x7f090955

.field public static final life_event_menu_button:I = 0x7f090953

.field public static final life_event_story_message:I = 0x7f090951

.field public static final life_event_subtitle:I = 0x7f09094f

.field public static final life_event_title:I = 0x7f09094e

.field public static final life_event_top_padding:I = 0x7f090952

.field public static final life_event_under_subtitle:I = 0x7f090950

.field public static final light:I = 0x7f090024

.field public static final like_button:I = 0x7f09017d

.field public static final like_button_inline:I = 0x7f090cc6

.field public static final like_text:I = 0x7f090181

.field public static final likers_arrow:I = 0x7f090771

.field public static final likers_header_text:I = 0x7f090772

.field public static final likers_separator:I = 0x7f090773

.field public static final likers_switch_view_holder:I = 0x7f090770

.field public static final line1:I = 0x7f090b31

.field public static final line2:I = 0x7f090b32

.field public static final linear:I = 0x7f090030

.field public static final link_attachment_bottom_view:I = 0x7f0901a5

.field public static final link_attachment_context_text:I = 0x7f0901a9

.field public static final link_attachment_large_photo:I = 0x7f0901a4

.field public static final link_attachment_small_photo:I = 0x7f0901a6

.field public static final link_attachment_title_text:I = 0x7f0901a8

.field public static final link_card_bottom_view:I = 0x7f0904d7

.field public static final link_preview_stub:I = 0x7f090bd1

.field public static final link_text:I = 0x7f090943

.field public static final list:I = 0x7f09092b

.field public static final listMode:I = 0x7f09003c

.field public static final list_collection_add_button:I = 0x7f0902ee

.field public static final list_collection_add_friend:I = 0x7f0902ef

.field public static final list_collection_buttons:I = 0x7f0902ed

.field public static final list_collection_group_join:I = 0x7f0902f1

.field public static final list_collection_icon_container:I = 0x7f0902eb

.field public static final list_collection_item_divider:I = 0x7f0902d8

.field public static final list_collection_item_icon:I = 0x7f0902ec

.field public static final list_collection_item_subtitle:I = 0x7f0902f4

.field public static final list_collection_item_title:I = 0x7f0902f3

.field public static final list_collection_subscribe:I = 0x7f0902f0

.field public static final list_collection_text_container:I = 0x7f0902f2

.field public static final list_collection_update_confirmation:I = 0x7f0902f5

.field public static final list_empty_progress:I = 0x7f0904c1

.field public static final list_empty_text:I = 0x7f0904c0

.field public static final list_empty_title:I = 0x7f090623

.field public static final list_footer:I = 0x7f090436

.field public static final list_item:I = 0x7f090105

.field public static final list_products:I = 0x7f090fa9

.field public static final list_skus:I = 0x7f090f95

.field public static final list_view:I = 0x7f090183

.field public static final list_view_feed_contents:I = 0x7f090967

.field public static final list_view_feed_header_text_container:I = 0x7f090964

.field public static final list_view_feed_timestamp:I = 0x7f090966

.field public static final list_view_feed_title:I = 0x7f090965

.field public static final live_wallpaper_settings_activity:I = 0x7f09089b

.field public static final loading:I = 0x7f09132c

.field public static final loadingBar:I = 0x7f090a62

.field public static final loading_button:I = 0x7f090415

.field public static final loading_container:I = 0x7f09076d

.field public static final loading_empty_item:I = 0x7f090cac

.field public static final loading_indicator:I = 0x7f090c6a

.field public static final loading_indicator_view:I = 0x7f09129e

.field public static final loading_more:I = 0x7f090fc7

.field public static final loading_progress:I = 0x7f090a27

.field public static final loading_spinner:I = 0x7f09062d

.field public static final loading_status_text:I = 0x7f090418

.field public static final loading_strings_error_view:I = 0x7f09134d

.field public static final loading_strings_progress_bar:I = 0x7f09134c

.field public static final loading_view:I = 0x7f090194

.field public static final location_button:I = 0x7f0907fb

.field public static final location_hover:I = 0x7f090f25

.field public static final location_list_view:I = 0x7f09096f

.field public static final location_name:I = 0x7f090f26

.field public static final location_nux_nub:I = 0x7f09136f

.field public static final location_ping_dialog_container:I = 0x7f090973

.field public static final location_ping_dialog_delete:I = 0x7f09097a

.field public static final location_ping_dialog_duration_button:I = 0x7f090976

.field public static final location_ping_dialog_duration_list:I = 0x7f090975

.field public static final location_ping_dialog_message:I = 0x7f090978

.field public static final location_ping_dialog_message_container:I = 0x7f090977

.field public static final location_ping_dialog_message_length:I = 0x7f090979

.field public static final location_ping_dialog_negative_button:I = 0x7f09097b

.field public static final location_ping_dialog_positive_button:I = 0x7f09097c

.field public static final location_ping_dialog_progress:I = 0x7f09097d

.field public static final location_ping_dialog_progress_text:I = 0x7f09097e

.field public static final location_ping_dialog_time_label:I = 0x7f090974

.field public static final location_ping_option_checkmark:I = 0x7f090972

.field public static final location_ping_option_time_until_text:I = 0x7f090971

.field public static final location_selector_autocomplete_input:I = 0x7f090980

.field public static final location_selector_list_empty_text:I = 0x7f090983

.field public static final location_selector_list_view:I = 0x7f090982

.field public static final location_selector_loading_indicator:I = 0x7f090981

.field public static final location_selector_main_view:I = 0x7f09097f

.field public static final location_services_enable_checkbox:I = 0x7f090bb8

.field public static final location_services_text:I = 0x7f090bb7

.field public static final location_setting_description:I = 0x7f090985

.field public static final location_setting_title:I = 0x7f090984

.field public static final location_settings_container:I = 0x7f090986

.field public static final location_settings_device_settings_container:I = 0x7f09098e

.field public static final location_settings_device_settings_header:I = 0x7f09098f

.field public static final location_settings_device_settings_rows_container:I = 0x7f090990

.field public static final location_settings_error:I = 0x7f090994

.field public static final location_settings_fb_settings_container:I = 0x7f090987

.field public static final location_settings_fb_settings_header:I = 0x7f090988

.field public static final location_settings_history_desc:I = 0x7f09098c

.field public static final location_settings_history_icon:I = 0x7f09098a

.field public static final location_settings_history_learn_more:I = 0x7f09098d

.field public static final location_settings_history_row:I = 0x7f090989

.field public static final location_settings_history_switch:I = 0x7f09098b

.field public static final location_settings_location_services_off_desc:I = 0x7f090991

.field public static final location_settings_location_services_off_manage:I = 0x7f090992

.field public static final location_settings_progress:I = 0x7f090993

.field public static final location_targeting_fragment_container:I = 0x7f090996

.field public static final location_typeahead_layout:I = 0x7f090995

.field public static final location_x:I = 0x7f090f27

.field public static final locatioon_picker_list_fragment:I = 0x7f090970

.field public static final lock_and_launcher:I = 0x7f091350

.field public static final lock_icon:I = 0x7f0912e8

.field public static final lock_screen_only:I = 0x7f09134f

.field public static final lock_wf:I = 0x7f090f70

.field public static final lockscreen_checkmark:I = 0x7f0909a3

.field public static final lockscreen_disable_dialog_navigation_button_view:I = 0x7f09099d

.field public static final lockscreen_disable_survey_dialog_navigation_button_view:I = 0x7f09099e

.field public static final lockscreen_dismiss_text:I = 0x7f090ad3

.field public static final lockscreen_group:I = 0x7f091111

.field public static final lockscreen_group_title:I = 0x7f091112

.field public static final lockscreen_launcher_description_text:I = 0x7f09099f

.field public static final lockscreen_launcher_fragment_container:I = 0x7f09110d

.field public static final lockscreen_notif_list:I = 0x7f090ad2

.field public static final lockscreen_notifications_container:I = 0x7f090ad0

.field public static final lockscreen_option_text:I = 0x7f0909a2

.field public static final lockscreen_picture:I = 0x7f0909a1

.field public static final lockscreen_settings_dialog_container:I = 0x7f090acf

.field public static final lockscreen_settings_entries_container:I = 0x7f0909af

.field public static final lockscreen_settings_entry_enable_checkbox:I = 0x7f0909b4

.field public static final lockscreen_settings_entry_notification_subtitle:I = 0x7f0909b3

.field public static final lockscreen_settings_entry_notification_title:I = 0x7f0909b2

.field public static final lockscreen_settings_light_up_screen_setting:I = 0x7f0909b0

.field public static final lockscreen_settings_master_switch:I = 0x7f0909ae

.field public static final lockscreen_settings_ok_button:I = 0x7f0909b1

.field public static final lockscreen_title_container:I = 0x7f090ad1

.field public static final log_into_another_account:I = 0x7f09045f

.field public static final log_out_button:I = 0x7f090821

.field public static final log_out_divider:I = 0x7f090820

.field public static final logged_out_layout:I = 0x7f0908a6

.field public static final login:I = 0x7f09041c

.field public static final login_approvals_code:I = 0x7f0902c9

.field public static final login_approvals_focus_holder:I = 0x7f09056e

.field public static final login_approvals_group:I = 0x7f09056d

.field public static final login_approvals_login:I = 0x7f09056f

.field public static final login_bottom_group:I = 0x7f090571

.field public static final login_fb_logo:I = 0x7f09045b

.field public static final login_fb_logo_alt:I = 0x7f09043b

.field public static final login_fb_logo_container:I = 0x7f090570

.field public static final login_focus_holder:I = 0x7f09056c

.field public static final login_forgot_password:I = 0x7f090467

.field public static final login_fragment_controller:I = 0x7f090562

.field public static final login_fragment_controller_host:I = 0x7f0909b7

.field public static final login_fragment_host:I = 0x7f090561

.field public static final login_help_button:I = 0x7f090bbd

.field public static final login_help_center_landscape:I = 0x7f090574

.field public static final login_help_center_portrait:I = 0x7f090572

.field public static final login_link:I = 0x7f091077

.field public static final login_login:I = 0x7f09056b

.field public static final login_logo:I = 0x7f0911d6

.field public static final login_logo1:I = 0x7f090426

.field public static final login_logo1_splash:I = 0x7f090bb9

.field public static final login_logo2:I = 0x7f090427

.field public static final login_logo2_splash:I = 0x7f090bba

.field public static final login_logo_container:I = 0x7f090bbb

.field public static final login_main_group:I = 0x7f090568

.field public static final login_password:I = 0x7f09056a

.field public static final login_progress:I = 0x7f090577

.field public static final login_root:I = 0x7f090563

.field public static final login_settings:I = 0x7f09045e

.field public static final login_signin:I = 0x7f090466

.field public static final login_signup:I = 0x7f090573

.field public static final login_splash:I = 0x7f090575

.field public static final login_sso_text:I = 0x7f09043c

.field public static final login_text:I = 0x7f09062b

.field public static final login_username:I = 0x7f090569

.field public static final logo:I = 0x7f090628

.field public static final logo_image:I = 0x7f090892

.field public static final logo_title_wrapper:I = 0x7f091279

.field public static final logout_custom_view:I = 0x7f0909b8

.field public static final low_data_use:I = 0x7f090451

.field public static final main_container:I = 0x7f0908d7

.field public static final main_expandable_photo:I = 0x7f0908d9

.field public static final main_group:I = 0x7f09110c

.field public static final main_text:I = 0x7f09090d

.field public static final main_view:I = 0x7f09079e

.field public static final map:I = 0x7f090bbf

.field public static final map_container:I = 0x7f0907f9

.field public static final map_container_with_border:I = 0x7f090f3b

.field public static final map_hint_view:I = 0x7f090f3e

.field public static final map_image_fallback:I = 0x7f090f3d

.field public static final map_image_stub:I = 0x7f090f3c

.field public static final map_layout:I = 0x7f090f16

.field public static final map_show_button:I = 0x7f0909c4

.field public static final map_url_image:I = 0x7f0909c3

.field public static final map_user_name:I = 0x7f090bc0

.field public static final map_user_snippet:I = 0x7f090bc2

.field public static final map_user_time:I = 0x7f090bc1

.field public static final map_view_group:I = 0x7f0910d9

.field public static final mark_duplicate:I = 0x7f091395

.field public static final masked_bubble_stub:I = 0x7f090be0

.field public static final match_parent:I = 0x7f090057

.field public static final match_siblings:I = 0x7f0900e5

.field public static final matrix:I = 0x7f090019

.field public static final me_main_fragment:I = 0x7f090245

.field public static final me_pivots_fragment:I = 0x7f090246

.field public static final media_attachments:I = 0x7f090380

.field public static final media_gallery:I = 0x7f090bc4

.field public static final media_gallery_content:I = 0x7f0909c9

.field public static final media_gallery_deep_link_stub:I = 0x7f0909db

.field public static final media_gallery_footer:I = 0x7f0909d9

.field public static final media_gallery_footer_buttons:I = 0x7f0909d2

.field public static final media_gallery_footer_caption:I = 0x7f0909cc

.field public static final media_gallery_footer_comment_button:I = 0x7f0909d4

.field public static final media_gallery_footer_counters:I = 0x7f0909d7

.field public static final media_gallery_footer_information:I = 0x7f0909ca

.field public static final media_gallery_footer_like_button:I = 0x7f0909d3

.field public static final media_gallery_footer_menu_button:I = 0x7f0909d6

.field public static final media_gallery_footer_separator:I = 0x7f0909d1

.field public static final media_gallery_footer_tagging_button:I = 0x7f0909d5

.field public static final media_gallery_footer_tagging_nux:I = 0x7f0909cd

.field public static final media_gallery_owner_name:I = 0x7f0909cb

.field public static final media_gallery_pager:I = 0x7f0909d8

.field public static final media_gallery_privacy_icon:I = 0x7f0909d0

.field public static final media_gallery_progress:I = 0x7f0909da

.field public static final media_gallery_root:I = 0x7f0909c7

.field public static final media_gallery_tag_bubble:I = 0x7f0909dd

.field public static final media_gallery_tag_remove_button:I = 0x7f0909df

.field public static final media_gallery_tag_text:I = 0x7f0909de

.field public static final media_gallery_time_and_privacy_container:I = 0x7f0909ce

.field public static final media_gallery_timestamp:I = 0x7f0909cf

.field public static final media_tray_button_divider_padding:I = 0x7f0909f2

.field public static final media_tray_edit_button:I = 0x7f0909f1

.field public static final media_tray_error:I = 0x7f090bcc

.field public static final media_tray_error_text:I = 0x7f090bcd

.field public static final media_tray_send_and_edit_button:I = 0x7f0909f0

.field public static final media_tray_send_button:I = 0x7f0909f3

.field public static final media_tray_video_player:I = 0x7f090bc8

.field public static final media_tray_video_player_stub:I = 0x7f090bc7

.field public static final medium:I = 0x7f090026

.field public static final medium_data_use:I = 0x7f090450

.field public static final megaphone_body:I = 0x7f0909ff

.field public static final megaphone_button_accept:I = 0x7f090a04

.field public static final megaphone_button_cancel:I = 0x7f090a03

.field public static final megaphone_button_solo_accept:I = 0x7f090a01

.field public static final megaphone_container:I = 0x7f0909fb

.field public static final megaphone_container_bottom:I = 0x7f090a02

.field public static final megaphone_container_top:I = 0x7f0909fc

.field public static final megaphone_facepile_1:I = 0x7f0909f4

.field public static final megaphone_facepile_2:I = 0x7f0909f5

.field public static final megaphone_facepile_3:I = 0x7f0909f6

.field public static final megaphone_facepile_4:I = 0x7f0909f7

.field public static final megaphone_facepile_5:I = 0x7f0909f8

.field public static final megaphone_facepile_6:I = 0x7f0909f9

.field public static final megaphone_facepile_container:I = 0x7f090a00

.field public static final megaphone_facepile_text:I = 0x7f0909fa

.field public static final megaphone_icon:I = 0x7f0909fd

.field public static final megaphone_title:I = 0x7f0909fe

.field public static final megaphone_x_button:I = 0x7f090a05

.field public static final member_bar_extra_pog_count:I = 0x7f090a07

.field public static final member_container:I = 0x7f090c9e

.field public static final member_email:I = 0x7f090ca1

.field public static final member_separator:I = 0x7f090ca2

.field public static final member_user:I = 0x7f090ca0

.field public static final member_user_img:I = 0x7f090c9f

.field public static final meme_bottom_edit:I = 0x7f090a0f

.field public static final meme_bottom_text:I = 0x7f090a0d

.field public static final meme_container:I = 0x7f090a0a

.field public static final meme_empty_item:I = 0x7f090bcf

.field public static final meme_grid:I = 0x7f090bd0

.field public static final meme_header:I = 0x7f090bce

.field public static final meme_image:I = 0x7f090a0b

.field public static final meme_layout:I = 0x7f090a09

.field public static final meme_send_button:I = 0x7f090a10

.field public static final meme_top_edit:I = 0x7f090a0e

.field public static final meme_top_text:I = 0x7f090a0c

.field public static final menu_button:I = 0x7f0903a9

.field public static final menu_group_phone_numbers:I = 0x7f090096

.field public static final menu_group_thread_list:I = 0x7f090094

.field public static final menu_item_call_number:I = 0x7f090095

.field public static final menu_item_copy:I = 0x7f091386

.field public static final menu_item_icon:I = 0x7f090a13

.field public static final menu_item_navigation:I = 0x7f091382

.field public static final menu_item_open_with:I = 0x7f091387

.field public static final menu_item_save:I = 0x7f091388

.field public static final menu_item_save_image:I = 0x7f091390

.field public static final menu_item_share:I = 0x7f091385

.field public static final menu_item_text:I = 0x7f090a14

.field public static final menu_row_icon:I = 0x7f090959

.field public static final menu_row_text:I = 0x7f09095a

.field public static final message:I = 0x7f09068e

.field public static final message_attachment_text:I = 0x7f090a1a

.field public static final message_audio:I = 0x7f090be4

.field public static final message_body:I = 0x7f090c3c

.field public static final message_bubble_container:I = 0x7f090bdf

.field public static final message_button:I = 0x7f091209

.field public static final message_button_text:I = 0x7f09120a

.field public static final message_composer_placeholder:I = 0x7f090b4d

.field public static final message_container:I = 0x7f090b14

.field public static final message_date:I = 0x7f090bee

.field public static final message_detail_container:I = 0x7f090beb

.field public static final message_divider_container:I = 0x7f090bdb

.field public static final message_divider_text:I = 0x7f090bdc

.field public static final message_failed_retry_button:I = 0x7f090bef

.field public static final message_group_divider:I = 0x7f09120b

.field public static final message_images:I = 0x7f090bf1

.field public static final message_left_bubble_sender_name:I = 0x7f090bdd

.field public static final message_list_container:I = 0x7f090ca9

.field public static final message_map_frame_mask:I = 0x7f090bec

.field public static final message_map_image:I = 0x7f090bed

.field public static final message_other:I = 0x7f090be9

.field public static final message_read_header:I = 0x7f090bf7

.field public static final message_read_timestamp:I = 0x7f090bf8

.field public static final message_read_without_timestamp:I = 0x7f090bf9

.field public static final message_send_progress_bar:I = 0x7f090bf0

.field public static final message_share:I = 0x7f090bea

.field public static final message_subject:I = 0x7f090a18

.field public static final message_text:I = 0x7f090a19

.field public static final message_text_view:I = 0x7f090285

.field public static final message_time:I = 0x7f090c1e

.field public static final message_timestamp_text:I = 0x7f090a1c

.field public static final message_top_section:I = 0x7f090c1b

.field public static final message_user:I = 0x7f090c1d

.field public static final message_user_tile:I = 0x7f090bde

.field public static final message_video:I = 0x7f090be5

.field public static final message_wrapper:I = 0x7f090284

.field public static final messages_compose:I = 0x7f090caf

.field public static final messages_list:I = 0x7f090caa

.field public static final messages_list_shadow_bottom:I = 0x7f090cae

.field public static final messages_list_shadow_top:I = 0x7f090cad

.field public static final messages_popup_nub:I = 0x7f090b38

.field public static final messages_tab:I = 0x7f09007b

.field public static final messenger_facepile:I = 0x7f0908fc

.field public static final messenger_image:I = 0x7f090ffc

.field public static final messenger_mask_container:I = 0x7f090ffb

.field public static final messenger_place_search_map_fragment:I = 0x7f090a20

.field public static final meta:I = 0x7f09005e

.field public static final middle:I = 0x7f09004f

.field public static final min_budget_error:I = 0x7f0911c0

.field public static final minutiae_activity_root_view:I = 0x7f09034b

.field public static final minutiae_badge_animation:I = 0x7f090a23

.field public static final minutiae_badge_text:I = 0x7f090a24

.field public static final minutiae_button:I = 0x7f0900d6

.field public static final minutiae_checkmark:I = 0x7f090369

.field public static final minutiae_chevron:I = 0x7f090368

.field public static final minutiae_delete_tag_btn:I = 0x7f090351

.field public static final minutiae_error_text:I = 0x7f090a25

.field public static final minutiae_footer_description:I = 0x7f09034f

.field public static final minutiae_footer_icon:I = 0x7f09034e

.field public static final minutiae_glyph:I = 0x7f090365

.field public static final minutiae_icon_image:I = 0x7f09036a

.field public static final minutiae_icon_picker_grid:I = 0x7f09035e

.field public static final minutiae_icon_picker_header:I = 0x7f09035d

.field public static final minutiae_icon_picker_header_container:I = 0x7f09035b

.field public static final minutiae_icon_picker_header_icon:I = 0x7f09035c

.field public static final minutiae_list_container:I = 0x7f090356

.field public static final minutiae_nux_view:I = 0x7f090353

.field public static final minutiae_place_picker_location_settings_button:I = 0x7f090a26

.field public static final minutiae_profile_pic:I = 0x7f090366

.field public static final minutiae_subtitle:I = 0x7f090367

.field public static final minutiae_taggable_object_search_progress_indicator:I = 0x7f090364

.field public static final minutiae_taggable_object_search_text_view:I = 0x7f090363

.field public static final minutiae_taggable_object_search_view:I = 0x7f090355

.field public static final minutiae_title:I = 0x7f090350

.field public static final minutiae_view_divider:I = 0x7f090354

.field public static final missed_stories_arrow:I = 0x7f0905d1

.field public static final missed_stories_button:I = 0x7f090746

.field public static final missed_stories_button_text:I = 0x7f0905d2

.field public static final mobile_zero_upsell_feed_unit_footer_divider:I = 0x7f090a2d

.field public static final mobile_zero_upsell_items_pager:I = 0x7f090a2e

.field public static final mobile_zero_upsell_title:I = 0x7f090a2c

.field public static final modify_settings_dont_ask_again:I = 0x7f090f3f

.field public static final moments_product_pager:I = 0x7f091236

.field public static final moments_title:I = 0x7f091235

.field public static final monochrome:I = 0x7f0900ce

.field public static final monospace:I = 0x7f090005

.field public static final more_indicator_stub:I = 0x7f090383

.field public static final more_options_flyout_bubble:I = 0x7f090442

.field public static final more_options_flyout_content:I = 0x7f090443

.field public static final more_options_flyout_list:I = 0x7f090444

.field public static final more_options_flyout_nub:I = 0x7f090445

.field public static final more_options_link:I = 0x7f0901cd

.field public static final multi_share_item_container:I = 0x7f0906a5

.field public static final multi_share_item_description:I = 0x7f0906ab

.field public static final multi_share_item_footer:I = 0x7f0906a8

.field public static final multi_share_item_image_container:I = 0x7f0906a6

.field public static final multi_share_item_photo:I = 0x7f0906a7

.field public static final multi_share_item_title:I = 0x7f0906aa

.field public static final multi_share_rows_item_container:I = 0x7f090a2f

.field public static final multi_share_rows_item_title:I = 0x7f090a30

.field public static final multi_share_see_more_text:I = 0x7f0906a9

.field public static final multipicker_contact_picker:I = 0x7f090b54

.field public static final multipicker_cover:I = 0x7f090b4e

.field public static final multipicker_custom_layout:I = 0x7f090b53

.field public static final multishare_attachment_pager:I = 0x7f09065e

.field public static final music_list_pager_landscape_stub:I = 0x7f090a52

.field public static final music_list_pager_stub:I = 0x7f090a51

.field public static final music_loading_spinner:I = 0x7f090a3c

.field public static final music_notification_back_view:I = 0x7f090a41

.field public static final music_notification_buffering_spinner:I = 0x7f090a46

.field public static final music_notification_front_view:I = 0x7f090a47

.field public static final music_preview_card_artist:I = 0x7f090a56

.field public static final music_preview_card_button:I = 0x7f090a54

.field public static final music_preview_card_cover_art:I = 0x7f090a53

.field public static final music_preview_card_ellipsis:I = 0x7f090a5a

.field public static final music_preview_card_save_button:I = 0x7f090a5d

.field public static final music_preview_card_save_button_container:I = 0x7f090a5c

.field public static final music_preview_card_song_provider_action:I = 0x7f090a57

.field public static final music_preview_card_source:I = 0x7f090a59

.field public static final music_preview_card_source_image:I = 0x7f090a58

.field public static final music_preview_card_title:I = 0x7f090a55

.field public static final music_preview_cover_container:I = 0x7f090a5b

.field public static final must_be_in_targeting_gk_warning:I = 0x7f090ffa

.field public static final mute_button:I = 0x7f090487

.field public static final mute_notifications:I = 0x7f091399

.field public static final my_layout:I = 0x7f090461

.field public static final name:I = 0x7f090863

.field public static final name_edit_text:I = 0x7f090184

.field public static final name_thread:I = 0x7f09139c

.field public static final native_name_maincontainer:I = 0x7f09074b

.field public static final native_name_name:I = 0x7f090a6c

.field public static final native_name_name_extra:I = 0x7f090afa

.field public static final native_name_surname:I = 0x7f090a6b

.field public static final native_name_surname_extra:I = 0x7f090af9

.field public static final nav_container:I = 0x7f09135a

.field public static final navigation_button_view:I = 0x7f09112e

.field public static final navigation_container:I = 0x7f0909bb

.field public static final navigation_frame:I = 0x7f091124

.field public static final navigion_button_view:I = 0x7f09100c

.field public static final navitem_list:I = 0x7f091243

.field public static final nearby_category_arts:I = 0x7f090a9f

.field public static final nearby_category_coffee:I = 0x7f090a9c

.field public static final nearby_category_hotels:I = 0x7f090aa0

.field public static final nearby_category_nearby:I = 0x7f090a9a

.field public static final nearby_category_nightlife:I = 0x7f090a9d

.field public static final nearby_category_outdoor:I = 0x7f090a9e

.field public static final nearby_category_restaurants:I = 0x7f090a9b

.field public static final nearby_category_shopping:I = 0x7f090aa1

.field public static final nearby_container:I = 0x7f090a8a

.field public static final nearby_list_background_slider:I = 0x7f090a8c

.field public static final nearby_loader_text:I = 0x7f090a7d

.field public static final nearby_map_area_fragment_container:I = 0x7f090a7c

.field public static final nearby_map_area_fragment_v2_container:I = 0x7f090a8b

.field public static final nearby_map_badge_bitmap:I = 0x7f09136d

.field public static final nearby_map_container:I = 0x7f090a94

.field public static final nearby_nux_search_button:I = 0x7f090a91

.field public static final nearby_place_badge:I = 0x7f09136c

.field public static final nearby_place_icon:I = 0x7f09136b

.field public static final nearby_places_fallback:I = 0x7f090a89

.field public static final nearby_places_fragment:I = 0x7f090a99

.field public static final nearby_places_list:I = 0x7f090a8d

.field public static final nearby_places_search_fragment:I = 0x7f090aa7

.field public static final nearby_progress_bar:I = 0x7f090a93

.field public static final nearby_progress_bar_container:I = 0x7f090a92

.field public static final nearby_search_container:I = 0x7f090a8e

.field public static final nearby_search_list:I = 0x7f090aa3

.field public static final nearby_search_query:I = 0x7f090aa2

.field public static final nearby_search_text:I = 0x7f090a90

.field public static final nearby_subcategory_fragment:I = 0x7f090aa9

.field public static final nearby_subcategory_list:I = 0x7f090aa8

.field public static final nearby_tab:I = 0x7f090080

.field public static final nearby_typeahead_status_view:I = 0x7f090aa4

.field public static final nearby_vertical_separator:I = 0x7f090a98

.field public static final nearbyrow_divider:I = 0x7f090a1f

.field public static final network_error_banner:I = 0x7f09081b

.field public static final network_image_presenter_params_tag:I = 0x7f0900e1

.field public static final network_image_presenter_tag:I = 0x7f0900e0

.field public static final neue_contact_picker_fragment_container:I = 0x7f090ab2

.field public static final never:I = 0x7f090047

.field public static final new_bookmark_favorites_indicator:I = 0x7f09020d

.field public static final new_bookmark_image:I = 0x7f09020e

.field public static final new_bookmark_label:I = 0x7f090210

.field public static final new_bookmark_section_link_indicator:I = 0x7f0910ff

.field public static final new_bookmark_section_name:I = 0x7f091100

.field public static final new_bookmark_small_sub_text:I = 0x7f090211

.field public static final new_bookmark_unread_count:I = 0x7f09020f

.field public static final new_event_get_together:I = 0x7f090ab5

.field public static final new_event_host_an_event:I = 0x7f090ab6

.field public static final new_message_pill_text:I = 0x7f090c04

.field public static final new_stories_arrow:I = 0x7f0905d3

.field public static final new_stories_button:I = 0x7f090743

.field public static final new_stories_button_text:I = 0x7f0905d4

.field public static final news_feed_tab:I = 0x7f090079

.field public static final newsfeed_container:I = 0x7f090740

.field public static final newsfeed_shadow:I = 0x7f090098

.field public static final next_button:I = 0x7f090439

.field public static final next_glyph:I = 0x7f090a4e

.field public static final no_contacts_found:I = 0x7f0906e2

.field public static final no_images_found_text:I = 0x7f0909e7

.field public static final no_images_middle:I = 0x7f0909e6

.field public static final no_images_selected_text:I = 0x7f0909e4

.field public static final no_images_top:I = 0x7f0909e5

.field public static final no_internet_layout:I = 0x7f0908a4

.field public static final no_internet_line:I = 0x7f090ab8

.field public static final no_photo_layout:I = 0x7f0908a5

.field public static final no_photo_view_stub:I = 0x7f091144

.field public static final no_physical_address:I = 0x7f091392

.field public static final nodex_button:I = 0x7f090abc

.field public static final nodex_exception:I = 0x7f090abb

.field public static final nodex_message:I = 0x7f090aba

.field public static final nodex_title:I = 0x7f090ab9

.field public static final none:I = 0x7f090038

.field public static final normal:I = 0x7f090002

.field public static final not_in_any_group_notice:I = 0x7f090ff8

.field public static final not_in_experiment_notice:I = 0x7f090ff7

.field public static final not_public:I = 0x7f091397

.field public static final not_you_link:I = 0x7f090429

.field public static final notes_collection_item:I = 0x7f0902f6

.field public static final notes_collection_item_body:I = 0x7f0902f9

.field public static final notes_collection_item_subtitle:I = 0x7f0902f8

.field public static final notes_collection_item_title:I = 0x7f0902f7

.field public static final notif_content:I = 0x7f090ac2

.field public static final notif_count:I = 0x7f0909a8

.field public static final notif_text:I = 0x7f0909a9

.field public static final notif_time:I = 0x7f0909a7

.field public static final notif_title:I = 0x7f0909b5

.field public static final notification_banner:I = 0x7f090745

.field public static final notification_banner_button:I = 0x7f090c08

.field public static final notification_banner_error_icon:I = 0x7f09082d

.field public static final notification_banner_left_container:I = 0x7f090c05

.field public static final notification_banner_text:I = 0x7f090c07

.field public static final notification_banner_title:I = 0x7f09082e

.field public static final notification_dismiss_button:I = 0x7f090abf

.field public static final notification_image:I = 0x7f090ac5

.field public static final notification_sender:I = 0x7f090555

.field public static final notification_sender1:I = 0x7f090ac6

.field public static final notification_sender2:I = 0x7f090ac7

.field public static final notification_sender_container:I = 0x7f090894

.field public static final notification_sender_multi_2:I = 0x7f090ac9

.field public static final notification_sender_multi_3:I = 0x7f090aca

.field public static final notification_sender_multi_container:I = 0x7f090ac8

.field public static final notification_settings:I = 0x7f0909b6

.field public static final notification_snippet:I = 0x7f090bb0

.field public static final notification_text:I = 0x7f090556

.field public static final notification_thumbnail_view:I = 0x7f090558

.field public static final notification_timestamp_text:I = 0x7f090557

.field public static final notification_title:I = 0x7f090895

.field public static final notification_user_tile:I = 0x7f090baf

.field public static final notification_view_container:I = 0x7f090abe

.field public static final notificationlist_container:I = 0x7f090ae0

.field public static final notifications_beeper:I = 0x7f090212

.field public static final notifications_container:I = 0x7f090ad7

.field public static final notifications_count:I = 0x7f0903c4

.field public static final notifications_loading_more:I = 0x7f090acd

.field public static final notifications_opt_in_banner:I = 0x7f090ad4

.field public static final notifications_opt_in_container:I = 0x7f090ad5

.field public static final notifications_tab:I = 0x7f09007c

.field public static final notifications_top_banner:I = 0x7f090ace

.field public static final notifications_view_fragment:I = 0x7f090acc

.field public static final nub:I = 0x7f09117b

.field public static final nuxMessage:I = 0x7f090a65

.field public static final nux_add_passcode_message_view:I = 0x7f090473

.field public static final nux_background:I = 0x7f090ae4

.field public static final nux_background_container:I = 0x7f090ae2

.field public static final nux_background_welcome_text:I = 0x7f090ae5

.field public static final nux_beta_tag:I = 0x7f090af2

.field public static final nux_bubble_view_bottom_pointer:I = 0x7f090aea

.field public static final nux_bubble_view_bubble:I = 0x7f090ae6

.field public static final nux_bubble_view_text_body:I = 0x7f090ae8

.field public static final nux_bubble_view_text_title:I = 0x7f090ae7

.field public static final nux_bubble_view_top_pointer:I = 0x7f090ae9

.field public static final nux_button_negative:I = 0x7f090af7

.field public static final nux_button_positive:I = 0x7f090af8

.field public static final nux_buttons:I = 0x7f090af6

.field public static final nux_image:I = 0x7f09082c

.field public static final nux_inner_content_container:I = 0x7f090af5

.field public static final nux_native_name_layout:I = 0x7f09074a

.field public static final nux_profile_info_item_privacy_selector_arrow:I = 0x7f090754

.field public static final nux_profile_info_item_privacy_selector_image:I = 0x7f090753

.field public static final nux_profile_info_item_privacy_selector_separator:I = 0x7f090752

.field public static final nux_steps_viewpager:I = 0x7f0912d4

.field public static final nux_string:I = 0x7f090c2f

.field public static final nux_subtitle:I = 0x7f090af4

.field public static final nux_text:I = 0x7f0912be

.field public static final nux_title:I = 0x7f090af3

.field public static final okButton:I = 0x7f090a69

.field public static final ongoing_notification:I = 0x7f090b02

.field public static final ongoing_notification_content:I = 0x7f090b03

.field public static final ongoing_notification_info:I = 0x7f090b04

.field public static final ongoing_notification_setting:I = 0x7f091117

.field public static final open_application_honey_client_event_params:I = 0x7f0900b2

.field public static final open_chat_head:I = 0x7f09138f

.field public static final open_contact_info_dialog:I = 0x7f091377

.field public static final open_full_view:I = 0x7f0913a2

.field public static final open_invite_picker:I = 0x7f091375

.field public static final open_profile:I = 0x7f091376

.field public static final opposite:I = 0x7f090008

.field public static final opt_in_title_container:I = 0x7f090ad6

.field public static final optin:I = 0x7f090b05

.field public static final optin_button:I = 0x7f0912e4

.field public static final optin_button_group:I = 0x7f090a6d

.field public static final optin_cancel_button:I = 0x7f090a6e

.field public static final optin_clickable_text_view:I = 0x7f091368

.field public static final optin_confirm_button:I = 0x7f090a6f

.field public static final optin_content_scrollview:I = 0x7f091364

.field public static final optin_desc:I = 0x7f0912e3

.field public static final optin_description_text_view:I = 0x7f090a73

.field public static final optin_detail_text_view:I = 0x7f091367

.field public static final optin_disclaimer_text_view:I = 0x7f090a77

.field public static final optin_facepile_message_text_view:I = 0x7f091366

.field public static final optin_facepile_view:I = 0x7f090a76

.field public static final optin_footer:I = 0x7f0912e5

.field public static final optin_friends_message_text_view:I = 0x7f090a75

.field public static final optin_header_group:I = 0x7f091361

.field public static final optin_help_button:I = 0x7f0912e6

.field public static final optin_image_view:I = 0x7f091365

.field public static final optin_logo_image_view:I = 0x7f090a74

.field public static final optin_main_body_scrollview:I = 0x7f090a71

.field public static final optin_primary_button:I = 0x7f091363

.field public static final optin_progress_spinner:I = 0x7f090a70

.field public static final optin_secondary_button:I = 0x7f091362

.field public static final optin_title_text_view:I = 0x7f090a72

.field public static final optinframe:I = 0x7f090b06

.field public static final orca_diverbar_fragment_container:I = 0x7f090b87

.field public static final orca_edit_favorites_fragment:I = 0x7f090b91

.field public static final orca_quick_cam_promo_button_dot:I = 0x7f09136e

.field public static final orca_quick_cam_tooltip:I = 0x7f090c34

.field public static final orca_thread_list_empty_logo_and_message:I = 0x7f090c84

.field public static final orca_thread_list_preview_image:I = 0x7f090c9a

.field public static final orca_thread_list_video_preview_state:I = 0x7f090c9d

.field public static final orca_thread_list_video_preview_state_background:I = 0x7f090c9c

.field public static final orca_thread_list_video_preview_state_button_stub:I = 0x7f090c9b

.field public static final orca_threadlist_fragment:I = 0x7f090c83

.field public static final orca_threadlist_fragment_container:I = 0x7f090c82

.field public static final orca_video_message_item_progress:I = 0x7f090cde

.field public static final orca_video_message_item_state:I = 0x7f090cdc

.field public static final orca_video_message_item_state_background:I = 0x7f090cdb

.field public static final orca_video_message_item_state_button_stub:I = 0x7f090be7

.field public static final orca_video_message_item_status:I = 0x7f090be8

.field public static final orca_video_message_item_video_thumbnail:I = 0x7f090be6

.field public static final orca_video_message_sending_progress_bar:I = 0x7f090cdd

.field public static final outer_container:I = 0x7f090478

.field public static final outer_ring:I = 0x7f0912bd

.field public static final overflow_button:I = 0x7f090cc3

.field public static final overlay:I = 0x7f0911cc

.field public static final owned_tab:I = 0x7f090c60

.field public static final pack_empty_list_view_item:I = 0x7f090c6e

.field public static final pack_list_view:I = 0x7f090c6d

.field public static final padding:I = 0x7f09032d

.field public static final page_activity_container:I = 0x7f09075b

.field public static final page_activity_insights_like_graph:I = 0x7f090d09

.field public static final page_activity_insights_likes_count:I = 0x7f090d01

.field public static final page_activity_insights_likes_description:I = 0x7f090d02

.field public static final page_activity_insights_likes_reach_divider:I = 0x7f090d03

.field public static final page_activity_insights_reach_count:I = 0x7f090d05

.field public static final page_activity_insights_reach_description:I = 0x7f090d06

.field public static final page_activity_insights_reach_graph:I = 0x7f090d0a

.field public static final page_activity_insights_summary_switcher:I = 0x7f090d07

.field public static final page_activity_insights_switcher_indicator:I = 0x7f090d08

.field public static final page_activity_insights_switcher_likes_container:I = 0x7f090d00

.field public static final page_activity_insights_switcher_reach_container:I = 0x7f090d04

.field public static final page_activity_uni_blue_like_circle:I = 0x7f090d1a

.field public static final page_activity_uni_button:I = 0x7f090d0b

.field public static final page_activity_uni_running_budget:I = 0x7f090d13

.field public static final page_activity_uni_running_duration:I = 0x7f090d14

.field public static final page_activity_uni_running_duration_status_divider:I = 0x7f090d15

.field public static final page_activity_uni_running_edit_icon:I = 0x7f090d0c

.field public static final page_activity_uni_running_likes_count:I = 0x7f090d0d

.field public static final page_activity_uni_running_likes_description:I = 0x7f090d0e

.field public static final page_activity_uni_running_likes_reach_divider:I = 0x7f090d0f

.field public static final page_activity_uni_running_reach_count:I = 0x7f090d10

.field public static final page_activity_uni_running_reach_description:I = 0x7f090d11

.field public static final page_activity_uni_running_spent:I = 0x7f090d12

.field public static final page_activity_uni_running_status_icon:I = 0x7f090d17

.field public static final page_activity_uni_running_status_row:I = 0x7f090d16

.field public static final page_activity_uni_running_status_text:I = 0x7f090d18

.field public static final page_admin_app_icon:I = 0x7f090d24

.field public static final page_admin_header_view_posted_by_subtitle:I = 0x7f090d1b

.field public static final page_admin_invite_others_button:I = 0x7f090d21

.field public static final page_admin_invite_others_button_text:I = 0x7f090d22

.field public static final page_admin_invite_others_chevron:I = 0x7f090d23

.field public static final page_admin_link_chevron:I = 0x7f090db7

.field public static final page_admin_link_door_icon:I = 0x7f090db8

.field public static final page_admin_link_title:I = 0x7f090db6

.field public static final page_admin_links_card:I = 0x7f09075c

.field public static final page_admin_new_like_link:I = 0x7f090d1e

.field public static final page_admin_notification_link:I = 0x7f090d1c

.field public static final page_admin_scheduled_posts_link:I = 0x7f090d20

.field public static final page_admin_upsell_card:I = 0x7f09075d

.field public static final page_admin_upsell_card_divider:I = 0x7f090d29

.field public static final page_admin_upsell_card_divider_new_like:I = 0x7f090d1d

.field public static final page_admin_upsell_card_divider_scheduled_post:I = 0x7f090d1f

.field public static final page_admin_upsell_title:I = 0x7f090d25

.field public static final page_friend_inviter_divider_row0:I = 0x7f090d34

.field public static final page_friend_inviter_divider_row1:I = 0x7f090d36

.field public static final page_friend_inviter_divider_row2:I = 0x7f090d38

.field public static final page_friend_inviter_divider_title:I = 0x7f090d32

.field public static final page_friend_inviter_invite_others:I = 0x7f090d39

.field public static final page_friend_inviter_row0:I = 0x7f090d33

.field public static final page_friend_inviter_row1:I = 0x7f090d35

.field public static final page_friend_inviter_row2:I = 0x7f090d37

.field public static final page_friend_inviter_title:I = 0x7f090d31

.field public static final page_header_container:I = 0x7f090d2d

.field public static final page_identity_about_overlay_container:I = 0x7f090d5a

.field public static final page_identity_action_sheet_bottom_border:I = 0x7f090d42

.field public static final page_identity_action_sheet_button1:I = 0x7f090d3e

.field public static final page_identity_action_sheet_button2:I = 0x7f090d3f

.field public static final page_identity_action_sheet_button3:I = 0x7f090d40

.field public static final page_identity_action_sheet_more_button:I = 0x7f090d41

.field public static final page_identity_action_sheet_public_refresh:I = 0x7f090d5d

.field public static final page_identity_action_sheet_stub:I = 0x7f090d5c

.field public static final page_identity_activity_badge_count:I = 0x7f090d4b

.field public static final page_identity_activity_tab:I = 0x7f090d49

.field public static final page_identity_activity_tab_text:I = 0x7f090d4a

.field public static final page_identity_actor_image:I = 0x7f090df4

.field public static final page_identity_admin_posts_by_others_card_view_stub:I = 0x7f090d82

.field public static final page_identity_admin_social_context_card:I = 0x7f090d6f

.field public static final page_identity_admin_social_context_card_stub:I = 0x7f090d6e

.field public static final page_identity_admin_tabs:I = 0x7f09075e

.field public static final page_identity_album_frag_container:I = 0x7f09018d

.field public static final page_identity_album_tab:I = 0x7f09018f

.field public static final page_identity_attribution_description:I = 0x7f090d4f

.field public static final page_identity_attribution_image:I = 0x7f090d4e

.field public static final page_identity_bar_chart:I = 0x7f090df3

.field public static final page_identity_bar_chart_average_text:I = 0x7f090df0

.field public static final page_identity_bar_chart_rating_stars:I = 0x7f090df1

.field public static final page_identity_bar_chart_total_ratings:I = 0x7f090df2

.field public static final page_identity_carousel_gallery:I = 0x7f090d52

.field public static final page_identity_carousel_header:I = 0x7f090d50

.field public static final page_identity_carousel_title:I = 0x7f090d51

.field public static final page_identity_categories:I = 0x7f090dab

.field public static final page_identity_checkins_ptat_divider:I = 0x7f090db2

.field public static final page_identity_chevron:I = 0x7f090d58

.field public static final page_identity_chevron_header:I = 0x7f090de2

.field public static final page_identity_chevron_header_count:I = 0x7f090e6b

.field public static final page_identity_child_locations_card:I = 0x7f090d67

.field public static final page_identity_child_locations_card_stub:I = 0x7f090d66

.field public static final page_identity_child_locations_list:I = 0x7f090d55

.field public static final page_identity_child_locations_list_container:I = 0x7f09013c

.field public static final page_identity_child_locations_map:I = 0x7f090d53

.field public static final page_identity_condensed_posts_by_others_card:I = 0x7f090d81

.field public static final page_identity_contact_information_title:I = 0x7f090dc8

.field public static final page_identity_context_items:I = 0x7f090d5f

.field public static final page_identity_context_items_container:I = 0x7f090d8c

.field public static final page_identity_context_items_container_border:I = 0x7f090d8d

.field public static final page_identity_context_items_header_stub:I = 0x7f090d5e

.field public static final page_identity_context_items_info_card_stub:I = 0x7f090d60

.field public static final page_identity_cover_gradient_wrapper:I = 0x7f090da4

.field public static final page_identity_cover_photo:I = 0x7f090da5

.field public static final page_identity_cover_photo_pressed_overlay:I = 0x7f090da6

.field public static final page_identity_day_chooser:I = 0x7f090dd4

.field public static final page_identity_day_spinner:I = 0x7f090dd6

.field public static final page_identity_error_banner:I = 0x7f09075a

.field public static final page_identity_error_icon:I = 0x7f090d8f

.field public static final page_identity_error_see_more:I = 0x7f090d8a

.field public static final page_identity_event_cover_photo:I = 0x7f090d90

.field public static final page_identity_event_date:I = 0x7f090d94

.field public static final page_identity_event_date_container:I = 0x7f090d92

.field public static final page_identity_event_divider:I = 0x7f090d9a

.field public static final page_identity_event_extra_padding:I = 0x7f090d99

.field public static final page_identity_event_info_container:I = 0x7f090d91

.field public static final page_identity_event_join_button_text_based:I = 0x7f090d9b

.field public static final page_identity_event_month:I = 0x7f090d93

.field public static final page_identity_event_social_context:I = 0x7f090d98

.field public static final page_identity_event_time:I = 0x7f090d97

.field public static final page_identity_event_title:I = 0x7f090d96

.field public static final page_identity_event_wall_of_text:I = 0x7f090d95

.field public static final page_identity_events_here_card:I = 0x7f090d7f

.field public static final page_identity_events_here_card_stub:I = 0x7f090d7e

.field public static final page_identity_facepile:I = 0x7f090de7

.field public static final page_identity_fake_last_header_view:I = 0x7f090d8b

.field public static final page_identity_friend_image:I = 0x7f090d9e

.field public static final page_identity_friend_inviter_more_friends_button:I = 0x7f090d47

.field public static final page_identity_friend_inviter_more_friends_button_text:I = 0x7f090e38

.field public static final page_identity_friend_name:I = 0x7f090d9f

.field public static final page_identity_friends_here_now_unit:I = 0x7f090e37

.field public static final page_identity_friends_info_summary_unit:I = 0x7f090e36

.field public static final page_identity_grid_facepile_face1:I = 0x7f090da0

.field public static final page_identity_grid_facepile_face2:I = 0x7f090da2

.field public static final page_identity_grid_facepile_face3:I = 0x7f090da3

.field public static final page_identity_grid_facepile_right_column:I = 0x7f090da1

.field public static final page_identity_header:I = 0x7f090d5b

.field public static final page_identity_header_about_label:I = 0x7f090daa

.field public static final page_identity_info_card:I = 0x7f090d61

.field public static final page_identity_info_card_about:I = 0x7f090dad

.field public static final page_identity_info_card_container:I = 0x7f090dac

.field public static final page_identity_insights_card_unit:I = 0x7f090e35

.field public static final page_identity_insights_tab:I = 0x7f090d4c

.field public static final page_identity_likes_checkins_divider:I = 0x7f090dae

.field public static final page_identity_likes_count:I = 0x7f090d44

.field public static final page_identity_likes_description_text:I = 0x7f090d45

.field public static final page_identity_likes_divider:I = 0x7f090d46

.field public static final page_identity_list_view:I = 0x7f090759

.field public static final page_identity_list_view_header_label:I = 0x7f090db9

.field public static final page_identity_loading_error:I = 0x7f090d8e

.field public static final page_identity_loading_progress:I = 0x7f090d89

.field public static final page_identity_map_divider:I = 0x7f090d54

.field public static final page_identity_map_image:I = 0x7f090dba

.field public static final page_identity_map_pin:I = 0x7f090dbb

.field public static final page_identity_more_child_locations_count_text:I = 0x7f090d59

.field public static final page_identity_name:I = 0x7f09036c

.field public static final page_identity_offer_story_card:I = 0x7f090d75

.field public static final page_identity_offer_story_card_stub:I = 0x7f090d74

.field public static final page_identity_opentable_card:I = 0x7f090d6b

.field public static final page_identity_opentable_card_stub:I = 0x7f090d6a

.field public static final page_identity_opentable_change_partysize_button:I = 0x7f090dc6

.field public static final page_identity_opentable_change_time_button:I = 0x7f090dc5

.field public static final page_identity_opentable_contact_info_back_button:I = 0x7f090dcd

.field public static final page_identity_opentable_contact_info_reserve_button:I = 0x7f090dce

.field public static final page_identity_opentable_contact_information_disclaimer:I = 0x7f090dcc

.field public static final page_identity_opentable_contact_information_disclaimer_title:I = 0x7f090dcb

.field public static final page_identity_opentable_container:I = 0x7f090dbc

.field public static final page_identity_opentable_email:I = 0x7f090dca

.field public static final page_identity_opentable_loading_view:I = 0x7f090dbd

.field public static final page_identity_opentable_phone_number:I = 0x7f090dc9

.field public static final page_identity_opentable_reservation_cancel_button:I = 0x7f090dd3

.field public static final page_identity_opentable_reservation_restaurant_message:I = 0x7f090dd2

.field public static final page_identity_opentable_reservation_secondary_title:I = 0x7f090dd1

.field public static final page_identity_opentable_reservation_title:I = 0x7f090dd0

.field public static final page_identity_opentable_search_view:I = 0x7f090dbe

.field public static final page_identity_opentable_secondary_title:I = 0x7f090dc0

.field public static final page_identity_opentable_slot_0:I = 0x7f090dc2

.field public static final page_identity_opentable_slot_1:I = 0x7f090dc3

.field public static final page_identity_opentable_slot_2:I = 0x7f090dc4

.field public static final page_identity_opentable_slots_container:I = 0x7f090dc1

.field public static final page_identity_overall_rating:I = 0x7f090e32

.field public static final page_identity_overall_rating_content_desc:I = 0x7f090e31

.field public static final page_identity_page_indicator:I = 0x7f090d4d

.field public static final page_identity_page_posts_heading:I = 0x7f090d87

.field public static final page_identity_page_posts_heading_text:I = 0x7f090d88

.field public static final page_identity_page_tab:I = 0x7f090d48

.field public static final page_identity_partysize_chooser:I = 0x7f090dd8

.field public static final page_identity_partysize_spinner:I = 0x7f090dd9

.field public static final page_identity_photo_attribution_container:I = 0x7f090ddc

.field public static final page_identity_photos_card:I = 0x7f090d7d

.field public static final page_identity_photos_card_stub:I = 0x7f090d7c

.field public static final page_identity_photos_gallery:I = 0x7f090de3

.field public static final page_identity_photos_header:I = 0x7f090de0

.field public static final page_identity_photos_header_text:I = 0x7f090de1

.field public static final page_identity_photos_tabs:I = 0x7f09018e

.field public static final page_identity_pinned_story_card:I = 0x7f090d73

.field public static final page_identity_pinned_story_card_stub:I = 0x7f090d72

.field public static final page_identity_pma_message_icon:I = 0x7f090d2c

.field public static final page_identity_pma_message_number:I = 0x7f090d2b

.field public static final page_identity_pma_messages_button:I = 0x7f090d2a

.field public static final page_identity_pma_notification_icon:I = 0x7f090d28

.field public static final page_identity_pma_notification_number:I = 0x7f090d27

.field public static final page_identity_pma_notifications_button:I = 0x7f090d26

.field public static final page_identity_posts_by_others_card_view_stub:I = 0x7f090d80

.field public static final page_identity_posts_by_others_chevron:I = 0x7f090de5

.field public static final page_identity_posts_by_others_container:I = 0x7f090de4

.field public static final page_identity_posts_by_others_friends:I = 0x7f090de6

.field public static final page_identity_posts_by_others_total_posts:I = 0x7f090d43

.field public static final page_identity_privacy_icon:I = 0x7f090e1d

.field public static final page_identity_privacy_icon_button:I = 0x7f090e1c

.field public static final page_identity_profile_pic:I = 0x7f09036b

.field public static final page_identity_profile_pic_container:I = 0x7f090da8

.field public static final page_identity_profile_pic_pressed_overlay:I = 0x7f090da9

.field public static final page_identity_ptat_container:I = 0x7f090db3

.field public static final page_identity_ptat_count:I = 0x7f090db4

.field public static final page_identity_ptat_description_text:I = 0x7f090db5

.field public static final page_identity_publisher_photo:I = 0x7f090ded

.field public static final page_identity_publisher_photo_button:I = 0x7f090def

.field public static final page_identity_publisher_photo_icon:I = 0x7f090dee

.field public static final page_identity_publisher_separator:I = 0x7f090dec

.field public static final page_identity_publisher_status:I = 0x7f090de9

.field public static final page_identity_publisher_status_button:I = 0x7f090deb

.field public static final page_identity_publisher_status_icon:I = 0x7f090dea

.field public static final page_identity_rating_content:I = 0x7f090df5

.field public static final page_identity_rating_page_rating_bar:I = 0x7f090e1b

.field public static final page_identity_ratings_divider:I = 0x7f090e34

.field public static final page_identity_recommendation:I = 0x7f090e06

.field public static final page_identity_recommendation_comment:I = 0x7f090dfb

.field public static final page_identity_recommendation_comment_container:I = 0x7f090e03

.field public static final page_identity_recommendation_comment_count_text:I = 0x7f090e05

.field public static final page_identity_recommendation_comment_icon:I = 0x7f090e04

.field public static final page_identity_recommendation_comment_separator:I = 0x7f090dfe

.field public static final page_identity_recommendation_comment_text:I = 0x7f090dff

.field public static final page_identity_recommendation_creator:I = 0x7f090df7

.field public static final page_identity_recommendation_feedback_container:I = 0x7f090dfc

.field public static final page_identity_recommendation_feedback_icon_container:I = 0x7f090e00

.field public static final page_identity_recommendation_feedback_like_text:I = 0x7f090dfd

.field public static final page_identity_recommendation_icon_separator:I = 0x7f090e01

.field public static final page_identity_recommendation_like_count_text:I = 0x7f090e02

.field public static final page_identity_recommendation_time:I = 0x7f090df9

.field public static final page_identity_review_0:I = 0x7f090e51

.field public static final page_identity_review_1:I = 0x7f090e09

.field public static final page_identity_review_2:I = 0x7f090e0b

.field public static final page_identity_review_3:I = 0x7f090e0d

.field public static final page_identity_review_button:I = 0x7f090e16

.field public static final page_identity_review_button_divider:I = 0x7f090e15

.field public static final page_identity_review_composer:I = 0x7f090e18

.field public static final page_identity_review_composer_divider:I = 0x7f090e17

.field public static final page_identity_review_divider_1:I = 0x7f090e08

.field public static final page_identity_review_divider_2:I = 0x7f090e0a

.field public static final page_identity_review_divider_3:I = 0x7f090e0c

.field public static final page_identity_review_question:I = 0x7f090e1a

.field public static final page_identity_reviews_card:I = 0x7f090d71

.field public static final page_identity_reviews_card_header:I = 0x7f090e07

.field public static final page_identity_reviews_card_header_overall_rating_text:I = 0x7f090e22

.field public static final page_identity_reviews_card_header_view_reviews_text:I = 0x7f090e23

.field public static final page_identity_reviews_card_overall_rating_pill:I = 0x7f090e20

.field public static final page_identity_reviews_card_overall_rating_text:I = 0x7f090e21

.field public static final page_identity_reviews_card_stub:I = 0x7f090d70

.field public static final page_identity_reviews_header:I = 0x7f090e10

.field public static final page_identity_reviews_header_more_chevron:I = 0x7f090e12

.field public static final page_identity_reviews_header_text:I = 0x7f090e11

.field public static final page_identity_reviews_inline_composer:I = 0x7f090e0f

.field public static final page_identity_reviews_inline_composer_divider:I = 0x7f090e0e

.field public static final page_identity_reviews_inline_rating_bar:I = 0x7f0910b8

.field public static final page_identity_saved_place_collections_card:I = 0x7f090d63

.field public static final page_identity_saved_place_collections_card_stub:I = 0x7f090d62

.field public static final page_identity_search_title:I = 0x7f090dbf

.field public static final page_identity_see_more_divider:I = 0x7f090d56

.field public static final page_identity_see_more_unit:I = 0x7f090d57

.field public static final page_identity_similar_page_item:I = 0x7f090e2c

.field public static final page_identity_similar_page_item_extra:I = 0x7f090e30

.field public static final page_identity_similar_page_item_image:I = 0x7f090e2d

.field public static final page_identity_similar_page_item_subtitle:I = 0x7f090e2f

.field public static final page_identity_similar_page_item_title:I = 0x7f090e2e

.field public static final page_identity_similar_pages_card:I = 0x7f090d84

.field public static final page_identity_similar_pages_card_stub:I = 0x7f090d83

.field public static final page_identity_similar_pages_header:I = 0x7f090e29

.field public static final page_identity_similar_pages_pager:I = 0x7f090e2b

.field public static final page_identity_similar_pages_title:I = 0x7f090e2a

.field public static final page_identity_social_context_card:I = 0x7f090d6d

.field public static final page_identity_social_context_card_stub:I = 0x7f090d6c

.field public static final page_identity_social_context_friend_inviter_unit:I = 0x7f090e39

.field public static final page_identity_story_pinned_icon:I = 0x7f090d9d

.field public static final page_identity_structured_content_action_icon:I = 0x7f090e4b

.field public static final page_identity_structured_content_action_text:I = 0x7f090e48

.field public static final page_identity_structured_content_attributions_container:I = 0x7f090e44

.field public static final page_identity_structured_content_card:I = 0x7f090d69

.field public static final page_identity_structured_content_card_stub:I = 0x7f090d68

.field public static final page_identity_structured_content_description:I = 0x7f090e43

.field public static final page_identity_structured_content_icon:I = 0x7f090e4d

.field public static final page_identity_structured_content_list:I = 0x7f090e3d

.field public static final page_identity_structured_content_map:I = 0x7f090e3b

.field public static final page_identity_structured_content_map_unit_full_divider:I = 0x7f090e3c

.field public static final page_identity_structured_content_more_info_unit:I = 0x7f090e45

.field public static final page_identity_structured_content_primary_icon:I = 0x7f090e46

.field public static final page_identity_structured_content_primary_text:I = 0x7f090e49

.field public static final page_identity_structured_content_row0:I = 0x7f090e3e

.field public static final page_identity_structured_content_row1:I = 0x7f090e3f

.field public static final page_identity_structured_content_row2:I = 0x7f090e40

.field public static final page_identity_structured_content_row3:I = 0x7f090e41

.field public static final page_identity_structured_content_row4:I = 0x7f090e42

.field public static final page_identity_structured_content_secondary_text:I = 0x7f090e4a

.field public static final page_identity_structured_content_separator:I = 0x7f090e4c

.field public static final page_identity_structured_content_text:I = 0x7f090e47

.field public static final page_identity_structured_media_content_list:I = 0x7f090e4e

.field public static final page_identity_subtext:I = 0x7f090dda

.field public static final page_identity_text_with_entities_view:I = 0x7f090e3a

.field public static final page_identity_time_chooser:I = 0x7f090dd5

.field public static final page_identity_time_spinner:I = 0x7f090dd7

.field public static final page_identity_timeline_button:I = 0x7f090e4f

.field public static final page_identity_timeline_story:I = 0x7f090d9c

.field public static final page_identity_top_recommendations:I = 0x7f090e14

.field public static final page_identity_top_reviews_divider:I = 0x7f090e13

.field public static final page_identity_total_raters:I = 0x7f090e33

.field public static final page_identity_tv_airings_card:I = 0x7f090d79

.field public static final page_identity_tv_airings_card_stub:I = 0x7f090d78

.field public static final page_identity_tv_airings_channel_label:I = 0x7f090e5f

.field public static final page_identity_tv_airings_channel_layout:I = 0x7f090e5e

.field public static final page_identity_tv_airings_channel_text:I = 0x7f090e60

.field public static final page_identity_tv_airings_date_layout:I = 0x7f090e58

.field public static final page_identity_tv_airings_date_text:I = 0x7f090e59

.field public static final page_identity_tv_airings_date_time_horizontal_layout:I = 0x7f090e57

.field public static final page_identity_tv_airings_divider_below_description:I = 0x7f090e56

.field public static final page_identity_tv_airings_divider_under_title:I = 0x7f090e53

.field public static final page_identity_tv_airings_episode_description_text:I = 0x7f090e55

.field public static final page_identity_tv_airings_episode_id_text:I = 0x7f090e54

.field public static final page_identity_tv_airings_reminder_button:I = 0x7f090e62

.field public static final page_identity_tv_airings_separator_above_channel:I = 0x7f090e5d

.field public static final page_identity_tv_airings_separator_above_reminder_button:I = 0x7f090e61

.field public static final page_identity_tv_airings_separator_between_date_and_time:I = 0x7f090e5a

.field public static final page_identity_tv_airings_time_layout:I = 0x7f090e5b

.field public static final page_identity_tv_airings_time_text:I = 0x7f090e5c

.field public static final page_identity_tv_airings_top_header_text:I = 0x7f090e52

.field public static final page_identity_tv_airings_vertical_separator:I = 0x7f090e63

.field public static final page_identity_ugc_photo_tab:I = 0x7f090190

.field public static final page_identity_vertex_about_attributions_container:I = 0x7f090e66

.field public static final page_identity_vertex_about_chevron:I = 0x7f090e64

.field public static final page_identity_vertex_about_description:I = 0x7f090e65

.field public static final page_identity_vertex_attribution_disclaimer_text:I = 0x7f090e67

.field public static final page_identity_vertex_attributions_container:I = 0x7f090e68

.field public static final page_identity_vertex_footer_card:I = 0x7f090d86

.field public static final page_identity_vertex_footer_card_stub:I = 0x7f090d85

.field public static final page_identity_video_card_comment_icon:I = 0x7f090e72

.field public static final page_identity_video_card_comment_text:I = 0x7f090e73

.field public static final page_identity_video_card_container:I = 0x7f090e6c

.field public static final page_identity_video_card_cover_image:I = 0x7f090e6d

.field public static final page_identity_video_card_feedback:I = 0x7f090e6f

.field public static final page_identity_video_card_like_icon:I = 0x7f090e70

.field public static final page_identity_video_card_like_text:I = 0x7f090e71

.field public static final page_identity_video_card_play_icon:I = 0x7f090e6e

.field public static final page_identity_videos_card:I = 0x7f090d7b

.field public static final page_identity_videos_card_stub:I = 0x7f090d7a

.field public static final page_identity_videos_chevron_header:I = 0x7f090e69

.field public static final page_identity_videos_chevron_header_text:I = 0x7f090e6a

.field public static final page_identity_view_pager:I = 0x7f09075f

.field public static final page_identity_viewer_profile_pic:I = 0x7f090e19

.field public static final page_identity_viewer_review:I = 0x7f090d77

.field public static final page_identity_viewer_review_stub:I = 0x7f090d76

.field public static final page_identity_were_here_container:I = 0x7f090daf

.field public static final page_identity_were_here_count:I = 0x7f090db0

.field public static final page_identity_were_here_description_text:I = 0x7f090db1

.field public static final page_identity_write_review_button:I = 0x7f090e1f

.field public static final page_image:I = 0x7f0909dc

.field public static final page_indicator:I = 0x7f0909bc

.field public static final page_indicator_off:I = 0x7f09113e

.field public static final page_indicator_on:I = 0x7f09113d

.field public static final page_info_action_sheet:I = 0x7f090142

.field public static final page_info_hours:I = 0x7f090140

.field public static final page_info_report_problem:I = 0x7f090146

.field public static final page_info_sections_container:I = 0x7f090143

.field public static final page_info_suggest_edit:I = 0x7f090144

.field public static final page_information_action_sheet_send_email_button:I = 0x7f090e74

.field public static final page_information_action_sheet_view_site_button:I = 0x7f090e75

.field public static final page_information_attribution_footer:I = 0x7f090145

.field public static final page_information_business_info_card:I = 0x7f090141

.field public static final page_information_business_info_divider_first:I = 0x7f090e77

.field public static final page_information_business_info_divider_second:I = 0x7f090e79

.field public static final page_information_container:I = 0x7f09013f

.field public static final page_information_description_field_multiline_content:I = 0x7f090e8f

.field public static final page_information_description_field_oneline_content:I = 0x7f090e8e

.field public static final page_information_description_field_title:I = 0x7f090e8d

.field public static final page_information_description_unit:I = 0x7f090e8b

.field public static final page_information_end_of_content_marker:I = 0x7f090149

.field public static final page_information_hours_card_days:I = 0x7f090e85

.field public static final page_information_hours_card_days_darker:I = 0x7f090e86

.field public static final page_information_hours_card_hours:I = 0x7f090e83

.field public static final page_information_hours_card_hours_darker:I = 0x7f090e84

.field public static final page_information_hours_card_hours_darker_first_row:I = 0x7f090e89

.field public static final page_information_hours_card_hours_first_row:I = 0x7f090e88

.field public static final page_information_hours_card_hours_row:I = 0x7f090e87

.field public static final page_information_hours_horizontal_divider:I = 0x7f090e8a

.field public static final page_information_hours_list:I = 0x7f090e81

.field public static final page_information_hours_timezone:I = 0x7f090e82

.field public static final page_information_info_field_paragraph_content:I = 0x7f090e91

.field public static final page_information_info_field_paragraph_header:I = 0x7f090e90

.field public static final page_information_info_section_field:I = 0x7f090e92

.field public static final page_information_loading_error:I = 0x7f090148

.field public static final page_information_loading_progress:I = 0x7f090147

.field public static final page_information_payment_option_amex:I = 0x7f090e7c

.field public static final page_information_payment_option_cash_only:I = 0x7f090e7b

.field public static final page_information_payment_option_discover:I = 0x7f090e7f

.field public static final page_information_payment_option_mastercard:I = 0x7f090e7d

.field public static final page_information_payment_option_visa:I = 0x7f090e7e

.field public static final page_information_payment_options:I = 0x7f090e7a

.field public static final page_information_report_problem_textview:I = 0x7f090e93

.field public static final page_information_scrollview:I = 0x7f09013e

.field public static final page_information_service_item:I = 0x7f090e94

.field public static final page_information_service_item_container:I = 0x7f090e95

.field public static final page_information_service_item_left:I = 0x7f090e97

.field public static final page_information_service_item_right:I = 0x7f090e98

.field public static final page_information_service_item_text:I = 0x7f090e96

.field public static final page_information_services:I = 0x7f090e78

.field public static final page_information_short_description_fields_card:I = 0x7f090e80

.field public static final page_information_specialties_parking:I = 0x7f090e76

.field public static final page_information_suggest_edit_button:I = 0x7f090e99

.field public static final page_information_title_and_one_line_value_container:I = 0x7f090e8c

.field public static final page_inline_friend_inviter_invitation_sent:I = 0x7f090d3c

.field public static final page_inline_friend_inviter_invite_button:I = 0x7f090d3d

.field public static final page_inline_friend_inviter_name:I = 0x7f090d3b

.field public static final page_inline_friend_inviter_photo:I = 0x7f090d3a

.field public static final page_opentable_contact_info_view:I = 0x7f090dc7

.field public static final page_opentable_reservation_view:I = 0x7f090dcf

.field public static final page_product_image_link:I = 0x7f090e9a

.field public static final page_product_image_pressed:I = 0x7f090e9b

.field public static final page_review_survey_blacklist_item:I = 0x7f090ea1

.field public static final page_review_survey_item:I = 0x7f090e9c

.field public static final page_review_survey_item_cover_photo:I = 0x7f090e9e

.field public static final page_review_survey_item_header:I = 0x7f090e9d

.field public static final page_review_survey_item_menu_button:I = 0x7f090eb0

.field public static final page_review_survey_item_privacy_selector_arrow:I = 0x7f090ea9

.field public static final page_review_survey_item_privacy_selector_icon:I = 0x7f090ea8

.field public static final page_review_survey_item_profile_pic:I = 0x7f090ea0

.field public static final page_review_survey_item_profile_pic_container:I = 0x7f090e9f

.field public static final page_review_survey_item_rating_bar:I = 0x7f090ea7

.field public static final page_review_survey_item_review:I = 0x7f090ead

.field public static final page_review_survey_item_review_actor_image:I = 0x7f090eae

.field public static final page_review_survey_item_review_actor_name:I = 0x7f090eaf

.field public static final page_review_survey_item_review_comment:I = 0x7f090eb2

.field public static final page_review_survey_item_review_privacy:I = 0x7f090eb4

.field public static final page_review_survey_item_review_rating:I = 0x7f090eb1

.field public static final page_review_survey_item_review_time:I = 0x7f090eb3

.field public static final page_review_survey_item_star_privacy_container:I = 0x7f090ea6

.field public static final page_review_survey_item_subtitle1:I = 0x7f090ea3

.field public static final page_review_survey_item_subtitle2:I = 0x7f090ea4

.field public static final page_review_survey_item_survey:I = 0x7f090ea5

.field public static final page_review_survey_item_survey_separator:I = 0x7f090eaa

.field public static final page_review_survey_item_text:I = 0x7f090eac

.field public static final page_review_survey_item_text_container:I = 0x7f090eab

.field public static final page_review_survey_item_title:I = 0x7f090ea2

.field public static final page_selector_error_view:I = 0x7f09036f

.field public static final page_suggestion_on_page_like:I = 0x7f090d65

.field public static final page_suggestion_on_page_like_stub:I = 0x7f090d64

.field public static final pager_dots_indicator:I = 0x7f090f85

.field public static final pager_frame:I = 0x7f0911ec

.field public static final pages_browser_explore_more:I = 0x7f090ebd

.field public static final pages_browser_explore_more_title:I = 0x7f090ebe

.field public static final pages_browser_pages_list:I = 0x7f090ebc

.field public static final pages_browser_section_name:I = 0x7f090ec6

.field public static final pages_browser_section_see_more:I = 0x7f090ec8

.field public static final pages_browser_sections_list:I = 0x7f090ebb

.field public static final pages_browser_suggestion_item:I = 0x7f090ec9

.field public static final pages_browser_suggestion_item_extra:I = 0x7f090ec4

.field public static final pages_browser_suggestion_item_image:I = 0x7f090ec0

.field public static final pages_browser_suggestion_item_like_button:I = 0x7f090ec5

.field public static final pages_browser_suggestion_item_like_button_container:I = 0x7f090ec2

.field public static final pages_browser_suggestion_item_subtitle:I = 0x7f090ec3

.field public static final pages_browser_suggestion_item_textbox:I = 0x7f090ec1

.field public static final pages_browser_suggestion_item_title:I = 0x7f090ebf

.field public static final pages_browser_suggestions_viewpager:I = 0x7f090ec7

.field public static final pages_identity_photo_widget_owner:I = 0x7f090ddd

.field public static final pages_identity_photo_widget_photo:I = 0x7f090ddb

.field public static final pages_identity_photo_widget_privacy_icon:I = 0x7f090ddf

.field public static final pages_identity_photo_widget_timestamp:I = 0x7f090dde

.field public static final pages_indicator:I = 0x7f0901ad

.field public static final pages_timeline_list:I = 0x7f090760

.field public static final pandora_benny_listview:I = 0x7f0900f5

.field public static final pandora_feed_listview:I = 0x7f0900f1

.field public static final pandora_friend_listview:I = 0x7f0900f2

.field public static final pandora_header_text:I = 0x7f090ecf

.field public static final pandora_pivotunits_listview:I = 0x7f0900f3

.field public static final pandora_thumbnail_position:I = 0x7f0900ef

.field public static final pandora_uploaded_mediaset_listview:I = 0x7f0900f4

.field public static final pandora_view_pager:I = 0x7f0900f0

.field public static final panel_press_state_overlay:I = 0x7f090a1b

.field public static final passcode:I = 0x7f090464

.field public static final passcode_view:I = 0x7f090472

.field public static final password:I = 0x7f09041b

.field public static final payment_amount:I = 0x7f090ee7

.field public static final payment_bottom_divider:I = 0x7f090ee9

.field public static final payment_button:I = 0x7f090ccc

.field public static final payment_card_info_text:I = 0x7f090edd

.field public static final payment_card_text:I = 0x7f0904c6

.field public static final payment_edit_text:I = 0x7f090cc9

.field public static final payment_info_header:I = 0x7f090ce7

.field public static final payment_info_text:I = 0x7f09119c

.field public static final payment_input_container:I = 0x7f090cc8

.field public static final payment_methods_button:I = 0x7f090eeb

.field public static final payment_pin_creation_pager:I = 0x7f090ee1

.field public static final payment_profile_picture:I = 0x7f090ee4

.field public static final payment_transaction_history_activity:I = 0x7f090ee2

.field public static final payment_transaction_history_button:I = 0x7f090eea

.field public static final payment_transaction_history_pager:I = 0x7f090ee3

.field public static final payment_transaction_loading_view:I = 0x7f090ee8

.field public static final payment_user_name:I = 0x7f090ee5

.field public static final payment_value_edit_text:I = 0x7f0904c5

.field public static final pending_operation_overlay:I = 0x7f09049d

.field public static final people_filter:I = 0x7f090fc2

.field public static final people_filter_icon:I = 0x7f090fc3

.field public static final people_filter_text_hint:I = 0x7f0907a0

.field public static final people_search_container:I = 0x7f09086f

.field public static final people_tab:I = 0x7f09007e

.field public static final people_you_may_know_header_divider:I = 0x7f09093b

.field public static final perf_stub:I = 0x7f09044b

.field public static final permalink_add_photo_button:I = 0x7f0905d9

.field public static final permalink_add_reply_text_field:I = 0x7f0905db

.field public static final permalink_comment_container:I = 0x7f0905f1

.field public static final permalink_comment_container_background:I = 0x7f0905f0

.field public static final permalink_comment_container_ghost:I = 0x7f0905f2

.field public static final permalink_container:I = 0x7f090762

.field public static final permalink_image_preview_badge:I = 0x7f0905d8

.field public static final permalink_image_preview_frame:I = 0x7f0905d6

.field public static final permalink_image_preview_image:I = 0x7f0905d7

.field public static final permalink_likes_container:I = 0x7f0905f3

.field public static final permalink_likes_extra_padding_for_last_item:I = 0x7f0905f5

.field public static final permalink_likes_text:I = 0x7f0905f4

.field public static final permalink_seen_by_text:I = 0x7f0905fb

.field public static final permanent_cards_container:I = 0x7f090997

.field public static final person_card_actionbar:I = 0x7f090ef7

.field public static final person_card_context_item_list:I = 0x7f090ef8

.field public static final person_card_contextual_items_error:I = 0x7f090ef0

.field public static final person_card_contextual_items_error_retry_button:I = 0x7f090ef1

.field public static final person_card_contextual_items_error_stub:I = 0x7f090eef

.field public static final person_card_contextual_items_list:I = 0x7f090eec

.field public static final person_card_contextual_items_loading:I = 0x7f090eed

.field public static final person_card_contextual_items_loading_static:I = 0x7f090eee

.field public static final person_card_footer:I = 0x7f090ef9

.field public static final person_card_header:I = 0x7f090ef6

.field public static final person_you_may_know_add_friend:I = 0x7f090fd7

.field public static final person_you_may_know_display_name:I = 0x7f090fd4

.field public static final person_you_may_know_extra_text:I = 0x7f090fd5

.field public static final person_you_may_know_text_add_friend:I = 0x7f090fd6

.field public static final person_you_may_know_user_image:I = 0x7f090fd3

.field public static final phone:I = 0x7f0901e2

.field public static final phone_number:I = 0x7f09029b

.field public static final phone_overlay:I = 0x7f0901e3

.field public static final phone_wrapper:I = 0x7f0901e1

.field public static final photo:I = 0x7f09054f

.field public static final photo_camera_box:I = 0x7f09026c

.field public static final photo_caption:I = 0x7f09039f

.field public static final photo_caption_gradient:I = 0x7f090283

.field public static final photo_capture_button:I = 0x7f09025c

.field public static final photo_chaining_item_image:I = 0x7f090eff

.field public static final photo_choose_button:I = 0x7f09025e

.field public static final photo_chrome:I = 0x7f09039c

.field public static final photo_count_container:I = 0x7f090c19

.field public static final photo_count_text:I = 0x7f090c1a

.field public static final photo_fbid:I = 0x7f0900cf

.field public static final photo_frame:I = 0x7f09054e

.field public static final photo_gallery:I = 0x7f090385

.field public static final photo_gallery_container:I = 0x7f09039b

.field public static final photo_gallery_option:I = 0x7f090274

.field public static final photo_gallery_view_pager:I = 0x7f090f00

.field public static final photo_image:I = 0x7f090c17

.field public static final photo_information_box:I = 0x7f09039d

.field public static final photo_owner_name:I = 0x7f09039e

.field public static final photo_privacy_icon:I = 0x7f0903a2

.field public static final photo_tags:I = 0x7f090282

.field public static final photo_time_and_privacy_container:I = 0x7f0903a0

.field public static final photo_timestamp:I = 0x7f0903a1

.field public static final photo_video_selector:I = 0x7f09026f

.field public static final photo_view_layout:I = 0x7f090c09

.field public static final photos_list:I = 0x7f09017b

.field public static final photos_search_container:I = 0x7f090870

.field public static final photos_tab_activity_container:I = 0x7f090f03

.field public static final picker_container:I = 0x7f090b90

.field public static final picker_grid:I = 0x7f0909e3

.field public static final picture_menu:I = 0x7f090266

.field public static final pin:I = 0x7f090462

.field public static final pin_creation_header:I = 0x7f090ede

.field public static final pin_digits:I = 0x7f090edf

.field public static final pin_icons:I = 0x7f090ee0

.field public static final pin_item_0:I = 0x7f091370

.field public static final pin_item_1:I = 0x7f091371

.field public static final pin_item_2:I = 0x7f091372

.field public static final pin_item_3:I = 0x7f091373

.field public static final pinterest_button:I = 0x7f0901b5

.field public static final pivotHolder:I = 0x7f090741

.field public static final pivot_container:I = 0x7f091165

.field public static final pivot_text:I = 0x7f090591

.field public static final pivot_view_title:I = 0x7f090f08

.field public static final pivots_dismiss_bar:I = 0x7f09024a

.field public static final pivots_drag_handle:I = 0x7f09024b

.field public static final pivots_list_pager:I = 0x7f09024c

.field public static final pivots_show_button:I = 0x7f090247

.field public static final pivots_top_divider:I = 0x7f090249

.field public static final place_address:I = 0x7f090f1d

.field public static final place_address_type:I = 0x7f090f1e

.field public static final place_average_rating_blue:I = 0x7f090a83

.field public static final place_card_divider:I = 0x7f090f0e

.field public static final place_card_title:I = 0x7f090f0d

.field public static final place_category_list:I = 0x7f090f40

.field public static final place_category_text:I = 0x7f090f19

.field public static final place_city:I = 0x7f090f1f

.field public static final place_context_wrapper:I = 0x7f090a81

.field public static final place_creation_dup:I = 0x7f090f20

.field public static final place_description:I = 0x7f091109

.field public static final place_distance:I = 0x7f090a7e

.field public static final place_edit_icon:I = 0x7f090f2b

.field public static final place_facepile:I = 0x7f090a80

.field public static final place_global_context:I = 0x7f090a86

.field public static final place_image:I = 0x7f090a1d

.field public static final place_info:I = 0x7f090a87

.field public static final place_label:I = 0x7f09096d

.field public static final place_left_items:I = 0x7f090f2a

.field public static final place_name:I = 0x7f090a7f

.field public static final place_phone:I = 0x7f090f1b

.field public static final place_picker_footer:I = 0x7f090f21

.field public static final place_picker_list_container:I = 0x7f090f24

.field public static final place_pin_badge_general:I = 0x7f090f28

.field public static final place_pin_badge_success:I = 0x7f090f29

.field public static final place_profile_picture:I = 0x7f090f0c

.field public static final place_rating_bar_container:I = 0x7f090a82

.field public static final place_saved_icon:I = 0x7f090a84

.field public static final place_saved_text:I = 0x7f090a85

.field public static final place_social_context:I = 0x7f090a88

.field public static final place_text_container:I = 0x7f090f2c

.field public static final place_thumbnail:I = 0x7f091105

.field public static final place_title:I = 0x7f090aa6

.field public static final place_website:I = 0x7f090f1c

.field public static final placeholder:I = 0x7f090001

.field public static final places_map_fragment:I = 0x7f090f41

.field public static final plaintext_guest_summary:I = 0x7f0904f0

.field public static final platform_dialog_title:I = 0x7f090f43

.field public static final platform_dialog_webview:I = 0x7f090f44

.field public static final play_button:I = 0x7f090486

.field public static final play_pause_animation_icon:I = 0x7f090815

.field public static final plus:I = 0x7f0906cf

.field public static final plutonium_covermedia_albums_label:I = 0x7f090f49

.field public static final plutonium_covermedia_image:I = 0x7f090f45

.field public static final plutonium_covermedia_photos_label:I = 0x7f090f46

.field public static final plutonium_covermedia_progress_bar:I = 0x7f090f48

.field public static final plutonium_covermedia_separator:I = 0x7f090f47

.field public static final plutonium_edit_cover_photo_icon:I = 0x7f090f5b

.field public static final plutonium_edit_cover_photo_icon_stub:I = 0x7f090f5a

.field public static final plutonium_profile_photo_name:I = 0x7f090f4a

.field public static final plutonium_profile_question_buttons_line:I = 0x7f090f51

.field public static final plutonium_profile_question_header_icon:I = 0x7f090f4d

.field public static final plutonium_profile_question_left_button:I = 0x7f090f54

.field public static final plutonium_profile_question_options:I = 0x7f090f50

.field public static final plutonium_profile_question_privacy_selector:I = 0x7f090f52

.field public static final plutonium_profile_question_right_button:I = 0x7f090f55

.field public static final plutonium_profile_question_spinner_block:I = 0x7f090f53

.field public static final plutonium_profile_question_subtitle_text:I = 0x7f090f4f

.field public static final plutonium_profile_question_title_text:I = 0x7f090f4e

.field public static final plutonium_timeline_context_item_badge_icon:I = 0x7f0903b7

.field public static final plutonium_timeline_context_item_icon:I = 0x7f0903b4

.field public static final plutonium_timeline_context_item_subtitle:I = 0x7f0903b6

.field public static final plutonium_timeline_context_item_title:I = 0x7f0903b5

.field public static final plutonium_timeline_image_block_layout:I = 0x7f0903b3

.field public static final pocket:I = 0x7f0901e4

.field public static final pogs_container:I = 0x7f090a06

.field public static final popout_gallery:I = 0x7f090bc5

.field public static final popover_button:I = 0x7f090857

.field public static final popover_container:I = 0x7f090765

.field public static final popover_header:I = 0x7f090446

.field public static final popover_message:I = 0x7f090856

.field public static final popup_menu_anchor:I = 0x7f090b5e

.field public static final popup_menu_button:I = 0x7f090b45

.field public static final position_text:I = 0x7f090fa7

.field public static final post_post_close_button:I = 0x7f0911f0

.field public static final post_post_dots:I = 0x7f0911ee

.field public static final post_post_dots_count:I = 0x7f0911ef

.field public static final post_post_header_divider:I = 0x7f0911ea

.field public static final post_post_meta:I = 0x7f0911eb

.field public static final post_post_title:I = 0x7f0911e9

.field public static final postplay_play_icon:I = 0x7f090812

.field public static final postplay_suggested_videos_view:I = 0x7f090f5d

.field public static final premium_video_current_gallery_item:I = 0x7f0900b3

.field public static final premium_video_image:I = 0x7f090f5f

.field public static final premium_video_pause_cover:I = 0x7f090f60

.field public static final premium_videos_inline_video_player:I = 0x7f090603

.field public static final premium_videos_item_header:I = 0x7f0905fc

.field public static final premium_videos_pager:I = 0x7f0905fe

.field public static final premium_videos_player_attachment:I = 0x7f0905ff

.field public static final premium_videos_unit_actor_profile_pic:I = 0x7f090600

.field public static final premium_videos_unit_actor_title:I = 0x7f090601

.field public static final premium_videos_unit_body:I = 0x7f0905fd

.field public static final premium_videos_unit_sponsored_text:I = 0x7f090602

.field public static final presence_feed_unit_active_view:I = 0x7f090f67

.field public static final presence_feed_unit_footer_view:I = 0x7f090f68

.field public static final presence_feed_unit_sell_all_text:I = 0x7f090f66

.field public static final presence_icon:I = 0x7f090f65

.field public static final presence_user_image:I = 0x7f090f63

.field public static final presence_user_name:I = 0x7f090f64

.field public static final prev_button:I = 0x7f090a3a

.field public static final prev_glyph:I = 0x7f090a4d

.field public static final preview:I = 0x7f090c6b

.field public static final preview_animator:I = 0x7f090897

.field public static final preview_bottom_divider:I = 0x7f0908a1

.field public static final preview_frame:I = 0x7f090896

.field public static final preview_image_date:I = 0x7f090f72

.field public static final preview_image_first:I = 0x7f090898

.field public static final preview_image_second:I = 0x7f090899

.field public static final preview_image_time:I = 0x7f090f71

.field public static final preview_image_weather:I = 0x7f090f73

.field public static final preview_launcher:I = 0x7f090f74

.field public static final preview_launcher_image:I = 0x7f090f75

.field public static final preview_launcher_overlay:I = 0x7f090f76

.field public static final preview_layout:I = 0x7f0908a0

.field public static final preview_lock:I = 0x7f090f6d

.field public static final preview_lock_image:I = 0x7f090f6e

.field public static final preview_lock_overlay:I = 0x7f090f6f

.field public static final preview_padding_bar:I = 0x7f090262

.field public static final preview_padding_side_1:I = 0x7f090263

.field public static final preview_padding_side_2:I = 0x7f090264

.field public static final price:I = 0x7f090c4b

.field public static final primary_action_button:I = 0x7f090129

.field public static final primary_action_button1:I = 0x7f09012e

.field public static final primary_action_button_divider:I = 0x7f090128

.field public static final primary_action_button_divider1:I = 0x7f09012d

.field public static final primary_action_progress:I = 0x7f09012a

.field public static final primary_button:I = 0x7f090a7b

.field public static final primary_named_button:I = 0x7f090127

.field public static final primary_named_button1:I = 0x7f09012c

.field public static final primary_named_button_divider:I = 0x7f090126

.field public static final primary_named_button_divider1:I = 0x7f09012b

.field public static final primary_text_view:I = 0x7f0910d7

.field public static final privacy_buttons_bar:I = 0x7f0901ca

.field public static final privacy_education_collapsed:I = 0x7f0904a8

.field public static final privacy_education_expanded:I = 0x7f0904a2

.field public static final privacy_header:I = 0x7f0903bf

.field public static final privacy_icon:I = 0x7f09044d

.field public static final privacy_picker:I = 0x7f0904e6

.field public static final privacy_root_view:I = 0x7f09048d

.field public static final privacy_section:I = 0x7f0904e4

.field public static final privacy_separator:I = 0x7f0904e5

.field public static final product_image:I = 0x7f091239

.field public static final product_like_text:I = 0x7f09123c

.field public static final product_price:I = 0x7f09123b

.field public static final product_title:I = 0x7f09123a

.field public static final production:I = 0x7f0900c4

.field public static final profile_card_profiles:I = 0x7f090fb7

.field public static final profile_card_title:I = 0x7f090fb6

.field public static final profile_card_view:I = 0x7f090fb5

.field public static final profile_checkbox:I = 0x7f0902c0

.field public static final profile_container:I = 0x7f0902b0

.field public static final profile_crop_overlay:I = 0x7f09027a

.field public static final profile_crop_square:I = 0x7f09027b

.field public static final profile_helper_frame:I = 0x7f090fb8

.field public static final profile_image:I = 0x7f0905af

.field public static final profile_info_item_clear_icon:I = 0x7f090750

.field public static final profile_info_item_text:I = 0x7f09074f

.field public static final profile_item_name:I = 0x7f090fc0

.field public static final profile_item_picture:I = 0x7f090fbf

.field public static final profile_list:I = 0x7f09105b

.field public static final profile_list_container:I = 0x7f090763

.field public static final profile_list_fragment:I = 0x7f090fc1

.field public static final profile_list_item_container:I = 0x7f090fbe

.field public static final profile_loading_view:I = 0x7f091050

.field public static final profile_loading_view_center:I = 0x7f09105a

.field public static final profile_name:I = 0x7f090fb2

.field public static final profile_photo:I = 0x7f090629

.field public static final profile_photo_container:I = 0x7f090ef3

.field public static final profile_photo_container_stub_drawable_hierarchy:I = 0x7f090ef4

.field public static final profile_photo_container_stub_url_image:I = 0x7f090ef2

.field public static final profile_pic:I = 0x7f0902b3

.field public static final profile_pic_container:I = 0x7f0902b2

.field public static final profile_pic_overlay:I = 0x7f09022b

.field public static final profile_pic_qcb_stub:I = 0x7f090fc5

.field public static final profile_pic_stub:I = 0x7f090fc4

.field public static final profile_question_privacy_image:I = 0x7f0908cd

.field public static final profile_question_privacy_placeholder:I = 0x7f0908cc

.field public static final profile_question_privacy_spinner:I = 0x7f0908ce

.field public static final profile_sub_name:I = 0x7f090fb3

.field public static final progress:I = 0x7f090f0a

.field public static final progress_bar:I = 0x7f090135

.field public static final progress_bar_container:I = 0x7f090cd5

.field public static final progress_circular:I = 0x7f090111

.field public static final progress_horizontal:I = 0x7f090112

.field public static final progress_message:I = 0x7f090abd

.field public static final progress_spinner:I = 0x7f090c06

.field public static final progress_thumbnail:I = 0x7f090c66

.field public static final progressbar:I = 0x7f090a63

.field public static final promo_sticker:I = 0x7f0908fb

.field public static final promotion_account_spinner:I = 0x7f0911c5

.field public static final promotion_account_title:I = 0x7f0911c4

.field public static final promotion_age:I = 0x7f0911a4

.field public static final promotion_age_max:I = 0x7f0911a5

.field public static final promotion_age_max_filler:I = 0x7f0911a6

.field public static final promotion_age_min:I = 0x7f0911a3

.field public static final promotion_audience_fof:I = 0x7f09119e

.field public static final promotion_audience_ncpp:I = 0x7f09119f

.field public static final promotion_audience_radio_group:I = 0x7f09119d

.field public static final promotion_budget_section:I = 0x7f0911b6

.field public static final promotion_budget_selector:I = 0x7f0911b7

.field public static final promotion_button:I = 0x7f0911c7

.field public static final promotion_gender_both:I = 0x7f0911a8

.field public static final promotion_gender_female:I = 0x7f0911a9

.field public static final promotion_gender_male:I = 0x7f0911aa

.field public static final promotion_gender_radio_group:I = 0x7f0911a7

.field public static final promotion_header_divider:I = 0x7f0911b4

.field public static final promotion_location:I = 0x7f0911a2

.field public static final promotion_location_edit:I = 0x7f0911a1

.field public static final promotion_pause_button:I = 0x7f0911c8

.field public static final promotion_payment_info:I = 0x7f0911c6

.field public static final promotion_progress_bar:I = 0x7f0911b8

.field public static final promotion_progress_header:I = 0x7f0911ae

.field public static final promotion_progress_reach_number:I = 0x7f0911b0

.field public static final promotion_progress_reach_text:I = 0x7f0911b1

.field public static final promotion_progress_spent_number:I = 0x7f0911b2

.field public static final promotion_progress_spent_text:I = 0x7f0911b3

.field public static final promotion_status_banner:I = 0x7f0911ab

.field public static final promotion_status_banner_text:I = 0x7f0911ac

.field public static final promotion_targeting:I = 0x7f0911c1

.field public static final promotion_targeting_details_section:I = 0x7f0911a0

.field public static final promotion_text_header:I = 0x7f0911ad

.field public static final promotion_warning:I = 0x7f0911c9

.field public static final public_privacy_button:I = 0x7f0901cb

.field public static final publish_mode_options_draft:I = 0x7f090fcd

.field public static final publish_mode_options_post:I = 0x7f090fc9

.field public static final publish_mode_options_schedule:I = 0x7f090fcb

.field public static final publish_mode_options_schedule_time:I = 0x7f090fcc

.field public static final publish_mode_options_schedule_wrapper:I = 0x7f090fca

.field public static final publish_mode_selector:I = 0x7f09033a

.field public static final publish_mode_selector_layout:I = 0x7f090fc8

.field public static final publish_mode_selector_view:I = 0x7f090395

.field public static final publish_mode_selector_view_stub:I = 0x7f090394

.field public static final publisher:I = 0x7f090c8f

.field public static final publisher_bar_container:I = 0x7f090c8d

.field public static final publisher_bar_root:I = 0x7f091208

.field public static final publisher_button0:I = 0x7f090fce

.field public static final publisher_button1:I = 0x7f090fd0

.field public static final publisher_button2:I = 0x7f090fd2

.field public static final publisher_checkin:I = 0x7f09125b

.field public static final publisher_checkin_button:I = 0x7f09125d

.field public static final publisher_checkin_divider:I = 0x7f09125a

.field public static final publisher_checkin_icon:I = 0x7f09125c

.field public static final publisher_container:I = 0x7f090742

.field public static final publisher_divider:I = 0x7f090de8

.field public static final publisher_divider0:I = 0x7f090fcf

.field public static final publisher_divider1:I = 0x7f090fd1

.field public static final publisher_more_actions:I = 0x7f09125f

.field public static final publisher_more_actions_divider:I = 0x7f09125e

.field public static final publisher_more_actions_icon:I = 0x7f091260

.field public static final publisher_photo:I = 0x7f091257

.field public static final publisher_photo_button:I = 0x7f091259

.field public static final publisher_photo_divider:I = 0x7f091256

.field public static final publisher_photo_icon:I = 0x7f091258

.field public static final publisher_shadow:I = 0x7f090747

.field public static final publisher_status:I = 0x7f091253

.field public static final publisher_status_button:I = 0x7f091255

.field public static final publisher_status_icon:I = 0x7f091254

.field public static final publishers_root:I = 0x7f090744

.field public static final pull:I = 0x7f090017

.field public static final pull_to_refresh_action_container:I = 0x7f090361

.field public static final pull_to_refresh_error_view:I = 0x7f090c27

.field public static final pull_to_refresh_image:I = 0x7f090c01

.field public static final pull_to_refresh_last_loaded_time:I = 0x7f090c26

.field public static final pull_to_refresh_list_mask:I = 0x7f090c00

.field public static final pull_to_refresh_list_shadow:I = 0x7f090bff

.field public static final pull_to_refresh_parent_container:I = 0x7f090360

.field public static final pull_to_refresh_progress:I = 0x7f090c02

.field public static final pull_to_refresh_refresh_container:I = 0x7f090362

.field public static final pull_to_refresh_spinner:I = 0x7f090bfd

.field public static final pull_to_refresh_spinner_mask:I = 0x7f090bfe

.field public static final pull_to_refresh_text_and_date_container:I = 0x7f090c20

.field public static final pull_to_refresh_text_container:I = 0x7f090c21

.field public static final pull_to_refresh_text_popup:I = 0x7f090c23

.field public static final pull_to_refresh_text_pull:I = 0x7f090c22

.field public static final pull_to_refresh_text_push:I = 0x7f090c24

.field public static final pull_to_refresh_text_refreshing:I = 0x7f090c29

.field public static final pull_to_refresh_text_refreshing_container:I = 0x7f090c28

.field public static final pull_to_refresh_text_release:I = 0x7f090c25

.field public static final push:I = 0x7f090018

.field public static final push_notification_1:I = 0x7f090ad9

.field public static final push_notification_2:I = 0x7f090ada

.field public static final push_notification_2_container:I = 0x7f090ad8

.field public static final push_notification_gray_bars:I = 0x7f0909ac

.field public static final push_notification_text:I = 0x7f0909ad

.field public static final push_notifications_not_now:I = 0x7f090adc

.field public static final push_notifications_turn_on:I = 0x7f090ade

.field public static final pymk_divider:I = 0x7f090fd8

.field public static final pymk_load_more_icon:I = 0x7f090fdb

.field public static final pymk_see_all_image_button:I = 0x7f090fd9

.field public static final pymk_see_all_link:I = 0x7f090fda

.field public static final pymk_suggestion_action_icon:I = 0x7f090efe

.field public static final pymk_suggestion_blacklist_icon:I = 0x7f090fe0

.field public static final pymk_suggestion_extra:I = 0x7f090efd

.field public static final pymk_suggestion_image:I = 0x7f090efb

.field public static final pymk_suggestion_name:I = 0x7f090efc

.field public static final pymk_suggestion_native_name:I = 0x7f090fe1

.field public static final pymk_suggestion_raw_name:I = 0x7f090fe2

.field public static final pymk_swipe_unit_end_view:I = 0x7f090fdf

.field public static final pymk_swipe_unit_end_view_stub:I = 0x7f090fde

.field public static final pymk_swipe_unit_load_view:I = 0x7f090fdd

.field public static final pymk_swipe_unit_load_view_stub:I = 0x7f090fdc

.field public static final pymk_swipe_unit_view:I = 0x7f090efa

.field public static final pymk_text_header:I = 0x7f09093a

.field public static final pyml_see_all_image_button:I = 0x7f090fe3

.field public static final pyml_see_all_link:I = 0x7f090fe4

.field public static final qp_divebar_buttons:I = 0x7f091006

.field public static final qp_multipage_promotion_pager:I = 0x7f091008

.field public static final qp_multipage_promotion_pager_indicator:I = 0x7f091009

.field public static final qr_code_image:I = 0x7f090fee

.field public static final qr_code_image_container:I = 0x7f090fed

.field public static final qr_code_layout:I = 0x7f090feb

.field public static final qr_code_login_approve:I = 0x7f090fe7

.field public static final qr_code_scan_text:I = 0x7f090ff2

.field public static final qr_code_scanner_guide:I = 0x7f090ff1

.field public static final qr_code_scanner_overlay:I = 0x7f090ff0

.field public static final qr_code_share_text:I = 0x7f090fec

.field public static final qr_login_info:I = 0x7f090fe6

.field public static final qr_login_info_title:I = 0x7f090fe5

.field public static final qrcode_action_bar:I = 0x7f090fef

.field public static final query:I = 0x7f090b68

.field public static final query_list:I = 0x7f090ff5

.field public static final query_scrollview:I = 0x7f0908a7

.field public static final quick_promotion_divebar:I = 0x7f091004

.field public static final quick_promotion_segue_container:I = 0x7f09100a

.field public static final quickpromotion_groupsfeed_footer:I = 0x7f09087e

.field public static final quickpromotion_thread_list_footer:I = 0x7f090c8e

.field public static final radial:I = 0x7f090031

.field public static final radio:I = 0x7f09010a

.field public static final radio_button:I = 0x7f09100e

.field public static final radio_button_subtitle:I = 0x7f091011

.field public static final radio_button_text_wrapper:I = 0x7f09100f

.field public static final radio_button_title:I = 0x7f091010

.field public static final radio_button_with_subtitle:I = 0x7f09100d

.field public static final rating_bar:I = 0x7f090372

.field public static final rating_bar_container:I = 0x7f090371

.field public static final rating_message:I = 0x7f090373

.field public static final rating_section_animated_image_url_image:I = 0x7f091012

.field public static final rating_section_items_card:I = 0x7f091014

.field public static final rating_section_items_card_below_picture1:I = 0x7f09101e

.field public static final rating_section_items_card_below_picture2:I = 0x7f09101d

.field public static final rating_section_items_card_below_picture3:I = 0x7f09101c

.field public static final rating_section_items_card_compose_review_button:I = 0x7f091022

.field public static final rating_section_items_card_compose_review_button_separator:I = 0x7f091020

.field public static final rating_section_items_card_header:I = 0x7f091017

.field public static final rating_section_items_card_image_stack:I = 0x7f09101b

.field public static final rating_section_items_card_privacy_icon:I = 0x7f09101a

.field public static final rating_section_items_card_privacy_icon_container:I = 0x7f091019

.field public static final rating_section_items_card_rating_bar:I = 0x7f091021

.field public static final rating_section_items_card_text:I = 0x7f091018

.field public static final rating_section_items_card_top_picture:I = 0x7f091015

.field public static final rating_section_items_card_top_picture_frame:I = 0x7f09101f

.field public static final rating_section_loading_layer:I = 0x7f091016

.field public static final rating_section_nux_arrow_container:I = 0x7f091024

.field public static final rating_section_nux_layer:I = 0x7f091023

.field public static final rating_section_nux_star_text:I = 0x7f091025

.field public static final rating_section_scroll_view:I = 0x7f091013

.field public static final rating_wrapper:I = 0x7f090378

.field public static final rating_wrapper_stub:I = 0x7f09038c

.field public static final reaction_attachment_container:I = 0x7f091047

.field public static final reaction_attachment_icon:I = 0x7f09103b

.field public static final reaction_attachment_separator:I = 0x7f091046

.field public static final reaction_card_bottom:I = 0x7f09104a

.field public static final reaction_card_container:I = 0x7f091054

.field public static final reaction_card_footer:I = 0x7f091049

.field public static final reaction_card_layout:I = 0x7f091041

.field public static final reaction_card_top:I = 0x7f091042

.field public static final reaction_done_button:I = 0x7f091059

.field public static final reaction_facepile_photo:I = 0x7f09104c

.field public static final reaction_footer_separator:I = 0x7f091048

.field public static final reaction_footer_text:I = 0x7f09104b

.field public static final reaction_gradient_body:I = 0x7f091052

.field public static final reaction_header_gradient:I = 0x7f091055

.field public static final reaction_hscroll_button_stub:I = 0x7f09102f

.field public static final reaction_hscroll_cover_photo:I = 0x7f091028

.field public static final reaction_hscroll_cover_photo_gradient:I = 0x7f091029

.field public static final reaction_hscroll_left_button:I = 0x7f09104d

.field public static final reaction_hscroll_no_action_layout:I = 0x7f091027

.field public static final reaction_hscroll_profile_picture:I = 0x7f09102c

.field public static final reaction_hscroll_profile_subtitle:I = 0x7f09102a

.field public static final reaction_hscroll_profile_title:I = 0x7f09102b

.field public static final reaction_hscroll_right_button:I = 0x7f09104e

.field public static final reaction_hscroll_wide_button:I = 0x7f09104f

.field public static final reaction_hscroll_with_actions_top:I = 0x7f09102e

.field public static final reaction_hscroll_with_actions_top_layout:I = 0x7f091030

.field public static final reaction_icon:I = 0x7f091043

.field public static final reaction_message:I = 0x7f091044

.field public static final reaction_overlay_container:I = 0x7f091051

.field public static final reaction_overlay_header:I = 0x7f091056

.field public static final reaction_photo:I = 0x7f091032

.field public static final reaction_photo_container:I = 0x7f091031

.field public static final reaction_photo_overlay:I = 0x7f091033

.field public static final reaction_photos:I = 0x7f091034

.field public static final reaction_profile_context:I = 0x7f091038

.field public static final reaction_profile_picture:I = 0x7f091035

.field public static final reaction_profile_row:I = 0x7f09105c

.field public static final reaction_profile_story_picture:I = 0x7f091039

.field public static final reaction_profile_story_text:I = 0x7f09103a

.field public static final reaction_profile_subtitle:I = 0x7f091037

.field public static final reaction_profile_timestamp:I = 0x7f09103c

.field public static final reaction_profile_title:I = 0x7f091036

.field public static final reaction_scroll_view:I = 0x7f091053

.field public static final reaction_single_photo:I = 0x7f09103d

.field public static final reaction_single_photo_message:I = 0x7f09103f

.field public static final reaction_single_photo_name:I = 0x7f09103e

.field public static final reaction_single_photo_timestamp:I = 0x7f091040

.field public static final reaction_summary:I = 0x7f091045

.field public static final reaction_title_label:I = 0x7f091058

.field public static final realtabcontent:I = 0x7f0904ec

.field public static final receipt_amount:I = 0x7f09105f

.field public static final receipt_container:I = 0x7f090c36

.field public static final receipt_dollar_icon:I = 0x7f09105e

.field public static final receipt_header_title:I = 0x7f091060

.field public static final receipt_icon:I = 0x7f090bf5

.field public static final receipt_payment_card_icon:I = 0x7f091061

.field public static final receipt_payment_card_used:I = 0x7f091062

.field public static final receipt_payment_id:I = 0x7f09110b

.field public static final receipt_status_icon:I = 0x7f091063

.field public static final receipt_status_text:I = 0x7f091064

.field public static final receipt_text:I = 0x7f090bf6

.field public static final receipt_time:I = 0x7f09110a

.field public static final receiver_name:I = 0x7f0904c4

.field public static final receiver_tile_img:I = 0x7f0904c3

.field public static final recenter_button:I = 0x7f090a96

.field public static final recommendation_image:I = 0x7f090df8

.field public static final recommendation_images:I = 0x7f09023d

.field public static final recommendation_list_empty_view:I = 0x7f090eb6

.field public static final recommendation_rating:I = 0x7f090dfa

.field public static final recommendation_secondary_action_button:I = 0x7f090df6

.field public static final recommendations_list:I = 0x7f090eb5

.field public static final recommendations_section:I = 0x7f09023a

.field public static final recommendations_title:I = 0x7f09023c

.field public static final recommendations_view_stub:I = 0x7f090239

.field public static final record_voice_button:I = 0x7f090ab1

.field public static final red_background_animation:I = 0x7f090b1c

.field public static final redial_accept:I = 0x7f091334

.field public static final redial_cancel:I = 0x7f091333

.field public static final redial_row_buttons:I = 0x7f091332

.field public static final redial_row_stub:I = 0x7f091345

.field public static final reflex_root:I = 0x7f09106f

.field public static final refresh:I = 0x7f090358

.field public static final refresh_button:I = 0x7f09029d

.field public static final refresh_loading_spinner:I = 0x7f09029e

.field public static final refreshing_spinner:I = 0x7f0912ec

.field public static final reg_steps_view_pager:I = 0x7f090ab3

.field public static final registration_button_next:I = 0x7f091074

.field public static final registration_button_secondary:I = 0x7f091076

.field public static final registration_contact_email_group:I = 0x7f09107d

.field public static final registration_contact_phone_group:I = 0x7f09107a

.field public static final registration_country_spinner:I = 0x7f09107b

.field public static final registration_minor_button_group:I = 0x7f091075

.field public static final registration_step_contact_email_input:I = 0x7f09107e

.field public static final registration_step_contact_phone_input:I = 0x7f09107c

.field public static final registration_step_description:I = 0x7f091071

.field public static final registration_step_error:I = 0x7f091072

.field public static final registration_step_gender_female:I = 0x7f091080

.field public static final registration_step_gender_male:I = 0x7f091081

.field public static final registration_step_name_first_name:I = 0x7f091082

.field public static final registration_step_name_last_name:I = 0x7f091083

.field public static final registration_step_password_input:I = 0x7f091084

.field public static final registration_step_progress:I = 0x7f09107f

.field public static final registration_step_title:I = 0x7f091070

.field public static final regular:I = 0x7f090025

.field public static final related_video_preview:I = 0x7f09108c

.field public static final related_videos_carousel_dim_layer:I = 0x7f09108b

.field public static final related_videos_carousel_item_cover_image:I = 0x7f09108d

.field public static final related_videos_carousel_item_like_container:I = 0x7f09108f

.field public static final related_videos_carousel_item_like_count:I = 0x7f091090

.field public static final related_videos_carousel_item_title:I = 0x7f09108e

.field public static final related_videos_carousel_list:I = 0x7f09108a

.field public static final related_videos_carousel_loading_shimmer:I = 0x7f091088

.field public static final related_videos_carousel_scroll:I = 0x7f091089

.field public static final related_videos_carousel_scroll_container:I = 0x7f091087

.field public static final related_videos_carousel_title:I = 0x7f091085

.field public static final related_videos_carousel_toggle_button:I = 0x7f091086

.field public static final reloading_event_progress_bar:I = 0x7f09050c

.field public static final remote_peer_view:I = 0x7f09133c

.field public static final remove_account:I = 0x7f090458

.field public static final remove_account_message:I = 0x7f090459

.field public static final remove_button:I = 0x7f0912dc

.field public static final remove_checkin:I = 0x7f090f39

.field public static final remove_filter_button:I = 0x7f09085c

.field public static final remove_item_button:I = 0x7f090917

.field public static final remove_passcode:I = 0x7f090465

.field public static final remove_people:I = 0x7f09139f

.field public static final remove_photo:I = 0x7f091379

.field public static final remove_pin:I = 0x7f090457

.field public static final replies_arrow:I = 0x7f0912ae

.field public static final replies_header_text:I = 0x7f0912af

.field public static final replies_separator:I = 0x7f0912ad

.field public static final replies_switch_view_holder:I = 0x7f0912ac

.field public static final report_problem_setting:I = 0x7f09111d

.field public static final request_button:I = 0x7f0908dd

.field public static final request_footer_text:I = 0x7f0908e1

.field public static final request_note_edit_text:I = 0x7f0908dc

.field public static final request_title:I = 0x7f0908db

.field public static final requester_display_name:I = 0x7f090789

.field public static final requester_social_context:I = 0x7f09078a

.field public static final requester_user_image:I = 0x7f090788

.field public static final requests_view_fragment:I = 0x7f090793

.field public static final research_poll_answers_container:I = 0x7f0910a6

.field public static final research_poll_checkboxes_container_stub:I = 0x7f09109f

.field public static final research_poll_question_checkbox_hint:I = 0x7f091092

.field public static final research_poll_question_text:I = 0x7f0910a5

.field public static final research_poll_radio_buttons_container_stub:I = 0x7f09109e

.field public static final research_poll_results_item_answer:I = 0x7f0910a8

.field public static final research_poll_results_item_percentage:I = 0x7f0910a7

.field public static final research_poll_results_item_progressbar:I = 0x7f0910a9

.field public static final research_poll_results_stub:I = 0x7f0910a2

.field public static final research_poll_thank_you:I = 0x7f0910a3

.field public static final research_poll_thank_you_text:I = 0x7f0910a4

.field public static final research_poll_unit_actor_profile_pic:I = 0x7f091095

.field public static final research_poll_unit_call_to_action:I = 0x7f091098

.field public static final research_poll_unit_call_to_action_icon:I = 0x7f09109d

.field public static final research_poll_unit_call_to_action_icon_divider:I = 0x7f09109a

.field public static final research_poll_unit_call_to_action_subtitle:I = 0x7f09109c

.field public static final research_poll_unit_call_to_action_text_container:I = 0x7f091099

.field public static final research_poll_unit_call_to_action_title:I = 0x7f09109b

.field public static final research_poll_unit_container:I = 0x7f091093

.field public static final research_poll_unit_header_container:I = 0x7f091094

.field public static final research_poll_unit_subtitle:I = 0x7f091097

.field public static final research_poll_unit_title:I = 0x7f091096

.field public static final research_poll_vote_button:I = 0x7f0910a0

.field public static final research_poll_vote_button_text:I = 0x7f0910a1

.field public static final resend_code_button:I = 0x7f090123

.field public static final restart:I = 0x7f09002a

.field public static final restore_list_button:I = 0x7f090a95

.field public static final restore_list_button_nux:I = 0x7f090a97

.field public static final result_pager:I = 0x7f09076f

.field public static final results_list:I = 0x7f090a21

.field public static final retryButton:I = 0x7f090a6a

.field public static final retry_button:I = 0x7f09096a

.field public static final retry_message:I = 0x7f090aff

.field public static final retry_separator:I = 0x7f090afc

.field public static final reverse:I = 0x7f09002b

.field public static final review_actor_profile_picture:I = 0x7f0910aa

.field public static final review_compose_icon:I = 0x7f0908e3

.field public static final review_feedback_comment_icon:I = 0x7f0910b4

.field public static final review_feedback_comment_text:I = 0x7f0910b0

.field public static final review_feedback_container:I = 0x7f0910ad

.field public static final review_feedback_icons_separator:I = 0x7f0910b3

.field public static final review_feedback_like_icon:I = 0x7f0910b2

.field public static final review_feedback_like_text:I = 0x7f0910ae

.field public static final review_feedback_text_icons_separator:I = 0x7f0910b1

.field public static final review_feedback_text_separator:I = 0x7f0910af

.field public static final review_main_content:I = 0x7f0910ab

.field public static final review_message_text:I = 0x7f09038e

.field public static final review_page_name:I = 0x7f0910ba

.field public static final review_page_profile_picture:I = 0x7f0910b9

.field public static final review_rating:I = 0x7f0910bb

.field public static final review_secondary_action:I = 0x7f0910b5

.field public static final review_subtitle:I = 0x7f0910ac

.field public static final review_text:I = 0x7f0910bc

.field public static final reviews_collection_item_icon:I = 0x7f0902fe

.field public static final reviews_collection_item_rating:I = 0x7f090300

.field public static final reviews_collection_item_text:I = 0x7f090301

.field public static final reviews_collection_item_title:I = 0x7f0902ff

.field public static final reviews_inline_composer_profile_pic:I = 0x7f0910b6

.field public static final reviews_inline_composer_title:I = 0x7f0910b7

.field public static final ridge_disabled:I = 0x7f090a2b

.field public static final ridge_init_text:I = 0x7f0910d1

.field public static final ridge_interstitial_tv_image:I = 0x7f0910c3

.field public static final ridge_megaphone_view:I = 0x7f090352

.field public static final ridge_nux_explanation_area:I = 0x7f0910c6

.field public static final ridge_nux_explanation_item_explanation:I = 0x7f0910bf

.field public static final ridge_nux_explanation_item_heading:I = 0x7f0910be

.field public static final ridge_nux_explanation_item_icon:I = 0x7f0910bd

.field public static final ridge_nux_learn_more_button:I = 0x7f0910c2

.field public static final ridge_nux_page_one:I = 0x7f0910c1

.field public static final ridge_nux_page_two:I = 0x7f0910c5

.field public static final ridge_nux_pager:I = 0x7f0910c0

.field public static final ridge_nux_phone_layout_actual_result:I = 0x7f0910c8

.field public static final ridge_nux_phone_layout_fake_results:I = 0x7f0910c9

.field public static final ridge_nux_phone_layout_results:I = 0x7f0910c7

.field public static final ridge_nux_phone_view:I = 0x7f0910c4

.field public static final ridge_prompt_blank:I = 0x7f0910d0

.field public static final ridge_prompt_text:I = 0x7f0910d2

.field public static final ridge_title_bar:I = 0x7f09034d

.field public static final ridge_title_bar_stub:I = 0x7f09034c

.field public static final ridge_widget_dot_grid:I = 0x7f0910ce

.field public static final ridge_widget_microphone_image:I = 0x7f0910cd

.field public static final ridge_widget_progress_bar:I = 0x7f0910cc

.field public static final ridge_widget_text_flipper:I = 0x7f0910cf

.field public static final right:I = 0x7f09000d

.field public static final rightOfAnchor:I = 0x7f090072

.field public static final right_action_button:I = 0x7f0902ba

.field public static final right_action_button_icon:I = 0x7f0902bb

.field public static final right_action_button_label:I = 0x7f0902bc

.field public static final right_arrow:I = 0x7f091290

.field public static final right_button:I = 0x7f090c7a

.field public static final right_column:I = 0x7f090aaf

.field public static final right_container:I = 0x7f09010f

.field public static final right_divider:I = 0x7f090f09

.field public static final right_icon:I = 0x7f090110

.field public static final right_image:I = 0x7f09029a

.field public static final right_origin_mask:I = 0x7f090b36

.field public static final right_origin_text_view:I = 0x7f090b37

.field public static final right_pager_button:I = 0x7f0907fd

.field public static final right_text:I = 0x7f0902a8

.field public static final robotext:I = 0x7f090377

.field public static final roboto:I = 0x7f090022

.field public static final root:I = 0x7f090f81

.field public static final rootLayout:I = 0x7f09027d

.field public static final root_container:I = 0x7f090413

.field public static final root_view:I = 0x7f0901b1

.field public static final rootview:I = 0x7f09048b

.field public static final rotate_button:I = 0x7f0911fa

.field public static final row_button:I = 0x7f090fb4

.field public static final row_container:I = 0x7f0902a3

.field public static final row_left_icon:I = 0x7f0902a4

.field public static final row_message:I = 0x7f0902a6

.field public static final row_title:I = 0x7f0902a5

.field public static final row_type_toggle_button:I = 0x7f09137c

.field public static final sandbox:I = 0x7f0900c5

.field public static final sans:I = 0x7f090003

.field public static final satellite:I = 0x7f0900bf

.field public static final save:I = 0x7f090b83

.field public static final save_button:I = 0x7f091393

.field public static final save_picture:I = 0x7f09138a

.field public static final saved_collection_action_icon_divider:I = 0x7f0910de

.field public static final saved_collection_feed_unit_item_bottom_section:I = 0x7f0910dc

.field public static final saved_collection_feed_unit_item_container:I = 0x7f0910da

.field public static final saved_collection_feed_unit_item_main_image:I = 0x7f0910db

.field public static final saved_collection_feed_unit_item_save_button:I = 0x7f0910e1

.field public static final saved_collection_feed_unit_item_subtitle:I = 0x7f0910e0

.field public static final saved_collection_feed_unit_item_text_container:I = 0x7f0910dd

.field public static final saved_collection_feed_unit_item_title:I = 0x7f0910df

.field public static final saved_dashboard_content_view:I = 0x7f0910f6

.field public static final saved_dashboard_empty_view:I = 0x7f0910f3

.field public static final saved_dashboard_filter:I = 0x7f0910f4

.field public static final saved_dashboard_filter_action_button:I = 0x7f0910e3

.field public static final saved_dashboard_filter_label:I = 0x7f0910e2

.field public static final saved_dashboard_header_content_divider:I = 0x7f0910f5

.field public static final saved_dashboard_interstitial_primary_button:I = 0x7f0910e4

.field public static final saved_dashboard_section_item_count:I = 0x7f0910e6

.field public static final saved_dashboard_section_item_title:I = 0x7f0910e5

.field public static final saved_item_action_button:I = 0x7f0910f0

.field public static final saved_item_attribution:I = 0x7f0910ef

.field public static final saved_item_profile_picture:I = 0x7f0910eb

.field public static final saved_item_profile_picture_container:I = 0x7f0910ea

.field public static final saved_item_subtitle:I = 0x7f0910ee

.field public static final saved_item_title:I = 0x7f0910ed

.field public static final saved_item_title_container:I = 0x7f0910ec

.field public static final saved_item_undo_button:I = 0x7f0910f2

.field public static final saved_item_undo_button_container:I = 0x7f0910f1

.field public static final saved_items_list_empty_view:I = 0x7f0910e9

.field public static final saved_items_list_view:I = 0x7f0910e8

.field public static final saved_list_container:I = 0x7f0910e7

.field public static final saved_places_check_icon:I = 0x7f090e25

.field public static final saved_places_lock_icon:I = 0x7f090e27

.field public static final saved_places_textview:I = 0x7f090e26

.field public static final saved_places_unit:I = 0x7f090e24

.field public static final saved_section_header_divider:I = 0x7f0910f7

.field public static final saved_section_header_title:I = 0x7f0910f8

.field public static final saved_sections_list_view:I = 0x7f0910f9

.field public static final scan_code_button:I = 0x7f090fea

.field public static final scanner_buttons:I = 0x7f090fe8

.field public static final schedule_button:I = 0x7f090336

.field public static final schedule_date:I = 0x7f0910fa

.field public static final schedule_post_text:I = 0x7f090390

.field public static final schedule_time:I = 0x7f0910fb

.field public static final scrim:I = 0x7f090276

.field public static final scrollView:I = 0x7f090a64

.field public static final scroll_to_top_button_text:I = 0x7f090e28

.field public static final scroll_view:I = 0x7f0908d4

.field public static final scrollview:I = 0x7f090374

.field public static final scrollview_extras:I = 0x7f09037c

.field public static final search_badge:I = 0x7f090115

.field public static final search_bar:I = 0x7f090114

.field public static final search_bar_view:I = 0x7f0910fc

.field public static final search_box:I = 0x7f090736

.field public static final search_box_container:I = 0x7f090735

.field public static final search_button:I = 0x7f090116

.field public static final search_close_btn:I = 0x7f09011b

.field public static final search_edit_frame:I = 0x7f090117

.field public static final search_edit_text:I = 0x7f090872

.field public static final search_edit_text_container:I = 0x7f090871

.field public static final search_go_btn:I = 0x7f09011d

.field public static final search_mag_icon:I = 0x7f090118

.field public static final search_messages_fragment_container:I = 0x7f090c39

.field public static final search_messages_list:I = 0x7f090c3a

.field public static final search_messages_view_fragment:I = 0x7f090c38

.field public static final search_overlay:I = 0x7f090796

.field public static final search_plate:I = 0x7f090119

.field public static final search_remaining_messages:I = 0x7f090c37

.field public static final search_results_container:I = 0x7f091297

.field public static final search_src_text:I = 0x7f09011a

.field public static final search_suggestion_header:I = 0x7f090797

.field public static final search_text:I = 0x7f09096e

.field public static final search_title:I = 0x7f09127d

.field public static final search_title_container:I = 0x7f09127c

.field public static final search_voice_btn:I = 0x7f09011e

.field public static final searchbox:I = 0x7f090947

.field public static final searchbox_container:I = 0x7f091295

.field public static final second_fake_result:I = 0x7f0910ca

.field public static final second_photo_pivot:I = 0x7f091167

.field public static final secondary_action_button:I = 0x7f090130

.field public static final secondary_action_button_divider:I = 0x7f090132

.field public static final secondary_actions_button_coordinates:I = 0x7f0900ae

.field public static final secondary_button:I = 0x7f090a79

.field public static final secondary_named_button:I = 0x7f09012f

.field public static final secondary_named_button_divider:I = 0x7f090131

.field public static final section_text:I = 0x7f0910fe

.field public static final section_title_container:I = 0x7f090303

.field public static final section_title_icon:I = 0x7f090304

.field public static final section_title_text:I = 0x7f090305

.field public static final security_code:I = 0x7f09015b

.field public static final see_more_empty_view:I = 0x7f090868

.field public static final see_more_list_view:I = 0x7f090867

.field public static final see_nearby_places_button:I = 0x7f0906f3

.field public static final select_at_layout:I = 0x7f091108

.field public static final select_button:I = 0x7f090343

.field public static final select_button_text:I = 0x7f090f7e

.field public static final select_video:I = 0x7f090270

.field public static final selected_border:I = 0x7f090f05

.field public static final selected_button:I = 0x7f0909ed

.field public static final selected_order_text:I = 0x7f090f06

.field public static final selected_person_header:I = 0x7f0907fa

.field public static final selected_privacy_tv:I = 0x7f09048e

.field public static final selectionDetails:I = 0x7f0900c8

.field public static final send_button:I = 0x7f0906cc

.field public static final send_button_inline:I = 0x7f090cc7

.field public static final send_button_stub:I = 0x7f090bda

.field public static final send_log_menu_item:I = 0x7f09008d

.field public static final separator:I = 0x7f09017c

.field public static final serif:I = 0x7f090004

.field public static final server_error_text:I = 0x7f090aab

.field public static final service_name:I = 0x7f090627

.field public static final set_photo:I = 0x7f09139d

.field public static final set_pin:I = 0x7f090133

.field public static final set_wallpaper:I = 0x7f09089d

.field public static final settings_fragment:I = 0x7f09089c

.field public static final settings_layout:I = 0x7f09081f

.field public static final setup_background:I = 0x7f091122

.field public static final setup_fragment_root:I = 0x7f091121

.field public static final setup_wallpaper_customize:I = 0x7f091130

.field public static final setup_wallpaper_not_now:I = 0x7f09112f

.field public static final shadow:I = 0x7f09084a

.field public static final share_attachment_image:I = 0x7f0906f6

.field public static final share_attachment_image_frame:I = 0x7f090640

.field public static final share_attachment_subtitle:I = 0x7f0906f8

.field public static final share_attachment_title:I = 0x7f0906f7

.field public static final share_attachment_upload_progress_indicator:I = 0x7f090641

.field public static final share_attribution:I = 0x7f090c43

.field public static final share_button:I = 0x7f09017f

.field public static final share_button_item:I = 0x7f091359

.field public static final share_content_box:I = 0x7f090c40

.field public static final share_description:I = 0x7f090c44

.field public static final share_dividing_line:I = 0x7f090c3e

.field public static final share_image:I = 0x7f090c41

.field public static final share_link_preview:I = 0x7f09113b

.field public static final share_link_preview_fragment:I = 0x7f09037e

.field public static final share_link_preview_stub:I = 0x7f09113c

.field public static final share_name:I = 0x7f090c42

.field public static final share_photo_image:I = 0x7f0906f9

.field public static final share_picture:I = 0x7f09138b

.field public static final share_preview_description:I = 0x7f091134

.field public static final share_robotext:I = 0x7f090c3f

.field public static final share_separator:I = 0x7f09037d

.field public static final share_story_play_button:I = 0x7f090642

.field public static final share_text_item:I = 0x7f091358

.field public static final shimmering_faceboxes_layer:I = 0x7f090347

.field public static final shortcut:I = 0x7f090109

.field public static final shortcut_close_button:I = 0x7f0902a2

.field public static final shortcut_icon:I = 0x7f0902a0

.field public static final shortcut_message:I = 0x7f0902a1

.field public static final should_add_facebox:I = 0x7f0900d4

.field public static final showCustom:I = 0x7f090042

.field public static final showHome:I = 0x7f09003f

.field public static final showTitle:I = 0x7f090041

.field public static final show_caches:I = 0x7f090f3a

.field public static final show_code_button:I = 0x7f090fe9

.field public static final show_contributor_view:I = 0x7f090495

.field public static final show_cover_photo_button:I = 0x7f09122d

.field public static final show_cover_photo_button_stub:I = 0x7f09122c

.field public static final show_cover_photo_icon:I = 0x7f091263

.field public static final show_cover_photo_progress_bar:I = 0x7f09122e

.field public static final show_last_gps_location:I = 0x7f090f31

.field public static final show_last_wifi_location:I = 0x7f090f30

.field public static final show_notifications_setting:I = 0x7f091113

.field public static final shutter:I = 0x7f090269

.field public static final side_shadow:I = 0x7f090403

.field public static final signup:I = 0x7f09042b

.field public static final silent_login_loading_view:I = 0x7f090bbe

.field public static final sim_balance:I = 0x7f090299

.field public static final simple_notif_icon:I = 0x7f09113f

.field public static final simple_notif_text2:I = 0x7f091141

.field public static final simple_notif_text3:I = 0x7f091142

.field public static final simple_notif_title:I = 0x7f091140

.field public static final simple_picker_root:I = 0x7f091143

.field public static final single_fitness_item:I = 0x7f090949

.field public static final single_music_item:I = 0x7f090a5e

.field public static final single_music_item_landscape_stub:I = 0x7f090a50

.field public static final single_music_item_stub:I = 0x7f090a4f

.field public static final skip_button:I = 0x7f090438

.field public static final skip_guardrail_button:I = 0x7f091185

.field public static final skip_link:I = 0x7f0901cf

.field public static final sliding_out_suggestion_button:I = 0x7f090c48

.field public static final sliding_out_suggestion_separator_bottom:I = 0x7f090c49

.field public static final sliding_out_suggestion_separator_top:I = 0x7f090c46

.field public static final sliding_out_suggestion_text:I = 0x7f090c47

.field public static final small:I = 0x7f090059

.field public static final small_image_cover_photo:I = 0x7f091157

.field public static final small_image_description_text:I = 0x7f091154

.field public static final small_image_ego_item:I = 0x7f09114d

.field public static final small_image_ego_item_actor:I = 0x7f091151

.field public static final small_image_ego_item_bottom:I = 0x7f091156

.field public static final small_image_ego_item_bottom_text:I = 0x7f091158

.field public static final small_image_ego_item_bottom_titles:I = 0x7f091159

.field public static final small_image_ego_item_header:I = 0x7f091150

.field public static final small_image_ego_item_inner:I = 0x7f091155

.field public static final small_image_ego_item_social_header_divider:I = 0x7f09114f

.field public static final small_image_ego_item_social_header_title:I = 0x7f09114e

.field public static final small_image_ego_item_sponsored_text:I = 0x7f091153

.field public static final small_image_ego_item_title:I = 0x7f091152

.field public static final small_image_ego_like_button:I = 0x7f09115c

.field public static final small_image_ego_like_sentence:I = 0x7f09115a

.field public static final small_image_feed_feedback_share_divider:I = 0x7f09115b

.field public static final snowflake_background:I = 0x7f0903ad

.field public static final snowflake_caption:I = 0x7f0903b1

.field public static final snowflake_list:I = 0x7f0903ae

.field public static final snowflake_photo_mask:I = 0x7f0903b0

.field public static final snowflake_ufi:I = 0x7f0903b2

.field public static final snowphoto:I = 0x7f0903af

.field public static final social_context_text:I = 0x7f090ffe

.field public static final sound_wave:I = 0x7f090a2a

.field public static final sound_wave_container:I = 0x7f090a29

.field public static final spacingWidth:I = 0x7f090039

.field public static final spacingWidthUniform:I = 0x7f09003b

.field public static final spinner:I = 0x7f09061f

.field public static final spinner_item_loading:I = 0x7f091169

.field public static final spinner_item_text:I = 0x7f091168

.field public static final split_action_bar:I = 0x7f0900fb

.field public static final sports_stories_with_scores_away_team_logo:I = 0x7f09116f

.field public static final sports_stories_with_scores_away_team_name:I = 0x7f091170

.field public static final sports_stories_with_scores_away_team_score:I = 0x7f091171

.field public static final sports_stories_with_scores_home_team_logo:I = 0x7f09116c

.field public static final sports_stories_with_scores_home_team_name:I = 0x7f09116d

.field public static final sports_stories_with_scores_home_team_score:I = 0x7f09116e

.field public static final sports_stories_without_scores_away_team_logo:I = 0x7f091175

.field public static final sports_stories_without_scores_away_team_name:I = 0x7f091174

.field public static final sports_stories_without_scores_home_team_logo:I = 0x7f091172

.field public static final sports_stories_without_scores_home_team_name:I = 0x7f091173

.field public static final spring_selector_spinner:I = 0x7f091176

.field public static final sso_change_user:I = 0x7f090567

.field public static final sso_focus_holder:I = 0x7f090565

.field public static final sso_group:I = 0x7f090564

.field public static final sso_login:I = 0x7f090566

.field public static final sso_progress_spinner:I = 0x7f090576

.field public static final start:I = 0x7f090015

.field public static final start_and_end_time_picker_end_date:I = 0x7f091181

.field public static final start_and_end_time_picker_end_date_time:I = 0x7f091180

.field public static final start_and_end_time_picker_end_time:I = 0x7f091182

.field public static final start_and_end_time_picker_start_date:I = 0x7f09117e

.field public static final start_and_end_time_picker_start_date_time:I = 0x7f09117d

.field public static final start_and_end_time_picker_start_time:I = 0x7f09117f

.field public static final start_conversation_text:I = 0x7f090c85

.field public static final static_pin_overlay:I = 0x7f090f18

.field public static final status_bar_latest_event_content:I = 0x7f090b30

.field public static final status_content_container:I = 0x7f090389

.field public static final status_icon:I = 0x7f090c67

.field public static final status_text:I = 0x7f090379

.field public static final status_text_wrapper:I = 0x7f090375

.field public static final status_update_frame:I = 0x7f09089a

.field public static final status_update_layout:I = 0x7f0908a8

.field public static final status_wrapper:I = 0x7f09038d

.field public static final step_intro_fragment:I = 0x7f0906f0

.field public static final sticker:I = 0x7f090c65

.field public static final sticker_bottom_divider:I = 0x7f090c68

.field public static final sticker_bottom_drag_slot:I = 0x7f090c69

.field public static final sticker_image:I = 0x7f090bf3

.field public static final sticker_item_display_frame:I = 0x7f090c64

.field public static final sticker_preview_animated:I = 0x7f090c50

.field public static final sticker_store_button:I = 0x7f090c4f

.field public static final sticker_store_fragment:I = 0x7f090c5c

.field public static final sticker_stub:I = 0x7f090be1

.field public static final sticker_tag_featured_tags_frame:I = 0x7f090c54

.field public static final sticker_tag_item_text:I = 0x7f090c73

.field public static final sticker_tag_item_thumbnail:I = 0x7f090c72

.field public static final sticker_tag_loading_message:I = 0x7f090c51

.field public static final sticker_tag_matches_text:I = 0x7f090c53

.field public static final sticker_tag_remove_tag:I = 0x7f090c5b

.field public static final sticker_tag_results_frame:I = 0x7f090c55

.field public static final sticker_tag_search_edit_box:I = 0x7f090c57

.field public static final sticker_tag_search_frame:I = 0x7f090c56

.field public static final sticker_tag_search_hint:I = 0x7f090c58

.field public static final sticker_tag_search_hint_icon:I = 0x7f090c59

.field public static final sticker_tag_search_hint_text:I = 0x7f090c5a

.field public static final sticker_tag_selection:I = 0x7f090c52

.field public static final sticker_top_divider:I = 0x7f090c63

.field public static final sticker_top_drag_slot:I = 0x7f090c62

.field public static final stickers_button:I = 0x7f090ccd

.field public static final sticky_guardrail_body:I = 0x7f091184

.field public static final sticky_guardrail_title:I = 0x7f091183

.field public static final sticky_sys_tray_notification_cancel:I = 0x7f09118e

.field public static final sticky_sys_tray_notification_fbicon:I = 0x7f091187

.field public static final sticky_sys_tray_notification_friends:I = 0x7f091188

.field public static final sticky_sys_tray_notification_friends_text:I = 0x7f091189

.field public static final sticky_sys_tray_notification_messages:I = 0x7f09118a

.field public static final sticky_sys_tray_notification_messages_text:I = 0x7f09118b

.field public static final sticky_sys_tray_notification_notifications:I = 0x7f09118c

.field public static final sticky_sys_tray_notification_notifications_text:I = 0x7f09118d

.field public static final store_button_badge:I = 0x7f090c71

.field public static final store_image:I = 0x7f090c70

.field public static final store_tab_content:I = 0x7f090c61

.field public static final store_tabs:I = 0x7f090c5d

.field public static final stories:I = 0x7f090f36

.field public static final story_attachment_add_friend_icon:I = 0x7f09063a

.field public static final story_attachment_add_friend_icon_divider:I = 0x7f09063b

.field public static final story_attachment_angora_bottom_view:I = 0x7f09065d

.field public static final story_attachment_app_name:I = 0x7f090645

.field public static final story_attachment_call_to_action:I = 0x7f09065c

.field public static final story_attachment_call_to_action_view_stub:I = 0x7f09065b

.field public static final story_attachment_coupon_background:I = 0x7f090647

.field public static final story_attachment_coupon_divider:I = 0x7f09064d

.field public static final story_attachment_coupon_get_offer:I = 0x7f09064b

.field public static final story_attachment_coupon_header:I = 0x7f090648

.field public static final story_attachment_coupon_image:I = 0x7f090649

.field public static final story_attachment_coupon_subtitle:I = 0x7f09064c

.field public static final story_attachment_coupon_title:I = 0x7f09064a

.field public static final story_attachment_extra:I = 0x7f09063d

.field public static final story_attachment_image:I = 0x7f090639

.field public static final story_attachment_leaveapp_icon:I = 0x7f0906b2

.field public static final story_attachment_name:I = 0x7f09063c

.field public static final story_attachment_preview:I = 0x7f09037f

.field public static final story_attachment_right_section:I = 0x7f090643

.field public static final story_attachment_robotext:I = 0x7f09063f

.field public static final story_attachment_sponsored:I = 0x7f0906b1

.field public static final story_attachment_subtitle:I = 0x7f090637

.field public static final story_attachment_text_container:I = 0x7f090644

.field public static final story_attachment_title:I = 0x7f090636

.field public static final story_attachment_video:I = 0x7f090662

.field public static final story_attachment_video_optimistic_progress:I = 0x7f090664

.field public static final story_attachment_video_optimistic_text:I = 0x7f090666

.field public static final story_attachment_video_optimistic_title:I = 0x7f090665

.field public static final story_attachment_video_optimistic_wrapper:I = 0x7f090663

.field public static final story_attachment_video_parent:I = 0x7f0905b3

.field public static final story_attachment_video_title:I = 0x7f090667

.field public static final story_author:I = 0x7f090ed2

.field public static final story_chaining_section:I = 0x7f090674

.field public static final story_content:I = 0x7f090f37

.field public static final story_date:I = 0x7f090ed3

.field public static final story_extra_info:I = 0x7f090ed5

.field public static final story_find_friends_subtitle:I = 0x7f0905d5

.field public static final story_find_friends_title:I = 0x7f09067e

.field public static final story_find_pages_subtitle:I = 0x7f090681

.field public static final story_find_pages_title:I = 0x7f090680

.field public static final story_friends:I = 0x7f090ed4

.field public static final story_insights_error_view:I = 0x7f091198

.field public static final story_menu_button:I = 0x7f091207

.field public static final story_pager_container:I = 0x7f09040a

.field public static final story_promotion_budget_label_1:I = 0x7f0911bb

.field public static final story_promotion_budget_label_2:I = 0x7f0911bc

.field public static final story_promotion_budget_label_3:I = 0x7f0911bd

.field public static final story_promotion_budget_label_4:I = 0x7f0911be

.field public static final story_promotion_budget_label_fixed_0:I = 0x7f0911ba

.field public static final story_promotion_view:I = 0x7f090370

.field public static final story_text:I = 0x7f091206

.field public static final strict_sandbox:I = 0x7f0900c6

.field public static final stub_generic_back:I = 0x7f090633

.field public static final stub_sports_stories_with_scores:I = 0x7f09116a

.field public static final stub_sports_stories_without_scores:I = 0x7f09116b

.field public static final stub_view:I = 0x7f090330

.field public static final sub_title:I = 0x7f0902c1

.field public static final submit_area:I = 0x7f09011c

.field public static final submit_button:I = 0x7f090122

.field public static final submit_native_name:I = 0x7f09074c

.field public static final substory_gallery:I = 0x7f0906bd

.field public static final substory_shadow:I = 0x7f090099

.field public static final subtext:I = 0x7f090f2d

.field public static final subtext_text_view:I = 0x7f0908fd

.field public static final subtitle:I = 0x7f09005d

.field public static final subtitle_view_text:I = 0x7f0911cb

.field public static final suffix_text_view:I = 0x7f09044c

.field public static final suggest_edits:I = 0x7f091394

.field public static final suggest_photo_icon:I = 0x7f0911cd

.field public static final suggest_profile_pic:I = 0x7f090f42

.field public static final suggest_profile_pic_container:I = 0x7f090f1a

.field public static final suggest_profile_pic_fragment_container:I = 0x7f090da7

.field public static final suggested_promo_row_button:I = 0x7f0902ac

.field public static final suggested_promo_row_left_icon:I = 0x7f0902a9

.field public static final suggested_promo_row_message:I = 0x7f0902ab

.field public static final suggested_promo_row_title:I = 0x7f0902aa

.field public static final suggested_promos:I = 0x7f0902ad

.field public static final suggested_promos_title:I = 0x7f0902ae

.field public static final suggested_videos_list:I = 0x7f09130d

.field public static final suggested_videos_title:I = 0x7f09130c

.field public static final suggestion_container:I = 0x7f090c45

.field public static final suggestion_face_box:I = 0x7f090798

.field public static final suggestion_picker_list_view:I = 0x7f09079c

.field public static final suggestion_picker_padding:I = 0x7f09079d

.field public static final suggestion_picker_selector_divider:I = 0x7f09079b

.field public static final suggestion_tag_picker_autocomplete_input:I = 0x7f090799

.field public static final suggestion_tag_picker_loading_indicator:I = 0x7f09079a

.field public static final suggestions_contacts_list:I = 0x7f090b56

.field public static final suggestions_container:I = 0x7f090b55

.field public static final suggestions_container_view:I = 0x7f090739

.field public static final suggestions_list_view:I = 0x7f09073a

.field public static final suggestions_multipicker_fragment:I = 0x7f090b7f

.field public static final summary_row_divider:I = 0x7f090515

.field public static final survey_answers_container:I = 0x7f0911d4

.field public static final survey_complete_container:I = 0x7f0911d5

.field public static final survey_feedback:I = 0x7f090cdf

.field public static final survey_question:I = 0x7f0911d3

.field public static final survey_unit_actor_profile_pic:I = 0x7f0911d0

.field public static final survey_unit_container:I = 0x7f0911ce

.field public static final survey_unit_header_container:I = 0x7f0911cf

.field public static final survey_unit_subtitle:I = 0x7f0911d2

.field public static final survey_unit_title:I = 0x7f0911d1

.field public static final swipe_down_text:I = 0x7f09100b

.field public static final switchWidget:I = 0x7f09043a

.field public static final sync_contacts_choice_dont_sync:I = 0x7f0911e0

.field public static final sync_contacts_choice_dont_sync_description:I = 0x7f0911e2

.field public static final sync_contacts_choice_dont_sync_item:I = 0x7f0911df

.field public static final sync_contacts_choice_dont_sync_text:I = 0x7f0911e1

.field public static final sync_contacts_choice_sync_all:I = 0x7f0911da

.field public static final sync_contacts_choice_sync_all_item:I = 0x7f0911d9

.field public static final sync_contacts_choice_sync_existing:I = 0x7f0911dd

.field public static final sync_contacts_choice_sync_existing_item:I = 0x7f0911dc

.field public static final sync_contacts_choice_sync_text:I = 0x7f0911de

.field public static final sync_contacts_choice_text:I = 0x7f0911db

.field public static final sync_contacts_footer_text:I = 0x7f0911e3

.field public static final sync_grid_parent:I = 0x7f0912f0

.field public static final sync_header:I = 0x7f0912e7

.field public static final sync_old_photo_checkbox:I = 0x7f0912fc

.field public static final sync_old_photo_setting:I = 0x7f0912fb

.field public static final sync_old_photo_text:I = 0x7f0912fd

.field public static final sync_warning_text:I = 0x7f090c74

.field public static final system_notification_view:I = 0x7f0911e4

.field public static final system_notifications:I = 0x7f0903c3

.field public static final system_notifications_prompt_dialog_text:I = 0x7f09112d

.field public static final system_notifications_prompt_dialog_title:I = 0x7f09112c

.field public static final system_notifications_root:I = 0x7f09112b

.field public static final tabMode:I = 0x7f09003d

.field public static final tab_activity_fullscreen_container:I = 0x7f0909be

.field public static final tab_activity_gallery_launcher:I = 0x7f0909bf

.field public static final tab_activity_main_expandable_photo:I = 0x7f0909c0

.field public static final tab_activity_main_expandable_video_container:I = 0x7f0909c1

.field public static final tab_bar_container:I = 0x7f0909ba

.field public static final tab_container_scroller:I = 0x7f090c78

.field public static final tab_content_viewpager:I = 0x7f0909bd

.field public static final tab_glow:I = 0x7f0911e8

.field public static final tab_host:I = 0x7f090f04

.field public static final tab_icon:I = 0x7f0911e6

.field public static final tab_image:I = 0x7f090c6f

.field public static final tab_indicator_text:I = 0x7f09021f

.field public static final tab_main_view:I = 0x7f0911e5

.field public static final tab_segments:I = 0x7f090730

.field public static final tab_slider:I = 0x7f09076e

.field public static final tab_unread_count:I = 0x7f0911e7

.field public static final tag_anchor:I = 0x7f090281

.field public static final tag_bubble:I = 0x7f090f01

.field public static final tag_button:I = 0x7f0903a5

.field public static final tag_button_new:I = 0x7f0903a7

.field public static final tag_count:I = 0x7f0912db

.field public static final tag_counter_button:I = 0x7f0903a8

.field public static final tag_frame:I = 0x7f0903a6

.field public static final tag_instruction:I = 0x7f0911fc

.field public static final tag_notification_checkbox_key:I = 0x7f0900da

.field public static final tag_notification_key:I = 0x7f0900d8

.field public static final tag_notification_type_key:I = 0x7f0900d9

.field public static final tag_people:I = 0x7f090335

.field public static final tag_people_badge:I = 0x7f090384

.field public static final tag_place:I = 0x7f090337

.field public static final tag_position_key:I = 0x7f0900d7

.field public static final tag_remove:I = 0x7f091091

.field public static final tag_remove_button:I = 0x7f090f02

.field public static final tag_text:I = 0x7f09027f

.field public static final tag_toggle:I = 0x7f0912da

.field public static final tag_typeahead:I = 0x7f09027e

.field public static final tag_typeahead_bubble_content:I = 0x7f0911f2

.field public static final tag_typeahead_bubble_layout:I = 0x7f0911f1

.field public static final tag_typeahead_divider:I = 0x7f0911f5

.field public static final tag_typeahead_edit_text:I = 0x7f0911f3

.field public static final tag_typeahead_list_item_text_view:I = 0x7f0911f8

.field public static final tag_typeahead_list_item_url_image:I = 0x7f0911f7

.field public static final tag_typeahead_list_view:I = 0x7f0911f6

.field public static final tag_typeahead_progress_bar:I = 0x7f0911f4

.field public static final tagged_person1:I = 0x7f090ed7

.field public static final tagged_person2:I = 0x7f090ed8

.field public static final tagged_person3:I = 0x7f090ed9

.field public static final tagged_person4:I = 0x7f090eda

.field public static final tagging_instructions:I = 0x7f0903ac

.field public static final tagging_x:I = 0x7f090280

.field public static final tail_segments_container:I = 0x7f0903e0

.field public static final take_photo_button:I = 0x7f090aad

.field public static final tap:I = 0x7f090033

.field public static final tap_again:I = 0x7f090554

.field public static final tap_to_open:I = 0x7f0911fd

.field public static final tap_to_reconnect_text:I = 0x7f09062f

.field public static final target_place:I = 0x7f0909c6

.field public static final task_description:I = 0x7f0911ff

.field public static final task_name:I = 0x7f0911fe

.field public static final taskview:I = 0x7f091201

.field public static final temperature_celsius:I = 0x7f091204

.field public static final temperature_fahrenheit:I = 0x7f091205

.field public static final temperature_unit_setting:I = 0x7f091114

.field public static final tension_label:I = 0x7f091178

.field public static final tension_seekbar:I = 0x7f091177

.field public static final terms_of_service:I = 0x7f0903f4

.field public static final terms_setting:I = 0x7f09111c

.field public static final terms_text_1:I = 0x7f091073

.field public static final terrain:I = 0x7f0900c0

.field public static final text:I = 0x7f090255

.field public static final text_amount_header:I = 0x7f090930

.field public static final text_button:I = 0x7f09046a

.field public static final text_card_info:I = 0x7f090156

.field public static final text_card_title:I = 0x7f090ceb

.field public static final text_category_name:I = 0x7f09028c

.field public static final text_child_name:I = 0x7f090fb0

.field public static final text_closing:I = 0x7f0903db

.field public static final text_container:I = 0x7f09086d

.field public static final text_content:I = 0x7f09126f

.field public static final text_content_container:I = 0x7f0910d6

.field public static final text_content_info:I = 0x7f09092e

.field public static final text_content_title:I = 0x7f09092d

.field public static final text_credit_card_info:I = 0x7f0903ce

.field public static final text_description_header:I = 0x7f090f8b

.field public static final text_error:I = 0x7f09014f

.field public static final text_error_message:I = 0x7f0904cd

.field public static final text_gift_info:I = 0x7f090ce0

.field public static final text_header:I = 0x7f09014e

.field public static final text_header_char:I = 0x7f091067

.field public static final text_header_info:I = 0x7f0903d7

.field public static final text_input_container:I = 0x7f090cc1

.field public static final text_item:I = 0x7f09135d

.field public static final text_item_title:I = 0x7f090934

.field public static final text_let_them_choose_header:I = 0x7f090fa2

.field public static final text_let_them_choose_info:I = 0x7f090fa5

.field public static final text_like_data:I = 0x7f09083e

.field public static final text_message:I = 0x7f090bb6

.field public static final text_messaging_input_container:I = 0x7f090cc2

.field public static final text_more_products_header:I = 0x7f090839

.field public static final text_name:I = 0x7f09083c

.field public static final text_no_results:I = 0x7f090faa

.field public static final text_option:I = 0x7f090845

.field public static final text_option_title:I = 0x7f090251

.field public static final text_order_discount_value:I = 0x7f090cf7

.field public static final text_order_shipping:I = 0x7f090cf3

.field public static final text_order_shipping_value:I = 0x7f090cf4

.field public static final text_order_subtotal:I = 0x7f090cf1

.field public static final text_order_subtotal_value:I = 0x7f090cf2

.field public static final text_order_total_value:I = 0x7f090cf0

.field public static final text_overlay:I = 0x7f091282

.field public static final text_price:I = 0x7f09083d

.field public static final text_product_category_header:I = 0x7f090fab

.field public static final text_product_name:I = 0x7f090853

.field public static final text_product_price:I = 0x7f090654

.field public static final text_product_sku_name:I = 0x7f090f82

.field public static final text_product_title:I = 0x7f090653

.field public static final text_reason:I = 0x7f091069

.field public static final text_recipient_name:I = 0x7f091139

.field public static final text_recipient_post_header:I = 0x7f091137

.field public static final text_recipient_title:I = 0x7f090cfb

.field public static final text_recommend_list_header:I = 0x7f090931

.field public static final text_recommended_header:I = 0x7f090844

.field public static final text_section_header:I = 0x7f09092f

.field public static final text_see_all:I = 0x7f090841

.field public static final text_sender_name:I = 0x7f09113a

.field public static final text_sexy_name:I = 0x7f090f96

.field public static final text_share_on_timeline:I = 0x7f090cfc

.field public static final text_sku_1_name:I = 0x7f090f9b

.field public static final text_sku_2_name:I = 0x7f090fa0

.field public static final text_sku_name:I = 0x7f090cf9

.field public static final text_skus_header:I = 0x7f090f97

.field public static final text_subtitle:I = 0x7f090241

.field public static final text_terms:I = 0x7f090cfe

.field public static final text_terms_apply_link:I = 0x7f090cfd

.field public static final text_terms_info:I = 0x7f090cff

.field public static final text_title:I = 0x7f090240

.field public static final text_view:I = 0x7f0903c1

.field public static final thin:I = 0x7f090023

.field public static final thread_divider:I = 0x7f090c96

.field public static final thread_failed_icon:I = 0x7f090c97

.field public static final thread_failure_container:I = 0x7f090c95

.field public static final thread_last_msg:I = 0x7f090c94

.field public static final thread_last_msg_stub:I = 0x7f090c93

.field public static final thread_list:I = 0x7f090c8a

.field public static final thread_list_composer_container:I = 0x7f090c89

.field public static final thread_list_container:I = 0x7f090c7d

.field public static final thread_list_loading_view:I = 0x7f090c90

.field public static final thread_list_mute_warning:I = 0x7f090c7f

.field public static final thread_list_mute_warning_view_stub:I = 0x7f090c7e

.field public static final thread_list_overlay_container:I = 0x7f090c7c

.field public static final thread_list_sync_disabled_warning:I = 0x7f090c81

.field public static final thread_list_sync_disabled_warning_view_stub:I = 0x7f090c80

.field public static final thread_load_more:I = 0x7f090bb1

.field public static final thread_name:I = 0x7f090c3d

.field public static final thread_name_edit_text_field:I = 0x7f090ca3

.field public static final thread_name_presence:I = 0x7f090b3f

.field public static final thread_presence_indicator:I = 0x7f090c92

.field public static final thread_preview_img:I = 0x7f090c99

.field public static final thread_send_progress_bar:I = 0x7f090c98

.field public static final thread_settings:I = 0x7f091398

.field public static final thread_tile_img:I = 0x7f090b2f

.field public static final thread_time:I = 0x7f090c91

.field public static final thread_title_header_image:I = 0x7f090ca4

.field public static final thread_title_header_view:I = 0x7f090ca5

.field public static final thread_title_name:I = 0x7f090b40

.field public static final thread_title_presence_indicator:I = 0x7f090b41

.field public static final thread_title_progress:I = 0x7f090b42

.field public static final thread_title_status:I = 0x7f090b46

.field public static final thread_title_view:I = 0x7f090b47

.field public static final thread_view_error_anchor:I = 0x7f09120f

.field public static final thread_view_fragment_host:I = 0x7f090a08

.field public static final thread_view_overlay_container:I = 0x7f090b39

.field public static final threadlist_container:I = 0x7f090c88

.field public static final threadlist_root:I = 0x7f090c86

.field public static final threadview_updates_empty_item:I = 0x7f090cab

.field public static final thumbnail:I = 0x7f090bc6

.field public static final thumbnail_card_thumbnails:I = 0x7f09121a

.field public static final thumbnail_card_title:I = 0x7f091219

.field public static final thumbnail_card_view:I = 0x7f091218

.field public static final thumbnail_image:I = 0x7f09121b

.field public static final thumbnail_picture:I = 0x7f090ed0

.field public static final time_item:I = 0x7f09135e

.field public static final timeline:I = 0x7f0913a5

.field public static final timeline_action_bar:I = 0x7f090f4b

.field public static final timeline_action_button_icon:I = 0x7f09121c

.field public static final timeline_action_button_text:I = 0x7f09121d

.field public static final timeline_action_links:I = 0x7f09121e

.field public static final timeline_block_confirm_message:I = 0x7f091224

.field public static final timeline_block_confirm_message_friends:I = 0x7f091225

.field public static final timeline_byline_fragment_icon:I = 0x7f091226

.field public static final timeline_byline_fragment_text:I = 0x7f091227

.field public static final timeline_byline_section:I = 0x7f0912d6

.field public static final timeline_collection_checkmark:I = 0x7f091229

.field public static final timeline_collection_plus_button:I = 0x7f091228

.field public static final timeline_container:I = 0x7f090758

.field public static final timeline_contextual_items:I = 0x7f090f4c

.field public static final timeline_cover_photo_edit_view:I = 0x7f0912d5

.field public static final timeline_cover_photo_stub:I = 0x7f09122a

.field public static final timeline_cover_photo_view:I = 0x7f090f58

.field public static final timeline_cover_pic:I = 0x7f09122b

.field public static final timeline_edit_cover_photo_view:I = 0x7f090d2e

.field public static final timeline_facepile_1:I = 0x7f09123d

.field public static final timeline_facepile_2:I = 0x7f09123e

.field public static final timeline_facepile_3:I = 0x7f09123f

.field public static final timeline_facepile_4:I = 0x7f091240

.field public static final timeline_facepile_5:I = 0x7f091241

.field public static final timeline_facepile_6:I = 0x7f091242

.field public static final timeline_follow:I = 0x7f091221

.field public static final timeline_friend:I = 0x7f09121f

.field public static final timeline_friend_list_item_checkbox:I = 0x7f091234

.field public static final timeline_friend_list_item_name:I = 0x7f091233

.field public static final timeline_friend_request_progress:I = 0x7f090f57

.field public static final timeline_gift:I = 0x7f091223

.field public static final timeline_load_more_icon:I = 0x7f091237

.field public static final timeline_load_more_progress:I = 0x7f091238

.field public static final timeline_message:I = 0x7f091222

.field public static final timeline_name:I = 0x7f090d30

.field public static final timeline_nav_item_text:I = 0x7f0900e6

.field public static final timeline_nav_item_tile:I = 0x7f0900e7

.field public static final timeline_profile_pic:I = 0x7f090d2f

.field public static final timeline_profile_pic_image:I = 0x7f091244

.field public static final timeline_profile_question_disabled_item:I = 0x7f091247

.field public static final timeline_profile_question_disabled_item_icon:I = 0x7f091248

.field public static final timeline_profile_question_disabled_item_text:I = 0x7f091249

.field public static final timeline_profile_question_item_button:I = 0x7f09124a

.field public static final timeline_profile_question_item_chevron:I = 0x7f09124d

.field public static final timeline_profile_question_item_icon:I = 0x7f09124b

.field public static final timeline_profile_question_item_text:I = 0x7f09124c

.field public static final timeline_profile_questions_close_button:I = 0x7f091250

.field public static final timeline_profile_questions_section:I = 0x7f091252

.field public static final timeline_profile_questions_subtitle:I = 0x7f091251

.field public static final timeline_profile_questions_title:I = 0x7f09124f

.field public static final timeline_profile_questions_title_bar:I = 0x7f09124e

.field public static final timeline_section_progress:I = 0x7f09035f

.field public static final timeline_section_title:I = 0x7f091261

.field public static final timeline_sectiontitle_progress:I = 0x7f091262

.field public static final timeline_show_posts_container:I = 0x7f091267

.field public static final timeline_show_posts_divider:I = 0x7f091268

.field public static final timeline_show_posts_progress:I = 0x7f09126a

.field public static final timeline_show_posts_title:I = 0x7f091269

.field public static final timeline_subscribe:I = 0x7f091220

.field public static final timeline_year_overview_item_icon:I = 0x7f09126c

.field public static final timeline_year_overview_item_title:I = 0x7f09126d

.field public static final timeline_year_overview_items:I = 0x7f091266

.field public static final timeline_year_overview_items_divider:I = 0x7f09126b

.field public static final timeline_year_overview_photos:I = 0x7f091265

.field public static final timeline_year_overview_show_all_link:I = 0x7f09126e

.field public static final timeline_year_overview_title:I = 0x7f091264

.field public static final timer:I = 0x7f090c2c

.field public static final timestamp:I = 0x7f09068d

.field public static final title:I = 0x7f09005c

.field public static final titleAndLoading:I = 0x7f090a60

.field public static final titleDivider:I = 0x7f090a61

.field public static final title_1:I = 0x7f091125

.field public static final title_2:I = 0x7f091126

.field public static final title_bar:I = 0x7f09033f

.field public static final title_bar_text:I = 0x7f090341

.field public static final title_button_1:I = 0x7f090089

.field public static final title_button_2:I = 0x7f09008a

.field public static final title_button_3:I = 0x7f09008b

.field public static final title_button_4:I = 0x7f09008c

.field public static final title_container:I = 0x7f09010d

.field public static final title_layout:I = 0x7f090386

.field public static final title_layout_highlights_bottom:I = 0x7f091272

.field public static final title_layout_highlights_top:I = 0x7f091273

.field public static final title_open_menu_icon_view:I = 0x7f090388

.field public static final title_plus_share_selector:I = 0x7f091277

.field public static final title_progress:I = 0x7f09127f

.field public static final title_row_text:I = 0x7f09019d

.field public static final title_text:I = 0x7f09032f

.field public static final title_text_layout:I = 0x7f090387

.field public static final title_text_view:I = 0x7f090468

.field public static final titlebar:I = 0x7f0900be

.field public static final titlebar_back_button:I = 0x7f090cb4

.field public static final titlebar_button:I = 0x7f090cb8

.field public static final titlebar_button_container:I = 0x7f090cb7

.field public static final titlebar_button_layout:I = 0x7f090cb9

.field public static final titlebar_button_progress:I = 0x7f090cba

.field public static final titlebar_container:I = 0x7f090cb3

.field public static final titlebar_default_text_title:I = 0x7f090cb6

.field public static final titlebar_divider:I = 0x7f090b76

.field public static final titlebar_progress:I = 0x7f090cbe

.field public static final titlebar_progress_container:I = 0x7f090cbd

.field public static final titlebar_stub:I = 0x7f0900bd

.field public static final titlebar_text:I = 0x7f090cbb

.field public static final titlebar_text_container:I = 0x7f090cb5

.field public static final titlebar_title:I = 0x7f090cbc

.field public static final toast_bottom_divider:I = 0x7f090cc0

.field public static final toast_layout_root:I = 0x7f09117c

.field public static final toast_top_divider:I = 0x7f090cbf

.field public static final toggle_bookmark_button:I = 0x7f09091d

.field public static final toggle_switch:I = 0x7f091381

.field public static final top:I = 0x7f09000a

.field public static final top_action_bar:I = 0x7f0900fd

.field public static final top_container:I = 0x7f090919

.field public static final top_divider:I = 0x7f090b0a

.field public static final top_half:I = 0x7f090278

.field public static final top_shadow:I = 0x7f090402

.field public static final top_tip_container:I = 0x7f090aeb

.field public static final top_tip_text:I = 0x7f090aec

.field public static final topic_detail:I = 0x7f090eb8

.field public static final topic_image:I = 0x7f090aa5

.field public static final topic_name:I = 0x7f090eb7

.field public static final touch_overlay:I = 0x7f090fa8

.field public static final touch_receiver:I = 0x7f090408

.field public static final track_title:I = 0x7f090a35

.field public static final tracking_codes:I = 0x7f0900b6

.field public static final tracking_node:I = 0x7f0900bb

.field public static final transaction_date:I = 0x7f090ee6

.field public static final travel_welcome_header_view_actor:I = 0x7f091286

.field public static final travel_welcome_header_view_menu_button:I = 0x7f091287

.field public static final travel_welcome_header_view_title:I = 0x7f091288

.field public static final tumblr_button:I = 0x7f0901b4

.field public static final typeahead_empty_item_progress:I = 0x7f090aaa

.field public static final typeahead_item_row_container:I = 0x7f09128b

.field public static final typeahead_item_row_inner_container:I = 0x7f09128c

.field public static final typeahead_search_bar_view:I = 0x7f090fbc

.field public static final typeahead_search_result_row_image:I = 0x7f090fb9

.field public static final typeahead_search_result_row_name:I = 0x7f090fba

.field public static final typeahead_search_result_row_subtext:I = 0x7f090fbb

.field public static final typeahead_search_text:I = 0x7f090fbd

.field public static final typing_bubble_container:I = 0x7f090ccf

.field public static final uberbar_root_container:I = 0x7f091294

.field public static final uberbar_search_x:I = 0x7f091296

.field public static final ubersearch_result_item_actions:I = 0x7f09129a

.field public static final ubersearch_result_item_details:I = 0x7f09129c

.field public static final ubersearch_result_item_holder:I = 0x7f091298

.field public static final ubersearch_result_item_icon:I = 0x7f091299

.field public static final ubersearch_result_item_title:I = 0x7f09129b

.field public static final ubersearch_top_divider:I = 0x7f09129d

.field public static final ufi_bar:I = 0x7f0912b1

.field public static final ufi_bling_bar_container:I = 0x7f0912b5

.field public static final ufi_bling_mini_names:I = 0x7f0912b9

.field public static final ufi_bling_text:I = 0x7f0912b6

.field public static final ufi_comment_button:I = 0x7f0912b3

.field public static final ufi_comment_text_view:I = 0x7f0912b8

.field public static final ufi_like_button:I = 0x7f0912b2

.field public static final ufi_like_text_view:I = 0x7f0912b7

.field public static final ufi_more_options_button:I = 0x7f0912b4

.field public static final ufi_separator_bar:I = 0x7f0912b0

.field public static final ufi_third_party_link:I = 0x7f0912ba

.field public static final ufi_view:I = 0x7f09044a

.field public static final ufiservices_comment_retry:I = 0x7f0906ff

.field public static final ufiservices_flyout_actors_arrow:I = 0x7f09071b

.field public static final ufiservices_flyout_actors_holder:I = 0x7f090719

.field public static final ufiservices_flyout_actors_textview:I = 0x7f09071a

.field public static final ufiservices_flyout_annotations_like_separator:I = 0x7f090703

.field public static final ufiservices_flyout_annotations_reply_separator:I = 0x7f091216

.field public static final ufiservices_flyout_button_holder:I = 0x7f090717

.field public static final ufiservices_flyout_comment_edit_attachment:I = 0x7f090315

.field public static final ufiservices_flyout_comment_edit_cancel:I = 0x7f090313

.field public static final ufiservices_flyout_comment_edit_textview:I = 0x7f090312

.field public static final ufiservices_flyout_comment_edit_update:I = 0x7f090314

.field public static final ufiservices_flyout_comment_like:I = 0x7f090704

.field public static final ufiservices_flyout_comment_like_button_image_view:I = 0x7f090707

.field public static final ufiservices_flyout_comment_like_count:I = 0x7f090708

.field public static final ufiservices_flyout_comment_like_separator:I = 0x7f090705

.field public static final ufiservices_flyout_comment_liker_holder:I = 0x7f090706

.field public static final ufiservices_flyout_comment_loading:I = 0x7f09070b

.field public static final ufiservices_flyout_comment_metadata:I = 0x7f0906fe

.field public static final ufiservices_flyout_comment_reply:I = 0x7f091217

.field public static final ufiservices_flyout_comment_reply_box:I = 0x7f09070a

.field public static final ufiservices_flyout_comment_reply_count:I = 0x7f091215

.field public static final ufiservices_flyout_comment_reply_link:I = 0x7f090709

.field public static final ufiservices_flyout_comment_text_view:I = 0x7f0906fb

.field public static final ufiservices_flyout_divider_view:I = 0x7f090715

.field public static final ufiservices_flyout_like_button:I = 0x7f090718

.field public static final ufiservices_flyout_more_comments_icon:I = 0x7f090720

.field public static final ufiservices_flyout_more_comments_spinner:I = 0x7f090721

.field public static final ufiservices_flyout_more_comments_text:I = 0x7f090722

.field public static final ufiservices_flyout_name_text_view:I = 0x7f0906fa

.field public static final ufiservices_flyout_profile_annotations_text_view:I = 0x7f090702

.field public static final ufiservices_flyout_profile_button_view:I = 0x7f09071c

.field public static final ufiservices_flyout_profile_extra_view:I = 0x7f09071e

.field public static final ufiservices_flyout_profile_image_view:I = 0x7f090311

.field public static final ufiservices_flyout_profile_name_view:I = 0x7f090716

.field public static final ufiservices_flyout_profile_presence_indicator:I = 0x7f09071d

.field public static final ufiservices_flyout_profile_time_text_view:I = 0x7f090700

.field public static final ufiservices_flyout_reply_name_text_view:I = 0x7f091214

.field public static final ufiservices_flyout_reply_profile_image_view:I = 0x7f091213

.field public static final ufiservices_flyout_time_annotations_separator:I = 0x7f090701

.field public static final ufiservices_menu_item_cancel:I = 0x7f0913ac

.field public static final ufiservices_menu_item_delete:I = 0x7f0913ab

.field public static final ufiservices_menu_item_try_again:I = 0x7f0913aa

.field public static final ufiservices_threaded_comment:I = 0x7f091212

.field public static final unconstrainedPan:I = 0x7f090036

.field public static final underlying_image:I = 0x7f0903cf

.field public static final underwood_attachments:I = 0x7f090381

.field public static final underwood_nux:I = 0x7f09031f

.field public static final underwood_nux_view_stub:I = 0x7f09031e

.field public static final underwood_top_placeholder:I = 0x7f090376

.field public static final unit_footer:I = 0x7f0908b1

.field public static final unit_footer_horizontal_divider:I = 0x7f0908b0

.field public static final unit_footer_padding:I = 0x7f0908b2

.field public static final unit_horizontal_divider:I = 0x7f0908af

.field public static final unit_title:I = 0x7f0908ac

.field public static final unit_title_chevron:I = 0x7f0908ab

.field public static final unit_title_container:I = 0x7f0908aa

.field public static final unknown_data_prompt:I = 0x7f0912bf

.field public static final unlock_gradient:I = 0x7f090404

.field public static final unlock_text_view:I = 0x7f090407

.field public static final unlock_text_view_container:I = 0x7f090406

.field public static final unread_message_pill_text:I = 0x7f090cd3

.field public static final unsubscribe_button:I = 0x7f0911ca

.field public static final up:I = 0x7f090055

.field public static final update_button:I = 0x7f090499

.field public static final update_list_container:I = 0x7f090ca6

.field public static final upsell_button_progress_bar:I = 0x7f0912c7

.field public static final upsell_button_text:I = 0x7f0912c6

.field public static final upsell_content_left_icon:I = 0x7f0912c8

.field public static final upsell_content_message:I = 0x7f0912cb

.field public static final upsell_content_right_text:I = 0x7f0912cc

.field public static final upsell_content_text:I = 0x7f0912ca

.field public static final upsell_content_title_text:I = 0x7f0912c9

.field public static final upsell_dialog_content:I = 0x7f0912c4

.field public static final upsell_dialog_header:I = 0x7f0912c3

.field public static final upsell_dialog_title:I = 0x7f0912c0

.field public static final upsell_dialog_top_divider:I = 0x7f0912c1

.field public static final upsell_header_divider:I = 0x7f0912d1

.field public static final upsell_header_left_icon:I = 0x7f0912cf

.field public static final upsell_header_text:I = 0x7f0912d0

.field public static final upsell_primary_content_button:I = 0x7f0912cd

.field public static final upsell_progress_bar:I = 0x7f0912c5

.field public static final upsell_scroll_view:I = 0x7f0912c2

.field public static final upsell_secondary_content_button:I = 0x7f0912ce

.field public static final url:I = 0x7f0900a9

.field public static final url_icon:I = 0x7f09128e

.field public static final url_image:I = 0x7f090ba3

.field public static final url_image_cover:I = 0x7f090cd9

.field public static final url_image_image:I = 0x7f090cd4

.field public static final url_image_placeholder:I = 0x7f090cd6

.field public static final url_image_progress:I = 0x7f0912d2

.field public static final url_image_retry:I = 0x7f0912d3

.field public static final url_image_upload_cover:I = 0x7f090cd7

.field public static final url_image_upload_progress:I = 0x7f090cd8

.field public static final urlimage_avatar:I = 0x7f091068

.field public static final urlimage_card_icon:I = 0x7f090cea

.field public static final urlimage_credit_card_icon:I = 0x7f090cf8

.field public static final urlimage_icon:I = 0x7f09092c

.field public static final urlimage_image:I = 0x7f09028d

.field public static final urlimage_item:I = 0x7f090933

.field public static final urlimage_product:I = 0x7f09083a

.field public static final urlimage_product_image:I = 0x7f090f90

.field public static final urlimage_profile:I = 0x7f090843

.field public static final urlimage_profile_pic:I = 0x7f09084e

.field public static final urlimage_recipient_avatar:I = 0x7f090cfa

.field public static final usage_description_container:I = 0x7f0901aa

.field public static final useLogo:I = 0x7f09003e

.field public static final useParticipantsNamesOnly:I = 0x7f090082

.field public static final useThreadNameIfAvailable:I = 0x7f090081

.field public static final use_button:I = 0x7f091149

.field public static final use_button_group:I = 0x7f091148

.field public static final use_button_text:I = 0x7f09114a

.field public static final use_current_wallpaper:I = 0x7f0908a2

.field public static final use_current_wallpaper_bottom_divider:I = 0x7f0908a3

.field public static final useless_view:I = 0x7f09033b

.field public static final user_badge_image:I = 0x7f090b18

.field public static final user_display:I = 0x7f090428

.field public static final user_image:I = 0x7f09135c

.field public static final user_name:I = 0x7f0902b4

.field public static final user_photo:I = 0x7f090134

.field public static final user_profile_img:I = 0x7f090c3b

.field public static final user_profile_pic:I = 0x7f090460

.field public static final user_status:I = 0x7f09135b

.field public static final username:I = 0x7f090453

.field public static final username_text:I = 0x7f09062a

.field public static final value:I = 0x7f090d19

.field public static final value_image:I = 0x7f090859

.field public static final value_text:I = 0x7f09085a

.field public static final vault_delete_progress_bar:I = 0x7f0912e1

.field public static final vault_error_banner:I = 0x7f0912ed

.field public static final vault_error_banner_image:I = 0x7f0912ee

.field public static final vault_error_banner_text:I = 0x7f0912ef

.field public static final vault_fail_icon:I = 0x7f0912df

.field public static final vault_icon_layout:I = 0x7f0912ea

.field public static final vault_nux_flyout:I = 0x7f0912f3

.field public static final vault_nux_flyout_close_button:I = 0x7f0912f6

.field public static final vault_nux_flyout_desc:I = 0x7f0912f5

.field public static final vault_nux_lock:I = 0x7f0912f4

.field public static final vault_optin_image:I = 0x7f0912e2

.field public static final vault_refresh_icon:I = 0x7f0912eb

.field public static final vault_spinner:I = 0x7f0912e0

.field public static final vault_sync_older_photos_menu_item:I = 0x7f0900e9

.field public static final vault_sync_privacy_header_text:I = 0x7f0912e9

.field public static final vault_sync_settings_button_sync_all:I = 0x7f0912f8

.field public static final vault_sync_settings_button_sync_off:I = 0x7f0912fa

.field public static final vault_sync_settings_button_sync_wifi:I = 0x7f0912f9

.field public static final vault_sync_settings_quota:I = 0x7f0912fe

.field public static final vault_sync_settings_radio_group:I = 0x7f0912f7

.field public static final vault_sync_settings_used:I = 0x7f0912ff

.field public static final vault_try_again:I = 0x7f0912de

.field public static final vertical:I = 0x7f090029

.field public static final verticalPan:I = 0x7f090035

.field public static final vertical_bar:I = 0x7f090a7a

.field public static final vertical_divider_1:I = 0x7f09091a

.field public static final vertical_divider_2:I = 0x7f09091c

.field public static final video_attachment_inline_player:I = 0x7f0908e4

.field public static final video_camera_icon:I = 0x7f091147

.field public static final video_cancel_screen:I = 0x7f090c32

.field public static final video_capture_button:I = 0x7f09025d

.field public static final video_chaining_item_image:I = 0x7f091301

.field public static final video_choose_button:I = 0x7f09025f

.field public static final video_cover_image:I = 0x7f091300

.field public static final video_end_screen_call_to_action_button:I = 0x7f091309

.field public static final video_end_screen_call_to_action_container:I = 0x7f091308

.field public static final video_end_screen_call_to_action_source:I = 0x7f09130b

.field public static final video_end_screen_call_to_action_title:I = 0x7f09130a

.field public static final video_end_screen_on_full_screen_player:I = 0x7f090259

.field public static final video_end_screen_replay_button:I = 0x7f091306

.field public static final video_end_screen_replay_container:I = 0x7f091305

.field public static final video_end_screen_replay_label:I = 0x7f091307

.field public static final video_gallery_option:I = 0x7f090275

.field public static final video_graphql_object:I = 0x7f0900d0

.field public static final video_grid_icon:I = 0x7f090346

.field public static final video_icon:I = 0x7f0912d9

.field public static final video_item_player_controls:I = 0x7f090f5e

.field public static final video_length:I = 0x7f090bcb

.field public static final video_play_icon:I = 0x7f0909e1

.field public static final video_player_container:I = 0x7f0908f2

.field public static final video_player_controls:I = 0x7f0908e6

.field public static final video_preview:I = 0x7f090cda

.field public static final video_spec_display:I = 0x7f0908ef

.field public static final video_suggestion_like_count:I = 0x7f091318

.field public static final video_suggestion_likes_view_layout:I = 0x7f091317

.field public static final video_suggestion_video_author:I = 0x7f091316

.field public static final video_suggestion_video_image:I = 0x7f091314

.field public static final video_suggestion_video_title:I = 0x7f091315

.field public static final video_suggestion_view_count:I = 0x7f091319

.field public static final video_trimming_edited:I = 0x7f091326

.field public static final video_trimming_editing_container:I = 0x7f091323

.field public static final video_trimming_film_strip:I = 0x7f091324

.field public static final video_trimming_inline_play_button:I = 0x7f09132b

.field public static final video_trimming_inline_video_player:I = 0x7f09132a

.field public static final video_trimming_layout:I = 0x7f091321

.field public static final video_trimming_metadata_video_duration:I = 0x7f091328

.field public static final video_trimming_metadata_video_label:I = 0x7f091327

.field public static final video_trimming_metadata_video_size:I = 0x7f091329

.field public static final video_trimming_original:I = 0x7f091325

.field public static final video_trimming_preview:I = 0x7f091322

.field public static final video_trimming_root:I = 0x7f091320

.field public static final videoview_camera_review:I = 0x7f091310

.field public static final view_archived_messages:I = 0x7f09008f

.field public static final view_button:I = 0x7f090c4d

.field public static final view_group_settings:I = 0x7f09139b

.field public static final view_map:I = 0x7f0913a7

.field public static final view_more_divider:I = 0x7f090b7b

.field public static final view_more_text:I = 0x7f090b7a

.field public static final view_other_messages:I = 0x7f090090

.field public static final view_pager:I = 0x7f090c7b

.field public static final view_people:I = 0x7f0913a0

.field public static final view_shared_image_history:I = 0x7f0913a3

.field public static final visible:I = 0x7f090083

.field public static final voice_clip_button:I = 0x7f090cce

.field public static final voip_action_button:I = 0x7f090b44

.field public static final voip_action_section:I = 0x7f090b43

.field public static final voip_call:I = 0x7f0913a4

.field public static final voip_call_button:I = 0x7f090b65

.field public static final voip_call_status_bar:I = 0x7f09132f

.field public static final voip_rating_description:I = 0x7f091330

.field public static final voip_stars_row:I = 0x7f091331

.field public static final voip_status_message:I = 0x7f09133e

.field public static final voip_survey_textarea:I = 0x7f091335

.field public static final wallpaper:I = 0x7f0903fa

.field public static final warning_container:I = 0x7f090b88

.field public static final weather_container:I = 0x7f0902c5

.field public static final weather_text_view:I = 0x7f0902c6

.field public static final webView:I = 0x7f090624

.field public static final web_product_description:I = 0x7f090f8c

.field public static final webview:I = 0x7f090242

.field public static final white_bg_view:I = 0x7f0908d3

.field public static final widget_button_next:I = 0x7f091360

.field public static final widget_button_prev:I = 0x7f09135f

.field public static final widget_notification_count:I = 0x7f091357

.field public static final wifi_data_use:I = 0x7f090452

.field public static final withBottomOfAnchor:I = 0x7f09006c

.field public static final withHorizontalCenterOfAnchor:I = 0x7f090068

.field public static final withLeftOfAnchor:I = 0x7f090067

.field public static final withRightOfAnchor:I = 0x7f090069

.field public static final withText:I = 0x7f09004a

.field public static final withTopOfAnchor:I = 0x7f09006a

.field public static final withVerticalCenterOfAnchor:I = 0x7f09006b

.field public static final wrap_content:I = 0x7f090058

.field public static final wrapped_gift:I = 0x7f091138

.field public static final wrapper:I = 0x7f091289

.field public static final wv:I = 0x7f0908b5

.field public static final x_out_button:I = 0x7f091005

.field public static final xlarge:I = 0x7f09005b

.field public static final zero_incoming_call_banner:I = 0x7f091342

.field public static final zero_rating_bottom_banner:I = 0x7f090c8c

.field public static final zero_rating_bottom_banner_stub:I = 0x7f090c8b


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
