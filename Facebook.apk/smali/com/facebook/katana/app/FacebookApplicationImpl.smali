.class public Lcom/facebook/katana/app/FacebookApplicationImpl;
.super Lcom/facebook/base/app/AbstractApplicationLike;
.source "FacebookApplicationImpl.java"

# interfaces
.implements Lcom/facebook/inject/FbInjectorProvider;
.implements Lcom/facebook/resources/HasOverridingResources;


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/facebook/nobreak/CatchMeIfYouCan;

.field private final e:Lcom/facebook/nobreak/RecoveryModeHelper;

.field private final f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

.field private final g:Z

.field private h:Lcom/facebook/katana/app/module/FbandroidProcessName;

.field private final i:J

.field private j:Lcom/facebook/base/app/LightweightPerfEvents;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/facebook/katana/app/FacebookApplication;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/katana/app/FacebookApplicationImpl;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/facebook/config/application/FbAppType;Lcom/facebook/nobreak/CatchMeIfYouCan;Lcom/facebook/nobreak/RecoveryModeHelper;Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;ZJLcom/facebook/base/app/LightweightPerfEvents;)V
    .locals 0

    invoke-direct {p0, p1, p2, p9}, Lcom/facebook/base/app/AbstractApplicationLike;-><init>(Landroid/app/Application;Lcom/facebook/config/application/FbAppType;Lcom/facebook/base/app/LightweightPerfEvents;)V

    iput-object p3, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->d:Lcom/facebook/nobreak/CatchMeIfYouCan;

    iput-object p4, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->e:Lcom/facebook/nobreak/RecoveryModeHelper;

    iput-object p5, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iput-boolean p6, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->g:Z

    iput-wide p7, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->i:J

    iput-object p1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->c:Landroid/content/Context;

    iput-object p9, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    return-void
.end method

.method private a(Lcom/facebook/inject/FbInjector;)V
    .locals 6

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v0, v0, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    invoke-virtual {p1, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v1, v1, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v2, v2, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v1, v1, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v3, v1

    :goto_0
    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v3, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    const-string v5, "DexLibLoader_ERROR_RECOVERY"

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v1, v1, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->f:Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    iget-object v2, v2, Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    invoke-interface {v0, v5, v1, v2}, Lcom/facebook/common/errorreporting/FbErrorReporter;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_0
    return-void

    :cond_1
    move v3, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/facebook/katana/app/FacebookApplicationImpl;Lcom/facebook/inject/FbInjector;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Lcom/facebook/inject/FbInjector;)V

    return-void
.end method

.method static synthetic a(Lcom/facebook/katana/app/FacebookApplicationImpl;Lcom/facebook/perf/StartupPerfLogger;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Lcom/facebook/perf/StartupPerfLogger;)V

    return-void
.end method

.method private a(Lcom/facebook/perf/StartupPerfLogger;)V
    .locals 2

    const-string v0, "NNFCold_AppCreateToLoginActivityCreate"

    invoke-virtual {p1, v0}, Lcom/facebook/perf/StartupPerfLogger;->a(Ljava/lang/String;)Lcom/facebook/perf/StartupPerfLogger;

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    new-instance v1, Lcom/facebook/katana/app/FacebookApplicationImpl$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/katana/app/FacebookApplicationImpl$2;-><init>(Lcom/facebook/katana/app/FacebookApplicationImpl;Lcom/facebook/perf/StartupPerfLogger;)V

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Lcom/facebook/base/app/LightweightPerfEvents$Marker;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    return-void
.end method

.method private a(Lcom/facebook/perf/StartupPerfLogger;Lcom/facebook/performancelogger/PerformanceLogger;J)V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    sget-object v0, Lcom/facebook/katana/app/module/FbandroidProcessName;->DASH:Lcom/facebook/katana/app/module/FbandroidProcessName;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    invoke-virtual {v0, v1}, Lcom/facebook/katana/app/module/FbandroidProcessName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/facebook/performancelogger/MarkerConfig;

    const-string v1, "DashColdStart"

    invoke-direct {v0, v1}, Lcom/facebook/performancelogger/MarkerConfig;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Lcom/facebook/performancelogger/MarkerConfig;->a(J)Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/facebook/analytics/tagging/AnalyticsTag;

    sget-object v2, Lcom/facebook/analytics/tagging/AnalyticsTag;->DASH_ACTIVITY_NAME:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v2, v1, v7

    sget-object v2, Lcom/facebook/analytics/tagging/AnalyticsTag;->DASH_SPLASH_ANALYTICS_NAME:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v2, v1, v8

    sget-object v2, Lcom/facebook/analytics/tagging/AnalyticsTag;->MODULE_DASH:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v2, v1, v9

    const/4 v2, 0x3

    sget-object v3, Lcom/facebook/analytics/tagging/AnalyticsTag;->MODULE_DASH_LAUNCHABLES:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/performancelogger/MarkerConfig;->a([Lcom/facebook/analytics/tagging/AnalyticsTag;)Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/performancelogger/MarkerConfig;->a()Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b(Lcom/facebook/performancelogger/MarkerConfig;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    invoke-virtual {v0}, Lcom/facebook/katana/app/module/FbandroidProcessName;->getProcessName()Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/process/ProcessName;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->i:J

    const-string v2, "NNFColdStartTTI"

    const-string v3, "ColdStart"

    const-string v4, "NNFFirstRunColdStart"

    const-string v5, "NNFColdStart"

    const-string v6, "NNFCacheColdStart"

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/facebook/perf/StartupPerfLogger;->a(JLjava/util/List;)Lcom/facebook/perf/StartupPerfLogger;

    new-instance v0, Lcom/facebook/performancelogger/MarkerConfig;

    const-string v1, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-direct {v0, v1}, Lcom/facebook/performancelogger/MarkerConfig;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->i:J

    invoke-virtual {v0, v1, v2}, Lcom/facebook/performancelogger/MarkerConfig;->a(J)Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    new-array v1, v9, [Lcom/facebook/analytics/tagging/AnalyticsTag;

    sget-object v2, Lcom/facebook/analytics/tagging/AnalyticsTag;->STORY_VIEW:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v2, v1, v7

    sget-object v2, Lcom/facebook/apptab/state/TabTag;->Notifications:Lcom/facebook/apptab/state/TabTag;

    iget-object v2, v2, Lcom/facebook/apptab/state/TabTag;->analyticsTag:Lcom/facebook/analytics/tagging/AnalyticsTag;

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/facebook/performancelogger/MarkerConfig;->a([Lcom/facebook/analytics/tagging/AnalyticsTag;)Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/performancelogger/MarkerConfig;->a()Lcom/facebook/performancelogger/MarkerConfig;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->b(Lcom/facebook/performancelogger/MarkerConfig;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Lcom/google/common/base/Strings;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->c:Landroid/content/Context;

    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/facebook/katana/app/FacebookApplicationImpl;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to disableReceiver: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static e(Lcom/facebook/common/process/ProcessName;)Lcom/facebook/katana/app/module/FbandroidProcessName;
    .locals 4

    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid process name: unknown."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/katana/app/module/FbandroidProcessName;->MAIN:Lcom/facebook/katana/app/module/FbandroidProcessName;

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/katana/app/module/FbandroidProcessName;->valueOf(Ljava/lang/String;)Lcom/facebook/katana/app/module/FbandroidProcessName;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized process name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private h()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.facebook.prefs.multiprocess.earlystart.PingProviders"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private i()V
    .locals 5

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->f()Lcom/facebook/inject/FbInjector;

    move-result-object v1

    const-class v0, Lcom/facebook/common/build/SignatureType;

    invoke-virtual {v1, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/build/SignatureType;

    if-eqz v0, :cond_0

    sget-object v2, Lcom/facebook/common/build/SignatureType;->DEBUG:Lcom/facebook/common/build/SignatureType;

    if-ne v0, v2, :cond_0

    new-instance v2, Lcom/facebook/strictmode/StrictModeAggregator;

    const-class v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    invoke-virtual {v1, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    const-class v3, Ljava/util/Random;

    const-class v4, Lcom/facebook/common/random/InsecureRandom;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Random;

    invoke-direct {v2, v0, v1}, Lcom/facebook/strictmode/StrictModeAggregator;-><init>(Lcom/facebook/common/errorreporting/FbErrorReporter;Ljava/util/Random;)V

    invoke-virtual {v2}, Lcom/facebook/strictmode/StrictModeAggregator;->a()Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;)V
    .locals 4

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a()Landroid/app/Application;

    move-result-object v0

    new-instance v1, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigImpl;

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/facebook/prefs/multiprocess/FbSharedPreferencesMultiprocessConfig;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v0, v2}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigImpl;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    const-string v2, "fbandroid_shared_preferences_providers_process"

    sget-object v3, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;->Gatekeeper:Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;

    invoke-virtual {p1, v2, v3, v1}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->a(Ljava/lang/String;Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;Lcom/facebook/multiprocess/experiment/config/MultiprocessConfig;)V

    new-instance v1, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigImpl;

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/facebook/ipc/notifications/GraphQLNotificationsContentProviderMultiprocessConfig;

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v1, v0, v2}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigImpl;-><init>(Landroid/content/Context;Landroid/content/ComponentName;)V

    sget-object v0, Lcom/facebook/ipc/notifications/GraphQLNotificationsContentProviderMultiprocessConfig;->a:Ljava/lang/String;

    sget-object v2, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;->SharedPreference:Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;

    invoke-virtual {p1, v0, v2, v1}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->a(Ljava/lang/String;Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry$ConfigType;Lcom/facebook/multiprocess/experiment/config/MultiprocessConfig;)V

    return-void
.end method

.method protected final a(Lcom/facebook/common/process/ProcessName;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->e(Lcom/facebook/common/process/ProcessName;)Lcom/facebook/katana/app/module/FbandroidProcessName;

    move-result-object v1

    sget-object v2, Lcom/facebook/katana/app/module/FbandroidProcessName;->DASH:Lcom/facebook/katana/app/module/FbandroidProcessName;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/facebook/katana/app/module/FbandroidProcessName;->DASH_SERVICE:Lcom/facebook/katana/app/module/FbandroidProcessName;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final b(Lcom/facebook/common/process/ProcessName;)V
    .locals 3

    invoke-static {}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->a()Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->e(Lcom/facebook/common/process/ProcessName;)Lcom/facebook/katana/app/module/FbandroidProcessName;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    sget-object v1, Lcom/facebook/katana/app/module/FbandroidProcessName;->MAIN:Lcom/facebook/katana/app/module/FbandroidProcessName;

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/facebook/ipc/notifications/GraphQLNotificationsContentProviderMultiprocessConfig;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "fbandroid_shared_preferences_providers_process"

    invoke-virtual {v0, v1}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->h()V

    :cond_1
    return-void
.end method

.method protected final c(Lcom/facebook/common/process/ProcessName;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/process/ProcessName;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/inject/Module;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/facebook/katana/app/FacebookApplicationImpl;->e(Lcom/facebook/common/process/ProcessName;)Lcom/facebook/katana/app/module/FbandroidProcessName;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    new-instance v0, Lcom/facebook/katana/app/module/FbandroidAppModule;

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->b()Lcom/facebook/config/application/FbAppType;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->d:Lcom/facebook/nobreak/CatchMeIfYouCan;

    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->e:Lcom/facebook/nobreak/RecoveryModeHelper;

    iget-object v4, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->h:Lcom/facebook/katana/app/module/FbandroidProcessName;

    iget-boolean v5, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->g:Z

    invoke-direct/range {v0 .. v5}, Lcom/facebook/katana/app/module/FbandroidAppModule;-><init>(Lcom/facebook/config/application/FbAppType;Lcom/facebook/nobreak/CatchMeIfYouCan;Lcom/facebook/nobreak/RecoveryModeHelper;Lcom/facebook/katana/app/module/FbandroidProcessName;Z)V

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 5

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBAppImpl.onCreate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->d(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ApplicationOnCreate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-super {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->c()V

    invoke-static {}, Lcom/facebook/common/errorreporting/AcraBLogBridge;->a()V

    const-class v0, Lcom/google/common/base/FinalizableReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Forcing crash (debuggable=true in release build)"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ApplicationOnCreate"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_1
    const-string v0, "FacebookApplicationImpl.onCreate"

    invoke-static {v0}, Lcom/facebook/debug/tracer/Tracer;->b(Ljava/lang/String;)Lcom/facebook/debug/tracer/Tracer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->f()Lcom/facebook/inject/FbInjector;

    move-result-object v4

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->i()V

    invoke-virtual {v0}, Lcom/facebook/debug/tracer/Tracer;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->j:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ApplicationOnCreate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/facebook/perf/StartupPerfLogger;->a(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/perf/StartupPerfLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/perf/StartupPerfLogger;

    const-class v1, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-virtual {v4, v1}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Lcom/facebook/perf/StartupPerfLogger;Lcom/facebook/performancelogger/PerformanceLogger;J)V

    invoke-static {v4}, Lcom/facebook/common/init/AppInitLock;->a(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/common/init/AppInitLock;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/init/AppInitLock;

    new-instance v2, Lcom/facebook/katana/app/FacebookApplicationImpl$1;

    invoke-direct {v2, p0, v0, v4}, Lcom/facebook/katana/app/FacebookApplicationImpl$1;-><init>(Lcom/facebook/katana/app/FacebookApplicationImpl;Lcom/facebook/perf/StartupPerfLogger;Lcom/facebook/inject/FbInjector;)V

    invoke-virtual {v1, v2}, Lcom/facebook/common/init/AppInitLock;->a(Lcom/facebook/common/init/AppInitLock$Listener;)V

    const-class v0, Lcom/facebook/common/appstate/AppStateManager;

    invoke-virtual {v4, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/appstate/AppStateManager;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/common/appstate/AppStateManager;->b(J)V

    return-void
.end method

.method protected final d()V
    .locals 1

    const-string v0, "com.facebook.auth.broadcast.CrossProcessLogoutReceiver"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    const-string v0, "com.facebook.home.launchables.receiver.LaunchablesReceiver"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    const-string v0, "com.facebook.dash.nobreak.DisableDashIntentReceiver"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    const-string v0, "com.facebook.dash.service.KeyguardServiceBooter"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    const-string v0, "com.facebook.dash.service.DashSsoPreloadReceiver"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    const-string v0, "com.facebook.dash.receivers.DashPackageUninstallReceiver"

    invoke-direct {p0, v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected final d(Lcom/facebook/common/process/ProcessName;)V
    .locals 3

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->f()Lcom/facebook/inject/FbInjector;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/backgroundtasks/BackgroundTaskController;->a(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/backgroundtasks/BackgroundTaskController;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundtasks/BackgroundTaskController;

    invoke-virtual {p1}, Lcom/facebook/common/process/ProcessName;->d()Z

    move-result v2

    if-nez v2, :cond_0

    const-class v1, Lcom/facebook/contacts/database/ContactsTaskTag;

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundtasks/BackgroundTaskController;->a(Ljava/lang/Class;)V

    const-class v1, Lcom/facebook/orca/background/MessagesDataTaskTag;

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundtasks/BackgroundTaskController;->a(Ljava/lang/Class;)V

    const-class v1, Lcom/facebook/orca/background/MessagesLocalTaskTag;

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundtasks/BackgroundTaskController;->a(Ljava/lang/Class;)V

    :goto_0
    return-void

    :cond_0
    const-class v0, Lcom/facebook/common/appstate/AppStateManager;

    invoke-virtual {v1, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/appstate/AppStateManager;

    iget-wide v1, p0, Lcom/facebook/katana/app/FacebookApplicationImpl;->i:J

    invoke-virtual {v0, v1, v2}, Lcom/facebook/common/appstate/AppStateManager;->a(J)V

    goto :goto_0
.end method
