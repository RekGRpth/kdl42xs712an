.class public Lcom/facebook/katana/app/FacebookApplication;
.super Lcom/facebook/base/app/DelegatingApplication;
.source "FacebookApplication.java"

# interfaces
.implements Lcom/facebook/inject/FbInjectorProvider;
.implements Lcom/facebook/resources/HasBaseResourcesAccess;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/app/DelegatingApplication",
        "<",
        "Lcom/facebook/base/app/ApplicationLike;",
        ">;",
        "Lcom/facebook/inject/FbInjectorProvider;",
        "Lcom/facebook/resources/HasBaseResourcesAccess;"
    }
.end annotation


# static fields
.field private static final g:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

.field private static final h:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;


# instance fields
.field private a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/config/application/FbAppType;

.field private c:Lcom/facebook/nobreak/CatchMeIfYouCan;

.field private d:J

.field private e:Lcom/facebook/base/app/LightweightPerfEvents;

.field private f:Lcom/facebook/nobreak/RecoveryModeHelper;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v2, 0x6

    const-wide/32 v9, 0x1d4c0

    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v11, 0x0

    new-array v12, v5, [Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    new-instance v0, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const-string v1, "dash"

    const-wide/32 v3, 0xea60

    move v6, v5

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;-><init>(Ljava/lang/String;IJZZZ)V

    aput-object v0, v12, v11

    sput-object v12, Lcom/facebook/katana/app/FacebookApplication;->g:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const/4 v0, 0x4

    new-array v14, v0, [Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    new-instance v6, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const-string v7, ""

    move v12, v11

    move v13, v11

    invoke-direct/range {v6 .. v13}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;-><init>(Ljava/lang/String;IJZZZ)V

    aput-object v6, v14, v11

    new-instance v6, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const-string v7, "dash_service"

    move v12, v11

    move v13, v11

    invoke-direct/range {v6 .. v13}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;-><init>(Ljava/lang/String;IJZZZ)V

    aput-object v6, v14, v5

    const/4 v0, 0x2

    new-instance v6, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const-string v7, "providers"

    move v12, v11

    move v13, v11

    invoke-direct/range {v6 .. v13}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;-><init>(Ljava/lang/String;IJZZZ)V

    aput-object v6, v14, v0

    new-instance v0, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    const-string v1, "dash"

    const-wide/32 v3, 0xea60

    move v5, v11

    move v6, v11

    move v7, v11

    invoke-direct/range {v0 .. v7}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;-><init>(Ljava/lang/String;IJZZZ)V

    aput-object v0, v14, v8

    sput-object v14, Lcom/facebook/katana/app/FacebookApplication;->h:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/facebook/base/app/DelegatingApplication;-><init>()V

    const-class v0, Lcom/facebook/katana/app/FacebookApplication;

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->a:Ljava/lang/Class;

    new-instance v0, Lcom/facebook/base/app/LightweightPerfEvents;

    invoke-direct {v0}, Lcom/facebook/base/app/LightweightPerfEvents;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    return-void
.end method

.method private a(Lcom/facebook/acra/ErrorReporter;)V
    .locals 4

    new-instance v0, Lcom/facebook/common/dextricks/DexDiagnostics;

    invoke-direct {v0, p0}, Lcom/facebook/common/dextricks/DexDiagnostics;-><init>(Landroid/content/Context;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/facebook/common/dextricks/DexDiagnostics;->a()I

    move-result v1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    const-string v3, "application_uid"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v3, v2}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "directory_owner_uid"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "/proc/mounts"

    const-string v2, " /data "

    invoke-static {v1, v2}, Lcom/facebook/common/dextricks/DexDiagnostics;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mounts_file"

    invoke-virtual {p1, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "/proc/uptime"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/common/dextricks/DexDiagnostics;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "uptime_file"

    invoke-virtual {p1, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "/proc/loadavg"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/common/dextricks/DexDiagnostics;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "loadavg_file"

    invoke-virtual {p1, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/common/dextricks/DexDiagnostics;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dex_directories"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/facebook/common/dextricks/DexDiagnostics;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "logcat_dump"

    invoke-virtual {p1, v1, v0}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-class v1, Lcom/facebook/katana/app/FacebookApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to read directory owner uid for ACRA report."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private e()Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Landroid/content/Context;Z)Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;

    move-result-object v0

    invoke-static {p0}, Lcom/facebook/common/dextricks/DexLibLoader;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/facebook/acra/ErrorReporter;->getInstance()Lcom/facebook/acra/ErrorReporter;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/katana/app/FacebookApplication;->a(Lcom/facebook/acra/ErrorReporter;)V

    invoke-static {}, Lcom/facebook/acra/ErrorReporter;->getInstance()Lcom/facebook/acra/ErrorReporter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/acra/ErrorReporter;->handleException(Ljava/lang/Throwable;)Lcom/facebook/acra/ErrorReporter$ReportsSenderWorker;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unreachable"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private g()Lcom/facebook/config/application/FbAppType;
    .locals 5

    :try_start_0
    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/katana/app/FacebookAppTypes;->d:Lcom/facebook/config/application/FbAppType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/katana/app/FacebookAppTypes;->e:Lcom/facebook/config/application/FbAppType;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->c()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/util/SecureHashUtil;->a([B)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/katana/app/FacebookAppTypes;->a:Lcom/facebook/config/application/FbAppType;

    invoke-virtual {v4}, Lcom/facebook/config/application/FbAppType;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v0, Lcom/facebook/katana/app/FacebookAppTypes;->a:Lcom/facebook/config/application/FbAppType;

    goto :goto_0

    :cond_2
    sget-object v4, Lcom/facebook/katana/app/FacebookAppTypes;->b:Lcom/facebook/config/application/FbAppType;

    invoke-virtual {v4}, Lcom/facebook/config/application/FbAppType;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v0, Lcom/facebook/katana/app/FacebookAppTypes;->b:Lcom/facebook/config/application/FbAppType;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_4
    sget-object v0, Lcom/facebook/katana/app/FacebookAppTypes;->c:Lcom/facebook/config/application/FbAppType;

    goto :goto_0
.end method

.method private h()Lcom/facebook/common/process/ProcessName;
    .locals 1

    invoke-static {p0}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan;->a(Landroid/content/Context;)Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/facebook/base/app/ApplicationLike;
    .locals 10

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->h()Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    const-string v1, "recovery"

    invoke-virtual {v0}, Lcom/facebook/common/process/ProcessName;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-virtual {v1, p0}, Lcom/facebook/nobreak/RecoveryModeHelper;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-static {}, Lcom/facebook/nobreak/RecoveryModeHelper;->a()V

    :cond_0
    :goto_0
    const-string v1, "nodex"

    invoke-virtual {v0}, Lcom/facebook/common/process/ProcessName;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/facebook/katana/app/NodexFacebookApplicationImpl;

    invoke-direct {v0}, Lcom/facebook/katana/app/NodexFacebookApplicationImpl;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/facebook/katana/app/RecoveryModeFacebookApplicationImpl;

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-direct {v0, p0, v1}, Lcom/facebook/katana/app/RecoveryModeFacebookApplicationImpl;-><init>(Landroid/app/Application;Lcom/facebook/nobreak/RecoveryModeHelper;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-virtual {v1, p0}, Lcom/facebook/nobreak/RecoveryModeHelper;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-static {}, Lcom/facebook/nobreak/RecoveryModeHelper;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    throw v0

    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/EnsureDexsLoaded"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->e()Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    :try_start_5
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/EnsureDexsLoaded"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_4

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->h()Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/process/ProcessName;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a(Landroid/content/Context;)V

    :cond_4
    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/facebook/dalvik/DalvikLinearAllocType;->FBANDROID_DEBUG:Lcom/facebook/dalvik/DalvikLinearAllocType;

    :goto_2
    invoke-static {v0}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a(Lcom/facebook/dalvik/DalvikLinearAllocType;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->h()Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/process/ProcessName;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_5
    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBApp.createDelegate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    new-instance v0, Lcom/facebook/katana/app/FacebookApplicationImpl;

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->b:Lcom/facebook/config/application/FbAppType;

    iget-object v3, p0, Lcom/facebook/katana/app/FacebookApplication;->c:Lcom/facebook/nobreak/CatchMeIfYouCan;

    iget-object v4, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    iget-boolean v6, p0, Lcom/facebook/katana/app/FacebookApplication;->i:Z

    iget-wide v7, p0, Lcom/facebook/katana/app/FacebookApplication;->d:J

    iget-object v9, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/facebook/katana/app/FacebookApplicationImpl;-><init>(Landroid/app/Application;Lcom/facebook/config/application/FbAppType;Lcom/facebook/nobreak/CatchMeIfYouCan;Lcom/facebook/nobreak/RecoveryModeHelper;Lcom/facebook/common/dextricks/DexErrorRecoveryInfo;ZJLcom/facebook/base/app/LightweightPerfEvents;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/EnsureDexsLoaded"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    throw v0

    :cond_6
    sget-object v0, Lcom/facebook/dalvik/DalvikLinearAllocType;->FBANDROID_RELEASE:Lcom/facebook/dalvik/DalvikLinearAllocType;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2
.end method

.method protected final b()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "LogUse"
        }
    .end annotation

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/katana/app/FacebookApplication;->d:J

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBAppImpl.onCreate"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBApp.onBaseContextAttached"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Ljava/lang/String;)V

    :try_start_0
    const-string v0, "true"

    new-instance v1, Lcom/facebook/common/manifest/ManifestReader;

    invoke-direct {v1, p0}, Lcom/facebook/common/manifest/ManifestReader;-><init>(Landroid/content/Context;)V

    const-string v2, "com.facebook.ndash"

    invoke-virtual {v1, v2}, Lcom/facebook/common/manifest/ManifestReader;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/katana/app/FacebookApplication;->i:Z

    iget-boolean v0, p0, Lcom/facebook/katana/app/FacebookApplication;->i:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    :cond_0
    new-instance v0, Lcom/facebook/nobreak/DummyCatchMeIfYouCan;

    invoke-direct {v0}, Lcom/facebook/nobreak/DummyCatchMeIfYouCan;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->c:Lcom/facebook/nobreak/CatchMeIfYouCan;

    :goto_0
    new-instance v0, Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-direct {v0}, Lcom/facebook/nobreak/RecoveryModeHelper;-><init>()V

    iput-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->f:Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-static {p0}, Lcom/facebook/nobreak/ExceptionHandlerToDispatchKnownExceptionRemedies;->a(Landroid/app/Application;)V

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "https://www.facebook.com/mobile/android_beta_crash_logs/"

    :goto_1
    new-instance v1, Lcom/facebook/common/errorreporting/FbCrashReporter;

    invoke-direct {v1, p0}, Lcom/facebook/common/errorreporting/FbCrashReporter;-><init>(Landroid/app/Application;)V

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/facebook/acra/ACRA;->init(Lcom/facebook/acra/reporter/ReportsCrashes;Ljava/lang/String;Z)Lcom/facebook/acra/ErrorReporter;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->g()Lcom/facebook/config/application/FbAppType;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->b:Lcom/facebook/config/application/FbAppType;

    const-string v1, "app"

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->b:Lcom/facebook/config/application/FbAppType;

    invoke-virtual {v2}, Lcom/facebook/config/application/FbAppType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "fb_app_id"

    iget-object v2, p0, Lcom/facebook/katana/app/FacebookApplication;->b:Lcom/facebook/config/application/FbAppType;

    invoke-virtual {v2}, Lcom/facebook/config/application/FbAppType;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "persisted_uid"

    new-instance v2, Lcom/facebook/common/errorreporting/persisteduid/UserIdFileReader;

    invoke-direct {v2, p0}, Lcom/facebook/common/errorreporting/persisteduid/UserIdFileReader;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/acra/ErrorReporter;->putLazyCustomData(Ljava/lang/String;Lcom/facebook/acra/CustomReportDataSupplier;)V

    new-instance v1, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;

    invoke-direct {v1, p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/acra/ErrorReporter;->isNativeCrashedOnPreviousRun()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->a()V

    invoke-virtual {v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->c()V

    invoke-virtual {v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->b()V

    :cond_1
    const-string v2, "crash_counter"

    invoke-virtual {v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f()Lcom/facebook/acra/CustomReportDataSupplier;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/acra/ErrorReporter;->putLazyCustomData(Ljava/lang/String;Lcom/facebook/acra/CustomReportDataSupplier;)V

    const-string v2, "crash_counter_24h"

    invoke-virtual {v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g()Lcom/facebook/acra/CustomReportDataSupplier;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putLazyCustomData(Ljava/lang/String;Lcom/facebook/acra/CustomReportDataSupplier;)V

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide/32 v1, 0x100000

    invoke-virtual {v0, v1, v2}, Lcom/facebook/acra/ErrorReporter;->setMaxReportSize(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    invoke-static {p0}, Lcom/facebook/soloader/SoLoader;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/facebook/soloader/SoLoader$SoLoaderOrderError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    invoke-static {p0}, Lcom/facebook/breakpad/BreakpadManager;->b(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v1, "ColdStart/FBApp.onBaseContextAttached"

    invoke-virtual {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    return-void

    :cond_3
    :try_start_3
    new-instance v1, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan;

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/facebook/katana/app/FacebookApplication;->h:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    :goto_3
    invoke-direct {v1, p0, v0}, Lcom/facebook/nobreak/DefaultCatchMeIfYouCan;-><init>(Landroid/app/Application;[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;)V

    iput-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->c:Lcom/facebook/nobreak/CatchMeIfYouCan;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/katana/app/FacebookApplication;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/FBApp.onBaseContextAttached"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    throw v0

    :cond_4
    :try_start_4
    sget-object v0, Lcom/facebook/katana/app/FacebookApplication;->g:[Lcom/facebook/nobreak/DefaultCatchMeIfYouCan$ProcessProfile;

    goto :goto_3

    :cond_5
    const-string v0, "https://www.facebook.com/mobile/android_crash_logs/"

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-class v1, Lcom/facebook/katana/app/FacebookApplication;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SoLoader initialization error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public final f()Lcom/facebook/inject/FbInjector;
    .locals 3

    invoke-virtual {p0}, Lcom/facebook/katana/app/FacebookApplication;->c()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    instance-of v1, v0, Lcom/facebook/katana/app/FacebookApplicationImpl;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/katana/app/FacebookApplicationImpl;

    invoke-virtual {v0}, Lcom/facebook/katana/app/FacebookApplicationImpl;->f()Lcom/facebook/inject/FbInjector;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Injector is not supported in process "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/facebook/katana/app/FacebookApplication;->h()Lcom/facebook/common/process/ProcessName;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/common/process/ProcessName;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
