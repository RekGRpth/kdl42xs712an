.class Lcom/facebook/systrace/TraceInternal;
.super Ljava/lang/Object;
.source "TraceInternal.java"


# static fields
.field public static final a:Z

.field public static final b:J

.field private static final c:Ljava/lang/reflect/Method;

.field private static final d:Ljava/lang/reflect/Method;

.field private static final e:Ljava/lang/reflect/Method;

.field private static final f:Ljava/lang/reflect/Method;

.field private static final g:Ljava/lang/reflect/Method;

.field private static volatile h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-lt v0, v4, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/facebook/systrace/TraceInternal;->a:Z

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->a:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->a()Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->a:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/facebook/systrace/TraceInternal;->c:Ljava/lang/reflect/Method;

    iget-object v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->b:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/facebook/systrace/TraceInternal;->d:Ljava/lang/reflect/Method;

    iget-object v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->c:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/facebook/systrace/TraceInternal;->e:Ljava/lang/reflect/Method;

    iget-object v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->e:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/facebook/systrace/TraceInternal;->f:Ljava/lang/reflect/Method;

    iget-object v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->d:Ljava/lang/reflect/Method;

    sput-object v2, Lcom/facebook/systrace/TraceInternal;->g:Ljava/lang/reflect/Method;

    iget-wide v2, v0, Lcom/facebook/systrace/TraceInternal$SystraceHiddenMembers;->f:J

    sput-wide v2, Lcom/facebook/systrace/TraceInternal;->b:J

    sput-boolean v1, Lcom/facebook/systrace/TraceInternal;->h:Z

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    sput-object v3, Lcom/facebook/systrace/TraceInternal;->c:Ljava/lang/reflect/Method;

    sput-object v3, Lcom/facebook/systrace/TraceInternal;->d:Ljava/lang/reflect/Method;

    sput-object v3, Lcom/facebook/systrace/TraceInternal;->e:Ljava/lang/reflect/Method;

    sput-object v3, Lcom/facebook/systrace/TraceInternal;->f:Ljava/lang/reflect/Method;

    sput-object v3, Lcom/facebook/systrace/TraceInternal;->g:Ljava/lang/reflect/Method;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/systrace/TraceInternal;->b:J

    sput-boolean v2, Lcom/facebook/systrace/TraceInternal;->h:Z

    goto :goto_2

    :cond_2
    move-object v0, v3

    goto :goto_1
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->h:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/systrace/TraceInternal;->g:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x3e8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sput-boolean v5, Lcom/facebook/systrace/TraceInternal;->h:Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Ljava/lang/reflect/InvocationTargetException;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/reflect/InvocationTargetException;)V
    .locals 2

    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_0
    instance-of v1, v0, Ljava/lang/Error;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Error;

    throw v0

    :cond_1
    return-void
.end method

.method public static a(Z)V
    .locals 6

    const/4 v5, 0x0

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->h:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/systrace/TraceInternal;->f:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sput-boolean v5, Lcom/facebook/systrace/TraceInternal;->h:Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Ljava/lang/reflect/InvocationTargetException;)V

    goto :goto_0
.end method

.method public static a(J)Z
    .locals 6

    const/4 v1, 0x0

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->h:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/systrace/TraceInternal;->e:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    sput-boolean v1, Lcom/facebook/systrace/TraceInternal;->h:Z

    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Ljava/lang/reflect/InvocationTargetException;)V

    move v0, v1

    goto :goto_0
.end method

.method public static b(JLjava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->h:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/systrace/TraceInternal;->c:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sput-boolean v5, Lcom/facebook/systrace/TraceInternal;->h:Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Ljava/lang/reflect/InvocationTargetException;)V

    goto :goto_0
.end method

.method public static c(JLjava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    sget-boolean v0, Lcom/facebook/systrace/TraceInternal;->h:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v0, Lcom/facebook/systrace/TraceInternal;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    sput-boolean v5, Lcom/facebook/systrace/TraceInternal;->h:Z

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Ljava/lang/reflect/InvocationTargetException;)V

    goto :goto_0
.end method
