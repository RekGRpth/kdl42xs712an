.class final Lcom/facebook/common/gcinitopt/GcOptimizer$2;
.super Ljava/lang/Object;
.source "GcOptimizer.java"

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->b()Lcom/facebook/common/references/OOMSoftReference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->b()Lcom/facebook/common/references/OOMSoftReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/references/OOMSoftReference;->a()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "GC optimization still active after timeout - clearing by force."

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c()V

    :cond_0
    return-void
.end method
