.class public Lcom/facebook/R$style;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ActivityFadeInOut:I = 0x7f0d0394

.field public static final ActivityFadeInOutFast:I = 0x7f0d0395

.field public static final ActivitySlideRightLeft:I = 0x7f0d0396

.field public static final AddMembersCreateThread:I = 0x7f0d0252

.field public static final AddMembersCreateThread_Neue:I = 0x7f0d0253

.field public static final AddMembersDivider:I = 0x7f0d0254

.field public static final AddMembersDivider_Neue:I = 0x7f0d0255

.field public static final AddMembersFooter:I = 0x7f0d0256

.field public static final AddMembersFooter_Neue:I = 0x7f0d0257

.field public static final AdminMessageAttachmentView:I = 0x7f0d0239

.field public static final AdminMessageAttachmentView_Neue:I = 0x7f0d023a

.field public static final AdminMessageItem:I = 0x7f0d0235

.field public static final AdminMessageItem_Neue:I = 0x7f0d0236

.field public static final AdminMessageTextView:I = 0x7f0d0237

.field public static final AdminMessageTextView_Neue:I = 0x7f0d0238

.field public static final BackgroundLocationNuxIntroBubbleText:I = 0x7f0d03ae

.field public static final BackgroundLocationSwitchTextAppearance:I = 0x7f0d03af

.field public static final BadgeTextViewBadgeAppearance:I = 0x7f0d02de

.field public static final BottomBannerText:I = 0x7f0d017a

.field public static final BottomBannerTitle:I = 0x7f0d017b

.field public static final BrowserMenuDim:I = 0x7f0d03e3

.field public static final ButtonBackgroundStyle:I = 0x7f0d0318

.field public static final ButtonBackgroundStyle_Large:I = 0x7f0d031a

.field public static final ButtonBackgroundStyle_Small:I = 0x7f0d0319

.field public static final ButtonTextShadow:I = 0x7f0d013e

.field public static final CentralEntityActionHighlighted:I = 0x7f0d0390

.field public static final CentralEntityActionNormal:I = 0x7f0d0391

.field public static final ChambrayCardUnit:I = 0x7f0d0348

.field public static final ChambrayText:I = 0x7f0d0338

.field public static final ChambrayText_Bold:I = 0x7f0d033c

.field public static final ChambrayText_Dark:I = 0x7f0d033b

.field public static final ChambrayText_Large:I = 0x7f0d0342

.field public static final ChambrayText_Large_Bold:I = 0x7f0d0347

.field public static final ChambrayText_Large_Dark:I = 0x7f0d0344

.field public static final ChambrayText_Large_Dark_Bold:I = 0x7f0d0345

.field public static final ChambrayText_Large_Light:I = 0x7f0d0343

.field public static final ChambrayText_Large_Light_Bold:I = 0x7f0d0346

.field public static final ChambrayText_Light:I = 0x7f0d0339

.field public static final ChambrayText_Light_Bold:I = 0x7f0d033a

.field public static final ChambrayText_Medium:I = 0x7f0d0341

.field public static final ChambrayText_Small:I = 0x7f0d033d

.field public static final ChambrayText_Small_Bold:I = 0x7f0d0340

.field public static final ChambrayText_Small_Dark:I = 0x7f0d033f

.field public static final ChambrayText_Small_Light:I = 0x7f0d033e

.field public static final ChatAvailabilitySwitchTextAppearance:I = 0x7f0d023d

.field public static final ChatHeadBadge:I = 0x7f0d013f

.field public static final ChatHeadTextBubble_Mask_LeftOrigin:I = 0x7f0d01a9

.field public static final ChatHeadTextBubble_Mask_RightOrigin:I = 0x7f0d01a8

.field public static final ChatHeadTextBubble_MultilineEllipsizeTextView:I = 0x7f0d020e

.field public static final ChatHeadTextBubble_MultilineEllipsizeTextView_LeftOrigin:I = 0x7f0d0210

.field public static final ChatHeadTextBubble_MultilineEllipsizeTextView_RightOrigin:I = 0x7f0d020f

.field public static final ChromeOverflowButton:I = 0x7f0d03e2

.field public static final CodeGeneratorProgressCircle:I = 0x7f0d0174

.field public static final ComposeDrawerPlaceholderButton:I = 0x7f0d0225

.field public static final ConnectionLostBanner:I = 0x7f0d0212

.field public static final ContactCardSectionRoot:I = 0x7f0d01a2

.field public static final ContactMultipickerContainerStyle:I = 0x7f0d01dd

.field public static final ContactMultipickerContainerStyle_Neue:I = 0x7f0d01de

.field public static final ContactMultipickerCover:I = 0x7f0d0248

.field public static final ContactMultipickerCover_GroupCreation:I = 0x7f0d0249

.field public static final ContactMultipickerListContainerStyle_GroupCreation:I = 0x7f0d0244

.field public static final ContactPickerAutocompleteContainerStyle:I = 0x7f0d0240

.field public static final ContactPickerAutocompleteContainerStyle_Neue:I = 0x7f0d0241

.field public static final ContactPickerCheckboxStyle:I = 0x7f0d01c0

.field public static final ContactPickerCheckboxStyle_Neue:I = 0x7f0d01c1

.field public static final ContactPickerFriendsListMask:I = 0x7f0d01db

.field public static final ContactPickerFriendsListMask_DiveHead:I = 0x7f0d01dc

.field public static final ContactPickerHeadingText:I = 0x7f0d0242

.field public static final ContactPickerHeadingText_GroupCreation:I = 0x7f0d0243

.field public static final ContactPickerItemDividerStyle:I = 0x7f0d01cc

.field public static final ContactPickerItemDividerStyle_DiveHead:I = 0x7f0d01ce

.field public static final ContactPickerItemDividerStyle_Divebar:I = 0x7f0d01cd

.field public static final ContactPickerItemNameStyle:I = 0x7f0d01b0

.field public static final ContactPickerItemNameStyle_DiveHead:I = 0x7f0d01b2

.field public static final ContactPickerItemNameStyle_Divebar:I = 0x7f0d01b1

.field public static final ContactPickerItemPresenceIndicatorStyle:I = 0x7f0d01b7

.field public static final ContactPickerItemPresenceIndicatorStyle_DiveHead:I = 0x7f0d01b8

.field public static final ContactPickerItemStatusStyle:I = 0x7f0d01b3

.field public static final ContactPickerItemStatusStyle_DiveHead:I = 0x7f0d01b5

.field public static final ContactPickerItemStatusStyle_Divebar:I = 0x7f0d01b4

.field public static final ContactPickerItemStatusStyle_Neue:I = 0x7f0d01b6

.field public static final ContactPickerItemStyle:I = 0x7f0d01bb

.field public static final ContactPickerItemStyle_DiveHead:I = 0x7f0d01be

.field public static final ContactPickerItemStyle_DiveHead_Reflex:I = 0x7f0d01bf

.field public static final ContactPickerItemStyle_Divebar:I = 0x7f0d01bc

.field public static final ContactPickerItemStyle_ThreadSettings:I = 0x7f0d01bd

.field public static final ContactPickerItemTextContainer:I = 0x7f0d01ae

.field public static final ContactPickerItemTextContainer_DiveHead:I = 0x7f0d01af

.field public static final ContactPickerList:I = 0x7f0d01aa

.field public static final ContactPickerListTopShadowStyle:I = 0x7f0d01cf

.field public static final ContactPickerListTopShadowStyle_DiveHead:I = 0x7f0d01d0

.field public static final ContactPickerList_DiveHead:I = 0x7f0d01ab

.field public static final ContactPickerList_DiveHead_Neue:I = 0x7f0d01ac

.field public static final ContactPickerList_EditFavorites:I = 0x7f0d01ad

.field public static final ContactPickerMembersDividerStyle:I = 0x7f0d0245

.field public static final ContactPickerMembersDividerStyle_GroupCreation:I = 0x7f0d0247

.field public static final ContactPickerMembersDividerStyle_Neue:I = 0x7f0d0246

.field public static final ContactPickerSearchDivider:I = 0x7f0d01c9

.field public static final ContactPickerSearchDivider_DiveHead:I = 0x7f0d01cb

.field public static final ContactPickerSearchDivider_Divebar:I = 0x7f0d01ca

.field public static final ContactPickerSecondaryCheckboxStyle:I = 0x7f0d01c2

.field public static final ContactPickerSecondaryCheckboxStyle_CreateGroups:I = 0x7f0d01c4

.field public static final ContactPickerSecondaryCheckboxStyle_Neue:I = 0x7f0d01c3

.field public static final ContactPickerSectionHeaderDividerStyle_DiveHead:I = 0x7f0d01da

.field public static final ContactPickerSectionHeaderStyle:I = 0x7f0d01d1

.field public static final ContactPickerSectionHeaderStyle_DiveHead:I = 0x7f0d01d3

.field public static final ContactPickerSectionHeaderStyle_Divebar:I = 0x7f0d01d2

.field public static final ContactPickerSectionHeaderTextStyle:I = 0x7f0d01d7

.field public static final ContactPickerSectionHeaderTextStyle_DiveHead:I = 0x7f0d01d9

.field public static final ContactPickerSectionHeaderTextStyle_Divebar:I = 0x7f0d01d8

.field public static final ContactPickerSectionSplitterStyle:I = 0x7f0d01d4

.field public static final ContactPickerSectionSplitterStyle_Divebar:I = 0x7f0d01d5

.field public static final ContactPickerSectionSplitterStyle_Neue:I = 0x7f0d01d6

.field public static final ContactPickerThreadTileView:I = 0x7f0d01c5

.field public static final ContactPickerThreadTileView_DiveHead:I = 0x7f0d01c6

.field public static final ContactPickerThreadTileView_Neue:I = 0x7f0d01c8

.field public static final ContactPickerThreadTileView_ViewPeople:I = 0x7f0d01c7

.field public static final ContactPickerTokenizedAutoCompleteTextViewStyle:I = 0x7f0d024a

.field public static final ContactPickerTokenizedAutoCompleteTextViewStyle_Neue:I = 0x7f0d024b

.field public static final ContactPickerTokenizedAutoCompleteTextViewStyle_Neue_GroupCreation:I = 0x7f0d024c

.field public static final ContactPickerViewMoreStyle:I = 0x7f0d01f9

.field public static final ContactPickerViewMoreStyle_DiveHead:I = 0x7f0d01fb

.field public static final ContactPickerViewMoreStyle_Divebar:I = 0x7f0d01fa

.field public static final ContactPickerWarning:I = 0x7f0d024d

.field public static final ContactPickerWarning_Neue:I = 0x7f0d024e

.field public static final CreatePinnedGroupDialogAnimation:I = 0x7f0d0299

.field public static final CreateThreadCustomLayoutStyle:I = 0x7f0d023e

.field public static final CreateThreadCustomLayoutStyle_Neue:I = 0x7f0d023f

.field public static final CropDoneButton:I = 0x7f0d02e9

.field public static final CustomDialogTheme:I = 0x7f0d0143

.field public static final CustomProgressBar:I = 0x7f0d0140

.field public static final DialogWindowTitleContainer_FacebookHolo_Light:I = 0x7f0d00c0

.field public static final DialogWindowTitle_FacebookHolo_Base:I = 0x7f0d00b9

.field public static final DialogWindowTitle_FacebookHolo_Light:I = 0x7f0d00c1

.field public static final DimmingAnimation:I = 0x7f0d03e4

.field public static final DivebarContainerStyle:I = 0x7f0d01f2

.field public static final DivebarContainerStyle_DiveHead:I = 0x7f0d01f3

.field public static final DivebarSearchContainerStyle:I = 0x7f0d01f4

.field public static final DivebarSearchContainerStyle_DiveHead:I = 0x7f0d01f5

.field public static final DivebarSearchEditTextStyle:I = 0x7f0d01f6

.field public static final DivebarSearchEditTextStyle_DiveHead:I = 0x7f0d01f8

.field public static final DivebarSearchEditTextStyle_Divebar:I = 0x7f0d01f7

.field public static final EditFavoriteDividerStyle:I = 0x7f0d01e3

.field public static final EditFavoriteDividerStyle_Neue:I = 0x7f0d01e4

.field public static final EditFavoriteListStyle:I = 0x7f0d01e5

.field public static final EditFavoriteListStyle_Neue:I = 0x7f0d01e6

.field public static final EditFavoriteSectionSplitterStyle:I = 0x7f0d01e7

.field public static final EditFavoriteSectionSplitterStyle_Neue:I = 0x7f0d01e8

.field public static final EditFavoritesContactPickerSearchBarStyle:I = 0x7f0d01e1

.field public static final EditFavoritesContactPickerSearchBarStyle_Neue:I = 0x7f0d01e2

.field public static final EditFavoritesItemDragStyle:I = 0x7f0d01df

.field public static final EditFavoritesItemDragStyle_Neue:I = 0x7f0d01e0

.field public static final EditFavoritesUserTileViewStyle:I = 0x7f0d01e9

.field public static final EditFavoritesUserTileViewStyle_Neue:I = 0x7f0d01ea

.field public static final EmojiAttachmentsDivider:I = 0x7f0d021d

.field public static final EmojiAttachmentsDividerNeue:I = 0x7f0d021e

.field public static final EmojiAttachmentsPageIndicator:I = 0x7f0d021f

.field public static final EmojiAttachmentsPageIndicator_Neue:I = 0x7f0d0220

.field public static final FacebookHolo_ButtonBar_Base:I = 0x7f0d00ba

.field public static final FacebookHolo_Light_ButtonBar_AlertDialog:I = 0x7f0d00c3

.field public static final FbProgressCircle:I = 0x7f0d0171

.field public static final FbProgressCircular:I = 0x7f0d0006

.field public static final FbProgressCircular_Large:I = 0x7f0d0007

.field public static final FbProgressCircular_Neue:I = 0x7f0d0266

.field public static final FbProgressCircular_Neue_Inverse:I = 0x7f0d0267

.field public static final FbProgressCircular_Small:I = 0x7f0d0008

.field public static final FbProgressCircular_Small_Neue:I = 0x7f0d016c

.field public static final FbProgressCircular_Small_Neue_Inverse:I = 0x7f0d016d

.field public static final FbProgressHorizontal:I = 0x7f0d0004

.field public static final FbProgressHorizontal_Inverse:I = 0x7f0d0005

.field public static final FbVideoSeekBarHorizontal:I = 0x7f0d016e

.field public static final FindFriendsAddStepProgressCircle:I = 0x7f0d0175

.field public static final GDPButtonContainer:I = 0x7f0d03db

.field public static final GDPDialogButton:I = 0x7f0d03dd

.field public static final GDPDialogHorizontalDivider:I = 0x7f0d03da

.field public static final GDPDialogTheme:I = 0x7f0d03d5

.field public static final GDPDialogTitle:I = 0x7f0d03d6

.field public static final GDPDialogVerticalDivider:I = 0x7f0d03dc

.field public static final GDPMessageText:I = 0x7f0d03d8

.field public static final GDPNuxText:I = 0x7f0d03d9

.field public static final GDPTitleDivider:I = 0x7f0d03d7

.field public static final GraphSearchFilterRowTextView:I = 0x7f0d038a

.field public static final GraphSearchResultTitleText:I = 0x7f0d038e

.field public static final GraphSearchSavedContextText:I = 0x7f0d038c

.field public static final GraphSearchSectionHeader:I = 0x7f0d038b

.field public static final GraphSearchTitleLinearLayout:I = 0x7f0d0389

.field public static final GraphSearchTitleSearchGlass:I = 0x7f0d0388

.field public static final GraphSearchTitleTextView:I = 0x7f0d0387

.field public static final GroupsSettingsPreviewPicture:I = 0x7f0d0260

.field public static final GroupsSettingsRow:I = 0x7f0d025b

.field public static final GroupsSettingsRowSummary:I = 0x7f0d025d

.field public static final GroupsSettingsRowTitle:I = 0x7f0d025c

.field public static final GroupsSettingsSeparator:I = 0x7f0d025f

.field public static final GroupsSettingsSwitch:I = 0x7f0d025e

.field public static final HomeActivityTheme:I = 0x7f0d013c

.field public static final HomeActivityThemeV2:I = 0x7f0d013d

.field public static final InlineActionBar:I = 0x7f0d0311

.field public static final InlineActionBarOverflow:I = 0x7f0d0314

.field public static final InlineActionBarThemeMinReqs:I = 0x7f0d0313

.field public static final InlineActionBar_Large:I = 0x7f0d0317

.field public static final InlineActionBar_Small:I = 0x7f0d0312

.field public static final InlineActionBar_TextAppearance_Large:I = 0x7f0d0315

.field public static final InlineActionBar_TextAppearance_Small:I = 0x7f0d0316

.field public static final IorgDialog:I = 0x7f0d03d3

.field public static final IorgInstallButton:I = 0x7f0d03cf

.field public static final IorgListHeaderDivider:I = 0x7f0d03d0

.field public static final IorgListHeaderText:I = 0x7f0d03d1

.field public static final IorgProgressBar:I = 0x7f0d03ce

.field public static final KeywordSearchTitle:I = 0x7f0d038f

.field public static final LocalCardUnit:I = 0x7f0d0359

.field public static final LocationNuxView:I = 0x7f0d0291

.field public static final LocationNuxView_Neue:I = 0x7f0d0292

.field public static final LoginProgressCircle:I = 0x7f0d0173

.field public static final MapImageShowButtonText:I = 0x7f0d0167

.field public static final MePreferencesFooterItemStyle:I = 0x7f0d024f

.field public static final MenuItemIconAndText:I = 0x7f0d03e0

.field public static final MenuItemTwoButtons:I = 0x7f0d03e1

.field public static final MessageComposerDivider:I = 0x7f0d0226

.field public static final MessageComposerDividerNeue:I = 0x7f0d0227

.field public static final MessageDivider:I = 0x7f0d022f

.field public static final MessageDividerBar:I = 0x7f0d0231

.field public static final MessageDividerBar_Neue:I = 0x7f0d0232

.field public static final MessageDividerNeue:I = 0x7f0d0230

.field public static final MessageDividerView:I = 0x7f0d0233

.field public static final MessageDividerView_Neue:I = 0x7f0d0234

.field public static final MessageItemView:I = 0x7f0d0228

.field public static final MessageItemViewTypingItem:I = 0x7f0d022d

.field public static final MessageItemViewTypingItem_Neue:I = 0x7f0d022e

.field public static final MessageItemViewUserTile:I = 0x7f0d022b

.field public static final MessageItemViewUserTile_Neue:I = 0x7f0d022c

.field public static final MessageItemView_MeUserNeue:I = 0x7f0d0229

.field public static final MessageItemView_OtherUserNeue:I = 0x7f0d022a

.field public static final MessengerLocationSearchResultRowDetails:I = 0x7f0d03cc

.field public static final MessengerLocationSearchResultRowTitle:I = 0x7f0d03cb

.field public static final NeedleSearchEntryRowButton:I = 0x7f0d038d

.field public static final NotificationContainer:I = 0x7f0d02db

.field public static final NotificationText:I = 0x7f0d0009

.field public static final NotificationTitle:I = 0x7f0d000a

.field public static final NuxDiveBarButtonLayout:I = 0x7f0d026a

.field public static final OrcaBlueNuxBubbleTextAppearance:I = 0x7f0d0268

.field public static final OrcaChatHeadsPopupMenu:I = 0x7f0d01a7

.field public static final OrcaNeueActionBar:I = 0x7f0d0213

.field public static final OrcaNeueActionBarSubtitleTextStyle:I = 0x7f0d0217

.field public static final OrcaNeueActionBarTab:I = 0x7f0d0218

.field public static final OrcaNeueActionBarTabBar:I = 0x7f0d0219

.field public static final OrcaNeueActionBarTitleTextStyle:I = 0x7f0d0216

.field public static final OrcaNeueActionBar_Home:I = 0x7f0d0214

.field public static final OrcaNeueActionBar_Home_ChatHeads:I = 0x7f0d0215

.field public static final OrcaNeueActionOverflow:I = 0x7f0d021b

.field public static final OrcaNeueProgressHorizontal:I = 0x7f0d021c

.field public static final OrcaNeueSplitBarActionButton:I = 0x7f0d021a

.field public static final OrcaVoiceClipsTooltipNuxText:I = 0x7f0d01a6

.field public static final OrcaVoiceClipsTooltipText:I = 0x7f0d01a4

.field public static final OrcaVoiceClipsTooltipTextHoverOff:I = 0x7f0d01a5

.field public static final PageActivityUniRunningInsightsDescription:I = 0x7f0d035e

.field public static final PageActivityUniRunningInsightsLabel:I = 0x7f0d035d

.field public static final PageActivityUniRunningRowName:I = 0x7f0d035f

.field public static final PageActivityUniRunningRowValue:I = 0x7f0d0360

.field public static final PageIdentityActionSheetButton:I = 0x7f0d034c

.field public static final PageIdentityAdminBadge:I = 0x7f0d035b

.field public static final PageIdentityAdminBadge_Secondary:I = 0x7f0d035c

.field public static final PageIdentityAlbumsFragmentText:I = 0x7f0d0361

.field public static final PageIdentityCard:I = 0x7f0d0351

.field public static final PageIdentityCardStyles:I = 0x7f0d0350

.field public static final PageIdentityExpandableText:I = 0x7f0d0362

.field public static final PageIdentityFeedStoryCard:I = 0x7f0d0353

.field public static final PageIdentityHeaderText:I = 0x7f0d034b

.field public static final PageIdentityHeaderTextRefresh:I = 0x7f0d034a

.field public static final PageIdentityStatNumberText:I = 0x7f0d034d

.field public static final PageIdentityTimeline:I = 0x7f0d0354

.field public static final PageIdentityTopCard:I = 0x7f0d0352

.field public static final PageIdentityTvAiringsLargeDarkText:I = 0x7f0d0358

.field public static final PageIdentityTvAiringsLargeLightText:I = 0x7f0d0357

.field public static final PageIdentityTvAiringsLargeText:I = 0x7f0d0356

.field public static final PageIdentityTvAiringsLayout:I = 0x7f0d0355

.field public static final PageInformationActionSheetButton:I = 0x7f0d034f

.field public static final PageReviewSurveyTitleText:I = 0x7f0d0365

.field public static final PageUIButtonCardText:I = 0x7f0d0349

.field public static final PagesBrowserSection:I = 0x7f0d03de

.field public static final PhotoOverlayButton:I = 0x7f0d039c

.field public static final PhotoOverlayButtonRight:I = 0x7f0d039d

.field public static final PhotoTextShadow:I = 0x7f0d02ac

.field public static final PickMediaLayout:I = 0x7f0d0258

.field public static final PickMediaLayout_Neue:I = 0x7f0d0259

.field public static final PinnedGroupsContinueButtonText:I = 0x7f0d0261

.field public static final PlaceName:I = 0x7f0d02ee

.field public static final PlutoniumActionBarOverflow:I = 0x7f0d0320

.field public static final PlutoniumActionBarTheme:I = 0x7f0d031f

.field public static final PopoverMenuWindow:I = 0x7f0d02e5

.field public static final PopoverMenuWindow_BadgeText:I = 0x7f0d02e7

.field public static final PopoverMenuWindow_Description:I = 0x7f0d02e8

.field public static final PopoverMenuWindow_Title:I = 0x7f0d02e6

.field public static final PopoverStyle:I = 0x7f0d02eb

.field public static final PopoverWindowAnimation:I = 0x7f0d02e2

.field public static final PopoverWindowAnimation_Above:I = 0x7f0d02e4

.field public static final PopoverWindowAnimation_Below:I = 0x7f0d02e3

.field public static final PostPostBackground:I = 0x7f0d02ad

.field public static final PresenceIndicatorIconStyle:I = 0x7f0d01eb

.field public static final PresenceIndicatorIconStyle_ChatHeads:I = 0x7f0d01ec

.field public static final PresenceIndicatorIconStyle_Neue:I = 0x7f0d01ee

.field public static final PresenceIndicatorIconStyle_OnlineToggle:I = 0x7f0d01ed

.field public static final PresenceIndicatorTextStyle:I = 0x7f0d01ef

.field public static final PresenceIndicatorTextStyle_ChatHeads:I = 0x7f0d01f0

.field public static final PresenceIndicatorTextStyle_Neue:I = 0x7f0d01f1

.field public static final PublishModeOptionsCheckedContentView:I = 0x7f0d02d6

.field public static final PublishModeOptionsCheckedContentViewText:I = 0x7f0d02d8

.field public static final PublishModeOptionsTextView:I = 0x7f0d02d7

.field public static final QP_Dialog_Button_Primary:I = 0x7f0d02f2

.field public static final QP_Dialog_Button_Secondary:I = 0x7f0d02f3

.field public static final QP_Interstitial_Button_Primary:I = 0x7f0d02f0

.field public static final QP_Interstitial_Button_Secondary:I = 0x7f0d02f1

.field public static final RatingDescription:I = 0x7f0d036e

.field public static final RefreshableViewTextStyle:I = 0x7f0d0176

.field public static final RidgeWidgetPromptText:I = 0x7f0d02d9

.field public static final RoundedList:I = 0x7f0d02cb

.field public static final RowReceiptTextView:I = 0x7f0d023b

.field public static final RowReceiptTextView_Neue:I = 0x7f0d023c

.field public static final SetupFlowActivity:I = 0x7f0d030e

.field public static final StickerPopupPageIndicator:I = 0x7f0d0221

.field public static final StickerPopupPageIndicator_Neue:I = 0x7f0d0222

.field public static final StickerPreviewPopup:I = 0x7f0d025a

.field public static final StickerStoreButton:I = 0x7f0d0223

.field public static final StickerStoreButton_Neue:I = 0x7f0d0224

.field public static final StickerStoreItemDividerStyle:I = 0x7f0d0262

.field public static final StickerStoreItemDividerStyle_Neue:I = 0x7f0d0263

.field public static final StickerStoreItemDragStyle:I = 0x7f0d0264

.field public static final StickerStoreItemDragStyle_Neue:I = 0x7f0d0265

.field public static final StickerStoreProgressBar:I = 0x7f0d0211

.field public static final StoryPausePromotionButton:I = 0x7f0d02c8

.field public static final StoryPromotionAgeText:I = 0x7f0d02ca

.field public static final StoryPromotionButton:I = 0x7f0d02c7

.field public static final StoryPromotionHeader:I = 0x7f0d02c9

.field public static final StructuredDataCardUnit:I = 0x7f0d035a

.field public static final System_Notifications:I = 0x7f0d03a6

.field public static final System_Notifications_Title:I = 0x7f0d03a5

.field public static final TextAppearanceNeuePopupMenuLarge:I = 0x7f0d0250

.field public static final TextAppearanceNeuePopupMenuSmall:I = 0x7f0d0251

.field public static final TextAppearance_AppCompat_Base_CompactMenu_Dialog:I = 0x7f0d0098

.field public static final TextAppearance_AppCompat_Base_SearchResult:I = 0x7f0d00a0

.field public static final TextAppearance_AppCompat_Base_SearchResult_Subtitle:I = 0x7f0d00a2

.field public static final TextAppearance_AppCompat_Base_SearchResult_Title:I = 0x7f0d00a1

.field public static final TextAppearance_AppCompat_Base_Widget_PopupMenu_Large:I = 0x7f0d009c

.field public static final TextAppearance_AppCompat_Base_Widget_PopupMenu_Small:I = 0x7f0d009d

.field public static final TextAppearance_AppCompat_Light_Base_SearchResult:I = 0x7f0d00a3

.field public static final TextAppearance_AppCompat_Light_Base_SearchResult_Subtitle:I = 0x7f0d00a5

.field public static final TextAppearance_AppCompat_Light_Base_SearchResult_Title:I = 0x7f0d00a4

.field public static final TextAppearance_AppCompat_Light_Base_Widget_PopupMenu_Large:I = 0x7f0d009e

.field public static final TextAppearance_AppCompat_Light_Base_Widget_PopupMenu_Small:I = 0x7f0d009f

.field public static final TextAppearance_AppCompat_Light_SearchResult_Subtitle:I = 0x7f0d0069

.field public static final TextAppearance_AppCompat_Light_SearchResult_Title:I = 0x7f0d0068

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Large:I = 0x7f0d0064

.field public static final TextAppearance_AppCompat_Light_Widget_PopupMenu_Small:I = 0x7f0d0065

.field public static final TextAppearance_AppCompat_SearchResult_Subtitle:I = 0x7f0d0067

.field public static final TextAppearance_AppCompat_SearchResult_Title:I = 0x7f0d0066

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Menu:I = 0x7f0d004f

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle:I = 0x7f0d003b

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0d003d

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title:I = 0x7f0d003a

.field public static final TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse:I = 0x7f0d003c

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle:I = 0x7f0d0053

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse:I = 0x7f0d0055

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title:I = 0x7f0d0052

.field public static final TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse:I = 0x7f0d0054

.field public static final TextAppearance_AppCompat_Widget_Base_ActionBar_Menu:I = 0x7f0d0088

.field public static final TextAppearance_AppCompat_Widget_Base_ActionBar_Subtitle:I = 0x7f0d008a

.field public static final TextAppearance_AppCompat_Widget_Base_ActionBar_Subtitle_Inverse:I = 0x7f0d008c

.field public static final TextAppearance_AppCompat_Widget_Base_ActionBar_Title:I = 0x7f0d0089

.field public static final TextAppearance_AppCompat_Widget_Base_ActionBar_Title_Inverse:I = 0x7f0d008b

.field public static final TextAppearance_AppCompat_Widget_Base_ActionMode_Subtitle:I = 0x7f0d0085

.field public static final TextAppearance_AppCompat_Widget_Base_ActionMode_Subtitle_Inverse:I = 0x7f0d0087

.field public static final TextAppearance_AppCompat_Widget_Base_ActionMode_Title:I = 0x7f0d0084

.field public static final TextAppearance_AppCompat_Widget_Base_ActionMode_Title_Inverse:I = 0x7f0d0086

.field public static final TextAppearance_AppCompat_Widget_Base_DropDownItem:I = 0x7f0d0096

.field public static final TextAppearance_AppCompat_Widget_DropDownItem:I = 0x7f0d0056

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Large:I = 0x7f0d0062

.field public static final TextAppearance_AppCompat_Widget_PopupMenu_Small:I = 0x7f0d0063

.field public static final TextAppearance_Appirater_DialogMessage:I = 0x7f0d0369

.field public static final TextAppearance_Appirater_DialogTitle:I = 0x7f0d036a

.field public static final TextAppearance_EventBirthday_Content:I = 0x7f0d03c0

.field public static final TextAppearance_EventBirthday_Content_AccentBlue:I = 0x7f0d03c1

.field public static final TextAppearance_FBUi:I = 0x7f0d00c8

.field public static final TextAppearance_FBUi_AllCaps:I = 0x7f0d00c9

.field public static final TextAppearance_FBUi_Button:I = 0x7f0d00d6

.field public static final TextAppearance_FBUi_Button_Dark:I = 0x7f0d00e4

.field public static final TextAppearance_FBUi_Button_Dark_Large:I = 0x7f0d00e7

.field public static final TextAppearance_FBUi_Button_Dark_Medium:I = 0x7f0d00e6

.field public static final TextAppearance_FBUi_Button_Dark_Small:I = 0x7f0d00e5

.field public static final TextAppearance_FBUi_Button_Light:I = 0x7f0d00d7

.field public static final TextAppearance_FBUi_Button_Light_Primary:I = 0x7f0d00dc

.field public static final TextAppearance_FBUi_Button_Light_Primary_Large:I = 0x7f0d00df

.field public static final TextAppearance_FBUi_Button_Light_Primary_Medium:I = 0x7f0d00de

.field public static final TextAppearance_FBUi_Button_Light_Primary_Small:I = 0x7f0d00dd

.field public static final TextAppearance_FBUi_Button_Light_Regular:I = 0x7f0d00d8

.field public static final TextAppearance_FBUi_Button_Light_Regular_Large:I = 0x7f0d00db

.field public static final TextAppearance_FBUi_Button_Light_Regular_Medium:I = 0x7f0d00da

.field public static final TextAppearance_FBUi_Button_Light_Regular_Small:I = 0x7f0d00d9

.field public static final TextAppearance_FBUi_Button_Light_Special:I = 0x7f0d00e0

.field public static final TextAppearance_FBUi_Button_Light_Special_Large:I = 0x7f0d00e3

.field public static final TextAppearance_FBUi_Button_Light_Special_Medium:I = 0x7f0d00e2

.field public static final TextAppearance_FBUi_Button_Light_Special_Small:I = 0x7f0d00e1

.field public static final TextAppearance_FBUi_CV_Content:I = 0x7f0d02b1

.field public static final TextAppearance_FBUi_CV_Meta:I = 0x7f0d02b2

.field public static final TextAppearance_FBUi_CV_Title:I = 0x7f0d02b0

.field public static final TextAppearance_FBUi_Cell_Meta:I = 0x7f0d00d3

.field public static final TextAppearance_FBUi_Cell_Subtitle:I = 0x7f0d00d2

.field public static final TextAppearance_FBUi_Cell_Title:I = 0x7f0d00d1

.field public static final TextAppearance_FBUi_Content:I = 0x7f0d00cf

.field public static final TextAppearance_FBUi_DialogMessage:I = 0x7f0d02b3

.field public static final TextAppearance_FBUi_DialogTitle:I = 0x7f0d02b4

.field public static final TextAppearance_FBUi_Header_Title:I = 0x7f0d03c9

.field public static final TextAppearance_FBUi_Large:I = 0x7f0d00cc

.field public static final TextAppearance_FBUi_List:I = 0x7f0d00d4

.field public static final TextAppearance_FBUi_List_Header:I = 0x7f0d00d5

.field public static final TextAppearance_FBUi_Medium:I = 0x7f0d00cb

.field public static final TextAppearance_FBUi_Medium_InlineActionBar:I = 0x7f0d0310

.field public static final TextAppearance_FBUi_Megaphone:I = 0x7f0d02c2

.field public static final TextAppearance_FBUi_Megaphone_Button:I = 0x7f0d02c5

.field public static final TextAppearance_FBUi_Megaphone_Button_Primary:I = 0x7f0d02c6

.field public static final TextAppearance_FBUi_Megaphone_Subtitle:I = 0x7f0d02c4

.field public static final TextAppearance_FBUi_Megaphone_Title:I = 0x7f0d02c3

.field public static final TextAppearance_FBUi_Meta:I = 0x7f0d00d0

.field public static final TextAppearance_FBUi_Small:I = 0x7f0d00ca

.field public static final TextAppearance_FBUi_Title:I = 0x7f0d00ce

.field public static final TextAppearance_FBUi_XLarge:I = 0x7f0d00cd

.field public static final TextAppearance_FacebookHolo_Base:I = 0x7f0d00b7

.field public static final TextAppearance_FacebookHolo_Light:I = 0x7f0d00bb

.field public static final TextAppearance_FacebookHolo_Light_DialogMessage:I = 0x7f0d00bd

.field public static final TextAppearance_FacebookHolo_Light_DialogWindowTitle:I = 0x7f0d00c2

.field public static final TextAppearance_FacebookHolo_Light_EditText:I = 0x7f0d00bc

.field public static final TextAppearance_FacebookHolo_Light_Inverse:I = 0x7f0d00be

.field public static final TextAppearance_FacebookHolo_Light_Medium_Inverse:I = 0x7f0d00bf

.field public static final TextAppearance_QPInterstitialButtonPrimary:I = 0x7f0d02f6

.field public static final TextAppearance_QPInterstitialButtonSecondary:I = 0x7f0d02f5

.field public static final TextAppearance_QPInterstitialHeader:I = 0x7f0d02f4

.field public static final TextAppearance_Widget_AppCompat_Base_ExpandedMenu_Item:I = 0x7f0d0097

.field public static final TextAppearance_Widget_AppCompat_ExpandedMenu_Item:I = 0x7f0d005e

.field public static final Theme:I = 0x7f0d0136

.field public static final Theme_AppCompat:I = 0x7f0d00aa

.field public static final Theme_AppCompat_Base_CompactMenu:I = 0x7f0d00b5

.field public static final Theme_AppCompat_Base_CompactMenu_Dialog:I = 0x7f0d00b6

.field public static final Theme_AppCompat_CompactMenu:I = 0x7f0d00ae

.field public static final Theme_AppCompat_CompactMenu_Dialog:I = 0x7f0d00af

.field public static final Theme_AppCompat_Light:I = 0x7f0d00ab

.field public static final Theme_AppCompat_Light_DarkActionBar:I = 0x7f0d00ad

.field public static final Theme_AppCompat_Light_ForceSupport:I = 0x7f0d00ac

.field public static final Theme_Appirater_Dialog:I = 0x7f0d036f

.field public static final Theme_Base:I = 0x7f0d00b0

.field public static final Theme_Base_AppCompat:I = 0x7f0d00b2

.field public static final Theme_Base_AppCompat_Light:I = 0x7f0d00b3

.field public static final Theme_Base_AppCompat_Light_DarkActionBar:I = 0x7f0d00b4

.field public static final Theme_Base_Light:I = 0x7f0d00b1

.field public static final Theme_DialogNoTitle:I = 0x7f0d039f

.field public static final Theme_FBUi:I = 0x7f0d0138

.field public static final Theme_FBUi_Dialog:I = 0x7f0d02ba

.field public static final Theme_FBUi_Overlay:I = 0x7f0d0139

.field public static final Theme_Facebook:I = 0x7f0d013b

.field public static final Theme_FacebookAlertDialogTransparent:I = 0x7f0d03a2

.field public static final Theme_FacebookDark:I = 0x7f0d013a

.field public static final Theme_FacebookDarkFadeInOut:I = 0x7f0d0397

.field public static final Theme_FacebookDarkFadeInOutFast:I = 0x7f0d0398

.field public static final Theme_FacebookDialog:I = 0x7f0d03a3

.field public static final Theme_FacebookDialogSimple:I = 0x7f0d03a4

.field public static final Theme_FacebookDialogTransparent:I = 0x7f0d03a1

.field public static final Theme_FacebookFadeInOutFast:I = 0x7f0d0399

.field public static final Theme_FacebookHolo_Base_Light_Dialog:I = 0x7f0d00c6

.field public static final Theme_FacebookHolo_Light_Dialog:I = 0x7f0d00c7

.field public static final Theme_FacebookSlideRightLeft:I = 0x7f0d039a

.field public static final Theme_Facebook_Dash:I = 0x7f0d0154

.field public static final Theme_Facebook_Dash_Dialog:I = 0x7f0d02c0

.field public static final Theme_Facebook_Dash_Dialog_Animations:I = 0x7f0d02c1

.field public static final Theme_Facebook_Dash_Preference:I = 0x7f0d0156

.field public static final Theme_Facebook_Dash_Preference_Category:I = 0x7f0d0157

.field public static final Theme_Facebook_Dash_Preference_CheckBoxPreference:I = 0x7f0d0158

.field public static final Theme_Facebook_Dash_Preference_DialogPreference:I = 0x7f0d015a

.field public static final Theme_Facebook_Dash_Preference_SwitchPreference:I = 0x7f0d0159

.field public static final Theme_Facebook_Dash_Preferences:I = 0x7f0d0155

.field public static final Theme_Facebook_Dash_Preferences_Animations:I = 0x7f0d015b

.field public static final Theme_Facebook_Dash_Preferences_Dialog:I = 0x7f0d015d

.field public static final Theme_Facebook_Dash_Preferences_TextAppearance:I = 0x7f0d015c

.field public static final Theme_Facebook_Preferences:I = 0x7f0d02ef

.field public static final Theme_IorgFb4a:I = 0x7f0d03d2

.field public static final Theme_KeyguardPendingIntent:I = 0x7f0d03ca

.field public static final Theme_MinimumReqs:I = 0x7f0d0137

.field public static final Theme_MinutiaeIconPicker:I = 0x7f0d02da

.field public static final Theme_Orca:I = 0x7f0d0000

.field public static final Theme_OrcaDialog:I = 0x7f0d026b

.field public static final Theme_OrcaDialog_Map:I = 0x7f0d0296

.field public static final Theme_OrcaDialog_Neue:I = 0x7f0d0295

.field public static final Theme_OrcaDialog_Neue_CreatePinnedGroup:I = 0x7f0d029a

.field public static final Theme_OrcaDialog_Neue_MediaEditDialog:I = 0x7f0d02a0

.field public static final Theme_OrcaDialog_Neue_PhotoEditChatHeadDialog:I = 0x7f0d02a3

.field public static final Theme_OrcaDialog_PickMedia:I = 0x7f0d0297

.field public static final Theme_OrcaDialog_PickMedia_Neue:I = 0x7f0d0298

.field public static final Theme_OrcaTransparent:I = 0x7f0d0001

.field public static final Theme_Orca_AddMembers:I = 0x7f0d0293

.field public static final Theme_Orca_CardDialog:I = 0x7f0d029d

.field public static final Theme_Orca_ClickableRow:I = 0x7f0d0003

.field public static final Theme_Orca_CreateThread:I = 0x7f0d0273

.field public static final Theme_Orca_DiveHead:I = 0x7f0d0278

.field public static final Theme_Orca_DiveHead_ViewPeople:I = 0x7f0d0279

.field public static final Theme_Orca_Divebar:I = 0x7f0d027a

.field public static final Theme_Orca_Divebar_Neue:I = 0x7f0d0280

.field public static final Theme_Orca_Divebar_Neue_DiveHead:I = 0x7f0d0282

.field public static final Theme_Orca_Divebar_Neue_Reflex:I = 0x7f0d0281

.field public static final Theme_Orca_EmojiAttachmentsPopup:I = 0x7f0d028f

.field public static final Theme_Orca_EmojiAttachmentsPopup_Neue:I = 0x7f0d0290

.field public static final Theme_Orca_ListView:I = 0x7f0d000b

.field public static final Theme_Orca_MessageComposer:I = 0x7f0d0288

.field public static final Theme_Orca_MessageComposer_Neue:I = 0x7f0d0289

.field public static final Theme_Orca_MessageComposer_Neue_ChatHeads:I = 0x7f0d028a

.field public static final Theme_Orca_Neue:I = 0x7f0d026c

.field public static final Theme_Orca_Neue_AddMembers:I = 0x7f0d0294

.field public static final Theme_Orca_Neue_CreateThread:I = 0x7f0d0274

.field public static final Theme_Orca_Neue_Home:I = 0x7f0d026f

.field public static final Theme_Orca_Neue_Home_ChatHeads:I = 0x7f0d0271

.field public static final Theme_Orca_Neue_Home_Reflex:I = 0x7f0d0270

.field public static final Theme_Orca_Neue_IntentHandler:I = 0x7f0d026d

.field public static final Theme_Orca_Neue_PhotoEdit:I = 0x7f0d02a2

.field public static final Theme_Orca_Neue_Preferences:I = 0x7f0d0272

.field public static final Theme_Orca_Neue_ShareLauncherActivity:I = 0x7f0d029b

.field public static final Theme_Orca_Neue_SingleRecipient:I = 0x7f0d029f

.field public static final Theme_Orca_Neue_Start:I = 0x7f0d026e

.field public static final Theme_Orca_Neue_Transparent:I = 0x7f0d0275

.field public static final Theme_Orca_Neue_VideoEditActivity:I = 0x7f0d02a1

.field public static final Theme_Orca_OrcaContactPicker:I = 0x7f0d0276

.field public static final Theme_Orca_OrcaContactPicker_Neue:I = 0x7f0d0277

.field public static final Theme_Orca_OrcaContactPicker_Neue_GroupCreation:I = 0x7f0d029e

.field public static final Theme_Orca_Preferences:I = 0x7f0d0002

.field public static final Theme_Orca_StickerPopup:I = 0x7f0d028b

.field public static final Theme_Orca_StickerPopup_Neue:I = 0x7f0d028c

.field public static final Theme_Orca_StickerStore:I = 0x7f0d028d

.field public static final Theme_Orca_StickerStore_Neue:I = 0x7f0d028e

.field public static final Theme_Orca_ThreadList:I = 0x7f0d027b

.field public static final Theme_Orca_ThreadList_Jewel:I = 0x7f0d03ad

.field public static final Theme_Orca_ThreadList_Neue:I = 0x7f0d027d

.field public static final Theme_Orca_ThreadList_Neue_Reflex:I = 0x7f0d027f

.field public static final Theme_Orca_ThreadList_Neue_WithPresence:I = 0x7f0d027e

.field public static final Theme_Orca_ThreadSettings:I = 0x7f0d027c

.field public static final Theme_Orca_ThreadTitle:I = 0x7f0d0283

.field public static final Theme_Orca_ThreadTitle_Neue:I = 0x7f0d0284

.field public static final Theme_Orca_ThreadView:I = 0x7f0d0285

.field public static final Theme_Orca_ThreadView_Neue:I = 0x7f0d0286

.field public static final Theme_Orca_ThreadView_Neue_ChatHeads:I = 0x7f0d0287

.field public static final Theme_ReactionDialog:I = 0x7f0d0366

.field public static final Theme_SavedDashboardInterstitial:I = 0x7f0d03f0

.field public static final Theme_Transparent:I = 0x7f0d0142

.field public static final Theme_TransparentNoAnimation:I = 0x7f0d03a0

.field public static final Theme_WhiteText:I = 0x7f0d039e

.field public static final ThreadListItemDividerStyle:I = 0x7f0d0208

.field public static final ThreadListItemDividerStyle_Jewel:I = 0x7f0d03ac

.field public static final ThreadListItemDividerStyle_Neue:I = 0x7f0d0209

.field public static final ThreadListItemNamePresenceStyle:I = 0x7f0d0200

.field public static final ThreadListItemNamePresenceStyle_Neue:I = 0x7f0d0201

.field public static final ThreadListItemNameStyle:I = 0x7f0d0206

.field public static final ThreadListItemNameStyle_Jewel:I = 0x7f0d03aa

.field public static final ThreadListItemNameStyle_Neue:I = 0x7f0d0207

.field public static final ThreadListItemSnippetStyle:I = 0x7f0d0202

.field public static final ThreadListItemSnippetStyle_Neue:I = 0x7f0d0203

.field public static final ThreadListItemStyle:I = 0x7f0d01fc

.field public static final ThreadListItemStyle_Jewel:I = 0x7f0d03a8

.field public static final ThreadListItemStyle_Neue:I = 0x7f0d01fd

.field public static final ThreadListItemThreadTileStyle:I = 0x7f0d01fe

.field public static final ThreadListItemThreadTileStyle_Jewel:I = 0x7f0d03a9

.field public static final ThreadListItemThreadTileStyle_Neue:I = 0x7f0d01ff

.field public static final ThreadListItemTimeStyle:I = 0x7f0d0204

.field public static final ThreadListItemTimeStyle_Jewel:I = 0x7f0d03ab

.field public static final ThreadListItemTimeStyle_Neue:I = 0x7f0d0205

.field public static final ThreadTitleName:I = 0x7f0d020a

.field public static final ThreadTitleName_Neue:I = 0x7f0d020b

.field public static final ThreadTitleStatus:I = 0x7f0d020c

.field public static final ThreadTitleStatus_Neue:I = 0x7f0d020d

.field public static final TimelineActionBarTheme:I = 0x7f0d0333

.field public static final TimelineOverflow:I = 0x7f0d0334

.field public static final TimelinePromptBadgeText:I = 0x7f0d0337

.field public static final TimelinePromptText:I = 0x7f0d0336

.field public static final UfiServicesCommentFlyoutProfileImage:I = 0x7f0d02a4

.field public static final UploadTheme:I = 0x7f0d02e0

.field public static final UploadTheme_Transparent:I = 0x7f0d02e1

.field public static final UrlImage:I = 0x7f0d016f

.field public static final UrlImageProgressCircle:I = 0x7f0d0172

.field public static final UrlImageProgressHorizontal:I = 0x7f0d0170

.field public static final UserTileViewUrlImage:I = 0x7f0d02bf

.field public static final VaultErrorBanner:I = 0x7f0d02cc

.field public static final VoipIncallTextShadow:I = 0x7f0d0269

.field public static final WalletFragmentDefaultButtonTextAppearance:I = 0x7f0d02bd

.field public static final WalletFragmentDefaultDetailsHeaderTextAppearance:I = 0x7f0d02bc

.field public static final WalletFragmentDefaultDetailsTextAppearance:I = 0x7f0d02bb

.field public static final WalletFragmentDefaultStyle:I = 0x7f0d02be

.field public static final WallpaperSettingsActivity:I = 0x7f0d0303

.field public static final Widget:I = 0x7f0d00e8

.field public static final Widget_AppCompat_ActionBar:I = 0x7f0d0035

.field public static final Widget_AppCompat_ActionBar_Solid:I = 0x7f0d0037

.field public static final Widget_AppCompat_ActionBar_TabBar:I = 0x7f0d0046

.field public static final Widget_AppCompat_ActionBar_TabText:I = 0x7f0d004c

.field public static final Widget_AppCompat_ActionBar_TabView:I = 0x7f0d0049

.field public static final Widget_AppCompat_ActionButton:I = 0x7f0d0040

.field public static final Widget_AppCompat_ActionButton_CloseMode:I = 0x7f0d0042

.field public static final Widget_AppCompat_ActionButton_Overflow:I = 0x7f0d0044

.field public static final Widget_AppCompat_ActionMode:I = 0x7f0d0050

.field public static final Widget_AppCompat_ActivityChooserView:I = 0x7f0d006c

.field public static final Widget_AppCompat_AutoCompleteTextView:I = 0x7f0d006a

.field public static final Widget_AppCompat_Base_ActionBar:I = 0x7f0d006e

.field public static final Widget_AppCompat_Base_ActionBar_Solid:I = 0x7f0d0070

.field public static final Widget_AppCompat_Base_ActionBar_TabBar:I = 0x7f0d0079

.field public static final Widget_AppCompat_Base_ActionBar_TabText:I = 0x7f0d007f

.field public static final Widget_AppCompat_Base_ActionBar_TabView:I = 0x7f0d007c

.field public static final Widget_AppCompat_Base_ActionButton:I = 0x7f0d0073

.field public static final Widget_AppCompat_Base_ActionButton_CloseMode:I = 0x7f0d0075

.field public static final Widget_AppCompat_Base_ActionButton_Overflow:I = 0x7f0d0077

.field public static final Widget_AppCompat_Base_ActionMode:I = 0x7f0d0082

.field public static final Widget_AppCompat_Base_ActivityChooserView:I = 0x7f0d00a8

.field public static final Widget_AppCompat_Base_AutoCompleteTextView:I = 0x7f0d00a6

.field public static final Widget_AppCompat_Base_DropDownItem_Spinner:I = 0x7f0d0091

.field public static final Widget_AppCompat_Base_Light_ListView:I = 0x7f0d0094

.field public static final Widget_AppCompat_Base_ListView_DropDown:I = 0x7f0d0093

.field public static final Widget_AppCompat_Base_ListView_Menu:I = 0x7f0d0099

.field public static final Widget_AppCompat_Base_PopupMenu:I = 0x7f0d009a

.field public static final Widget_AppCompat_Base_ProgressBar:I = 0x7f0d008e

.field public static final Widget_AppCompat_Base_ProgressBar_Horizontal:I = 0x7f0d008d

.field public static final Widget_AppCompat_Base_Spinner:I = 0x7f0d008f

.field public static final Widget_AppCompat_DropDownItem_Spinner:I = 0x7f0d0059

.field public static final Widget_AppCompat_Light_ActionBar:I = 0x7f0d0036

.field public static final Widget_AppCompat_Light_ActionBar_Solid:I = 0x7f0d0038

.field public static final Widget_AppCompat_Light_ActionBar_Solid_Inverse:I = 0x7f0d0039

.field public static final Widget_AppCompat_Light_ActionBar_TabBar:I = 0x7f0d0047

.field public static final Widget_AppCompat_Light_ActionBar_TabBar_Inverse:I = 0x7f0d0048

.field public static final Widget_AppCompat_Light_ActionBar_TabText:I = 0x7f0d004d

.field public static final Widget_AppCompat_Light_ActionBar_TabText_Inverse:I = 0x7f0d004e

.field public static final Widget_AppCompat_Light_ActionBar_TabView:I = 0x7f0d004a

.field public static final Widget_AppCompat_Light_ActionBar_TabView_Inverse:I = 0x7f0d004b

.field public static final Widget_AppCompat_Light_ActionButton:I = 0x7f0d0041

.field public static final Widget_AppCompat_Light_ActionButton_CloseMode:I = 0x7f0d0043

.field public static final Widget_AppCompat_Light_ActionButton_Overflow:I = 0x7f0d0045

.field public static final Widget_AppCompat_Light_ActionMode_Inverse:I = 0x7f0d0051

.field public static final Widget_AppCompat_Light_ActivityChooserView:I = 0x7f0d006d

.field public static final Widget_AppCompat_Light_AutoCompleteTextView:I = 0x7f0d006b

.field public static final Widget_AppCompat_Light_Base_ActionBar:I = 0x7f0d006f

.field public static final Widget_AppCompat_Light_Base_ActionBar_Solid:I = 0x7f0d0071

.field public static final Widget_AppCompat_Light_Base_ActionBar_Solid_Inverse:I = 0x7f0d0072

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabBar:I = 0x7f0d007a

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabBar_Inverse:I = 0x7f0d007b

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabText:I = 0x7f0d0080

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabText_Inverse:I = 0x7f0d0081

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabView:I = 0x7f0d007d

.field public static final Widget_AppCompat_Light_Base_ActionBar_TabView_Inverse:I = 0x7f0d007e

.field public static final Widget_AppCompat_Light_Base_ActionButton:I = 0x7f0d0074

.field public static final Widget_AppCompat_Light_Base_ActionButton_CloseMode:I = 0x7f0d0076

.field public static final Widget_AppCompat_Light_Base_ActionButton_Overflow:I = 0x7f0d0078

.field public static final Widget_AppCompat_Light_Base_ActionMode_Inverse:I = 0x7f0d0083

.field public static final Widget_AppCompat_Light_Base_ActivityChooserView:I = 0x7f0d00a9

.field public static final Widget_AppCompat_Light_Base_AutoCompleteTextView:I = 0x7f0d00a7

.field public static final Widget_AppCompat_Light_Base_DropDownItem_Spinner:I = 0x7f0d0092

.field public static final Widget_AppCompat_Light_Base_ListView_DropDown:I = 0x7f0d0095

.field public static final Widget_AppCompat_Light_Base_PopupMenu:I = 0x7f0d009b

.field public static final Widget_AppCompat_Light_Base_Spinner:I = 0x7f0d0090

.field public static final Widget_AppCompat_Light_DropDownItem_Spinner:I = 0x7f0d005a

.field public static final Widget_AppCompat_Light_ListView:I = 0x7f0d005c

.field public static final Widget_AppCompat_Light_ListView_DropDown:I = 0x7f0d005d

.field public static final Widget_AppCompat_Light_PopupMenu:I = 0x7f0d0060

.field public static final Widget_AppCompat_Light_Spinner_DropDown_ActionBar:I = 0x7f0d0058

.field public static final Widget_AppCompat_ListView_DropDown:I = 0x7f0d005b

.field public static final Widget_AppCompat_ListView_Menu:I = 0x7f0d0061

.field public static final Widget_AppCompat_PopupMenu:I = 0x7f0d005f

.field public static final Widget_AppCompat_ProgressBar:I = 0x7f0d003f

.field public static final Widget_AppCompat_ProgressBar_Horizontal:I = 0x7f0d003e

.field public static final Widget_AppCompat_Spinner_DropDown_ActionBar:I = 0x7f0d0057

.field public static final Widget_Appirater_DialogMessage:I = 0x7f0d036d

.field public static final Widget_Appirater_DialogTitle:I = 0x7f0d036c

.field public static final Widget_Appirater_DialogTitleContainer:I = 0x7f0d036b

.field public static final Widget_Button:I = 0x7f0d00e9

.field public static final Widget_ButtonBar_FBUi:I = 0x7f0d02b8

.field public static final Widget_Button_FBUi:I = 0x7f0d00f5

.field public static final Widget_Button_FBUi_Borderless:I = 0x7f0d02b9

.field public static final Widget_Button_FBUi_Dark:I = 0x7f0d0103

.field public static final Widget_Button_FBUi_Dark_Primary:I = 0x7f0d0108

.field public static final Widget_Button_FBUi_Dark_Primary_Large:I = 0x7f0d010b

.field public static final Widget_Button_FBUi_Dark_Primary_Medium:I = 0x7f0d010a

.field public static final Widget_Button_FBUi_Dark_Primary_Small:I = 0x7f0d0109

.field public static final Widget_Button_FBUi_Dark_Regular:I = 0x7f0d0104

.field public static final Widget_Button_FBUi_Dark_Regular_Large:I = 0x7f0d0107

.field public static final Widget_Button_FBUi_Dark_Regular_Medium:I = 0x7f0d0106

.field public static final Widget_Button_FBUi_Dark_Regular_Small:I = 0x7f0d0105

.field public static final Widget_Button_FBUi_Dark_Special:I = 0x7f0d010c

.field public static final Widget_Button_FBUi_Dark_Special_Large:I = 0x7f0d010f

.field public static final Widget_Button_FBUi_Dark_Special_Medium:I = 0x7f0d010e

.field public static final Widget_Button_FBUi_Dark_Special_Small:I = 0x7f0d010d

.field public static final Widget_Button_FBUi_Light:I = 0x7f0d00f6

.field public static final Widget_Button_FBUi_Light_Primary:I = 0x7f0d00fb

.field public static final Widget_Button_FBUi_Light_Primary_Large:I = 0x7f0d00fe

.field public static final Widget_Button_FBUi_Light_Primary_Medium:I = 0x7f0d00fd

.field public static final Widget_Button_FBUi_Light_Primary_Small:I = 0x7f0d00fc

.field public static final Widget_Button_FBUi_Light_Regular:I = 0x7f0d00f7

.field public static final Widget_Button_FBUi_Light_Regular_Large:I = 0x7f0d00fa

.field public static final Widget_Button_FBUi_Light_Regular_Medium:I = 0x7f0d00f9

.field public static final Widget_Button_FBUi_Light_Regular_Small:I = 0x7f0d00f8

.field public static final Widget_Button_FBUi_Light_Special:I = 0x7f0d00ff

.field public static final Widget_Button_FBUi_Light_Special_Large:I = 0x7f0d0102

.field public static final Widget_Button_FBUi_Light_Special_Medium:I = 0x7f0d0101

.field public static final Widget_Button_FBUi_Light_Special_Small:I = 0x7f0d0100

.field public static final Widget_Card:I = 0x7f0d00ea

.field public static final Widget_Card_FBUi:I = 0x7f0d012e

.field public static final Widget_Card_FBUi_White:I = 0x7f0d012f

.field public static final Widget_CheckBox:I = 0x7f0d00ec

.field public static final Widget_CheckBox_FBUi:I = 0x7f0d00ee

.field public static final Widget_CheckBox_FBUi_Dark:I = 0x7f0d00ef

.field public static final Widget_CheckBox_FBUi_Light:I = 0x7f0d00f0

.field public static final Widget_CheckedContentView:I = 0x7f0d0134

.field public static final Widget_ContentView:I = 0x7f0d0132

.field public static final Widget_ContentViewWithButton:I = 0x7f0d0133

.field public static final Widget_DialogMessage_FBUi:I = 0x7f0d02b7

.field public static final Widget_DialogTitleContainer_FBUi:I = 0x7f0d02b5

.field public static final Widget_DialogTitle_FBUi:I = 0x7f0d02b6

.field public static final Widget_FacebookHolo_Base_EditText:I = 0x7f0d00b8

.field public static final Widget_FacebookHolo_Light_Button_Borderless_Small:I = 0x7f0d00c4

.field public static final Widget_FacebookHolo_Light_EditText:I = 0x7f0d00c5

.field public static final Widget_Header:I = 0x7f0d012b

.field public static final Widget_Header_FBUi:I = 0x7f0d012c

.field public static final Widget_Header_FBUi_List:I = 0x7f0d012d

.field public static final Widget_Holo_ProgressBar_Clockwise:I = 0x7f0d000c

.field public static final Widget_Holo_ProgressBar_Large_Clockwise:I = 0x7f0d000d

.field public static final Widget_Holo_ProgressBar_Small_Clockwise:I = 0x7f0d000e

.field public static final Widget_ImageButton_FBUi:I = 0x7f0d0110

.field public static final Widget_ImageButton_FBUi_Dark:I = 0x7f0d011e

.field public static final Widget_ImageButton_FBUi_Dark_Primary:I = 0x7f0d0123

.field public static final Widget_ImageButton_FBUi_Dark_Primary_Large:I = 0x7f0d0126

.field public static final Widget_ImageButton_FBUi_Dark_Primary_Medium:I = 0x7f0d0125

.field public static final Widget_ImageButton_FBUi_Dark_Primary_Small:I = 0x7f0d0124

.field public static final Widget_ImageButton_FBUi_Dark_Regular:I = 0x7f0d011f

.field public static final Widget_ImageButton_FBUi_Dark_Regular_Large:I = 0x7f0d0122

.field public static final Widget_ImageButton_FBUi_Dark_Regular_Medium:I = 0x7f0d0121

.field public static final Widget_ImageButton_FBUi_Dark_Regular_Small:I = 0x7f0d0120

.field public static final Widget_ImageButton_FBUi_Dark_Special:I = 0x7f0d0127

.field public static final Widget_ImageButton_FBUi_Dark_Special_Large:I = 0x7f0d012a

.field public static final Widget_ImageButton_FBUi_Dark_Special_Medium:I = 0x7f0d0129

.field public static final Widget_ImageButton_FBUi_Dark_Special_Small:I = 0x7f0d0128

.field public static final Widget_ImageButton_FBUi_Light:I = 0x7f0d0111

.field public static final Widget_ImageButton_FBUi_Light_Primary:I = 0x7f0d0116

.field public static final Widget_ImageButton_FBUi_Light_Primary_Large:I = 0x7f0d0119

.field public static final Widget_ImageButton_FBUi_Light_Primary_Medium:I = 0x7f0d0118

.field public static final Widget_ImageButton_FBUi_Light_Primary_Small:I = 0x7f0d0117

.field public static final Widget_ImageButton_FBUi_Light_Regular:I = 0x7f0d0112

.field public static final Widget_ImageButton_FBUi_Light_Regular_Large:I = 0x7f0d0115

.field public static final Widget_ImageButton_FBUi_Light_Regular_Medium:I = 0x7f0d0114

.field public static final Widget_ImageButton_FBUi_Light_Regular_Small:I = 0x7f0d0113

.field public static final Widget_ImageButton_FBUi_Light_Special:I = 0x7f0d011a

.field public static final Widget_ImageButton_FBUi_Light_Special_Large:I = 0x7f0d011d

.field public static final Widget_ImageButton_FBUi_Light_Special_Medium:I = 0x7f0d011c

.field public static final Widget_ImageButton_FBUi_Light_Special_Small:I = 0x7f0d011b

.field public static final Widget_ListView:I = 0x7f0d00eb

.field public static final Widget_ListView_FBUi:I = 0x7f0d00f4

.field public static final Widget_ListView_FriendsNearby:I = 0x7f0d030f

.field public static final Widget_Messenger:I = 0x7f0d017c

.field public static final Widget_Messenger_Button:I = 0x7f0d017d

.field public static final Widget_Messenger_Button_ContactPickerSectionHeader:I = 0x7f0d0181

.field public static final Widget_Messenger_Button_Dark:I = 0x7f0d017e

.field public static final Widget_Messenger_Button_DivebarQuickPromotion:I = 0x7f0d017f

.field public static final Widget_Messenger_Button_DivebarQuickPromotion_Secondary:I = 0x7f0d0180

.field public static final Widget_Messenger_Button_EditFavorites:I = 0x7f0d0182

.field public static final Widget_Messenger_ComposerEditText:I = 0x7f0d0184

.field public static final Widget_Messenger_ComposerLocationButton:I = 0x7f0d0187

.field public static final Widget_Messenger_Divebar:I = 0x7f0d018c

.field public static final Widget_Messenger_Divebar_Button:I = 0x7f0d018d

.field public static final Widget_Messenger_Divebar_Button_ContactPickerSectionHeader:I = 0x7f0d018e

.field public static final Widget_Messenger_Divebar_EditFavoritesButton:I = 0x7f0d018f

.field public static final Widget_Messenger_Divebar_EditFavoritesButton_AddItem:I = 0x7f0d0191

.field public static final Widget_Messenger_Divebar_EditFavoritesButton_DeleteItem:I = 0x7f0d0190

.field public static final Widget_Messenger_EditText:I = 0x7f0d0183

.field public static final Widget_Messenger_EmojiAttachmentsButton:I = 0x7f0d0188

.field public static final Widget_Messenger_EmojiAttachmentsButton_Backspace:I = 0x7f0d018a

.field public static final Widget_Messenger_EmojiAttachmentsButton_Backspace_Backside:I = 0x7f0d018b

.field public static final Widget_Messenger_EmojiAttachmentsButton_ShowMore:I = 0x7f0d0189

.field public static final Widget_Messenger_ListView:I = 0x7f0d0185

.field public static final Widget_Messenger_ListView_ContactPicker:I = 0x7f0d0186

.field public static final Widget_Messenger_Neue:I = 0x7f0d0192

.field public static final Widget_Messenger_Neue_Button:I = 0x7f0d0193

.field public static final Widget_Messenger_Neue_Button_AttachmentMethod:I = 0x7f0d019a

.field public static final Widget_Messenger_Neue_Button_ContactPickerSectionHeader:I = 0x7f0d0196

.field public static final Widget_Messenger_Neue_Button_EditFavorites:I = 0x7f0d0197

.field public static final Widget_Messenger_Neue_Button_EditFavorites_AddItem:I = 0x7f0d0199

.field public static final Widget_Messenger_Neue_Button_EditFavorites_DeleteItem:I = 0x7f0d0198

.field public static final Widget_Messenger_Neue_Button_PanelBottom:I = 0x7f0d0195

.field public static final Widget_Messenger_Neue_Button_Rounded:I = 0x7f0d0194

.field public static final Widget_Messenger_Neue_ComposerLocationButton:I = 0x7f0d019d

.field public static final Widget_Messenger_Neue_EmojiAttachmentsButton:I = 0x7f0d019e

.field public static final Widget_Messenger_Neue_EmojiAttachmentsButton_Backspace:I = 0x7f0d01a0

.field public static final Widget_Messenger_Neue_EmojiAttachmentsButton_Backspace_Backside:I = 0x7f0d01a1

.field public static final Widget_Messenger_Neue_EmojiAttachmentsButton_ShowMore:I = 0x7f0d019f

.field public static final Widget_Messenger_Neue_ListView:I = 0x7f0d019b

.field public static final Widget_Messenger_Neue_ListView_ContactPicker:I = 0x7f0d019c

.field public static final Widget_ProgressBar:I = 0x7f0d0130

.field public static final Widget_ProgressBar_FBUi:I = 0x7f0d0131

.field public static final Widget_RadioButton:I = 0x7f0d00ed

.field public static final Widget_RadioButton_FBUi:I = 0x7f0d00f1

.field public static final Widget_RadioButton_FBUi_Dark:I = 0x7f0d00f2

.field public static final Widget_RadioButton_FBUi_Light:I = 0x7f0d00f3

.field public static final Widget_TabbedViewPagerIndicatorStyle:I = 0x7f0d0135

.field public static final ZeroBlueButton:I = 0x7f0d0179

.field public static final ZeroDarkButton:I = 0x7f0d0178

.field public static final ZeroFullscreenDialog:I = 0x7f0d02df

.field public static final ZeroModalDialog:I = 0x7f0d0177

.field public static final ZoomControls:I = 0x7f0d039b

.field public static final activity_style:I = 0x7f0d000f

.field public static final actor_caption_text:I = 0x7f0d0148

.field public static final audience_picker_fixed_text_view:I = 0x7f0d02d4

.field public static final bauble_button_label_style:I = 0x7f0d0151

.field public static final bookmark_divider_tab_text:I = 0x7f0d02f8

.field public static final bookmark_divider_text:I = 0x7f0d02f7

.field public static final bookmark_item_text:I = 0x7f0d02fa

.field public static final bookmark_item_text_base:I = 0x7f0d02f9

.field public static final bookmark_tab_item_text:I = 0x7f0d02fb

.field public static final caption_container_spacing_style:I = 0x7f0d0144

.field public static final caption_message_text:I = 0x7f0d0147

.field public static final caption_suffix_text:I = 0x7f0d0149

.field public static final caption_title_text:I = 0x7f0d0146

.field public static final category_icon:I = 0x7f0d0384

.field public static final collection_facepile_image:I = 0x7f0d03f6

.field public static final collections_collection_card:I = 0x7f0d03f2

.field public static final collections_collection_card_bottom:I = 0x7f0d03f5

.field public static final collections_collection_card_middle:I = 0x7f0d03f4

.field public static final collections_collection_card_top:I = 0x7f0d03f3

.field public static final common_caption_text_style:I = 0x7f0d0145

.field public static final composer_add_photo:I = 0x7f0d02d5

.field public static final condensed_ufi_text:I = 0x7f0d02a6

.field public static final contactPickerEmptyListItemStyle:I = 0x7f0d01b9

.field public static final contactPickerEmptyListItemStyle_Divebar:I = 0x7f0d01ba

.field public static final cover_photo_upload_text:I = 0x7f0d0330

.field public static final crop_image_fragment_text:I = 0x7f0d02ea

.field public static final dialogAnimations:I = 0x7f0d029c

.field public static final dialog_text:I = 0x7f0d03a7

.field public static final disclosure_links_link:I = 0x7f0d014f

.field public static final disclosure_links_text:I = 0x7f0d014e

.field public static final disclosure_text:I = 0x7f0d014d

.field public static final ego_feed_unit_item_bottom_title_text:I = 0x7f0d02aa

.field public static final error_text:I = 0x7f0d015f

.field public static final event_card_location_etc_text:I = 0x7f0d03c7

.field public static final event_card_social_context_text:I = 0x7f0d03c8

.field public static final event_card_time_etc_text:I = 0x7f0d03c6

.field public static final event_card_title_text:I = 0x7f0d03c5

.field public static final event_permalink_subtitle_text:I = 0x7f0d03b1

.field public static final event_permalink_summary_item_action_text:I = 0x7f0d03b4

.field public static final event_permalink_summary_item_subtitle_text:I = 0x7f0d03b3

.field public static final event_permalink_summary_item_title_text:I = 0x7f0d03b2

.field public static final event_permalink_title_text:I = 0x7f0d03b0

.field public static final events_dashboard_card_title:I = 0x7f0d03c3

.field public static final events_dashboard_card_view_all:I = 0x7f0d03c4

.field public static final events_dashboard_create_button_text:I = 0x7f0d03b6

.field public static final events_dashboard_row_context_text:I = 0x7f0d03bb

.field public static final events_dashboard_row_day_text:I = 0x7f0d03be

.field public static final events_dashboard_row_inline_rsvp_text:I = 0x7f0d03bc

.field public static final events_dashboard_row_location_text:I = 0x7f0d03ba

.field public static final events_dashboard_row_month_text:I = 0x7f0d03bd

.field public static final events_dashboard_row_name_text:I = 0x7f0d03b8

.field public static final events_dashboard_row_time_text:I = 0x7f0d03b9

.field public static final events_dashboard_selector_header:I = 0x7f0d03b5

.field public static final events_dashboard_sticky_section_header_text:I = 0x7f0d03b7

.field public static final events_dashboard_view_all_row_text:I = 0x7f0d03bf

.field public static final fb4a_login_button:I = 0x7f0d0150

.field public static final feed_coupon_button:I = 0x7f0d02a7

.field public static final feed_new_stories_button:I = 0x7f0d02a9

.field public static final feed_settings_contact_item_text:I = 0x7f0d0371

.field public static final feed_sources_list_view:I = 0x7f0d0370

.field public static final footer_bar_button:I = 0x7f0d02d2

.field public static final form_text_field:I = 0x7f0d037f

.field public static final friend_menu_item:I = 0x7f0d0329

.field public static final friend_menu_item_button:I = 0x7f0d032c

.field public static final friend_menu_item_group:I = 0x7f0d032b

.field public static final friend_menu_item_text:I = 0x7f0d032a

.field public static final friend_requests_confirm_button:I = 0x7f0d0164

.field public static final friend_requests_reply_button:I = 0x7f0d0163

.field public static final gating_text:I = 0x7f0d0152

.field public static final gift_card_mall_selector_text:I = 0x7f0d037e

.field public static final gifts_button:I = 0x7f0d0381

.field public static final grab_bag_selector_text:I = 0x7f0d037d

.field public static final gruops_feed_publisher_button:I = 0x7f0d03f1

.field public static final homesetup_button:I = 0x7f0d030a

.field public static final homesetup_description_text:I = 0x7f0d0305

.field public static final homesetup_dialog_button:I = 0x7f0d030b

.field public static final homesetup_links_link:I = 0x7f0d0307

.field public static final homesetup_links_text:I = 0x7f0d0306

.field public static final homesetup_option_button:I = 0x7f0d0308

.field public static final homesetup_option_checkbox:I = 0x7f0d0309

.field public static final homesetup_settings_group:I = 0x7f0d030c

.field public static final homesetup_settings_group_title:I = 0x7f0d030d

.field public static final homesetup_title_text:I = 0x7f0d0304

.field public static final horizontal_button_bar:I = 0x7f0d002c

.field public static final horizontal_divider:I = 0x7f0d0302

.field public static final horizontal_selector_text:I = 0x7f0d037c

.field public static final identity_growth_progress_circle:I = 0x7f0d02ec

.field public static final launcher_action_button:I = 0x7f0d0160

.field public static final light:I = 0x7f0d0011

.field public static final light_billboard:I = 0x7f0d0012

.field public static final light_large:I = 0x7f0d0014

.field public static final light_xlarge:I = 0x7f0d0013

.field public static final lockscreen_navigation_button:I = 0x7f0d02dc

.field public static final lockscreen_switch:I = 0x7f0d02dd

.field public static final megaphone_facepile_image:I = 0x7f0d02ed

.field public static final music_notification_app_label:I = 0x7f0d0301

.field public static final music_notification_text_with_shadow:I = 0x7f0d0300

.field public static final nag_screen_button:I = 0x7f0d0162

.field public static final nag_screen_text:I = 0x7f0d0161

.field public static final navigation_button:I = 0x7f0d002e

.field public static final navigation_button_secondary:I = 0x7f0d002f

.field public static final navigation_row_section_group:I = 0x7f0d0030

.field public static final new_bookmark_item_selectable:I = 0x7f0d03d4

.field public static final notification_message_badge:I = 0x7f0d02fe

.field public static final notification_message_text:I = 0x7f0d02fd

.field public static final notification_rear_text:I = 0x7f0d02ff

.field public static final notification_text:I = 0x7f0d02fc

.field public static final num_photos_text_view:I = 0x7f0d02d3

.field public static final nux_balloon_text:I = 0x7f0d015e

.field public static final nux_bubble_text:I = 0x7f0d0168

.field public static final order_review_cost_totals_layout:I = 0x7f0d0380

.field public static final pandora_benny_loading_spinner_style:I = 0x7f0d03df

.field public static final photo_caption_suffix_link:I = 0x7f0d02af

.field public static final place_details:I = 0x7f0d0385

.field public static final plutonium_action_bar_button:I = 0x7f0d031b

.field public static final plutonium_action_bar_text_disabled:I = 0x7f0d031e

.field public static final plutonium_action_bar_text_highlighted:I = 0x7f0d031d

.field public static final plutonium_action_bar_text_normal:I = 0x7f0d031c

.field public static final plutonium_friend_requests_reply_button:I = 0x7f0d0331

.field public static final plutonium_timeline_alternate_name:I = 0x7f0d0321

.field public static final plutonium_timeline_publisher_bar_button:I = 0x7f0d0323

.field public static final preference_switch_text_style:I = 0x7f0d0153

.field public static final premium_videos_sound_button_text:I = 0x7f0d02a8

.field public static final profile_info_request_button:I = 0x7f0d03cd

.field public static final progress_bar:I = 0x7f0d0386

.field public static final publisher_button_text:I = 0x7f0d01a3

.field public static final reaction_attachment_profile_story_actor:I = 0x7f0d0367

.field public static final reaction_attachment_profile_story_text:I = 0x7f0d0368

.field public static final regular:I = 0x7f0d0015

.field public static final regular_large:I = 0x7f0d0016

.field public static final regular_medium:I = 0x7f0d0017

.field public static final regular_small:I = 0x7f0d0018

.field public static final regular_xsmall:I = 0x7f0d0019

.field public static final related_videos_card_like_count:I = 0x7f0d037b

.field public static final related_videos_card_title:I = 0x7f0d037a

.field public static final related_videos_carousel_item_like_count:I = 0x7f0d0378

.field public static final related_videos_carousel_item_like_image:I = 0x7f0d0379

.field public static final related_videos_carousel_item_title:I = 0x7f0d0377

.field public static final related_videos_carousel_title:I = 0x7f0d0376

.field public static final review_creator_name_text:I = 0x7f0d02d1

.field public static final review_feedback_text:I = 0x7f0d02d0

.field public static final review_text_medium:I = 0x7f0d02ce

.field public static final review_text_small:I = 0x7f0d02cf

.field public static final saved_filter_action_text:I = 0x7f0d03ea

.field public static final saved_filter_title:I = 0x7f0d03e9

.field public static final saved_header_title:I = 0x7f0d03e5

.field public static final saved_interstitial_subtitle:I = 0x7f0d03ef

.field public static final saved_interstitial_title:I = 0x7f0d03ee

.field public static final saved_item_subtitle:I = 0x7f0d03e7

.field public static final saved_item_title:I = 0x7f0d03e6

.field public static final saved_item_undo_button:I = 0x7f0d03e8

.field public static final saved_load_more_failed_text:I = 0x7f0d03ed

.field public static final saved_section_list_item_subtitle:I = 0x7f0d03ec

.field public static final saved_section_list_item_title:I = 0x7f0d03eb

.field public static final secondary_tab_button:I = 0x7f0d0165

.field public static final seconday_tab_bar_vertical_separator:I = 0x7f0d0166

.field public static final setup_flow_content_container:I = 0x7f0d0010

.field public static final shadowedWhiteText:I = 0x7f0d034e

.field public static final shadowed_button:I = 0x7f0d0382

.field public static final shadowed_button_white_text:I = 0x7f0d0383

.field public static final shadowed_white_text:I = 0x7f0d001a

.field public static final shadowed_white_text_bold:I = 0x7f0d0026

.field public static final shadowed_white_text_bold_large:I = 0x7f0d0028

.field public static final shadowed_white_text_bold_medium:I = 0x7f0d0029

.field public static final shadowed_white_text_bold_small:I = 0x7f0d002a

.field public static final shadowed_white_text_bold_small_dimmed:I = 0x7f0d002b

.field public static final shadowed_white_text_bold_xlarge:I = 0x7f0d0027

.field public static final shadowed_white_text_light:I = 0x7f0d001b

.field public static final shadowed_white_text_light_billboard:I = 0x7f0d001c

.field public static final shadowed_white_text_light_xlarge:I = 0x7f0d001d

.field public static final shadowed_white_text_regular:I = 0x7f0d001e

.field public static final shadowed_white_text_regular_large:I = 0x7f0d001f

.field public static final shadowed_white_text_regular_large_cover_feed:I = 0x7f0d0020

.field public static final shadowed_white_text_regular_medium:I = 0x7f0d0021

.field public static final shadowed_white_text_regular_medium_cover_feed:I = 0x7f0d0022

.field public static final shadowed_white_text_regular_small:I = 0x7f0d0023

.field public static final shadowed_white_text_regular_small_dimmed:I = 0x7f0d0025

.field public static final shadowed_white_text_regular_xsmall:I = 0x7f0d0024

.field public static final spinner:I = 0x7f0d03c2

.field public static final sports_stories_team_name:I = 0x7f0d0363

.field public static final sports_stories_with_scores_team_score:I = 0x7f0d0364

.field public static final story_attachment_app_name_text:I = 0x7f0d0033

.field public static final story_attachment_robotext_text:I = 0x7f0d0034

.field public static final story_attachment_subtitle_text:I = 0x7f0d0032

.field public static final story_attachment_title_text:I = 0x7f0d0031

.field public static final timeline_action_button:I = 0x7f0d0324

.field public static final timeline_action_button_text:I = 0x7f0d0325

.field public static final timeline_alternate_name:I = 0x7f0d032d

.field public static final timeline_navtile_image:I = 0x7f0d032e

.field public static final timeline_profile_image_view:I = 0x7f0d0322

.field public static final timeline_publisher_bar_text:I = 0x7f0d0335

.field public static final timeline_publisher_button:I = 0x7f0d0327

.field public static final timeline_publisher_button_text:I = 0x7f0d0326

.field public static final timeline_publisher_button_withouttext:I = 0x7f0d0328

.field public static final timeline_year_overview_show_more:I = 0x7f0d0332

.field public static final travel_welcome_feed_unit_cover_text:I = 0x7f0d02ab

.field public static final typeahead_divider:I = 0x7f0d0169

.field public static final typeahead_divider_fig:I = 0x7f0d016a

.field public static final typeahead_divider_fig_header:I = 0x7f0d016b

.field public static final ubersearch_result_item_details_text:I = 0x7f0d0393

.field public static final ubersearch_result_item_title_text:I = 0x7f0d0392

.field public static final ufi_button_text:I = 0x7f0d02a5

.field public static final ufi_popover_header:I = 0x7f0d014b

.field public static final ufi_popover_text:I = 0x7f0d014c

.field public static final ufi_text:I = 0x7f0d014a

.field public static final user_timeline_name:I = 0x7f0d032f

.field public static final vaultDeletingBarHorizontal:I = 0x7f0d02cd

.field public static final vertical_button_bar:I = 0x7f0d002d

.field public static final video_postplay_suggestions_title_style:I = 0x7f0d0375

.field public static final video_suggestion_author_style:I = 0x7f0d0373

.field public static final video_suggestion_title_style:I = 0x7f0d0372

.field public static final video_suggestions_likes_views_style:I = 0x7f0d0374

.field public static final view_photo_button:I = 0x7f0d02ae

.field public static final white_text_with_shadow:I = 0x7f0d0141


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
