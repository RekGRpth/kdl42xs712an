.class public Lcom/facebook/dalvik/DalvikInternals;
.super Ljava/lang/Object;
.source "DalvikInternals.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "fb_dalvik-internals"

    invoke-static {v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/dalvik/DalvikInternals;->init()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()J
    .locals 16

    const-wide/16 v9, 0x0

    const/high16 v6, 0x1000000

    invoke-static {}, Lcom/facebook/dalvik/ProcSelfMaps;->a()Lcom/facebook/dalvik/ProcSelfMaps;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/dalvik/ProcSelfMaps;->b()Lcom/facebook/dalvik/ProcSelfMaps;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/dalvik/ProcSelfMaps;->c()[J

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    packed-switch v2, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    const-string v2, "LinearAlloc"

    invoke-virtual {v1, v2}, Lcom/facebook/dalvik/ProcSelfMaps;->a(Ljava/lang/String;)Lcom/facebook/dalvik/ProcSelfMaps$Mapping;

    move-result-object v12

    if-nez v12, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Could not find LinearAlloc memory mapping."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v3, 0x14

    goto :goto_0

    :pswitch_1
    const/16 v3, 0x18

    goto :goto_0

    :cond_0
    const-string v2, "[heap]"

    invoke-virtual {v1, v2}, Lcom/facebook/dalvik/ProcSelfMaps;->a(Ljava/lang/String;)Lcom/facebook/dalvik/ProcSelfMaps$Mapping;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->a()J

    move-result-wide v7

    invoke-virtual {v1}, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->b()J

    move-result-wide v9

    :goto_1
    const/16 v1, 0x2c0

    const/16 v2, 0x5c8

    const/4 v4, 0x3

    const/high16 v5, 0x80000

    const/high16 v11, 0x500000

    const/16 v13, 0x1004

    invoke-virtual {v12}, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->a()J

    move-result-wide v14

    move v12, v6

    invoke-static/range {v0 .. v15}, Lcom/facebook/dalvik/DalvikInternals;->nativeFindLinearAllocHeader([JIIIIIIJJIIIJ)J

    move-result-wide v0

    return-wide v0

    :cond_1
    move-wide v7, v9

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static native deleteRecursive(Ljava/lang/String;)V
.end method

.method public static native dumpLinearAllocProfiles(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public static native fixDvmForCrossDexHack()V
.end method

.method public static native getLinearAllocUsage(J)I
.end method

.method public static native getOwnerUid(Ljava/lang/String;)I
.end method

.method private static native init()V
.end method

.method public static native link(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method private static native nativeFindLinearAllocHeader([JIIIIIIJJIIIJ)J
.end method

.method public static native noop()V
.end method

.method public static native printLinearAllocHeaderInfo()V
.end method

.method public static native readOdexDepBlock(Ljava/lang/String;)[B
.end method

.method public static native replaceLinearAllocBuffer(JII)V
.end method

.method public static native resetLinearAllocProfiles()V
.end method
