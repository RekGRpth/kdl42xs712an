.class public Lcom/facebook/dalvik/ProcSelfMaps$Mapping;
.super Ljava/lang/Object;
.source "ProcSelfMaps.java"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Ljava/lang/String;


# direct methods
.method constructor <init>(JJZZZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->a:J

    iput-wide p3, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->b:J

    iput-boolean p5, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->c:Z

    iput-boolean p6, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->d:Z

    iput-boolean p7, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->e:Z

    iput-object p8, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->a:J

    return-wide v0
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->b:J

    return-wide v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->c:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/dalvik/ProcSelfMaps$Mapping;->f:Ljava/lang/String;

    return-object v0
.end method
