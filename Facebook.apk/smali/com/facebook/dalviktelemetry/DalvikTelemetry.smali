.class public Lcom/facebook/dalviktelemetry/DalvikTelemetry;
.super Ljava/lang/Object;
.source "DalvikTelemetry.java"


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static b:Lcom/facebook/analytics/logger/HoneyClientEvent;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static c:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private static d:Ljava/lang/Throwable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;

    sput-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 14

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v4

    mul-int/2addr v4, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    mul-int/2addr v5, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v0

    mul-int v6, v0, v1

    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    const-string v1, "lib"

    invoke-direct {v7, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    new-array v0, v2, [Ljava/io/File;

    :cond_0
    array-length v8, v0

    move v1, v2

    :goto_0
    if-ge v1, v8, :cond_5

    aget-object v9, v0, v1

    invoke-virtual {v9}, Ljava/io/File;->canRead()Z

    move-result v9

    if-nez v9, :cond_3

    move v1, v2

    :goto_1
    const-string v8, "dalvik_hack_library_not_loaded"

    invoke-static {v8}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "so_count"

    array-length v10, v0

    invoke-virtual {v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "so_readable"

    invoke-virtual {v8, v9, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "total_storage_size"

    invoke-virtual {v8, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "avail_storage_size"

    invoke-virtual {v8, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "free_storage_size"

    invoke-virtual {v8, v1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lib_dir"

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "read="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v7}, Ljava/io/File;->canRead()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-static {v7}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/io/File;)Lcom/facebook/common/util/TriState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/common/util/TriState;->isSet()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, ", execute="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/common/util/TriState;->asBoolean()Z

    move-result v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    const-string v4, "lib_dir_info"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    array-length v6, v0

    move v4, v2

    :goto_2
    if-ge v4, v6, :cond_4

    aget-object v7, v0, v4

    const-string v1, "lib_dir_%s_size"

    new-array v9, v3, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v2

    invoke-static {v1, v9}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const-string v1, "lib_dir_%s_info"

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v2

    invoke-static {v1, v10}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v1, "read=%b"

    new-array v11, v3, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/File;->canRead()Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v11, v2

    invoke-static {v1, v11}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/io/File;)Lcom/facebook/common/util/TriState;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/common/util/TriState;->isSet()Z

    move-result v12

    if-eqz v12, :cond_2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, ", execute=%s"

    new-array v13, v3, [Ljava/lang/Object;

    invoke-virtual {v11}, Lcom/facebook/common/util/TriState;->asBoolean()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v13, v2

    invoke-static {v12, v13}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v11

    invoke-virtual {v8, v9, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v8, v10, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_4
    const-string v0, "lib_dir_content"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    return-object v8

    :cond_5
    move v1, v3

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/facebook/analytics/tagging/AnalyticsTag;->MODULE_APP:Lcom/facebook/analytics/tagging/AnalyticsTag;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Lcom/facebook/analytics/tagging/AnalyticsTag;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sdk_version"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fingerprint"

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "device"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "cpu_abi"

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    return-object v0
.end method

.method private static a(Ljava/io/File;)Lcom/facebook/common/util/TriState;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->canExecute()Z

    move-result v0

    invoke-static {v0}, Lcom/facebook/common/util/TriState;->valueOf(Z)Lcom/facebook/common/util/TriState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/common/util/TriState;->UNSET:Lcom/facebook/common/util/TriState;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "dalvik_hack_telemetry"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create dir: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->NORMAL:Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;

    invoke-static {}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    :try_start_0
    const-string v5, "attempted"

    invoke-static {p0, p1, v5}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v2, "attempted"

    const/4 v5, 0x0

    invoke-static {p0, p1, v2, v5}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->TEST:Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    const-string v1, "dalvik_hack_storage_failure"

    sput-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    sput-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    const-string v1, "dalvik_hack_storage_failure"

    invoke-static {v1}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    new-instance v2, Ljava/io/StringWriter;

    invoke-direct {v2}, Ljava/io/StringWriter;-><init>()V

    new-instance v3, Ljava/io/PrintWriter;

    invoke-direct {v3, v2}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v3}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintWriter;)V

    const-string v3, "io_stack_trace"

    invoke-virtual {v2}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "io_exception"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    sput-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a:Ljava/lang/Class;

    const-string v2, "IOException"

    invoke-static {v1, v2, v0}, Lcom/facebook/debug/log/BLog;->e(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/util/Random;->nextFloat()F

    move-result v2

    const v3, 0x3c23d70a    # 0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    const-string v2, "attempted"

    const/4 v3, 0x1

    invoke-static {p0, p1, v2, v3}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    sget-object v2, Lcom/facebook/dalvik/DalvikLinearAllocType;->FBANDROID_RELEASE:Lcom/facebook/dalvik/DalvikLinearAllocType;

    invoke-static {v2}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b(Lcom/facebook/dalvik/DalvikLinearAllocType;)V

    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->TEST:Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;

    const-string v2, "attempted"

    const/4 v3, 0x0

    invoke-static {p0, p1, v2, v3}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v2, v0

    move v3, v4

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {v2}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v3, :cond_8

    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a()Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    move-result-object v0

    :goto_2
    sget-object v4, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->NORMAL:Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;

    if-ne v2, v4, :cond_9

    sget-object v4, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v4, :cond_4

    const-string v1, "dalvik_hack_failure"

    invoke-static {v1}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "dalvik_hack_failure"

    sput-object v4, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "Failed to apply dalvik hack."

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    :cond_4
    :goto_3
    sget-object v4, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->NOT_ATTEMPTED_NATIVE_LIBRARY_NOT_LOADED:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v4, :cond_5

    invoke-static {p0}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    const-string v1, "dalvik_hack_error"

    invoke-static {v1}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "report_type"

    invoke-virtual {v2}, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "result"

    invoke-virtual {v0}, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "dalvik_hack_error"

    sput-object v2, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/Throwable;

    const-string v4, "Unexpected telemetry state."

    invoke-direct {v2, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    :cond_6
    sget-object v2, Lcom/facebook/analytics/tagging/AnalyticsTag;->MODULE_APP:Lcom/facebook/analytics/tagging/AnalyticsTag;

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Lcom/facebook/analytics/tagging/AnalyticsTag;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v2, :cond_7

    const-string v2, "failure_string"

    if-nez v3, :cond_b

    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    :cond_7
    sput-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_1

    :cond_8
    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    goto :goto_2

    :cond_9
    sget-object v4, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->SUCCESS:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v4, :cond_a

    const-string v1, "dalvik_hack_telemetry_success"

    invoke-static {v1}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    goto :goto_3

    :cond_a
    sget-object v4, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v4, :cond_4

    const-string v1, "dalvik_hack_telemetry_failure"

    invoke-static {v1}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v4, "dalvik_hack_telemetry_failure"

    sput-object v4, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/Throwable;

    const-string v5, "Failed to apply dalvik hack."

    invoke-direct {v4, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    goto :goto_3

    :cond_b
    const-string v0, "Previous attempt crashed the process"

    goto :goto_4

    :catch_0
    move-exception v2

    move v3, v4

    move-object v6, v0

    move-object v0, v2

    move-object v2, v6

    goto/16 :goto_0

    :catch_1
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    goto/16 :goto_0

    :cond_c
    move-object v2, v0

    move v3, v4

    move-object v0, v1

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    invoke-static {p0, p1, p2}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-ne p3, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write(I)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create a file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to delete a file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Lcom/facebook/inject/FbInjector;)V
    .locals 3

    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/analytics/logger/AnalyticsLogger;

    invoke-virtual {p0, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/analytics/logger/AnalyticsLogger;

    sget-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->b:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, Lcom/facebook/analytics/logger/AnalyticsLogger;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    :cond_0
    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    const-class v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    invoke-virtual {p0, v0}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/errorreporting/FbErrorReporter;

    sget-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->c:Ljava/lang/String;

    sget-object v2, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->d:Ljava/lang/Throwable;

    invoke-interface {v0, v1, v2}, Lcom/facebook/common/errorreporting/FbErrorReporter;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private static a()Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a()Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    move-result-object v1

    sget-object v2, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->NOT_ATTEMPTED:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;)Z
    .locals 4

    const/4 v0, 0x1

    sget-object v1, Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;->TEST:Lcom/facebook/dalviktelemetry/DalvikTelemetry$ReportType;

    if-ne p0, v1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a()Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    move-result-object v1

    sget-object v2, Lcom/facebook/dalviktelemetry/DalvikTelemetry$1;->a:[I

    invoke-virtual {v1}, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown replace buffer result="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/facebook/dalviktelemetry/DalvikTelemetry;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method
