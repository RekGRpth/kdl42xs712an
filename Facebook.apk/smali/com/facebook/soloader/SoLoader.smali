.class public Lcom/facebook/soloader/SoLoader;
.super Ljava/lang/Object;
.source "SoLoader.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "LogUse"
    }
.end annotation


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Ljava/lang/String;

.field private static c:J

.field private static d:Z

.field private static e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    sput-object v0, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    sput-object v0, Lcom/facebook/soloader/SoLoader;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/soloader/SoLoader;->c:J

    sput-boolean v2, Lcom/facebook/soloader/SoLoader;->d:Z

    sput-boolean v2, Lcom/facebook/soloader/SoLoader;->e:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/StringBuilder;)Lcom/facebook/soloader/SoLoader$UnpackResult;
    .locals 10

    invoke-static {}, Lcom/facebook/soloader/SoLoader;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SoLoader failed to initialize successfully"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->FATAL:Lcom/facebook/soloader/SoLoader$UnpackResult;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    sget-wide v2, Lcom/facebook/soloader/SoLoader;->c:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->SUCCESS:Lcom/facebook/soloader/SoLoader$UnpackResult;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    const-wide/16 v1, -0x1

    :try_start_0
    new-instance v5, Ljava/util/jar/JarFile;

    sget-object v3, Lcom/facebook/soloader/SoLoader;->b:Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/util/jar/JarFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    invoke-virtual {v5, p0}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v3, "Main so file unavailable:"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/util/jar/JarFile;->getJarEntry(Ljava/lang/String;)Ljava/util/jar/JarEntry;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v0, "Could not load alternative so file:"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->FATAL:Lcom/facebook/soloader/SoLoader$UnpackResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    move-object v4, v3

    :try_start_3
    invoke-virtual {v4}, Ljava/util/jar/JarEntry;->getSize()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-wide v2

    :try_start_4
    invoke-static {v2, v3}, Lcom/facebook/soloader/SoLoader;->a(J)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v0, "not enough free space"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->FATAL:Lcom/facebook/soloader/SoLoader$UnpackResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_3
    :try_start_6
    invoke-virtual {v5, v4}, Ljava/util/jar/JarFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v4

    const/4 v1, 0x0

    :try_start_7
    const-string v6, "soloader"

    const-string v7, ".tmp"

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v8

    invoke-static {v6, v7, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v7, Ljava/io/BufferedOutputStream;

    invoke-direct {v7, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/16 v6, 0x4000

    :try_start_8
    new-array v6, v6, [B

    :cond_4
    const/4 v8, 0x0

    const/16 v9, 0x4000

    invoke-virtual {v4, v6, v8, v9}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-lez v8, :cond_5

    const/4 v9, 0x0

    invoke-virtual {v7, v6, v9, v8}, Ljava/io/BufferedOutputStream;->write([BII)V

    :cond_5
    const/4 v9, -0x1

    if-ne v8, v9, :cond_4

    invoke-virtual {v7}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-static {v7}, Lcom/facebook/soloader/SoLoader;->a(Ljava/io/Closeable;)V

    invoke-virtual {v1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    invoke-static {v4}, Lcom/facebook/soloader/SoLoader;->a(Ljava/io/Closeable;)V

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_6
    sget-wide v6, Lcom/facebook/soloader/SoLoader;->c:J

    invoke-virtual {p2, v6, v7}, Ljava/io/File;->setLastModified(J)Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-nez v1, :cond_8

    :cond_7
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_a

    :cond_8
    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->RETRY:Lcom/facebook/soloader/SoLoader$UnpackResult;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_c
    invoke-static {v7}, Lcom/facebook/soloader/SoLoader;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_d
    invoke-static {v4}, Lcom/facebook/soloader/SoLoader;->a(Ljava/io/Closeable;)V

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_9
    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :catchall_2
    move-exception v0

    move-wide v1, v2

    :goto_2
    :try_start_e
    invoke-virtual {v5}, Ljava/util/jar/JarFile;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    :goto_3
    :try_start_f
    throw v0
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    move-wide v2, v1

    goto :goto_1

    :catch_3
    move-exception v0

    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    move-wide v2, v1

    goto :goto_1

    :cond_a
    if-eqz v0, :cond_b

    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->SUCCESS:Lcom/facebook/soloader/SoLoader$UnpackResult;

    goto/16 :goto_0

    :cond_b
    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->FATAL:Lcom/facebook/soloader/SoLoader$UnpackResult;

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v3

    goto :goto_3

    :catch_6
    move-exception v0

    move-wide v1, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    sput-object p0, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    sget-boolean v0, Lcom/facebook/soloader/SoLoader;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/soloader/SoLoader$SoLoaderOrderError;

    invoke-direct {v0}, Lcom/facebook/soloader/SoLoader$SoLoaderOrderError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 1

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static declared-synchronized a(Ljava/lang/String;)V
    .locals 11

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    const-class v3, Lcom/facebook/soloader/SoLoader;

    monitor-enter v3

    :try_start_0
    sget-boolean v4, Lcom/facebook/soloader/SoLoader;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    monitor-exit v3

    return-void

    :cond_1
    const/4 v4, 0x1

    :try_start_1
    sput-boolean v4, Lcom/facebook/soloader/SoLoader;->d:Z

    invoke-static {p0}, Ljava/lang/System;->mapLibraryName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "lib/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "lib/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/facebook/soloader/SoLoader;->b()Ljava/io/File;

    move-result-object v7

    invoke-static {v4}, Lcom/facebook/soloader/SoLoader;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0xb

    if-ge v8, v9, :cond_8

    if-eqz v7, :cond_8

    invoke-static {}, Lcom/facebook/soloader/SoLoader;->a()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    sget-wide v9, Lcom/facebook/soloader/SoLoader;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    cmp-long v7, v7, v9

    if-gez v7, :cond_8

    :goto_1
    if-nez v2, :cond_2

    :try_start_2
    invoke-static {p0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v2, "SoLoader"

    const-string v7, "Failed to load library!"

    invoke-static {v2, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    sget-object v2, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    if-nez v2, :cond_3

    const-string v2, "Application context is not set - will not try alternative load"

    invoke-static {v2, v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    sget-object v0, Lcom/facebook/soloader/SoLoader$UnpackResult;->SUCCESS:Lcom/facebook/soloader/SoLoader$UnpackResult;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    const/4 v7, 0x3

    if-ge v1, v7, :cond_6

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-static {v5, v6, v4, v2}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/StringBuilder;)Lcom/facebook/soloader/SoLoader$UnpackResult;

    move-result-object v0

    sget-object v7, Lcom/facebook/soloader/SoLoader$UnpackResult;->FATAL:Lcom/facebook/soloader/SoLoader$UnpackResult;

    invoke-virtual {v0, v7}, Lcom/facebook/soloader/SoLoader$UnpackResult;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "failed to unpack from apk: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Error:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    sget-object v7, Lcom/facebook/soloader/SoLoader$UnpackResult;->SUCCESS:Lcom/facebook/soloader/SoLoader$UnpackResult;

    invoke-virtual {v0, v7}, Lcom/facebook/soloader/SoLoader$UnpackResult;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_6
    sget-object v1, Lcom/facebook/soloader/SoLoader$UnpackResult;->SUCCESS:Lcom/facebook/soloader/SoLoader$UnpackResult;

    invoke-virtual {v0, v1}, Lcom/facebook/soloader/SoLoader$UnpackResult;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to unpack from apk (multiple attempts): "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_7
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    :try_start_4
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/Runtime;->load(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "failed to load from "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_8
    move v2, v1

    goto/16 :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsatisfiedLinkError;

    invoke-direct {v0, p0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/UnsatisfiedLinkError;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    throw v0
.end method

.method private static a()Z
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/facebook/soloader/SoLoader;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    sget-object v1, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageResourcePath()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/facebook/soloader/SoLoader;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/facebook/soloader/SoLoader;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/soloader/SoLoader;->c:J

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(J)Z
    .locals 6

    const/4 v0, 0x0

    new-instance v1, Landroid/os/StatFs;

    sget-object v2, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    const-string v3, "libs"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v4, v1

    mul-long v1, v2, v4

    cmp-long v1, p0, v1

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static b()Ljava/io/File;
    .locals 3

    sget-object v0, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/lib"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    sget-object v0, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/soloader/SoLoader;->a:Landroid/content/Context;

    const-string v1, "libs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method
