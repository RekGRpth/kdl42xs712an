.class public Lcom/facebook/nobreak/RecoveryModeHelper;
.super Ljava/lang/Object;
.source "RecoveryModeHelper.java"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/facebook/nobreak/RecoveryModeHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/nobreak/RecoveryModeHelper;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->b:Z

    iput-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->c:Z

    return-void
.end method

.method public static a()V
    .locals 1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method static synthetic c(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/facebook/nobreak/RecoveryModeHelper;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string v0, "recovery"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "recovery_file"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 2

    sget-object v0, Lcom/facebook/nobreak/RecoveryModeHelper;->a:Ljava/lang/String;

    const-string v1, "Perform recovery, wiping data"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/facebook/nobreak/RecoveryModeHelper$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/nobreak/RecoveryModeHelper$1;-><init>(Lcom/facebook/nobreak/RecoveryModeHelper;Landroid/content/Context;Landroid/os/Handler;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/facebook/nobreak/RecoveryModeHelper$1;->start()V

    return-void
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 6

    const/4 v5, 0x1

    iget-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->c:Z

    :goto_0
    return v0

    :cond_0
    iput-boolean v5, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->b:Z

    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/facebook/nobreak/RecoveryModeHelper;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    iput-boolean v5, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->c:Z

    :goto_1
    iget-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->c:Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;)V
    .locals 2

    sget-object v0, Lcom/facebook/nobreak/RecoveryModeHelper;->a:Ljava/lang/String;

    const-string v1, "Ending RecoveryMode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    invoke-static {p1}, Lcom/facebook/nobreak/RecoveryModeHelper;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/nobreak/RecoveryModeHelper$MainProcessReceiver;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/nobreak/RecoveryModeHelper;->c:Z

    return-void
.end method
