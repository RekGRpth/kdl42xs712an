.class public abstract Lcom/facebook/base/app/DelegatingApplication;
.super Landroid/app/Application;
.source "DelegatingApplication.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/base/app/ApplicationLike;",
        ">",
        "Landroid/app/Application;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/base/app/ApplicationLike;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private declared-synchronized e()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/base/app/DelegatingApplication;->a()Lcom/facebook/base/app/ApplicationLike;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()V
    .locals 5

    :try_start_0
    const-string v0, "com.facebook.watson.WatsonLogger"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "enablePersistentLogging"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "watson"

    const-string v1, "enablePersistentLogging called."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()Lcom/facebook/base/app/ApplicationLike;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/facebook/base/app/DelegatingApplication;->b()V

    invoke-direct {p0}, Lcom/facebook/base/app/DelegatingApplication;->e()V

    invoke-direct {p0}, Lcom/facebook/base/app/DelegatingApplication;->f()V

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method protected final c()Lcom/facebook/base/app/ApplicationLike;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/facebook/base/app/DelegatingApplication;->e()V

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    return-object v0
.end method

.method public final d()Landroid/content/res/Resources;
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 3

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    instance-of v0, v0, Lcom/facebook/resources/HasOverridingResources;

    if-eqz v0, :cond_1

    instance-of v0, p0, Lcom/facebook/resources/HasBaseResourcesAccess;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " illegally implements HasOverridingResources without HasBaseResourcesAccess."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    check-cast v0, Lcom/facebook/resources/HasOverridingResources;

    invoke-interface {v0}, Lcom/facebook/resources/HasOverridingResources;->g()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-direct {p0}, Lcom/facebook/base/app/DelegatingApplication;->e()V

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    invoke-interface {v0}, Lcom/facebook/base/app/ApplicationLike;->c()V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    invoke-interface {v0}, Lcom/facebook/base/app/ApplicationLike;->e()V

    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/base/app/DelegatingApplication;->a:Lcom/facebook/base/app/ApplicationLike;

    invoke-interface {v0, p1}, Lcom/facebook/base/app/ApplicationLike;->a(I)V

    :cond_0
    return-void
.end method
