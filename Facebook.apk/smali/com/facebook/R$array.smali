.class public Lcom/facebook/R$array;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final default_scrollbar_insets:I = 0x7f0a0002

.field public static final event_dashboard_time_am_pm_symbols:I = 0x7f0a003d

.field public static final link_preview_actions:I = 0x7f0a0003

.field public static final logger_levels:I = 0x7f0a0000

.field public static final logger_levels_values:I = 0x7f0a0001

.field public static final mqtt_server_tiers:I = 0x7f0a0006

.field public static final mqtt_server_tiers_values:I = 0x7f0a0007

.field public static final page_identity_structured_content_composers:I = 0x7f0a0036

.field public static final page_identity_structured_content_creators:I = 0x7f0a0037

.field public static final page_identity_structured_content_creators_writers:I = 0x7f0a003c

.field public static final page_identity_structured_content_directors:I = 0x7f0a0038

.field public static final page_identity_structured_content_directors_writers:I = 0x7f0a003b

.field public static final page_identity_structured_content_genres:I = 0x7f0a003a

.field public static final page_identity_structured_content_writers:I = 0x7f0a0039

.field public static final review_composer_review_length_message:I = 0x7f0a0035

.field public static final review_composer_star_label:I = 0x7f0a0034

.field public static final upload_contacts_batch_size_values:I = 0x7f0a0009

.field public static final upload_contacts_batch_sizes:I = 0x7f0a0008

.field public static final voip_adaptive_fec_turn_on_bwe_threshold_names:I = 0x7f0a0018

.field public static final voip_adaptive_fec_turn_on_bwe_threshold_values:I = 0x7f0a0019

.field public static final voip_audio_modes:I = 0x7f0a000a

.field public static final voip_audio_modes_values:I = 0x7f0a000b

.field public static final voip_codec_bitrate_override_empty:I = 0x7f0a000e

.field public static final voip_codec_bitrate_override_empty_values:I = 0x7f0a000f

.field public static final voip_codec_bitrate_override_isac:I = 0x7f0a0010

.field public static final voip_codec_bitrate_override_isac_values:I = 0x7f0a0011

.field public static final voip_codec_bitrate_override_opus:I = 0x7f0a0014

.field public static final voip_codec_bitrate_override_opus_values:I = 0x7f0a0015

.field public static final voip_codec_bitrate_override_speex:I = 0x7f0a0012

.field public static final voip_codec_bitrate_override_speex_values:I = 0x7f0a0013

.field public static final voip_codec_override_modes:I = 0x7f0a000c

.field public static final voip_codec_override_modes_values:I = 0x7f0a000d

.field public static final voip_excessive_jitter_names:I = 0x7f0a001e

.field public static final voip_excessive_jitter_values:I = 0x7f0a001f

.field public static final voip_fec_names:I = 0x7f0a0016

.field public static final voip_fec_values:I = 0x7f0a0017

.field public static final voip_logging_modes:I = 0x7f0a0032

.field public static final voip_logging_modes_values:I = 0x7f0a0033

.field public static final voip_no_voice_communication_types:I = 0x7f0a0030

.field public static final voip_no_voice_communication_values:I = 0x7f0a0031

.field public static final voip_opus_bandwidth_names:I = 0x7f0a001a

.field public static final voip_opus_bandwidth_values:I = 0x7f0a001b

.field public static final voip_opus_complexity_names:I = 0x7f0a001c

.field public static final voip_opus_complexity_values:I = 0x7f0a001d

.field public static final voip_stream_types:I = 0x7f0a002c

.field public static final voip_stream_types_values:I = 0x7f0a002d

.field public static final voip_use_jni_audio_types:I = 0x7f0a002e

.field public static final voip_use_jni_audio_values:I = 0x7f0a002f

.field public static final voip_webrtc_aecm_modes:I = 0x7f0a0024

.field public static final voip_webrtc_aecm_modes_values:I = 0x7f0a0025

.field public static final voip_webrtc_agc_modes:I = 0x7f0a0026

.field public static final voip_webrtc_agc_modes_values:I = 0x7f0a0027

.field public static final voip_webrtc_ec_mode_values:I = 0x7f0a0023

.field public static final voip_webrtc_ec_modes:I = 0x7f0a0022

.field public static final voip_webrtc_lafns_modes:I = 0x7f0a002a

.field public static final voip_webrtc_lafns_modes_values:I = 0x7f0a002b

.field public static final voip_webrtc_ns_modes:I = 0x7f0a0028

.field public static final voip_webrtc_ns_modes_values:I = 0x7f0a0029

.field public static final voip_webrtc_option_controls:I = 0x7f0a0020

.field public static final voip_webrtc_option_controls_values:I = 0x7f0a0021

.field public static final web_server_tiers:I = 0x7f0a0004

.field public static final web_server_tiers_values:I = 0x7f0a0005


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
