.class abstract Lorg/tukaani/xz/index/IndexBase;
.super Ljava/lang/Object;


# instance fields
.field a:J

.field b:J

.field c:J

.field d:J

.field private final e:Lorg/tukaani/xz/XZIOException;


# direct methods
.method constructor <init>(Lorg/tukaani/xz/XZIOException;)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->a:J

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->b:J

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->c:J

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->d:J

    iput-object p1, p0, Lorg/tukaani/xz/index/IndexBase;->e:Lorg/tukaani/xz/XZIOException;

    return-void
.end method

.method private c()J
    .locals 4

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->d:J

    invoke-static {v0, v1}, Lorg/tukaani/xz/common/Util;->a(J)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-long v0, v0

    iget-wide v2, p0, Lorg/tukaani/xz/index/IndexBase;->c:J

    add-long/2addr v0, v2

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private d()J
    .locals 6

    const-wide/16 v4, 0xc

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->a:J

    add-long/2addr v0, v4

    invoke-virtual {p0}, Lorg/tukaani/xz/index/IndexBase;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    add-long/2addr v0, v4

    return-wide v0
.end method


# virtual methods
.method public final a()J
    .locals 4

    invoke-direct {p0}, Lorg/tukaani/xz/index/IndexBase;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x3

    add-long/2addr v0, v2

    const-wide/16 v2, -0x4

    and-long/2addr v0, v2

    return-wide v0
.end method

.method a(JJ)V
    .locals 8

    const-wide/16 v6, 0x0

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->a:J

    const-wide/16 v2, 0x3

    add-long/2addr v2, p1

    const-wide/16 v4, -0x4

    and-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->a:J

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->b:J

    add-long/2addr v0, p3

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->b:J

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->c:J

    invoke-static {p1, p2}, Lorg/tukaani/xz/common/Util;->a(J)I

    move-result v2

    invoke-static {p3, p4}, Lorg/tukaani/xz/common/Util;->a(J)I

    move-result v3

    add-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->c:J

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->d:J

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->a:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexBase;->b:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lorg/tukaani/xz/index/IndexBase;->a()J

    move-result-wide v0

    const-wide v2, 0x400000000L

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-direct {p0}, Lorg/tukaani/xz/index/IndexBase;->d()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-gez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/index/IndexBase;->e:Lorg/tukaani/xz/XZIOException;

    throw v0

    :cond_1
    return-void
.end method

.method final b()I
    .locals 4

    const-wide/16 v0, 0x4

    invoke-direct {p0}, Lorg/tukaani/xz/index/IndexBase;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3

    and-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method
