.class public final Lorg/tukaani/xz/lzma/LZMADecoder;
.super Lorg/tukaani/xz/lzma/LZMACoder;


# instance fields
.field private final m:Lorg/tukaani/xz/lz/LZDecoder;

.field private final n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

.field private final o:Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

.field private final p:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

.field private final q:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;


# direct methods
.method public constructor <init>(Lorg/tukaani/xz/lz/LZDecoder;Lorg/tukaani/xz/rangecoder/RangeDecoder;III)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p5}, Lorg/tukaani/xz/lzma/LZMACoder;-><init>(I)V

    new-instance v0, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-direct {v0, p0, v1}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;-><init>(Lorg/tukaani/xz/lzma/LZMADecoder;B)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->p:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    new-instance v0, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-direct {v0, p0, v1}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;-><init>(Lorg/tukaani/xz/lzma/LZMADecoder;B)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->q:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    iput-object p1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    iput-object p2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    new-instance v0, Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

    invoke-direct {v0, p0, p3, p4}, Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;-><init>(Lorg/tukaani/xz/lzma/LZMADecoder;II)V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->o:Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMADecoder;->a()V

    return-void
.end method

.method static a(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/lz/LZDecoder;
    .locals 1

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    return-object v0
.end method

.method private b(I)I
    .locals 7

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/State;->d()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    const/4 v1, 0x3

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v2, v2, v4

    aput v2, v0, v1

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v1, v1, v3

    aput v1, v0, v4

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v1, v1, v6

    aput v1, v0, v3

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->p:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-virtual {v0, p1}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;->a(I)I

    move-result v0

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->j:[[S

    invoke-static {v0}, Lorg/tukaani/xz/lzma/LZMADecoder;->a(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->b([S)I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aput v1, v2, v6

    :goto_0
    return v0

    :cond_0
    shr-int/lit8 v2, v1, 0x1

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    and-int/lit8 v4, v1, 0x1

    or-int/lit8 v4, v4, 0x2

    shl-int/2addr v4, v2

    aput v4, v3, v6

    const/16 v3, 0xe

    if-ge v1, v3, :cond_1

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v3, v2, v6

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v5, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->k:[[S

    add-int/lit8 v1, v1, -0x4

    aget-object v1, v5, v1

    invoke-virtual {v4, v1}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->c([S)I

    move-result v1

    or-int/2addr v1, v3

    aput v1, v2, v6

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v3, v1, v6

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    add-int/lit8 v2, v2, -0x4

    invoke-virtual {v4, v2}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x4

    or-int/2addr v2, v3

    aput v2, v1, v6

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v2, v1, v6

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v4, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->l:[S

    invoke-virtual {v3, v4}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->c([S)I

    move-result v3

    or-int/2addr v2, v3

    aput v2, v1, v6

    goto :goto_0
.end method

.method static b(Lorg/tukaani/xz/lzma/LZMADecoder;)Lorg/tukaani/xz/rangecoder/RangeDecoder;
    .locals 1

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    return-object v0
.end method

.method private c(I)I
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v1, 0x1

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->f:[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->i:[[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2, p1}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/State;->f()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->g:[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v0, v0, v1

    :goto_1
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v3, v3, v5

    aput v3, v2, v1

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aput v0, v1, v5

    :cond_1
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/State;->e()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->q:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-virtual {v0, p1}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;->a(I)I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->h:[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v0, v0, v4

    :goto_2
    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v3, v3, v1

    aput v3, v2, v4

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v0, v0, v6

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    aget v3, v3, v4

    aput v3, v2, v6

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-super {p0}, Lorg/tukaani/xz/lzma/LZMACoder;->a()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->o:Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;->a()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->p:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;->a()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->q:Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMADecoder$LengthDecoder;->a()V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lz/LZDecoder;->e()V

    :goto_0
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lz/LZDecoder;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lz/LZDecoder;->d()I

    move-result v0

    iget v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->a:I

    and-int/2addr v0, v1

    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->d:[[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2, v0}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->o:Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMADecoder$LiteralDecoder;->b()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->e:[S

    iget-object v3, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->c:Lorg/tukaani/xz/lzma/State;

    invoke-virtual {v3}, Lorg/tukaani/xz/lzma/State;->b()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a([SI)I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lorg/tukaani/xz/lzma/LZMADecoder;->b(I)I

    move-result v0

    :goto_1
    iget-object v1, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->m:Lorg/tukaani/xz/lz/LZDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->b:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2, v0}, Lorg/tukaani/xz/lz/LZDecoder;->a(II)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lorg/tukaani/xz/lzma/LZMADecoder;->c(I)I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->c()V

    iget-object v0, p0, Lorg/tukaani/xz/lzma/LZMADecoder;->n:Lorg/tukaani/xz/rangecoder/RangeDecoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/rangecoder/RangeDecoder;->a()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_3
    return-void
.end method
