.class abstract Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lorg/tukaani/xz/lzma/LZMACoder;


# direct methods
.method constructor <init>(Lorg/tukaani/xz/lzma/LZMACoder;II)V
    .locals 1

    iput-object p1, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->c:Lorg/tukaani/xz/lzma/LZMACoder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->a:I

    const/4 v0, 0x1

    shl-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->b:I

    return-void
.end method


# virtual methods
.method final a(II)I
    .locals 3

    iget v0, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->a:I

    rsub-int/lit8 v0, v0, 0x8

    shr-int v0, p1, v0

    iget v1, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->b:I

    and-int/2addr v1, p2

    iget v2, p0, Lorg/tukaani/xz/lzma/LZMACoder$LiteralCoder;->a:I

    shl-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method
