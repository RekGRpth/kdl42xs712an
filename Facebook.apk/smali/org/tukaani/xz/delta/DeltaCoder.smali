.class abstract Lorg/tukaani/xz/delta/DeltaCoder;
.super Ljava/lang/Object;


# instance fields
.field final a:I

.field final b:[B

.field c:I


# direct methods
.method constructor <init>(I)V
    .locals 2

    const/16 v1, 0x100

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [B

    iput-object v0, p0, Lorg/tukaani/xz/delta/DeltaCoder;->b:[B

    const/4 v0, 0x0

    iput v0, p0, Lorg/tukaani/xz/delta/DeltaCoder;->c:I

    if-lez p1, :cond_0

    if-le p1, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iput p1, p0, Lorg/tukaani/xz/delta/DeltaCoder;->a:I

    return-void
.end method
