.class public Lcom/android/email/activity/MessageListItemCoordinates;
.super Ljava/lang/Object;
.source "MessageListItemCoordinates.java"


# static fields
.field private static MINIMUM_WIDTH_WIDE_MODE:I

.field private static MSG_USE_WIDE_MODE:I

.field private static mCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/email/activity/MessageListItemCoordinates;",
            ">;"
        }
    .end annotation
.end field

.field private static sPaint:Landroid/text/TextPaint;


# instance fields
.field checkmarkWidthIncludingMargins:I

.field checkmarkX:I

.field checkmarkY:I

.field chipHeight:I

.field chipWidth:I

.field chipX:I

.field chipY:I

.field dateAscent:I

.field dateFontSize:I

.field dateXEnd:I

.field dateY:I

.field paperclipY:I

.field sendersAscent:I

.field sendersFontSize:I

.field sendersLineCount:I

.field sendersWidth:I

.field sendersX:I

.field sendersY:I

.field starX:I

.field starY:I

.field stateX:I

.field stateY:I

.field subjectAscent:I

.field subjectFontSize:I

.field subjectLineCount:I

.field subjectWidth:I

.field subjectX:I

.field subjectY:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, -0x1

    sput v0, Lcom/android/email/activity/MessageListItemCoordinates;->MINIMUM_WIDTH_WIDE_MODE:I

    sput v0, Lcom/android/email/activity/MessageListItemCoordinates;->MSG_USE_WIDE_MODE:I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/email/activity/MessageListItemCoordinates;->mCache:Landroid/util/SparseArray;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/email/activity/MessageListItemCoordinates;->sPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/android/email/activity/MessageListItemCoordinates;->sPaint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v0, Lcom/android/email/activity/MessageListItemCoordinates;->sPaint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forWidth(Landroid/content/Context;IZ)Lcom/android/email/activity/MessageListItemCoordinates;
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z

    sget-object v17, Lcom/android/email/activity/MessageListItemCoordinates;->mCache:Landroid/util/SparseArray;

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/email/activity/MessageListItemCoordinates;

    if-nez v5, :cond_0

    new-instance v5, Lcom/android/email/activity/MessageListItemCoordinates;

    invoke-direct {v5}, Lcom/android/email/activity/MessageListItemCoordinates;-><init>()V

    sget-object v17, Lcom/android/email/activity/MessageListItemCoordinates;->mCache:Landroid/util/SparseArray;

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-static/range {p0 .. p2}, Lcom/android/email/activity/MessageListItemCoordinates;->getMode(Landroid/content/Context;IZ)I

    move-result v9

    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/android/email/activity/MessageListItemCoordinates;->getHeight(Landroid/content/Context;I)I

    move-result v7

    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v17

    invoke-static {v9}, Lcom/android/email/activity/MessageListItemCoordinates;->getLayoutId(I)I

    move-result v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    const/high16 v17, 0x40000000    # 2.0f

    move/from16 v0, v17

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    move/from16 v0, v16

    invoke-virtual {v15, v0, v8}, Landroid/view/View;->measure(II)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v15, v0, v1, v2, v7}, Landroid/view/View;->layout(IIII)V

    const v17, 0x7f0f0085    # com.android.email.R.id.checkmark

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkX:I

    invoke-static {v3}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkY:I

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/email/activity/MessageListItemCoordinates;->getWidth(Landroid/view/View;Z)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkWidthIncludingMargins:I

    const v17, 0x7f0f0086    # com.android.email.R.id.star

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    invoke-static {v12}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->starX:I

    invoke-static {v12}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->starY:I

    const v17, 0x7f0f0084    # com.android.email.R.id.reply_state

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-static {v13}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->stateX:I

    invoke-static {v13}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->stateY:I

    const v17, 0x7f0f008a    # com.android.email.R.id.senders

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-static {v11}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersX:I

    invoke-static {v11}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersY:I

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v11, v0}, Lcom/android/email/activity/MessageListItemCoordinates;->getWidth(Landroid/view/View;Z)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersWidth:I

    invoke-static {v11}, Lcom/android/email/activity/MessageListItemCoordinates;->getLineCount(Landroid/widget/TextView;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersLineCount:I

    invoke-virtual {v11}, Landroid/widget/TextView;->getTextSize()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersFontSize:I

    invoke-virtual {v11}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/text/TextPaint;->ascent()F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->sendersAscent:I

    const v17, 0x7f0f0063    # com.android.email.R.id.subject

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    invoke-static {v14}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectX:I

    invoke-static {v14}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectY:I

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v14, v0}, Lcom/android/email/activity/MessageListItemCoordinates;->getWidth(Landroid/view/View;Z)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectWidth:I

    invoke-static {v14}, Lcom/android/email/activity/MessageListItemCoordinates;->getLineCount(Landroid/widget/TextView;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectLineCount:I

    invoke-virtual {v14}, Landroid/widget/TextView;->getTextSize()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectFontSize:I

    invoke-virtual {v14}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/text/TextPaint;->ascent()F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->subjectAscent:I

    const v17, 0x7f0f004b    # com.android.email.R.id.color_chip

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->chipX:I

    invoke-static {v4}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->chipY:I

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v4, v0}, Lcom/android/email/activity/MessageListItemCoordinates;->getWidth(Landroid/view/View;Z)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->chipWidth:I

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v4, v0}, Lcom/android/email/activity/MessageListItemCoordinates;->getHeight(Landroid/view/View;Z)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->chipHeight:I

    const v17, 0x7f0f0089    # com.android.email.R.id.date

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-static {v6}, Lcom/android/email/activity/MessageListItemCoordinates;->getX(Landroid/view/View;)I

    move-result v17

    invoke-virtual {v6}, Landroid/widget/TextView;->getWidth()I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->dateXEnd:I

    invoke-static {v6}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->dateY:I

    invoke-virtual {v6}, Landroid/widget/TextView;->getTextSize()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->dateFontSize:I

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/text/TextPaint;->ascent()F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->round(F)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->dateAscent:I

    const v17, 0x7f0f0088    # com.android.email.R.id.paperclip

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-static {v10}, Lcom/android/email/activity/MessageListItemCoordinates;->getY(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v5, Lcom/android/email/activity/MessageListItemCoordinates;->paperclipY:I

    :cond_0
    return-object v5
.end method

.method public static getHeight(Landroid/content/Context;I)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-nez p1, :cond_0

    const v0, 0x7f0a0007    # com.android.email.R.dimen.message_list_item_height_wide

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0

    :cond_0
    const v0, 0x7f0a0008    # com.android.email.R.dimen.message_list_item_height_normal

    goto :goto_0
.end method

.method public static getHeight(Landroid/view/View;Z)I
    .locals 4
    .param p0    # Landroid/view/View;
    .param p1    # Z

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    if-eqz p1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v3

    :goto_0
    add-int/2addr v1, v2

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getLayoutId(I)I
    .locals 3
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown conversation header view mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f040034    # com.android.email.R.layout.message_list_item_wide

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f040033    # com.android.email.R.layout.message_list_item_normal

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getLineCount(Landroid/widget/TextView;)I
    .locals 2
    .param p0    # Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method public static getMode(Landroid/content/Context;IZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_1

    const v2, 0x7f0b0001    # com.android.email.R.integer.message_search_list_header_mode

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v2, Lcom/android/email/activity/MessageListItemCoordinates;->MINIMUM_WIDTH_WIDE_MODE:I

    if-gtz v2, :cond_2

    const v2, 0x7f0a0023    # com.android.email.R.dimen.minimum_width_wide_mode

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/android/email/activity/MessageListItemCoordinates;->MINIMUM_WIDTH_WIDE_MODE:I

    :cond_2
    sget v2, Lcom/android/email/activity/MessageListItemCoordinates;->MSG_USE_WIDE_MODE:I

    if-gez v2, :cond_3

    const/high16 v2, 0x7f0b0000    # com.android.email.R.integer.message_use_wide_header_mode

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    sput v2, Lcom/android/email/activity/MessageListItemCoordinates;->MSG_USE_WIDE_MODE:I

    :cond_3
    const/4 v0, 0x1

    sget v2, Lcom/android/email/activity/MessageListItemCoordinates;->MSG_USE_WIDE_MODE:I

    if-eqz v2, :cond_0

    sget v2, Lcom/android/email/activity/MessageListItemCoordinates;->MINIMUM_WIDTH_WIDE_MODE:I

    if-le p1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getWidth(Landroid/view/View;Z)I
    .locals 4
    .param p0    # Landroid/view/View;
    .param p1    # Z

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    if-eqz p1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    :goto_0
    add-int/2addr v1, v2

    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getX(Landroid/view/View;)I
    .locals 3
    .param p0    # Landroid/view/View;

    const/4 v1, 0x0

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    :goto_1
    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method

.method private static getY(Landroid/view/View;)I
    .locals 3
    .param p0    # Landroid/view/View;

    const/4 v1, 0x0

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getY()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    :goto_1
    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    return v1
.end method

.method public static isMultiPane(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/android/email/activity/UiUtilities;->useTwoPane(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static resetCaches()V
    .locals 1

    sget-object v0, Lcom/android/email/activity/MessageListItemCoordinates;->mCache:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method
