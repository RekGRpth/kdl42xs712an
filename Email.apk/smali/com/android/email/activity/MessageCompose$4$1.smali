.class Lcom/android/email/activity/MessageCompose$4$1;
.super Ljava/lang/Object;
.source "MessageCompose.java"

# interfaces
.implements Lcom/android/email/activity/MessageCompose$AttachmentLoadedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/email/activity/MessageCompose$4;->onMessageLoaded(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/email/activity/MessageCompose$4;


# direct methods
.method constructor <init>(Lcom/android/email/activity/MessageCompose$4;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/MessageCompose$4$1;->this$1:Lcom/android/email/activity/MessageCompose$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachmentLoaded([Lcom/android/emailcommon/provider/EmailContent$Attachment;)V
    .locals 5
    .param p1    # [Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    iget-object v4, p0, Lcom/android/email/activity/MessageCompose$4$1;->this$1:Lcom/android/email/activity/MessageCompose$4;

    iget-object v4, v4, Lcom/android/email/activity/MessageCompose$4;->this$0:Lcom/android/email/activity/MessageCompose;

    # invokes: Lcom/android/email/activity/MessageCompose;->addAttachment(Lcom/android/emailcommon/provider/EmailContent$Attachment;)V
    invoke-static {v4, v1}, Lcom/android/email/activity/MessageCompose;->access$400(Lcom/android/email/activity/MessageCompose;Lcom/android/emailcommon/provider/EmailContent$Attachment;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
