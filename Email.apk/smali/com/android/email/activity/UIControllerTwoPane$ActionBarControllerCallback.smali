.class Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;
.super Ljava/lang/Object;
.source "UIControllerTwoPane.java"

# interfaces
.implements Lcom/android/email/activity/ActionBarController$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/UIControllerTwoPane;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActionBarControllerCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/UIControllerTwoPane;


# direct methods
.method private constructor <init>(Lcom/android/email/activity/UIControllerTwoPane;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/email/activity/UIControllerTwoPane;Lcom/android/email/activity/UIControllerTwoPane$1;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/UIControllerTwoPane;
    .param p2    # Lcom/android/email/activity/UIControllerTwoPane$1;

    invoke-direct {p0, p1}, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;-><init>(Lcom/android/email/activity/UIControllerTwoPane;)V

    return-void
.end method


# virtual methods
.method public getMailboxId()J
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->getMessageListMailboxId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMessageSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->isMessageViewInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->getMessageViewFragment()Lcom/android/email/activity/MessageViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageViewFragment;->isMessageOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->getMessageViewFragment()Lcom/android/email/activity/MessageViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageViewFragment;->getMessage()Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSearchHint()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->getSearchHint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleMode()I
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v0, v0, Lcom/android/email/activity/UIControllerTwoPane;->mThreePane:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v0}, Lcom/android/email/activity/ThreePaneLayout;->isLeftPaneVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v0, v0, Lcom/android/email/activity/UIControllerTwoPane;->mThreePane:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v0}, Lcom/android/email/activity/ThreePaneLayout;->isRightPaneVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v0, v0, Lcom/android/email/activity/UIControllerTwoPane;->mThreePane:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v0}, Lcom/android/email/activity/ThreePaneLayout;->isMiddlePaneVisible()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/16 v0, 0x12

    goto :goto_0
.end method

.method public getUIAccountId()J
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->getUIAccountId()J

    move-result-wide v0

    return-wide v0
.end method

.method public onAccountSelected(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/android/email/activity/UIControllerTwoPane;->switchAccount(JZ)V

    return-void
.end method

.method public onMailboxSelected(JJ)V
    .locals 1
    .param p1    # J
    .param p3    # J

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/email/activity/UIControllerTwoPane;->openMailbox(JJ)V

    return-void
.end method

.method public onNoAccountsFound()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v0, v0, Lcom/android/email/activity/UIControllerTwoPane;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v0}, Lcom/android/email/activity/Welcome;->actionStart(Landroid/app/Activity;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v0, v0, Lcom/android/email/activity/UIControllerTwoPane;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0}, Lcom/android/email/activity/EmailActivity;->finish()V

    return-void
.end method

.method public onSearchExit()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->onSearchExit()V

    return-void
.end method

.method public onSearchStarted()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0}, Lcom/android/email/activity/UIControllerTwoPane;->onSearchStarted()V

    return-void
.end method

.method public onSearchSubmit(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/UIControllerTwoPane;->onSearchSubmit(Ljava/lang/String;)V

    return-void
.end method

.method public shouldShowUp()Z
    .locals 5

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    iget-object v4, v4, Lcom/android/email/activity/UIControllerTwoPane;->mThreePane:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v4}, Lcom/android/email/activity/ThreePaneLayout;->getVisiblePanes()I

    move-result v1

    and-int/lit8 v4, v1, 0x4

    if-nez v4, :cond_2

    move v0, v3

    :goto_0
    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v4}, Lcom/android/email/activity/UIControllerTwoPane;->isMailboxListInstalled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/email/activity/UIControllerTwoPane$ActionBarControllerCallback;->this$0:Lcom/android/email/activity/UIControllerTwoPane;

    invoke-virtual {v4}, Lcom/android/email/activity/UIControllerTwoPane;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/email/activity/MailboxListFragment;->canNavigateUp()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v2, v3

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0
.end method
