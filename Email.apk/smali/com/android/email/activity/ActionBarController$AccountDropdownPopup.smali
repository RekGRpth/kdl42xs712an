.class Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;
.super Landroid/widget/ListPopupWindow;
.source "ActionBarController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/ActionBarController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccountDropdownPopup"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/email/activity/ActionBarController;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/ActionBarController;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->this$0:Lcom/android/email/activity/ActionBarController;

    invoke-direct {p0, p2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    # getter for: Lcom/android/email/activity/ActionBarController;->mAccountSpinner:Landroid/view/View;
    invoke-static {p1}, Lcom/android/email/activity/ActionBarController;->access$700(Lcom/android/email/activity/ActionBarController;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setAnchorView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setModal(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setPromptPosition(I)V

    new-instance v0, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup$1;

    invoke-direct {v0, p0, p1}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup$1;-><init>(Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;Lcom/android/email/activity/ActionBarController;)V

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->this$0:Lcom/android/email/activity/ActionBarController;

    # getter for: Lcom/android/email/activity/ActionBarController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/email/activity/ActionBarController;->access$300(Lcom/android/email/activity/ActionBarController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0046    # com.android.email.R.dimen.account_dropdown_dropdownwidth

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setWidth(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->setInputMethodMode(I)V

    invoke-super {p0}, Landroid/widget/ListPopupWindow;->show()V

    invoke-virtual {p0}, Lcom/android/email/activity/ActionBarController$AccountDropdownPopup;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    return-void
.end method
