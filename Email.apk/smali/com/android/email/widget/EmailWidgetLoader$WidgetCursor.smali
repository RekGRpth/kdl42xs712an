.class Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;
.super Landroid/database/CursorWrapper;
.source "EmailWidgetLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/widget/EmailWidgetLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "WidgetCursor"
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mMailboxName:Ljava/lang/String;

.field private final mMessageCount:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    iput p2, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mMessageCount:I

    iput-object p3, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mAccountName:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mMailboxName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getMailboxName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mMailboxName:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageCount()I
    .locals 1

    iget v0, p0, Lcom/android/email/widget/EmailWidgetLoader$WidgetCursor;->mMessageCount:I

    return v0
.end method
