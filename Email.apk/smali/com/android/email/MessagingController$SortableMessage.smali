.class Lcom/android/email/MessagingController$SortableMessage;
.super Ljava/lang/Object;
.source "MessagingController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/MessagingController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SortableMessage"
.end annotation


# instance fields
.field private final mMessage:Lcom/android/emailcommon/mail/Message;

.field private final mUid:J


# direct methods
.method constructor <init>(Lcom/android/emailcommon/mail/Message;J)V
    .locals 0
    .param p1    # Lcom/android/emailcommon/mail/Message;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/email/MessagingController$SortableMessage;->mMessage:Lcom/android/emailcommon/mail/Message;

    iput-wide p2, p0, Lcom/android/email/MessagingController$SortableMessage;->mUid:J

    return-void
.end method

.method static synthetic access$600(Lcom/android/email/MessagingController$SortableMessage;)J
    .locals 2
    .param p0    # Lcom/android/email/MessagingController$SortableMessage;

    iget-wide v0, p0, Lcom/android/email/MessagingController$SortableMessage;->mUid:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/android/email/MessagingController$SortableMessage;)Lcom/android/emailcommon/mail/Message;
    .locals 1
    .param p0    # Lcom/android/email/MessagingController$SortableMessage;

    iget-object v0, p0, Lcom/android/email/MessagingController$SortableMessage;->mMessage:Lcom/android/emailcommon/mail/Message;

    return-object v0
.end method
