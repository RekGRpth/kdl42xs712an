.class public final Lcom/android/ex/chips/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ex/chips/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final RecipientEditTextView:[I

.field public static final SizeBoundingFrameLayout_attributes:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/ex/chips/R$styleable;->RecipientEditTextView:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/ex/chips/R$styleable;->SizeBoundingFrameLayout_attributes:[I

    return-void

    :array_0
    .array-data 4
        0x7f010002    # com.android.email.R.attr.invalidChipBackground
        0x7f010003    # com.android.email.R.attr.chipBackground
        0x7f010004    # com.android.email.R.attr.chipBackgroundPressed
        0x7f010005    # com.android.email.R.attr.chipDelete
        0x7f010006    # com.android.email.R.attr.chipAlternatesLayout
        0x7f010007    # com.android.email.R.attr.chipPadding
        0x7f010008    # com.android.email.R.attr.chipHeight
        0x7f010009    # com.android.email.R.attr.chipFontSize
    .end array-data

    :array_1
    .array-data 4
        0x7f010000    # com.android.email.R.attr.maxWidth
        0x7f010001    # com.android.email.R.attr.maxHeight
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
