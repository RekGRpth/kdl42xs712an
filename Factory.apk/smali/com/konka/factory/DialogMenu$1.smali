.class Lcom/konka/factory/DialogMenu$1;
.super Ljava/lang/Object;
.source "DialogMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/DialogMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/DialogMenu;


# direct methods
.method constructor <init>(Lcom/konka/factory/DialogMenu;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    # getter for: Lcom/konka/factory/DialogMenu;->bUSBUpgradeFlag:Z
    invoke-static {v0}, Lcom/konka/factory/DialogMenu;->access$000(Lcom/konka/factory/DialogMenu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget v1, v1, Lcom/konka/factory/DialogMenu;->parentItemId:I

    invoke-virtual {v0, v1}, Lcom/konka/factory/DialogMenu;->DialogMenuConfirm(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget v1, v1, Lcom/konka/factory/DialogMenu;->parentItemId:I

    # invokes: Lcom/konka/factory/DialogMenu;->DialogMenuCancel(I)V
    invoke-static {v0, v1}, Lcom/konka/factory/DialogMenu;->access$100(Lcom/konka/factory/DialogMenu;I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/DialogMenu$1;->this$0:Lcom/konka/factory/DialogMenu;

    iget v1, v1, Lcom/konka/factory/DialogMenu;->parentItemId:I

    # invokes: Lcom/konka/factory/DialogMenu;->DialogMenuCancel(I)V
    invoke-static {v0, v1}, Lcom/konka/factory/DialogMenu;->access$100(Lcom/konka/factory/DialogMenu;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0067
        :pswitch_0    # com.konka.factory.R.id.textview_factory_dialogmenu_confirm
        :pswitch_1    # com.konka.factory.R.id.textview_factory_dialogmenu_cancel
    .end packed-switch
.end method
