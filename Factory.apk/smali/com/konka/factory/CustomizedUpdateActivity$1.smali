.class Lcom/konka/factory/CustomizedUpdateActivity$1;
.super Landroid/os/Handler;
.source "CustomizedUpdateActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/CustomizedUpdateActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomizedUpdateActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomizedUpdateActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    # getter for: Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z
    invoke-static {v0}, Lcom/konka/factory/CustomizedUpdateActivity;->access$000(Lcom/konka/factory/CustomizedUpdateActivity;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    # getter for: Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z
    invoke-static {v0}, Lcom/konka/factory/CustomizedUpdateActivity;->access$100(Lcom/konka/factory/CustomizedUpdateActivity;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    # getter for: Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z
    invoke-static {v0}, Lcom/konka/factory/CustomizedUpdateActivity;->access$200(Lcom/konka/factory/CustomizedUpdateActivity;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    # getter for: Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z
    invoke-static {v0}, Lcom/konka/factory/CustomizedUpdateActivity;->access$300(Lcom/konka/factory/CustomizedUpdateActivity;)Z

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    const-string v1, "Write customer default settings success."

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/konka/factory/CustomizedUpdateActivity;->access$400(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity$1;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    const-string v1, "Failed to upgrade customer default settings!"

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/konka/factory/CustomizedUpdateActivity;->access$400(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
