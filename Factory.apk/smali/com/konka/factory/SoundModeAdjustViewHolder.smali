.class public Lcom/konka/factory/SoundModeAdjustViewHolder;
.super Ljava/lang/Object;
.source "SoundModeAdjustViewHolder.java"


# instance fields
.field private eq1:I

.field private eq2:I

.field private eq3:I

.field private eq4:I

.field private eq5:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private peq:I

.field protected progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

.field protected progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

.field protected progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

.field protected progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

.field protected progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

.field protected progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

.field private soundModeActivity:Lcom/konka/factory/MainmenuActivity;

.field private soundmodearray:[Ljava/lang/String;

.field private soundmodeindex:I

.field protected text_factory_soundmode_eq1_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_eq2_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_eq3_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_eq4_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_eq5_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_peq_val:Landroid/widget/TextView;

.field protected text_factory_soundmode_soundmode_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 3
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v2, 0x0

    const/16 v0, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    const/16 v0, 0x78

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "STANDARD"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "MUSIC"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MOVIE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NEWS"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "USER"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ONSITE1"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ONSITE2"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodearray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method private freshDataToUIWhenSoundModChange()V
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq1_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq2_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq3_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq4_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq5_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_peq_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01fb    # com.konka.factory.R.id.textview_factory_soundmode_soundmode_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_soundmode_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01fe    # com.konka.factory.R.id.textview_factory_soundmode_eq1_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq1_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0202    # com.konka.factory.R.id.textview_factory_soundmode_eq2_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq2_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0206    # com.konka.factory.R.id.textview_factory_soundmode_eq3_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq3_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a020a    # com.konka.factory.R.id.textview_factory_soundmode_eq4_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq4_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a020e    # com.konka.factory.R.id.textview_factory_soundmode_eq5_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq5_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0212    # com.konka.factory.R.id.textview_factory_soundmode_peq_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_peq_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ff    # com.konka.factory.R.id.progressbar_facroty_soundmode_eq1

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0203    # com.konka.factory.R.id.progressbar_facroty_soundmode_eq2

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0207    # com.konka.factory.R.id.progressbar_facroty_soundmode_eq3

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a020b    # com.konka.factory.R.id.progressbar_facroty_soundmode_eq4

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a020f    # com.konka.factory.R.id.progressbar_facroty_soundmode_eq5

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0213    # com.konka.factory.R.id.progressbar_facroty_soundmode_peq

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

    return-void
.end method

.method public onCreate()Z
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==terry oncreate soundmodeindex:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    invoke-interface {v0, v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    const/4 v2, 0x6

    invoke-interface {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq1_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq2_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq3_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq4_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq5_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_peq_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_soundmode_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodearray:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return v3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0x64

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    const/4 v2, 0x0

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->SOUND_MODE_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    add-int/lit8 v5, v2, -0x1

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    :goto_1
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "terry , change sound mode :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_soundmode_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/SoundModeAdjustViewHolder;->freshDataToUIWhenSoundModChange()V

    goto :goto_0

    :cond_0
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    goto :goto_1

    :sswitch_2
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    if-eq v4, v5, :cond_1

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    :goto_2
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq1_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-interface {v4, v5, v7, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto :goto_0

    :cond_1
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    goto :goto_2

    :sswitch_3
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    if-eq v4, v5, :cond_2

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    :goto_3
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq2_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-interface {v4, v5, v8, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_2
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    goto :goto_3

    :sswitch_4
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    :goto_4
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq3_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-interface {v4, v5, v9, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_3
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    goto :goto_4

    :sswitch_5
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    :goto_5
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq4_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x4

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_4
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    goto :goto_5

    :sswitch_6
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    if-eq v4, v5, :cond_5

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    :goto_6
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq5_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x5

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_5
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    goto :goto_6

    :sswitch_7
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    const/16 v5, 0xf0

    if-eq v4, v5, :cond_6

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    :goto_7
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_peq_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x6

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setPEQ85HzGain(I)Z

    goto/16 :goto_0

    :cond_6
    iput v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    goto :goto_7

    :sswitch_8
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_9
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->SOUND_MODE_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    if-lez v4, :cond_7

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    :goto_8
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "terry ,left, change sound mode :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_soundmode_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/SoundModeAdjustViewHolder;->freshDataToUIWhenSoundModChange()V

    goto/16 :goto_0

    :cond_7
    add-int/lit8 v4, v2, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    goto :goto_8

    :sswitch_a
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    if-eqz v4, :cond_8

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    :goto_9
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq1_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq1:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    invoke-interface {v4, v5, v7, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_8
    iput v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq1:I

    goto :goto_9

    :sswitch_b
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    if-eqz v4, :cond_9

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    :goto_a
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq2_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq2:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    invoke-interface {v4, v5, v8, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_9
    iput v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq2:I

    goto :goto_a

    :sswitch_c
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    if-eqz v4, :cond_a

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    :goto_b
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq3_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq3:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    invoke-interface {v4, v5, v9, v6}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_a
    iput v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq3:I

    goto :goto_b

    :sswitch_d
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    if-eqz v4, :cond_b

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    :goto_c
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq4_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq4:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x4

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_b
    iput v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq4:I

    goto :goto_c

    :sswitch_e
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    if-eqz v4, :cond_c

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    :goto_d
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_eq5_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_eq5:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x5

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z

    goto/16 :goto_0

    :cond_c
    iput v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->eq5:I

    goto :goto_d

    :sswitch_f
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    if-eqz v4, :cond_d

    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    :goto_e
    iget v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->text_factory_soundmode_peq_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->progress_factory_soundmode_peq:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundmodeindex:I

    aget-object v5, v5, v6

    const/4 v6, 0x6

    iget v7, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-interface {v4, v5, v6, v7}, Lcom/konka/factory/desk/IFactoryDesk;->setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setPEQ85HzGain(I)Z

    goto/16 :goto_0

    :cond_d
    const/16 v4, 0xf0

    iput v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->peq:I

    goto :goto_e

    :sswitch_10
    iget-object v4, p0, Lcom/konka/factory/SoundModeAdjustViewHolder;->soundModeActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_10
        0x15 -> :sswitch_8
        0x16 -> :sswitch_0
        0x52 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a01f9 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_soundmode_soundmode
        0x7f0a01fc -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq1
        0x7f0a0200 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq2
        0x7f0a0204 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq3
        0x7f0a0208 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq4
        0x7f0a020c -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq5
        0x7f0a0210 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_soundmode_peq
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a01f9 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_soundmode_soundmode
        0x7f0a01fc -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq1
        0x7f0a0200 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq2
        0x7f0a0204 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq3
        0x7f0a0208 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq4
        0x7f0a020c -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_soundmode_eq5
        0x7f0a0210 -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_soundmode_peq
    .end sparse-switch
.end method
