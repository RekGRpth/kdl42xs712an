.class public Lcom/konka/factory/DialogMenu;
.super Landroid/app/Dialog;
.source "DialogMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;
    }
.end annotation


# static fields
.field private static dialogmenuactivity:Lcom/konka/factory/MainmenuActivity;

.field protected static handler:Landroid/os/Handler;

.field private static progressDialog:Landroid/app/ProgressDialog;

.field protected static textview_dialog_hint:Landroid/widget/TextView;

.field private static final upgradeStatus:[Ljava/lang/String;


# instance fields
.field private bUSBUpgradeFlag:Z

.field private listener:Landroid/view/View$OnClickListener;

.field public parentItemId:I

.field protected textview_dialog_cancel:Landroid/widget/TextView;

.field protected textview_dialog_confirm:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Fail!"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Success!"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "File Not Found!"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Please Plug USB!"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/factory/DialogMenu$2;

    invoke-direct {v0}, Lcom/konka/factory/DialogMenu$2;-><init>()V

    sput-object v0, Lcom/konka/factory/DialogMenu;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    new-instance v0, Lcom/konka/factory/DialogMenu$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/DialogMenu$1;-><init>(Lcom/konka/factory/DialogMenu;)V

    iput-object v0, p0, Lcom/konka/factory/DialogMenu;->listener:Landroid/view/View$OnClickListener;

    iput p2, p0, Lcom/konka/factory/DialogMenu;->parentItemId:I

    check-cast p1, Lcom/konka/factory/MainmenuActivity;

    sput-object p1, Lcom/konka/factory/DialogMenu;->dialogmenuactivity:Lcom/konka/factory/MainmenuActivity;

    return-void
.end method

.method private CheckUsbIsExist()Z
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/factory/MainmenuActivity;->CheckUsbIsExist()Z

    move-result v0

    return v0
.end method

.method private DialogMenuCancel(I)V
    .locals 0
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/factory/DialogMenu;->dismiss()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0161
        :pswitch_0    # com.konka.factory.R.id.textview_factory_otheroption_upgrademboot_val
    .end packed-switch
.end method

.method private static FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;

    const-string v0, ""

    new-instance v4, Ljava/io/File;

    const-string v5, "/mnt/usb/"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    array-length v5, v3

    if-ge v1, v5, :cond_0

    aget-object v5, v3, v1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v3, v1

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v3, v1

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static Upgrade6M30Fun()I
    .locals 4

    const/4 v1, 0x0

    const-string v3, "6m30.bin"

    invoke-static {v3}, Lcom/konka/factory/DialogMenu;->FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    if-ne v3, v2, :cond_0

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/TvManager;->startUrsaFirmwareUpgrade(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->ordinal()I

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v1

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static UpgradeMainFun()I
    .locals 6

    const/4 v2, 0x0

    const-string v3, "MstarUpgrade.bin"

    invoke-static {v3}, Lcom/konka/factory/DialogMenu;->FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    if-ne v3, v1, :cond_0

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "upgrade_mode"

    const-string v5, "usb"

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static UpgradeMbootFun()I
    .locals 4

    const/4 v2, 0x0

    const-string v3, "mboot.bin"

    invoke-static {v3}, Lcom/konka/factory/DialogMenu;->FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    if-ne v3, v1, :cond_0

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/tvapi/common/TvManager;->resetForMbootUpgrade(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/konka/factory/DialogMenu;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/DialogMenu;

    iget-boolean v0, p0, Lcom/konka/factory/DialogMenu;->bUSBUpgradeFlag:Z

    return v0
.end method

.method static synthetic access$100(Lcom/konka/factory/DialogMenu;I)V
    .locals 0
    .param p0    # Lcom/konka/factory/DialogMenu;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/konka/factory/DialogMenu;->DialogMenuCancel(I)V

    return-void
.end method

.method static synthetic access$200()Landroid/app/ProgressDialog;
    .locals 1

    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$202(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Landroid/app/ProgressDialog;

    sput-object p0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method static synthetic access$300()Landroid/app/ProgressDialog;
    .locals 1

    invoke-static {}, Lcom/konka/factory/DialogMenu;->getProgressDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;

    return-object v0
.end method

.method public static getHandler()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/konka/factory/DialogMenu;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private static getProgressDialog()Landroid/app/ProgressDialog;
    .locals 4

    const/4 v3, 0x1

    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    sget-object v1, Lcom/konka/factory/DialogMenu;->dialogmenuactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    sget-object v1, Lcom/konka/factory/DialogMenu;->dialogmenuactivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0800fd    # com.konka.factory.R.string.str_textview_factory_otheroption_waiting_val

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    :cond_0
    sget-object v0, Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/DialogMenu;->textview_dialog_confirm:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/DialogMenu;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/DialogMenu;->textview_dialog_cancel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/DialogMenu;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public DialogMenuConfirm(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lcom/konka/factory/DialogMenu;->CheckUsbIsExist()Z

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/konka/factory/USBUpgradeThread;->UpgradeMboot()Z

    iput-boolean v2, p0, Lcom/konka/factory/DialogMenu;->bUSBUpgradeFlag:Z

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    sget-object v1, Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/konka/factory/DialogMenu;->CheckUsbIsExist()Z

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-static {}, Lcom/konka/factory/DialogMenu;->UpgradeMainFun()I

    move-result v0

    sget-object v1, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v1}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    const-string v1, "File Not Found!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/konka/factory/DialogMenu;->UpgradeMainFun()I

    move-result v0

    sget-object v1, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v1}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    const-string v1, "Rebooting!!! Please Wait..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    const-string v1, "Set Environment Faild!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    const-string v1, "Please Plug USB!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/konka/factory/DialogMenu;->CheckUsbIsExist()Z

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-static {}, Lcom/konka/factory/USBUpgradeThread;->Upgrade6M30()Z

    iput-boolean v2, p0, Lcom/konka/factory/DialogMenu;->bUSBUpgradeFlag:Z

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    sget-object v1, Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0161 -> :sswitch_0    # com.konka.factory.R.id.textview_factory_otheroption_upgrademboot_val
        0x7f0a0164 -> :sswitch_1    # com.konka.factory.R.id.textview_factory_otheroption_upgrademain_val
        0x7f0a0167 -> :sswitch_2    # com.konka.factory.R.id.textview_factory_otheroption_upgrade6m30_val
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const v2, 0x7f030005    # com.konka.factory.R.layout.dialogmenu

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/konka/factory/DialogMenu;->parentItemId:I

    const v1, 0x7f0a0161    # com.konka.factory.R.id.textview_factory_otheroption_upgrademboot_val

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/konka/factory/DialogMenu;->setContentView(I)V

    :cond_0
    iget v0, p0, Lcom/konka/factory/DialogMenu;->parentItemId:I

    const v1, 0x7f0a0164    # com.konka.factory.R.id.textview_factory_otheroption_upgrademain_val

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v2}, Lcom/konka/factory/DialogMenu;->setContentView(I)V

    :cond_1
    iget v0, p0, Lcom/konka/factory/DialogMenu;->parentItemId:I

    const v1, 0x7f0a0167    # com.konka.factory.R.id.textview_factory_otheroption_upgrade6m30_val

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/konka/factory/DialogMenu;->setContentView(I)V

    :cond_2
    const v0, 0x7f0a0067    # com.konka.factory.R.id.textview_factory_dialogmenu_confirm

    invoke-virtual {p0, v0}, Lcom/konka/factory/DialogMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/DialogMenu;->textview_dialog_confirm:Landroid/widget/TextView;

    const v0, 0x7f0a0068    # com.konka.factory.R.id.textview_factory_dialogmenu_cancel

    invoke-virtual {p0, v0}, Lcom/konka/factory/DialogMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/DialogMenu;->textview_dialog_cancel:Landroid/widget/TextView;

    const v0, 0x7f0a0065    # com.konka.factory.R.id.textview_factory_dialogmenu_title

    invoke-virtual {p0, v0}, Lcom/konka/factory/DialogMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/factory/DialogMenu;->registerListeners()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/factory/DialogMenu;->bUSBUpgradeFlag:Z

    return-void
.end method
