.class Lcom/konka/factory/CustomerIDActivity$2;
.super Ljava/lang/Object;
.source "CustomerIDActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/CustomerIDActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomerIDActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    const/4 v14, 0x3

    const/16 v13, 0x8

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    new-instance v11, Lcom/konka/factory/ViewHolder;

    iget-object v12, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {v11, v12}, Lcom/konka/factory/ViewHolder;-><init>(Lcom/konka/factory/CustomerIDActivity;)V

    # setter for: Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$302(Lcom/konka/factory/CustomerIDActivity;Lcom/konka/factory/ViewHolder;)Lcom/konka/factory/ViewHolder;

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$300(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/ViewHolder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/konka/factory/ViewHolder;->findViewsForCustomerID()V

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    invoke-virtual {v11}, Lcom/konka/factory/CustomerIDActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090004    # com.konka.factory.R.array.str_customer_id

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v11

    # setter for: Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$702(Lcom/konka/factory/CustomerIDActivity;[Ljava/lang/String;)[Ljava/lang/String;

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    invoke-virtual {v11}, Lcom/konka/factory/CustomerIDActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090005    # com.konka.factory.R.array.str_customer_id_to_save

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v11

    # setter for: Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$802(Lcom/konka/factory/CustomerIDActivity;[Ljava/lang/String;)[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    new-array v0, v13, [S

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    new-array v3, v13, [C

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v11, 0x1d

    iget-object v12, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v12}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    iget-object v12, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v12}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v12, 0xd8

    invoke-virtual {v10, v11, v12}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const/4 v5, 0x0

    :goto_1
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    if-ge v5, v13, :cond_0

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    add-int/lit16 v10, v5, 0xd0

    aget-short v10, v0, v10

    int-to-char v10, v10

    aput-char v10, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v14, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v10, "KONKA"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v6, 0x0

    :goto_2
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$800(Lcom/konka/factory/CustomerIDActivity;)[Ljava/lang/String;

    move-result-object v10

    array-length v10, v10

    if-ge v6, v10, :cond_1

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$800(Lcom/konka/factory/CustomerIDActivity;)[Ljava/lang/String;

    move-result-object v10

    aget-object v10, v10, v6

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    move v2, v6

    :cond_1
    :goto_3
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->CustomerIDTable:[I
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$1000(Lcom/konka/factory/CustomerIDActivity;)[I

    move-result-object v10

    array-length v7, v10

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v7, :cond_2

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->CustomerIDTable:[I
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$1000(Lcom/konka/factory/CustomerIDActivity;)[I

    move-result-object v10

    aget v10, v10, v5

    if-ne v10, v2, :cond_5

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # setter for: Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I
    invoke-static {v10, v5}, Lcom/konka/factory/CustomerIDActivity;->access$1102(Lcom/konka/factory/CustomerIDActivity;I)I

    :cond_2
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$1100(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v11

    div-int/lit8 v11, v11, 0x9

    # setter for: Lcom/konka/factory/CustomerIDActivity;->page_index:I
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$1202(Lcom/konka/factory/CustomerIDActivity;I)I

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$1100(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v11

    rem-int/lit8 v11, v11, 0x9

    # setter for: Lcom/konka/factory/CustomerIDActivity;->curItem_index:I
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$1302(Lcom/konka/factory/CustomerIDActivity;I)I

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$700(Lcom/konka/factory/CustomerIDActivity;)[Ljava/lang/String;

    move-result-object v11

    array-length v11, v11

    # setter for: Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$1402(Lcom/konka/factory/CustomerIDActivity;I)I

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$1400(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v10

    rem-int/lit8 v10, v10, 0x9

    if-nez v10, :cond_6

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$1400(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v11

    div-int/lit8 v11, v11, 0x9

    # setter for: Lcom/konka/factory/CustomerIDActivity;->page_total:I
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$202(Lcom/konka/factory/CustomerIDActivity;I)I

    :goto_5
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # invokes: Lcom/konka/factory/CustomerIDActivity;->registerListeners()V
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$1500(Lcom/konka/factory/CustomerIDActivity;)V

    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v10}, Lcom/konka/factory/CustomerIDActivity;->access$1600(Lcom/konka/factory/CustomerIDActivity;)Landroid/os/Handler;

    move-result-object v10

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->MSG_ONCREAT:I
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$000(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_6
    iget-object v10, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v11, p0, Lcom/konka/factory/CustomerIDActivity$2;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I
    invoke-static {v11}, Lcom/konka/factory/CustomerIDActivity;->access$1400(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v11

    div-int/lit8 v11, v11, 0x9

    add-int/lit8 v11, v11, 0x1

    # setter for: Lcom/konka/factory/CustomerIDActivity;->page_total:I
    invoke-static {v10, v11}, Lcom/konka/factory/CustomerIDActivity;->access$202(Lcom/konka/factory/CustomerIDActivity;I)I

    goto :goto_5
.end method
