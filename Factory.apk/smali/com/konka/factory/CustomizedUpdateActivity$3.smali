.class Lcom/konka/factory/CustomizedUpdateActivity$3;
.super Ljava/lang/Object;
.source "CustomizedUpdateActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/CustomizedUpdateActivity;->updateFile(Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomizedUpdateActivity;

.field final synthetic val$filename:Ljava/lang/String;

.field final synthetic val$folder:Ljava/lang/String;

.field final synthetic val$isDestiDir:Z


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iput-object p2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$filename:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$folder:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$isDestiDir:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$filename:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$folder:Ljava/lang/String;

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->downloadFile(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v2, v3, v4}, Lcom/konka/factory/CustomizedUpdateActivity;->access$600(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$isDestiDir:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$filename:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$folder:Ljava/lang/String;

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->moveFileToDestination(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {v2, v3, v4}, Lcom/konka/factory/CustomizedUpdateActivity;->access$700(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$filename:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$folder:Ljava/lang/String;

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->delFile(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/konka/factory/CustomizedUpdateActivity;->access$800(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->val$filename:Ljava/lang/String;

    # invokes: Lcom/konka/factory/CustomizedUpdateActivity;->setFlag(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/konka/factory/CustomizedUpdateActivity;->access$900(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    iput v0, v1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity$3;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iget-object v2, v2, Lcom/konka/factory/CustomizedUpdateActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
