.class public final enum Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;
.super Ljava/lang/Enum;
.source "DialogMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/DialogMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumUpgradeStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

.field public static final enum E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

.field public static final enum E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

.field public static final enum E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    const-string v1, "E_UPGRADE_FAIL"

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    new-instance v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    const-string v1, "E_UPGRADE_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    new-instance v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    const-string v1, "E_UPGRADE_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    sget-object v1, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FAIL:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->$VALUES:[Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;
    .locals 1

    const-class v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;
    .locals 1

    sget-object v0, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->$VALUES:[Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v0}, [Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    return-object v0
.end method
