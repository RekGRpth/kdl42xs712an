.class public Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "T_MS_COLOR_TEMP_DATA"
.end annotation


# instance fields
.field public bluegain:S

.field public blueoffset:S

.field public greengain:S

.field public greenoffset:S

.field public redgain:S

.field public redoffset:S


# direct methods
.method public constructor <init>(SSSSSS)V
    .locals 0
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S
    .param p6    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iput-short p2, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iput-short p3, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iput-short p4, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iput-short p5, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iput-short p6, p0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    return-void
.end method
