.class public Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AUDIO_PEQ_PARAM"
.end annotation


# instance fields
.field public Band:I

.field public Foh:I

.field public Fol:I

.field public Gain:I

.field public QValue:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x50

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Band:I

    const/16 v0, 0x78

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    iput v1, p0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    const/16 v0, 0x2d

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    iput v1, p0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    return-void
.end method
