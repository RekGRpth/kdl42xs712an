.class public final enum Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;
.super Ljava/lang/Enum;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_COLOR_TEMP"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_COOL:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_NATURE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_USER:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_USER2:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_WARM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_COOL"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_COOL:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_NATURE"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_WARM"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_WARM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_USER"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_USER2"

    invoke-direct {v0, v1, v7}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER2:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_NUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_COOL:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_WARM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER2:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;
    .locals 1

    const-class v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;
    .locals 1

    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, [Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    return-object v0
.end method
