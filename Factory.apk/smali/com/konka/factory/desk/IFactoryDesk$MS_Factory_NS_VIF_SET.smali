.class public Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_Factory_NS_VIF_SET"
.end annotation


# instance fields
.field public ChinaDescramblerBox:S

.field public GainDistributionThr:I

.field public VifACIAGCREF:S

.field public VifAgcVgaBase:S

.field public VifAsiaSignalOption:Z

.field public VifClampgainClampOvNegative:I

.field public VifClampgainGainOvNegative:I

.field public VifCrKi:S

.field public VifCrKi1:S

.field public VifCrKi2:S

.field public VifCrKp:S

.field public VifCrKp1:S

.field public VifCrKp2:S

.field public VifCrKpKiAdjust:Z

.field public VifCrLockThr:I

.field public VifCrThr:I

.field public VifDelayReduce:S

.field public VifOverModulation:Z

.field public VifTop:S

.field public VifVersion:S

.field public VifVgaMaximum:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAgcVgaBase:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrLockThr:I

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    iput-boolean v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    iput-boolean v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainClampOvNegative:I

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    iput-boolean v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    iput-short v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    return-void
.end method
