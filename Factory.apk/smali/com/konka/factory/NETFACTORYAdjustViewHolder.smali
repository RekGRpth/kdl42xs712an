.class public Lcom/konka/factory/NETFACTORYAdjustViewHolder;
.super Ljava/lang/Object;
.source "NETFACTORYAdjustViewHolder.java"


# instance fields
.field private NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field protected gridview_factory_netfactorymenu_hdcpkey:Landroid/widget/GridView;

.field protected text_factory_netfactorymenu_macaddress_val:Landroid/widget/TextView;

.field protected text_factory_netfactorymenu_serialnumber_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 0
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method private static toChars([B)[C
    .locals 7
    .param p0    # [B

    const/16 v6, 0xa

    array-length v0, p0

    mul-int/lit8 v4, v0, 0x2

    new-array v3, v4, [C

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-byte v4, p0, v2

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v1, v4, 0xf

    mul-int/lit8 v5, v2, 0x2

    if-lt v1, v6, :cond_0

    add-int/lit8 v4, v1, 0x61

    add-int/lit8 v4, v4, -0xa

    :goto_1
    int-to-char v4, v4

    aput-char v4, v3, v5

    aget-byte v4, p0, v2

    and-int/lit8 v1, v4, 0xf

    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v5, v4, 0x1

    if-lt v1, v6, :cond_1

    add-int/lit8 v4, v1, 0x61

    add-int/lit8 v4, v4, -0xa

    :goto_2
    int-to-char v4, v4

    aput-char v4, v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v4, v1, 0x30

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v1, 0x30

    goto :goto_2

    :cond_2
    return-object v3
.end method


# virtual methods
.method public DisplayMode()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "DisplayMode"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public MacAddress()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "MacAddress"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public OnlineUpdate()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "OnlineUpdate"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public SerialNumber()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "SerialNumber"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public USB1Port()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "USB1Port"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public USB2Port()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "USB2Port"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public USB3Port()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "USB3Port"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public USB4Port()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "USB4Port"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00a9    # com.konka.factory.R.id.textview_factory_netfactorymenu_serialnumber_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->text_factory_netfactorymenu_serialnumber_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00ac    # com.konka.factory.R.id.textview_factory_netfactorymenu_macaddress_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->text_factory_netfactorymenu_macaddress_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00c1    # com.konka.factory.R.id.gridview_factory_netfactorymenu_hdcpkey

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->gridview_factory_netfactorymenu_hdcpkey:Landroid/widget/GridView;

    return-void
.end method

.method public onCreate()V
    .locals 28

    const/4 v13, 0x0

    invoke-static {}, Lcom/konka/factory/Netfactory;->InitNetwork()I

    move-result v13

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "InitNetwork = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v13, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/konka/factory/Netfactory;->CheckVersion()I

    move-result v13

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "CheckVersion = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v13, :cond_1

    invoke-static {}, Lcom/konka/factory/Netfactory;->CloseNetwork()I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/konka/factory/Netfactory;->GetData()I

    move-result v13

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "GetData = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v13, :cond_2

    invoke-static {}, Lcom/konka/factory/Netfactory;->CloseNetwork()I

    goto :goto_0

    :cond_2
    const/16 v24, 0x2000

    move/from16 v0, v24

    new-array v7, v0, [S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    const/16 v26, 0x2000

    invoke-virtual/range {v24 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    invoke-static {}, Lcom/konka/factory/Netfactory;->GetMac()[B

    move-result-object v4

    if-eqz v4, :cond_4

    new-instance v19, Ljava/lang/String;

    invoke-static {v4}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->toChars([B)[C

    move-result-object v24

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "GetMac = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    array-length v0, v4

    move/from16 v16, v0

    const/16 v24, 0x6

    move/from16 v0, v16

    move/from16 v1, v24

    if-le v0, v1, :cond_3

    const/16 v16, 0x6

    :cond_3
    const/4 v12, 0x0

    :goto_1
    move/from16 v0, v16

    if-ge v12, v0, :cond_5

    add-int/lit8 v24, v12, 0x0

    aget-byte v25, v4, v12

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    aput-short v25, v7, v24

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :catch_0
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "GetMac = null"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->text_factory_netfactorymenu_macaddress_val:Landroid/widget/TextView;

    move-object/from16 v24, v0

    const-string v25, "macaddress null"

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    invoke-static {}, Lcom/konka/factory/Netfactory;->GetHdcp()[B

    move-result-object v3

    if-eqz v3, :cond_7

    new-instance v18, Ljava/lang/String;

    invoke-static {v3}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->toChars([B)[C

    move-result-object v24

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "GetHdcp = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    array-length v0, v3

    move/from16 v16, v0

    const/16 v24, 0x130

    move/from16 v0, v16

    move/from16 v1, v24

    if-le v0, v1, :cond_6

    const/16 v16, 0x130

    :cond_6
    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_8

    add-int/lit16 v0, v12, 0x100

    move/from16 v24, v0

    aget-byte v25, v3, v12

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    aput-short v25, v7, v24

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_7
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "GetHdcp = null"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    invoke-static {}, Lcom/konka/factory/Netfactory;->GetSN()Ljava/lang/String;

    move-result-object v20

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "GetSN = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v24, 0x32

    move/from16 v0, v24

    new-array v5, v0, [B

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v0, v6

    move/from16 v16, v0

    const/16 v24, 0x32

    move/from16 v0, v16

    move/from16 v1, v24

    if-le v0, v1, :cond_9

    const/16 v16, 0x32

    :cond_9
    const/4 v12, 0x0

    :goto_3
    move/from16 v0, v16

    if-ge v12, v0, :cond_a

    aget-byte v24, v6, v12

    aput-byte v24, v5, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_a
    const/4 v12, 0x0

    :goto_4
    const/16 v24, 0x32

    move/from16 v0, v24

    if-ge v12, v0, :cond_b

    add-int/lit16 v0, v12, 0x80

    move/from16 v24, v0

    aget-byte v25, v5, v12

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    aput-short v25, v7, v24

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_b
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v7}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    const-string v24, "%02X:%02X:%02X:%02X:%02X:%02X"

    const/16 v25, 0x6

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    const/16 v27, 0x1

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x2

    const/16 v27, 0x2

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x3

    const/16 v27, 0x3

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x4

    const/16 v27, 0x4

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x5

    const/16 v27, 0x5

    aget-short v27, v7, v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const-string v25, "ethaddr"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    const/16 v26, 0x2000

    invoke-virtual/range {v24 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v9, v0, [B

    const/4 v12, 0x0

    :goto_5
    const/16 v24, 0x6

    move/from16 v0, v24

    if-ge v12, v0, :cond_c

    add-int/lit8 v24, v12, 0x0

    aget-short v24, v7, v24

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v9, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_5

    :catch_1
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_c
    const/16 v19, 0x0

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const-string v25, "ethaddr"

    invoke-virtual/range {v24 .. v25}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironment(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v19

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->text_factory_netfactorymenu_macaddress_val:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v24, 0x130

    move/from16 v0, v24

    new-array v8, v0, [B

    const/4 v12, 0x0

    :goto_7
    const/16 v24, 0x130

    move/from16 v0, v24

    if-ge v12, v0, :cond_d

    add-int/lit16 v0, v12, 0x100

    move/from16 v24, v0

    aget-short v24, v7, v24

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v8, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_7

    :catch_2
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->gridview_factory_netfactorymenu_hdcpkey:Landroid/widget/GridView;

    move-object/from16 v24, v0

    new-instance v25, Lcom/konka/factory/GridViewAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    move-object/from16 v26, v0

    invoke-static {v8}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->toChars([B)[C

    move-result-object v27

    invoke-direct/range {v25 .. v27}, Lcom/konka/factory/GridViewAdapter;-><init>(Landroid/content/Context;[C)V

    invoke-virtual/range {v24 .. v25}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/16 v24, 0x32

    move/from16 v0, v24

    new-array v10, v0, [B

    const/4 v12, 0x0

    :goto_8
    const/16 v24, 0x32

    move/from16 v0, v24

    if-ge v12, v0, :cond_e

    add-int/lit16 v0, v12, 0x80

    move/from16 v24, v0

    aget-short v24, v7, v24

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v10, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    :cond_e
    new-instance v20, Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->text_factory_netfactorymenu_serialnumber_val:Landroid/widget/TextView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v21, 0x0

    const/16 v24, 0x5f

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    move-object/from16 v21, v20

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    move-object/from16 v22, v0

    const-wide/16 v14, 0x0

    :try_start_3
    invoke-static/range {v22 .. v22}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    move-object/from16 v0, v21

    invoke-static {v0, v4, v14, v15}, Lcom/konka/factory/Netfactory;->SetTVInfo(Ljava/lang/String;[BJ)I

    move-result v13

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "SetTVInfo = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_a
    invoke-static {}, Lcom/konka/factory/Netfactory;->CloseNetwork()I

    goto/16 :goto_0

    :cond_f
    const/16 v24, 0x0

    const/16 v25, 0x5f

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v25

    move-object/from16 v0, v20

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    goto :goto_9

    :catch_3
    move-exception v11

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "TVsn error"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_a
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->SerialNumber()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->MacAddress()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->USB1Port()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->USB2Port()V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->USB3Port()V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->USB4Port()V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->DisplayMode()V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->OnlineUpdate()V

    goto :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->NetFactoryActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_9
        0x42 -> :sswitch_0
        0x52 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a00a7 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_serialnumber
        0x7f0a00aa -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_macaddress
        0x7f0a00ad -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_usb1port
        0x7f0a00b0 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_usb2port
        0x7f0a00b3 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_usb3port
        0x7f0a00b6 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_usb4port
        0x7f0a00b9 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_displaymode
        0x7f0a00bc -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_netfactorymenu_onlineupdate
    .end sparse-switch
.end method
