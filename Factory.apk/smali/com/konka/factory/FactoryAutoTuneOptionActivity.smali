.class public Lcom/konka/factory/FactoryAutoTuneOptionActivity;
.super Landroid/app/Activity;
.source "FactoryAutoTuneOptionActivity.java"


# static fields
.field public static PersionCustomer:Ljava/lang/String;

.field public static PersionCustomer1:Ljava/lang/String;


# instance fields
.field private CountrySelectTable:[I

.field private CountrySelectTable_snowa:[I

.field private CountrySelectTable_xvision:[I

.field private MAXINDEXS:I

.field private MSG_ONCREAT:I

.field private final ONEPINDEXS:I

.field private OptionCountrys:[Ljava/lang/String;

.field private OptionScanTypes:[Ljava/lang/String;

.field public curCustomer:Ljava/lang/String;

.field private curItem_index:I

.field private factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

.field private hint_left_arrow:Landroid/widget/ImageView;

.field private hint_right_arrow:Landroid/widget/ImageView;

.field private listener:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private page_index:I

.field private page_total:I

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private tableCountrySelect_index:I

.field private tuningtype:I

.field private viewholder_autotune:Lcom/konka/factory/ViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    const-string v0, "x.vision"

    sput-object v0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/16 v7, 0x9

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tuningtype:I

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curItem_index:I

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "ATV"

    aput-object v1, v0, v3

    const-string v1, "DTV"

    aput-object v1, v0, v4

    const-string v1, "ATV/DTV"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;

    iput v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->ONEPINDEXS:I

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I

    const/16 v0, 0x3e9

    iput v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MSG_ONCREAT:I

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable:[I

    const/16 v0, 0x3b

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    new-instance v0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;-><init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity$4;-><init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    iput-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private CoutrySelected(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->ordinal()I

    move-result v3

    iput v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tuningtype:I

    iget v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    mul-int/lit8 v3, v3, 0x9

    add-int v1, v3, p1

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v4, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    aget v0, v3, v1

    :goto_0
    const-string v3, "FactoryAutoTuneOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***33***coutry_index==  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v0, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v6, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v6, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    invoke-virtual {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateCUS_DEF_SETTING()V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n===>>time.timezone "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v3

    if-lt v0, v3, :cond_2

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v4, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    aget v0, v3, v1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable:[I

    aget v0, v3, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-direct {p0, v3}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    invoke-virtual {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->finish()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MSG_ONCREAT:I

    return v0
.end method

.method static synthetic access$100(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutryRequestFocus()V

    return-void
.end method

.method static synthetic access$1000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    return-object v0
.end method

.method static synthetic access$1100(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable:[I

    return-object v0
.end method

.method static synthetic access$1202(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    return p1
.end method

.method static synthetic access$1302(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curItem_index:I

    return p1
.end method

.method static synthetic access$1400(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/kkinterface/tv/TvDeskProvider;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tuningtype:I

    return p1
.end method

.method static synthetic access$1600(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/konka/factory/FactoryAutoTuneOptionActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    return v0
.end method

.method static synthetic access$1702(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    return p1
.end method

.method static synthetic access$1800(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->registerListeners()V

    return-void
.end method

.method static synthetic access$1900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I

    return v0
.end method

.method static synthetic access$2000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)V
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CoutrySelected(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I

    return p1
.end method

.method static synthetic access$300(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/factory/ViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/factory/ViewHolder;)Lcom/konka/factory/ViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # Lcom/konka/factory/ViewHolder;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    return-object p1
.end method

.method static synthetic access$402(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$502(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    return-void
.end method

.method static synthetic access$700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    return-object v0
.end method

.method static synthetic access$702(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    return-object p1
.end method

.method static synthetic access$800(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    return-object v0
.end method

.method static synthetic access$900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I

    return v0
.end method

.method static synthetic access$902(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I

    return p1
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v2, "Asia/Shanghai"

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_0

    const-string v2, "Australia/Sydney"

    :goto_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_47

    :goto_1
    return-void

    :cond_0
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1

    const-string v2, "Europe/Vienna"

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2

    const-string v2, "Europe/Brussels"

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3

    const-string v2, "Europe/Sofia"

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_4

    const-string v2, "Europe/Zagreb"

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_5

    const-string v2, "Europe/Prague"

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_6

    const-string v2, "Europe/Copenhagen"

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_7

    const-string v2, "Europe/Helsinki"

    goto :goto_0

    :cond_7
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_8

    const-string v2, "Europe/Paris"

    goto :goto_0

    :cond_8
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_9

    const-string v2, "Europe/Berlin"

    goto :goto_0

    :cond_9
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_a

    const-string v2, "Europe/Athens"

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_b

    const-string v2, "Europe/Budapest"

    goto :goto_0

    :cond_b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_c

    const-string v2, "Europe/Rome"

    goto :goto_0

    :cond_c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_d

    const-string v2, "Europe/Luxembourg"

    goto :goto_0

    :cond_d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_e

    const-string v2, "Europe/Amsterdam"

    goto :goto_0

    :cond_e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_f

    const-string v2, "Europe/Oslo"

    goto :goto_0

    :cond_f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_10

    const-string v2, "Europe/Warsaw"

    goto :goto_0

    :cond_10
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_11

    const-string v2, "Europe/Lisbon"

    goto/16 :goto_0

    :cond_11
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_12

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_12
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_13

    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_13
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_14

    const-string v2, "Europe/Belgrade"

    goto/16 :goto_0

    :cond_14
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_15

    const-string v2, "Europe/Ljubljana"

    goto/16 :goto_0

    :cond_15
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_16

    const-string v2, "Europe/Madrid"

    goto/16 :goto_0

    :cond_16
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_17

    const-string v2, "Europe/Stockholm"

    goto/16 :goto_0

    :cond_17
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_18

    const-string v2, "Europe/Zurich"

    goto/16 :goto_0

    :cond_18
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_19

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_19
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1a

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_1a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1b

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_1b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1c

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1d

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1e

    const-string v2, "Europe/Riga"

    goto/16 :goto_0

    :cond_1e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1f

    const-string v2, "Europe/Bratislava"

    goto/16 :goto_0

    :cond_1f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_20

    const-string v2, "Europe/Istanbul"

    goto/16 :goto_0

    :cond_20
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_21

    const-string v2, "Europe/Dublin"

    goto/16 :goto_0

    :cond_21
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_22

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_22
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_23

    const-string v2, "Asia/Manila"

    goto/16 :goto_0

    :cond_23
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_24

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_24
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_25

    const-string v2, "Indian/Maldives"

    goto/16 :goto_0

    :cond_25
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_26

    const-string v2, "America/Montevideo"

    goto/16 :goto_0

    :cond_26
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_27

    const-string v2, "America/Lima"

    goto/16 :goto_0

    :cond_27
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_28

    const-string v2, "America/Argentina/Buenos_Aires"

    goto/16 :goto_0

    :cond_28
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_29

    const-string v2, "America/Santiago"

    goto/16 :goto_0

    :cond_29
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2a

    const-string v2, "America/Caracas"

    goto/16 :goto_0

    :cond_2a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2b

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2c

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2d

    const-string v2, "America/Asuncion"

    goto/16 :goto_0

    :cond_2d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2e

    const-string v2, "America/La_Paz"

    goto/16 :goto_0

    :cond_2e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2f

    const-string v2, "America/Belize"

    goto/16 :goto_0

    :cond_2f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_30

    const-string v2, "America/Managua"

    goto/16 :goto_0

    :cond_30
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_31

    const-string v2, "America/Guatemala"

    goto/16 :goto_0

    :cond_31
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_32

    const-string v2, "Asia/Shanghai"

    goto/16 :goto_0

    :cond_32
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_33

    const-string v2, "Asia/Taipei"

    goto/16 :goto_0

    :cond_33
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_34

    const-string v2, "America/Noronha"

    goto/16 :goto_0

    :cond_34
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_35

    const-string v2, "America/St_Johns"

    goto/16 :goto_0

    :cond_35
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_36

    const-string v2, "America/Mexico_City"

    goto/16 :goto_0

    :cond_36
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_37

    const-string v2, "America/New_York"

    goto/16 :goto_0

    :cond_37
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_38

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_38
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_39

    const-string v2, "Pacific/Fiji"

    goto/16 :goto_0

    :cond_39
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3b

    :cond_3a
    const-string v2, "Asia/Karachi"

    goto/16 :goto_0

    :cond_3b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3d

    :cond_3c
    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_3d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3e

    const-string v2, "Asia/Tehran"

    goto/16 :goto_0

    :cond_3e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3f

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_3f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_41

    :cond_40
    const-string v2, "Europe/Athens"

    goto/16 :goto_0

    :cond_41
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_43

    :cond_42
    const-string v2, "Europe/Amsterdam"

    goto/16 :goto_0

    :cond_43
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_44

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_44
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_45

    const-string v2, "Asia/Calcutta"

    goto/16 :goto_0

    :cond_45
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_46

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_46
    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_47
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private updateUiCoutryRequestFocus()V
    .locals 1

    iget v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curItem_index:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private updateUiCoutrySelect()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    mul-int/lit8 v0, v1, 0x9

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_4
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_5
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_6
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_7
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_8

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_8
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_9
    iget v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_a
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_3

    :cond_4
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_4

    :cond_5
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_5

    :cond_6
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_6

    :cond_7
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_7

    :cond_8
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_8

    :cond_9
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_9

    :cond_a
    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_a
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030001    # com.konka.factory.R.layout.autotuningcountryoption

    invoke-virtual {p0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->setContentView(I)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;

    invoke-direct {v2, p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;-><init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-static {}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->values()[Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v2

    iget v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tuningtype:I

    aget-object v2, v2, v3

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0x9

    iget v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0x9

    iget v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    mul-int/lit8 v2, v2, 0x9

    iget v3, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_1
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_4
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_5
    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v2, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;

    iget-object v2, v2, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->finish()V

    goto/16 :goto_0

    :sswitch_3
    const/4 v2, 0x1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a0023
        :pswitch_0    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_denmark
        :pswitch_1    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_finland
        :pswitch_2    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_france
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f0a001d
        :pswitch_3    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_australia
        :pswitch_4    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_austria
        :pswitch_5    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_beligum
    .end packed-switch
.end method
