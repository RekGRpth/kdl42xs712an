.class Lcom/konka/factory/MainmenuActivity$1;
.super Ljava/lang/Object;
.source "MainmenuActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/MainmenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/MainmenuActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/MainmenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/4 v1, 0x6

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/ADCAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/ADCAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$202(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/ADCAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/ADCAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/ADCAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->onCreate()Z

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/4 v1, 0x7

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/WBAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/WBAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$402(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/WBAdjustViewHolder;)Lcom/konka/factory/WBAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/WBAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/WBAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/WBAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/WBAdjustViewHolder;->onCreate()Z

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0x8

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/PictureModeAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/PictureModeAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$502(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/PictureModeAdjustViewHolder;)Lcom/konka/factory/PictureModeAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$500(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PictureModeAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$500(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PictureModeAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0x9

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/SoundModeAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/SoundModeAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$602(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SoundModeAdjustViewHolder;)Lcom/konka/factory/SoundModeAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$600(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SoundModeAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/SoundModeAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$600(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SoundModeAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/SoundModeAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xa

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/EnergyAjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/EnergyAjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$702(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/EnergyAjustViewHolder;)Lcom/konka/factory/EnergyAjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/EnergyAjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/EnergyAjustViewHolder;->findview()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/EnergyAjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/EnergyAjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xb

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/NonStandardAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/NonStandardAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$802(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NonStandardAdjustViewHolder;)Lcom/konka/factory/NonStandardAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonStandardAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NonStandardAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonStandardAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NonStandardAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xc

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/NonLinearAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/NonLinearAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$902(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NonLinearAdjustViewHolder;)Lcom/konka/factory/NonLinearAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$900(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonLinearAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NonLinearAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$900(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonLinearAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NonLinearAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xd

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/OverScanAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/OverScanAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1002(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/OverScanAdjustViewHolder;)Lcom/konka/factory/OverScanAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1000(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OverScanAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/OverScanAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1000(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OverScanAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/OverScanAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xe

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/SSCAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/SSCAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1102(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SSCAdjustViewHolder;)Lcom/konka/factory/SSCAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1100(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SSCAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/SSCAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1100(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SSCAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/SSCAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0xf

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/PEQAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/PEQAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1202(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/PEQAdjustViewHolder;)Lcom/konka/factory/PEQAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PEQAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/PEQAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PEQAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/PEQAdjustViewHolder;->onCreate()V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/UPDATEAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/UPDATEAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1302(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/UPDATEAdjustViewHolder;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->findview()V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v3}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1402(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NETFACTORYAdjustViewHolder;)Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->onCreate()V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v4}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->defaultconfigViewHolder:Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1502(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;)Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v5}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/SerialPrintAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/SerialPrintAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->serialprintViewHolder:Lcom/konka/factory/SerialPrintAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1602(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SerialPrintAdjustViewHolder;)Lcom/konka/factory/SerialPrintAdjustViewHolder;

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    const/16 v1, 0x10

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/OtherOptionAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1702(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OtherOptionAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OtherOptionAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # setter for: Lcom/konka/factory/MainmenuActivity;->currentPage:I
    invoke-static {v0, v2}, Lcom/konka/factory/MainmenuActivity;->access$002(Lcom/konka/factory/MainmenuActivity;I)I

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    iget-object v3, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v3}, Lcom/konka/factory/MainmenuActivity;->access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    # setter for: Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    invoke-static {v0, v1}, Lcom/konka/factory/MainmenuActivity;->access$1802(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;)Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity$1;->this$0:Lcom/konka/factory/MainmenuActivity;

    # getter for: Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    invoke-static {v0}, Lcom/konka/factory/MainmenuActivity;->access$1800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OnCreate()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a007a -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_netfactory
        0x7f0a007c -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_update
        0x7f0a007e -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings
        0x7f0a0080 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_defaultconfig
        0x7f0a0082 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_serialprint
        0x7f0a0084 -> :sswitch_0    # com.konka.factory.R.id.linearlayout_factory_adc
        0x7f0a0085 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_whitebalance
        0x7f0a0087 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_picturemode
        0x7f0a0089 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_soundmode
        0x7f0a008b -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_energy
        0x7f0a008d -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_nonstandard
        0x7f0a008f -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_non_linear
        0x7f0a0091 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_overscan
        0x7f0a0093 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_ssc
        0x7f0a0095 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_peq
        0x7f0a00a3 -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_otheroption
    .end sparse-switch
.end method
