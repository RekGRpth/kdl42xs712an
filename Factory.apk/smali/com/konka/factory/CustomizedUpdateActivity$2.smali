.class Lcom/konka/factory/CustomizedUpdateActivity$2;
.super Ljava/lang/Object;
.source "CustomizedUpdateActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/CustomizedUpdateActivity;->downloadCusDefSettingFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomizedUpdateActivity;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomizedUpdateActivity;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomizedUpdateActivity$2;->this$0:Lcom/konka/factory/CustomizedUpdateActivity;

    iput-object p2, p0, Lcom/konka/factory/CustomizedUpdateActivity$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const-string v7, "/data/misc/konka/config.txt"

    const/4 v5, 0x0

    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v11, "http://192.168.0.100:8080/FactoryFastConfig/ResponseFileRequest?filename=config.txt"

    invoke-direct {v1, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    check-cast v3, Ljava/net/HttpURLConnection;

    const/16 v11, 0xbb8

    invoke-virtual {v3, v11}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const/16 v11, 0xbb8

    invoke-virtual {v3, v11}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/16 v11, 0x2000

    new-array v2, v11, [B

    const/4 v10, 0x0

    :goto_0
    const/4 v11, 0x0

    const/16 v12, 0x2000

    invoke-virtual {v9, v2, v11, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_0

    const/4 v11, 0x0

    invoke-virtual {v8, v2, v11, v10}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/net/MalformedURLException;->printStackTrace()V

    const/4 v5, 0x1

    :goto_1
    move v0, v5

    iget-object v11, p0, Lcom/konka/factory/CustomizedUpdateActivity$2;->val$handler:Landroid/os/Handler;

    new-instance v12, Lcom/konka/factory/CustomizedUpdateActivity$2$1;

    invoke-direct {v12, p0, v0}, Lcom/konka/factory/CustomizedUpdateActivity$2$1;-><init>(Lcom/konka/factory/CustomizedUpdateActivity$2;Z)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    const-string v11, "CustomizedUpdateActivity"

    const-string v12, "downloadCusDefSettingFile==========>>>OK\n"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const/4 v5, 0x1

    goto :goto_1
.end method
