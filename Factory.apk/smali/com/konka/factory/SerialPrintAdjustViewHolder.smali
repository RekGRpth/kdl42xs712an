.class public Lcom/konka/factory/SerialPrintAdjustViewHolder;
.super Ljava/lang/Object;
.source "SerialPrintAdjustViewHolder.java"


# instance fields
.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private serialprintactivity:Lcom/konka/factory/MainmenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 6
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprintactivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget-object v3, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprintactivity:Lcom/konka/factory/MainmenuActivity;

    const v4, 0x7f0a01f7    # com.konka.factory.R.id.linearlayout_factory_serialprint_open

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprintactivity:Lcom/konka/factory/MainmenuActivity;

    const v4, 0x7f0a01f8    # com.konka.factory.R.id.linearlayout_factory_serialprint_close

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    :try_start_0
    const-string v3, "on"

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    const-string v5, "UARTOnOff"

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprintactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprint_open()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprint_close()V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/konka/factory/SerialPrintAdjustViewHolder;->serialprintactivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x42 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a01f7
        :pswitch_0    # com.konka.factory.R.id.linearlayout_factory_serialprint_open
        :pswitch_1    # com.konka.factory.R.id.linearlayout_factory_serialprint_close
    .end packed-switch
.end method

.method public serialprint_close()V
    .locals 4

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "serialprint close"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "setEnvironment"

    const-string v2, "UARTOnOff off"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "UARTOnOff"

    const-string v3, "off"

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public serialprint_open()V
    .locals 4

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "serialprint open"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "setEnvironment"

    const-string v2, "UARTOnOff on"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "UARTOnOff"

    const-string v3, "on"

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
