.class public Lcom/konka/factory/ViewHolder;
.super Ljava/lang/Object;
.source "ViewHolder.java"


# instance fields
.field private autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

.field protected button_cha_country_1:Landroid/widget/Button;

.field protected button_cha_country_2:Landroid/widget/Button;

.field protected button_cha_country_3:Landroid/widget/Button;

.field protected button_cha_country_4:Landroid/widget/Button;

.field protected button_cha_country_5:Landroid/widget/Button;

.field protected button_cha_country_6:Landroid/widget/Button;

.field protected button_cha_country_7:Landroid/widget/Button;

.field protected button_cha_country_8:Landroid/widget/Button;

.field protected button_cha_country_9:Landroid/widget/Button;

.field protected button_cha_customer_1:Landroid/widget/Button;

.field protected button_cha_customer_2:Landroid/widget/Button;

.field protected button_cha_customer_3:Landroid/widget/Button;

.field protected button_cha_customer_4:Landroid/widget/Button;

.field protected button_cha_customer_5:Landroid/widget/Button;

.field protected button_cha_customer_6:Landroid/widget/Button;

.field protected button_cha_customer_7:Landroid/widget/Button;

.field protected button_cha_customer_8:Landroid/widget/Button;

.field protected button_cha_customer_9:Landroid/widget/Button;

.field private customerID:Lcom/konka/factory/CustomerIDActivity;

.field protected text_cha_hint_currentpage_val:Landroid/widget/TextView;

.field protected text_cha_hint_totalpage_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0
    .param p1    # Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    return-void
.end method

.method public constructor <init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0
    .param p1    # Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    return-void
.end method


# virtual methods
.method findViewsForAutoTuning()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a001d    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_australia

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a001e    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_austria

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a001f    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_beligum

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0020    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_bulgaral

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0021    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_croatia

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0022    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_czech

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0023    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_denmark

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0024    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_finland

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0025    # com.konka.factory.R.id.button_cha_autotuning_choosecountry_france

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0027    # com.konka.factory.R.id.textview_cha_hint_currentpage

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->autotune:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v1, 0x7f0a0028    # com.konka.factory.R.id.textview_cha_hint_totalpage

    invoke-virtual {v0, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    return-void
.end method

.method findViewsForCustomerID()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002a    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_one

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002b    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_two

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002c    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_three

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002d    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_four

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002e    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_five

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a002f    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_six

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a0030    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_seven

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a0031    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_eight

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a0032    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_nine

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a0027    # com.konka.factory.R.id.textview_cha_hint_currentpage

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ViewHolder;->customerID:Lcom/konka/factory/CustomerIDActivity;

    const v1, 0x7f0a0028    # com.konka.factory.R.id.textview_cha_hint_totalpage

    invoke-virtual {v0, v1}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    return-void
.end method
