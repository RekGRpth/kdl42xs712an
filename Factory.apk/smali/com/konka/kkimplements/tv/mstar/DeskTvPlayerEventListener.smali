.class public Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskTvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;


# static fields
.field private static tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public on4k2kHDMIDisableDualView(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePip(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePop(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisableTravelingMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onEpgUpdateList(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z
    .locals 1
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;

    const/4 v0, 0x0

    return v0
.end method

.method public onPopupDialog(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramReady(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyCiPlusProtection(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyCiPlusRetentionLimitUpdate(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyOverRun(I)Z
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const-string v3, "TvApp"

    const-string v4, "onPvrOverRun in DeskTvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_OVER_RUN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    if-eq p1, v3, :cond_1

    const-string v3, "TvApp"

    const-string v4, "!!!!onPvrOverRun msg match error"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_OVER_RUN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onPvrNotifyParentalControl(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyPlaybackBegin(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyPlaybackSpeedChange(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyPlaybackStop(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "qhc1"

    const-string v3, "onPvrNotifyPlaybackStop in DeskTvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackTime(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyRecordSize(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyRecordStop(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyRecordTime(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyTimeShiftOverwritesAfter(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyTimeShiftOverwritesBefore(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyUsbRemoved(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "qhc1"

    const-string v3, "onPvrNotifyUsbRemoved in DeskTvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onScreenSaverMode(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    if-eq p1, v2, :cond_1

    const-string v2, "TvApp"

    const-string v3, "!!!!onScreenSaverMode msg match error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Status"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    if-eq p1, v2, :cond_1

    const-string v2, "TvApp"

    const-string v3, "!!!!onSignalLock msg match error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    if-eq p1, v2, :cond_1

    const-string v2, "TvApp"

    const-string v3, "!!!!onSignalUnLock msg match error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onTvProgramInfoReady(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method
