.class public Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;
.super Ljava/lang/Object;
.source "DataBaseDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/DataBaseDesk;


# static fields
.field private static dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;


# instance fields
.field private DefOSDLanguageTmp:Ljava/lang/String;

.field private DefTuningCountryTmp:I

.field private HotelEnabledTmp:I

.field private IsRestoreNeededTmp:I

.field private final TAG:Ljava/lang/String;

.field public astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

.field blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

.field colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private cr:Landroid/content/ContentResolver;

.field customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

.field private factoryCusSchema:Ljava/lang/String;

.field private factorySchema:Ljava/lang/String;

.field mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

.field mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

.field mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

.field mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

.field m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_bADCAutoTune:Z

.field m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

.field m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

.field m_stCISet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

.field m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

.field m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

.field m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

.field m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

.field m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

.field soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

.field stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

.field stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

.field stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

.field stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

.field stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

.field private tableDirtyFlags:[Ljava/lang/Boolean;

.field private userSettingCusSchema:Ljava/lang/String;

.field private userSettingSchema:Ljava/lang/String;

.field videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/16 v4, 0x58

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.konka.kkimplements.tv.mstar.DataBaseDeskImpl"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v0, "content://mstar.tv.usersetting"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.usersetting"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    const-string v0, "content://mstar.tv.factory"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factorySchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.factory"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    iput v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefTuningCountryTmp:I

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefOSDLanguageTmp:Ljava/lang/String;

    iput v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsRestoreNeededTmp:I

    iput v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->HotelEnabledTmp:I

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v0, v2}, [I

    move-result-object v0

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v0, v2}, [I

    move-result-object v0

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v0, v2}, [I

    move-result-object v0

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    filled-new-array {v0, v2}, [I

    move-result-object v0

    const-class v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->context:Landroid/content/Context;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n context  \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n getContentResolver cr  \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\n getInstance   com \n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarPicture()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->InitSettingVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initCECVar()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarSound()Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->initVarFactory()Z

    new-array v0, v4, [Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    move v0, v1

    :goto_0
    if-lt v0, v4, :cond_0

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->openDB()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->loadEssentialDataFromDB()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private InitSettingVar()Z
    .locals 11

    const-wide/16 v4, 0x0

    const/4 v3, 0x2

    const/4 v10, 0x1

    const/16 v1, 0x80

    const/4 v8, 0x0

    const-string v0, "TvService"

    const-string v2, "SettingServiceImpl InitVar!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->checkSum:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDSMATV:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->EN_SATEPF_HDPLUS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v4, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;->MS_SUPER_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bTeletext:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bAudioCloseBacklight:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bLcnArrange:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEarphoneSoundOnly:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DvbMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v4, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;->MS_OFFLINE_DET_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bBlueScreen:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Music:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;->MS_POWERON_LOGO_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Logo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->StickerDemo:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ssuSaveMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECFunctionMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECAutoStandbyMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECAutoPoweroffMode:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECARCStatus:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->MSGState:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v8, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ModeVal:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    move v7, v8

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    if-lt v7, v0, :cond_0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-direct {v0, v1, v2, v8, v8}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;-><init>(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;ZZ)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    invoke-direct {v0, v8, v8, v8}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;-><init>(III)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return v10

    :cond_0
    iget-object v9, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    aput-object v0, v9, v7

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0
.end method

.method public static getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    if-nez v0, :cond_0

    const-string v0, "KKJAVAAPI"

    const-string v1, "create databasedesk now"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    const-string v0, "KKJAVAAPI"

    const-string v1, "DataBaseDeskImpl "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->openDB()V

    const-string v0, "KKJAVAAPI"

    const-string v1, "open DB"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    return-object v0
.end method

.method private initCECVar()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    const v1, 0xffff

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;-><init>(ISSSS)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stCECPara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method private initVarFactory()Z
    .locals 12

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_bADCAutoTune:Z

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v8, :cond_0

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v8, :cond_2

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v2, 0x0

    :goto_2
    if-lt v2, v8, :cond_4

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v8

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v3, 0x0

    :goto_3
    if-lt v3, v8, :cond_6

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stCISet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    new-instance v10, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;-><init>()V

    iput-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const/4 v10, 0x1

    return v10

    :cond_0
    const/4 v4, 0x0

    :goto_4
    if-lt v4, v9, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v0

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_2
    const/4 v5, 0x0

    :goto_5
    if-lt v5, v9, :cond_3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_4
    const/4 v6, 0x0

    :goto_6
    if-lt v6, v9, :cond_5

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v2

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_6
    const/4 v7, 0x0

    :goto_7
    if-lt v7, v9, :cond_7

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    iget-object v10, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v3

    new-instance v11, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_7
.end method

.method private initVarPicture()Z
    .locals 17

    const/4 v1, 0x7

    new-array v14, v1, [[S

    const/4 v1, 0x0

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_0

    aput-object v2, v14, v1

    const/4 v1, 0x1

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_1

    aput-object v2, v14, v1

    const/4 v1, 0x2

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_2

    aput-object v2, v14, v1

    const/4 v1, 0x3

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_3

    aput-object v2, v14, v1

    const/4 v1, 0x4

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_4

    aput-object v2, v14, v1

    const/4 v1, 0x5

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_5

    aput-object v2, v14, v1

    const/4 v1, 0x6

    const/4 v2, 0x5

    new-array v2, v2, [S

    fill-array-data v2, :array_6

    aput-object v2, v14, v1

    new-instance v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const v2, 0xffff

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->CheckSum:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-array v2, v15, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    const/4 v1, 0x0

    move v13, v1

    :goto_0
    if-lt v13, v15, :cond_0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-array v3, v2, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    iput-object v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const v3, 0xffff

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;-><init>(ISS)V

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_FULL_HD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->MAPI_VIDEO_OUT_VE_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->E_MAPI_VIDEOSTANDARD_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;->E_AUDIOMODE_MONO_:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->MS_FILM_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;->DB_ThreeD_Video_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->DB_ThreeD_Video_3DDEPTH_LEVEL_15:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->DB_ThreeD_Video_3DOFFSET_LEVEL_15:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    sget-object v8, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->DB_ThreeD_Video_AUTOSTART_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_FULLSCREEN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-direct/range {v1 .. v10}, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;-><init>(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)V

    iput-object v1, v11, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;-><init>(SSSS)V

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->DISPLAY_TVFORMAT_16TO9HD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->SKIN_TONE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    new-instance v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/16 v2, 0x80

    const/16 v3, 0x80

    const/16 v4, 0x80

    const/16 v5, 0x80

    const/16 v6, 0x80

    const/16 v7, 0x80

    invoke-direct/range {v1 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->colorParaEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v1, 0x1

    return v1

    :cond_0
    const/4 v1, 0x0

    aget-object v2, v14, v13

    const/4 v4, 0x1

    aget-short v3, v2, v1

    aget-object v1, v14, v13

    const/4 v2, 0x2

    aget-short v4, v1, v4

    aget-object v1, v14, v13

    const/4 v6, 0x3

    aget-short v5, v1, v2

    aget-object v1, v14, v13

    const/4 v2, 0x4

    aget-short v6, v1, v6

    aget-object v1, v14, v13

    aget-short v7, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    move-object/from16 v16, v0

    new-instance v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    const/16 v2, 0x64

    sget-object v8, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v10, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v11, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v12, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-direct/range {v1 .. v12}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;-><init>(SSSSSSLcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;)V

    aput-object v1, v16, v13

    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    new-instance v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-direct {v4, v5, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;-><init>(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    nop

    :array_0
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_1
    .array-data 2
        0x3cs
        0x37s
        0x3cs
        0x3cs
        0x32s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x28s
        0x2ds
        0x2ds
        0x28s
        0x32s
    .end array-data

    nop

    :array_3
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_4
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_5
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_6
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data
.end method

.method private initVarSound()Z
    .locals 15

    const/16 v14, 0x1e

    const/4 v13, 0x1

    const/16 v12, 0x3c

    const/16 v11, 0x28

    const/16 v1, 0x32

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->u16CheckSum:I

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v3, 0x46

    const/16 v5, 0x46

    move v4, v11

    move v6, v12

    move v7, v1

    move v8, v1

    move v9, v11

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v13

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x2

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v7, 0x2d

    move v3, v12

    move v4, v14

    move v5, v12

    move v6, v1

    move v8, v11

    move v9, v14

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x3

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/16 v4, 0x50

    const/16 v6, 0x2d

    const/16 v9, 0x50

    move v3, v11

    move v5, v11

    move v7, v1

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x4

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x5

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x6

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    return v13
.end method

.method private static openDB()V
    .locals 0

    return-void
.end method

.method private readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v2, p2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public loadEssentialDataFromDB()V
    .locals 7

    const/4 v2, 0x0

    const/16 v6, 0x22

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enInputSourceType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryADCAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryExtern()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySSCAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserLocSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSubtitleSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUsrColorTmpExData()[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySRSAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCustomerCfgMiscSetting()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryBlockSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    return-void

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method public queryADCAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/adcadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    array-length v2, v2

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v3, v2, -0x1

    if-gt v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16RedGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redgain:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16GreenGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greengain:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16BlueGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->bluegain:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16RedOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redoffset:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16GreenOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greenoffset:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v3, v3, v0

    const-string v4, "u16BlueOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->blueoffset:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public queryAllVideoPara(I)Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;
    .locals 9

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v3, " InputSrcType = ? "

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/picmode_setting"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "PictureModeType"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    array-length v3, v0

    move v0, v6

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_9

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v3, " InputSrcType = ? "

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/nrmode"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "NRMode"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    array-length v1, v1

    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_a

    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    move-result-object v3

    const-string v4, "eThreeDVideo"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    const-string v0, "eThreeDVideo3DDepth"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ltz v0, :cond_2

    const/16 v3, 0x1f

    if-le v0, v3, :cond_3

    :cond_2
    const/16 v0, 0xf

    :cond_3
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-result-object v4

    aget-object v0, v4, v0

    iput-object v0, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-result-object v3

    const-string v4, "eThreeDVideo3DOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    move-result-object v3

    const-string v4, "eThreeDVideoAutoStart"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    move-result-object v3

    const-string v4, "eThreeDVideo3DOutputAspect"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v3

    const-string v4, "eThreeDVideoLRViewSwitch"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v3

    const-string v4, "eThreeDVideoSelfAdaptiveDetect"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    :cond_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/useroverscanmode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHposition"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHposition:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVposition"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVposition:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHRatio"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHRatio:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVRatio"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVRatio:S

    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    return-object v0

    :cond_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v3

    const-string v4, "ePicture"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v3

    const-string v4, "enARCType"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-result-object v3

    const-string v4, "fOutput_RES"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-result-object v3

    const-string v4, "tvsys"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v3

    const-string v4, "LastVideoStandardMode"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-result-object v3

    const-string v4, "LastAudioStandardMode"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-result-object v3

    const-string v4, "eDynamic_Contrast"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    move-result-object v3

    const-string v4, "eFilm"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-result-object v3

    const-string v4, "eTvFormat"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    move-result-object v3

    const-string v4, "skinTone"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->skinTone:Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    const-string v0, "detailEnhance"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v6

    :goto_3
    iput-boolean v0, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->detailEnhance:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v3

    const-string v4, "DNR"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->DNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v3, "u8SubBrightness"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubBrightness:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v3, "u8SubContrast"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubContrast:S

    goto/16 :goto_0

    :cond_8
    move v0, v7

    goto :goto_3

    :cond_9
    add-int/lit8 v5, v3, -0x1

    if-gt v0, v5, :cond_0

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Backlight"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Contrast"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Brightness"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Saturation"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Sharpness"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    const-string v7, "u8Hue"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v7

    const-string v8, "eColorTemp"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v7

    const-string v8, "eVibrantColour"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v7

    const-string v8, "ePerfectClear"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v7

    const-string v8, "eDynamicContrast"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v5, v5, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v7

    const-string v8, "eDynamicBacklight"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    aget-object v7, v7, v8

    iput-object v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_a
    add-int/lit8 v3, v1, -0x1

    if-gt v6, v3, :cond_1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v3, v3, v6

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v4

    const-string v5, "eNR"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    aget-object v4, v4, v5

    iput-object v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->videopara:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v3, v3, v6

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    move-result-object v4

    const-string v5, "eMPEG_NR"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    aget-object v4, v4, v5

    iput-object v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2
.end method

.method public queryBlockSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/blocksyssetting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const-string v4, "u8BlockSysLockMode"

    aput-object v4, v2, v6

    const-string v4, "u8UnratedLoack"

    aput-object v4, v2, v7

    const-string v4, "u8VideoBlockMode"

    aput-object v4, v2, v8

    const-string v4, "u8BlockSysPWSetStatus"

    aput-object v4, v2, v9

    const/4 v4, 0x5

    const-string v5, "u8ParentalControl"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "u8EnterLockPage"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "u16BlockSysPassword"

    aput-object v5, v2, v4

    const/16 v4, 0x8

    const-string v5, "u8LockKeypad"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysLockMode:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->unrateLock:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->videoBlockMode:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPWSetStatus:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->parentalControl:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->enterLockPage:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockSysPassword:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    const/16 v2, 0x8

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;->blockchildlockMode:I

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->blockSysSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;

    return-object v0
.end method

.method public queryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/cusdefsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "u8DefTuningCountry"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefTuningCountryTmp:I

    const-string v0, "strDefOSDLanguage"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v0, "u8IsRestoreNeeded"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsRestoreNeededTmp:I

    const-string v0, "u8HotelEnabled"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->HotelEnabledTmp:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    const-string v2, "u8PowerOffLogoEnabled"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoEnabled:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    const-string v2, "iPowerOffLogoDspTime"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoDspTime:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    const-string v2, "bTeletext"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsTeletextEnabled:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    const-string v2, "iAudioDelayTime"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->AudioDelayTime:I

    const-string v0, "trace factory cus def"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DefTuningCountryTmp:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefTuningCountryTmp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",DefOSDLanguageTmp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefOSDLanguageTmp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",IsRestoreNeededTmp:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsRestoreNeededTmp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",HotelEnabledTmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->HotelEnabledTmp:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefTuningCountryTmp:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    :goto_0
    const/16 v0, 0xb

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "en_US"

    aput-object v0, v2, v6

    const/4 v0, 0x1

    const-string v3, "ru_RU"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "fr_FR"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "ar_EG"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "iw_IL"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    const-string v3, "in_ID"

    aput-object v3, v2, v0

    const/4 v0, 0x6

    const-string v3, "fa_IR"

    aput-object v3, v2, v0

    const/4 v0, 0x7

    const-string v3, "kd_KD"

    aput-object v3, v2, v0

    const/16 v0, 0x8

    const-string v3, "tr_TR"

    aput-object v3, v2, v0

    const/16 v0, 0x9

    const-string v3, "th_TH"

    aput-object v3, v2, v0

    const/16 v0, 0xa

    const-string v3, "zh_CN"

    aput-object v3, v2, v0

    move v0, v6

    :goto_1
    array-length v3, v2

    if-lt v0, v3, :cond_3

    :goto_2
    array-length v2, v2

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    const-string v2, "en_US"

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    :cond_0
    iget v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsRestoreNeededTmp:I

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    :goto_3
    iget v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->HotelEnabledTmp:I

    if-gez v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    :cond_1
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefTuningCountryTmp:I

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->DefOSDLanguageTmp:Ljava/lang/String;

    aget-object v4, v2, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    aget-object v4, v2, v0

    iput-object v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsRestoreNeededTmp:I

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->HotelEnabledTmp:I

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    goto :goto_4
.end method

.method public queryCustomerCfgMiscSetting()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/miscsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "getColumnIndex=%x\n"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "EnergyEnable"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "getInt=%x\n"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "EnergyEnable"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const-string v0, "EnergyEnable"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->customerCfgMiscSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;

    const-string v2, "EnergyPercent"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;->energyPercent:S

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-void

    :cond_1
    move v0, v7

    goto :goto_0
.end method

.method public queryFactoryColorTempData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;
    .locals 6

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/factorycolortemp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "ColorTemperatureID"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    array-length v2, v2

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    return-object v0

    :cond_1
    add-int/lit8 v3, v2, -0x1

    if-gt v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8RedGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8GreenGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8BlueGain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8RedOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8GreenOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v3, v3, v0

    const-string v4, "u8BlueOffset"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method

.method public queryFactoryColorTempExData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;
    .locals 8

    const/4 v6, 0x0

    const-string v0, ""

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    move v7, v6

    :goto_0
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v0

    if-lt v7, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    return-object v0

    :cond_0
    const-string v3, " InputSourceID = ? "

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/factorycolortempex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "ColorTemperatureID"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v6

    :goto_1
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    if-lt v0, v2, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16RedGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16GreenGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16BlueGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16RedOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16GreenOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v7

    const-string v3, "u16BlueOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public queryFactoryExtern()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/factoryextern"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SoftWareVersion"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    const-string v0, "BoardType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    const-string v0, "PanelType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    const-string v0, "CompileTime"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "TestPatternMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "stPowerMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "DtvAvAbnormalDelay"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "FactoryPreSetFeature"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "PanelSwing"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "AudioPrescale"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "vdDspVersion"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "eHidevMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "audioNrThr"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "audioSifThreshold"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "audioDspVersion"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "bBurnIn"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v2, "m_bAgingMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stFactoryExt:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    return-object v0

    :cond_1
    move v0, v7

    goto/16 :goto_0

    :cond_2
    move v0, v7

    goto :goto_1

    :cond_3
    move v6, v7

    goto :goto_2
.end method

.method public queryNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D4"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D4:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D5_Bit2"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D5_Bit2:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D8_Bit3210"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D8_Bit3210:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D9_Bit0"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D9_Bit0:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D7_LOW_BOUND"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_LOW_BOUND:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_D7_HIGH_BOUND"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_HIGH_BOUND:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_A0"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A0:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_A1"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A1:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_66_Bit76"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_66_Bit76:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_6E_Bit7654"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit7654:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_6E_Bit3210"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit3210:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_44"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_44:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-string v2, "u8AFEC_CB"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_CB:S

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mNoStandSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    return-object v0
.end method

.method public queryNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifTop"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifVgaMaximum"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKp"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKi"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKp1"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKi1"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKp2"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrKi2"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "VifAsiaSignalOption"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "VifCrKpKiAdjust"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifOverModulation"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifClampgainGainOvNegative"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "ChinaDescramblerBox"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifDelayReduce"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifCrThr"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifVersion"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "VifACIAGCREF"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v2, "GainDistributionThr"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mVifSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    return-object v0

    :cond_1
    move v0, v7

    goto/16 :goto_0

    :cond_2
    move v0, v7

    goto/16 :goto_1

    :cond_3
    move v6, v7

    goto :goto_2
.end method

.method public queryNonLinearAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;
    .locals 7

    const/4 v6, 0x0

    const-string v3, " InputSrcType = ? "

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/nonlinearadjust"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "CurveTypeIndex"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    array-length v2, v0

    move v0, v6

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v3, v2, -0x1

    if-gt v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v0

    const-string v4, "u8OSD_V0"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v0

    const-string v4, "u8OSD_V25"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v0

    const-string v4, "u8OSD_V50"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v0

    const-string v4, "u8OSD_V75"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_pastNLASet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v0

    const-string v4, "u8OSD_V100"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public queryOverscanAdjusts(I)[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;
    .locals 8

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v2

    :pswitch_0
    const-string v0, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, " FactoryOverScanType = ? "

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v3

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v4

    move v1, v6

    :goto_1
    if-lt v1, v3, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto :goto_0

    :cond_0
    move v0, v6

    :goto_2
    if-lt v0, v4, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16H_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16V_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Left"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Right"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Up"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Down"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_1
    const-string v0, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, " FactoryOverScanType = ? "

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v3

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v4

    move v1, v6

    :goto_3
    if-lt v1, v3, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_3
    move v0, v6

    :goto_4
    if-lt v0, v4, :cond_4

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16H_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16V_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Left"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Right"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Up"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Down"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_2
    const-string v0, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, " FactoryOverScanType = ? "

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v3

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v4

    move v1, v6

    :goto_5
    if-lt v1, v3, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_6
    move v0, v6

    :goto_6
    if-lt v0, v4, :cond_7

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16H_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16V_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Left"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Right"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Up"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Down"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :pswitch_3
    const-string v0, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, " FactoryOverScanType = ? "

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v3

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v4

    move v1, v6

    :goto_7
    if-lt v1, v3, :cond_9

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    goto/16 :goto_0

    :cond_9
    move v0, v6

    :goto_8
    if-lt v0, v4, :cond_a

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_a
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16H_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u16V_CapStart"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Left"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8HCrop_Right"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Up"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v5, v5, v1

    aget-object v5, v5, v0

    const-string v7, "u8VCrop_Down"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public queryPEQAdjusts()Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/peqadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    array-length v2, v2

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    return-object v0

    :cond_1
    add-int/lit8 v3, v2, -0x1

    if-gt v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v0

    const-string v4, "Band"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v0

    const-string v4, "Gain"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v0

    const-string v4, "Foh"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v0

    const-string v4, "Fol"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_stPEQSet:Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v0

    const-string v4, "QValue"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public querySRSAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/srsadjust"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iInputGain"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iSurrLevelCtl"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iSpeakerAudio"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iSpeakerAnalysis"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iTrubassCtl"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iDCCtl"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    const-string v2, "iDefinitionCtl"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSRSSet:Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    return-object v0
.end method

.method public querySSCAdjust()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v1, "content://mstar.tv.factory/sscadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "Miu_SscEnable"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Lvds_SscEnable"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Lvds_SscSpan"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Lvds_SscStep"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu_SscSpan"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu_SscStep"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu1_SscSpan"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu1_SscStep"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu2_SscSpan"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v2, "Miu2_SscStep"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->mSscSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    return-object v0

    :cond_1
    move v0, v7

    goto/16 :goto_0

    :cond_2
    move v6, v7

    goto :goto_1
.end method

.method public querySoundModeSettings()[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/soundmodesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    array-length v3, v0

    move v0, v6

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    return-object v0

    :cond_1
    add-int/lit8 v1, v3, -0x1

    if-gt v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "Bass"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "Treble"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand1"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand2"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand3"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand4"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand5"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand6"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "EqBand7"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v4, v1, v0

    const-string v1, "UserMode"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    move v1, v6

    :goto_1
    iput-boolean v1, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    const-string v4, "Balance"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v4

    const-string v5, "enSoundAudioChannel"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    aget-object v4, v4, v5

    iput-object v4, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
    .locals 9

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/soundsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v2

    const-string v3, "SoundMode"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    move-result-object v2

    const-string v3, "AudysseyDynamicVolume"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyDynamicVolume:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    move-result-object v2

    const-string v3, "AudysseyEQ"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyEQ:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    move-result-object v2

    const-string v3, "SurroundSoundMode"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundSoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    move-result-object v2

    const-string v3, "Surround"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v0, "bEnableAVC"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "Volume"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "HPVolume"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->HPVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "Balance"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "Primary_Flag"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Primary_Flag:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v2, "enSoundAudioLan1"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v0, v0, v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v2

    const-string v3, "enSoundAudioLan2"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "MUTE_Flag"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->MUTE_Flag:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v2

    const-string v3, "enSoundAudioChannel"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "bEnableAD"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_8

    :goto_3
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAD:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "ADVolume"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    move-result-object v2

    const-string v3, "ADOutput"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADOutput:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "SPDIF_Delay"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SPDIF_Delay:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v2, "Speaker_Delay"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Speaker_Delay:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    const-string v3, "hdmi1AudioSource"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    const-string v3, "hdmi2AudioSource"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    const-string v3, "hdmi3AudioSource"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    const-string v3, "hdmi4AudioSource"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-object v0

    :cond_1
    move v0, v7

    goto/16 :goto_0

    :cond_2
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_3
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "enSoundAudioLan1"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/soundsetting"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v0, v5, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v3, v0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v3, "update failed"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_6
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_7

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enSoundAudioLan2"

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->soundpara:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/soundsetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    int-to-long v2, v0

    goto/16 :goto_2

    :catch_1
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v2, "update failed"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_8
    move v6, v7

    goto/16 :goto_3
.end method

.method public queryUserLocSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/userlocationsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v2, "u16LocationNo"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mLocationNo:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v2, "s16ManualLongitude"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLongitude:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUserLocationSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v2, "s16ManualLatitude"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLatitude:I

    goto :goto_0
.end method

.method public queryUserSubtitleSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
    .locals 9

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/subtitlesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v2, "SubtitleDefaultLanguage"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v0, v0, v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v2

    const-string v3, "SubtitleDefaultLanguage_2"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v0, "fHardOfHearing"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v6

    :goto_2
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v2, "fEnableSubTitle"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_8

    :goto_3
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    return-object v0

    :cond_1
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v3, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "SubtitleDefaultLanguage"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/subtitlesetting"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v0, v5, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v3, v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v3, "update failed"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v2, v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stSubtitleSet:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "SubtitleDefaultLanguage_2"

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/subtitlesetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    int-to-long v2, v0

    goto/16 :goto_1

    :catch_1
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v2, "update failed"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    move v0, v7

    goto/16 :goto_2

    :cond_8
    move v6, v7

    goto/16 :goto_3
.end method

.method public queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/systemsetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const-string v0, "CN"

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fRunInstallationGuide"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fNoChannel"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bDisableSiAutoUpdate"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    :goto_2
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    const-string v3, "enInputSourceType"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v2

    const-string v3, "Country"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    move-result-object v2

    const-string v3, "enCableOperators"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    move-result-object v2

    const-string v3, "enSatellitePlatform"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v2, "Language"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "/system/build.prop"

    const-string v3, "persist.sys.country"

    invoke-direct {p0, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->readValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_3
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    move-result-object v2

    const-string v3, "enSPDIFMODE"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "fSoftwareUpdate"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fSoftwareUpdate:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "U8OADTime"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "fOADScanAfterWakeup"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "fAutoVolume"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "fDcPowerOFFMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "DtvRoute"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "ScartOutRGB"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "U8Transparency"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u32MenuTimeOut"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "AudioOnly"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "bEnableWDT"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u8FavoriteRegion"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u8Bandwidth"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u8TimeShiftSizeType"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "fOadScan"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "bEnablePVRRecordAll"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u8ColorRangeMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u8HDMIAudioSource"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "bEnableAlwaysTimeshift"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    move-result-object v2

    const-string v3, "eSUPER"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bUartBus"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v6

    :goto_4
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bTeletext"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v6

    :goto_5
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bTeletext:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bMicOn"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_a

    move v0, v6

    :goto_6
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bAudioCloseBacklight"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_b

    move v0, v6

    :goto_7
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bAudioCloseBacklight:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bLcnArrange"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_c

    move v0, v6

    :goto_8
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bLcnArrange:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bEarphoneSoundOnly"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_d

    move v0, v6

    :goto_9
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEarphoneSoundOnly:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "DvbMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DvbMode:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "m_AutoZoom"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bOverScan"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_e

    move v0, v6

    :goto_a
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "m_u8BrazilVideoStandardType"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "m_u8SoftwareUpdateMode"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "OSD_Active_Time"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "m_MessageBoxExist"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_f

    move v0, v6

    :goto_b
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "u16LastOADVersion"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bEnableAutoChannelUpdate"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_10

    move v0, v6

    :goto_c
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "standbyNoOperation"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoOperation:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "standbyNoSignal"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_11

    move v0, v6

    :goto_d
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->standbyNoSignal:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bMSGState"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_12

    move v0, v6

    :goto_e
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->MSGState:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "bModeState"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ModeVal:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "screenSaveMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_13

    move v0, v6

    :goto_f
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->screenSaveMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bStickerDemo"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_14

    move v0, v6

    :goto_10
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->StickerDemo:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bDemoMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_15

    move v0, v6

    :goto_11
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDemoMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "ssuSaveMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_16

    move v0, v6

    :goto_12
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ssuSaveMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "cecFunctionMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_17

    move v0, v6

    :goto_13
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECFunctionMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "cecAutoStandbyMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_18

    move v0, v6

    :goto_14
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECAutoStandbyMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "cecAutoPoweroffMode"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_19

    move v0, v6

    :goto_15
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECAutoPoweroffMode:Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "cecARCEnable"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1a

    move v0, v6

    :goto_16
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->CECARCStatus:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "U8SystemAutoTimeType"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8SystemAutoTimeType:I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v2

    const-string v3, "smartEnergySaving"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    move-result-object v2

    const-string v3, "colorWheelMode"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bMemc"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1b

    move v0, v6

    :goto_17
    iput-boolean v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMemcEnable:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v2, "bAlTimeShift"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_1c

    :goto_18
    iput-boolean v6, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bAlTimeshift:Z

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    return-object v0

    :cond_1
    move v0, v7

    goto/16 :goto_0

    :cond_2
    move v0, v7

    goto/16 :goto_1

    :cond_3
    move v0, v7

    goto/16 :goto_2

    :cond_4
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_5
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "Language"

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/systemsetting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v2, v0

    goto/16 :goto_3

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v2, "update failed"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrData:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object v0, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_8
    move v0, v7

    goto/16 :goto_4

    :cond_9
    move v0, v7

    goto/16 :goto_5

    :cond_a
    move v0, v7

    goto/16 :goto_6

    :cond_b
    move v0, v7

    goto/16 :goto_7

    :cond_c
    move v0, v7

    goto/16 :goto_8

    :cond_d
    move v0, v7

    goto/16 :goto_9

    :cond_e
    move v0, v7

    goto/16 :goto_a

    :cond_f
    move v0, v7

    goto/16 :goto_b

    :cond_10
    move v0, v7

    goto/16 :goto_c

    :cond_11
    move v0, v7

    goto/16 :goto_d

    :cond_12
    move v0, v7

    goto/16 :goto_e

    :cond_13
    move v0, v7

    goto/16 :goto_f

    :cond_14
    move v0, v7

    goto/16 :goto_10

    :cond_15
    move v0, v7

    goto/16 :goto_11

    :cond_16
    move v0, v7

    goto/16 :goto_12

    :cond_17
    move v0, v7

    goto/16 :goto_13

    :cond_18
    move v0, v7

    goto/16 :goto_14

    :cond_19
    move v0, v7

    goto/16 :goto_15

    :cond_1a
    move v0, v7

    goto/16 :goto_16

    :cond_1b
    move v0, v7

    goto/16 :goto_17

    :cond_1c
    move v6, v7

    goto/16 :goto_18
.end method

.method public queryUsrColorTmpData()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/usercolortemp"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    return-object v0

    :cond_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8RedGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8GreenGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8BlueGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8RedOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8GreenOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v3, "u8BlueOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public queryUsrColorTmpExData()[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/usercolortempex"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    return-object v0

    :cond_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gt v0, v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16RedGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16GreenGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16BlueGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16RedOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16GreenOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    const-string v3, "u16BlueOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateCUS_DEF_SETTING()V

    const/4 v0, 0x0

    return v0
.end method

.method public updateCUS_DEF_SETTING()V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "u8DefTuningCountry"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "strDefOSDLanguage"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "u8IsRestoreNeeded"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u8HotelEnabled"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u8PowerOffLogoEnabled"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoEnabled:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "iPowerOffLogoDspTime"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoDspTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "bTeletext"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsTeletextEnabled:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "iAudioDelayTime"

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->m_cusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->AudioDelayTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->cr:Landroid/content/ContentResolver;

    const-string v4, "content://mstar.tv.factory/cusdefsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x34

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
