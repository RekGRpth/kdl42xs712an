.class public Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;
.super Ljava/lang/Object;
.source "DeskCaEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;
    }
.end annotation


# static fields
.field private static caEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->caEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method public static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->caEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->caEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->caEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    return-object v0
.end method


# virtual methods
.method public onActionRequest(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_ACTION_REQUEST:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onDetitleReceived(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_DETITLE_RECEVIED:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onEmailNotifyIcon(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_EMAIL_NOTIFY_ICON:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onEntitleChanged(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_ENTITLE_CHANGED:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onHideIPPVDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_HIDE_IPPV_DLG:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onHideOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_HIDE_OSD_MESSAGE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaLockService;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mstar/android/tvapi/dtv/vo/CaLockService;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_LOCKSERVICE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iput-object p5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onOtaState(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_OTASTATE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onRequestFeeding(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_REQUEST_FEEDING:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onShowBuyMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    const-string v2, "DeskCaEventListener"

    const-string v3, "//////////////////////////////////EV_CA_SHOW_BUY_MESSAGE/////////////////////////////////////////////////"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_SHOW_BUY_MESSAGE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageFrom"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onShowFingerMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_SHOW_FINGER_MESSAGE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onShowOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILjava/lang/String;)Z
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v5

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_SHOW_OSD_MESSAGE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "StringType"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onShowOSDMessage:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onShowProgressStrip(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_SHOW_PROGRESS_STRIP:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onStartIppvBuyDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_START_IPPV_BUY_DLG:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iput-object p5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUNLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->EV_CA_LOCKSERVICE:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener$CA_EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "MessageType"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "MessageType2"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
