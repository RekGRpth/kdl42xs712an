.class public Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;
.super Ljava/lang/Object;
.source "DeskTvEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;


# static fields
.field private static tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;


# instance fields
.field private final SYNC_TIME_ANDROID:I

.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    const/16 v0, 0x789

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->SYNC_TIME_ANDROID:I

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->tvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    return-object v0
.end method


# virtual methods
.method public on4k2kHDMIDisableDualView(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePip(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePop(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisableTravelingMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onAtscPopupDialog(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onDeadthEvent(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onDtvReadyPopupDialog(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onScartMuteOsdMode(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onScreenSaverMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const-string v2, "TvApp"

    const-string v3, "onSignalLock in DeskTvEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSignalUnlock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const-string v2, "TvApp"

    const-string v3, "onSignalUnLock in DeskTvEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskTvEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUnityEvent(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v3, 0x789

    const/4 v2, 0x0

    if-ne p2, v3, :cond_0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
