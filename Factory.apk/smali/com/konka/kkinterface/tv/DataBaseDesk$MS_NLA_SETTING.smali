.class public Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_NLA_SETTING"
.end annotation


# instance fields
.field public msNlaSetIndex:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;

.field public stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    new-array v1, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    iput-object v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;->EN_NLA_VOLUME:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iput-object v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
