.class public interface abstract Lcom/konka/kkinterface/tv/CommonDesk;
.super Ljava/lang/Object;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;,
        Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;,
        Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;,
        Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;,
        Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;,
        Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;,
        Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;,
        Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;
    }
.end annotation


# virtual methods
.method public abstract GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;
.end method

.method public abstract getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;
.end method

.method public abstract getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;
.end method

.method public abstract isHdmiSignalMode()Z
.end method

.method public abstract isSignalStable()Z
.end method
