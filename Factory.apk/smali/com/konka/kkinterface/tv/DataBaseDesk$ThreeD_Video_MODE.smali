.class public Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThreeD_Video_MODE"
.end annotation


# instance fields
.field public eThreeDVideo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

.field public eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

.field public eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

.field public eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

.field public eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

.field public eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

.field public eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

.field public eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;


# direct methods
.method public constructor <init>(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)V
    .locals 0
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;
    .param p2    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .param p3    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .param p4    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    .param p5    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;
    .param p6    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;
    .param p7    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;
    .param p8    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;
    .param p9    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;

    iput-object p2, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iput-object p3, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iput-object p4, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iput-object p5, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    iput-object p6, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    iput-object p7, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    iput-object p8, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    iput-object p9, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-void
.end method
