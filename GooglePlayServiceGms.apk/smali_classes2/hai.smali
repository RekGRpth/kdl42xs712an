.class public final Lhai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgzx;


# static fields
.field private static final a:Ljava/text/NumberFormat;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/DecimalFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lhai;->a:Ljava/text/NumberFormat;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;II)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-gtz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lhai;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V

    return-void

    :cond_0
    packed-switch p2, :pswitch_data_0

    const-string v1, "UsageCartDetailsRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized usage unit: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :pswitch_0
    const v1, 0x7f0b012e    # com.google.android.gms.R.string.wallet_usage_discount

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0002    # com.google.android.gms.R.plurals.wallet_minutes

    invoke-virtual {v4, v5, p3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V
    .locals 4

    const v3, 0x7f0b0127    # com.google.android.gms.R.string.wallet_minute

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhai;->b:Landroid/content/Context;

    iput-object p1, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    packed-switch p2, :pswitch_data_0

    const-string v0, "UsageCartDetailsRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized usage unit: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhai;->d:Ljava/lang/String;

    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p3, p0, Lhai;->e:Ljava/lang/String;

    :goto_1
    return-void

    :pswitch_0
    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhai;->d:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhai;->e:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Liob;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c()V

    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    iget-object v0, p1, Liob;->b:[Liod;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Liob;->b:[Liod;

    aget-object v2, v0, v6

    iget-object v0, v2, Liod;->a:Ljava/lang/String;

    iget-object v1, v2, Liod;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v2, Liod;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    iget-object v0, v2, Liod;->c:Lioh;

    invoke-static {v0}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    const v3, 0x7f04014a    # com.google.android.gms.R.layout.wallet_row_line_item_usage_long

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a034c    # com.google.android.gms.R.id.item_details

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a034e    # com.google.android.gms.R.id.item_price

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lhai;->d:Ljava/lang/String;

    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v2, "\n"

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lhai;->b:Landroid/content/Context;

    const v4, 0x7f0b0128    # com.google.android.gms.R.string.wallet_per_unit

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lhai;->b:Landroid/content/Context;

    const v6, 0x7f100039    # com.google.android.gms.R.style.WalletFineDetailText

    invoke-direct {v4, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v2, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhai;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    const v1, 0x7f040148    # com.google.android.gms.R.layout.wallet_row_line_item_usage_discount_long

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0350    # com.google.android.gms.R.id.item_usage_discount

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lhai;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final b(Liob;)V
    .locals 12

    const v11, 0x7f0a0351    # com.google.android.gms.R.id.item_usage_info

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p1, Liob;->b:[Liod;

    array-length v0, v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Liob;->b:[Liod;

    aget-object v1, v0, v7

    iget-object v0, v1, Liod;->c:Lioh;

    invoke-static {v0}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Liob;->f:Lipt;

    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    const v4, 0x7f0b0123    # com.google.android.gms.R.string.wallet_inclusive_tax_tax

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_1

    iget v4, v3, Lipt;->c:I

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_1
    if-eqz v3, :cond_3

    iget-boolean v3, v3, Lipt;->b:Z

    if-eqz v3, :cond_3

    iget-object v1, p0, Lhai;->b:Landroid/content/Context;

    const v3, 0x7f0b012a    # com.google.android.gms.R.string.wallet_usage_info_tax_included

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v2, v4, v7

    iget-object v2, p0, Lhai;->d:Ljava/lang/String;

    aput-object v2, v4, v8

    aput-object v0, v4, v9

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v11}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lhai;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    const v2, 0x7f040149    # com.google.android.gms.R.layout.wallet_row_line_item_usage_info_long

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v8}, Ljava/util/HashMap;-><init>(I)V

    const-string v3, "rate"

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    iget-object v5, p0, Lhai;->b:Landroid/content/Context;

    const v6, 0x7f100038    # com.google.android.gms.R.style.WalletImportantFineDetailText

    invoke-direct {v4, v5, v6}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v2}, Lgtp;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    const v4, 0x7f0b0121    # com.google.android.gms.R.string.wallet_inclusive_tax_gst

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    const v4, 0x7f0b0122    # com.google.android.gms.R.string.wallet_inclusive_tax_vat

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-wide v3, v1, Liod;->j:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    sget-object v3, Lhai;->a:Ljava/text/NumberFormat;

    iget-wide v4, v1, Liod;->j:J

    invoke-static {v4, v5}, Lgth;->a(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lhai;->b:Landroid/content/Context;

    const v4, 0x7f0b012b    # com.google.android.gms.R.string.wallet_usage_info_tax_excluded

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v7

    iget-object v2, p0, Lhai;->d:Ljava/lang/String;

    aput-object v2, v5, v8

    aput-object v1, v5, v9

    aput-object v0, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhai;->b:Landroid/content/Context;

    const v1, 0x7f0b0129    # com.google.android.gms.R.string.wallet_usage_info

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v2, v3, v7

    iget-object v2, p0, Lhai;->d:Ljava/lang/String;

    aput-object v2, v3, v8

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
