.class public final Lgxj;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Lhgu;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    iput-object p1, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgxj;->b:Landroid/view/LayoutInflater;

    new-instance v0, Lhgu;

    invoke-direct {v0, p2}, Lhgu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgxj;->c:Lhgu;

    iget-object v0, p0, Lgxj;->c:Lhgu;

    invoke-static {p2}, Lhgs;->a(Landroid/content/Context;)Lhgs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgu;->a(Lhgs;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    instance-of v0, p0, Lioj;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lgxi;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13

    invoke-virtual {p0, p1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v9

    if-eqz p3, :cond_0

    invoke-static {v9}, Lgxj;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lgxj;->a(Ljava/lang/Object;)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/16 p3, 0x0

    :cond_0
    iget-object v1, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->isEnabled()Z

    move-result v11

    const/4 v4, 0x0

    instance-of v1, v9, Lioj;

    if-eqz v1, :cond_e

    if-eqz p3, :cond_1

    invoke-static/range {p3 .. p3}, Lgxj;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    if-eqz p2, :cond_7

    iget-object v1, p0, Lgxj;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f040143    # com.google.android.gms.R.layout.wallet_row_instrument_spinner_drop_down

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_2
    :goto_0
    const v1, 0x7f0a0347    # com.google.android.gms.R.id.instrument_description

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a0346    # com.google.android.gms.R.id.instrument_image

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/widget/ImageView;

    move-object v2, v9

    check-cast v2, Lioj;

    if-eqz v11, :cond_8

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lioj;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    move v11, v3

    :goto_1
    iget-object v3, v2, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lgxj;->c:Lhgu;

    invoke-static {v8, v2, v1}, Lgth;->a(Landroid/widget/ImageView;Lioj;Lhgu;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    if-eqz p2, :cond_12

    const v1, 0x7f0a0348    # com.google.android.gms.R.id.address_description

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0a0349    # com.google.android.gms.R.id.phone_description

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v6, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->c(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v2, Lioj;->e:Lipv;

    if-eqz v6, :cond_3

    iget-object v6, v2, Lioj;->e:Lipv;

    iget-object v5, v6, Lipv;->a:Lixo;

    const-string v7, ", "

    const/4 v10, 0x0

    const/4 v12, 0x0

    invoke-static {v5, v7, v10, v12}, Lgvs;->a(Lixo;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v5, 0x1

    iget-object v6, v6, Lipv;->d:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x1

    :cond_3
    if-eqz v5, :cond_a

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    if-eqz v4, :cond_b

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    const v1, 0x7f0a0345    # com.google.android.gms.R.id.error

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {p0}, Lgxj;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->d(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z

    move-result v3

    iget-object v4, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->e(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z

    move-result v4

    iget-object v5, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->f(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)Z

    move-result v5

    iget-object v6, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v6

    iget-object v7, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lgth;->a(Landroid/content/Context;Lioj;ZZZ[I[I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b()Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v3

    invoke-virtual {v8, v3}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    const v1, 0x7f0a034a    # com.google.android.gms.R.id.info_clickable

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v3

    iget-object v4, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v4

    invoke-static {v2, v3, v4}, Lgth;->a(Lioj;[I[I)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    move-object v4, v1

    :cond_4
    :goto_6
    if-eqz p3, :cond_6

    if-nez p2, :cond_5

    iget-object v1, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->g(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)I

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f0a0112    # com.google.android.gms.R.id.label

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->g(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p3

    invoke-static {v0, v11}, Lbqc;->a(Landroid/view/View;Z)V

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_6
    return-object p3

    :cond_7
    iget-object v1, p0, Lgxj;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f040142    # com.google.android.gms.R.layout.wallet_row_instrument_spinner

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_1

    :cond_9
    const/16 v1, 0x8

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_a
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_b
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_c
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_d
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :goto_7
    move-object v4, v1

    goto :goto_6

    :cond_e
    instance-of v1, v9, Lgxi;

    if-eqz v1, :cond_4

    if-eqz p3, :cond_f

    invoke-static/range {p3 .. p3}, Lgxj;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_10

    :cond_f
    if-eqz p2, :cond_11

    iget-object v1, p0, Lgxj;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f04013b    # com.google.android.gms.R.layout.wallet_row_action_item_drop_down

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_10
    :goto_8
    move-object v1, v9

    check-cast v1, Lgxi;

    const v2, 0x7f0a0092    # com.google.android.gms.R.id.description

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v1, Lgxi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, v1, Lgxi;->c:I

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v1, v3, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_6

    :cond_11
    iget-object v1, p0, Lgxj;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f04013a    # com.google.android.gms.R.layout.wallet_row_action_item

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto :goto_8

    :cond_12
    move-object v1, v4

    goto :goto_7
.end method

.method private static a(Landroid/view/View;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lioj;)I
    .locals 6

    const/4 v2, -0x1

    if-eqz p1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lgxj;->getCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p0, v1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lioj;

    if-eqz v4, :cond_1

    check-cast v0, Lioj;

    iget-object v4, p1, Lioj;->a:Ljava/lang/String;

    iget-object v5, v0, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lgxj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxk;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lgxk;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0, p1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lgxi;

    if-eqz v1, :cond_0

    check-cast v0, Lgxi;

    iget-boolean v0, v0, Lgxi;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lgxj;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lgxj;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lgxj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgxk;

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lgxk;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgxj;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lgxj;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lgxj;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    instance-of v2, v0, Lioj;

    if-eqz v2, :cond_3

    check-cast v0, Lioj;

    iget-object v2, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lioj;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->a(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v2

    iget-object v3, p0, Lgxj;->a:Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;->b(Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorSpinner;)[I

    move-result-object v3

    invoke-static {v0, v2, v3}, Lgth;->a(Lioj;[I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    instance-of v2, v0, Lgxi;

    if-eqz v2, :cond_4

    check-cast v0, Lgxi;

    iget-boolean v0, v0, Lgxi;->b:Z

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
