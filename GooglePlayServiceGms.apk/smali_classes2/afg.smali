.class public final enum Lafg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Lafg;

.field public static final enum B:Lafg;

.field public static final enum C:Lafg;

.field public static final enum D:Lafg;

.field public static final enum E:Lafg;

.field public static final enum F:Lafg;

.field public static final enum G:Lafg;

.field public static final enum H:Lafg;

.field public static final enum I:Lafg;

.field public static final enum J:Lafg;

.field public static final enum K:Lafg;

.field public static final enum L:Lafg;

.field public static final enum M:Lafg;

.field public static final enum N:Lafg;

.field public static final enum O:Lafg;

.field public static final enum P:Lafg;

.field public static final enum Q:Lafg;

.field public static final enum R:Lafg;

.field public static final enum S:Lafg;

.field public static final enum T:Lafg;

.field public static final enum U:Lafg;

.field public static final enum V:Lafg;

.field public static final enum W:Lafg;

.field public static final enum X:Lafg;

.field public static final enum Y:Lafg;

.field public static final enum Z:Lafg;

.field public static final enum a:Lafg;

.field public static final enum aa:Lafg;

.field public static final enum ab:Lafg;

.field private static final synthetic ac:[Lafg;

.field public static final enum b:Lafg;

.field public static final enum c:Lafg;

.field public static final enum d:Lafg;

.field public static final enum e:Lafg;

.field public static final enum f:Lafg;

.field public static final enum g:Lafg;

.field public static final enum h:Lafg;

.field public static final enum i:Lafg;

.field public static final enum j:Lafg;

.field public static final enum k:Lafg;

.field public static final enum l:Lafg;

.field public static final enum m:Lafg;

.field public static final enum n:Lafg;

.field public static final enum o:Lafg;

.field public static final enum p:Lafg;

.field public static final enum q:Lafg;

.field public static final enum r:Lafg;

.field public static final enum s:Lafg;

.field public static final enum t:Lafg;

.field public static final enum u:Lafg;

.field public static final enum v:Lafg;

.field public static final enum w:Lafg;

.field public static final enum x:Lafg;

.field public static final enum y:Lafg;

.field public static final enum z:Lafg;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lafg;

    const-string v1, "TRACK_VIEW"

    invoke-direct {v0, v1, v3}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->a:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_VIEW_WITH_APPSCREEN"

    invoke-direct {v0, v1, v4}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->b:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_EVENT"

    invoke-direct {v0, v1, v5}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->c:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_TRANSACTION"

    invoke-direct {v0, v1, v6}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->d:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_EXCEPTION_WITH_DESCRIPTION"

    invoke-direct {v0, v1, v7}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->e:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_EXCEPTION_WITH_THROWABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->f:Lafg;

    new-instance v0, Lafg;

    const-string v1, "BLANK_06"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->g:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_TIMING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->h:Lafg;

    new-instance v0, Lafg;

    const-string v1, "TRACK_SOCIAL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->i:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->j:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->k:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SEND"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->l:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_START_SESSION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->m:Lafg;

    new-instance v0, Lafg;

    const-string v1, "BLANK_13"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->n:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_NAME"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->o:Lafg;

    new-instance v0, Lafg;

    const-string v1, "BLANK_15"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->p:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_VERSION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->q:Lafg;

    new-instance v0, Lafg;

    const-string v1, "BLANK_17"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->r:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_SCREEN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->s:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_TRACKING_ID"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->t:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_ANONYMIZE_IP"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->u:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_ANONYMIZE_IP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->v:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_SAMPLE_RATE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->w:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_SAMPLE_RATE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->x:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_USE_SECURE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->y:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_USE_SECURE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->z:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_REFERRER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->A:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_CAMPAIGN"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->B:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_ID"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->C:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_APP_ID"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->D:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_EXCEPTION_PARSER"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->E:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_EXCEPTION_PARSER"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->F:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_TRANSACTION"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->G:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_EXCEPTION"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->H:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_RAW_EXCEPTION"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->I:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_TIMING"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->J:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_SOCIAL"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->K:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_DEBUG"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->L:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_DEBUG"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->M:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_TRACKER"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->N:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_DEFAULT_TRACKER"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->O:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_DEFAULT_TRACKER"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->P:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_OPT_OUT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->Q:Lafg;

    new-instance v0, Lafg;

    const-string v1, "REQUEST_APP_OPT_OUT"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->R:Lafg;

    new-instance v0, Lafg;

    const-string v1, "DISPATCH"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->S:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_DISPATCH_PERIOD"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->T:Lafg;

    new-instance v0, Lafg;

    const-string v1, "BLANK_48"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->U:Lafg;

    new-instance v0, Lafg;

    const-string v1, "REPORT_UNCAUGHT_EXCEPTIONS"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->V:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_AUTO_ACTIVITY_TRACKING"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->W:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_SESSION_TIMEOUT"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->X:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_EVENT"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->Y:Lafg;

    new-instance v0, Lafg;

    const-string v1, "CONSTRUCT_ITEM"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->Z:Lafg;

    new-instance v0, Lafg;

    const-string v1, "SET_APP_INSTALLER_ID"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->aa:Lafg;

    new-instance v0, Lafg;

    const-string v1, "GET_APP_INSTALLER_ID"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lafg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lafg;->ab:Lafg;

    const/16 v0, 0x36

    new-array v0, v0, [Lafg;

    sget-object v1, Lafg;->a:Lafg;

    aput-object v1, v0, v3

    sget-object v1, Lafg;->b:Lafg;

    aput-object v1, v0, v4

    sget-object v1, Lafg;->c:Lafg;

    aput-object v1, v0, v5

    sget-object v1, Lafg;->d:Lafg;

    aput-object v1, v0, v6

    sget-object v1, Lafg;->e:Lafg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lafg;->f:Lafg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lafg;->g:Lafg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lafg;->h:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lafg;->i:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lafg;->j:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lafg;->k:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lafg;->l:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lafg;->m:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lafg;->n:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lafg;->o:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lafg;->p:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lafg;->q:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lafg;->r:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lafg;->s:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lafg;->t:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lafg;->u:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lafg;->v:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lafg;->w:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lafg;->x:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lafg;->y:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lafg;->z:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lafg;->A:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lafg;->B:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lafg;->C:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lafg;->D:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lafg;->E:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lafg;->F:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lafg;->G:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lafg;->H:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lafg;->I:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lafg;->J:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lafg;->K:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lafg;->L:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lafg;->M:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lafg;->N:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lafg;->O:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lafg;->P:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lafg;->Q:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lafg;->R:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lafg;->S:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lafg;->T:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lafg;->U:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lafg;->V:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lafg;->W:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lafg;->X:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lafg;->Y:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lafg;->Z:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lafg;->aa:Lafg;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lafg;->ab:Lafg;

    aput-object v2, v0, v1

    sput-object v0, Lafg;->ac:[Lafg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lafg;
    .locals 1

    const-class v0, Lafg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lafg;

    return-object v0
.end method

.method public static values()[Lafg;
    .locals 1

    sget-object v0, Lafg;->ac:[Lafg;

    invoke-virtual {v0}, [Lafg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lafg;

    return-object v0
.end method
