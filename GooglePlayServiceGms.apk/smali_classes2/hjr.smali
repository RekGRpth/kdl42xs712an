.class public final Lhjr;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lilt;

.field private final c:J

.field private final d:I

.field private final e:J


# direct methods
.method public constructor <init>(Lilt;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhjr;-><init>(Lilt;B)V

    return-void
.end method

.method private constructor <init>(Lilt;B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhjr;->b:Lilt;

    const/4 v0, 0x3

    iput v0, p0, Lhjr;->d:I

    const-wide/32 v0, 0x927c0

    iput-wide v0, p0, Lhjr;->c:J

    const-wide/32 v0, 0xdbba0

    iput-wide v0, p0, Lhjr;->e:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjr;->a:Z

    return-void
.end method

.method private static a(Ljava/util/List;Lilt;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    invoke-virtual {v0, p1, v1}, Lilt;->a(Lilt;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 11

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lhjr;->b:Lilt;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjt;

    iget-wide v2, p0, Lhjr;->c:J

    const-wide/16 v4, 0x4

    div-long v4, v2, v4

    iget-object v0, v0, Lhjt;->a:Lilt;

    iget-wide v2, v0, Lilt;->a:J

    iget-object v7, p0, Lhjr;->b:Lilt;

    iget-wide v7, v7, Lilt;->a:J

    cmp-long v2, v2, v7

    if-nez v2, :cond_1

    iget-wide v2, v0, Lilt;->a:J

    :goto_1
    iget-wide v7, v0, Lilt;->b:J

    iget-object v9, p0, Lhjr;->b:Lilt;

    iget-wide v9, v9, Lilt;->b:J

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    iget-wide v4, v0, Lilt;->b:J

    :goto_2
    cmp-long v0, v4, v2

    if-gtz v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v2, "Block too small to substract."

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-wide v2, v0, Lilt;->a:J

    add-long/2addr v2, v4

    goto :goto_1

    :cond_2
    iget-wide v7, v0, Lilt;->b:J

    sub-long v4, v7, v4

    goto :goto_2

    :cond_3
    new-instance v0, Lilt;

    invoke-direct {v0, v2, v3, v4, v5}, Lilt;-><init>(JJ)V

    invoke-static {v1, v0}, Lhjr;->a(Ljava/util/List;Lilt;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_4
    invoke-static {v1}, Lhjr;->c(Ljava/util/List;)V

    return-object v1
.end method

.method private a(Ljava/util/SortedMap;)Ljava/util/List;
    .locals 14

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/SortedMap;->size()I

    move-result v0

    if-gtz v0, :cond_1

    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_0

    invoke-static {v8}, Lhjr;->b(Ljava/util/List;)V

    :cond_0
    move-object v0, v8

    :goto_0
    return-object v0

    :cond_1
    const-wide/16 v1, -0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v3, v0

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lhjs;

    const-wide/16 v10, -0x1

    cmp-long v0, v1, v10

    if-nez v0, :cond_4

    sget-object v0, Lhjs;->a:Lhjs;

    if-eq v7, v0, :cond_2

    sget-object v0, Lhjs;->c:Lhjs;

    if-ne v7, v0, :cond_3

    :cond_2
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjs;

    move-object v5, v0

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    move-object v3, v0

    goto :goto_1

    :cond_4
    if-eq v7, v5, :cond_3

    if-nez v3, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long/2addr v10, v1

    iget-wide v12, p0, Lhjr;->c:J

    add-long/2addr v10, v12

    iget-wide v12, p0, Lhjr;->e:J

    cmp-long v0, v10, v12

    if-ltz v0, :cond_6

    new-instance v0, Lhjt;

    iget-object v4, p0, Lhjr;->b:Lilt;

    iget-wide v10, v4, Lilt;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v12, p0, Lhjr;->c:J

    add-long/2addr v3, v12

    invoke-static {v10, v11, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lhjt;-><init>(JJLhjs;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    sget-object v0, Lhjs;->a:Lhjs;

    if-eq v7, v0, :cond_7

    sget-object v0, Lhjs;->c:Lhjs;

    if-ne v7, v0, :cond_8

    :cond_7
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjs;

    move-object v5, v0

    goto :goto_2

    :cond_8
    const-wide/16 v1, -0x1

    const/4 v5, 0x0

    goto :goto_2

    :cond_9
    const-wide/16 v3, -0x1

    cmp-long v0, v1, v3

    if-eqz v0, :cond_a

    invoke-interface {p1}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Long;

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, v1

    iget-wide v9, p0, Lhjr;->c:J

    add-long/2addr v6, v9

    iget-wide v9, p0, Lhjr;->e:J

    cmp-long v0, v6, v9

    if-ltz v0, :cond_a

    new-instance v0, Lhjt;

    iget-object v4, p0, Lhjr;->b:Lilt;

    iget-wide v6, v4, Lilt;->b:J

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-wide v9, p0, Lhjr;->c:J

    add-long/2addr v3, v9

    invoke-static {v6, v7, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lhjt;-><init>(JJLhjs;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_b

    invoke-static {v8}, Lhjr;->b(Ljava/util/List;)V

    :cond_b
    move-object v0, v8

    goto/16 :goto_0
.end method

.method private a(Ljava/util/Map;Ljava/lang/Integer;Livi;)Ljava/util/TreeMap;
    .locals 12

    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Livi;->k(I)I

    move-result v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_11

    const/4 v0, 0x2

    invoke-virtual {p3, v0, v4}, Livi;->c(II)Livi;

    move-result-object v7

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Livi;->c(I)I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    move-object v3, v0

    :goto_1
    if-nez v3, :cond_5

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Invalid entry."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Livi;->c(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Could not find matched timezone."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x0

    move-object v3, v0

    goto :goto_1

    :cond_4
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Livi;->d(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object v3, v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    const/4 v0, 0x7

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    :cond_6
    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "InOutdoorTransitionTimeFinder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Skipping weekends: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhjr;->b:Lilt;

    invoke-virtual {v0, v3}, Lilt;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "InOutdoorTransitionTimeFinder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not in target time span."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    invoke-static {v3}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    iget-object v2, p0, Lhjr;->b:Lilt;

    iget-wide v8, v2, Lilt;->a:J

    sub-long v8, v0, v8

    iget-wide v10, p0, Lhjr;->c:J

    rem-long/2addr v8, v10

    sub-long v8, v0, v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Livi;->b(I)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    move v2, v1

    :goto_3
    if-nez v0, :cond_b

    new-instance v1, Lhju;

    if-eqz v2, :cond_a

    const/4 v0, 0x1

    :goto_4
    invoke-direct {v1, p0, v0}, Lhju;-><init>(Lhjr;I)V

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v0, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_5
    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-virtual {v7, v0}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x5

    invoke-virtual {v7, v0}, Livi;->b(I)Z

    move-result v0

    :goto_6
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v8, "InOutdoorTransitionTimeFinder"

    const-string v9, "%s: isIndoor:%s, isStill:%s, derived indoor:%s"

    const/4 v1, 0x4

    new-array v10, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    aput-object v3, v10, v1

    const/4 v3, 0x1

    const/4 v1, 0x3

    invoke-virtual {v7, v1}, Livi;->b(I)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "T"

    :goto_7
    aput-object v1, v10, v3

    const/4 v1, 0x2

    if-eqz v0, :cond_f

    const-string v0, "T"

    :goto_8
    aput-object v0, v10, v1

    const/4 v1, 0x3

    if-eqz v2, :cond_10

    const-string v0, "T"

    :goto_9
    aput-object v0, v10, v1

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    const/4 v1, 0x5

    invoke-virtual {v7, v1}, Livi;->b(I)Z

    move-result v1

    move v2, v1

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :cond_b
    if-eqz v2, :cond_c

    iget v1, v0, Lhju;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lhju;->a:I

    :cond_c
    iget v1, v0, Lhju;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lhju;->b:I

    goto :goto_5

    :cond_d
    const/4 v0, 0x0

    goto :goto_6

    :cond_e
    const-string v1, "F"

    goto :goto_7

    :cond_f
    const-string v0, "F"

    goto :goto_8

    :cond_10
    const-string v0, "F"

    goto :goto_9

    :cond_11
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    invoke-virtual {v5}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhju;

    iget v4, v0, Lhju;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v0, v0, Lhju;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4, v0}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a

    :cond_12
    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lhjr;->c(Ljava/util/SortedMap;)V

    :cond_13
    return-object v1
.end method

.method private a(Ljava/util/TreeMap;)Ljava/util/TreeMap;
    .locals 12

    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    invoke-virtual {p1}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhue;

    iget-object v1, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v6, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhue;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    sget-object v0, Lhjs;->f:Lhjs;

    iget v1, p0, Lhjr;->d:I

    int-to-long v10, v1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_3

    long-to-double v0, v8

    div-double v0, v6, v0

    double-to-float v0, v0

    const v1, 0x3f4ccccd    # 0.8f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    sget-object v0, Lhjs;->a:Lhjs;

    :cond_0
    :goto_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const v1, 0x3e4ccccc    # 0.19999999f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    sget-object v0, Lhjs;->c:Lhjs;

    goto :goto_1

    :cond_2
    sget-object v0, Lhjs;->e:Lhjs;

    goto :goto_1

    :cond_3
    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-lez v1, :cond_0

    long-to-double v8, v8

    cmpl-double v1, v6, v8

    if-nez v1, :cond_4

    sget-object v0, Lhjs;->b:Lhjs;

    goto :goto_1

    :cond_4
    const-wide/16 v8, 0x0

    cmpl-double v1, v6, v8

    if-nez v1, :cond_0

    sget-object v0, Lhjs;->d:Lhjs;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhjr;->b:Lilt;

    iget-wide v0, v0, Lilt;->a:J

    :goto_2
    iget-object v3, p0, Lhjr;->b:Lilt;

    iget-wide v3, v3, Lilt;->b:J

    cmp-long v3, v0, v3

    if-gez v3, :cond_7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Lhjs;->f:Lhjs;

    invoke-virtual {v2, v3, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    iget-wide v3, p0, Lhjr;->c:J

    add-long/2addr v0, v3

    goto :goto_2

    :cond_7
    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_8

    invoke-direct {p0, v2}, Lhjr;->b(Ljava/util/SortedMap;)V

    :cond_8
    invoke-static {v2}, Lhjr;->b(Ljava/util/TreeMap;)V

    iget-boolean v0, p0, Lhjr;->a:Z

    if-eqz v0, :cond_a

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_9

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Correcting short low confidence time spans."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-direct {p0, v2}, Lhjr;->b(Ljava/util/SortedMap;)V

    :cond_a
    return-object v2
.end method

.method private static b(Ljava/util/List;)V
    .locals 7

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Block info:"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjt;

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "InOutdoorTransitionTimeFinder"

    const-string v3, "%s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lhjt;->a:Lilt;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v0, v0, Lhjt;->b:Lhjs;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "None"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private b(Ljava/util/SortedMap;)V
    .locals 11

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Indoor results:"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_1

    const-string v3, "InOutdoorTransitionTimeFinder"

    const-string v4, "%s ~ %s: %s"

    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v6, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-wide v9, p0, Lhjr;->c:J

    add-long/2addr v7, v9

    invoke-static {v7, v8}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static b(Ljava/util/TreeMap;)V
    .locals 5

    invoke-virtual {p0}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lhjs;->b:Lhjs;

    if-ne v2, v3, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lhjs;->a:Lhjs;

    if-ne v2, v4, :cond_0

    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lhjs;->a:Lhjs;

    if-ne v2, v3, :cond_0

    :cond_2
    sget-object v2, Lhjs;->a:Lhjs;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lhjs;->d:Lhjs;

    if-ne v2, v3, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/TreeMap;->lowerEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/util/TreeMap;->higherEntry(Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v3

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Lhjs;->c:Lhjs;

    if-ne v2, v4, :cond_0

    :cond_4
    if-eqz v3, :cond_5

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lhjs;->c:Lhjs;

    if-ne v2, v3, :cond_0

    :cond_5
    sget-object v2, Lhjs;->c:Lhjs;

    invoke-interface {v0, v2}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    return-void
.end method

.method private static c(Ljava/util/List;)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "TimeSpans for possible in/outdoor transitions:"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilt;

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    const-string v2, "InOutdoorTransitionTimeFinder"

    invoke-virtual {v0}, Lilt;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private c(Ljava/util/SortedMap;)V
    .locals 11

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v1, "Aggregated stats:"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhue;

    iget-object v1, v1, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhue;

    iget-object v1, v1, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "InOutdoorTransitionTimeFinder"

    const-string v6, "%s ~ %s: IN: %d, OUT: %d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-wide v9, p0, Lhjr;->c:J

    add-long/2addr v4, v9

    invoke-static {v4, v5}, Lilv;->b(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v4

    const/4 v4, 0x3

    sub-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Livi;)Ljava/util/Map;
    .locals 8

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Livi;->k(I)I

    move-result v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v5, v0}, Livi;->c(II)Livi;

    move-result-object v3

    invoke-virtual {v3, v6}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v5}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v6}, Livi;->c(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5}, Livi;->g(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    new-instance v3, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/HashMap;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-direct {p0, v2, v1, p1}, Lhjr;->a(Ljava/util/Map;Ljava/lang/Integer;Livi;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-direct {p0, v1}, Lhjr;->a(Ljava/util/TreeMap;)Ljava/util/TreeMap;

    move-result-object v1

    invoke-direct {p0, v1}, Lhjr;->a(Ljava/util/SortedMap;)Ljava/util/List;

    move-result-object v5

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    const-string v6, "InOutdoorTransitionTimeFinder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Final result for time zone: "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, v5}, Lhjr;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-object v3
.end method
