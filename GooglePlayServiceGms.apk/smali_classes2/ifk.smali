.class public final Lifk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liza;


# instance fields
.field a:Lizd;

.field b:Z

.field private c:Livi;


# direct methods
.method public constructor <init>(Livi;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lizd;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lizd;-><init>(Liyf;)V

    iput-object v0, p0, Lifk;->a:Lizd;

    iput-object p1, p0, Lifk;->c:Livi;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lifk;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Liyz;Lizb;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lifk;->b:Z

    :try_start_0
    invoke-virtual {p2}, Lizb;->Z_()Ljava/io/InputStream;

    move-result-object v0

    iget v1, p2, Lizb;->d:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lifk;->c:Livi;

    invoke-static {v0, v1}, Lilv;->a(Ljava/io/InputStream;Livi;)V

    iget-object v0, p0, Lifk;->a:Lizd;

    iget-object v1, p0, Lifk;->c:Livi;

    invoke-virtual {v0, v1}, Lizd;->a(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_1

    const-string v0, "ProtoRequestListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestCompleted() HTTP response code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lizb;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lifk;->a:Lizd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lizd;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "ProtoRequestListener"

    const-string v2, "requestCompleted()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    iget-object v0, p0, Lifk;->a:Lizd;

    invoke-virtual {v0, v3}, Lizd;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Liyz;Ljava/lang/Exception;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lifk;->b:Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ProtoRequestListener"

    const-string v1, "requestFailed()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
