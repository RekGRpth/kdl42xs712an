.class public final Lhwr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Lhuz;

.field c:I

.field d:I

.field e:J

.field f:Z

.field private g:J


# direct methods
.method public constructor <init>(Lhuz;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhwr;->a:Ljava/lang/Object;

    iput v1, p0, Lhwr;->c:I

    iput v1, p0, Lhwr;->d:I

    iput-wide v2, p0, Lhwr;->e:J

    iput-boolean v1, p0, Lhwr;->f:Z

    iput-wide v2, p0, Lhwr;->g:J

    iput-object p1, p0, Lhwr;->b:Lhuz;

    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/gms/location/LocationStatus;
    .locals 8

    const/16 v2, 0x8

    iget-object v5, p0, Lhwr;->a:Ljava/lang/Object;

    monitor-enter v5

    if-nez p1, :cond_0

    const/4 v0, 0x7

    const/4 v1, 0x7

    :try_start_0
    iget-object v2, p0, Lhwr;->b:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/location/LocationStatus;->a(IIJ)Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    monitor-exit v5

    :goto_0
    return-object v0

    :cond_0
    iget v4, p0, Lhwr;->c:I

    iget v3, p0, Lhwr;->d:I

    iget-wide v0, p0, Lhwr;->e:J

    iget-boolean v6, p0, Lhwr;->f:Z

    if-eqz v6, :cond_2

    if-nez v4, :cond_1

    iget-wide v6, p0, Lhwr;->g:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move v4, v2

    :cond_1
    if-nez v3, :cond_2

    iget-wide v6, p0, Lhwr;->g:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move v3, v4

    :goto_1
    invoke-static {v3, v2, v0, v1}, Lcom/google/android/gms/location/LocationStatus;->a(IIJ)Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_2
    move v2, v3

    move v3, v4

    goto :goto_1
.end method

.method public final a()V
    .locals 4

    iget-object v1, p0, Lhwr;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhwr;->f:Z

    iget-object v0, p0, Lhwr;->b:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lhwr;->g:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 1

    iget v0, p0, Lhwr;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget v0, p0, Lhwr;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
