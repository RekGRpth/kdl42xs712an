.class final Lhvn;
.super Leqd;
.source "SourceFile"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# instance fields
.field final synthetic a:Lhvb;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/pm/PackageManager;

.field private final d:Landroid/app/PendingIntent;

.field private final e:Landroid/os/PowerManager$WakeLock;

.field private final f:Z


# direct methods
.method constructor <init>(Lhvb;Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/Collection;Z)V
    .locals 3

    const/4 v2, 0x1

    iput-object p1, p0, Lhvn;->a:Lhvb;

    invoke-direct {p0}, Leqd;-><init>()V

    iput-object p2, p0, Lhvn;->b:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lhvn;->c:Landroid/content/pm/PackageManager;

    iput-object p3, p0, Lhvn;->d:Landroid/app/PendingIntent;

    iput-boolean p5, p0, Lhvn;->f:Z

    const-string v0, "power"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const-string v1, "GCoreFlp"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iget-object v0, p0, Lhvn;->b:Landroid/content/Context;

    invoke-static {v0}, Lbqf;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    invoke-static {p4}, Lile;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lhvn;->d:Landroid/app/PendingIntent;

    iget-object v1, p0, Lhvn;->c:Landroid/content/pm/PackageManager;

    invoke-static {v0, v1}, Lilm;->a(Landroid/app/PendingIntent;Landroid/content/pm/PackageManager;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lhvn;->f:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    :cond_0
    const-string v0, "GCoreFlp"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Package %s permissions were downgraded, not reporting location"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lhvn;->d:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lhvn;->a:Lhvb;

    iget-object v1, p0, Lhvn;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lhvb;->b(Landroid/app/PendingIntent;)V

    :goto_0
    return-void

    :cond_2
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v0, "com.google.android.location.LOCATION"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    iget-object v0, p0, Lhvn;->d:Landroid/app/PendingIntent;

    iget-object v1, p0, Lhvn;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Package %s canceled PendingIntent, removing"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lhvn;->d:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lhvn;->a:Lhvb;

    iget-object v1, p0, Lhvn;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Lhvb;->b(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public final onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhvn;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method
