.class public Lgwb;
.super Landroid/widget/Spinner;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private b:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lgwb;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgwb;->d:Z

    invoke-direct {p0}, Lgwb;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lgwb;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgwb;->d:Z

    invoke-direct {p0}, Lgwb;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lgwb;->c:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgwb;->d:Z

    invoke-direct {p0}, Lgwb;->a()V

    return-void
.end method

.method static synthetic a(Lgwb;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    iget-object v0, p0, Lgwb;->a:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lgwb;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lgwc;

    invoke-direct {v0, p0}, Lgwc;-><init>(Lgwb;)V

    iput-object v0, p0, Lgwb;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v0, p0, Lgwb;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-super {p0, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method static synthetic b(Lgwb;)Z
    .locals 1

    iget-boolean v0, p0, Lgwb;->d:Z

    return v0
.end method

.method static synthetic c(Lgwb;)I
    .locals 1

    iget v0, p0, Lgwb;->c:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    iput p1, p0, Lgwb;->c:I

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lgwb;->a:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-nez p1, :cond_2

    iget-object v0, p0, Lgwb;->a:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgwb;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v1, p0, Lgwb;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-super {p0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lgwb;->a()V

    goto :goto_0
.end method
