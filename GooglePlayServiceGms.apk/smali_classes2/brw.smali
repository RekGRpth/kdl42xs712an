.class public final Lbrw;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 3

    iget-object v0, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    const-string v1, "Invalid create request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid create request: no parent"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v1, "Invalid create request: no metadata"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrw;->b:Lbrc;

    iget-object v1, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lbrw;->c:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lbrc;->b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v0, p0, Lbrw;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v0, v2}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V

    return-void
.end method
