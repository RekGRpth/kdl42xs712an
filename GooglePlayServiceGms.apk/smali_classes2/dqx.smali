.class final Ldqx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldrs;


# instance fields
.field final synthetic a:Ldqv;

.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Ldqv;Ldad;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Ldqx;->a:Ldqv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ldqx;->b:Ldad;

    iput-object p3, p0, Ldqx;->c:Ljava/lang/String;

    iput-boolean p4, p0, Ldqx;->d:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 6

    iget-object v0, p0, Ldqx;->a:Ldqv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ldqv;->a(Ldqv;Ldqz;)Ldqz;

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Ldqx;->b:Ldad;

    const/16 v1, 0x1b58

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Ldad;->s(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesService"

    const-string v1, "The client is not reachable"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v2, Ldqy;

    iget-object v0, p0, Ldqx;->a:Ldqv;

    iget-object v1, p0, Ldqx;->b:Ldad;

    iget-boolean v3, p0, Ldqx;->d:Z

    invoke-direct {v2, v0, v1, v3}, Ldqy;-><init>(Ldqv;Ldad;Z)V

    invoke-static {}, Ldqv;->z()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Ldqx;->a:Ldqv;

    invoke-static {v0}, Ldqv;->a(Ldqv;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Ldqx;->a:Ldqv;

    invoke-static {v0}, Ldqv;->e(Ldqv;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ldqx;->a:Ldqv;

    invoke-static {v1}, Ldqv;->f(Ldqv;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    iget-object v3, p0, Ldqx;->a:Ldqv;

    invoke-static {v3}, Ldqv;->g(Ldqv;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldqx;->c:Ljava/lang/String;

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldqy;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    iget-object v0, p0, Ldqx;->a:Ldqv;

    invoke-static {v0}, Ldqv;->h(Ldqv;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ldrq;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ldqx;->a:Ldqv;

    invoke-static {v0}, Ldqv;->d(Ldqv;)Ldqz;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "RoomConnectAsyncTask is not null and maybe already running."

    invoke-static {v0, v3}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldqx;->a:Ldqv;

    new-instance v3, Ldqz;

    iget-object v4, p0, Ldqx;->a:Ldqv;

    invoke-direct {v3, v4, p1}, Ldqz;-><init>(Ldqv;Ldrq;)V

    invoke-static {v0, v3}, Ldqv;->a(Ldqv;Ldqz;)Ldqz;

    iget-object v0, p0, Ldqx;->a:Ldqv;

    invoke-static {v0}, Ldqv;->d(Ldqv;)Ldqz;

    move-result-object v0

    new-array v1, v1, [Ldrs;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Ldqz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method
