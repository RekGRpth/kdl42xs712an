.class public abstract Lhha;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field protected final b:Landroid/content/Context;

.field protected final c:Landroid/content/res/Resources;

.field protected d:Lhgs;

.field protected e:Landroid/graphics/Bitmap;

.field protected f:Z

.field private final g:Landroid/util/SparseArray;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhha;->g:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhha;->a:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhha;->f:Z

    iput-object p1, p0, Lhha;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lhha;->c:Landroid/content/res/Resources;

    return-void
.end method

.method public static a(Landroid/widget/ImageView;)Lhhc;
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v1, v0, Lhhb;

    if-eqz v1, :cond_0

    check-cast v0, Lhhb;

    invoke-virtual {v0}, Lhhb;->a()Lhhc;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lhha;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    iget-boolean v0, p0, Lhha;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x106000d    # android.R.color.transparent

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lhha;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
.end method

.method public final a(Lhgs;)V
    .locals 0

    iput-object p1, p0, Lhha;->d:Lhgs;

    return-void
.end method

.method protected final a(Ljava/lang/Object;Landroid/widget/ImageView;I)V
    .locals 3

    iget-object v0, p0, Lhha;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lhha;->c:Landroid/content/res/Resources;

    invoke-static {v0, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lhha;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, p3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lhha;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, v0}, Lhha;->a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "ImageWorker"

    const-string v2, "Ignored error on decodeResource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhha;->d:Lhgs;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lhha;->d:Lhgs;

    invoke-virtual {v0, v3}, Lhgs;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p2}, Lhha;->a(Landroid/widget/ImageView;)Lhhc;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lhhc;->a(Lhhc;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Lhhc;->a(Lhhc;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0, v2}, Lhhc;->cancel(Z)Z

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    new-instance v0, Lhhc;

    invoke-direct {v0, p0, p2}, Lhhc;-><init>(Lhha;Landroid/widget/ImageView;)V

    new-instance v3, Lhhb;

    iget-object v4, p0, Lhha;->c:Landroid/content/res/Resources;

    invoke-direct {v3, v4, p3, v0}, Lhhb;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Lhhc;)V

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-virtual {v0, v2}, Lhhc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method
