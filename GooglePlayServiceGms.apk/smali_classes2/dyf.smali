.class final Ldyf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/widget/TextView;

.field final d:Landroid/database/CharArrayBuffer;

.field final e:Landroid/widget/TextView;

.field final synthetic f:Ldye;


# direct methods
.method public constructor <init>(Ldye;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Ldyf;->f:Ldye;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0a016c    # com.google.android.gms.R.id.player_image

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Ldyf;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v0, 0x7f0a01b4    # com.google.android.gms.R.id.player_name_me

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyf;->b:Landroid/widget/TextView;

    const v0, 0x7f0a016d    # com.google.android.gms.R.id.player_name

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyf;->c:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Ldyf;->d:Landroid/database/CharArrayBuffer;

    const v0, 0x7f0a01e6    # com.google.android.gms.R.id.player_status

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyf;->e:Landroid/widget/TextView;

    return-void
.end method
