.class public final Liyo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lixv;


# instance fields
.field final a:Lixv;

.field private b:Ljava/util/Hashtable;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:[B


# direct methods
.method private constructor <init>(Ljava/lang/String;Lixv;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Liyo;->c:Ljava/lang/String;

    iput-object p2, p0, Liyo;->a:Lixv;

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Liyo;
    .locals 9

    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, p0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readShort()S

    move-result v1

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Liyt;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3}, Liyt;->a(Ljava/io/DataInputStream;)Ljava/util/Hashtable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, " => "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Lixu;

    invoke-direct {v6, v3, v0}, Lixu;-><init>(Ljava/io/InputStream;I)V

    new-instance v0, Liyp;

    const/4 v3, -0x1

    invoke-direct {v0, v1, v3, v6}, Liyp;-><init>(Ljava/lang/String;ILixu;)V

    invoke-virtual {v6}, Lixu;->b()V

    new-instance v1, Liyo;

    invoke-direct {v1, v2, v0}, Liyo;-><init>(Ljava/lang/String;Lixv;)V

    iput-object v5, v1, Liyo;->b:Ljava/util/Hashtable;

    iput-object v4, v1, Liyo;->d:Ljava/lang/String;

    return-object v1

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Liyo;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Liyt;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-direct {p0}, Liyo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {p0}, Liyo;->d()Ljava/util/Hashtable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Liyt;->a(Ljava/io/DataOutputStream;Ljava/util/Hashtable;Ljava/lang/String;)V

    iget-object v2, p0, Liyo;->a:Lixv;

    invoke-interface {v2}, Lixv;->Y_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v2, p0, Liyo;->a:Lixv;

    invoke-interface {v2}, Lixv;->Y_()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Liyo;->e:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liyo;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liyo;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final Y_()I
    .locals 2

    invoke-direct {p0}, Liyo;->e()V

    iget-object v0, p0, Liyo;->e:[B

    array-length v0, v0

    iget-object v1, p0, Liyo;->a:Lixv;

    invoke-interface {v1}, Lixv;->Y_()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final Z_()Ljava/io/InputStream;
    .locals 3

    invoke-direct {p0}, Liyo;->e()V

    new-instance v0, Livd;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Liyo;->e:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Liyo;->a:Lixv;

    invoke-interface {v2}, Lixv;->Z_()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Livd;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Liyo;->a:Lixv;

    invoke-interface {v0}, Lixv;->a()V

    return-void
.end method

.method public final declared-synchronized d()Ljava/util/Hashtable;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Liyo;->b:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Liyo;->b:Ljava/util/Hashtable;

    :cond_0
    iget-object v0, p0, Liyo;->b:Ljava/util/Hashtable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
