.class public final Leie;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile f:[Leie;


# instance fields
.field public a:I

.field public b:[Leig;

.field public c:[Ljava/lang/String;

.field public d:[Leim;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Leie;->a:I

    invoke-static {}, Leig;->c()[Leig;

    move-result-object v0

    iput-object v0, p0, Leie;->b:[Leig;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Leie;->c:[Ljava/lang/String;

    invoke-static {}, Leim;->c()[Leim;

    move-result-object v0

    iput-object v0, p0, Leie;->d:[Leim;

    const/4 v0, 0x1

    iput v0, p0, Leie;->e:I

    const/4 v0, -0x1

    iput v0, p0, Leie;->C:I

    return-void
.end method

.method public static c()[Leie;
    .locals 2

    sget-object v0, Leie;->f:[Leie;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Leie;->f:[Leie;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Leie;

    sput-object v0, Leie;->f:[Leie;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Leie;->f:[Leie;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v2, p0, Leie;->a:I

    if-eqz v2, :cond_0

    iget v2, p0, Leie;->a:I

    invoke-static {v6, v2}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Leie;->b:[Leig;

    if-eqz v2, :cond_3

    iget-object v2, p0, Leie;->b:[Leig;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Leie;->b:[Leig;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Leie;->b:[Leig;

    aget-object v3, v3, v0

    if-eqz v3, :cond_1

    const/4 v4, 0x2

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    :cond_3
    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_1
    iget-object v5, p0, Leie;->c:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_5

    iget-object v5, p0, Leie;->c:[Ljava/lang/String;

    aget-object v5, v5, v2

    if-eqz v5, :cond_4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lizn;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    add-int/2addr v0, v3

    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Leie;->d:[Leim;

    if-eqz v2, :cond_8

    iget-object v2, p0, Leie;->d:[Leim;

    array-length v2, v2

    if-lez v2, :cond_8

    :goto_2
    iget-object v2, p0, Leie;->d:[Leim;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    iget-object v2, p0, Leie;->d:[Leim;

    aget-object v2, v2, v1

    if-eqz v2, :cond_7

    const/4 v3, 0x4

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_8
    iget v1, p0, Leie;->e:I

    if-eq v1, v6, :cond_9

    const/4 v1, 0x5

    iget v2, p0, Leie;->e:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iput v0, p0, Leie;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leie;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leie;->b:[Leig;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leig;

    if-eqz v0, :cond_1

    iget-object v3, p0, Leie;->b:[Leig;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Leig;

    invoke-direct {v3}, Leig;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Leie;->b:[Leig;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Leig;

    invoke-direct {v3}, Leig;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Leie;->b:[Leig;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leie;->c:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Leie;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Leie;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Leie;->c:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leie;->d:[Leim;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leim;

    if-eqz v0, :cond_7

    iget-object v3, p0, Leie;->d:[Leim;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Leim;

    invoke-direct {v3}, Leim;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Leie;->d:[Leim;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Leim;

    invoke-direct {v3}, Leim;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Leie;->d:[Leim;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leie;->e:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget v0, p0, Leie;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Leie;->a:I

    invoke-virtual {p1, v4, v0}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Leie;->b:[Leig;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leie;->b:[Leig;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v2, p0, Leie;->b:[Leig;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Leie;->b:[Leig;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Leie;->c:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Leie;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Leie;->d:[Leim;

    if-eqz v0, :cond_6

    iget-object v0, p0, Leie;->d:[Leim;

    array-length v0, v0

    if-lez v0, :cond_6

    :goto_2
    iget-object v0, p0, Leie;->d:[Leim;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Leie;->d:[Leim;

    aget-object v0, v0, v1

    if-eqz v0, :cond_5

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    iget v0, p0, Leie;->e:I

    if-eq v0, v4, :cond_7

    const/4 v0, 0x5

    iget v1, p0, Leie;->e:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_7
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leie;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leie;

    iget v2, p0, Leie;->a:I

    iget v3, p1, Leie;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Leie;->b:[Leig;

    iget-object v3, p1, Leie;->b:[Leig;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Leie;->c:[Ljava/lang/String;

    iget-object v3, p1, Leie;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Leie;->d:[Leim;

    iget-object v3, p1, Leie;->d:[Leim;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Leie;->e:I

    iget v3, p1, Leie;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Leie;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Leie;->b:[Leig;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Leie;->c:[Ljava/lang/String;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Leie;->d:[Leim;

    invoke-static {v1}, Lizq;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leie;->e:I

    add-int/2addr v0, v1

    return v0
.end method
