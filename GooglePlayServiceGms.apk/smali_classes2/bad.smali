.class final Lbad;
.super Laww;
.source "SourceFile"


# instance fields
.field final synthetic b:Lbac;


# direct methods
.method constructor <init>(Lbac;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lbad;->b:Lbac;

    invoke-direct {p0, p2}, Laww;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->c(Lbac;)Lbai;

    move-result-object v0

    invoke-interface {v0, p1}, Lbai;->c(I)V

    return-void
.end method

.method protected final a(Lavw;)V
    .locals 3

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->a(Lbac;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->b(Lbac;)Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbad;->a:Laye;

    const-string v1, "an app was running before; notifying that it\'s gone now"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbad;->b:Lbac;

    const/16 v1, 0x7d5

    invoke-static {v0, v1}, Lbac;->a(Lbac;I)V

    :cond_0
    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0, p1}, Lbac;->a(Lbac;Lavw;)V

    return-void
.end method

.method protected final a(Lavw;DZZ)V
    .locals 6

    iget-object v0, p0, Lbad;->a:Laye;

    const-string v1, "onStatusReceived"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0, p2, p3}, Lbac;->a(Lbac;D)D

    iget-object v0, p0, Lbad;->b:Lbac;

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lbac;->a(Lbac;Lavw;DZZ)V

    return-void
.end method

.method protected final b(I)V
    .locals 1

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->c(Lbac;)Lbai;

    move-result-object v0

    invoke-interface {v0, p1}, Lbai;->b(I)V

    return-void
.end method

.method protected final c(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbad;->a:Laye;

    const-string v1, "onStatusRequestFailed: statusCode=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->h(Lbac;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->i(Lbac;)Lbap;

    move-result-object v0

    invoke-virtual {v0}, Lbap;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbad;->a:Laye;

    const-string v1, "calling Listener.onConnectedWithoutApp()"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->c(Lbac;)Lbai;

    move-result-object v0

    invoke-interface {v0}, Lbai;->k_()V

    :goto_0
    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->j(Lbac;)Ljava/lang/String;

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->k(Lbac;)Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->c(Lbac;)Lbai;

    move-result-object v0

    invoke-interface {v0, p1}, Lbai;->b(I)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    iget-object v0, p0, Lbad;->b:Lbac;

    invoke-static {v0}, Lbac;->d(Lbac;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbad;->b:Lbac;

    invoke-static {v1}, Lbac;->e(Lbac;)Z

    iget-object v1, p0, Lbad;->b:Lbac;

    invoke-static {v1}, Lbac;->f(Lbac;)Ljava/lang/String;

    iget-object v1, p0, Lbad;->b:Lbac;

    invoke-static {v1}, Lbac;->g(Lbac;)Ljava/lang/String;

    iget-object v1, p0, Lbad;->b:Lbac;

    invoke-static {v1, v0}, Lbac;->a(Lbac;I)V

    return-void

    :cond_0
    const/16 v0, 0x7d5

    goto :goto_0
.end method
