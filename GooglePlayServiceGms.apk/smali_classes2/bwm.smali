.class public final Lbwm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/concurrent/CountDownLatch;

.field b:I

.field private final c:Lbwp;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lbwp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lbwm;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Lbwm;->c:Lbwp;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lbwm;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbwm;->c:Lbwp;

    invoke-virtual {v0}, Lbwp;->b()V

    :cond_0
    return-void
.end method

.method public final a(Lbwk;)V
    .locals 1

    iget-object v0, p0, Lbwm;->c:Lbwp;

    iget-object v0, v0, Lbwp;->a:Lbwj;

    invoke-virtual {v0, p1}, Lbwj;->a(Lbwk;)V

    return-void
.end method

.method public final b()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbwm;->a:Ljava/util/concurrent/CountDownLatch;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbwm;->a:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lbwn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbwn;-><init>(Lbwm;B)V

    invoke-virtual {p0, v0}, Lbwm;->a(Lbwk;)V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbwm;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    iget v0, p0, Lbwm;->b:I

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
