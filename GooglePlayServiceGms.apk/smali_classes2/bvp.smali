.class public final Lbvp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcfz;

.field private final b:Lcll;


# direct methods
.method public constructor <init>(Lcfz;Lcll;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbvp;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcll;

    iput-object v0, p0, Lbvp;->b:Lcll;

    return-void
.end method

.method public constructor <init>(Lcoy;)V
    .locals 2

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvp;-><init>(Lcfz;Lcll;)V

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 3

    iget-object v0, p0, Lbvp;->a:Lcfz;

    iget-object v1, p1, Lbsp;->a:Lcfc;

    invoke-interface {v0, v1}, Lcfz;->e(Lcfc;)Ljava/util/Set;

    move-result-object v0

    iget-wide v1, p1, Lbsp;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbvp;->b(Lbsp;)V

    :cond_0
    return-void
.end method

.method final b(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbvp;->b:Lcll;

    invoke-interface {v0, p1}, Lcll;->a(Lbsp;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lbvp;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_0
    iget-object v1, p0, Lbvp;->a:Lcfz;

    invoke-interface {v1, p1}, Lcfz;->a(Lbsp;)Lcfg;

    move-result-object v1

    iget-object v2, v1, Lcfg;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    iget-object v3, p0, Lbvp;->a:Lcfz;

    invoke-interface {v3, p1, v2}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v2

    invoke-static {v2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v0}, Lcfp;->f(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcfp;->k()V

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcfg;->b:Z

    invoke-virtual {v1}, Lcfg;->k()V

    iget-object v0, p0, Lbvp;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbvp;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbvp;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method
