.class public final Lixw;
.super Liur;
.source "SourceFile"


# static fields
.field protected static b:Lixw;


# instance fields
.field protected c:Livs;

.field protected d:Liyq;

.field protected e:Ljava/util/Vector;

.field protected f:Ljava/util/Hashtable;

.field protected g:Ljava/util/Hashtable;

.field h:Ljava/lang/Object;

.field i:I

.field protected j:J

.field protected k:J

.field protected l:Livt;

.field protected m:Livt;

.field private n:Liyi;

.field private o:Liuw;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/util/Vector;

.field private s:I

.field private t:I

.field private final u:J

.field private v:Z

.field private w:Liye;


# direct methods
.method private constructor <init>(Liya;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Liur;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lixw;->r:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lixw;->e:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lixw;->f:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lixw;->g:Ljava/util/Hashtable;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lixw;->h:Ljava/lang/Object;

    iput v1, p0, Lixw;->s:I

    iput v1, p0, Lixw;->i:I

    iput v1, p0, Lixw;->t:I

    iput-boolean v1, p0, Lixw;->v:Z

    iget-wide v0, p1, Liya;->g:J

    iput-wide v0, p0, Lixw;->u:J

    new-instance v0, Liyq;

    iget-object v1, p1, Liya;->c:Ljava/lang/String;

    iget-object v2, p1, Liya;->d:Ljava/lang/String;

    iget-object v3, p1, Liya;->e:Ljava/lang/String;

    iget-object v4, p1, Liya;->f:Ljava/lang/String;

    const-string v5, "g"

    invoke-direct/range {v0 .. v5}, Liyq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lixw;->d:Liyq;

    new-instance v0, Livs;

    new-instance v1, Livn;

    invoke-direct {v1}, Livn;-><init>()V

    const-string v2, "MobileServiceMux TaskRunner"

    invoke-direct {v0, v1, v2}, Livs;-><init>(Livp;Ljava/lang/String;)V

    iput-object v0, p0, Lixw;->c:Livs;

    iget-object v0, p0, Lixw;->c:Livs;

    invoke-virtual {v0}, Livs;->b()V

    invoke-static {}, Liun;->a()Liun;

    move-result-object v0

    invoke-virtual {v0}, Liun;->d()Liuw;

    move-result-object v0

    iput-object v0, p0, Lixw;->o:Liuw;

    iget-object v0, p1, Liya;->a:Ljava/lang/String;

    iput-object v0, p0, Lixw;->p:Ljava/lang/String;

    iget-object v0, p1, Liya;->b:Ljava/lang/String;

    iput-object v0, p0, Lixw;->q:Ljava/lang/String;

    new-instance v0, Liyi;

    iget-object v1, p0, Lixw;->c:Livs;

    new-instance v2, Livn;

    invoke-direct {v2}, Livn;-><init>()V

    iget-object v3, p0, Lixw;->o:Liuw;

    const-string v4, "MobileServiceMux AsyncHttpRequestFactory"

    invoke-direct {v0, v1, v2, v3, v4}, Liyi;-><init>(Livs;Livp;Liuw;Ljava/lang/String;)V

    iput-object v0, p0, Lixw;->n:Liyi;

    iget-object v0, p0, Lixw;->n:Liyi;

    invoke-virtual {v0}, Liyi;->a()V

    new-instance v0, Liye;

    invoke-direct {v0}, Liye;-><init>()V

    iput-object v0, p0, Lixw;->w:Liye;

    new-instance v0, Livt;

    iget-object v1, p0, Lixw;->c:Livs;

    new-instance v2, Lixx;

    invoke-direct {v2, p0}, Lixx;-><init>(Lixw;)V

    invoke-direct {v0, v1, v2}, Livt;-><init>(Livs;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lixw;->l:Livt;

    new-instance v0, Lixy;

    iget-object v1, p0, Lixw;->c:Livs;

    invoke-direct {v0, p0, v1}, Lixy;-><init>(Lixw;Livs;)V

    iput-object v0, p0, Lixw;->m:Livt;

    return-void
.end method

.method private a(Lizc;Liyu;)Lizb;
    .locals 10

    invoke-virtual {p1}, Lizc;->j()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Liyv;

    invoke-virtual {p2}, Liyu;->d()I

    move-result v0

    iget v1, p2, Lizb;->d:I

    iget-object v2, p2, Liyu;->a:Liyo;

    invoke-direct {v4, v0, v1, v2}, Liyv;-><init>(IILiyo;)V

    new-instance v5, Ljava/util/Hashtable;

    invoke-direct {v5}, Ljava/util/Hashtable;-><init>()V

    iget-object v6, p2, Liyu;->b:[Liyo;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    array-length v0, v6

    if-ge v2, v0, :cond_2

    aget-object v7, v6, v2

    invoke-virtual {v7}, Liyo;->d()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "Content-Location"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liyv;

    if-nez v1, :cond_0

    invoke-virtual {v7}, Liyo;->d()Ljava/util/Hashtable;

    move-result-object v1

    const-string v8, "X-Masf-Response-Code"

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v8, Liyv;

    invoke-virtual {p2}, Liyu;->d()I

    move-result v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v8, v9, v1, v7}, Liyv;-><init>(IILiyo;)V

    invoke-virtual {v5, v0, v8}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v7}, Liyv;->a(Liyo;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v7}, Liyv;->a(Liyo;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    iget-object v2, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v2

    :goto_2
    :try_start_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyv;

    iget-object v6, p0, Lixw;->w:Liye;

    invoke-virtual {v0}, Liyv;->a()Liyu;

    move-result-object v0

    invoke-virtual {v6, v3, v0}, Liye;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Liyv;->a()Liyu;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lixw;)V
    .locals 0

    invoke-direct {p0}, Lixw;->f()V

    return-void
.end method

.method static synthetic a(Lixw;Liyh;[Liyz;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-interface {p1}, Liyh;->aa_()I

    move-result v1

    invoke-interface {p1}, Liyh;->h()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x1f6

    if-ne v1, v3, :cond_0

    new-instance v2, Lixt;

    invoke-direct {v2, v1}, Lixt;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    const/16 v3, 0xc8

    if-eq v1, v3, :cond_1

    new-instance v2, Lixt;

    invoke-direct {v2, v1}, Lixt;-><init>(I)V

    invoke-direct {p0, p2, v2}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    invoke-direct {p0}, Lixw;->g()[Liyb;

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    const-string v1, "application/binary"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Bad content-type"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    :try_start_0
    iget v1, p0, Lixw;->t:I

    int-to-long v1, v1

    invoke-interface {p1}, Liyh;->e()J

    move-result-wide v6

    add-long/2addr v1, v6

    long-to-int v1, v1

    iput v1, p0, Lixw;->t:I

    invoke-interface {p1}, Liyh;->j()Ljava/io/DataInputStream;

    move-result-object v1

    new-instance v7, Liyy;

    invoke-direct {v7, v1}, Liyy;-><init>(Ljava/io/DataInputStream;)V

    move v6, v5

    :goto_1
    array-length v1, p2

    if-ge v6, v1, :cond_b

    iget-object v1, v7, Liyy;->b:Lixu;

    if-eqz v1, :cond_4

    iget-object v1, v7, Liyy;->b:Lixu;

    invoke-virtual {v1}, Lixu;->a()V

    const/4 v1, 0x0

    iput-object v1, v7, Liyy;->b:Lixu;

    :cond_4
    iget-object v1, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iget-object v2, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v2

    const v3, 0x8100

    if-ne v2, v3, :cond_6

    new-instance v2, Lixu;

    iget-object v3, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-direct {v2, v3, v1}, Lixu;-><init>(Ljava/io/InputStream;I)V

    iput-object v2, v7, Liyy;->b:Lixu;

    new-instance v3, Liyx;

    iget-object v1, v7, Liyy;->b:Lixu;

    invoke-direct {v3, v1}, Liyx;-><init>(Lixu;)V

    :goto_2
    if-eqz v3, :cond_b

    move v1, v5

    :goto_3
    array-length v2, p2

    if-ge v1, v2, :cond_d

    aget-object v2, p2, v1

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Liyz;->d()I

    move-result v8

    invoke-virtual {v3}, Lizb;->d()I

    move-result v9

    if-ne v8, v9, :cond_8

    const/4 v8, 0x0

    aput-object v8, p2, v1

    :goto_4
    if-eqz v2, :cond_a

    instance-of v1, v3, Liyu;

    if-eqz v1, :cond_5

    instance-of v1, v2, Lizc;

    if-eqz v1, :cond_5

    move-object v0, v2

    check-cast v0, Lizc;

    move-object v1, v0

    check-cast v3, Liyu;

    invoke-direct {p0, v1, v3}, Lixw;->a(Lizc;Liyu;)Lizb;

    move-result-object v3

    :cond_5
    iget v1, v3, Lizb;->d:I

    const/16 v8, 0x226

    if-ne v1, v8, :cond_9

    invoke-virtual {v3}, Lizb;->e()V

    new-instance v3, Lixt;

    invoke-direct {v3, v1}, Lixt;-><init>(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {p0, v2, v8, v9}, Lixw;->a(Liyz;J)Z

    :goto_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_6
    const v3, 0x8101

    if-ne v2, v3, :cond_7

    new-instance v2, Lixu;

    iget-object v3, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-direct {v2, v3, v1}, Lixu;-><init>(Ljava/io/InputStream;I)V

    iput-object v2, v7, Liyy;->b:Lixu;

    new-instance v3, Liyu;

    iget-object v1, v7, Liyy;->b:Lixu;

    invoke-direct {v3, v1}, Liyu;-><init>(Lixu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_7
    :try_start_1
    iget-object v2, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->skipBytes(I)I

    move-object v3, v4

    goto :goto_2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    invoke-direct {p0, v2, v3}, Lixw;->a(Liyz;Lizb;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :catch_1
    move-exception v1

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_a
    :try_start_2
    invoke-virtual {v3}, Lizb;->Y_()I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v3}, Lizb;->Z_()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_6
    :try_start_3
    invoke-virtual {v3}, Lizb;->e()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Request didn\'t complete"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v2}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    throw v1

    :cond_b
    :try_start_4
    iget-object v1, v7, Liyy;->b:Lixu;

    if-eqz v1, :cond_c

    const/4 v1, 0x0

    iput-object v1, v7, Liyy;->b:Lixu;

    :cond_c
    iget-object v1, v7, Liyy;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    const/4 v1, 0x0

    iput-object v1, v7, Liyy;->a:Ljava/io/DataInputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Request didn\'t complete"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto :goto_6

    :cond_d
    move-object v2, v4

    goto/16 :goto_4
.end method

.method static synthetic a(Lixw;[Liyz;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    return-void
.end method

.method public static declared-synchronized a(Liya;)V
    .locals 2

    const-class v1, Lixw;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lixw;->b:Lixw;

    if-nez v0, :cond_0

    new-instance v0, Lixw;

    invoke-direct {v0, p0}, Lixw;-><init>(Liya;)V

    sput-object v0, Lixw;->b:Lixw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Liyz;Lizb;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Liyz;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Liyz;->f()Liza;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Liza;->a(Liyz;Lizb;)V

    :cond_0
    invoke-direct {p0}, Lixw;->g()[Liyb;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-virtual {p2}, Lizb;->e()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p2}, Lizb;->e()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lizb;->e()V

    throw v0
.end method

.method private a(Ljava/util/Vector;Z)V
    .locals 6

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v4, v0, [Liyz;

    invoke-virtual {p1, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    array-length v0, v4

    new-array v1, v0, [Ljava/io/InputStream;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_0

    aget-object v2, v4, v0

    invoke-virtual {v2}, Liyz;->Z_()Ljava/io/InputStream;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Livd;

    iget-object v0, p0, Lixw;->d:Liyq;

    invoke-virtual {v0}, Liyq;->Z_()Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Livd;

    invoke-direct {v3, v1}, Livd;-><init>([Ljava/io/InputStream;)V

    invoke-direct {v2, v0, v3}, Livd;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    if-eqz p2, :cond_1

    invoke-direct {p0}, Lixw;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lixw;->q:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lixw;->n:Liyi;

    invoke-virtual {v1, v0}, Liyi;->a(Ljava/lang/String;)Liyh;

    move-result-object v3

    const-string v0, "POST"

    invoke-interface {v3, v0}, Liyh;->a(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v5

    invoke-interface {v3, v2}, Liyh;->a(Ljava/io/InputStream;)V

    iget-wide v0, p0, Lixw;->u:J

    invoke-interface {v3, v0, v1}, Liyh;->a(J)V

    const-string v0, "application/binary"

    invoke-interface {v3, v0}, Liyh;->b(Ljava/lang/String;)V

    new-instance v0, Lixz;

    iget-object v2, p0, Lixw;->c:Livs;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lixz;-><init>(Lixw;Livs;Liyh;[Liyz;I)V

    invoke-interface {v3, v0}, Liyh;->b(Livq;)V

    return-void

    :cond_1
    iget-object v0, p0, Lixw;->p:Ljava/lang/String;

    goto :goto_1
.end method

.method private a([Liyz;Ljava/lang/Exception;)V
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v4

    move v1, v0

    :goto_0
    :try_start_0
    array-length v5, p1

    if-ge v1, v5, :cond_1

    aget-object v5, p1, v1

    if-eqz v5, :cond_0

    aget-object v5, p1, v1

    invoke-direct {p0, v5, v2, v3}, Lixw;->a(Liyz;J)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    aput-object v5, p1, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lixw;->f()V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    if-eqz v1, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v1}, Liyz;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    aget-object v1, p1, v0

    invoke-virtual {v1}, Liyz;->f()Liza;

    move-result-object v1

    if-eqz v1, :cond_2

    aget-object v2, p1, v0

    invoke-interface {v1, v2, p2}, Liza;->a(Liyz;Ljava/lang/Exception;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_3
    return-void
.end method

.method private a([Liyz;Z)V
    .locals 8

    :try_start_0
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    iget-object v1, p0, Lixw;->d:Liyq;

    invoke-virtual {v1}, Liyq;->Y_()I

    move-result v3

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    array-length v1, p1

    if-ge v4, v1, :cond_3

    aget-object v2, p1, v4

    if-eqz v2, :cond_4

    instance-of v1, v2, Lizc;

    if-eqz v1, :cond_1

    move-object v0, v2

    check-cast v0, Lizc;

    move-object v1, v0

    invoke-virtual {v1}, Lizc;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v6, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v7, p0, Lixw;->w:Liye;

    invoke-virtual {v7, v1}, Liye;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    const/4 v6, 0x0

    :try_start_2
    aput-object v6, p1, v4

    check-cast v1, Lizb;

    invoke-direct {p0, v2, v1}, Lixw;->a(Liyz;Lizb;)V

    move v1, v3

    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v6

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    invoke-direct {p0, p1, v1}, Lixw;->a([Liyz;Ljava/lang/Exception;)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    :try_start_3
    invoke-virtual {v2}, Liyz;->Y_()I

    move-result v1

    add-int/2addr v1, v3

    const v6, 0x8000

    if-le v1, v6, :cond_2

    invoke-direct {p0, v5, p2}, Lixw;->a(Ljava/util/Vector;Z)V

    iget-object v1, p0, Lixw;->d:Liyq;

    invoke-virtual {v1}, Liyq;->Y_()I

    move-result v3

    invoke-virtual {v5}, Ljava/util/Vector;->removeAllElements()V

    :cond_2
    const/4 v1, 0x0

    aput-object v1, p1, v4

    invoke-virtual {v5, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v2}, Liyz;->Y_()I

    move-result v1

    add-int/2addr v1, v3

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lixw;->f()V

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-direct {p0, v5, p2}, Lixw;->a(Ljava/util/Vector;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method private a(Liyz;J)Z
    .locals 2

    invoke-virtual {p1, p2, p3}, Liyz;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, p3}, Liyz;->b(J)V

    iget-object v0, p0, Lixw;->f:Ljava/util/Hashtable;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized b()Lixw;
    .locals 2

    const-class v0, Lixw;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lixw;->b:Lixw;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lixw;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 9

    const-wide/16 v4, -0x1

    iget-object v6, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lixw;->m:Livt;

    invoke-virtual {v0}, Livt;->a()I

    iget-object v0, p0, Lixw;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v7

    move-wide v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyz;

    iget-boolean v1, v0, Liyz;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lixw;->f:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Liyz;->m:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lixw;->g:Ljava/util/Hashtable;

    iget-object v0, v0, Liyz;->m:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    :try_start_1
    iget-wide v0, v0, Liyz;->g:J

    cmp-long v8, v0, v4

    if-eqz v8, :cond_4

    cmp-long v8, v2, v4

    if-eqz v8, :cond_1

    cmp-long v8, v2, v0

    if-lez v8, :cond_4

    :cond_1
    :goto_1
    move-wide v2, v0

    goto :goto_0

    :cond_2
    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    monitor-exit v6

    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lixw;->m:Livt;

    invoke-virtual {v0, v2, v3}, Livt;->b(J)V

    iget-object v0, p0, Lixw;->m:Livt;

    invoke-virtual {v0}, Livt;->d()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_4
    move-wide v0, v2

    goto :goto_1
.end method

.method private g()[Liyb;
    .locals 3

    iget-object v1, p0, Lixw;->r:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lixw;->r:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Liyb;

    iget-object v2, p0, Lixw;->r:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized h()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lixw;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixw;->s:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)Liuv;
    .locals 1

    new-instance v0, Liyc;

    invoke-direct {v0, p1, p2}, Liyc;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final a(Liyz;Z)V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v1, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p1, Liyz;->i:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p1, Liyz;->i:J

    :cond_0
    iget-boolean v0, p1, Liyz;->h:Z

    if-eqz v0, :cond_1

    iget-wide v2, p1, Liyz;->j:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const-wide/16 v2, 0x4e20

    iput-wide v2, p1, Liyz;->j:J

    :cond_1
    invoke-direct {p0}, Lixw;->h()I

    move-result v0

    invoke-virtual {p1, v0}, Liyz;->a(I)V

    iget-wide v2, p1, Liyz;->f:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p1, Liyz;->h:Z

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lixw;->d()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_2
    iget-object v0, p0, Lixw;->f:Ljava/util/Hashtable;

    const-string v2, ""

    invoke-virtual {v0, p1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lixw;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final c()V
    .locals 11

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lixw;->h:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v0, p0, Lixw;->f:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyz;

    iget-wide v8, v0, Liyz;->f:J

    cmp-long v8, v8, v6

    if-gtz v8, :cond_0

    iget-object v8, p0, Lixw;->f:Ljava/util/Hashtable;

    invoke-virtual {v8, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, v0, Liyz;->m:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lixw;->g:Ljava/util/Hashtable;

    iget-object v9, v0, Liyz;->m:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v0, v6, v7}, Liyz;->a(J)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v8, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_4

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyz;

    iget-boolean v8, v0, Liyz;->h:Z

    if-eqz v8, :cond_3

    invoke-virtual {v0, v6, v7}, Liyz;->a(J)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v0, v6, v7}, Liyz;->c(J)V

    iget-object v8, p0, Lixw;->f:Ljava/util/Hashtable;

    const-string v9, ""

    invoke-virtual {v8, v0, v9}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_4
    move v1, v2

    :goto_2
    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyz;

    invoke-virtual {v0}, Liyz;->g()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    invoke-direct {p0}, Lixw;->f()V

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_6

    monitor-exit v3

    :goto_3
    return-void

    :cond_6
    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v6, v0, [Liyz;

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0, v6}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    iget-object v0, p0, Lixw;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Lixw;->e()Z

    move-result v0

    if-eqz v0, :cond_c

    move v3, v2

    move v1, v4

    move-object v0, v5

    :goto_4
    array-length v7, v6

    if-ge v3, v7, :cond_9

    aget-object v7, v6, v3

    iget-boolean v7, v7, Liyz;->l:Z

    if-eqz v7, :cond_8

    if-nez v0, :cond_7

    array-length v0, v6

    new-array v0, v0, [Liyz;

    :cond_7
    aget-object v7, v6, v3

    aput-object v7, v0, v3

    aput-object v5, v6, v3

    move v10, v1

    move-object v1, v0

    move v0, v10

    :goto_5
    add-int/lit8 v3, v3, 0x1

    move v10, v0

    move-object v0, v1

    move v1, v10

    goto :goto_4

    :cond_8
    aget-object v7, v6, v3

    iget-boolean v7, v7, Liyz;->h:Z

    if-eqz v7, :cond_d

    move-object v1, v0

    move v0, v2

    goto :goto_5

    :cond_9
    if-eqz v1, :cond_b

    if-eqz v0, :cond_b

    move v1, v2

    :goto_6
    array-length v3, v6

    if-ge v1, v3, :cond_b

    aget-object v3, v6, v1

    if-eqz v3, :cond_a

    aget-object v3, v6, v1

    aput-object v3, v0, v1

    aput-object v5, v6, v1

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_b
    if-eqz v0, :cond_c

    invoke-direct {p0, v0, v4}, Lixw;->a([Liyz;Z)V

    :cond_c
    invoke-direct {p0, v6, v2}, Lixw;->a([Liyz;Z)V

    goto :goto_3

    :cond_d
    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_5
.end method

.method protected final d()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0xa

    add-long/2addr v2, v0

    iget-wide v4, p0, Lixw;->k:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    const-wide/16 v4, 0x64

    add-long/2addr v0, v4

    iput-wide v0, p0, Lixw;->j:J

    iput-wide v2, p0, Lixw;->k:J

    iget-object v0, p0, Lixw;->l:Livt;

    iget-wide v1, p0, Lixw;->k:J

    invoke-virtual {v0, v1, v2}, Livt;->b(J)V

    iget-object v0, p0, Lixw;->l:Livt;

    invoke-virtual {v0}, Livt;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lixw;->j:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    iput-wide v2, p0, Lixw;->k:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lixw;->k:J

    iget-wide v2, p0, Lixw;->j:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lixw;->j:J

    iput-wide v0, p0, Lixw;->k:J

    goto :goto_0
.end method
