.class public final Lhlu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lidu;

.field final b:Lhkr;

.field public final c:Lhmb;

.field public d:Lhlz;

.field e:J


# direct methods
.method public constructor <init>(Lidu;Lhkr;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhmb;

    invoke-direct {v0, p0}, Lhmb;-><init>(Lhlu;)V

    iput-object v0, p0, Lhlu;->c:Lhmb;

    new-instance v0, Lhly;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhly;-><init>(Lhlu;B)V

    iput-object v0, p0, Lhlu;->d:Lhlz;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhlu;->e:J

    iput-object p1, p0, Lhlu;->a:Lidu;

    iput-object p2, p0, Lhlu;->b:Lhkr;

    iget-object v0, p0, Lhlu;->c:Lhmb;

    iget-object v1, p0, Lhlu;->d:Lhlz;

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lhmb;->a(Lhlz;J)V

    return-void
.end method

.method static synthetic a(Lhlu;)V
    .locals 6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "VehExitDetector"

    const-string v1, "Vehicle exited detected"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v0

    sget-object v1, Licn;->X:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v1, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v2, 0x6

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    iget-object v2, p0, Lhlu;->a:Lidu;

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->b()J

    move-result-wide v2

    iget-object v4, p0, Lhlu;->a:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    iget-object v1, p0, Lhlu;->a:Lidu;

    invoke-interface {v1, v0}, Lidu;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    new-instance v0, Lhmc;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lhmc;-><init>(Lhlu;B)V

    invoke-direct {p0, v0}, Lhlu;->a(Lhlz;)V

    return-void
.end method

.method static synthetic a(Lhlu;Lhlz;)V
    .locals 0

    invoke-direct {p0, p1}, Lhlu;->a(Lhlz;)V

    return-void
.end method

.method private a(Lhlz;)V
    .locals 8

    iget-object v0, p0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "VehExitDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Leaving state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v2}, Lhlz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->A()Licp;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v2}, Lhlz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " -> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lhlz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lidf;

    sget-object v2, Licn;->W:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lidf;-><init>(Licp;Licn;JLjava/lang/String;)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v0, p1}, Lhlz;->a(Lhlz;)V

    iput-object p1, p0, Lhlu;->d:Lhlz;

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "VehExitDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Entering state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v2}, Lhlz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhlu;->c:Lhmb;

    invoke-virtual {v0, p1, v6, v7}, Lhmb;->a(Lhlz;J)V

    iget-object v0, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v0}, Lhlz;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v1

    if-ne v2, v0, :cond_0

    const/16 v3, 0x4b

    if-gt v1, v3, :cond_1

    :cond_0
    const/4 v1, 0x2

    if-ne v2, v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    const-wide/16 v4, -0x1

    iget-wide v0, p0, Lhlu;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iget-wide v2, p0, Lhlu;->e:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-wide v0, p0, Lhlu;->e:J

    iput-wide v4, p0, Lhlu;->e:J

    invoke-virtual {p0, v0, v1}, Lhlu;->a(J)V

    goto :goto_0

    :cond_1
    iput-wide v4, p0, Lhlu;->e:J

    iget-object v0, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v0}, Lhlz;->c()V

    goto :goto_0
.end method

.method final a(J)V
    .locals 5

    iget-wide v0, p0, Lhlu;->e:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    iput-wide p1, p0, Lhlu;->e:J

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "VehExitDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Alarm set to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lhlu;->a:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->c()J

    move-result-wide v3

    add-long/2addr v3, p1

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhlu;->a:Lidu;

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, p2, v2}, Lidu;->a(IJLilx;)V

    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lhlu;->d:Lhlz;

    invoke-virtual {v0, p1}, Lhlz;->a(Z)V

    return-void
.end method
