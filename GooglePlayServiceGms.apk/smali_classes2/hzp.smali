.class public final enum Lhzp;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhzp;

.field public static final enum b:Lhzp;

.field public static final enum c:Lhzp;

.field public static final enum d:Lhzp;

.field public static final enum e:Lhzp;

.field private static final synthetic f:[Lhzp;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhzp;

    const-string v1, "UNAVAILABLE"

    invoke-direct {v0, v1, v2}, Lhzp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhzp;->a:Lhzp;

    new-instance v0, Lhzp;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lhzp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhzp;->b:Lhzp;

    new-instance v0, Lhzp;

    const-string v1, "STILL"

    invoke-direct {v0, v1, v4}, Lhzp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhzp;->c:Lhzp;

    new-instance v0, Lhzp;

    const-string v1, "WALKING"

    invoke-direct {v0, v1, v5}, Lhzp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhzp;->d:Lhzp;

    new-instance v0, Lhzp;

    const-string v1, "FAST_MOVING"

    invoke-direct {v0, v1, v6}, Lhzp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhzp;->e:Lhzp;

    const/4 v0, 0x5

    new-array v0, v0, [Lhzp;

    sget-object v1, Lhzp;->a:Lhzp;

    aput-object v1, v0, v2

    sget-object v1, Lhzp;->b:Lhzp;

    aput-object v1, v0, v3

    sget-object v1, Lhzp;->c:Lhzp;

    aput-object v1, v0, v4

    sget-object v1, Lhzp;->d:Lhzp;

    aput-object v1, v0, v5

    sget-object v1, Lhzp;->e:Lhzp;

    aput-object v1, v0, v6

    sput-object v0, Lhzp;->f:[Lhzp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhzp;
    .locals 1

    const-class v0, Lhzp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhzp;

    return-object v0
.end method

.method public static values()[Lhzp;
    .locals 1

    sget-object v0, Lhzp;->f:[Lhzp;

    invoke-virtual {v0}, [Lhzp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhzp;

    return-object v0
.end method
