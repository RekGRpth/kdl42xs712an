.class public Lifw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:Ligc;

.field private d:Lilo;

.field private final e:Landroid/content/Context;

.field private final f:Lhsy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lifw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lifw;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ligc;Lilo;Lhsy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lifw;->e:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lifw;->b:Landroid/os/Handler;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligc;

    iput-object v0, p0, Lifw;->c:Ligc;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilo;

    iput-object v0, p0, Lifw;->d:Lilo;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsy;

    iput-object v0, p0, Lifw;->f:Lhsy;

    return-void
.end method

.method static synthetic a(Lifw;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lifw;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private static a(Livi;[Liga;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x2

    const/4 v1, 0x0

    invoke-static {}, Lbgr;->d()Lbgt;

    move-result-object v4

    invoke-virtual {p0, v10}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v10}, Livi;->f(I)Livi;

    move-result-object v5

    invoke-virtual {v5, v11}, Livi;->k(I)I

    move-result v6

    invoke-virtual {v5, v10}, Livi;->k(I)I

    move-result v7

    if-nez v6, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    if-eqz v7, :cond_2

    if-eq v7, v6, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_7

    invoke-virtual {v5, v11, v2}, Livi;->a(II)I

    move-result v0

    array-length v8, p1

    if-lt v0, v8, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    aget-object v8, p1, v0

    if-nez v8, :cond_4

    move-object v0, v1

    goto :goto_0

    :cond_4
    aget-object v0, p1, v0

    invoke-virtual {v0, p2}, Liga;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v8

    if-nez v8, :cond_5

    move-object v0, v1

    goto :goto_0

    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v9, v6

    div-float/2addr v0, v9

    if-eqz v7, :cond_6

    invoke-virtual {v5, v10, v2}, Livi;->a(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v9, 0x42c80000    # 100.0f

    div-float/2addr v0, v9

    :cond_6
    invoke-static {v8, v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->a(Lcom/google/android/gms/location/places/internal/PlaceImpl;F)Lcom/google/android/gms/location/places/PlaceLikelihood;

    move-result-object v0

    invoke-static {v4, v0}, Lbgr;->a(Lbgt;Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_7
    invoke-virtual {v4, v3}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/os/Handler;Ligc;Lilo;)Lifw;
    .locals 6

    invoke-static {p0}, Life;->a(Landroid/content/Context;)V

    new-instance v0, Lifw;

    invoke-static {}, Lhsy;->a()Lhsy;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lifw;-><init>(Landroid/content/Context;Landroid/os/Handler;Ligc;Lilo;Lhsy;)V

    return-object v0
.end method

.method private static a(Lizb;Ljava/util/List;)Livi;
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x6

    iget v1, p0, Lizb;->d:I

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    sget-object v1, Lifw;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lifw;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GLS returned HTTP response code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lizb;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x7

    invoke-static {v1}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, p1}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Livi;

    sget-object v2, Lihj;->T:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    :try_start_0
    invoke-virtual {p0}, Lizb;->Z_()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2, v1}, Lilv;->a(Ljava/io/InputStream;Livi;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lifw;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lifw;->a:Ljava/lang/String;

    const-string v3, "could not parse response"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    const/16 v1, 0x8

    invoke-static {v1}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, p1}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lifw;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/common/data/DataHolder;Lerq;)V
    .locals 3

    :try_start_0
    sget-object v0, Lifw;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lifw;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "statusCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", estimate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-interface {p1, p0}, Lerq;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const-string v2, "places callback failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerq;

    invoke-static {p0, v0}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Lerq;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Livi;Liza;)V
    .locals 4

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {p1, v0}, Livi;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Liyw;

    const-string v2, "g:loc/qp"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v1, v2, v0}, Liyw;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v1, p2}, Liyz;->a(Liza;)V

    iget-object v0, p0, Lifw;->f:Lhsy;

    invoke-virtual {v0, v1}, Lhsy;->a(Liyz;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "unable to write payload"

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v1, v2}, Liza;->a(Liyz;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private a(Livi;JLjava/util/List;)[Liga;
    .locals 9

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Livi;->k(I)I

    move-result v4

    new-array v5, v4, [Liga;

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_6

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v3}, Livi;->c(II)Livi;

    move-result-object v6

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lifw;->c:Ligc;

    invoke-virtual {v1, v0, p2, p3}, Ligc;->a(Ljava/lang/String;J)Liga;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Liga;->a()Lcom/google/android/gms/location/places/internal/PlaceImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->k()J

    move-result-wide v7

    const/16 v1, 0xe

    invoke-virtual {v6, v1}, Livi;->i(I)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "PlaceCodec"

    const-string v2, "received place lacks timestamp"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, -0x1

    :goto_2
    cmp-long v1, v7, v1

    if-nez v1, :cond_5

    invoke-virtual {v0}, Liga;->b()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1, v6}, Lige;->a(Ljava/util/Map;Livi;)V

    :cond_0
    :goto_3
    if-nez v0, :cond_2

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const-string v2, "could not parse place in reply"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, p4}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    :cond_2
    aput-object v0, v5, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Livi;->g(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/16 v1, 0xe

    invoke-virtual {v6, v1}, Livi;->d(I)J

    move-result-wide v1

    goto :goto_2

    :cond_5
    invoke-static {v6}, Lige;->a(Livi;)Liga;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lifw;->c:Ligc;

    invoke-virtual {v1, v0, p2, p3}, Ligc;->a(Liga;J)V

    goto :goto_3

    :cond_6
    return-object v5
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 2

    iget-object v0, p0, Lifw;->e:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Ligz;->a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    invoke-static {p3, v0}, Ligz;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Livi;)Livi;

    move-result-object v0

    new-instance v1, Lifx;

    invoke-direct {v1, p0, p3, p4}, Lifx;-><init>(Lifw;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    invoke-direct {p0, v0, v1}, Lifw;->a(Livi;Liza;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 2

    iget-object v0, p0, Lifw;->e:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Ligz;->a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    invoke-static {p4, v0}, Ligz;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Livi;)Livi;

    move-result-object v0

    new-instance v1, Lifx;

    invoke-direct {v1, p0, p4, p5}, Lifx;-><init>(Lifw;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    invoke-direct {p0, v0, v1}, Lifw;->a(Livi;Liza;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 11

    const/4 v10, 0x1

    const/4 v0, 0x0

    const/4 v8, 0x5

    const/4 v9, 0x2

    const/4 v3, 0x0

    iget-object v1, p0, Lifw;->d:Lilo;

    invoke-virtual {v1}, Lilo;->a()J

    move-result-wide v5

    check-cast p1, Lifx;

    iget-object v7, p1, Lifx;->b:Ljava/util/List;

    iget-object v1, p1, Lifx;->c:Ljava/lang/Exception;

    if-eqz v1, :cond_1

    const/4 v0, 0x7

    invoke-static {v0}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-static {v0, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p1, Lifx;->d:Lizb;

    invoke-static {v1, v7}, Lifw;->a(Lizb;Ljava/util/List;)Livi;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Life;->e(Livi;)V

    invoke-static {v1, v10}, Ligv;->a(Livi;I)I

    move-result v2

    if-eqz v2, :cond_3

    sget-object v1, Lifw;->a:Ljava/lang/String;

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lifw;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "RPC failed with status "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v1, 0x7

    invoke-static {v1}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    move-object v4, v0

    :goto_0
    if-eqz v4, :cond_0

    invoke-direct {p0, v4, v5, v6, v7}, Lifw;->a(Livi;JLjava/util/List;)[Liga;

    move-result-object v5

    if-eqz v5, :cond_0

    move v2, v3

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerq;

    invoke-virtual {v4, v9, v2}, Livi;->c(II)Livi;

    move-result-object v6

    iget-object v1, p1, Lifx;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-static {v6, v10}, Ligv;->a(Livi;I)I

    move-result v8

    if-eqz v8, :cond_c

    invoke-static {v3}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v0}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Lerq;)V

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v9}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v1, Lifw;->a:Ljava/lang/String;

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const-string v2, "no ReplyElement"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {v3}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    move-object v4, v0

    goto :goto_0

    :cond_5
    invoke-virtual {v1, v9}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-static {v1, v10}, Ligv;->a(Livi;I)I

    move-result v2

    if-eqz v2, :cond_7

    sget-object v1, Lifw;->a:Ljava/lang/String;

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lifw;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "GLS failed with status "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {v3}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    move-object v4, v0

    goto :goto_0

    :cond_7
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_9

    sget-object v1, Lifw;->a:Ljava/lang/String;

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lifw;->a:Ljava/lang/String;

    const-string v2, "no place reply in ReplyElement"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-static {v3}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    move-object v4, v0

    goto/16 :goto_0

    :cond_9
    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-virtual {v1, v9}, Livi;->k(I)I

    move-result v2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    if-eq v2, v4, :cond_b

    sget-object v2, Lifw;->a:Ljava/lang/String;

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    sget-object v2, Lifw;->a:Ljava/lang/String;

    const-string v4, "%d query results in GPlaceQuery but expected %d"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v1, v9}, Livi;->k(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v10

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    invoke-static {v3}, Leri;->c(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v7}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/List;)V

    move-object v4, v0

    goto/16 :goto_0

    :cond_b
    move-object v4, v1

    goto/16 :goto_0

    :cond_c
    iget-object v1, v1, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {v6, v5, v1}, Lifw;->a(Livi;[Liga;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-static {v1, v0}, Lifw;->a(Lcom/google/android/gms/common/data/DataHolder;Lerq;)V

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 2

    iget-object v0, p0, Lifw;->e:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Ligz;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    invoke-static {p2, v0}, Ligz;->a(Lcom/google/android/gms/location/places/internal/PlacesParams;Livi;)Livi;

    move-result-object v0

    new-instance v1, Lifx;

    invoke-direct {v1, p0, p2, p3}, Lifx;-><init>(Lifw;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    invoke-direct {p0, v0, v1}, Lifw;->a(Livi;Liza;)V

    return-void
.end method
