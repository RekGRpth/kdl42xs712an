.class public final Ligi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/os/Handler;

.field final c:Landroid/content/pm/PackageManager;

.field final d:Lhvb;

.field final e:Ligf;

.field public final f:Lcom/google/android/location/places/PlaceSubscription;

.field final g:Ligk;

.field final h:Lign;

.field final i:Ligm;

.field final j:Landroid/app/PendingIntent$OnFinished;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lhvb;Ligf;Lcom/google/android/location/places/PlaceSubscription;Ligk;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lign;

    invoke-direct {v0, p0, v1}, Lign;-><init>(Ligi;B)V

    iput-object v0, p0, Ligi;->h:Lign;

    new-instance v0, Ligm;

    invoke-direct {v0, p0, v1}, Ligm;-><init>(Ligi;B)V

    iput-object v0, p0, Ligi;->i:Ligm;

    new-instance v0, Ligl;

    invoke-direct {v0, p0, v1}, Ligl;-><init>(Ligi;B)V

    iput-object v0, p0, Ligi;->j:Landroid/app/PendingIntent$OnFinished;

    iput-object p1, p0, Ligi;->a:Landroid/content/Context;

    iput-object p2, p0, Ligi;->b:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Ligi;->c:Landroid/content/pm/PackageManager;

    iput-object p3, p0, Ligi;->d:Lhvb;

    iput-object p4, p0, Ligi;->e:Ligf;

    iput-object p5, p0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    iput-object p6, p0, Ligi;->g:Ligk;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const-string v0, "PlaceReporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopping location updates for subscription: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {v2}, Lcom/google/android/location/places/PlaceSubscription;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Ligi;->d:Lhvb;

    iget-object v1, p0, Ligi;->h:Lign;

    invoke-virtual {v0, v1}, Lhvb;->a(Leqc;)V

    return-void
.end method
