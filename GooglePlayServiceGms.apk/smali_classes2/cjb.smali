.class public final Lcjb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcjb;->a:Ljava/util/Map;

    sget-object v0, Lcjg;->a:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->b:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->c:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->d:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->e:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->h:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->f:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->g:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->i:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->j:Lciz;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->k:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->l:Lciz;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->m:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->n:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->o:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->p:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->q:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->r:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->s:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->t:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->u:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->v:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->w:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->x:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lcjg;->y:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckh;->a:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckh;->b:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckh;->c:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckh;->d:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckh;->e:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckq;->a:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    sget-object v0, Lckq;->b:Lcjd;

    invoke-static {v0}, Lcjb;->a(Lcjd;)V

    invoke-static {}, Lcjb;->a()V

    return-void
.end method

.method public static a(Lcje;)Lcjd;
    .locals 1

    sget-object v0, Lcjb;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcjd;

    return-object v0
.end method

.method private static a()V
    .locals 4

    invoke-static {}, Lckw;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    sget-object v2, Lcjb;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No registered converter for field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcje;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-object v0, Lcjb;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    iget-object v2, v0, Lcje;->a:Ljava/lang/String;

    invoke-static {v2}, Lckw;->a(Ljava/lang/String;)Lcje;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Registered converter for unregistered field: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcje;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    return-void
.end method

.method private static a(Lcjd;)V
    .locals 2

    iget-object v0, p0, Lcjd;->a:Lcje;

    instance-of v0, v0, Lcja;

    if-eqz v0, :cond_0

    instance-of v0, p0, Lciz;

    const-string v1, "Converters for collection fields must be collection converters."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    sget-object v0, Lcjb;->a:Ljava/util/Map;

    iget-object v1, p0, Lcjd;->a:Lcje;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
