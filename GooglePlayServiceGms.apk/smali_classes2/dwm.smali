.class final Ldwm;
.super Ldwj;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldvv;


# direct methods
.method public constructor <init>(Ldvv;)V
    .locals 2

    const/4 v1, 0x1

    iput-object p1, p0, Ldwm;->a:Ldvv;

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0, v1, v1}, Ldwj;-><init>(Ldvv;IIZ)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 5

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b020f    # com.google.android.gms.R.string.games_gcore_notification_scope

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0a0082    # com.google.android.gms.R.id.summary

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Ldwm;->a:Ldvv;

    invoke-static {v1}, Ldvv;->b(Ldvv;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const v0, 0x7f0a01ae    # com.google.android.gms.R.id.widget_frame

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v1, p0, Ldwm;->a:Ldvv;

    invoke-static {v1}, Ldvv;->b(Ldvv;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    const v1, 0x7f0b0460    # com.google.android.gms.R.string.common_chips_label_only_you

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ldwm;->a:Ldvv;

    invoke-static {v1}, Ldvv;->b(Ldvv;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v1, p0, Ldwm;->a:Ldvv;

    invoke-static {v1}, Ldvv;->b(Ldvv;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_2
    const-string v1, ", "

    invoke-static {v1, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
