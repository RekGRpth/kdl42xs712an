.class public abstract Lbrp;
.super Lbro;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lbrc;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lbro;-><init>(Lbrc;Lchq;)V

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbrp;->b:Lbrc;

    invoke-interface {v0}, Lbrc;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lbrm;

    const/16 v1, 0xa

    const-string v2, "App is not authorized to make this request."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lbrp;->b()V

    return-void
.end method

.method public abstract b()V
.end method
