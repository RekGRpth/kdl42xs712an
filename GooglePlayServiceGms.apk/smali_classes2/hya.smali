.class public final Lhya;
.super Lhxs;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V
    .locals 0

    invoke-direct {p0, p1}, Lhxs;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 5

    const/4 v0, 0x1

    invoke-super {p0, p1}, Lhxs;->a(Landroid/location/Location;)V

    iget-wide v1, p0, Lhya;->c:D

    iget-object v3, p0, Lhya;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v3}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v3

    float-to-double v3, v3

    cmpg-double v1, v1, v3

    if-gtz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    iput-byte v0, p0, Lhya;->b:B

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method
