.class public final Liay;
.super Lhth;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/Map;

.field final synthetic b:J

.field final synthetic c:Libb;

.field final synthetic d:Liax;


# direct methods
.method public constructor <init>(Liax;JLibb;)V
    .locals 1

    iput-object p1, p0, Liay;->d:Liax;

    const/4 v0, 0x0

    iput-object v0, p0, Liay;->a:Ljava/util/Map;

    iput-wide p2, p0, Liay;->b:J

    iput-object p4, p0, Liay;->c:Libb;

    invoke-direct {p0}, Lhth;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhtf;)V
    .locals 9

    const-wide v7, 0x3e7ad7f29abcaf48L    # 1.0E-7

    iget-object v0, p0, Liay;->d:Liax;

    iget-object v1, p0, Liay;->a:Ljava/util/Map;

    iget-wide v2, p0, Liay;->b:J

    invoke-virtual {v0, p1, v1, v2, v3}, Liax;->a(Lhtf;Ljava/util/Map;J)Lhno;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, v1, Lhno;->d:Ljava/lang/Object;

    check-cast v0, Lhug;

    invoke-virtual {v0}, Lhug;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Liay;->c:Libb;

    iget-object v0, v1, Lhno;->d:Ljava/lang/Object;

    check-cast v0, Lhug;

    iget v1, v0, Lhug;->d:I

    int-to-double v3, v1

    mul-double/2addr v3, v7

    iget v1, v0, Lhug;->e:I

    int-to-double v5, v1

    mul-double/2addr v5, v7

    iget v1, v0, Lhug;->f:I

    div-int/lit16 v1, v1, 0x3e8

    iget v0, v0, Lhug;->g:I

    iget v7, v2, Libb;->c:I

    iget-object v8, v2, Libb;->g:[I

    array-length v8, v8

    if-ge v7, v8, :cond_1

    const/16 v7, 0x1388

    if-gt v1, v7, :cond_1

    iget-wide v7, v2, Libb;->a:D

    add-double/2addr v7, v3

    iput-wide v7, v2, Libb;->a:D

    iget-wide v7, v2, Libb;->b:D

    add-double/2addr v7, v5

    iput-wide v7, v2, Libb;->b:D

    iget v7, v2, Libb;->d:I

    if-le v0, v7, :cond_0

    iput v0, v2, Libb;->d:I

    :cond_0
    iget-object v0, v2, Libb;->e:[D

    iget v7, v2, Libb;->c:I

    aput-wide v3, v0, v7

    iget-object v0, v2, Libb;->f:[D

    iget v3, v2, Libb;->c:I

    aput-wide v5, v0, v3

    iget-object v0, v2, Libb;->g:[I

    iget v3, v2, Libb;->c:I

    aput v1, v0, v3

    iget v0, v2, Libb;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Libb;->c:I

    :cond_1
    return-void
.end method
