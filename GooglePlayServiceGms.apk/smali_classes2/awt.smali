.class public abstract Lawt;
.super Laxx;
.source "SourceFile"


# static fields
.field private static b:Ljava/security/PublicKey;


# instance fields
.field private c:I

.field private d:I

.field private final e:Ljava/lang/String;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "MIIDhzCCAm+gAwIBAgIBATANBgkqhkiG9w0BAQUFADB8MQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNTW91bnRhaW4gVmlldzETMBEGA1UECgwKR29vZ2xlIEluYzESMBAGA1UECwwJR29vZ2xlIFRWMRcwFQYDVQQDDA5FdXJla2EgUm9vdCBDQTAeFw0xMjEyMTkwMDQ3MTJaFw0zMjEyMTQwMDQ3MTJaMH0xCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRYwFAYDVQQHDA1Nb3VudGFpbiBWaWV3MRMwEQYDVQQKDApHb29nbGUgSW5jMRIwEAYDVQQLDAlHb29nbGUgVFYxGDAWBgNVBAMMD0V1cmVrYSBHZW4xIElDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALwigL2A9johADuudl41fz3DZFxVlIY0LwWHKM33aYwXs1CnuIL638dDLdZ+q6BvtxNygKRHFcEgmVDN7BRiCVukmM3SQbY2Tv/oLjIwSoGoQqNsmzNuyrL1U2bgJ1OGGoUepzk/SneO+1RmZvtYVMBeOcf1UAYL4IrUzuFqVR+LFwDmaaMn5gglaTwSnY0FLNYuojHetFJQ1iBJ3nGg+a0gQBLx3SXr1ea4NvTWj3/KQ9zXEFvmP1GKhbPz//YDLcsjT5ytGOeTBYysUpr3TOmZer5ufk0K48YcqZP6OqWRXRy9ZuvMYNyGdMrP+JIcmH1X+mFHnquAt+RIgCqSxRsCAwEAAaMTMBEwDwYDVR0TBAgwBgEB/wIBATANBgkqhkiG9w0BAQUFAAOCAQEAi9Shsc9dzXtsSEpBH1MvGC0yRf+eq9NzPh8i1+r6AeZzAw8rxiW7pe7F9UXLJBIqrcJdBfR69cKbEBZa0QpzxRY5oBDK0WiFnvueJoOOWPN3oE7l25e+LQBf9ZTbsZ1la/3w0QRR38ySppktcfVN1SP+MxyptKvFvxq40YDvicniH5xMSDui+gIK3IQBiocC+1nup0wEfXSZh2olRK0WquxONRt8e4TJsT/hgnDlDefZbfqVtsXkHugRm9iy86T9E/ODT/cHFCC7IqWmj9a126l0eOKTDeUjLwUX4LKXZzRND5x2Q3umIUpWBfYqfPJ/EpSCJikH8AtsbHkUsHTVbA=="

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    sput-object v0, Lawt;->b:Ljava/security/PublicKey;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "DeviceAuthChannel"

    const-string v2, "Chromecast ICA cert."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 2

    const-string v0, "urn:x-cast:com.google.cast.tp.deviceauth"

    const-string v1, "DeviceAuthChannel"

    invoke-direct {p0, v0, v1}, Laxx;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lawt;->c:I

    iput-object p2, p0, Lawt;->f:[B

    iput-object p1, p0, Lawt;->e:Ljava/lang/String;

    return-void
.end method

.method private b(I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iput p1, p0, Lawt;->c:I

    if-nez p1, :cond_0

    iget-object v0, p0, Lawt;->a:Laye;

    const-string v1, "Device authentication succeeded."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p0, p1}, Lawt;->a(I)V

    return-void

    :cond_0
    if-ne p1, v5, :cond_1

    iget-object v0, p0, Lawt;->a:Laye;

    const-string v1, "Device authentication failed: %s %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lawt;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lawt;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lawt;->a:Laye;

    const-string v1, "Device authentication failed: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget v3, p0, Lawt;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    new-instance v0, Lbab;

    invoke-direct {v0}, Lbab;-><init>()V

    new-instance v1, Lazy;

    invoke-direct {v1}, Lazy;-><init>()V

    invoke-virtual {v0, v1}, Lbab;->a(Lazy;)Lbab;

    invoke-virtual {v0}, Lbab;->d()[B

    move-result-object v0

    iget-object v1, p0, Lawt;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lawt;->a([BLjava/lang/String;)V

    return-void
.end method

.method protected abstract a(I)V
.end method

.method public final a([B)V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    new-instance v0, Lbab;

    invoke-direct {v0}, Lbab;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lizk;->a([BI)Lizk;

    move-result-object v0

    check-cast v0, Lbab;
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lawt;->a:Laye;

    const-string v2, "Received a protobuf: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lbab;->a:Lazy;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lawt;->a:Laye;

    const-string v1, "Received DeviceAuthMessage with challenge instead of response (ignored)."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lawt;->a:Laye;

    const-string v2, "Received an unparseable protobuf (ignored): %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lizj;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lbab;->c:Lazz;

    if-eqz v1, :cond_1

    iget v0, v1, Lazz;->a:I

    iput v0, p0, Lawt;->d:I

    invoke-direct {p0, v5}, Lawt;->b(I)V

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lbab;->b:Lbaa;

    if-nez v2, :cond_2

    iget-object v0, p0, Lawt;->a:Laye;

    const-string v1, "Received DeviceAuthMessage with no response (ignored)."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, v2, Lbaa;->b:Lizf;

    invoke-virtual {v3}, Lizf;->b()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_4

    :try_start_2
    const-string v1, "SHA1withRSA"

    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v1

    sget-object v3, Lawt;->b:Ljava/security/PublicKey;

    invoke-virtual {v1, v3}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getTBSCertificate()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/Signature;->update([B)V

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/Signature;->verify([B)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lawt;->b(I)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/security/SignatureException; {:try_start_2 .. :try_end_2} :catch_7

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v3, p0, Lawt;->a:Laye;

    const-string v4, "SHA1withRSA"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4, v5}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    :goto_1
    :try_start_3
    const-string v1, "SHA1withRSA"

    invoke-static {v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v1

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    iget-object v0, p0, Lawt;->f:[B

    invoke-virtual {v1, v0}, Ljava/security/Signature;->update([B)V

    iget-object v0, v2, Lbaa;->a:Lizf;

    invoke-virtual {v0}, Lizf;->b()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lawt;->b(I)V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/security/SignatureException; {:try_start_3 .. :try_end_3} :catch_9

    goto/16 :goto_0

    :catch_2
    move-exception v0

    iget-object v1, p0, Lawt;->a:Laye;

    const-string v2, "SHA1withRSA"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    invoke-direct {p0, v6}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-direct {p0, v7}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_4
    move-exception v0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_5
    move-exception v1

    iget-object v3, p0, Lawt;->a:Laye;

    const-string v4, "ICA public key"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v4, v5}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_6
    move-exception v0

    invoke-direct {p0, v7}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_7
    move-exception v0

    invoke-direct {p0, v8}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_8
    move-exception v0

    invoke-direct {p0, v7}, Lawt;->b(I)V

    goto/16 :goto_0

    :catch_9
    move-exception v0

    invoke-direct {p0, v9}, Lawt;->b(I)V

    goto/16 :goto_0
.end method
