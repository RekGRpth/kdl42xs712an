.class public abstract Lcjd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcje;

.field private final b:Lceg;


# direct methods
.method public constructor <init>(Lcje;Lceg;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "field"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcje;

    iput-object v0, p0, Lcjd;->a:Lcje;

    iput-object p2, p0, Lcjd;->b:Lceg;

    return-void
.end method


# virtual methods
.method protected abstract a(Lcfp;)Ljava/lang/Object;
.end method

.method protected abstract a(Lorg/json/JSONObject;)Ljava/lang/Object;
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method final a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 2

    const-string v0, "entry"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "changeSet"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p0, p1}, Lcjd;->a(Lcfp;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    return-void
.end method

.method protected abstract a(Lcfp;Ljava/lang/Object;)V
.end method

.method protected abstract a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V
.end method

.method final a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcfp;)V
    .locals 1

    const-string v0, "entry"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcjd;->a(Lcfp;Ljava/lang/Object;)V

    return-void
.end method

.method final a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/internal/model/File;)V
    .locals 1

    const-string v0, "changeSet"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "serverFile"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, v0}, Lcjd;->a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "collection"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "jsonObject"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcje;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcjd;->a:Lcje;

    iget-object v0, v0, Lcje;->a:Ljava/lang/String;

    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p2, v0}, Lcjd;->a(Lorg/json/JSONObject;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Lorg/json/JSONObject;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 2

    const-string v0, "jsonObject"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "collection"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcjd;->a:Lcje;

    iget-object v0, v0, Lcje;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjd;->a:Lcje;

    iget-object v0, v0, Lcje;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcjd;->a:Lcje;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcjd;->a:Lcje;

    invoke-virtual {p0, p1}, Lcjd;->a(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected abstract a(Lorg/json/JSONObject;Ljava/lang/Object;)V
.end method

.method public b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcjd;->b:Lceg;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcjd;->a:Lcje;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be read directly from the database."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcjd;->b:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
