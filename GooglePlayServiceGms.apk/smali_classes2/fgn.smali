.class final Lfgn;
.super Lfgu;
.source "SourceFile"


# instance fields
.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Ljava/util/List;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

.field final synthetic j:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    .locals 0

    iput-object p8, p0, Lfgn;->c:Ljava/lang/String;

    iput-object p9, p0, Lfgn;->d:Ljava/util/List;

    iput-object p10, p0, Lfgn;->e:Ljava/util/List;

    iput-object p11, p0, Lfgn;->f:Ljava/lang/String;

    iput-object p12, p0, Lfgn;->g:Ljava/lang/String;

    iput-object p13, p0, Lfgn;->h:Ljava/lang/String;

    iput-object p14, p0, Lfgn;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p15, p0, Lfgn;->j:Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 8

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lfgn;->c:Ljava/lang/String;

    iget-object v4, p0, Lfgn;->d:Ljava/util/List;

    iget-object v5, p0, Lfgn;->e:Ljava/util/List;

    move-object v0, p2

    move-object v1, p1

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lffu;->a(Lffe;Landroid/content/Context;Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v7

    iget-object v0, p0, Lfgn;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lfgn;->g:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lfgn;->h:Ljava/lang/String;

    iget-object v3, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    iget-object v4, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lfgn;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lffu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iget-object v1, p0, Lfgn;->h:Ljava/lang/String;

    iget-object v2, p0, Lfgn;->j:Ljava/lang/String;

    iget-object v3, p0, Lfgn;->c:Ljava/lang/String;

    iget-object v4, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/util/List;

    iget-object v5, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I

    move-result v0

    iget-object v1, p0, Lfgn;->j:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lfgn;->a:Landroid/content/Context;

    invoke-static {v1}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v1

    invoke-virtual {v1}, Lfbc;->h()Lfhz;

    move-result-object v1

    iget-object v2, p0, Lfgn;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lfhz;->c(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :goto_1
    const-string v1, "added_circles"

    iget-object v0, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v1, "removed_circles"

    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v6, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    invoke-direct {v0, v1, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    iget-object v1, p0, Lfgn;->f:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfgn;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v1, p0, Lfgn;->h:Ljava/lang/String;

    iget-object v2, p0, Lfgn;->j:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lfhz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method
