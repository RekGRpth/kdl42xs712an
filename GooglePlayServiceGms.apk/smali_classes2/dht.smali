.class public final Ldht;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ldhs;

.field public b:Ldhg;

.field public c:I

.field public d:Ldgx;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v1, p0, Ldht;->a:Ldhs;

    iput-object v1, p0, Ldht;->b:Ldhg;

    const/4 v0, 0x0

    iput v0, p0, Ldht;->c:I

    iput-object v1, p0, Ldht;->d:Ldgx;

    const/4 v0, -0x1

    iput v0, p0, Ldht;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ldht;->a:Ldhs;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ldht;->a:Ldhs;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ldht;->b:Ldhg;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ldht;->b:Ldhg;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ldht;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ldht;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ldht;->d:Ldgx;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ldht;->d:Ldgx;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Ldht;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ldht;->a:Ldhs;

    if-nez v0, :cond_1

    new-instance v0, Ldhs;

    invoke-direct {v0}, Ldhs;-><init>()V

    iput-object v0, p0, Ldht;->a:Ldhs;

    :cond_1
    iget-object v0, p0, Ldht;->a:Ldhs;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ldht;->b:Ldhg;

    if-nez v0, :cond_2

    new-instance v0, Ldhg;

    invoke-direct {v0}, Ldhg;-><init>()V

    iput-object v0, p0, Ldht;->b:Ldhg;

    :cond_2
    iget-object v0, p0, Ldht;->b:Ldhg;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ldht;->c:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Ldht;->d:Ldgx;

    if-nez v0, :cond_3

    new-instance v0, Ldgx;

    invoke-direct {v0}, Ldgx;-><init>()V

    iput-object v0, p0, Ldht;->d:Ldgx;

    :cond_3
    iget-object v0, p0, Ldht;->d:Ldgx;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Ldht;->a:Ldhs;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ldht;->a:Ldhs;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Ldht;->b:Ldhg;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ldht;->b:Ldhg;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget v0, p0, Ldht;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ldht;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    iget-object v0, p0, Ldht;->d:Ldgx;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ldht;->d:Ldgx;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ldht;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ldht;

    iget-object v2, p0, Ldht;->a:Ldhs;

    if-nez v2, :cond_3

    iget-object v2, p1, Ldht;->a:Ldhs;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ldht;->a:Ldhs;

    iget-object v3, p1, Ldht;->a:Ldhs;

    invoke-virtual {v2, v3}, Ldhs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ldht;->b:Ldhg;

    if-nez v2, :cond_5

    iget-object v2, p1, Ldht;->b:Ldhg;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ldht;->b:Ldhg;

    iget-object v3, p1, Ldht;->b:Ldhg;

    invoke-virtual {v2, v3}, Ldhg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Ldht;->c:I

    iget v3, p1, Ldht;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ldht;->d:Ldgx;

    if-nez v2, :cond_8

    iget-object v2, p1, Ldht;->d:Ldgx;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ldht;->d:Ldgx;

    iget-object v3, p1, Ldht;->d:Ldgx;

    invoke-virtual {v2, v3}, Ldgx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ldht;->a:Ldhs;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldht;->b:Ldhg;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ldht;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldht;->d:Ldgx;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ldht;->a:Ldhs;

    invoke-virtual {v0}, Ldhs;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldht;->b:Ldhg;

    invoke-virtual {v0}, Ldhg;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ldht;->d:Ldgx;

    invoke-virtual {v1}, Ldgx;->hashCode()I

    move-result v1

    goto :goto_2
.end method
