.class public final Lhnt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/File;

.field final b:[B

.field final c:Lidq;

.field final d:Lhox;

.field final e:Lhnw;

.field final f:Lhuj;

.field private g:Ljava/util/concurrent/ExecutorService;

.field private final h:Lhpk;

.field private final i:Lhpc;


# direct methods
.method public constructor <init>(IILjava/util/concurrent/ExecutorService;Lhuj;Lhpc;Ljava/io/File;[BLidq;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lt p1, p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Memory capacity is expected to be larger than disk capacity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p4, p0, Lhnt;->f:Lhuj;

    iput-object p6, p0, Lhnt;->a:Ljava/io/File;

    iput-object p7, p0, Lhnt;->b:[B

    if-nez p7, :cond_1

    iput-object v1, p0, Lhnt;->h:Lhpk;

    :goto_0
    iput-object p3, p0, Lhnt;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lhox;

    invoke-direct {v0, p1}, Lhox;-><init>(I)V

    iput-object v0, p0, Lhnt;->d:Lhox;

    iget-object v0, p0, Lhnt;->h:Lhpk;

    if-eqz v0, :cond_2

    new-instance v0, Lhnw;

    invoke-direct {v0, p2, p3, p6}, Lhnw;-><init>(ILjava/util/concurrent/ExecutorService;Ljava/io/File;)V

    iput-object v0, p0, Lhnt;->e:Lhnw;

    :goto_1
    iput-object p5, p0, Lhnt;->i:Lhpc;

    iput-object p8, p0, Lhnt;->c:Lidq;

    return-void

    :cond_1
    invoke-static {p7, v1}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v0

    iput-object v0, p0, Lhnt;->h:Lhpk;

    goto :goto_0

    :cond_2
    iput-object v1, p0, Lhnt;->e:Lhnw;

    goto :goto_1
.end method

.method static a(Ljava/io/Closeable;)V
    .locals 4

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_0

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error while closing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    iget-object v0, p0, Lhnt;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;J)Ljava/lang/Object;
    .locals 5

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnt;->d:Lhox;

    invoke-virtual {v0, p1}, Lhox;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuo;

    iget-object v1, p0, Lhnt;->h:Lhpk;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v1, p1}, Lhnw;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhuo;

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lhnt;->h:Lhpk;

    if-eqz v2, :cond_0

    iput-wide p2, v1, Lhuo;->c:J

    :cond_0
    invoke-virtual {v0, p2, p3}, Lhuo;->a(J)Ljava/lang/Object;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move-object v1, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    if-eqz v1, :cond_4

    const-string v0, ""

    iget-object v3, v1, Lhuo;->a:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lhuo;

    iget-wide v3, v1, Lhuo;->b:J

    invoke-direct {v0, v2, v3, v4}, Lhuo;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lhnt;->d:Lhox;

    invoke-virtual {v1, p1, v0}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v2

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    iget-object v0, p0, Lhnt;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lhnu;

    invoke-direct {v3, p0, p1, v1}, Lhnu;-><init>(Lhnt;Ljava/lang/Object;Lhuo;)V

    invoke-interface {v0, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-object v0, v2

    goto :goto_1

    :cond_4
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "DiskTemporalCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Element for key "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " not in memory or on disk."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method

.method public final declared-synchronized a()V
    .locals 7

    const/4 v5, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnt;->h:Lhpk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lhnt;->a:Ljava/io/File;

    const-string v2, "lru.cache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    :try_start_2
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v0

    if-eq v0, v5, :cond_4

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    const-string v2, "DiskTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Mismatched version number: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lhnt;->d()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_3
    :goto_0
    :try_start_4
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    :goto_1
    iget-object v0, p0, Lhnt;->d:Lhox;

    invoke-virtual {v0}, Lhox;->size()I

    move-result v0

    iget-object v1, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v1}, Lhnw;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    iget-object v2, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v2}, Lhnw;->size()I

    move-result v2

    iget-object v4, p0, Lhnt;->d:Lhox;

    invoke-virtual {v4}, Lhox;->a()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0}, Lhnw;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v3

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    if-ge v0, v2, :cond_7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    :try_start_5
    iget-object v0, p0, Lhnt;->h:Lhpk;

    invoke-virtual {v0, v1}, Lhpk;->a(Ljava/io/DataInputStream;)Lhue;

    move-result-object v0

    iget-object v2, p0, Lhnt;->i:Lhpc;

    iget-object v4, p0, Lhnt;->e:Lhnw;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    iget-object v0, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v0, [B

    invoke-direct {v5, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-interface {v2, v4, v5}, Lhpc;->a(Lhox;Ljava/io/InputStream;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "DiskTemporalCache"

    const-string v2, "Loaded %d entries from file %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v6}, Lhnw;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "lru.cache"

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_3
    :try_start_6
    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_5

    const-string v2, "DiskTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "On disk LRU cache file not found: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-direct {p0}, Lhnt;->d()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_8
    sget-boolean v2, Licj;->c:Z

    if-eqz v2, :cond_6

    const-string v2, "DiskTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException while reading LRU cache file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0}, Lhnw;->clear()V

    invoke-direct {p0}, Lhnt;->d()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    goto/16 :goto_1

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_5
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_7
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_8
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lhnt;->a(Ljava/lang/Object;J)Ljava/lang/Object;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_3
.end method

.method public final declared-synchronized a(JJ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnt;->d:Lhox;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhox;->a(JJ)V

    iget-object v0, p0, Lhnt;->h:Lhpk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhnw;->a(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;Livi;J)V
    .locals 4

    iget-object v0, p0, Lhnt;->h:Lhpk;

    if-nez v0, :cond_1

    new-instance v0, Lhuo;

    iget-object v1, p0, Lhnt;->f:Lhuj;

    invoke-virtual {v1, p2}, Lhuj;->a(Livi;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1, p3, p4}, Lhuo;-><init>(Ljava/lang/Object;J)V

    iget-object v1, p0, Lhnt;->d:Lhox;

    invoke-virtual {v1, p1, v0}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0, p1}, Lhnw;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Lhuo;

    iget-object v0, p0, Lhnt;->f:Lhuj;

    invoke-virtual {v0, p2}, Lhuj;->a(Livi;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0, p3, p4}, Lhuo;-><init>(Ljava/lang/Object;J)V

    new-instance v2, Lhuo;

    if-nez p2, :cond_2

    const-string v0, ""

    :goto_1
    invoke-direct {v2, v0, p3, p4}, Lhuo;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnt;->d:Lhox;

    invoke-virtual {v0, p1, v1}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0, p1, v2}, Lhnw;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".cache"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, Lhnt;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lhnv;

    invoke-direct {v1, p0, v2, p2, p1}, Lhnv;-><init>(Lhnt;Lhuo;Livi;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnt;->h:Lhpk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhnt;->e:Lhnw;

    invoke-virtual {v0, p1}, Lhnw;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lhnt;->d:Lhox;

    invoke-virtual {v0, p1}, Lhox;->containsKey(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 6

    iget-object v0, p0, Lhnt;->h:Lhpk;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Ljava/io/File;

    iget-object v0, p0, Lhnt;->a:Ljava/io/File;

    const-string v1, "lru.cache"

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    iget-object v0, p0, Lhnt;->c:Lidq;

    invoke-interface {v0, v3}, Lidq;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v2, p0, Lhnt;->i:Lhpc;

    iget-object v4, p0, Lhnt;->e:Lhnw;

    invoke-interface {v2, v4, v0}, Lhpc;->a(Lhox;Ljava/io/OutputStream;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v2, p0, Lhnt;->h:Lhpk;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lhpk;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    move-exception v0

    :goto_1
    :try_start_5
    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_1

    const-string v2, "DiskTemporalCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find LRU cache file to write to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_1
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_6
    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "DiskTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException while writing LRU cache file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lhnt;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lhnt;->d()V

    iget-object v0, p0, Lhnt;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method
