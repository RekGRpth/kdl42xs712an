.class final Lhnu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Lhuo;

.field final synthetic c:Lhnt;


# direct methods
.method constructor <init>(Lhnt;Ljava/lang/Object;Lhuo;)V
    .locals 0

    iput-object p1, p0, Lhnu;->c:Lhnt;

    iput-object p2, p0, Lhnu;->a:Ljava/lang/Object;

    iput-object p3, p0, Lhnu;->b:Lhuo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    const/4 v9, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v1, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhox;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :cond_0
    monitor-exit p0

    new-instance v6, Ljava/io/File;

    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v1, v0, Lhnt;->a:Ljava/io/File;

    iget-object v0, p0, Lhnu;->b:Lhuo;

    iget-object v0, v0, Lhuo;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v6, v1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_1
    new-instance v0, Lidx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lhnu;->c:Lhnt;

    iget-object v4, v4, Lhnt;->b:[B

    iget-object v5, p0, Lhnu;->c:Lhnt;

    iget-object v5, v5, Lhnt;->f:Lhuj;

    iget-object v5, v5, Lhuj;->a:Livk;

    sget-object v7, Lidx;->a:Lidz;

    iget-object v8, p0, Lhnu;->c:Lhnt;

    iget-object v8, v8, Lhnt;->c:Lidq;

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    invoke-virtual {v0}, Lidx;->a()Livi;

    move-result-object v0

    new-instance v1, Lhuo;

    iget-object v2, p0, Lhnu;->c:Lhnt;

    iget-object v2, v2, Lhnt;->f:Lhuj;

    invoke-virtual {v2, v0}, Lhuj;->a(Livi;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lhnu;->b:Lhuo;

    iget-wide v2, v2, Lhuo;->b:J

    invoke-direct {v1, v0, v2, v3}, Lhuo;-><init>(Ljava/lang/Object;J)V

    monitor-enter p0
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v2, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "DiskTemporalCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully loaded element "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from disk"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_1
    invoke-static {v9}, Lhnt;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit p0

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :catch_0
    move-exception v0

    :try_start_5
    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_2

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "File not on disk even though in disk LRU: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :try_start_6
    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v1, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhox;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->e:Lhnw;

    iget-object v1, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhnw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-static {v9}, Lhnt;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit p0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :catchall_3
    move-exception v0

    invoke-static {v9}, Lhnt;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v0

    :try_start_8
    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_3

    const-string v1, "DiskTemporalCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException while reading element from disk: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    monitor-enter p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :try_start_9
    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->d:Lhox;

    iget-object v1, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhox;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhnu;->c:Lhnt;

    iget-object v0, v0, Lhnt;->e:Lhnw;

    iget-object v1, p0, Lhnu;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lhnw;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    invoke-static {v9}, Lhnt;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit p0

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3
.end method
