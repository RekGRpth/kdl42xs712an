.class final Lhlw;
.super Lhlz;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhlu;

.field private c:J


# direct methods
.method private constructor <init>(Lhlu;)V
    .locals 2

    iput-object p1, p0, Lhlw;->a:Lhlu;

    invoke-direct {p0, p1}, Lhlz;-><init>(Lhlu;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhlw;->c:J

    return-void
.end method

.method synthetic constructor <init>(Lhlu;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhlw;-><init>(Lhlu;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    invoke-super {p0}, Lhlz;->a()V

    iget-object v0, p0, Lhlw;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhlw;->c:J

    iget-object v0, p0, Lhlw;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    invoke-virtual {v0}, Lhkr;->f()V

    return-void
.end method

.method final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lhlw;->a:Lhlu;

    invoke-static {p1}, Lhlu;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhlw;->a:Lhlu;

    invoke-static {v0}, Lhlu;->a(Lhlu;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lhlw;->a:Lhlu;

    new-instance v1, Lhlx;

    iget-object v2, p0, Lhlw;->a:Lhlu;

    invoke-direct {v1, v2, v4}, Lhlx;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhlw;->a:Lhlu;

    invoke-static {p1}, Lhlu;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhlw;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhlw;->c:J

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhlw;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lhlw;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1b7740

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "VehExitDetector"

    const-string v1, "Timed out while InVehicle state. Missed the transition?"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhlw;->a:Lhlu;

    new-instance v1, Lhmc;

    iget-object v2, p0, Lhlw;->a:Lhlu;

    invoke-direct {v1, v2, v4}, Lhmc;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    goto :goto_0
.end method

.method protected final a(Lhlz;)V
    .locals 1

    invoke-super {p0, p1}, Lhlz;->a(Lhlz;)V

    iget-object v0, p0, Lhlw;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    invoke-virtual {v0}, Lhkr;->g()V

    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "InVehicle"

    return-object v0
.end method
