.class public final Lhmx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:D

.field public final b:D

.field public final c:D

.field public final d:D

.field public final e:D

.field public final f:D

.field private final g:[D

.field private final h:[D

.field private final i:[D

.field private final j:[D

.field private final k:[D

.field private final l:[D

.field private final m:[D

.field private final n:[D


# direct methods
.method public constructor <init>(DDDDDD[D[D[D[D[D[D[D)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lhmx;->a:D

    iput-wide p3, p0, Lhmx;->b:D

    iput-wide p5, p0, Lhmx;->c:D

    iput-wide p7, p0, Lhmx;->d:D

    iput-wide p9, p0, Lhmx;->e:D

    move-wide/from16 v0, p11

    iput-wide v0, p0, Lhmx;->f:D

    move-object/from16 v0, p13

    iput-object v0, p0, Lhmx;->g:[D

    move-object/from16 v0, p14

    iput-object v0, p0, Lhmx;->h:[D

    move-object/from16 v0, p15

    iput-object v0, p0, Lhmx;->i:[D

    move-object/from16 v0, p15

    array-length v2, v0

    new-array v2, v2, [D

    iput-object v2, p0, Lhmx;->j:[D

    const/4 v2, 0x0

    iget-object v3, p0, Lhmx;->j:[D

    const/4 v4, 0x0

    move-object/from16 v0, p15

    array-length v5, v0

    move-object/from16 v0, p15

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p16

    iput-object v0, p0, Lhmx;->k:[D

    move-object/from16 v0, p17

    iput-object v0, p0, Lhmx;->l:[D

    move-object/from16 v0, p18

    iput-object v0, p0, Lhmx;->m:[D

    move-object/from16 v0, p19

    iput-object v0, p0, Lhmx;->n:[D

    return-void
.end method

.method private static a(D[D)D
    .locals 4

    array-length v1, p2

    int-to-double v2, v1

    mul-double/2addr v2, p0

    double-to-int v0, v2

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    aget-wide v0, p2, v0

    return-wide v0
.end method


# virtual methods
.method public final a()D
    .locals 2

    iget-object v0, p0, Lhmx;->g:[D

    iget-object v1, p0, Lhmx;->g:[D

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public final a(D)D
    .locals 2

    iget-object v0, p0, Lhmx;->g:[D

    invoke-static {p1, p2, v0}, Lhmx;->a(D[D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(I)D
    .locals 2

    iget-object v0, p0, Lhmx;->i:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final b(D)D
    .locals 2

    iget-object v0, p0, Lhmx;->h:[D

    invoke-static {p1, p2, v0}, Lhmx;->a(D[D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final b(I)D
    .locals 2

    iget-object v0, p0, Lhmx;->k:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final c(I)D
    .locals 2

    iget-object v0, p0, Lhmx;->l:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ActivityFeatures [accelStandardDeviationRatio="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lhmx;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accelMeanCrossingRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhmx;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyStandardDeviationRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhmx;->e:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhmx;->i:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", frequencyTangentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhmx;->k:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dominantFrequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhmx;->l:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", earlyMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhmx;->m:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lateMean="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhmx;->n:[D

    invoke-static {v1}, Ljava/util/Arrays;->toString([D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
