.class public final Lelu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/util/Set;


# instance fields
.field public final a:Lejk;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/content/pm/PackageManager;

.field public final d:Lelx;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.google.android.inputmethod.latin"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.google.android.inputmethod.latin.dogfood"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lelu;->e:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lejk;Landroid/content/Context;Landroid/content/pm/PackageManager;Lelx;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lelu;->a:Lejk;

    iput-object p2, p0, Lelu;->b:Landroid/content/Context;

    iput-object p3, p0, Lelu;->c:Landroid/content/pm/PackageManager;

    iput-object p4, p0, Lelu;->d:Lelx;

    return-void
.end method

.method public static c()Ljava/util/Set;
    .locals 1

    sget-object v0, Lelu;->e:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method final a()Z
    .locals 2

    iget-object v0, p0, Lelu;->c:Landroid/content/pm/PackageManager;

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method final a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lelu;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelu;->c:Landroid/content/pm/PackageManager;

    invoke-static {v0, p1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Z
    .locals 2

    iget-object v0, p0, Lelu;->c:Landroid/content/pm/PackageManager;

    const-string v1, "com.google.android.apps.icing.ui"

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
