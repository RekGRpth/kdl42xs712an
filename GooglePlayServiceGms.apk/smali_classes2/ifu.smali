.class final Lifu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lhuf;


# instance fields
.field private final b:Lhou;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lifv;

    invoke-direct {v0}, Lifv;-><init>()V

    sput-object v0, Lifu;->a:Lhuf;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhou;

    const/16 v1, 0x14

    sget-object v2, Ligx;->d:Lhuf;

    sget-object v3, Lifu;->a:Lhuf;

    invoke-direct {v0, v1, v2, v3}, Lhou;-><init>(ILhuf;Lhuf;)V

    iput-object v0, p0, Lifu;->b:Lhou;

    iget-object v0, p0, Lifu;->b:Lhou;

    iget-object v0, v0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->clear()V

    return-void
.end method


# virtual methods
.method public final a(Ligx;)Lify;
    .locals 13

    const/4 v11, 0x0

    const/16 v10, 0xb

    const/4 v9, 0x0

    iget-object v0, p0, Lifu;->b:Lhou;

    iget-object v0, v0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ligx;

    iget-object v0, p1, Ligx;->b:Ljava/lang/String;

    iget-object v1, v6, Ligx;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v11

    :goto_1
    if-eqz v0, :cond_0

    iget-object v0, p1, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v2, p1, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v4, v6, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v6, v6, Ligx;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v0

    double-to-int v1, v0

    if-ge v1, v10, :cond_6

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    iget-object v0, v0, Lhno;->d:Ljava/lang/Object;

    check-cast v0, Lify;

    :goto_2
    move-object v9, v0

    move v10, v1

    goto :goto_0

    :cond_1
    iget-object v0, v6, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    iget-object v1, p1, Ligx;->c:Lcom/google/android/gms/location/places/PlaceFilter;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceFilter;->b()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v11

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceFilter;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v11

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Z

    move-result v0

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/PlaceFilter;->d()Z

    move-result v1

    if-eq v0, v1, :cond_4

    move v0, v11

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_1

    :cond_5
    return-object v9

    :cond_6
    move-object v0, v9

    move v1, v10

    goto :goto_2
.end method
