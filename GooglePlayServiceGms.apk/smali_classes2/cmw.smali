.class public final Lcmw;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:Lcfs;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcfs;Lcms;)V
    .locals 6

    sget-object v1, Lcmr;->i:Lcmr;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    iput-object p4, p0, Lcmw;->c:Lcfs;

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 2

    sget-object v0, Lcmr;->i:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "trashedState"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcfs;->a(J)Lcfs;

    move-result-object v0

    iput-object v0, p0, Lcmw;->c:Lcfs;

    return-void
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmw;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 6

    invoke-virtual {p2}, Lcfp;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v4, Lcfs;->a:Lcfs;

    :goto_0
    sget-object v0, Lcfs;->a:Lcfs;

    iget-object v1, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcfp;->D()V

    :goto_1
    invoke-virtual {p2}, Lcfp;->k()V

    invoke-virtual {p0, p1}, Lcmw;->b(Lcfz;)Lbsp;

    move-result-object v0

    iget-object v1, v0, Lbsp;->a:Lcfc;

    iget-object v2, v0, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p2}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    iget-object v0, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v0, v4}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcmj;

    sget-object v3, Lcms;->b:Lcms;

    invoke-direct {v0, v1, v2, v3}, Lcmj;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V

    :goto_2
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcfp;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v4, Lcfs;->b:Lcfs;

    goto :goto_0

    :cond_1
    sget-object v4, Lcfs;->c:Lcfs;

    goto :goto_0

    :cond_2
    sget-object v0, Lcfs;->b:Lcfs;

    iget-object v1, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcfp;->B()V

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Lcfp;->z()V

    goto :goto_1

    :cond_4
    new-instance v0, Lcmw;

    sget-object v5, Lcms;->b:Lcms;

    invoke-direct/range {v0 .. v5}, Lcmw;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcfs;Lcms;)V

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 2

    sget-object v0, Lcfs;->c:Lcfs;

    iget-object v1, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v0, v1}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcoy;->j()Lcll;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Lcoy;->j()Lcll;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcll;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 4

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "trashedState"

    iget-object v2, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v2}, Lcfs;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcmw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcmw;

    invoke-virtual {p0, p1}, Lcmw;->a(Lclu;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcmw;->c:Lcfs;

    iget-object v3, p1, Lcmw;->c:Lcfs;

    invoke-virtual {v2, v3}, Lcfs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcmw;->c:Lcfs;

    invoke-virtual {v1}, Lcfs;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "SetTrashedOp [%s, mTrashedState=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcmw;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcmw;->c:Lcfs;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
