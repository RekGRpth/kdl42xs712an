.class public final Lhgs;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lhgs;


# instance fields
.field final a:Ldl;


# direct methods
.method private constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhgt;

    invoke-direct {v0, p0, p1}, Lhgt;-><init>(Lhgs;I)V

    iput-object v0, p0, Lhgs;->a:Ldl;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lhgs;
    .locals 2

    const-string v0, "context is null"

    invoke-static {p0, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhgs;->b:Lhgs;

    if-nez v0, :cond_1

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    mul-int/lit16 v0, v0, 0x400

    mul-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    const/high16 v0, 0x200000

    :cond_0
    new-instance v1, Lhgs;

    invoke-direct {v1, v0}, Lhgs;-><init>(I)V

    sput-object v1, Lhgs;->b:Lhgs;

    :cond_1
    sget-object v0, Lhgs;->b:Lhgs;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lhgs;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method
