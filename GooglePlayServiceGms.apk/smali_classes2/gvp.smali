.class public final Lgvp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    :goto_2
    iput-boolean v1, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->createCharArray()[C

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    const-class v1, Lixo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    const/4 v0, -0x1

    if-ne v4, v0, :cond_4

    const/4 v0, 0x0

    :cond_0
    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    return-object v3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_3
    if-ge v2, v4, :cond_0

    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    return-object v0
.end method
