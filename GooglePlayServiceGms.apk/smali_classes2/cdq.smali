.class final Lcdq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:I

.field private c:Lceh;

.field private d:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcdq;->c:Lceh;

    const/4 v0, 0x0

    iput v0, p0, Lcdq;->d:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcdq;->a:Ljava/util/Map;

    const/16 v0, 0xc

    iput v0, p0, Lcdq;->b:I

    return-void
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 3

    iget-object v0, p0, Lcdq;->c:Lceh;

    if-eqz v0, :cond_0

    iget v0, p0, Lcdq;->b:I

    iget v1, p0, Lcdq;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcdq;->a(I)Lcdq;

    :cond_0
    new-instance v0, Lcdp;

    iget-object v1, p0, Lcdq;->a:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcdp;-><init>(Ljava/util/Map;B)V

    return-object v0
.end method

.method public final a(I)Lcdq;
    .locals 4

    iget-object v0, p0, Lcdq;->c:Lceh;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No field definition to remove"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcdq;->d:I

    if-gt p1, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcdq;->d:I

    :goto_0
    if-ge v0, p1, :cond_2

    iget-object v1, p0, Lcdq;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcdq;->c:Lceh;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcdq;->c:Lceh;

    iput p1, p0, Lcdq;->d:I

    return-object p0
.end method

.method public final a(ILcei;)Lcdq;
    .locals 11

    new-instance v0, Lceh;

    iget-object v1, p2, Lcei;->a:Ljava/lang/String;

    iget-object v2, p2, Lcei;->b:Lcek;

    iget-boolean v3, p2, Lcei;->e:Z

    iget-object v4, p2, Lcei;->f:Ljava/util/Set;

    iget-boolean v5, p2, Lcei;->g:Z

    iget-object v6, p2, Lcei;->h:Ljava/lang/Object;

    iget-object v7, p2, Lcei;->c:Lcdt;

    iget-object v8, p2, Lcei;->d:Lcdp;

    iget-object v9, p2, Lcei;->i:Lcej;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lceh;-><init>(Ljava/lang/String;Lcek;ZLjava/util/Set;ZLjava/lang/Object;Lcdt;Lcdp;Lcej;B)V

    iget-object v1, p0, Lcdq;->c:Lceh;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add a new field definition until the existing definition is removed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v1, p0, Lcdq;->d:I

    if-ge p1, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v0, p0, Lcdq;->c:Lceh;

    iput p1, p0, Lcdq;->d:I

    return-object p0
.end method
