.class public final Lcfn;
.super Lcfl;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/io/File;

.field public final c:Ljavax/crypto/SecretKey;

.field public final d:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/Long;


# direct methods
.method private constructor <init>(Lcdu;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/Long;Lcom/google/android/gms/drive/auth/AppIdentity;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, p1, v2, v3}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    move v3, v0

    :goto_0
    if-nez p4, :cond_1

    move v2, v0

    :goto_1
    if-eq v3, v2, :cond_2

    :goto_2
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p2, p0, Lcfn;->g:Ljava/lang/String;

    iput-object p3, p0, Lcfn;->a:Ljava/io/File;

    iput-object p5, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    iput-object p6, p0, Lcfn;->h:Ljava/lang/Long;

    iput-object p4, p0, Lcfn;->b:Ljava/io/File;

    iput-object p7, p0, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    return-void

    :cond_0
    move v3, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private synthetic constructor <init>(Lcdu;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/Long;Lcom/google/android/gms/drive/auth/AppIdentity;B)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcfn;-><init>(Lcdu;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/Long;Lcom/google/android/gms/drive/auth/AppIdentity;)V

    return-void
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcfn;
    .locals 9

    const/4 v1, 0x1

    const/4 v8, 0x0

    sget-object v0, Lcec;->b:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v2, Lcec;->n:Lcec;

    invoke-virtual {v2}, Lcec;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    new-instance v7, Lcfo;

    invoke-direct {v7, p0, v0, v2}, Lcfo;-><init>(Lcdu;Ljava/lang/String;Lcom/google/android/gms/drive/auth/AppIdentity;)V

    sget-object v0, Lcec;->e:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcec;->f:Lcec;

    invoke-virtual {v2}, Lcec;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    const-string v0, ""

    :cond_0
    sget-object v2, Lcec;->d:Lcec;

    invoke-virtual {v2}, Lcec;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    sget-object v2, Lcec;->c:Lcec;

    invoke-virtual {v2}, Lcec;->a()Lcdp;

    move-result-object v2

    invoke-virtual {v2}, Lcdp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    const/4 v2, 0x0

    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v2, v5, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    :cond_1
    if-eqz v3, :cond_4

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, v7, Lcfo;->d:Ljava/io/File;

    :goto_0
    sget-object v0, Lcec;->g:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, v7, Lcfo;->f:Ljava/lang/Long;

    :cond_2
    sget-object v0, Lcec;->l:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v7, Lcfo;->g:Ljava/lang/Long;

    :cond_3
    iget-object v0, v7, Lcfo;->c:Ljava/io/File;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v2, v7, Lcfo;->d:Ljava/io/File;

    if-nez v2, :cond_6

    move v2, v1

    :goto_2
    if-eq v0, v2, :cond_7

    :goto_3
    invoke-static {v1}, Lbkm;->a(Z)V

    new-instance v0, Lcfn;

    iget-object v1, v7, Lcfo;->a:Lcdu;

    iget-object v2, v7, Lcfo;->b:Ljava/lang/String;

    iget-object v3, v7, Lcfo;->c:Ljava/io/File;

    iget-object v4, v7, Lcfo;->d:Ljava/io/File;

    iget-object v5, v7, Lcfo;->e:Ljavax/crypto/SecretKey;

    iget-object v6, v7, Lcfo;->g:Ljava/lang/Long;

    iget-object v7, v7, Lcfo;->h:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct/range {v0 .. v8}, Lcfn;-><init>(Lcdu;Ljava/lang/String;Ljava/io/File;Ljava/io/File;Ljavax/crypto/SecretKey;Ljava/lang/Long;Lcom/google/android/gms/drive/auth/AppIdentity;B)V

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v1

    invoke-virtual {v1}, Lceb;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcfn;->d(J)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, v7, Lcfo;->c:Ljava/io/File;

    iput-object v2, v7, Lcfo;->e:Ljavax/crypto/SecretKey;

    goto :goto_0

    :cond_5
    move v0, v8

    goto :goto_1

    :cond_6
    move v2, v8

    goto :goto_2

    :cond_7
    move v1, v8

    goto :goto_3
.end method

.method private a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfn;->a:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcfn;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcfn;->b:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcfn;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 2

    sget-object v0, Lcec;->e:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcfn;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcec;->f:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcfn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcec;->l:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfn;->h:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lcec;->b:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfn;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    if-eqz v0, :cond_0

    sget-object v0, Lcec;->c:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    sget-object v0, Lcec;->d:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->d()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    sget-object v1, Lcec;->n:Lcec;

    invoke-virtual {v1}, Lcec;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1}, Lcdp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v0, Lcec;->c:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    sget-object v0, Lcec;->d:Lcec;

    invoke-virtual {v0}, Lcec;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocumentContent [contentType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcfn;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ownedFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcfn;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notOwnedFile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcfn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", encryptionKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", referencedContentSqlId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfn;->h:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creatorIdentity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
