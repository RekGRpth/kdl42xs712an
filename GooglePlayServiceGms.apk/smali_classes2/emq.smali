.class public final Lemq;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a([BIILjava/lang/Class;)Lizs;
    .locals 5

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-static {v0, p0, p1, p2}, Lizs;->b(Lizs;[BII)Lizs;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot deserialize message of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "Error constructing new message of type %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-static {v0, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v2, "Error constructing new message of type %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-static {v0, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0
.end method

.method static a(Ljava/lang/Iterable;Z)Ljava/nio/ByteBuffer;
    .locals 10

    const/4 v6, 0x0

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    const-wide/16 v2, 0x8

    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x4

    move v1, v0

    :goto_1
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v5

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-virtual {v0}, Lizs;->a()I

    move-result v0

    add-int/2addr v0, v1

    int-to-long v8, v0

    add-long/2addr v2, v8

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, 0xc

    move v1, v0

    goto :goto_1

    :cond_2
    if-nez v4, :cond_3

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_3
    long-to-int v0, v2

    :try_start_0
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-virtual {v0}, Lizs;->b()I

    move-result v4

    :try_start_1
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;
    :try_end_1
    .catch Ljava/nio/BufferOverflowException; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v5, v5, 0x4

    invoke-static {v0, v2, v5, v4}, Lizs;->a(Lizs;[BII)V

    :try_start_2
    invoke-virtual {v1, v2, v5, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;
    :try_end_2
    .catch Ljava/nio/BufferOverflowException; {:try_start_2 .. :try_end_2} :catch_2

    add-int/2addr v5, v4

    if-nez p1, :cond_4

    sub-int v0, v5, v4

    invoke-static {v1, v2, v0, v4}, Lemq;->a(Ljava/nio/ByteBuffer;[BII)V

    add-int/lit8 v5, v5, 0x8

    goto :goto_4

    :catch_0
    move-exception v0

    const-string v1, "Too big to serialize, %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lell;->a(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v0, v1, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v6

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {v0}, Lemq;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-static {v0}, Lemq;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    goto :goto_3

    :cond_5
    if-eqz p1, :cond_6

    const/4 v0, 0x0

    :try_start_3
    invoke-static {v1, v2, v0, v5}, Lemq;->a(Ljava/nio/ByteBuffer;[BII)V
    :try_end_3
    .catch Ljava/nio/BufferOverflowException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_6
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-object v0, v1

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lemq;->a(Ljava/nio/BufferOverflowException;)V

    move-object v0, v6

    goto :goto_3
.end method

.method static a(Ljava/nio/ByteBuffer;Ljava/lang/Class;)Ljava/util/List;
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, -0x8

    if-gez v1, :cond_1

    const-string v1, "Protobuf data too short to be valid"

    invoke-static {v1}, Lehe;->d(Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v3

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-static {v5, v7, v1, v3, v4}, Lemq;->a([BIIJ)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v1, "Ignoring corrupt protobuf data"

    invoke-static {v1}, Lehe;->d(Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v3, v1, -0x8

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    div-int/lit16 v4, v4, 0x3e8

    add-int/lit8 v4, v4, 0x1

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    if-ge v4, v3, :cond_5

    :try_start_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I
    :try_end_0
    .catch Ljava/nio/BufferUnderflowException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-gez v4, :cond_4

    const-string v1, "Invalid message size: %d.May have given the wrong message type: %s"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v3, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "Buffer underflow. May have given the wrong message type: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v7

    invoke-static {v1, v3, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    invoke-static {v5, v6, v4, p1}, Lemq;->a([BIILjava/lang/Class;)Lizs;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {p0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/nio/BufferOverflowException;)V
    .locals 2

    const-string v0, "Buffer underflow. A message may have an invalid serialized form or has been concurrently modified."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;[BII)V
    .locals 2

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    return-void
.end method

.method static a([BIIJ)Z
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v1, p0, v0, p2}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v1}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v1

    cmp-long v3, v1, p3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    const-string v3, "Corrupt protobuf data, expected CRC: %d computed CRC: %d"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v3, v4, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_1
    return v0
.end method
