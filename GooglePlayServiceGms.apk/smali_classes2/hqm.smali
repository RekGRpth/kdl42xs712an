.class final Lhqm;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field protected final a:Limb;

.field private final b:Lhqu;

.field private final c:Landroid/os/Handler;

.field private final d:I

.field private e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lhqu;Landroid/os/Handler;ILimb;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhqm;->e:Z

    new-instance v0, Lhqn;

    invoke-direct {v0, p0}, Lhqn;-><init>(Lhqm;)V

    iput-object v0, p0, Lhqm;->f:Ljava/lang/Runnable;

    iput-object p1, p0, Lhqm;->b:Lhqu;

    iput-object p2, p0, Lhqm;->c:Landroid/os/Handler;

    iput p3, p0, Lhqm;->d:I

    invoke-static {p4}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhqm;->a:Limb;

    return-void
.end method

.method static synthetic a(Lhqm;)Z
    .locals 1

    iget-boolean v0, p0, Lhqm;->e:Z

    return v0
.end method

.method static synthetic b(Lhqm;)Lhqu;
    .locals 1

    iget-object v0, p0, Lhqm;->b:Lhqu;

    return-object v0
.end method

.method static synthetic c(Lhqm;)I
    .locals 1

    iget v0, p0, Lhqm;->d:I

    return v0
.end method

.method static synthetic d(Lhqm;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lhqm;->c:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lhqm;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhqm;->e:Z

    return v0
.end method


# virtual methods
.method final a()V
    .locals 1

    iget-object v0, p0, Lhqm;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lhqm;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(FFFF)V
    .locals 4

    iget-object v0, p0, Lhqm;->b:Lhqu;

    new-instance v1, Livi;

    sget-object v2, Lihj;->aC:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Livi;->a(IF)Livi;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2}, Livi;->a(IF)Livi;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p3}, Livi;->a(IF)Livi;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p4}, Livi;->a(IF)Livi;

    iget-object v2, v0, Lhqu;->d:Livi;

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v0}, Lhqu;->b()V

    return-void
.end method

.method final a(IFFFFFFIJJ)V
    .locals 12

    const/16 v0, 0xe

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lhqm;->b:Lhqu;

    move v1, p2

    move v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-wide/from16 v8, p9

    move-wide/from16 v10, p11

    invoke-virtual/range {v0 .. v11}, Lhqu;->a(FFFFFFIJJ)V

    :cond_0
    return-void
.end method

.method final a(IFFFIJJ)V
    .locals 13

    const/4 v4, 0x3

    if-ne p1, v4, :cond_1

    iget-object v4, p0, Lhqm;->b:Lhqu;

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-wide/from16 v11, p8

    invoke-virtual/range {v4 .. v12}, Lhqu;->a(FFFIJJ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    iget-object v4, p0, Lhqm;->b:Lhqu;

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-wide/from16 v11, p8

    invoke-virtual/range {v4 .. v12}, Lhqu;->b(FFFIJJ)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x2

    if-ne p1, v4, :cond_3

    iget-object v4, p0, Lhqm;->b:Lhqu;

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-wide/from16 v11, p8

    invoke-virtual/range {v4 .. v12}, Lhqu;->c(FFFIJJ)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x4

    if-ne p1, v4, :cond_4

    iget-object v4, p0, Lhqm;->b:Lhqu;

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-wide/from16 v11, p8

    invoke-virtual/range {v4 .. v12}, Lhqu;->d(FFFIJJ)V

    goto :goto_0

    :cond_4
    const/4 v4, 0x6

    if-ne p1, v4, :cond_0

    iget-object v6, p0, Lhqm;->b:Lhqu;

    iget-boolean v4, v6, Lhqu;->a:Z

    if-eqz v4, :cond_5

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_0

    iget-object v4, v6, Lhqu;->g:Limb;

    const-string v5, "Could not add barometer snapshot after the composer is closed."

    invoke-virtual {v4, v5}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-wide/from16 v0, p6

    move-wide/from16 v2, p8

    invoke-virtual {v6, v0, v1, v2, v3}, Lhqu;->a(JJ)V

    new-instance v7, Livi;

    sget-object v4, Lihj;->aG:Livk;

    const/4 v5, 0x4

    invoke-direct {v7, v4, v5}, Livi;-><init>(Livk;I)V

    const/4 v4, 0x1

    invoke-virtual {v7, v4, p2}, Livi;->a(IF)Livi;

    iget-object v4, v6, Lhqu;->d:Livi;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Livi;->i(I)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v6, Lhqu;->d:Livi;

    const/4 v5, 0x1

    move-wide/from16 v0, p6

    invoke-virtual {v6, v0, v1}, Lhqu;->a(J)J

    move-result-wide v8

    invoke-virtual {v4, v5, v8, v9}, Livi;->a(IJ)Livi;

    :cond_6
    iget-wide v4, v6, Lhqu;->f:J

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_b

    iget-object v4, v6, Lhqu;->d:Livi;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Livi;->d(I)J

    move-result-wide v8

    move-wide/from16 v0, p6

    invoke-virtual {v6, v0, v1}, Lhqu;->a(J)J

    move-result-wide v10

    invoke-virtual {v6, v8, v9, v10, v11}, Lhqu;->b(JJ)I

    move-result v5

    invoke-virtual {v6, v8, v9, v10, v11}, Lhqu;->c(JJ)I

    move-result v4

    :goto_1
    if-eqz v5, :cond_7

    const/4 v8, 0x3

    invoke-virtual {v7, v8, v5}, Livi;->e(II)Livi;

    :cond_7
    if-eqz v4, :cond_8

    const/4 v5, 0x4

    invoke-virtual {v7, v5, v4}, Livi;->e(II)Livi;

    :cond_8
    move-wide/from16 v0, p6

    iput-wide v0, v6, Lhqu;->f:J

    iget-object v4, v6, Lhqu;->d:Livi;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Livi;->k(I)I

    move-result v4

    if-eqz v4, :cond_9

    iget v4, v6, Lhqu;->e:I

    move/from16 v0, p5

    if-eq v4, v0, :cond_a

    :cond_9
    const/4 v4, 0x2

    move/from16 v0, p5

    invoke-virtual {v7, v4, v0}, Livi;->e(II)Livi;

    move/from16 v0, p5

    iput v0, v6, Lhqu;->e:I

    :cond_a
    iget-object v4, v6, Lhqu;->d:Livi;

    const/16 v5, 0xb

    invoke-virtual {v4, v5, v7}, Livi;->a(ILivi;)V

    invoke-virtual {v6}, Lhqu;->b()V

    goto/16 :goto_0

    :cond_b
    iget-wide v4, v6, Lhqu;->f:J

    move-wide/from16 v0, p6

    invoke-virtual {v6, v4, v5, v0, v1}, Lhqu;->b(JJ)I

    move-result v5

    iget-wide v8, v6, Lhqu;->f:J

    move-wide/from16 v0, p6

    invoke-virtual {v6, v8, v9, v0, v1}, Lhqu;->c(JJ)I

    move-result v4

    goto :goto_1
.end method

.method final a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 8

    iget-object v0, p0, Lhqm;->b:Lhqu;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-wide v6, p6

    invoke-virtual/range {v0 .. v7}, Lhqu;->a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V

    return-void
.end method

.method public final a(Landroid/location/GpsStatus;J)V
    .locals 1

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhqm;->b:Lhqu;

    invoke-virtual {v0, p1, p2, p3}, Lhqu;->a(Landroid/location/GpsStatus;J)V

    return-void
.end method

.method final a(Landroid/location/Location;J)V
    .locals 1

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhqm;->b:Lhqu;

    invoke-virtual {v0, p1, p2, p3}, Lhqu;->a(Landroid/location/Location;J)Z

    return-void
.end method

.method final a(Lhrx;)V
    .locals 1

    new-instance v0, Lhqo;

    invoke-direct {v0, p0, p1}, Lhqo;-><init>(Lhqm;Lhrx;)V

    invoke-virtual {p0, v0}, Lhqm;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method final a(Ljava/util/List;J)V
    .locals 1

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lhqp;

    invoke-direct {v0, p0, p1, p2, p3}, Lhqp;-><init>(Lhqm;Ljava/util/List;J)V

    invoke-virtual {p0, v0}, Lhqm;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
