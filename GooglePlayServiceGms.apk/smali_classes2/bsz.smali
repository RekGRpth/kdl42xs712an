.class public final Lbsz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbsu;


# instance fields
.field private final a:Lcdu;

.field private final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final c:J

.field private final d:Lcom/google/android/gms/drive/database/data/EntrySpec;


# direct methods
.method public constructor <init>(Lcdu;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/database/data/EntrySpec;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lbsz;->a:Lcdu;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/data/EntrySpec;

    iput-object v0, p0, Lbsz;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lbsz;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-wide p3, p0, Lbsz;->c:J

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 7

    new-instance v0, Lcge;

    iget-object v1, p0, Lbsz;->a:Lcdu;

    iget-object v3, p0, Lbsz;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    iget-object v4, p0, Lbsz;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v5, p0, Lbsz;->c:J

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcge;-><init>(Lcdu;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)V

    invoke-virtual {v0}, Lcge;->k()V

    return-object v0
.end method
