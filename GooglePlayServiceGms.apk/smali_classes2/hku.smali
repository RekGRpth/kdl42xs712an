.class final Lhku;
.super Lhks;
.source "SourceFile"

# interfaces
.implements Lhkp;


# instance fields
.field final synthetic c:Lhkr;

.field private e:Z


# direct methods
.method private constructor <init>(Lhkr;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lhku;->c:Lhkr;

    invoke-direct {p0, p1, v0}, Lhks;-><init>(Lhkr;B)V

    iput-boolean v0, p0, Lhku;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Lhkr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhku;-><init>(Lhkr;)V

    return-void
.end method


# virtual methods
.method public final a(D)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 4

    iget-boolean v0, p0, Lhku;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lhku;->c:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    iget-object v0, p0, Lhku;->c:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhku;->c:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lhku;->e:Z

    if-eqz v0, :cond_5

    :cond_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Low power not still detected. tiltingDetected="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lhku;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_4

    const-string v0, "ActivityScheduler"

    const-string v1, "Switching to full mode"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhku;->c:Lhkr;

    invoke-static {v0}, Lhkr;->r(Lhkr;)V

    iget-object v0, p0, Lhku;->c:Lhkr;

    new-instance v1, Lhkt;

    iget-object v2, p0, Lhku;->c:Lhkr;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lhkt;-><init>(Lhkr;B)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0

    :cond_5
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_6

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Low power still detected. Reporting: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lhku;->c:Lhkr;

    invoke-static {v0, p1}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-object v0, p0, Lhku;->c:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhku;->c:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto/16 :goto_0
.end method

.method protected final b()Lhms;
    .locals 1

    iget-object v0, p0, Lhku;->c:Lhkr;

    invoke-static {v0}, Lhkr;->i(Lhkr;)Lhms;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 2

    invoke-super {p0, p1}, Lhks;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-boolean v0, p0, Lhku;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhku;->e:Z

    goto :goto_0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    const-string v0, "LowPowerDetector"

    return-object v0
.end method

.method protected final h()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected final i()D
    .locals 2

    const-wide/high16 v0, 0x4020000000000000L    # 8.0

    return-wide v0
.end method
