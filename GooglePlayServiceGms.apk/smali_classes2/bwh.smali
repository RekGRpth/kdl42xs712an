.class public final Lbwh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbvs;


# instance fields
.field private final a:Landroid/content/SyncResult;

.field private final b:Lcfc;

.field private final c:Lbvr;

.field private final d:Lcfz;

.field private final e:Ljava/lang/Boolean;

.field private f:J

.field private g:Z


# direct methods
.method public constructor <init>(Lcoy;Lcfc;Landroid/content/SyncResult;Ljava/lang/Boolean;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lbwh;->b:Lcfc;

    iput-object p3, p0, Lbwh;->a:Landroid/content/SyncResult;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbwh;->d:Lcfz;

    iput-object p4, p0, Lbwh;->e:Ljava/lang/Boolean;

    new-instance v0, Lbvr;

    invoke-direct {v0, p1}, Lbvr;-><init>(Lcoy;)V

    iput-object v0, p0, Lbwh;->c:Lbvr;

    iput-boolean p5, p0, Lbwh;->g:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lbwh;->d:Lcfz;

    iget-object v1, p0, Lbwh;->b:Lcfc;

    iget-object v1, v1, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v0

    invoke-virtual {v0}, Lcfe;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lbwh;->f:J

    return-void
.end method

.method public final a(Lclm;)V
    .locals 2

    iget-object v0, p0, Lbwh;->d:Lcfz;

    iget-object v1, p0, Lbwh;->b:Lcfc;

    invoke-interface {v0, v1}, Lcfz;->b(Lcfc;)V

    return-void
.end method

.method public final a(Lclm;Lclk;)V
    .locals 10

    const-wide/16 v8, 0x1

    invoke-interface {p2}, Lclk;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbwh;->c:Lbvr;

    iget-object v1, p0, Lbwh;->b:Lcfc;

    iget-object v2, p0, Lbwh;->e:Ljava/lang/Boolean;

    invoke-static {v1}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v1

    iget-object v0, v0, Lbvr;->a:Lcfz;

    invoke-interface {p2}, Lclk;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcfp;->l()V

    :cond_0
    iget-object v0, p0, Lbwh;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v1, v8

    iput-wide v1, v0, Landroid/content/SyncStats;->numEntries:J

    iget-object v0, p0, Lbwh;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v1, v8

    iput-wide v1, v0, Landroid/content/SyncStats;->numDeletes:J

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbwh;->c:Lbvr;

    iget-object v1, p0, Lbwh;->b:Lcfc;

    iget-object v3, p0, Lbwh;->e:Ljava/lang/Boolean;

    iget-wide v4, p0, Lbwh;->f:J

    const/4 v6, 0x1

    iget-boolean v7, p0, Lbwh;->g:Z

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lbvr;->a(Lcfc;Lclk;Ljava/lang/Boolean;JZZ)V

    iget-object v0, p0, Lbwh;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v1, v8

    iput-wide v1, v0, Landroid/content/SyncStats;->numInserts:J

    iget-object v0, p0, Lbwh;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numEntries:J

    add-long/2addr v1, v8

    iput-wide v1, v0, Landroid/content/SyncStats;->numEntries:J

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final b(Lclm;)V
    .locals 0

    return-void
.end method
