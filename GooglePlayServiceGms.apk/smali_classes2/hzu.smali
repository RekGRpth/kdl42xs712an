.class final Lhzu;
.super Landroid/hardware/location/GeofenceHardwareMonitorCallback;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhzs;


# direct methods
.method constructor <init>(Lhzs;)V
    .locals 0

    iput-object p1, p0, Lhzu;->a:Lhzs;

    invoke-direct {p0}, Landroid/hardware/location/GeofenceHardwareMonitorCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMonitoringSystemChange(IZLandroid/location/Location;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/hardware/location/GeofenceHardwareMonitorCallback;->onMonitoringSystemChange(IZLandroid/location/Location;)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "BlockingGeofenceHardware"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onMonitoringSystemChange: monitoringType="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " available="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lhzu;->a:Lhzs;

    iget-object v0, v0, Lhzs;->c:Lhyt;

    invoke-virtual {v0, p2}, Lhyt;->b(Z)V

    :cond_1
    return-void
.end method
