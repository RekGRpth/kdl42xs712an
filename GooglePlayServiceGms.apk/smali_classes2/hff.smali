.class final Lhff;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhfd;


# direct methods
.method constructor <init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhff;->f:Lhfd;

    iput-object p4, p0, Lhff;->d:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    iput-object p5, p0, Lhff;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/16 v0, 0x8

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lhff;->d:Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Ljaw;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Ljaw;

    iget-object v0, p0, Lhff;->f:Lhfd;

    iget-object v0, p0, Lhff;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhfd;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Z

    move-result v0

    iput-boolean v0, v2, Ljaw;->l:Z

    invoke-static {}, Lhfx;->a()Ljbf;

    move-result-object v0

    iput-object v0, v2, Ljaw;->m:Ljbf;

    iget-object v0, p0, Lhff;->f:Lhfd;

    invoke-static {v0}, Lhfd;->a(Lhfd;)Lgtm;

    move-result-object v0

    iget-object v3, p0, Lhff;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lhff;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lgtm;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, v2, Ljaw;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhff;->f:Lhfd;

    invoke-static {v0}, Lhfd;->d(Lhfd;)Landroid/content/Context;

    invoke-static {}, Lbox;->a()J

    move-result-wide v3

    iput-wide v3, v2, Ljaw;->j:J

    iget-object v0, p0, Lhff;->f:Lhfd;

    invoke-static {v0}, Lhfd;->b(Lhfd;)Lhef;

    move-result-object v0

    iget-object v3, p0, Lhff;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhef;->a(Ljava/lang/String;)Ljan;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    :goto_0
    return-object v0

    :cond_1
    iget-object v3, v2, Ljaw;->a:Ljbg;

    iput-object v0, v3, Ljbg;->a:Ljan;

    iget-object v0, v2, Ljaw;->e:Linv;

    if-eqz v0, :cond_8

    iget-object v0, v2, Ljaw;->e:Linv;

    iget-object v3, v0, Linv;->b:Lint;

    if-eqz v3, :cond_8

    iget-object v7, v0, Linv;->b:Lint;

    iget-object v0, v7, Lint;->a:Linu;

    if-eqz v0, :cond_8

    iget-object v4, v7, Lint;->a:Linu;

    iget-object v0, v4, Linu;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v5

    :goto_1
    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v0, v4, Linu;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v5

    :goto_2
    invoke-static {v0}, Lbkm;->b(Z)V

    iget-object v3, v4, Linu;->a:Ljava/lang/String;

    iget-object v4, v4, Linu;->b:Ljava/lang/String;

    iput-object v1, v7, Lint;->a:Linu;

    :goto_3
    iget-object v0, p0, Lhff;->f:Lhfd;

    invoke-static {v0}, Lhfd;->c(Lhfd;)Lhfo;

    move-result-object v1

    iget-object v0, p0, Lhff;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhfd;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, v2, Ljaw;->e:Linv;

    if-eqz v0, :cond_2

    iget-object v0, v2, Ljaw;->e:Linv;

    iget-object v0, v0, Linv;->b:Lint;

    if-eqz v0, :cond_2

    iget-object v0, v2, Ljaw;->e:Linv;

    iget-object v0, v0, Linv;->b:Lint;

    iget-object v0, v0, Lint;->a:Linu;

    if-nez v0, :cond_5

    move v0, v5

    :goto_4
    invoke-static {v0}, Lbkm;->b(Z)V

    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_5
    invoke-static {v5}, Lbkm;->b(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "/online-secure/v2/sdk/v1/getMaskedWalletForBuyerSelection?s7e=card_number;cvn"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v1, Lhfo;->a:Landroid/content/Context;

    new-instance v0, Lhfq;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lhfq;-><init>(Lhfo;Ljaw;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgm;)V

    const-string v1, "get_masked_wallet_signup"

    invoke-static {v7, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto/16 :goto_0

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    move v0, v6

    goto :goto_2

    :cond_5
    move v0, v6

    goto :goto_4

    :cond_6
    move v5, v6

    goto :goto_5

    :cond_7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/online/v2/wallet/sdk/v1/getMaskedWalletForBuyerSelection"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v1, Lhfo;->a:Landroid/content/Context;

    new-instance v4, Lhfr;

    invoke-direct {v4, v1, v0, p1, v2}, Lhfr;-><init>(Lhfo;Ljava/lang/String;Lhgm;Ljaw;)V

    const-string v0, "get_masked_wallet"

    invoke-static {v3, v4, v0}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto/16 :goto_0

    :cond_8
    move-object v4, v1

    move-object v3, v1

    goto/16 :goto_3
.end method
