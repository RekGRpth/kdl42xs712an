.class public final Lhzs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhyl;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field a:Lhyo;

.field final b:Ljava/util/ArrayList;

.field final c:Lhyt;

.field final d:Lhzt;

.field final e:Lhzu;


# direct methods
.method public constructor <init>(Lhyt;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x32

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Collections;->nCopies(ILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lhzs;->b:Ljava/util/ArrayList;

    new-instance v0, Lhzt;

    invoke-direct {v0, p0}, Lhzt;-><init>(Lhzs;)V

    iput-object v0, p0, Lhzs;->d:Lhzt;

    new-instance v0, Lhzu;

    invoke-direct {v0, p0}, Lhzu;-><init>(Lhzs;)V

    iput-object v0, p0, Lhzs;->e:Lhzu;

    iput-object p1, p0, Lhzs;->c:Lhyt;

    return-void
.end method

.method private b()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhzs;->a:Lhyo;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lhzs;->a:Lhyo;

    iget-object v1, v1, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    invoke-virtual {v1}, Landroid/hardware/location/GeofenceHardware;->getMonitoringTypes()[I

    move-result-object v1

    array-length v1, v1

    if-gtz v1, :cond_2

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "BlockingGeofenceHardware"

    const-string v2, "GPS geofencing not supported."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "BlockingGeofenceHardware"

    const-string v2, "System refused to grant LOCATION_HARDWARE permission."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lhzs;->a:Lhyo;

    invoke-virtual {v1}, Lhyo;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "BlockingGeofenceHardware"

    const-string v2, "GPS geofencing not supported."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/util/List;Landroid/location/Location;)Z
    .locals 11

    const/16 v0, 0x32

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget-object v0, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v0, v8}, Lhzt;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v8, :cond_6

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhxp;

    const/16 v0, 0x31

    if-ne v7, v0, :cond_3

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v6}, Lhxp;->a()D

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Landroid/hardware/location/GeofenceHardwareRequest;->createCircularGeofence(DDD)Landroid/hardware/location/GeofenceHardwareRequest;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setLastTransition(I)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setMonitorTransitions(I)V

    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setNotificationResponsiveness(I)V

    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setUnknownTimer(I)V

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "BlockingGeofenceHardware"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pushing sentinel geofence "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "[CIRCLE %.6f, %.6f %.1fm]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Landroid/hardware/location/GeofenceHardwareRequest;->getLatitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Landroid/hardware/location/GeofenceHardwareRequest;->getLongitude()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Landroid/hardware/location/GeofenceHardwareRequest;->getRadius()D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_2
    iget-object v1, p0, Lhzs;->b:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lhzs;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v7, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lhzs;->a:Lhyo;

    iget-object v2, p0, Lhzs;->d:Lhzt;

    iget-object v1, v1, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    const/4 v3, 0x0

    invoke-virtual {v1, v7, v3, v0, v2}, Landroid/hardware/location/GeofenceHardware;->addGeofence(IILandroid/hardware/location/GeofenceHardwareRequest;Landroid/hardware/location/GeofenceHardwareCallback;)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    :goto_3
    if-nez v0, :cond_2

    iget-object v0, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v0, v7}, Lhzt;->b(I)V

    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_1

    :cond_3
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_4

    const-string v0, "BlockingGeofenceHardware"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Pushing "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v4, v6, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v4}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->c()D

    move-result-wide v0

    invoke-virtual {v4}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->d()D

    move-result-wide v2

    invoke-virtual {v4}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->e()F

    move-result v4

    float-to-double v4, v4

    invoke-static/range {v0 .. v5}, Landroid/hardware/location/GeofenceHardwareRequest;->createCircularGeofence(DDD)Landroid/hardware/location/GeofenceHardwareRequest;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setLastTransition(I)V

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setMonitorTransitions(I)V

    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setNotificationResponsiveness(I)V

    const/16 v1, 0x2328

    invoke-virtual {v0, v1}, Landroid/hardware/location/GeofenceHardwareRequest;->setUnknownTimer(I)V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_5

    const-string v0, "BlockingGeofenceHardware"

    const-string v1, "System refused to grant LOCATION_HARDWARE permission."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    :try_start_2
    iget-object v0, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v0}, Lhzt;->a()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private c()V
    .locals 4

    iget-object v1, p0, Lhzs;->b:Ljava/util/ArrayList;

    monitor-enter v1

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x32

    if-ge v0, v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lhzs;->b:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lhyo;)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "BlockingGeofenceHardware"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got new geofence hardware: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez p1, :cond_2

    iput-object v3, p0, Lhzs;->a:Lhyo;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lhzs;->a:Lhyo;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    iget-object v1, p0, Lhzs;->a:Lhyo;

    iget-object v1, v1, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    if-eq v0, v1, :cond_1

    :cond_3
    iput-object p1, p0, Lhzs;->a:Lhyo;

    :try_start_0
    iget-object v0, p0, Lhzs;->a:Lhyo;

    iget-object v1, p0, Lhzs;->e:Lhzu;

    iget-object v0, v0, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/hardware/location/GeofenceHardware;->registerForMonitorStateChangeCallback(ILandroid/hardware/location/GeofenceHardwareMonitorCallback;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "BlockingGeofenceHardware"

    const-string v1, "System refused to grant LOCATION_HARDWARE permission."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lhzs;->a:Lhyo;

    goto :goto_0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 7

    const/4 v1, 0x0

    const-string v0, "Dump of GPS geofencing:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "\n    Class is RealBlockingGeofenceHardware, limit=50"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "\n    Supported="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lhzs;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " available="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lhzs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhzs;->a:Lhyo;

    invoke-virtual {v0}, Lhyo;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "\n    Registered geofences:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v3, p0, Lhzs;->b:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lhzs;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    if-eqz v0, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\n        id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    const-string v0, "\n    <none>"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final a()Z
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Lhzs;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lhzs;->c()V

    iget-object v1, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v1}, Lhzt;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    move v2, v0

    :goto_1
    const/16 v1, 0x32

    if-ge v2, v1, :cond_5

    :try_start_0
    iget-object v1, p0, Lhzs;->a:Lhyo;

    iget-object v1, v1, Lhyo;->a:Landroid/hardware/location/GeofenceHardware;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/hardware/location/GeofenceHardware;->removeGeofence(II)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_2
    if-nez v1, :cond_2

    iget-object v1, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v1}, Lhzt;->c()V

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-boolean v3, Lhyb;->a:Z

    if-eqz v3, :cond_3

    const-string v3, "BlockingGeofenceHardware"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "removeGeofence: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v1, v0

    goto :goto_2

    :catch_1
    move-exception v1

    sget-boolean v3, Lhyb;->a:Z

    if-eqz v3, :cond_4

    const-string v3, "BlockingGeofenceHardware"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "removeGeofence: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v1, v0

    goto :goto_2

    :cond_5
    :try_start_1
    iget-object v0, p0, Lhzs;->d:Lhzt;

    invoke-virtual {v0}, Lhzt;->d()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_3
.end method

.method public final a(Ljava/util/List;Landroid/location/Location;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lhzs;->b()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lhzs;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "BlockingGeofenceHardware"

    const-string v2, "Ignoring same geofences for hardware."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lhzs;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, Lhzs;->b(Ljava/util/List;Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_5

    const-string v1, "BlockingGeofenceHardware"

    const-string v2, "Removing all geofences because some of them could not be added."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lhzs;->a()Z

    invoke-direct {p0}, Lhzs;->c()V

    goto :goto_0
.end method
