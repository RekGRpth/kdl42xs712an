.class public final Latp;
.super Lato;
.source "SourceFile"


# instance fields
.field public e:J

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lato;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    sget-object v0, Laqc;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Laqc;->l:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lavq;

    invoke-direct {v0}, Lavq;-><init>()V

    iget-wide v1, p0, Latp;->e:J

    invoke-virtual {v0, v1, v2}, Lavq;->a(J)Lavq;

    iget-boolean v1, p0, Latp;->f:Z

    invoke-virtual {v0, v1}, Lavq;->a(Z)Lavq;

    iget-boolean v1, p0, Latp;->g:Z

    invoke-virtual {v0, v1}, Lavq;->b(Z)Lavq;

    iget-boolean v1, p0, Latp;->h:Z

    invoke-virtual {v0, v1}, Lavq;->c(Z)Lavq;

    iget-boolean v1, p0, Latp;->i:Z

    invoke-virtual {v0, v1}, Lavq;->d(Z)Lavq;

    new-instance v1, Lavs;

    invoke-direct {v1}, Lavs;-><init>()V

    iget-object v2, p0, Latp;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->a(Ljava/lang/String;)Lavs;

    iget-object v2, p0, Latp;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->b(Ljava/lang/String;)Lavs;

    iget-object v2, p0, Latp;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lavs;->c(Ljava/lang/String;)Lavs;

    invoke-virtual {v0, v1}, Lavq;->a(Lavs;)Lavq;

    new-instance v1, Lavp;

    invoke-direct {v1}, Lavp;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lavp;->a(I)Lavp;

    invoke-virtual {v1, v0}, Lavp;->a(Lavq;)Lavp;

    iget-object v0, p0, Latp;->a:Lfko;

    const-string v2, "GetToken"

    invoke-virtual {v1}, Lavp;->d()[B

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    iget-object v0, p0, Latp;->a:Lfko;

    invoke-virtual {v0}, Lfko;->a()V

    goto :goto_0
.end method
