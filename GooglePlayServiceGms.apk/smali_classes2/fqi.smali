.class public final Lfqi;
.super Lbon;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfqe;

.field private final c:Ljava/util/List;

.field private final d:I

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lfqe;Ljava/util/List;)V
    .locals 6

    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lfqi;-><init>(Lfqe;Ljava/util/List;III)V

    return-void
.end method

.method public constructor <init>(Lfqe;Ljava/util/List;III)V
    .locals 1

    iput-object p1, p0, Lfqi;->a:Lfqe;

    invoke-direct {p0, p1}, Lbon;-><init>(Lbol;)V

    iput-object p2, p0, Lfqi;->c:Ljava/util/List;

    iput p3, p0, Lfqi;->d:I

    sub-int v0, p4, p3

    iput v0, p0, Lfqi;->e:I

    iput p5, p0, Lfqi;->f:I

    return-void
.end method

.method private c(I)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2

    iget-object v0, p0, Lfqi;->c:Ljava/util/List;

    iget v1, p0, Lfqi;->d:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lfqi;->e:I

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 13

    invoke-direct {p0, p1}, Lfqi;->c(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "selectionSource"

    iget v3, p0, Lfqi;->f:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lfqi;->a:Lfqe;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "secondaryText"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "contactsAvatarUri"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lfqi;->a:Lfqe;

    invoke-static {v7}, Lfqe;->c(Lfqe;)Lfrb;

    move-result-object v7

    iget-object v7, v7, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    const-string v8, "Audience must not be null."

    invoke-static {v7, v8}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "Audience member must not be null."

    invoke-static {v1, v8}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    const v8, 0x7f0400c7    # com.google.android.gms.R.layout.plus_audience_selection_list_person

    const/4 v11, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "checkboxEnabled"

    const/4 v12, 0x1

    invoke-virtual {v9, v10, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    move-object v9, p2

    move/from16 v10, p4

    invoke-virtual/range {v0 .. v12}, Lfqe;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lfqi;->c(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    iget-object v0, p0, Lfqi;->a:Lfqe;

    iget v0, v0, Lfqe;->k:I

    return v0
.end method
