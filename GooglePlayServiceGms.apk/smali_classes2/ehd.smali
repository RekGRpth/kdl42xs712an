.class public final Lehd;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v1, p0, Lehd;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lehd;->b:I

    iput v1, p0, Lehd;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lehd;->C:I

    return-void
.end method

.method public static a([B)Lehd;
    .locals 1

    new-instance v0, Lehd;

    invoke-direct {v0}, Lehd;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Lehd;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Lehd;->a:I

    if-eqz v1, :cond_0

    iget v1, p0, Lehd;->a:I

    invoke-static {v2, v1}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Lehd;->b:I

    if-eq v1, v2, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Lehd;->b:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lehd;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lehd;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lehd;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehd;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lehd;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehd;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lehd;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lehd;->a:I

    invoke-virtual {p1, v1, v0}, Lizn;->a(II)V

    :cond_0
    iget v0, p0, Lehd;->b:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lehd;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1
    iget v0, p0, Lehd;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lehd;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehd;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehd;

    iget v2, p0, Lehd;->a:I

    iget v3, p1, Lehd;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lehd;->b:I

    iget v3, p1, Lehd;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lehd;->c:I

    iget v3, p1, Lehd;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lehd;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehd;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehd;->c:I

    add-int/2addr v0, v1

    return v0
.end method
