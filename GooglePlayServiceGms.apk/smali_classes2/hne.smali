.class public final Lhne;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a([D[D)D
    .locals 7

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, p0

    if-ge v0, v3, :cond_0

    aget-wide v3, p0, v0

    aget-wide v5, p1, v0

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method private static a([D)Ljava/util/List;
    .locals 13

    array-length v2, p0

    mul-int/lit8 v1, v2, 0x2

    new-array v3, v1, [D

    new-array v4, v2, [D

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    int-to-double v5, v0

    add-int/lit8 v7, v2, -0x1

    int-to-double v7, v7

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    div-double/2addr v7, v9

    sub-double/2addr v5, v7

    neg-double v5, v5

    int-to-double v7, v0

    add-int/lit8 v9, v2, -0x1

    int-to-double v9, v9

    const-wide/high16 v11, 0x4000000000000000L    # 2.0

    div-double/2addr v9, v11

    sub-double/2addr v7, v9

    mul-double/2addr v5, v7

    add-int/lit8 v7, v2, -0x1

    int-to-double v7, v7

    const-wide/high16 v9, 0x4010000000000000L    # 4.0

    div-double/2addr v7, v9

    add-int/lit8 v9, v2, -0x1

    int-to-double v9, v9

    const-wide/high16 v11, 0x4010000000000000L    # 4.0

    div-double/2addr v9, v11

    mul-double/2addr v7, v9

    div-double/2addr v5, v7

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->exp(D)D

    move-result-wide v5

    aput-wide v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-wide v5, p0, v0

    aget-wide v7, v4, v0

    mul-double/2addr v5, v7

    aput-wide v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_2
    if-ge v0, v1, :cond_2

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    array-length v0, v3

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v5, v3

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v5, :cond_3

    aget-wide v6, v3, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    new-instance v0, Lhnc;

    invoke-direct {v0}, Lhnc;-><init>()V

    invoke-virtual {v0, v4}, Lhnc;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    mul-int v0, v1, v1

    int-to-double v0, v0

    div-double/2addr v5, v0

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnd;

    iget-wide v7, v0, Lhnd;->a:D

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnd;

    iget-wide v0, v0, Lhnd;->a:D

    mul-double/2addr v0, v7

    mul-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    move v1, v0

    :goto_4
    if-ge v1, v2, :cond_4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnd;

    iget-wide v7, v0, Lhnd;->a:D

    iget-wide v9, v0, Lhnd;->a:D

    mul-double/2addr v7, v9

    iget-wide v9, v0, Lhnd;->b:D

    iget-wide v11, v0, Lhnd;->b:D

    mul-double/2addr v9, v11

    add-double/2addr v7, v9

    mul-double/2addr v7, v5

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_4
    return-object v4
.end method


# virtual methods
.method public final a([[DLjava/util/List;J)[D
    .locals 18

    const/16 v2, 0x40

    const/4 v3, 0x0

    aget-object v3, p1, v3

    array-length v3, v3

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    move-wide/from16 v0, p3

    long-to-double v3, v0

    const-wide/high16 v5, 0x4050000000000000L    # 64.0

    div-double v6, v3, v5

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    new-array v8, v3, [D

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    array-length v3, v8

    if-ge v4, v3, :cond_0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    long-to-double v9, v9

    aput-wide v9, v8, v4

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v3, 0x0

    move v5, v3

    move v3, v4

    :goto_1
    const/16 v4, 0x40

    if-ge v5, v4, :cond_7

    const/4 v4, 0x0

    aget-wide v9, v8, v4

    int-to-double v11, v5

    mul-double/2addr v11, v6

    add-double/2addr v9, v11

    :goto_2
    aget-wide v11, v8, v3

    const-wide v13, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    add-double/2addr v11, v13

    cmpg-double v4, v11, v9

    if-gez v4, :cond_1

    array-length v4, v8

    if-ge v3, v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    array-length v4, v8

    if-ne v3, v4, :cond_3

    array-length v4, v8

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p1, v4

    aput-object v4, v2, v5

    :cond_2
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    :cond_3
    aget-wide v11, v8, v3

    sub-double v11, v9, v11

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(D)D

    move-result-wide v11

    const-wide v13, 0x3ec0c6f7a0b5ed8dL    # 2.0E-6

    cmpg-double v4, v11, v13

    if-ltz v4, :cond_4

    if-nez v3, :cond_5

    :cond_4
    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v3, -0x1

    aget-wide v11, v8, v4

    sub-double v11, v9, v11

    aget-wide v13, v8, v3

    sub-double v9, v13, v9

    add-double v13, v11, v9

    invoke-static {v13, v14}, Ljava/lang/Math;->abs(D)D

    move-result-wide v13

    const-wide v15, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v4, v13, v15

    if-gez v4, :cond_6

    aget-object v4, p1, v3

    aput-object v4, v2, v5

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    :goto_4
    aget-object v13, p1, v3

    array-length v13, v13

    if-ge v4, v13, :cond_2

    aget-object v13, v2, v5

    add-int/lit8 v14, v3, -0x1

    aget-object v14, p1, v14

    aget-wide v14, v14, v4

    mul-double/2addr v14, v9

    aget-object v16, p1, v3

    aget-wide v16, v16, v4

    mul-double v16, v16, v11

    add-double v14, v14, v16

    add-double v16, v11, v9

    div-double v14, v14, v16

    aput-wide v14, v13, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_7
    const/16 v3, 0x40

    new-array v7, v3, [D

    const/16 v3, 0x40

    new-array v8, v3, [D

    const/4 v3, 0x3

    new-array v4, v3, [D

    fill-array-data v4, :array_0

    const/4 v3, 0x0

    move-object v5, v4

    move v4, v3

    :goto_5
    const/16 v3, 0x40

    if-ge v4, v3, :cond_b

    aget-object v6, v2, v4

    invoke-static {v6, v6}, Lhne;->a([D[D)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v9

    aput-wide v9, v7, v4

    if-lez v4, :cond_a

    invoke-static {v5, v5}, Lhne;->a([D[D)D

    move-result-wide v9

    array-length v3, v6

    new-array v11, v3, [D

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const-wide v14, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v3, v12, v14

    if-gez v3, :cond_8

    const/4 v3, 0x0

    :goto_6
    array-length v5, v6

    if-ge v3, v5, :cond_9

    aget-wide v9, v6, v3

    aput-wide v9, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    invoke-static {v6, v5}, Lhne;->a([D[D)D

    move-result-wide v12

    div-double v9, v12, v9

    const/4 v3, 0x0

    :goto_7
    array-length v12, v6

    if-ge v3, v12, :cond_9

    aget-wide v12, v6, v3

    aget-wide v14, v5, v3

    mul-double/2addr v14, v9

    sub-double/2addr v12, v14

    aput-wide v12, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_9
    add-int/lit8 v3, v4, -0x1

    invoke-static {v11, v11}, Lhne;->a([D[D)D

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v9

    aput-wide v9, v8, v3

    :cond_a
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v5, v6

    goto :goto_5

    :cond_b
    const/16 v2, 0x7f

    new-array v4, v2, [D

    invoke-static {v7}, Lhne;->a([D)Ljava/util/List;

    move-result-object v5

    const/4 v2, 0x0

    move v3, v2

    :goto_8
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    aput-wide v6, v4, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    :cond_c
    invoke-static {v8}, Lhne;->a([D)Ljava/util/List;

    move-result-object v5

    const/4 v2, 0x0

    move v3, v2

    :goto_9
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v3, v2, :cond_d

    add-int/lit8 v6, v3, 0x40

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    aput-wide v7, v4, v6

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_9

    :cond_d
    return-object v4

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
    .end array-data
.end method
