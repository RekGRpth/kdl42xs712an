.class public final Lhwn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lili;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lili;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p1, v1}, Lili;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lhwn;->a:Lili;

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;ZZ)Landroid/location/Location;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    move-object p1, v0

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    if-eqz p2, :cond_2

    if-nez p3, :cond_0

    :cond_2
    invoke-static {p1}, Lilk;->e(Landroid/location/Location;)Z

    move-result v2

    if-nez p3, :cond_3

    if-eqz v2, :cond_5

    :cond_3
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-virtual {v1, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-static {p1}, Lilk;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilk;->c(Landroid/location/Location;Ljava/lang/String;)V

    invoke-static {p1}, Lilk;->b(Landroid/location/Location;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lilk;->a(Landroid/location/Location;Ljava/lang/Integer;)V

    if-eqz v2, :cond_4

    invoke-static {v1}, Lilk;->f(Landroid/location/Location;)V

    :cond_4
    move-object p1, v1

    goto :goto_0

    :cond_5
    const-string v1, "noGPSLocation"

    invoke-static {p1, v1}, Lilk;->a(Landroid/location/Location;Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Lhwn;->a:Lili;

    invoke-virtual {v0, v1}, Lili;->a(Landroid/location/Location;)Landroid/location/Location;

    move-result-object p1

    if-eqz v2, :cond_0

    invoke-static {p1}, Lilk;->f(Landroid/location/Location;)V

    goto :goto_0

    :cond_6
    move-object p1, v0

    goto :goto_0
.end method
