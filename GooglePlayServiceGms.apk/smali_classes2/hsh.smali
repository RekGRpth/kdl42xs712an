.class final Lhsh;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhsf;


# direct methods
.method constructor <init>(Lhsf;)V
    .locals 0

    iput-object p1, p0, Lhsh;->a:Lhsf;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 9

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lhsh;->a:Lhsf;

    invoke-static {v0}, Lhsf;->b(Lhsf;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsh;->a:Lhsf;

    invoke-static {v0}, Lhsf;->c(Lhsf;)Limb;

    move-result-object v0

    const-string v1, "PreScanning finished."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhsh;->a:Lhsf;

    invoke-static {v0}, Lhsf;->e(Lhsf;)Lhqz;

    move-result-object v0

    iget-object v1, p0, Lhsh;->a:Lhsf;

    invoke-static {v1}, Lhsf;->d(Lhsf;)Livi;

    invoke-interface {v0}, Lhqz;->a()Z

    move-result v1

    iget-object v0, p0, Lhsh;->a:Lhsf;

    invoke-static {v0}, Lhsf;->g(Lhsf;)Lhqq;

    move-result-object v0

    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->f(Lhsf;)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lhqq;->a(ZZ)V

    const/4 v0, 0x0

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhsh;->a:Lhsf;

    invoke-static {v1}, Lhsf;->f(Lhsf;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lhsh;->a:Lhsf;

    new-instance v1, Lhrl;

    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->c(Lhsf;)Limb;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lhrl;-><init>(Landroid/os/Handler;Limb;)V

    invoke-static {v0, v1}, Lhsf;->a(Lhsf;Lhrl;)Lhrl;

    iget-object v0, p0, Lhsh;->a:Lhsf;

    invoke-static {v0}, Lhsf;->n(Lhsf;)Lhrl;

    move-result-object v0

    iget-object v1, p0, Lhsh;->a:Lhsf;

    invoke-static {v1}, Lhsf;->h(Lhsf;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->i(Lhsf;)Lhqk;

    move-result-object v2

    iget-object v3, p0, Lhsh;->a:Lhsf;

    invoke-static {v3}, Lhsf;->j(Lhsf;)Lhsu;

    move-result-object v3

    iget-object v4, p0, Lhsh;->a:Lhsf;

    invoke-static {v4}, Lhsf;->k(Lhsf;)Liea;

    move-result-object v4

    iget-object v5, p0, Lhsh;->a:Lhsf;

    invoke-static {v5}, Lhsf;->l(Lhsf;)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lhsh;->a:Lhsf;

    invoke-static {v6}, Lhsf;->m(Lhsf;)Livi;

    move-result-object v6

    iget-object v7, p0, Lhsh;->a:Lhsf;

    invoke-static {v7}, Lhsf;->g(Lhsf;)Lhqq;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lhrl;->a(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v1, "RealScanner: Nothing to scan."

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->c(Lhsf;)Limb;

    move-result-object v2

    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->g(Lhsf;)Lhqq;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhsh;->a:Lhsf;

    invoke-static {v2}, Lhsf;->g(Lhsf;)Lhqq;

    move-result-object v2

    invoke-interface {v2, v1}, Lhqq;->a(Ljava/lang/String;)V

    :cond_2
    :goto_1
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lhsh;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_3
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_4
    :try_start_1
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhsh;->a:Lhsf;

    invoke-static {v1}, Lhsf;->c(Lhsf;)Limb;

    move-result-object v2

    iget-object v1, p0, Lhsh;->a:Lhsf;

    invoke-static {v1}, Lhsf;->f(Lhsf;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Stopped by client during prescan."

    :goto_2
    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v1, "PreScanDecision returns false. Will not do real collection."
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lhsh;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
