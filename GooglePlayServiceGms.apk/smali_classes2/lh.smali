.class public abstract Llh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lmb;


# instance fields
.field private a:I

.field private b:I

.field protected h:Landroid/content/Context;

.field protected i:Landroid/content/Context;

.field protected j:Llm;

.field protected k:Landroid/view/LayoutInflater;

.field protected l:Landroid/view/LayoutInflater;

.field public m:Lmc;

.field protected n:Lmd;

.field public o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Llh;->h:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llh;->k:Landroid/view/LayoutInflater;

    iput p2, p0, Llh;->a:I

    iput p3, p0, Llh;->b:I

    return-void
.end method


# virtual methods
.method public a(Llq;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    instance-of v0, p2, Lme;

    if-eqz v0, :cond_0

    check-cast p2, Lme;

    move-object v0, p2

    :goto_0
    invoke-virtual {p0, p1, v0}, Llh;->a(Llq;Lme;)V

    check-cast v0, Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Llh;->k:Landroid/view/LayoutInflater;

    iget v1, p0, Llh;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lme;

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;)Lmd;
    .locals 3

    iget-object v0, p0, Llh;->n:Lmd;

    if-nez v0, :cond_0

    iget-object v0, p0, Llh;->k:Landroid/view/LayoutInflater;

    iget v1, p0, Llh;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmd;

    iput-object v0, p0, Llh;->n:Lmd;

    iget-object v0, p0, Llh;->n:Lmd;

    iget-object v1, p0, Llh;->j:Llm;

    invoke-interface {v0, v1}, Lmd;->a(Llm;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Llh;->a(Z)V

    :cond_0
    iget-object v0, p0, Llh;->n:Lmd;

    return-object v0
.end method

.method public a(Landroid/content/Context;Llm;)V
    .locals 1

    iput-object p1, p0, Llh;->i:Landroid/content/Context;

    iget-object v0, p0, Llh;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llh;->l:Landroid/view/LayoutInflater;

    iput-object p2, p0, Llh;->j:Llm;

    return-void
.end method

.method public a(Llm;Z)V
    .locals 1

    iget-object v0, p0, Llh;->m:Lmc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llh;->m:Lmc;

    invoke-interface {v0, p1, p2}, Lmc;->a(Llm;Z)V

    :cond_0
    return-void
.end method

.method public abstract a(Llq;Lme;)V
.end method

.method public a(Z)V
    .locals 10

    const/4 v5, 0x0

    iget-object v0, p0, Llh;->n:Lmd;

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Llh;->j:Llm;

    if-eqz v1, :cond_8

    iget-object v1, p0, Llh;->j:Llm;

    invoke-virtual {v1}, Llm;->l()V

    iget-object v1, p0, Llh;->j:Llm;

    invoke-virtual {v1}, Llm;->k()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    move v4, v5

    :goto_0
    if-ge v6, v8, :cond_6

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Llq;

    invoke-virtual {p0, v1}, Llh;->a(Llq;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v2, v3, Lme;

    if-eqz v2, :cond_5

    move-object v2, v3

    check-cast v2, Lme;

    invoke-interface {v2}, Lme;->a()Llq;

    move-result-object v2

    :goto_1
    invoke-virtual {p0, v1, v3, v0}, Llh;->a(Llq;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    if-eq v1, v2, :cond_2

    invoke-virtual {v9, v5}, Landroid/view/View;->setPressed(Z)V

    :cond_2
    if-eq v9, v3, :cond_4

    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Llh;->n:Lmd;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v9, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :cond_4
    add-int/lit8 v1, v4, 0x1

    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    :goto_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v4, v1, :cond_0

    invoke-virtual {p0, v0, v4}, Llh;->a(Landroid/view/ViewGroup;I)Z

    move-result v1

    if-nez v1, :cond_6

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    move v1, v4

    goto :goto_2

    :cond_8
    move v4, v5

    goto :goto_3
.end method

.method public a(Landroid/view/ViewGroup;I)Z
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Llq;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lmh;)Z
    .locals 1

    iget-object v0, p0, Llh;->m:Lmc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llh;->m:Lmc;

    invoke-interface {v0, p1}, Lmc;->b(Llm;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
