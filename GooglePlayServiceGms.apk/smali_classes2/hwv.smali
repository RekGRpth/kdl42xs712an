.class final Lhwv;
.super Lhww;
.source "SourceFile"

# interfaces
.implements Lhlo;


# instance fields
.field final synthetic a:Lhws;

.field private c:J


# direct methods
.method private constructor <init>(Lhws;)V
    .locals 1

    iput-object p1, p0, Lhwv;->a:Lhws;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhww;-><init>(Lhws;B)V

    return-void
.end method

.method synthetic constructor <init>(Lhws;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhwv;-><init>(Lhws;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    iget-object v0, p0, Lhwv;->a:Lhws;

    invoke-static {v0}, Lhws;->a(Lhws;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lhwt;

    iget-object v1, p0, Lhwv;->a:Lhws;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhwt;-><init>(Lhws;B)V

    invoke-virtual {p0, v0}, Lhwv;->a(Lhww;)Lhww;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->c:Lhxf;

    iput-object p0, v0, Lhxf;->a:Lhlo;

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->b()V

    iget-object v0, p0, Lhwv;->a:Lhws;

    invoke-virtual {v0, p1}, Lhws;->a(Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhwv;->c:J

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->c()V

    return-void
.end method

.method public final d()J
    .locals 2

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->e:Lhwm;

    iget-wide v0, v0, Lhwm;->c:J

    return-wide v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->e:Lhwm;

    iget-object v0, v0, Lhwm;->g:Ljava/util/Collection;

    return-object v0
.end method

.method public final g()V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lhwv;->c:J

    sub-long/2addr v2, v4

    iget-object v0, p0, Lhwv;->a:Lhws;

    iget-object v0, v0, Lhws;->e:Lhwm;

    iget-wide v4, v0, Lhwm;->b:J

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lhwt;

    iget-object v3, p0, Lhwv;->a:Lhws;

    invoke-direct {v2, v3, v1}, Lhwt;-><init>(Lhws;B)V

    invoke-virtual {p0, v2}, Lhwv;->a(Lhww;)Lhww;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhww;->a(Z)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
