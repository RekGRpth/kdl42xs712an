.class public final Lffu;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 12

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {p0}, Lffe;->a(Landroid/content/Context;)Lffe;

    move-result-object v5

    invoke-static {p0, p1, p2, p3}, Lffe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lffa;

    move-result-object v6

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v2

    :cond_0
    :try_start_0
    invoke-virtual {v5, p0, v6, v0}, Lffe;->a(Landroid/content/Context;Lffa;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->e()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    move v4, v3

    :goto_0
    if-ge v4, v9, :cond_5

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    const-string v10, "circle"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v11

    invoke-interface {v11}, Lgld;->d()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->d()Lgld;

    move-result-object v0

    invoke-interface {v0}, Lgld;->c()Ljava/lang/String;

    move-result-object v11

    const-string v0, "limited"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    invoke-virtual {v1, v11, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    const-string v0, "public"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const-string v0, "private"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    invoke-virtual {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->f()Ljava/lang/String;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_2
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleService"

    const-string v3, "Authentication error"

    invoke-static {v1, v3, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v2

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "PeopleService"

    const-string v3, "Network error"

    invoke-static {v1, v3, v0}, Lfjv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v2

    goto :goto_2
.end method

.method static a(Lffe;Landroid/content/Context;Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    .locals 17

    invoke-static/range {p1 .. p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p2 .. p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "qualifiedId"

    move-object/from16 v0, p3

    invoke-static {v0, v2}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p4, :cond_0

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct/range {p4 .. p4}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    if-nez p5, :cond_1

    new-instance p5, Ljava/util/ArrayList;

    invoke-direct/range {p5 .. p5}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v8, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v9, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static/range {p3 .. p3}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static/range {p3 .. p3}, Lfdl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v7}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v6, :cond_4

    :try_start_0
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2, v8, v9}, Lffe;->a(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v12, "PeopleService"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_2

    const-string v12, "PeopleService"

    const-string v13, "%s added to %s"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object p3, v14, v15

    const/4 v15, 0x1

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    :cond_3
    invoke-static/range {p3 .. p3}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v4, v2

    :cond_4
    :goto_2
    if-nez v4, :cond_a

    if-nez v3, :cond_a

    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v7, :cond_a

    :try_start_1
    move-object/from16 v0, p5

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2, v8, v9}, Lffe;->b(Lffa;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v6, "PeopleService"

    const/4 v12, 0x3

    invoke-static {v6, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "PeopleService"

    const-string v12, "%s removed from %s"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p3, v13, v14

    const/4 v14, 0x1

    aput-object v2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lamq; {:try_start_1 .. :try_end_1} :catch_3

    :cond_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    :catch_1
    move-exception v2

    move-object v3, v2

    goto :goto_2

    :catch_2
    move-exception v2

    move-object/from16 v16, v3

    move-object v3, v2

    move-object/from16 v2, v16

    :goto_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-nez v4, :cond_9

    if-eqz v3, :cond_7

    const-string v2, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "PeopleService"

    const-string v4, "Error during a server call."

    invoke-static {v2, v4, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_6
    throw v3

    :catch_3
    move-exception v2

    move-object v3, v4

    goto :goto_4

    :cond_7
    if-eqz v2, :cond_9

    const-string v3, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "PeopleService"

    const-string v4, "Error during a server call."

    invoke-static {v3, v4, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_8
    throw v2

    :cond_9
    invoke-static {v10, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    return-object v2

    :cond_a
    move-object v2, v3

    move-object v3, v4

    goto :goto_4
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;ILjava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    invoke-static/range {p0 .. p13}, Lffu;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;ILjava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->c()Lfbn;

    move-result-object v2

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v3

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-static {p0}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v5

    array-length v6, v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_2

    aget-object v7, v5, v1

    new-instance v8, Lcom/google/android/gms/people/model/AccountMetadata;

    invoke-direct {v8}, Lcom/google/android/gms/people/model/AccountMetadata;-><init>()V

    invoke-static {p0, v7}, Lfgy;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/people/model/AccountMetadata;->b:Z

    iget-object v0, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {v3, v0, v9}, Lfhz;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/people/model/AccountMetadata;->c:Z

    iget-object v0, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3}, Lfhz;->b()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v3, v0}, Lfhz;->e(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, v8, Lcom/google/android/gms/people/model/AccountMetadata;->d:Z

    iget-object v0, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v9, "pageid"

    invoke-virtual {v3, v0, v9}, Lfhz;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/people/model/AccountMetadata;->e:Z

    iget-object v0, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v0}, Lfhz;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "account_metadata"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {}, Lfdl;->e()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    const-string v0, "1"

    :goto_2
    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p2}, Lfdl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-static {p3}, Lfdl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    if-nez p4, :cond_4

    const-string v0, "account_name,(page_gaia_id IS NOT NULL),display_name COLLATE LOCALIZED"

    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT _id,account_name,display_name,gaia_id,page_gaia_id,avatar,cover_photo_url,cover_photo_height,cover_photo_width,cover_photo_id,last_sync_start_time,last_sync_finish_time,last_sync_status,last_successful_sync_time,sync_to_contacts,sync_circles_to_contacts,sync_evergreen_to_contacts,is_dasher,dasher_domain FROM owners leftOwners  WHERE ((?2=\'\') OR (account_name=?2 AND (((?3 = \'\') AND (page_gaia_id IS NULL)) OR (?3=page_gaia_id)))) AND ( ?1 OR (page_gaia_id IS NULL)) ORDER BY "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v2

    :cond_3
    const-string v0, "0"

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    if-ne p4, v0, :cond_5

    const-string v0, "(SELECT _id FROM owners WHERE account_name=leftOwners.account_name AND page_gaia_id IS NULL) ,(page_gaia_id IS NOT NULL),display_name COLLATE LOCALIZED"

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Value of sortOrder isn\'t valid.sortOrder= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    if-nez p5, :cond_0

    sget-object p5, Lfnp;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Lbmt;

    invoke-direct {v0, p0}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v0

    sget-object v1, Lfnx;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    invoke-static {p3}, Lbcl;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-static {p0, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lbmt;

    invoke-direct {v0, p0}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v0

    sget-object v1, Lfnx;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    invoke-static {p4}, Lbcl;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-static {p0, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    :cond_2
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "p"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbmt;

    invoke-direct {v0, p0}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v2

    if-eqz p3, :cond_0

    sget-object v0, Lfnx;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-virtual {v2, v0}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    sget-object v2, Lfnp;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    invoke-static {v1}, Lbcl;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-static {p0, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    return-void

    :cond_0
    sget-object v0, Lfnx;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 13

    new-instance v0, Lfgg;

    move-object v1, p1

    move-object v2, p2

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p3

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lfgg;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {p0, p1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglh;

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Lglh;->d()Z

    move-result v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)[Lcom/google/android/gms/common/data/DataHolder;
    .locals 17

    or-int/lit8 v2, p6, 0x2

    or-int/lit8 v2, v2, 0x4

    or-int/lit8 v7, v2, 0x1

    invoke-static/range {p3 .. p3}, Lfdl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ""

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "name COLLATE LOCALIZED,_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v6, 0x0

    :goto_1
    sget-object v2, Lfbd;->x:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_2
    if-eqz p9, :cond_2

    sget-object v2, Lfbd;->v:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lfbd;->w:Lbfy;

    invoke-virtual {v2}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v2

    invoke-virtual {v2}, Lfbc;->a()Lfbe;

    move-result-object v2

    invoke-virtual {v2}, Lfbe;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lfbo;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v15, 0x1

    :goto_3
    invoke-static/range {p0 .. p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v2

    invoke-virtual {v2}, Lfbo;->c()Lfbn;

    move-result-object v16

    const/4 v5, 0x0

    const-wide/16 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v8, p5

    move/from16 v12, p4

    move/from16 v14, p7

    invoke-static/range {v2 .. v15}, Lffu;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;ILjava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    :try_start_0
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "SELECT DISTINCT gaia_id,value FROM gaia_id_map WHERE type!=4"

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v3, 0x1

    new-instance v5, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v2, v6, v7}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    aput-object v5, v4, v3

    return-object v4

    :cond_0
    const-string v2, "sort_key_irank DESC,"

    goto/16 :goto_0

    :cond_1
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {p8 .. p8}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    goto/16 :goto_1

    :pswitch_0
    const/4 v15, 0x1

    goto :goto_3

    :pswitch_1
    const/16 p9, 0x1

    goto/16 :goto_2

    :cond_2
    const/4 v15, 0x0

    goto :goto_3

    :cond_3
    :try_start_1
    const-string v2, "SELECT DISTINCT gaia_id,value FROM gaia_id_map WHERE type!=4 AND gaia_id=?"

    invoke-static/range {p8 .. p8}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v4}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Landroid/database/AbstractWindowedCursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception v2

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;ILjava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    invoke-static/range {p11 .. p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p11, "sort_key"

    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    invoke-virtual {v3, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "pagegaiaid"

    invoke-virtual {v3, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->c()Lfbn;

    move-result-object v4

    invoke-static {p0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v1

    invoke-virtual {v1}, Lfbo;->e()Lfbk;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Lfgv;

    invoke-direct {v7, v6}, Lfgv;-><init>(Ljava/lang/StringBuilder;)V

    const/4 v8, 0x1

    const-string v9, "qualified_id"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/4 v8, 0x2

    const-string v9, "gaia_id"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/4 v8, 0x4

    const-string v9, "name"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x1000

    const-string v9, "given_name"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x2000

    const-string v9, "family_name"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x800

    const-string v9, "name_verified"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x8

    const-string v9, "sort_key"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x10

    const-string v9, "sort_key_irank"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x20

    const-string v9, "avatar"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x40

    const-string v9, "profile_type"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x80

    const-string v9, "(SELECT group_concat(circle_id) FROM circle_members AS CM  WHERE CM.owner_id==P.owner_id AND CM.qualified_id=P.qualified_id)AS v_circle_ids"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x100

    const-string v9, "blocked"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x200

    const-string v9, "last_modified"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/16 v8, 0x400

    const-string v9, "in_viewer_domain"

    invoke-virtual {v7, p5, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/4 v8, 0x1

    const-string v9, "(SELECT group_concat(type||\"\u0001\"||ifnull(custom_label,\'\')||\"\u0001\"||email,\"\u0002\") FROM emails AS EM  WHERE EM.owner_id==P.owner_id AND EM.qualified_id=P.qualified_id)AS v_emails"

    move/from16 v0, p12

    invoke-virtual {v7, v0, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const/4 v8, 0x2

    const-string v9, "(SELECT group_concat(type||\"\u0001\"||ifnull(custom_label,\'\')||\"\u0001\"||phone,\"\u0002\") FROM phones AS PH  WHERE PH.owner_id==P.owner_id AND PH.qualified_id=P.qualified_id)AS v_phones"

    move/from16 v0, p12

    invoke-virtual {v7, v0, v8, v9}, Lfgv;->a(IILjava/lang/String;)V

    const-string v7, " FROM people AS P"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " WHERE (owner_id = ?)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, " AND (qualified_id IN (SELECT qualified_id FROM circle_members WHERE owner_id=P.owner_id AND circle_id=?))"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz p6, :cond_1

    const-string v1, " AND (profile_type=1)"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-wide/16 v1, 0x0

    cmp-long v1, p7, v1

    if-eqz v1, :cond_2

    const-string v1, " AND (last_modified>=?)"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {p7 .. p8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz p4, :cond_8

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    const-string v1, " AND qualified_id IN ("

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_1
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_7

    if-nez v1, :cond_6

    const-string v2, "?"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string v1, " AND ((in_circle =1)"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p13, :cond_5

    const-string v1, " OR (in_contacts=1"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lfbd;->W:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, " AND EXISTS (SELECT 1 FROM gaia_id_map G  WHERE G.owner_id=P.owner_id AND G.type!=2 AND G.gaia_id=P.gaia_id)"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v1, ")"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v1, ")"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_6
    const-string v2, ",?"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    const-string v1, ")"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    invoke-static/range {p9 .. p9}, Lfdl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    sget-object v2, Lfdl;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    :goto_3
    array-length v2, v7

    if-ge v1, v2, :cond_d

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    aget-object v2, v7, v1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lfbj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, "%"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, " AND (_id IN "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "(SELECT "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "person_id"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " FROM search_index WHERE ((value LIKE ?"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " AND kind!=1) OR (value LIKE ?"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " AND kind=1))"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    move/from16 v0, p10

    if-eq v0, v2, :cond_c

    const-string v2, " AND kind IN("

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ""

    and-int/lit8 v8, p10, 0x1

    if-eqz v8, :cond_9

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    :cond_9
    and-int/lit8 v8, p10, 0x2

    if-eqz v8, :cond_a

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ","

    :cond_a
    and-int/lit8 v8, p10, 0x4

    if-eqz v8, :cond_b

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_b
    const-string v2, ")"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const-string v2, ")"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_d
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ORDER BY "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lfdl;->c:[Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const-string v2, "PeopleService"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "PeopleService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Query="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "  args="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v1}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    check-cast v1, Landroid/database/AbstractWindowedCursor;

    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v4, 0x0

    invoke-direct {v2, v1, v4, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v2
.end method

.method static synthetic b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->d()Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglh;

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Lglh;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
