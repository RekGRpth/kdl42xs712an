.class public final Lbre;
.super Lchp;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lbrc;

.field private final c:Lbrl;

.field private final d:Lcgw;

.field private final e:Lbra;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lbrc;Lbra;Lbrl;)V
    .locals 2

    invoke-direct {p0}, Lchp;-><init>()V

    iput-object p1, p0, Lbre;->a:Landroid/content/Context;

    iput-object p2, p0, Lbre;->b:Lbrc;

    iput-object p4, p0, Lbre;->c:Lbrl;

    new-instance v0, Lcgw;

    invoke-direct {v0}, Lcgw;-><init>()V

    iput-object v0, p0, Lbre;->d:Lcgw;

    iput-object p3, p0, Lbre;->e:Lbra;

    iget-object v0, p0, Lbre;->e:Lbra;

    iget-object v1, v0, Lbra;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lbra;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lbrc;Lbra;Lbrl;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lbre;-><init>(Landroid/content/Context;Lbrc;Lbra;Lbrl;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;)Landroid/content/IntentSender;
    .locals 6

    const/4 v4, 0x0

    :try_start_0
    iget-object v0, p0, Lbre;->b:Lbrc;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lbrc;->a(Z)Lbsp;

    move-result-object v1

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    iget-object v2, v1, Lbsp;->a:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->b()I

    move-result v3

    if-ltz v3, :cond_0

    const/4 v4, 0x1

    :cond_0
    const-string v5, "The request id must be provided."

    invoke-static {v4, v5}, Lbkm;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->d()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/data/ui/create/CreateFileActivityDelegate;->a(Landroid/content/Context;Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v1, v2}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;
    :try_end_0
    .catch Lbsl; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "DriveService"

    const-string v2, "Authorization failed"

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;)Landroid/content/IntentSender;
    .locals 5

    :try_start_0
    iget-object v0, p0, Lbre;->b:Lbrc;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lbrc;->a(Z)Lbsp;

    move-result-object v0

    iget-object v1, p0, Lbre;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->c()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->a(Landroid/content/Context;Lbsp;)Lbyf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbyf;->a(Ljava/lang/String;)Lbyf;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbyf;->a([Ljava/lang/String;)Lbyf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbyf;->a(Lcom/google/android/gms/drive/DriveId;)Lbyf;

    move-result-object v0

    invoke-virtual {v0}, Lbyf;->a()Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v0, v2}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;
    :try_end_0
    .catch Lbsl; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "DriveService"

    const-string v2, "Authorization failed"

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lbre;->b:Lbrc;

    invoke-interface {v0}, Lbrc;->e()V

    iget-object v0, p0, Lbre;->e:Lbra;

    iget-object v1, v0, Lbra;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lbra;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lchq;)V
    .locals 4

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbru;

    iget-object v2, p0, Lbre;->b:Lbrc;

    iget-object v3, p0, Lbre;->c:Lbrl;

    invoke-direct {v1, v2, v3, p1}, Lbru;-><init>(Lbrc;Lbrl;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V
    .locals 7

    iget-object v6, p0, Lbre;->a:Landroid/content/Context;

    new-instance v0, Lbrn;

    iget-object v1, p0, Lbre;->b:Lbrc;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbrn;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrq;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbrq;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lchq;)V
    .locals 8

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v4

    iget-object v7, p0, Lbre;->a:Landroid/content/Context;

    new-instance v0, Lbrs;

    iget-object v1, p0, Lbre;->b:Lbrc;

    iget-object v2, p0, Lbre;->c:Lbrl;

    invoke-virtual {v4}, Lcoy;->s()Lbsr;

    move-result-object v3

    invoke-virtual {v4}, Lcoy;->r()Lcnf;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lbrs;-><init>(Lbrc;Lbrl;Lbsr;Lcnf;Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lchq;)V

    invoke-static {v7, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lchq;)V
    .locals 4

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrt;

    iget-object v2, p0, Lbre;->b:Lbrc;

    iget-object v3, p0, Lbre;->c:Lbrl;

    invoke-direct {v1, v2, v3, p1, p2}, Lbrt;-><init>(Lbrc;Lbrl;Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrv;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbrv;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/CreateFileRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrw;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbrw;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/DisconnectRequest;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrx;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p0}, Lbrx;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/DisconnectRequest;Lbre;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbry;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbry;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/ListParentsRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbrz;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbrz;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/ListParentsRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lchq;)V
    .locals 4

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbsa;

    iget-object v2, p0, Lbre;->b:Lbrc;

    iget-object v3, p0, Lbre;->c:Lbrl;

    invoke-direct {v1, v2, v3, p1, p2}, Lbsa;-><init>(Lbrc;Lbrl;Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/QueryRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbsc;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbsc;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/QueryRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V
    .locals 7

    iget-object v6, p0, Lbre;->a:Landroid/content/Context;

    new-instance v0, Lbsd;

    iget-object v1, p0, Lbre;->b:Lbrc;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbsd;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Lcht;Ljava/lang/String;Lchq;)V

    invoke-static {v6, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbsg;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbsg;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbsi;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1, p2}, Lbsi;-><init>(Lbrc;Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method final b()Lbrc;
    .locals 1

    iget-object v0, p0, Lbre;->b:Lbrc;

    return-object v0
.end method

.method public final b(Lchq;)V
    .locals 3

    iget-object v0, p0, Lbre;->a:Landroid/content/Context;

    new-instance v1, Lbse;

    iget-object v2, p0, Lbre;->b:Lbrc;

    invoke-direct {v1, v2, p1}, Lbse;-><init>(Lbrc;Lchq;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lbqz;)V

    return-void
.end method

.method public final binderDied()V
    .locals 0

    invoke-virtual {p0}, Lbre;->a()V

    return-void
.end method
