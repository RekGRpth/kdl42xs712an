.class public final Lheq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljan;


# direct methods
.method public constructor <init>(Ljau;)V
    .locals 4

    iget-object v1, p1, Ljau;->f:Ljava/lang/String;

    iget-object v2, p1, Ljau;->a:Ljan;

    iget-object v3, p1, Ljau;->b:Ljava/lang/String;

    iget-object v0, p1, Ljau;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Ljau;->c:Ljava/lang/String;

    :goto_0
    invoke-direct {p0, v1, v2, v3, v0}, Lheq;-><init>(Ljava/lang/String;Ljan;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljan;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u001f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u001f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p4, :cond_0

    :goto_0
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lheq;->a:Ljava/lang/String;

    iput-object p2, p0, Lheq;->b:Ljan;

    return-void

    :cond_0
    const-string p4, ""

    goto :goto_0
.end method

.method public constructor <init>(Ljbg;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p1, Ljbg;->b:Ljava/lang/String;

    iget-object v1, p1, Ljbg;->a:Ljan;

    invoke-direct {p0, v0, v1, p2, p3}, Lheq;-><init>(Ljava/lang/String;Ljan;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lheq;

    iget-object v2, p0, Lheq;->a:Ljava/lang/String;

    iget-object v3, p1, Lheq;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lheq;->b:Ljan;

    iget-object v3, p1, Lheq;->b:Ljan;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Lizs;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lheq;->a:Ljava/lang/String;

    iget-object v1, p0, Lheq;->b:Ljan;

    invoke-static {v0, v1}, Lgtj;->a(Ljava/lang/String;Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
