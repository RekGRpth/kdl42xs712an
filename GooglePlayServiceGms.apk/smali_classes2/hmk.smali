.class final Lhmk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0xa

    const-wide v7, 0x3fc3333333333333L    # 0.15

    const/16 v6, 0xb

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3feaa8848387df5dL    # 0.833071

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd222d5171e29b7L    # 0.283376

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6372a38b8ae31dL    # 0.002374

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd948b220791c4cL    # 0.395062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2733226c3b927dL    # 1.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1de26916440f24L    # 1.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe6800000000000L    # 0.703125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f588b11409a2403L    # 0.001498

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f949cf56eac8605L    # 0.02013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff9f3edd8b60f1bL    # 1.622053

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d5c31593e5fb7L    # 1.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3eed5c31593e5fb7L    # 1.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40018d2e514c22eeL    # 2.193936

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402506fc158fb43eL    # 10.513642

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f636a400fba8827L    # 0.00237

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0ffb480a5accd5L    # 6.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f04727dcbddb984L    # 3.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f572ef0ae536502L    # 0.001415

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f164840e1719f80L    # 8.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f26052502eec7c9L    # 1.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd8d5347a5b0ff1L    # 0.388013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3b97353b4b2fa9L    # 4.21E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffe2991ff7164c7L    # 1.885149

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0bb6672fba01fL    # 0.522876

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbb074a771c970fL    # 0.10558

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faa32096787ce96L    # 0.051163

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff79104d551d68cL    # 1.472905

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f19b0ab2e1693c0L    # 9.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022ea8262456f76L    # 9.458026

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa48f75536933a0L    # 0.040157

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcb6db940fecdd1L    # 0.214286

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f49008205ff1d82L    # 7.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40f776c4827b7L    # 0.156722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd70e4da09cc31aL    # 0.360248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2fb82c2bd7f51fL    # 2.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f893ab430f49492L    # 0.012319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f451a437824d4ccL    # 6.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e2584f4c6e6daL    # 2.3E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a79fec99f1ae3L    # 4.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7693c03bc4d22dL    # 0.005512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fad5f78359bc340L    # 0.057369

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd7a4b124d099e1L    # 0.369427

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff1559a30984e40L    # 1.083399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb7baa9b499d020L    # 0.092692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd7588e368f0846L    # 0.36478

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffafdd97f62b6aeL    # 1.686975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f438fffbce4217dL    # 5.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f221682f944241cL    # 1.38E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4127b2cc70868L    # 0.156814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7184c271fff79dL    # 0.004277

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa51e96c3fc43b3L    # 0.041249

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff8ca6117720c8dL    # 1.549409

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f04f8b588e368f1L    # 4.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff4035696e58a33L    # 1.250815

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f856f32bdc26dceL    # 0.010466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f524d099e0e7360L    # 0.001117

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f317f84449dbec2L    # 2.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6a6d698fe69271L    # 0.003226

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f96cdaf4adbc665L    # 0.022269

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ac36544fe36d2L    # 0.003267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffab8055fbb517aL    # 1.669927

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f227b2cc70867aeL    # 1.41E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f04f8b588e368f1L    # 4.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c25d074213a0cL    # 0.001718

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f168b5cbff47736L    # 8.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6615ebfa8f7db7L    # 0.002696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f19b0ab2e1693c0L    # 9.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f340dd3fe1975f3L    # 3.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb546d3f9e7b80bL    # 0.083112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f8a8f3a9b068L    # 0.156026

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fe43675ddd2bL    # 0.156197

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2b21c475e6362aL    # 2.07E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f417720c8cd63ccL    # 5.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0c4fc1df3300deL    # 5.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4e81cb46bacf74L    # 9.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x400a1f9d6f112fd3L    # 3.265437

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff904e618ce2d1fL    # 1.563696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcaf42fe82517e7L    # 0.210577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f650b955f78359cL    # 0.002569

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fddbaa1511dffc5L    # 0.464516

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37b95a294141eaL    # 3.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a36e2eb1c432dL    # 2.0E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40215a7a63736cdfL    # 8.676715

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5e1d2178f68be3L    # 0.001838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5e4712e40852b5L    # 0.001848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f60539fba450accL    # 0.001993

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde1d323fee2c99L    # 0.470532

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021872c0e7bc3c6L    # 8.764008

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40021d75e2046c76L    # 2.264385

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4d4b6a619da9caL    # 8.94E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f45b1422ccb3a26L    # 6.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f413c68661ae70cL    # 5.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb3f2b239a38fbdL    # 0.077922

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f49d2391d57ff9bL    # 7.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f89da16616b54e3L    # 0.012623

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f142f61ed5ae1ceL    # 7.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6856a37ac3eb7dL    # 0.002971

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f50be9424e59296L    # 0.001022

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23879c4113c686L    # 1.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4024f49363f572deL    # 10.477687

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8906466b1e5c0cL    # 0.012219

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f16ce789e774eecL    # 8.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400178e3ac0c62e5L    # 2.184028

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc3716d2aa5c5f8L    # 0.151899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0abd1aa821f299L    # 5.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401158344c37e6f7L    # 4.336137

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f2778140dd40L    # 0.155837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400544840e1719f8L    # 2.658455

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbecaeea63b688cL    # 0.120284

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fa6f3b213e3e3L    # 7.913039

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f34e7ee9142b303L    # 3.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6d14e3bcd35a86L    # 0.00355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1f31f46ed245b3L    # 1.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d82d9cf13ceeaL    # 14.755568

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0f75104d551d69L    # 6.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5f4b1ee2435697L    # 0.00191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5e9f2778140dd4L    # 0.001869

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f121682f944241cL    # 6.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcf4c3c18b502acL    # 0.244514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd276295208e150L    # 0.288462

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f02dfd694ccab3fL    # 3.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f90c62e4d1a6506L    # 0.016381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe39435ac8a3726L    # 0.611842

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5144cbe1eb4203L    # 0.001054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402548f3882278d1L    # 10.642483

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fce412cf0f9d2bfL    # 0.236364

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdcdc8b86b15f89L    # 0.450961

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc94b1ee2435697L    # 0.197605

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbab356da0168b6L    # 0.104299

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f76cf850df15a4bL    # 0.005569

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd22da554b8bef9L    # 0.284036

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9fda4052d666aaL    # 0.031106

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb0e7ddca4b124dL    # 0.066038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f164840e1719f80L    # 8.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f64a6eb91b3f20aL    # 0.002521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe4a528ae74f2f1L    # 0.645161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0a36e2eb1c432dL    # 5.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4000cfe69270b06cL    # 2.101514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f210a137f38c543L    # 1.3E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6da37ef5a964e9L    # 0.003618

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f09b0ab2e1693c0L    # 4.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe3bd017dae8188L    # 0.616822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9844d013a92a30L    # 0.0237

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d9f4d37c1376dL    # 1.13E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa030e3cd9a5226L    # 0.031623

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbc9eecbfb15b57L    # 0.1118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd90b1feeb2d0a2L    # 0.391304

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4002d3775b813016L    # 2.353255

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1eabbcb1cc9646L    # 1.17E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f34938583622L    # 0.155862

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fad2e514c22ee42L    # 0.056994

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf984e3ffef391L    # 0.493671

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f24727dcbddb984L    # 1.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f64d50ebaade658L    # 0.002543

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67b30f8c64fdb1L    # 0.002893

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f69014b599aaL    # 0.155962

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd81c0ca600b029L    # 0.376712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67cc39ffd60e95L    # 0.002905

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff49715c63ae254L    # 1.286886

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5272c94b380cb7L    # 0.001126

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe161f9f01b866eL    # 0.54321

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbb4cc25072085bL    # 0.10664

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb3f66e86c6583fL    # 0.077979

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f66feb4a66559f7L    # 0.002807

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f20624dd2f1a9fcL    # 1.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4021073bea91d9b2L    # 8.514129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb8a44c7b02d59dL    # 0.096257

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb5058dde7a743aL    # 0.082116

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f90d7be9856a37bL    # 0.016448

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ae70c1333b96bL    # 8.21E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fddec0b5675579bL    # 0.467532

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6a5ca29845dc83L    # 0.003218

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3fe92d3c792d1288L    # 0.786772

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40ac7da1ec4e7L    # 0.156579

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0bc98a222d5172L    # 5.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f04727dcbddb984L    # 3.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4050374ff865dL    # 0.156403

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f872799a1fd156aL    # 0.011306

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5b089a02752546L    # 0.00165

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    iget-wide v0, p1, Lhmx;->d:D

    const-wide/high16 v2, 0x3fb4000000000000L    # 0.078125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc406e19b90ea9eL    # 0.15646

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f701d19157abb88L    # 0.003934

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f598aeb80ecfa6aL    # 0.001559

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x400b95275ee99a63L    # 3.447829

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3871e6cd29131fL    # 3.73E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40f776c4827b7L    # 0.156722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2e4712e40852b5L    # 2.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2908e581cf7879L    # 1.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa1947cfa26a22bL    # 0.034336

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4554fbdad7518bL    # 6.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f936501e2584f4cL    # 0.01894

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f715379fa97e133L    # 0.00423

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f920fb224aada34L    # 0.017638

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fdcbff8a8f3a9b0L    # 0.449217

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd075c4a83b1d0dL    # 0.257188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc65c896dd26b72L    # 0.174699

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4aac53b0813cacL    # 8.14E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4804d983947497L    # 7.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f38b502ababead5L    # 3.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404924c0bdcad14aL    # 50.287132

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9102795703f2d4L    # 0.016611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb7524399b2c40dL    # 0.091099

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f39d2391d57ff9bL    # 3.94E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f58c5c9a34ca0c3L    # 0.001512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7413122b7baecdL    # 0.004901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f292a737110e454L    # 1.92E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f14b599aa60913aL    # 7.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd6e9e1b089a027L    # 0.358025

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40371dee78183f92L    # 23.11692

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402f2b0a4e379b78L    # 15.584063

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f841e9af5ba2be0L    # 0.009824

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb1eb4202d9cf14L    # 0.069996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd48ebcc6c54bcfL    # 0.321212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc5cdee34fc610fL    # 0.170347

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd215e39713ad5cL    # 0.282586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400ba784a9478c87L    # 3.456796

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f7d73c9257860L    # 9.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f29f3c70c996b76L    # 1.98E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f967b5f1bef49cfL    # 0.021955

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f35f45e0b4e11dcL    # 3.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4c68ec52a411c3L    # 8.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8442c7fbacb429L    # 0.009893

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a5870da5daf08L    # 4.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f70cf5b1c864884L    # 0.004104

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020b6518f3eccc4L    # 8.356091

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402423124d099e0eL    # 10.068499

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f75d3dc8b86b160L    # 0.005329

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3040bfe3b03e21L    # 2.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40589acbc8c0dL    # 0.156419

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa62eb1c432ca58L    # 0.043325

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0e4f765fd8adbL    # 0.52795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc045996744b2b7L    # 0.127124

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f142f61ed5ae1ceL    # 7.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40251c3f5f91600fL    # 10.555171

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13ec460ed80a18L    # 7.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef3ec460ed80a18L    # 1.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b9f98b71b8aa0L    # 8.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ee711947cfa26a2L    # 1.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40000c0d6f544bb2L    # 2.005885

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2882adc4c9c90cL    # 1.87E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f74f26aec0724b7L    # 0.005114

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f82ccf6be37de94L    # 0.00918

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402848e5e679463dL    # 12.142379

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb025aee631f8a1L    # 0.063075

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76b54e2b063e08L    # 0.005544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f43b18dac258d58L    # 6.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd375f6fd21ff2eL    # 0.304075

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f29d2391d57ff9bL    # 1.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f08a43bb40b34e7L    # 4.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0eeed8904f6dfcL    # 5.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f51b1d92b7fe08bL    # 0.00108

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdc3dc486ad2dcbL    # 0.44127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f196d8f4f93bc0aL    # 9.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f77a89331a08bfcL    # 0.005776

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7c054ef459d990L    # 0.006841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37dae81882adc5L    # 3.64E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4461b6d43d0397L    # 6.22E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40713f077ccc0L    # 0.156466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe24573a797891eL    # 0.570978

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efe68a0d349be90L    # 2.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f393b3a68b19a41L    # 3.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x400147fcb923a29cL    # 2.16015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022df31d2e0e304L    # 9.435927

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd0e7d9988d2a20L    # 0.264151

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40102a593a2df938L    # 4.041356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb490c4dec1c1d7L    # 0.080334

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff40a7c5ac471b4L    # 1.25256

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023ab8cda6e75ffL    # 9.835059

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f75a8deb0fadf2fL    # 0.005288

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4001f029ae4f3344L    # 2.242267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fad4bf0995aaf79L    # 0.05722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff20ff759685122L    # 1.128898

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3333b96af038e3L    # 2.93E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f44488c60cbf2b2L    # 6.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40056197e5647347L    # 2.672653

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff19785f8d2e515L    # 1.099493

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff19b24e9c45458L    # 1.100377

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402d90a58b3f63c3L    # 14.782513

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ec918e325d4aL    # 0.155657

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff9e3864cb5bb38L    # 1.618048

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc097d8cf398e97L    # 0.129634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde16c1e364bec6L    # 0.470139

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd3666666666666L    # 0.303125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fec99f1ae2daL    # 0.156213

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb8755ffe6d58c9L    # 0.095541

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d2306a2b17050L    # 14.56841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f746a1a500d5e8dL    # 0.004984

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f65f676ea422899L    # 0.002681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3edfa43fe5c92L    # 0.1557

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f96dacabc515486L    # 0.022319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc5ee78183f91e6L    # 0.17134

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e32a0663c74fbL    # 0.014745

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dac57e23f24eL    # 2.356822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fcfacf7446f9b99L    # 0.247466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faa5a89b951c5c5L    # 0.051472

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4001aa116659d12dL    # 2.208041

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f571e29b6b2af14L    # 0.001411

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2f31f46ed245b3L    # 2.38E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f26f0068db8bac7L    # 1.75E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd0117720c8cd64L    # 0.251066

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f593f6c2699c7bdL    # 0.001541

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9a3400b88ca3e8L    # 0.025589

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f84e6e221c8a7a4L    # 0.010206

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcf382e44b6e936L    # 0.243902

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f76fc9bc7714339L    # 0.005612

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034c5ce075f6fd2L    # 20.772675

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa0d130df9bdc6aL    # 0.032846

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fbcc111ada76d98L    # 0.112321

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401207f6f4be835eL    # 4.507778

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a79fec99f1ae3L    # 2.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff54842fa509396L    # 1.330142

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400402c98e53eb3aL    # 2.501361

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffe45ad538ac190L    # 1.892011

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40218b4bb5e0f7fdL    # 8.772062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400ba784a9478c87L    # 3.456796

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc59f23465625a7L    # 0.168919

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020c5352220bc38L    # 8.385171

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403d46c01e68a0d3L    # 29.276369

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fdd1a75cd0bb6edL    # 0.45474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff66fd438d1d8a5L    # 1.402302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc1eacc92146a1aL    # 0.139978

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40300482cf52b90aL    # 16.017621

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021cb55ac03ff69L    # 8.897138

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcacf8d716d2aa6L    # 0.209459

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f413404ea4a8c15L    # 5.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffddf1172ef0ae5L    # 1.86696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9350d2806af46bL    # 0.018863

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f626e978d4fdf3bL    # 0.00225

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7b4cc25072085bL    # 0.006665

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f99bcba30121683L    # 0.025134

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffb89cb252ce033L    # 1.721141

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40041ea897635e74L    # 2.51497

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ffc67d028a1dfb9L    # 1.775345

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa29aa1d755bccbL    # 0.036336

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5958969a0ad8a1L    # 0.001547

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fba70f3882278d1L    # 0.103286

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3faa19eb6390c910L    # 0.050979

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa28438088509c0L    # 0.036165

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbde43ed959a30aL    # 0.116764

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff90f01fb82c2bdL    # 1.566164

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40064bf338716095L    # 2.787085

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc46aca79357604L    # 0.159509

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb7ad6cb5350093L    # 0.09249

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6dd7ecbb7f9d6fL    # 0.003643

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40203f47b677f6b2L    # 8.123594

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff88a7829068987L    # 1.533806

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbdcc857f3061c8L    # 0.116402

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91b25f633ce63aL    # 0.017282

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401932a8c9b84556L    # 6.299472

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40332b1c970f7b9eL    # 19.168405

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405387cc20d5629eL    # 78.121834

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f26aceaaf35e311L    # 1.73E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd711cb039ef0f1L    # 0.360461

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404486bedfa43fe6L    # 41.0527

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f385058dde7a744L    # 3.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40176559b3d07c85L    # 5.848975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4044ac3e7d135116L    # 41.345657

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f613a4f8726d04eL    # 0.002103

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fe97d1dcd7060bbL    # 0.796523

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa78ad256798959L    # 0.045981

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa70f3882278d0dL    # 0.045038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc611a975afaf86L    # 0.172414

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fea47304039abf3L    # 0.82119

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7a93293d102bc7L    # 0.006488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa7cfc3f811f4f5L    # 0.046507

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032ef381d7dbf48L    # 18.93445

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032491600f34507L    # 18.285492

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40019912556d19dfL    # 2.199742

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9d5a9eb2074ea9L    # 0.028666

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd8457c0b135979L    # 0.379241

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc09f94855da273L    # 0.12987

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4aa21a719b4ddL    # 0.322884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fd24b09e98dcdb3L    # 0.28583

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f72ec6bce8533b1L    # 0.00462

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f86976bc1eff9f8L    # 0.011031

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f93daf8df7a4e7bL    # 0.01939

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fa3150dae3e6cL    # 7.90926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405bb8923e5b8562L    # 110.883926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7815a07b352a84L    # 0.00588

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc1bc23315d701eL    # 0.138554

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x4000169f4906034fL    # 2.011046

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e8c4c165907d9L    # 7.637009

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd3b78cc9a77e5fL    # 0.308078

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fcce736049ecb32L    # 0.225806

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403273af102363b2L    # 18.45189

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc27c200c0f01fcL    # 0.144413

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8327243137b070L    # 0.009352

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40012a98aa8650e7L    # 2.145799

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fad2d666a98244fL    # 0.056987

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc86bc621b7e0acL    # 0.190789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3fc8f32378ab0dL    # 4.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402d5ec93a711516L    # 14.685129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4051a146412cf0faL    # 70.519913

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8569f4906034f4L    # 0.010456

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc6355043e5321eL    # 0.173502

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f933b5393250b52L    # 0.018781

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc1e679463cfb33L    # 0.139846

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4012c7210be9424eL    # 4.694462

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbc0ad03d9a9542L    # 0.10954

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    invoke-virtual {p1, v7, v8}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021a285a921ccd9L    # 8.817426

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6abf3387160957L    # 0.003265

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6c432ca57a786cL    # 0.00345

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8fd9ba1b1960faL    # 0.015552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fca98eda22f6a51L    # 0.207792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff27eb5b2d4d402L    # 1.155935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc2d8ba40d90e24L    # 0.147239

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe3430d306a2b17L    # 0.601935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    invoke-virtual {p1, v6}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa079a2834d26faL    # 0.032178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f753586ca89fc6eL    # 0.005178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa196fa82e87d2cL    # 0.034355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb65fd8adab9f56L    # 0.0874

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc10b417ca2120eL    # 0.133156

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f56e37154003255L    # 0.001397

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd725072085b185L    # 0.361635

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f719dece5710881L    # 0.004301

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x4016f24a276b7ed4L    # 5.736611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa3456b441bb8c3L    # 0.037639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    invoke-virtual {p1, v7, v8}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa6e8af81626b2fL    # 0.044744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ffac1d29dc72L    # 0.15624

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31904b3c3e74b0L    # 2.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdd79f23465625aL    # 0.460568

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3ebc83a96d4c34L    # 4.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f673b85e80bed74L    # 0.002836

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f20624dd2f1a9fcL    # 1.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0eeed8904f6dfcL    # 5.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4030d5da059a73b4L    # 16.835358

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    invoke-virtual {p1, v6}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e33acd5b6805aL    # 0.014747

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
