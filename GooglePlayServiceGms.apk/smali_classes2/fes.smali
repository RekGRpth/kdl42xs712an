.class public final Lfes;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lfew;

.field public b:I

.field public c:J

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfes;->a:Lfew;

    iput v2, p0, Lfes;->b:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfes;->c:J

    iput-boolean v2, p0, Lfes;->d:Z

    iput-boolean v2, p0, Lfes;->e:Z

    iput-boolean v2, p0, Lfes;->f:Z

    iput-boolean v2, p0, Lfes;->g:Z

    iput-boolean v2, p0, Lfes;->h:Z

    const/4 v0, -0x1

    iput v0, p0, Lfes;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lfes;->a:Lfew;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lfes;->a:Lfew;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Lfes;->b:I

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Lfes;->b:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-wide v1, p0, Lfes;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Lfes;->c:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lfes;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lfes;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lfes;->e:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lfes;->e:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lfes;->g:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lfes;->g:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lfes;->f:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Lfes;->f:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lfes;->h:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-boolean v2, p0, Lfes;->h:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lfes;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lfes;->a:Lfew;

    if-nez v0, :cond_1

    new-instance v0, Lfew;

    invoke-direct {v0}, Lfew;-><init>()V

    iput-object v0, p0, Lfes;->a:Lfew;

    :cond_1
    iget-object v0, p0, Lfes;->a:Lfew;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfes;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lfes;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfes;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfes;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfes;->g:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfes;->f:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfes;->h:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-object v0, p0, Lfes;->a:Lfew;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lfes;->a:Lfew;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget v0, p0, Lfes;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lfes;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1
    iget-wide v0, p0, Lfes;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lfes;->c:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_2
    iget-boolean v0, p0, Lfes;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lfes;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lfes;->e:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lfes;->e:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_4
    iget-boolean v0, p0, Lfes;->g:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lfes;->g:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Lfes;->f:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lfes;->f:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Lfes;->h:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lfes;->h:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_7
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfes;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfes;

    iget-object v2, p0, Lfes;->a:Lfew;

    if-nez v2, :cond_3

    iget-object v2, p1, Lfes;->a:Lfew;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfes;->a:Lfew;

    iget-object v3, p1, Lfes;->a:Lfew;

    invoke-virtual {v2, v3}, Lfew;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lfes;->b:I

    iget v3, p1, Lfes;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lfes;->c:J

    iget-wide v4, p1, Lfes;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lfes;->d:Z

    iget-boolean v3, p1, Lfes;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lfes;->e:Z

    iget-boolean v3, p1, Lfes;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Lfes;->f:Z

    iget-boolean v3, p1, Lfes;->f:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-boolean v2, p0, Lfes;->g:Z

    iget-boolean v3, p1, Lfes;->g:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-boolean v2, p0, Lfes;->h:Z

    iget-boolean v3, p1, Lfes;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 8

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    iget-object v0, p0, Lfes;->a:Lfew;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lfes;->b:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lfes;->c:J

    iget-wide v5, p0, Lfes;->c:J

    const/16 v7, 0x20

    ushr-long/2addr v5, v7

    xor-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfes;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfes;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfes;->f:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lfes;->g:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lfes;->h:Z

    if-eqz v3, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lfes;->a:Lfew;

    invoke-virtual {v0}, Lfew;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
