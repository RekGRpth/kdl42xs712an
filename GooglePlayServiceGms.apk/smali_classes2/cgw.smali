.class public final Lcgw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcgw;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;)Z
    .locals 10

    const/4 v3, 0x1

    instance-of v0, p1, Lcom/google/android/gms/drive/events/ResourceEvent;

    const-string v1, "Only resource events supported"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    const/4 v1, 0x0

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/drive/events/ResourceEvent;

    invoke-interface {v0}, Lcom/google/android/gms/drive/events/ResourceEvent;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    iget-object v5, p0, Lcgw;->a:Ljava/util/Map;

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/drive/internal/OnEventResponse;

    check-cast p1, Lcom/google/android/gms/drive/events/ChangeEvent;

    invoke-direct {v7, p1}, Lcom/google/android/gms/drive/internal/OnEventResponse;-><init>(Lcom/google/android/gms/drive/events/ChangeEvent;)V

    move v2, v1

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgx;

    iget v8, v1, Lcgx;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v3, v8, :cond_0

    :try_start_1
    iget-object v1, v1, Lcgx;->b:Lcht;

    invoke-interface {v1, v7}, Lcht;->a(Lcom/google/android/gms/drive/internal/OnEventResponse;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v8, "CallbackStore"

    const-string v9, "Event callback error"

    invoke-static {v8, v1, v9}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_1
    :try_start_3
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcgw;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move v0, v2

    :goto_1
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
