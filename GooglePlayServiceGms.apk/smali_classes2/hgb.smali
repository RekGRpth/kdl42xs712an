.class public final Lhgb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/accounts/Account;

.field public e:Ljbg;

.field public f:Z

.field public g:Z

.field public h:[Ljava/lang/String;

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZ)V
    .locals 12

    const-wide/16 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, Lhgb;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZJ)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Ljbg;ZZ[Ljava/lang/String;ZZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p10, p0, Lhgb;->a:J

    iput-object p1, p0, Lhgb;->b:Ljava/lang/String;

    iput-object p2, p0, Lhgb;->c:Ljava/lang/String;

    iput-object p3, p0, Lhgb;->d:Landroid/accounts/Account;

    iput-object p4, p0, Lhgb;->e:Ljbg;

    iput-boolean p5, p0, Lhgb;->f:Z

    iput-boolean p6, p0, Lhgb;->g:Z

    iput-object p7, p0, Lhgb;->h:[Ljava/lang/String;

    iput-boolean p8, p0, Lhgb;->i:Z

    iput-boolean p9, p0, Lhgb;->j:Z

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v0, v3, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    check-cast p1, Lhgb;

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    iget-object v3, p1, Lhgb;->d:Landroid/accounts/Account;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhgb;->c:Ljava/lang/String;

    iget-object v3, p1, Lhgb;->c:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhgb;->b:Ljava/lang/String;

    iget-object v3, p1, Lhgb;->b:Ljava/lang/String;

    invoke-static {v0, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhgb;->e:Ljbg;

    iget-object v3, p1, Lhgb;->e:Ljbg;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Lizs;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lhgb;->f:Z

    iget-boolean v3, p1, Lhgb;->f:Z

    if-ne v0, v3, :cond_b

    iget-boolean v0, p0, Lhgb;->g:Z

    iget-boolean v3, p1, Lhgb;->g:Z

    if-ne v0, v3, :cond_b

    iget-object v4, p0, Lhgb;->h:[Ljava/lang/String;

    iget-object v5, p1, Lhgb;->h:[Ljava/lang/String;

    if-eq v4, v5, :cond_a

    if-nez v4, :cond_4

    move v3, v2

    :goto_1
    if-nez v5, :cond_5

    move v0, v2

    :goto_2
    if-nez v3, :cond_3

    if-eqz v0, :cond_a

    :cond_3
    if-eq v3, v0, :cond_6

    move v0, v2

    :goto_3
    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lhgb;->i:Z

    iget-boolean v3, p1, Lhgb;->i:Z

    if-ne v0, v3, :cond_b

    iget-boolean v0, p0, Lhgb;->j:Z

    iget-boolean v3, p1, Lhgb;->j:Z

    if-ne v0, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_4
    array-length v0, v4

    move v3, v0

    goto :goto_1

    :cond_5
    array-length v0, v5

    goto :goto_2

    :cond_6
    new-instance v6, Lbpa;

    invoke-direct {v6, v3}, Lbpa;-><init>(I)V

    array-length v3, v4

    move v0, v2

    :goto_4
    if-ge v0, v3, :cond_7

    aget-object v7, v4, v0

    invoke-virtual {v6, v7}, Lbpa;->a(Ljava/lang/Object;)Lbpb;

    move-result-object v7

    iget v8, v7, Lbpb;->a:I

    add-int/lit8 v8, v8, 0x1

    iput v8, v7, Lbpb;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    array-length v3, v5

    move v0, v2

    :goto_5
    if-ge v0, v3, :cond_8

    aget-object v4, v5, v0

    invoke-virtual {v6, v4}, Lbpa;->a(Ljava/lang/Object;)Lbpb;

    move-result-object v4

    iget v7, v4, Lbpb;->a:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v4, Lbpb;->a:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v0, v6, Lbpa;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpb;

    iget v0, v0, Lbpb;->a:I

    if-eqz v0, :cond_9

    move v0, v2

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_3

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgb;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhgb;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lhgb;->e:Ljbg;

    if-eqz v4, :cond_0

    invoke-static {v4}, Lizs;->a(Lizs;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhgb;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhgb;->g:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhgb;->h:[Ljava/lang/String;

    if-eqz v0, :cond_6

    array-length v4, v0

    if-lez v4, :cond_6

    :goto_5
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhgb;->i:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lhgb;->j:Z

    if-eqz v1, :cond_8

    :goto_7
    add-int/2addr v0, v2

    return v0

    :cond_1
    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhgb;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhgb;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    move v0, v3

    goto :goto_6

    :cond_8
    move v2, v3

    goto :goto_7
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lgtq;->a()Lgts;

    move-result-object v0

    iget-wide v2, p0, Lhgb;->a:J

    iget-object v4, v0, Lgts;->a:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lhgb;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v0

    iget-object v2, p0, Lhgb;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v2

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v0}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v2

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhgb;->d:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Lgts;->a(Ljava/lang/String;)Lgts;

    move-result-object v0

    iget-object v2, p0, Lhgb;->e:Ljbg;

    if-nez v2, :cond_2

    iget-object v2, v0, Lgts;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-boolean v2, p0, Lhgb;->f:Z

    invoke-virtual {v0, v2}, Lgts;->a(Z)Lgts;

    move-result-object v0

    iget-object v2, v0, Lgts;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v2, p0, Lhgb;->g:Z

    invoke-virtual {v0, v2}, Lgts;->a(Z)Lgts;

    move-result-object v0

    iget-object v2, p0, Lhgb;->h:[Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, v0, Lgts;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    iget-boolean v1, p0, Lhgb;->i:Z

    invoke-virtual {v0, v1}, Lgts;->a(Z)Lgts;

    move-result-object v0

    iget-boolean v1, p0, Lhgb;->j:Z

    invoke-virtual {v0, v1}, Lgts;->a(Z)Lgts;

    move-result-object v0

    invoke-virtual {v0}, Lgts;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    iget-object v3, v0, Lgts;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->c(Lizs;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lgts;->a:Ljava/util/ArrayList;

    const-string v3, "\u203d"

    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Lgtq;->a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method
