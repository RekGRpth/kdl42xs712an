.class public interface abstract Lbrc;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a()Lbsp;
.end method

.method public abstract a(Z)Lbsp;
.end method

.method public abstract a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;)Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
.end method

.method public abstract a(JLcom/google/android/gms/drive/DriveId;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;ILcht;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lbwk;)V
.end method

.method public abstract a(Ljava/lang/Runnable;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/events/DriveEvent;)Z
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;)Lcfp;
.end method

.method public abstract b()Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;ILcht;)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;ILjava/lang/String;)V
.end method

.method public abstract c()Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract c(Lcom/google/android/gms/drive/DriveId;)V
.end method

.method public abstract d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract d()Z
.end method

.method public abstract e()V
.end method
