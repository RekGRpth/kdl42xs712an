.class public final Lgtg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v3}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    const-class v0, Lioj;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    const-class v0, Lipv;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    const-class v0, Lioj;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    const-class v0, Lipv;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    const-class v0, Ljai;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Parcel;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljai;

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    return-object v3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/wallet/common/PaymentModel;

    return-object v0
.end method
