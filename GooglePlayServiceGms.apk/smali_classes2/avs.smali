.class public final Lavs;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lavs;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lavs;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lavs;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lavs;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lavs;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lavs;->b()I

    :cond_0
    iget v0, p0, Lavs;->g:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Lavs;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavs;->a:Z

    iput-object p1, p0, Lavs;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lavs;->a(Ljava/lang/String;)Lavs;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lavs;->b(Ljava/lang/String;)Lavs;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lavs;->c(Ljava/lang/String;)Lavs;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lavs;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lavs;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lavs;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lavs;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lavs;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lavs;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lavs;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lavs;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lavs;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lavs;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lavs;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lavs;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lavs;->g:I

    return v0
.end method

.method public final b(Ljava/lang/String;)Lavs;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavs;->c:Z

    iput-object p1, p0, Lavs;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lavs;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavs;->e:Z

    iput-object p1, p0, Lavs;->f:Ljava/lang/String;

    return-object p0
.end method
