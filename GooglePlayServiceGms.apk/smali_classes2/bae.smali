.class final Lbae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbac;


# direct methods
.method constructor <init>(Lbac;)V
    .locals 0

    iput-object p1, p0, Lbae;->a:Lbac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lbae;->a:Lbac;

    invoke-static {v2}, Lbac;->l(Lbac;)Lawu;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbae;->a:Lbac;

    invoke-static {v2}, Lbac;->l(Lbac;)Lawu;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lawu;->a(J)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lbae;->a:Lbac;

    invoke-static {v0}, Lbac;->m(Lbac;)Laye;

    move-result-object v0

    const-string v1, "disconnecting due to heartbeat timeout"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lbae;->a:Lbac;

    invoke-static {v0}, Lbac;->n(Lbac;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lbae;->a:Lbac;

    invoke-static {v2}, Lbac;->p(Lbac;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lbae;->a:Lbac;

    invoke-static {v3}, Lbac;->o(Lbac;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    iget-object v2, p0, Lbae;->a:Lbac;

    invoke-static {v2}, Lbac;->q(Lbac;)Laww;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lbae;->a:Lbac;

    invoke-static {v2}, Lbac;->q(Lbac;)Laww;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Laww;->a(J)V

    goto :goto_0
.end method
