.class public final Lcmh;
.super Lclt;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V
    .locals 6

    sget-object v5, Lcms;->a:Lcms;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcmh;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcms;)V

    return-void
.end method

.method private constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcms;)V
    .locals 6

    sget-object v1, Lcmr;->c:Lcmr;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lclt;-><init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcms;)V

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-void
.end method

.method private constructor <init>(Lcfc;Lorg/json/JSONObject;)V
    .locals 1

    sget-object v0, Lcmr;->c:Lcmr;

    invoke-direct {p0, v0, p1, p2}, Lclt;-><init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V

    const-string v0, "metadataDelta"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcjc;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    return-void
.end method

.method synthetic constructor <init>(Lcfc;Lorg/json/JSONObject;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcmh;-><init>(Lcfc;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcfz;Lcfp;Lbsp;)Lcml;
    .locals 6

    iget-object v0, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {p2, v0}, Lcjc;->a(Lcfp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v4

    invoke-virtual {p2}, Lcfp;->k()V

    invoke-virtual {p0, p1}, Lcmh;->b(Lcfz;)Lbsp;

    move-result-object v2

    new-instance v0, Lcmh;

    iget-object v1, v2, Lbsp;->a:Lcfc;

    iget-object v2, v2, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    sget-object v5, Lcms;->b:Lcms;

    invoke-direct/range {v0 .. v5}, Lcmh;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcms;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcoy;)V
    .locals 2

    invoke-virtual {p3}, Lcoy;->j()Lcll;

    move-result-object v0

    iget-object v1, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-interface {v0, p1, p2, v1}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    return-void
.end method

.method public final b()Lorg/json/JSONObject;
    .locals 3

    invoke-super {p0}, Lclt;->b()Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "metadataDelta"

    iget-object v2, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcjc;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcmh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcmh;

    invoke-virtual {p0, p1}, Lcmh;->a(Lclu;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lclt;->a:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/data/EntrySpec;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "MetadataOp [%s, metadataChangeSet=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcmh;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcmh;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
