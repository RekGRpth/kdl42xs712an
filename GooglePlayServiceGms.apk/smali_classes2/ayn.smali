.class final Layn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laxi;


# instance fields
.field final synthetic a:Layl;


# direct methods
.method constructor <init>(Layl;)V
    .locals 0

    iput-object p1, p0, Layn;->a:Layl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onAllDevicesOffline"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->e(Layl;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazf;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->c(Layl;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-eqz v0, :cond_0

    iput-boolean v5, v0, Laze;->b:Z

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v3, "device %s is in use; not removing route"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->f(Layl;)Lazg;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->f(Layl;)Lazg;

    move-result-object v0

    invoke-interface {v0, v2}, Lazg;->b(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->a(Layl;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onDeviceOnline :%s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->c(Layl;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-eqz v0, :cond_0

    iput-boolean v4, v0, Laze;->b:Z

    :cond_0
    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->d(Layl;)Lazj;

    move-result-object v0

    invoke-virtual {v0, p1}, Lazj;->b(Lcom/google/android/gms/cast/CastDevice;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "DeviceScanner.Listener#onDeviceOffline :%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->c(Layl;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laze;

    if-eqz v0, :cond_0

    iput-boolean v3, v0, Laze;->b:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0, p1}, Layl;->a(Layl;Lcom/google/android/gms/cast/CastDevice;)V

    iget-object v0, p0, Layn;->a:Layl;

    invoke-static {v0}, Layl;->a(Layl;)V

    goto :goto_0
.end method
