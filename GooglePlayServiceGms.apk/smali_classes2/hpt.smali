.class public final Lhpt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhqx;


# static fields
.field private static final a:Ljava/util/Set;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Landroid/os/Handler;

.field private final g:Lhqf;

.field private final h:Limb;

.field private i:[B

.field private j:[B

.field private volatile k:Z

.field private final l:Lilx;

.field private volatile m:Z

.field private n:Ljava/util/concurrent/CountDownLatch;

.field private o:Ljava/lang/Runnable;

.field private p:Lhqa;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lhpt;->a:Ljava/util/Set;

    const-string v1, ".lck"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lhpt;->a:Ljava/util/Set;

    const-string v1, "sessionId"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lhpt;->a:Ljava/util/Set;

    const-string v1, "uploadedSeq"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lhpt;->a:Ljava/util/Set;

    const-string v1, "sessionSummary"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BLhqf;Landroid/os/Looper;Limb;Lilx;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lhpt;->k:Z

    iput-boolean v1, p0, Lhpt;->m:Z

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lhpt;->n:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Lhpu;

    invoke-direct {v0, p0}, Lhpu;-><init>(Lhpt;)V

    iput-object v0, p0, Lhpt;->o:Ljava/lang/Runnable;

    if-eqz p6, :cond_0

    if-eqz p7, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "looper could not be null when listener is not null."

    invoke-static {v0, v3}, Lhsn;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_1

    if-eqz p5, :cond_5

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Need non empty key or keyPath."

    invoke-static {v0, v3}, Lhsn;->a(ZLjava/lang/Object;)V

    if-eqz p5, :cond_2

    array-length v0, p5

    const/16 v3, 0x20

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    const-string v0, "signingKey needs to be 32 byte long."

    invoke-static {v1, v0}, Lhsn;->a(ZLjava/lang/Object;)V

    invoke-static {p8}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhpt;->h:Limb;

    iput-object p1, p0, Lhpt;->b:Landroid/content/Context;

    iput-object p3, p0, Lhpt;->c:Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhpt;->d:Ljava/lang/String;

    :goto_2
    iput-object p4, p0, Lhpt;->i:[B

    iput-object p5, p0, Lhpt;->j:[B

    iput-boolean v2, p0, Lhpt;->e:Z

    iput-object p6, p0, Lhpt;->g:Lhqf;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lhpt;->f:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-object v0, p0, Lhpt;->l:Lilx;

    return-void

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    iput-object p2, p0, Lhpt;->d:Ljava/lang/String;

    goto :goto_2
.end method

.method static synthetic a(Lhpt;[Ljava/lang/String;)Lhpy;
    .locals 1

    invoke-direct {p0, p1}, Lhpt;->a([Ljava/lang/String;)Lhpy;

    move-result-object v0

    return-object v0
.end method

.method private a([Ljava/lang/String;)Lhpy;
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-static {}, Lhsn;->a()Ljava/util/ArrayList;

    move-result-object v5

    array-length v6, p1

    move v4, v3

    move-object v0, v2

    move-object v1, v2

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v7, p1, v4

    const-string v8, "sessionId"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-direct {p0, v7}, Lhpt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    move-object v1, v2

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const-string v8, "uploadedSeq"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-direct {p0, v7}, Lhpt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v8, "sessionSummary"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    if-nez v1, :cond_4

    invoke-static {}, Lhrp;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lhpt;->b(Ljava/lang/String;)Z

    :cond_4
    new-instance v2, Lhpy;

    invoke-direct {v2, p0, v5, v1}, Lhpy;-><init>(Lhpt;Ljava/util/List;Ljava/lang/String;)V

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    move v0, v3

    :goto_2
    if-ge v0, v4, :cond_6

    aget-object v3, v1, v0

    invoke-static {v3}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v5}, Lhpy;->a(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_0
    move-exception v5

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lhpt;->h:Limb;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid number, ignoring seqNum "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Limb;->a(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    return-object v2
.end method

.method static synthetic a(Lhpt;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhpt;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lhpt;->d:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lhpt;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method static synthetic a(Lhpt;Lhpy;)V
    .locals 13

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v12, 0x0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->h:Limb;

    const-string v1, "Uploading %d files, sessionId: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p1, Lhpy;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v12

    iget-object v4, p1, Lhpy;->b:Ljava/lang/String;

    aput-object v4, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lhqa;

    iget-object v1, p0, Lhpt;->g:Lhqf;

    invoke-direct {v0, p0, p1, v1, v12}, Lhqa;-><init>(Lhpt;Lhpy;Lhqf;B)V

    iput-object v0, p0, Lhpt;->p:Lhqa;

    new-instance v0, Lhrp;

    iget-object v1, p0, Lhpt;->b:Landroid/content/Context;

    sget-object v2, Lhrs;->b:Lhrs;

    iget-object v4, p0, Lhpt;->c:Ljava/lang/String;

    iget-object v7, p0, Lhpt;->p:Lhqa;

    iget-object v8, p1, Lhpy;->b:Ljava/lang/String;

    iget-object v10, p0, Lhpt;->l:Lilx;

    move-object v5, v3

    move-object v6, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v10}, Lhrp;-><init>(Landroid/content/Context;Lhrs;Lhsd;Ljava/lang/String;Ljava/lang/String;[BLhqq;Ljava/lang/String;Limb;Lilx;)V

    iget-object v1, p1, Lhpy;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-boolean v4, p0, Lhpt;->m:Z

    if-nez v4, :cond_2

    invoke-direct {p0, v0, p1, v1}, Lhpt;->a(Lhrp;Lhpy;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_2
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v2, p0, Lhpt;->h:Limb;

    iget-boolean v1, p0, Lhpt;->m:Z

    if-eqz v1, :cond_4

    const-string v1, "Upload stoped by client."

    :goto_0
    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lhpy;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lhpt;->p:Lhqa;

    iget-object v5, p1, Lhpy;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lhqa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v1, "Fatal error while uploading, will quit early."

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lhrp;->d()V

    :goto_2
    :try_start_0
    iget-object v0, p0, Lhpt;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    iget-boolean v0, p0, Lhpt;->e:Z

    if-nez v0, :cond_8

    move v0, v11

    :goto_3
    iget-boolean v1, p0, Lhpt;->e:Z

    if-eqz v1, :cond_6

    invoke-direct {p0, p1}, Lhpt;->a(Lhpy;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v11

    :cond_6
    :goto_4
    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lhpy;->c()Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v12

    :goto_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_a

    if-eqz v0, :cond_7

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    move v0, v12

    goto :goto_3

    :cond_9
    move v0, v12

    goto :goto_4

    :cond_a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_c

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lhpt;->h:Limb;

    const-string v1, "Writing uploaded seqNum."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_b
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lhpt;->d:Ljava/lang/String;

    const-string v5, "uploadedSeq"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-direct {v1, v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_c
    :goto_6
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " quitting."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_d
    return-void

    :catch_0
    move-exception v0

    move-object v1, v3

    :goto_7
    :try_start_4
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_e

    iget-object v2, p0, Lhpt;->h:Limb;

    const-string v4, "Failed write uploaded seqNum: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Limb;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_e
    if-eqz v1, :cond_c

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_6

    :catchall_0
    move-exception v0

    move-object v1, v3

    :goto_8
    if-eqz v1, :cond_f

    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_f
    :goto_9
    :try_start_7
    throw v0
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2

    :catch_2
    move-exception v0

    goto/16 :goto_2

    :catch_3
    move-exception v0

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_9

    :catchall_1
    move-exception v0

    goto :goto_8

    :catch_5
    move-exception v0

    goto :goto_7
.end method

.method static synthetic a(Lhpt;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lhpt;->g:Lhqf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->f:Landroid/os/Handler;

    new-instance v1, Lhpw;

    invoke-direct {v1, p0, p1}, Lhpw;-><init>(Lhpt;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lhpt;Ljava/io/File;)Z
    .locals 1

    invoke-direct {p0, p1}, Lhpt;->a(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lhpt;Z)Z
    .locals 0

    iput-boolean p1, p0, Lhpt;->k:Z

    return p1
.end method

.method private a(Lhpy;)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p1, Lhpy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lhpy;->a(Ljava/lang/String;)Lhsm;

    move-result-object v2

    const-string v3, "Summary should not be null after all complete."

    invoke-static {v2, v3}, Lhsn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, v2, Lhsm;->a:I

    if-ne v3, v5, :cond_1

    iget v3, v2, Lhsm;->b:I

    if-nez v3, :cond_1

    iget v3, v2, Lhsm;->c:I

    if-nez v3, :cond_1

    iget v3, v2, Lhsm;->f:I

    if-eqz v3, :cond_2

    :cond_1
    iget v2, v2, Lhsm;->d:I

    if-ne v2, v5, :cond_4

    :cond_2
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lhpt;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deleted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to delete file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    :try_start_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to delete file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Limb;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_4
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lhpt;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " won\'t be deleted either because failed to be read or some GLocRequest failed to be upload."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Limb;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0}, Lhpt;->c()Z

    move-result v0

    return v0
.end method

.method private a(Lhrp;Lhpy;Ljava/lang/String;)Z
    .locals 12

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpt;->h:Limb;

    const-string v4, "Processing file %s."

    new-array v5, v3, [Ljava/lang/Object;

    aput-object p3, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lhpt;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lhpt;->i:[B

    if-eqz v0, :cond_e

    iget-object v5, p0, Lhpt;->i:[B

    iget-object v6, p0, Lhpt;->h:Limb;

    new-instance v0, Lhpk;

    invoke-direct {v0, v5, v1, v6}, Lhpk;-><init>([B[BLimb;)V

    :goto_0
    iget-object v5, p0, Lhpt;->j:[B

    if-eqz v5, :cond_1

    iget-object v1, p0, Lhpt;->j:[B

    iget-object v5, p0, Lhpt;->h:Limb;

    invoke-static {v1, v5}, Lhpk;->a([BLimb;)Lhpk;

    move-result-object v1

    :cond_1
    new-instance v5, Lhpl;

    invoke-direct {v5, v4, v0, v1}, Lhpl;-><init>(Ljava/lang/String;Lhpk;Lhpk;)V

    sget-object v0, Lhpz;->c:Lhpz;

    :cond_2
    :goto_1
    :try_start_0
    invoke-virtual {v5}, Lhpl;->a()[B

    move-result-object v1

    if-eqz v1, :cond_4

    iget-boolean v4, p0, Lhpt;->m:Z

    if-eqz v4, :cond_6

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Stopped processing the rest of file "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lhrp;->c()V

    sget-object v0, Lhpz;->b:Lhpz;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lhpm; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    :try_start_1
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    invoke-virtual {p2, p3, v0}, Lhpy;->a(Ljava/lang/String;Lhpz;)V

    iget-object v0, p0, Lhpt;->p:Lhqa;

    iget-object v1, p2, Lhpy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Lhqa;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhpt;->h:Limb;

    const-string v1, "Finish reading file %s. Upload is still in process."

    new-array v4, v3, [Ljava/lang/Object;

    aput-object p3, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_5
    move v0, v3

    :goto_3
    return v0

    :cond_6
    :try_start_2
    new-instance v4, Livi;

    sget-object v6, Lihj;->Q:Livk;

    invoke-direct {v4, v6}, Livi;-><init>(Livk;)V

    invoke-virtual {v4, v1}, Livi;->b([B)Livi;

    invoke-virtual {v4}, Livi;->d()Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v0, Ljava/io/IOException;

    const-string v1, "isValid returned after parsing GLocRequest"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lhpm; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_3
    sget-object v0, Lhpz;->d:Lhpz;

    const-string v4, "File not found."

    sget-boolean v6, Licj;->e:Z

    if-eqz v6, :cond_7

    iget-object v6, p0, Lhpt;->h:Limb;

    invoke-virtual {v6, v4, v1}, Limb;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_7
    :try_start_4
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :cond_8
    const/4 v1, 0x6

    :try_start_5
    invoke-virtual {v4, v1}, Livi;->f(I)Livi;

    move-result-object v1

    invoke-static {v1}, Lhsn;->a(Livi;)Livi;

    move-result-object v1

    const/4 v6, 0x6

    invoke-virtual {v4, v6}, Livi;->j(I)V

    const/4 v6, 0x3

    invoke-virtual {v1, v6}, Livi;->c(I)I

    move-result v6

    invoke-virtual {p2, p3, v6}, Lhpy;->a(Ljava/lang/String;I)V

    invoke-virtual {p2, v6}, Lhpy;->b(I)Z

    move-result v7

    if-eqz v7, :cond_b

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lhpt;->h:Limb;

    const-string v4, "%d has been uploaded, skipping."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v4, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Limb;->a(Ljava/lang/String;)V

    :cond_9
    const/4 v1, 0x1

    invoke-virtual {p2, v6, v1}, Lhpy;->a(IZ)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lhpm; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_6
    sget-object v0, Lhpz;->d:Lhpz;

    const-string v4, "Error reading file."

    sget-boolean v6, Licj;->e:Z

    if-eqz v6, :cond_a

    iget-object v6, p0, Lhpt;->h:Limb;

    invoke-virtual {v6, v4, v1}, Limb;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_a
    :try_start_7
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v1

    goto/16 :goto_2

    :cond_b
    :try_start_8
    sget-boolean v7, Licj;->b:Z

    if-eqz v7, :cond_c

    iget-object v7, p0, Lhpt;->h:Limb;

    const-string v8, "Submitting #%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Limb;->a(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1, v4, v1}, Lhrp;->b(Livi;Livi;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v0, p0, Lhpt;->p:Lhqa;

    iget-object v1, p2, Lhpy;->b:Ljava/lang/String;

    const-string v4, "Fatal: can not submit task."

    invoke-virtual {v0, v1, v6, v4}, Lhqa;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lhpm; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :goto_4
    move v0, v2

    goto/16 :goto_3

    :catch_4
    move-exception v0

    move-object v1, v0

    :try_start_a
    sget-object v0, Lhpz;->e:Lhpz;

    const-string v4, "Invalid file format."

    sget-boolean v6, Licj;->e:Z

    if-eqz v6, :cond_d

    iget-object v6, p0, Lhpt;->h:Limb;

    invoke-virtual {v6, v4, v1}, Limb;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_d
    :try_start_b
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    :try_start_c
    invoke-virtual {v5}, Lhpl;->b()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :goto_5
    throw v0

    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v1

    goto/16 :goto_2

    :catch_8
    move-exception v1

    goto :goto_5

    :cond_e
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private a(Ljava/io/File;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0, v4}, Lhpt;->a(Ljava/io/File;)Z

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhpt;->h:Limb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Directory "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deleted."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_2
    const/4 v0, 0x1

    :cond_3
    :goto_2
    return v0

    :cond_4
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhpt;->h:Limb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to delete directory "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static a([Ljava/io/File;)Z
    .locals 5

    const/4 v0, 0x0

    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_1
    return v0

    :cond_1
    sget-object v4, Lhpt;->a:Ljava/util/Set;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    array-length v1, p0

    sget-object v2, Lhpt;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_1
    invoke-virtual {v2, v3}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_3

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v1

    :goto_2
    :try_start_2
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhpt;->h:Limb;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not read file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Limb;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_2
    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v0

    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_4

    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_4
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v2, v0

    goto :goto_2
.end method

.method static synthetic b(Lhpt;)Limb;
    .locals 1

    iget-object v0, p0, Lhpt;->h:Limb;

    return-object v0
.end method

.method static synthetic b(Lhpt;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lhpt;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v4, Ljava/io/File;

    iget-object v2, p0, Lhpt;->d:Ljava/lang/String;

    const-string v3, "sessionId"

    invoke-direct {v4, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    move-object v2, v3

    :goto_1
    :try_start_3
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhpt;->h:Limb;

    const-string v4, "Failed to save session id: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Limb;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_0
    if-eqz v2, :cond_1

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_1
    :goto_2
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_2

    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :goto_4
    throw v0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_1
.end method

.method private c()Z
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lhpt;->d:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error: Trying to delete non-directory"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {v2}, Lhpt;->a([Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lhpt;->a(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v2

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhpt;->h:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to delete directory "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lhpt;)Z
    .locals 1

    iget-boolean v0, p0, Lhpt;->e:Z

    return v0
.end method

.method static synthetic d(Lhpt;)Lhqf;
    .locals 1

    iget-object v0, p0, Lhpt;->g:Lhqf;

    return-object v0
.end method

.method static synthetic e(Lhpt;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lhpt;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lhpt;)Z
    .locals 1

    invoke-direct {p0}, Lhpt;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lhpt;)Z
    .locals 1

    iget-boolean v0, p0, Lhpt;->k:Z

    return v0
.end method

.method static synthetic h(Lhpt;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    iget-object v0, p0, Lhpt;->n:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lhpt;->o:Ljava/lang/Runnable;

    const-string v2, "BatchScanResultUploader.Thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhpt;->m:Z

    return-void
.end method
