.class public final Ledu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfaq;
.implements Lfax;


# instance fields
.field private a:Lcom/google/android/gms/games/Player;

.field private b:Ljava/util/ArrayList;

.field private c:Z

.field private d:[Ljava/lang/String;

.field private e:Z

.field private f:Ljava/util/ArrayList;

.field private g:Z

.field private final h:Landroid/support/v4/app/Fragment;

.field private final i:Landroid/app/Activity;

.field private final j:Ljava/lang/String;

.field private final k:Lfaj;

.field private final l:I

.field private final m:Ledv;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/Player;Ledv;Landroid/app/Activity;Lfaj;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ledu;->a:Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Ledu;->m:Ledv;

    iput-object v0, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    iput-object p3, p0, Ledu;->i:Landroid/app/Activity;

    iput-object p4, p0, Ledu;->k:Lfaj;

    iput-object p5, p0, Ledu;->j:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Ledu;->l:I

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/Player;Ledv;Landroid/support/v4/app/Fragment;Lfaj;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ledu;->a:Lcom/google/android/gms/games/Player;

    iput-object p2, p0, Ledu;->m:Ledv;

    iput-object p3, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    const/4 v0, 0x0

    iput-object v0, p0, Ledu;->i:Landroid/app/Activity;

    iput-object p4, p0, Ledu;->k:Lfaj;

    iput-object p5, p0, Ledu;->j:Ljava/lang/String;

    iput p6, p0, Ledu;->l:I

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/Player;Ledv;Landroid/support/v4/app/Fragment;Lfaj;Ljava/lang/String;I)Ledu;
    .locals 7

    new-instance v0, Ledu;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Ledu;-><init>(Lcom/google/android/gms/games/Player;Ledv;Landroid/support/v4/app/Fragment;Lfaj;Ljava/lang/String;I)V

    invoke-virtual {v0}, Ledu;->a()V

    return-object v0
.end method

.method private c()V
    .locals 8

    const/4 v1, 0x0

    iget-boolean v0, p0, Ledu;->g:Z

    if-eqz v0, :cond_1

    const-string v0, "ManageCirclesHelper"

    const-string v1, "computeBelongingCircles: Canceled! Bailing out..."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Ledu;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ledu;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ledu;->f:Ljava/util/ArrayList;

    iget-object v0, p0, Ledu;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ledu;->d:[Ljava/lang/String;

    array-length v4, v0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_4

    iget-object v0, p0, Ledu;->d:[Ljava/lang/String;

    aget-object v5, v0, v3

    iget-object v0, p0, Ledu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_3

    iget-object v0, p0, Ledu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Ledu;->f:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Ledu;->g:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->T_()Lo;

    move-result-object v0

    iget-object v1, p0, Ledu;->j:Ljava/lang/String;

    iget-object v2, p0, Ledu;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ledu;->f:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Leee;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    iget v2, p0, Ledu;->l:I

    invoke-static {v1, v0, v2}, Leee;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Ledu;->i:Landroid/app/Activity;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ledu;->i:Landroid/app/Activity;

    iget-object v1, p0, Ledu;->j:Ljava/lang/String;

    iget-object v2, p0, Ledu;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ledu;->f:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Leee;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ledu;->i:Landroid/app/Activity;

    iget v2, p0, Ledu;->l:I

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mFragment and mActivity cannot both be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledu;->h:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->T_()Lo;

    move-result-object v0

    :goto_0
    sget v1, Lxf;->aL:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    iget-object v0, p0, Ledu;->i:Landroid/app/Activity;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-boolean v0, p0, Ledu;->g:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Restarting a previously canceled ManageCirclesHelper instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v3, p0, Ledu;->b:Ljava/util/ArrayList;

    iput-boolean v6, p0, Ledu;->c:Z

    iput-object v3, p0, Ledu;->d:[Ljava/lang/String;

    iput-boolean v6, p0, Ledu;->e:Z

    iput-object v3, p0, Ledu;->f:Ljava/util/ArrayList;

    iput-boolean v6, p0, Ledu;->g:Z

    iget-object v0, p0, Ledu;->k:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ManageCirclesHelper"

    const-string v1, "ManageCirclesHelper.start: PeopleClient not connected!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ledu;->k:Lfaj;

    iget-object v2, p0, Ledu;->j:Ljava/lang/String;

    const/4 v4, -0x1

    move-object v1, p0

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lfaj;->a(Lfaq;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    new-instance v0, Lfal;

    invoke-direct {v0}, Lfal;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Ledu;->a:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v1}, Lfal;->a(Ljava/util/Collection;)Lfal;

    iget-object v1, p0, Ledu;->k:Lfaj;

    iget-object v2, p0, Ledu;->j:Ljava/lang/String;

    invoke-virtual {v1, p0, v2, v3, v0}, Lfaj;->a(Lfax;Ljava/lang/String;Ljava/lang/String;Lfal;)V

    goto :goto_0
.end method

.method public final a(Lbbo;Lfeb;)V
    .locals 3

    iget-boolean v0, p0, Ledu;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onCirclesLoaded: Canceled! Ignoring this callback..."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ManageCirclesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCirclesLoaded: error status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ledu;->d()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ledu;->m:Ledv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ledu;->m:Ledv;

    invoke-interface {v0}, Ledv;->aa()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onCirclesLoaded: processing halted at client\'s request"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ledu;->d()V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Ledu;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ledu;->b:Ljava/util/ArrayList;

    :goto_1
    invoke-virtual {p2}, Lfeb;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfea;

    invoke-interface {v0}, Lfea;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lfea;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    iget-object v2, p0, Ledu;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lfeb;->b()V

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Ledu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lfeb;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ledu;->c:Z

    invoke-direct {p0}, Ledu;->c()V

    goto :goto_0
.end method

.method public final a(Lbbo;Lfef;)V
    .locals 3

    iget-boolean v0, p0, Ledu;->g:Z

    if-eqz v0, :cond_0

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onPeopleLoaded: Canceled! Ignoring this callback..."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ManageCirclesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPeopleLoaded: error status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ledu;->d()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ledu;->m:Ledv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ledu;->m:Ledv;

    invoke-interface {v0}, Ledv;->aa()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ManageCirclesHelper"

    const-string v1, "onCirclesLoaded: processing halted at client\'s request"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ledu;->d()V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {p2}, Lfef;->a()I

    move-result v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lfef;->b(I)Lfee;

    move-result-object v0

    invoke-interface {v0}, Lfee;->d()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ledu;->d:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {p2}, Lfef;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ledu;->e:Z

    invoke-direct {p0}, Ledu;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lfef;->b()V

    throw v0
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Ledu;->g:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Canceling a previously canceled ManageCirclesHelper instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ledu;->g:Z

    return-void
.end method
