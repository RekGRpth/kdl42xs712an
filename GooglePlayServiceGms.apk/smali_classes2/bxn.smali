.class public final Lbxn;
.super Lk;
.source "SourceFile"


# instance fields
.field final synthetic Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V
    .locals 0

    iput-object p1, p0, Lbxn;->Y:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-direct {p0}, Lk;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lbxn;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0070    # com.google.android.gms.R.string.drive_storage_management_confirm_unpin_all_items_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0071    # com.google.android.gms.R.string.drive_storage_management_confirm_unpin_all_items_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b006d    # com.google.android.gms.R.string.drive_storage_management_dialog_positive

    new-instance v3, Lbxp;

    invoke-direct {v3, p0}, Lbxp;-><init>(Lbxn;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0073    # com.google.android.gms.R.string.drive_storage_management_cancel

    new-instance v3, Lbxo;

    invoke-direct {v3, p0}, Lbxo;-><init>(Lbxn;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
