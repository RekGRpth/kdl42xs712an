.class final Lhje;
.super Lhsi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhjd;


# direct methods
.method private constructor <init>(Lhjd;)V
    .locals 0

    iput-object p1, p0, Lhje;->a:Lhjd;

    invoke-direct {p0}, Lhsi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhjd;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhje;-><init>(Lhjd;)V

    return-void
.end method

.method private a(Livi;I)Lhue;
    .locals 18

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Livi;->k(I)I

    move-result v6

    if-ge v1, v6, :cond_6

    const/4 v6, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v1}, Livi;->c(II)Livi;

    move-result-object v6

    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Livi;->i(I)Z

    move-result v7

    if-eqz v7, :cond_5

    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Livi;->f(I)Livi;

    move-result-object v11

    const/4 v7, 0x1

    const/4 v6, 0x0

    move/from16 v16, v6

    move/from16 v17, v7

    move v7, v5

    move-wide v5, v3

    move v4, v2

    move/from16 v3, v17

    move/from16 v2, v16

    :goto_1
    move/from16 v0, p2

    invoke-virtual {v11, v0}, Livi;->k(I)I

    move-result v8

    if-ge v2, v8, :cond_4

    move/from16 v0, p2

    invoke-virtual {v11, v0, v2}, Livi;->c(II)Livi;

    move-result-object v9

    add-int/lit8 v7, v7, 0x1

    int-to-long v12, v7

    const-wide/16 v14, 0xa

    cmp-long v8, v12, v14

    if-lez v8, :cond_0

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/16 v8, 0x1388

    if-ge v4, v8, :cond_0

    const/4 v8, 0x7

    invoke-virtual {v9, v8}, Livi;->i(I)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x7

    invoke-virtual {v9, v8}, Livi;->c(I)I

    move-result v8

    :goto_3
    const/16 v12, 0x8

    invoke-virtual {v9, v12}, Livi;->i(I)Z

    move-result v12

    if-eqz v12, :cond_3

    const/16 v12, 0x8

    invoke-virtual {v9, v12}, Livi;->c(I)I

    move-result v9

    :goto_4
    int-to-long v12, v8

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    int-to-long v8, v9

    add-long/2addr v8, v12

    add-long/2addr v5, v8

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x0

    goto :goto_3

    :cond_3
    const/4 v9, 0x0

    goto :goto_4

    :cond_4
    move v2, v4

    move-wide v3, v5

    move v5, v7

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    const/16 v1, 0x64

    if-ge v2, v1, :cond_8

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lhje;->a:Lhjd;

    iget-object v1, v1, Lhjd;->a:Ljava/lang/String;

    const-string v2, "Not enough data to properly estimate the sensor rate."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x0

    :goto_5
    return-object v1

    :cond_8
    long-to-double v3, v3

    int-to-double v1, v2

    div-double v1, v3, v1

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lhje;->a:Lhjd;

    iget-object v3, v3, Lhjd;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TimeBetweenEvents for sensor:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v10, v1}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v1

    goto :goto_5
.end method

.method private a(Z)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v1, p0, Lhje;->a:Lhjd;

    invoke-static {v1}, Lhjd;->a(Lhjd;)J

    move-result-wide v1

    iput-wide v1, v0, Lhjd;->i:J

    :cond_0
    iget-object v0, p0, Lhje;->a:Lhjd;

    sget-object v1, Lhir;->b:Lhir;

    iput-object v1, v0, Lhjd;->f:Lhir;

    iget-object v0, p0, Lhje;->a:Lhjd;

    invoke-static {v0}, Lhjd;->b(Lhjd;)V

    return-void
.end method


# virtual methods
.method public final a(Livi;)V
    .locals 14

    const/4 v13, 0x4

    const/4 v12, 0x2

    const-wide v10, 0x3ef4f8b588e368f1L    # 2.0E-5

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, p1, v13}, Lhje;->a(Livi;I)Lhue;

    move-result-object v5

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lhje;->a(Livi;I)Lhue;

    move-result-object v1

    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, v4}, Lhje;->a(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, v5, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iget-object v0, v1, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget v0, v0, Lhjd;->m:I

    const-wide v1, 0x415b3f7249249249L    # 7142857.142857143

    cmpg-double v1, v6, v1

    if-gez v1, :cond_2

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget v0, v0, Lhjd;->m:I

    invoke-static {v0}, Lhll;->a(I)I

    move-result v0

    :cond_2
    iget-object v1, p0, Lhje;->a:Lhjd;

    iget v1, v1, Lhjd;->n:I

    const-wide v6, 0x416d5804e0000000L    # 1.5384615E7

    cmpg-double v2, v8, v6

    if-gez v2, :cond_3

    iget-object v1, p0, Lhje;->a:Lhjd;

    iget v1, v1, Lhjd;->n:I

    invoke-static {v1}, Lhll;->a(I)I

    move-result v1

    :cond_3
    iget-object v2, p0, Lhje;->a:Lhjd;

    iget v2, v2, Lhjd;->m:I

    if-ne v0, v2, :cond_4

    iget-object v2, p0, Lhje;->a:Lhjd;

    iget v2, v2, Lhjd;->n:I

    if-eq v1, v2, :cond_8

    :cond_4
    move v2, v4

    :goto_1
    iget-object v6, p0, Lhje;->a:Lhjd;

    iget-object v6, v6, Lhjd;->g:Lhjf;

    invoke-virtual {v6}, Lhjf;->f()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lhje;->a:Lhjd;

    iget-object v6, v6, Lhjd;->g:Lhjf;

    invoke-virtual {v6}, Lhjf;->g()Z

    move-result v6

    if-eqz v6, :cond_5

    if-eqz v2, :cond_6

    :cond_5
    iget-object v6, p0, Lhje;->a:Lhjd;

    iget-object v6, v6, Lhjd;->g:Lhjf;

    iget-object v7, v6, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_0
    iget-object v6, v6, Lhjf;->c:Livi;

    const/4 v8, 0x4

    invoke-virtual {v6, v8, v0}, Livi;->e(II)Livi;

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->g:Lhjf;

    iget-object v6, v0, Lhjf;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    iget-object v0, v0, Lhjf;->c:Livi;

    const/4 v7, 0x5

    invoke-virtual {v0, v7, v1}, Livi;->e(II)Livi;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->g:Lhjf;

    invoke-virtual {v0}, Lhjf;->a()V

    :cond_6
    if-eqz v2, :cond_9

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->a:Ljava/lang/String;

    const-string v1, "Sensor recording too fast. Will lower the rate and try again."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v0, v4

    :goto_2
    if-eqz v0, :cond_a

    invoke-direct {p0, v3}, Lhje;->a(Z)V

    goto/16 :goto_0

    :cond_8
    move v2, v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_9
    move v0, v3

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->b()J

    move-result-wide v1

    iget-object v0, v5, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lhjd;->a(Ljava/util/List;)[[D

    move-result-object v0

    if-eqz v0, :cond_f

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_b

    const-string v5, "CalibrationCollector"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Gyro stats: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    aget-object v5, v0, v4

    aget-wide v5, v5, v3

    cmpg-double v5, v5, v10

    if-gez v5, :cond_e

    aget-object v5, v0, v4

    aget-wide v5, v5, v4

    cmpg-double v5, v5, v10

    if-gez v5, :cond_e

    aget-object v5, v0, v4

    aget-wide v5, v5, v12

    cmpg-double v5, v5, v10

    if-gez v5, :cond_e

    sget-boolean v5, Licj;->b:Z

    if-eqz v5, :cond_c

    const-string v5, "CalibrationCollector"

    const-string v6, "Gyro variance stable"

    invoke-static {v5, v6}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    new-instance v5, Livi;

    sget-object v6, Lihj;->w:Livk;

    invoke-direct {v5, v6}, Livi;-><init>(Livk;)V

    aget-object v6, v0, v3

    aget-wide v6, v6, v3

    double-to-float v6, v6

    invoke-virtual {v5, v4, v6}, Livi;->a(IF)Livi;

    aget-object v6, v0, v3

    aget-wide v6, v6, v4

    double-to-float v6, v6

    invoke-virtual {v5, v12, v6}, Livi;->a(IF)Livi;

    const/4 v6, 0x3

    aget-object v0, v0, v3

    aget-wide v7, v0, v12

    double-to-float v0, v7

    invoke-virtual {v5, v6, v0}, Livi;->a(IF)Livi;

    invoke-virtual {v5, v13, v1, v2}, Livi;->a(IJ)Livi;

    new-instance v0, Livi;

    sget-object v1, Lihj;->v:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    invoke-virtual {v0, v4, v5}, Livi;->b(ILivi;)Livi;

    :goto_3
    if-eqz v0, :cond_12

    :try_start_2
    iget-object v1, p0, Lhje;->a:Lhjd;

    iget-object v1, v1, Lhjd;->b:Lidu;

    invoke-interface {v1}, Lidu;->s()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_10

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_10

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create sensorCacheDir: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_d

    iget-object v1, p0, Lhje;->a:Lhjd;

    iget-object v1, v1, Lhjd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_d
    :goto_4
    invoke-direct {p0, v4}, Lhje;->a(Z)V

    goto/16 :goto_0

    :cond_e
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_f

    const-string v0, "CalibrationCollector"

    const-string v1, "Gyro variance too high"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const/4 v0, 0x0

    goto :goto_3

    :cond_10
    :try_start_3
    new-instance v2, Ljava/io/File;

    const-string v3, "calibration"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Livi;->f()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    iget-object v1, p0, Lhje;->a:Lhjd;

    iput-object v0, v1, Lhjd;->l:Livi;

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lhje;->a:Lhjd;

    iget-object v1, v1, Lhjd;->a:Ljava/lang/String;

    const-string v2, "Calibration successfully written to disk"

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    iget-object v1, p0, Lhje;->a:Lhjd;

    iget-object v2, p0, Lhje;->a:Lhjd;

    invoke-static {v2, v0}, Lhjd;->a(Lhjd;Livi;)J

    move-result-wide v2

    iput-wide v2, v1, Lhjd;->h:J
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    :cond_12
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->a:Ljava/lang/String;

    const-string v1, "Calibration failed due to phone not stationary"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    sget-boolean v0, Licj;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lilz;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhje;->a:Lhjd;

    iget-object v0, v0, Lhjd;->k:Lhqy;

    invoke-interface {v0}, Lhqy;->b()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhje;->a(Z)V

    return-void
.end method
