.class public final Liwe;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Liwe;


# instance fields
.field public final b:Liwf;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:F

.field public final g:D

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Liwe;

    sget-object v1, Liwf;->e:Liwf;

    invoke-direct {v0, v1}, Liwe;-><init>(Liwf;)V

    sput-object v0, Liwe;->a:Liwe;

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;IFDLiwf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Liwe;->c:I

    iput p2, p0, Liwe;->d:I

    iput p3, p0, Liwe;->e:I

    iput-object p4, p0, Liwe;->h:Ljava/lang/String;

    iput-object p5, p0, Liwe;->i:Ljava/lang/String;

    iput p6, p0, Liwe;->j:I

    iput p7, p0, Liwe;->f:F

    iput-wide p8, p0, Liwe;->g:D

    if-nez p10, :cond_0

    sget-object p10, Liwf;->e:Liwf;

    :cond_0
    iput-object p10, p0, Liwe;->b:Liwf;

    return-void
.end method

.method private constructor <init>(Liwf;)V
    .locals 11

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object v0, p0

    move v2, v1

    move v3, v1

    move-object v5, v4

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Liwe;-><init>(IIILjava/lang/String;Ljava/lang/String;IFDLiwf;)V

    return-void
.end method


# virtual methods
.method public final a(Liwe;)D
    .locals 12

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    iget v0, p0, Liwe;->c:I

    int-to-double v0, v0

    iget v2, p0, Liwe;->d:I

    int-to-double v2, v2

    iget v4, p1, Liwe;->c:I

    int-to-double v4, v4

    iget v6, p1, Liwe;->d:I

    int-to-double v6, v6

    invoke-static {v0, v1}, Liwd;->a(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Liwd;->a(D)D

    move-result-wide v2

    invoke-static {v4, v5}, Liwd;->a(D)D

    move-result-wide v4

    invoke-static {v6, v7}, Liwd;->a(D)D

    move-result-wide v6

    sub-double v8, v4, v0

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    sub-double v2, v6, v2

    mul-double/2addr v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double v6, v8, v8

    mul-double/2addr v2, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    add-double/2addr v0, v6

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/16 v6, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double v0, v8, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    mul-double/2addr v0, v2

    const-wide v2, 0x415849c600000000L    # 6367000.0

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Liwe;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Liwe;

    iget v1, p0, Liwe;->c:I

    iget v2, p1, Liwe;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Liwe;->d:I

    iget v2, p1, Liwe;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Liwe;->e:I

    iget v2, p1, Liwe;->e:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Liwe;->h:Ljava/lang/String;

    iget-object v2, p1, Liwe;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lirf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Liwe;->i:Ljava/lang/String;

    iget-object v2, p1, Liwe;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lirf;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Liwe;->j:I

    iget v2, p1, Liwe;->j:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Liwe;->f:F

    iget v2, p1, Liwe;->f:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-wide v1, p0, Liwe;->g:D

    iget-wide v3, p1, Liwe;->g:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Liwe;->b:Liwf;

    iget-object v2, p1, Liwe;->b:Liwf;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Liwe;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Liwe;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Liwe;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Liwe;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Liwe;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Liwe;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Liwe;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Liwe;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Liwe;->b:Liwf;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Position [latE7="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Liwe;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lngE7="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Liwe;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accuracyMm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Liwe;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clusterId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Liwe;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", levelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Liwe;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bearing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Liwe;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", speedMps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Liwe;->f:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", altitudeMeters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Liwe;->g:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Liwe;->b:Liwf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
