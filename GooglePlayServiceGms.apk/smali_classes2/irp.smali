.class final Lirp;
.super Lirv;
.source "SourceFile"


# static fields
.field static final a:Lirp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lirp;

    invoke-direct {v0}, Lirp;-><init>()V

    sput-object v0, Lirp;->a:Lirp;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lirv;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lirx;
    .locals 1

    invoke-static {}, Lirx;->c()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lirx;
    .locals 1

    invoke-static {}, Lirx;->c()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lirs;
    .locals 1

    sget-object v0, Lirs;->b:Lirs;

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lirx;->c()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lirx;->c()Lirx;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "{}"

    return-object v0
.end method

.method public final bridge synthetic values()Ljava/util/Collection;
    .locals 1

    sget-object v0, Lirs;->b:Lirs;

    return-object v0
.end method
