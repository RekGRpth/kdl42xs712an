.class public final Lels;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lelu;

.field public final c:Ljava/util/Set;

.field public final d:Lema;

.field e:Z

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field public h:Z

.field public i:Ljava/lang/String;

.field public final j:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/String;Lelu;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lema;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lema;-><init>(Ljava/lang/Object;IJ)V

    iput-object v0, p0, Lels;->d:Lema;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lels;->j:Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lels;->a:Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelu;

    iput-object v0, p0, Lels;->b:Lelu;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lels;->c:Ljava/util/Set;

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lejk;->f(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lels;->e:Z

    iget-object v1, p0, Lels;->d:Lema;

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lejk;->g(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v2

    iget-object v3, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lejk;->h(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lejk;->i(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lema;->a(Ljava/lang/Object;IJ)V

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lejk;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lels;->f:Ljava/lang/String;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lejk;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lels;->g:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lejk;->j(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lels;->h:Z

    return-void
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;)Landroid/content/Intent;
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.icing.GlobalSearchAppRegistered2"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->writeToParcel(Landroid/os/Parcel;I)V

    const-string v2, "App"

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method private k()V
    .locals 8

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v1, v0, Lelu;->a:Lejk;

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v2, p0, Lels;->d:Lema;

    invoke-virtual {v2}, Lema;->b()I

    move-result v2

    iget-object v3, p0, Lels;->d:Lema;

    invoke-virtual {v3}, Lema;->c()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lejk;->e:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-virtual {v1, v5}, Lejk;->c(Ljava/lang/String;)Leij;

    move-result-object v7

    invoke-static {v0}, Lell;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Lehs;

    move-result-object v0

    iput-object v0, v7, Leij;->c:Lehs;

    iput v2, v7, Leij;->d:I

    iput-wide v3, v7, Leij;->g:J

    invoke-virtual {v1, v5, v7}, Lejk;->a(Ljava/lang/String;Leij;)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_0
    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    iget-object v2, v0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    invoke-virtual {v0, v1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v4, 0x0

    iput-object v4, v3, Leij;->c:Lehs;

    const/4 v4, 0x0

    iput v4, v3, Leij;->d:I

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Leij;->g:J

    invoke-virtual {v0, v1, v3}, Lejk;->a(Ljava/lang/String;Leij;)V

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private l()Z
    .locals 2

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private m()Ljava/lang/String;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "install-time-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "last-update-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 3

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lels;->c:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lema;)V
    .locals 6

    invoke-virtual {p1}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "setGlobalSearchInfo"

    invoke-static {v0}, Lehe;->b(Ljava/lang/String;)I

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0, p1}, Lema;->a(Lema;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lemb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GlobalSearchApplicationInfo: cannot "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lema;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " when previously "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lels;->d:Lema;

    invoke-virtual {v3}, Lema;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lemb;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lema;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "setGlobalSearchInfo info changed for %s"

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0, p1}, Lema;->b(Lema;)V

    invoke-direct {p0}, Lels;->k()V

    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {v0}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v4, p0, Lels;->b:Lelu;

    iget-object v4, v4, Lelu;->d:Lelx;

    invoke-interface {v4, p0}, Lelx;->b(Lels;)Ljava/util/Map;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/Map;)V

    iget-object v0, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.gms.icing.GlobalSearchAppRegistered"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "AppInfo"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Lels;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v0, p0, Lels;->b:Lelu;

    invoke-virtual {v0}, Lelu;->a()Z

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "gsaSigned= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lehe;->b(Ljava/lang/String;)I

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const-string v3, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending intent: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lehe;->b(Ljava/lang/String;)I

    iget-object v3, p0, Lels;->b:Lelu;

    iget-object v3, v3, Lelu;->b:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.icing.GlobalSearchableAppUnRegistered"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "AppPackageName"

    iget-object v4, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v0, "Not sending global search app notification. Search app not installed or not signed properly."

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    :cond_5
    :goto_3
    monitor-exit v1

    return-void

    :cond_6
    const-string v0, "setGlobalSearchInfo info unchanged for %s; updating timestamp to %d"

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lema;->c()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lels;->d:Lema;

    invoke-virtual {p1}, Lema;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lema;->a(J)V

    invoke-direct {p0}, Lels;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method public final a(Z)V
    .locals 6

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lels;->e:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lels;->e:Z

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    iget-boolean v3, p0, Lels;->e:Z

    iget-object v4, v0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v0, v2}, Lejk;->c(Ljava/lang/String;)Leij;

    move-result-object v5

    iput-boolean v3, v5, Leij;->b:Z

    invoke-virtual {v0, v2, v5}, Lejk;->a(Ljava/lang/String;Leij;)V

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lels;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()I
    .locals 2

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lels;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lels;->i:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()Z
    .locals 2

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lels;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Z
    .locals 2

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lels;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lels;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Z
    .locals 3

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lels;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lels;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()I
    .locals 6

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lels;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lels;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0}, Lels;->l()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-object v0, p0, Lels;->f:Ljava/lang/String;

    iget-object v2, p0, Lels;->b:Lelu;

    iget-object v2, v2, Lelu;->a:Lejk;

    iget-object v3, p0, Lels;->a:Ljava/lang/String;

    iget-object v4, v2, Lejk;->e:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v2, v3}, Lejk;->c(Ljava/lang/String;)Leij;

    move-result-object v5

    if-nez v0, :cond_2

    const-string v0, ""

    iput-object v0, v5, Leij;->a:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v3, v5}, Lejk;->a(Ljava/lang/String;Leij;)V

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :cond_2
    :try_start_2
    iput-object v0, v5, Leij;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    const/4 v0, 0x1

    :try_start_4
    monitor-exit v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lels;->f:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method public final g()V
    .locals 6

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "Unregistering package %s"

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lels;->f:Ljava/lang/String;

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    iget-object v3, v0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v0, v2}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v5, ""

    iput-object v5, v4, Leij;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Lejk;->a(Ljava/lang/String;Leij;)V

    :cond_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lels;->h:Z

    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->a:Lejk;

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lejk;->a(Ljava/lang/String;Z)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()Landroid/content/res/Resources;
    .locals 3

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lels;->b:Lelu;

    iget-object v0, v0, Lelu;->c:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get resources for client "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final i()V
    .locals 6

    invoke-direct {p0}, Lels;->n()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object v0, p0, Lels;->g:Ljava/lang/String;

    iget-object v2, p0, Lels;->b:Lelu;

    iget-object v2, v2, Lelu;->a:Lejk;

    iget-object v3, p0, Lels;->a:Ljava/lang/String;

    iget-object v4, v2, Lejk;->e:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v2, v3}, Lejk;->c(Ljava/lang/String;)Leij;

    move-result-object v5

    if-nez v0, :cond_0

    const-string v0, ""

    iput-object v0, v5, Leij;->e:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2, v3, v5}, Lejk;->a(Ljava/lang/String;Leij;)V

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :cond_0
    :try_start_3
    iput-object v0, v5, Leij;->e:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v4

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()Z
    .locals 3

    iget-object v1, p0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lels;->n()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lels;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lels;->g:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
