.class final enum Lcfq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcfq;

.field public static final enum b:Lcfq;

.field public static final enum c:Lcfq;

.field public static final enum d:Lcfq;

.field public static final enum e:Lcfq;

.field public static final enum f:Lcfq;

.field public static final enum g:Lcfq;

.field private static final k:Ljava/util/Map;

.field private static final synthetic l:[Lcfq;


# instance fields
.field private final h:I

.field private final i:I

.field private final j:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcfq;

    const-string v1, "ZIP"

    const v3, 0x7f02012a    # com.google.android.gms.R.drawable.ic_drive_type_zip_big

    const v4, 0x7f0b003e    # com.google.android.gms.R.string.drive_document_type_zip_archive

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "application/x-compress"

    aput-object v6, v5, v2

    const-string v6, "application/x-compressed"

    aput-object v6, v5, v9

    const-string v6, "application/x-gtar"

    aput-object v6, v5, v10

    const-string v6, "application/x-gzip"

    aput-object v6, v5, v11

    const-string v6, "application/x-tar"

    aput-object v6, v5, v12

    const/4 v6, 0x5

    const-string v7, "application/zip"

    aput-object v7, v5, v6

    invoke-static {v5}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v0, Lcfq;->a:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "IMAGE"

    const v6, 0x7f020123    # com.google.android.gms.R.drawable.ic_drive_type_image_big

    const v7, 0x7f0b003b    # com.google.android.gms.R.string.drive_document_type_picture

    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "image/gif"

    aput-object v1, v0, v2

    const-string v1, "image/jpeg"

    aput-object v1, v0, v9

    const-string v1, "image/tiff"

    aput-object v1, v0, v10

    const-string v1, "image/png"

    aput-object v1, v0, v11

    const-string v1, "image/cgm"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "image/fits"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "image/g3fax"

    aput-object v5, v0, v1

    const/4 v1, 0x7

    const-string v5, "image/ief"

    aput-object v5, v0, v1

    const/16 v1, 0x8

    const-string v5, "image/jp2"

    aput-object v5, v0, v1

    const/16 v1, 0x9

    const-string v5, "image/jpm"

    aput-object v5, v0, v1

    const/16 v1, 0xa

    const-string v5, "image/jpx"

    aput-object v5, v0, v1

    const/16 v1, 0xb

    const-string v5, "image/ktx"

    aput-object v5, v0, v1

    const/16 v1, 0xc

    const-string v5, "image/naplps"

    aput-object v5, v0, v1

    const/16 v1, 0xd

    const-string v5, "image/prs.bitf"

    aput-object v5, v0, v1

    const/16 v1, 0xe

    const-string v5, "image/prs.pti"

    aput-object v5, v0, v1

    const/16 v1, 0xf

    const-string v5, "image/svg+xml"

    aput-object v5, v0, v1

    const/16 v1, 0x10

    const-string v5, "image/tiff-fx"

    aput-object v5, v0, v1

    const/16 v1, 0x11

    const-string v5, "image/vnd.adobe.photoshop"

    aput-object v5, v0, v1

    const/16 v1, 0x12

    const-string v5, "image/vnd.svf"

    aput-object v5, v0, v1

    const/16 v1, 0x13

    const-string v5, "image/vnd.xiff"

    aput-object v5, v0, v1

    const/16 v1, 0x14

    const-string v5, "image/vnd.microsoft.icon"

    aput-object v5, v0, v1

    const/16 v1, 0x15

    const-string v5, "image/x-ms-bmp"

    aput-object v5, v0, v1

    const/16 v1, 0x16

    const-string v5, "application/vnd.google.panorama360+jpg"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->b:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "VIDEO"

    const v6, 0x7f020128    # com.google.android.gms.R.drawable.ic_drive_type_video_big

    const v7, 0x7f0b003d    # com.google.android.gms.R.string.drive_document_type_video

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "video/3gpp"

    aput-object v1, v0, v2

    const-string v1, "video/3gp"

    aput-object v1, v0, v9

    const-string v1, "video/H261"

    aput-object v1, v0, v10

    const-string v1, "video/H263"

    aput-object v1, v0, v11

    const-string v1, "video/H264"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "video/mp4"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "video/mpeg"

    aput-object v5, v0, v1

    const/4 v1, 0x7

    const-string v5, "video/quicktime"

    aput-object v5, v0, v1

    const/16 v1, 0x8

    const-string v5, "video/raw"

    aput-object v5, v0, v1

    const/16 v1, 0x9

    const-string v5, "video/vnd.motorola.video"

    aput-object v5, v0, v1

    const/16 v1, 0xa

    const-string v5, "video/vnd.motorola.videop"

    aput-object v5, v0, v1

    const/16 v1, 0xb

    const-string v5, "video/x-la-asf"

    aput-object v5, v0, v1

    const/16 v1, 0xc

    const-string v5, "video/x-m4v"

    aput-object v5, v0, v1

    const/16 v1, 0xd

    const-string v5, "video/x-matroska"

    aput-object v5, v0, v1

    const/16 v1, 0xe

    const-string v5, "video/x-ms-asf"

    aput-object v5, v0, v1

    const/16 v1, 0xf

    const-string v5, "video/x-msvideo"

    aput-object v5, v0, v1

    const/16 v1, 0x10

    const-string v5, "video/x-sgi-movie"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->c:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "MSWORD"

    const v6, 0x7f020129    # com.google.android.gms.R.drawable.ic_drive_type_word_big

    const v7, 0x7f0b0039    # com.google.android.gms.R.string.drive_document_type_ms_word

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/msword"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-word.document.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-word.template.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    aput-object v1, v0, v12

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->d:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "MSEXCEL"

    const v6, 0x7f02011c    # com.google.android.gms.R.drawable.ic_drive_type_excel_big

    const v7, 0x7f0b0037    # com.google.android.gms.R.string.drive_document_type_ms_excel

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/vnd.ms-excel"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-excel.addin.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.ms-excel.sheet.macroEnabled.12"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.ms-excel.template.macroEnabled.12"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v5, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    aput-object v5, v0, v1

    const/4 v1, 0x6

    const-string v5, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    aput-object v5, v0, v1

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->e:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "MSPOWERPOINT"

    const/4 v5, 0x5

    const v6, 0x7f020125    # com.google.android.gms.R.drawable.ic_drive_type_powerpoint_big

    const v7, 0x7f0b0038    # com.google.android.gms.R.string.drive_document_type_ms_powerpoint

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "application/vnd.ms-powerpoint"

    aput-object v1, v0, v2

    const-string v1, "application/vnd.ms-powerpoint.addin.macroEnabled.12"

    aput-object v1, v0, v9

    const-string v1, "application/vnd.ms-powerpoint.presentation.macroEnabled.12"

    aput-object v1, v0, v10

    const-string v1, "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"

    aput-object v1, v0, v11

    const-string v1, "application/vnd.ms-powerpoint.template.macroEnabled.12"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v8, "application/vnd.openxmlformats-officedocument.presentationml.template"

    aput-object v8, v0, v1

    const/4 v1, 0x6

    const-string v8, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    aput-object v8, v0, v1

    const/4 v1, 0x7

    const-string v8, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    aput-object v8, v0, v1

    const/16 v1, 0x8

    const-string v8, "application/vnd.openxmlformats-officedocument.presentationml.slide"

    aput-object v8, v0, v1

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->f:Lcfq;

    new-instance v3, Lcfq;

    const-string v4, "AUDIO"

    const/4 v5, 0x6

    const v6, 0x7f020119    # com.google.android.gms.R.drawable.ic_drive_type_audio_big

    const v7, 0x7f0b002e    # com.google.android.gms.R.string.drive_document_type_audio

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "audio/annodex"

    aput-object v1, v0, v2

    const-string v1, "audio/basic"

    aput-object v1, v0, v9

    const-string v1, "audio/flac"

    aput-object v1, v0, v10

    const-string v1, "audio/mid"

    aput-object v1, v0, v11

    const-string v1, "audio/mpeg"

    aput-object v1, v0, v12

    const/4 v1, 0x5

    const-string v8, "audio/ogg"

    aput-object v8, v0, v1

    const/4 v1, 0x6

    const-string v8, "audio/x-aiff"

    aput-object v8, v0, v1

    const/4 v1, 0x7

    const-string v8, "audio/x-mpegurl"

    aput-object v8, v0, v1

    const/16 v1, 0x8

    const-string v8, "audio/x-pn-realaudio"

    aput-object v8, v0, v1

    const/16 v1, 0x9

    const-string v8, "audio/wav"

    aput-object v8, v0, v1

    const/16 v1, 0xa

    const-string v8, "audio/x-wav"

    aput-object v8, v0, v1

    invoke-static {v0}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcfq;-><init>(Ljava/lang/String;IIILjava/util/Set;)V

    sput-object v3, Lcfq;->g:Lcfq;

    const/4 v0, 0x7

    new-array v0, v0, [Lcfq;

    sget-object v1, Lcfq;->a:Lcfq;

    aput-object v1, v0, v2

    sget-object v1, Lcfq;->b:Lcfq;

    aput-object v1, v0, v9

    sget-object v1, Lcfq;->c:Lcfq;

    aput-object v1, v0, v10

    sget-object v1, Lcfq;->d:Lcfq;

    aput-object v1, v0, v11

    sget-object v1, Lcfq;->e:Lcfq;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, Lcfq;->f:Lcfq;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lcfq;->g:Lcfq;

    aput-object v3, v0, v1

    sput-object v0, Lcfq;->l:[Lcfq;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcfq;->values()[Lcfq;

    move-result-object v3

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    iget-object v0, v5, Lcfq;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcfq;->k:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/util/Set;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcfq;->h:I

    iput p4, p0, Lcfq;->i:I

    iput-object p5, p0, Lcfq;->j:Ljava/util/Set;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcfq;
    .locals 1

    sget-object v0, Lcfq;->k:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfq;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcfq;
    .locals 1

    const-class v0, Lcfq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcfq;

    return-object v0
.end method

.method public static values()[Lcfq;
    .locals 1

    sget-object v0, Lcfq;->l:[Lcfq;

    invoke-virtual {v0}, [Lcfq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcfq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcfq;->h:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcfq;->i:I

    return v0
.end method
