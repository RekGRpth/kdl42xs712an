.class public final Lemr;
.super Liri;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/DataInputStream;

.field private final b:J

.field private final c:Ljava/lang/Class;

.field private d:J

.field private e:[B


# direct methods
.method public constructor <init>(Ljava/io/FileInputStream;JLjava/lang/Class;)V
    .locals 2

    invoke-direct {p0}, Liri;-><init>()V

    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lemr;->a:Ljava/io/DataInputStream;

    iput-wide p2, p0, Lemr;->b:J

    iput-object p4, p0, Lemr;->c:Ljava/lang/Class;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lemr;->d:J

    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lemr;->e:[B

    return-void
.end method

.method private b()Lizs;
    .locals 11

    const-wide/16 v9, 0x8

    const-wide/16 v7, 0x4

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-wide v3, p0, Lemr;->d:J

    iget-wide v5, p0, Lemr;->b:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_8

    iget-wide v3, p0, Lemr;->b:J

    cmp-long v3, v3, v7

    if-gez v3, :cond_0

    const-string v0, "File too short to contain valid data"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_7

    :goto_1
    return-object v0

    :cond_0
    iget-object v3, p0, Lemr;->a:Ljava/io/DataInputStream;

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    iget-wide v4, p0, Lemr;->d:J

    add-long/2addr v4, v7

    iput-wide v4, p0, Lemr;->d:J

    if-lez v3, :cond_1

    iget-wide v4, p0, Lemr;->d:J

    int-to-long v6, v3

    add-long/2addr v4, v6

    add-long/2addr v4, v9

    iget-wide v6, p0, Lemr;->b:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    :cond_1
    const-string v0, "Read bad message size: %d. (pos=%d, len=%d)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v5

    const/4 v3, 0x1

    iget-wide v5, p0, Lemr;->d:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x2

    iget-wide v5, p0, Lemr;->b:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Lehe;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-lez v3, :cond_3

    :goto_2
    invoke-static {v0}, Lwe;->a(Z)V

    iget-object v0, p0, Lemr;->e:[B

    array-length v0, v0

    if-ge v0, v3, :cond_5

    iget-object v0, p0, Lemr;->e:[B

    array-length v0, v0

    :goto_3
    if-ge v0, v3, :cond_4

    mul-int/lit8 v0, v0, 0x2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    new-array v0, v0, [B

    iput-object v0, p0, Lemr;->e:[B

    :cond_5
    iget-object v0, p0, Lemr;->a:Ljava/io/DataInputStream;

    iget-object v4, p0, Lemr;->e:[B

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Ljava/io/DataInputStream;->read([BII)I

    iget-wide v4, p0, Lemr;->d:J

    int-to-long v6, v3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lemr;->d:J

    iget-object v0, p0, Lemr;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    iget-wide v6, p0, Lemr;->d:J

    add-long/2addr v6, v9

    iput-wide v6, p0, Lemr;->d:J

    iget-object v0, p0, Lemr;->e:[B

    const/4 v6, 0x0

    invoke-static {v0, v6, v3, v4, v5}, Lemq;->a([BIIJ)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lemr;->e:[B

    iget-object v4, p0, Lemr;->c:Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-static {v0, v5, v3, v4}, Lemq;->a([BIILjava/lang/Class;)Lizs;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const-string v0, "Failed to read a valid message from the file."

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_8
    :goto_4
    invoke-virtual {p0}, Lemr;->c()Ljava/lang/Object;

    move-object v0, v1

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v3, "Exception while reading from file."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_4
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lemr;->b()Lizs;

    move-result-object v0

    return-object v0
.end method
