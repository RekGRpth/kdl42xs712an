.class public final enum Lias;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lias;

.field public static final enum b:Lias;

.field private static final synthetic c:[Lias;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lias;

    const-string v1, "LEAF"

    invoke-direct {v0, v1, v2}, Lias;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lias;->a:Lias;

    new-instance v0, Lias;

    const-string v1, "INNER"

    invoke-direct {v0, v1, v3}, Lias;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lias;->b:Lias;

    const/4 v0, 0x2

    new-array v0, v0, [Lias;

    sget-object v1, Lias;->a:Lias;

    aput-object v1, v0, v2

    sget-object v1, Lias;->b:Lias;

    aput-object v1, v0, v3

    sput-object v0, Lias;->c:[Lias;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lias;
    .locals 1

    const-class v0, Lias;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lias;

    return-object v0
.end method

.method public static values()[Lias;
    .locals 1

    sget-object v0, Lias;->c:[Lias;

    invoke-virtual {v0}, [Lias;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lias;

    return-object v0
.end method
