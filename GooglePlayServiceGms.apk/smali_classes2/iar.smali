.class public final Liar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lias;

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:Liat;


# direct methods
.method constructor <init>(IFII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lias;->b:Lias;

    iput-object v0, p0, Liar;->a:Lias;

    iput p1, p0, Liar;->b:I

    iput p2, p0, Liar;->c:F

    iput p3, p0, Liar;->d:I

    iput p4, p0, Liar;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Liar;->f:Liat;

    return-void
.end method

.method constructor <init>(Liat;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lias;->a:Lias;

    iput-object v0, p0, Liar;->a:Lias;

    iput-object p1, p0, Liar;->f:Liat;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liar;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liar;

    iget-object v2, p0, Liar;->a:Lias;

    iget-object v3, p1, Liar;->a:Lias;

    if-ne v2, v3, :cond_3

    iget v2, p0, Liar;->b:I

    iget v3, p1, Liar;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Liar;->c:F

    iget v3, p1, Liar;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Liar;->d:I

    iget v3, p1, Liar;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Liar;->e:I

    iget v3, p1, Liar;->e:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Liar;->f:Liat;

    if-nez v2, :cond_4

    iget-object v2, p1, Liar;->f:Liat;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liar;->f:Liat;

    iget-object v3, p1, Liar;->f:Liat;

    invoke-virtual {v2, v3}, Liat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Liar;->a:Lias;

    invoke-virtual {v0}, Lias;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Liar;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Liar;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Liar;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Liar;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Liar;->f:Liat;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Liar;->f:Liat;

    invoke-virtual {v0}, Liat;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Liar;->a:Lias;

    sget-object v1, Lias;->a:Lias;

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Leaf: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Liar;->f:Liat;

    invoke-virtual {v1}, Liat;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[%d] <= %f (%d) : (%d)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Liar;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Liar;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Liar;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Liar;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
