.class final Lhrb;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhra;


# direct methods
.method constructor <init>(Lhra;Ljava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V
    .locals 8

    const/4 v1, 0x1

    iput-object p1, p0, Lhrb;->a:Lhra;

    const-wide/16 v3, 0x3c

    move-object v0, p0

    move v2, v1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/RejectedExecutionHandler;)V

    return-void
.end method


# virtual methods
.method protected final terminated()V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrb;->a:Lhra;

    iget-object v0, v0, Lhra;->b:Limb;

    const-string v1, "LocalScanResultWriter terminated."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-static {}, Lhru;->a()Lhru;

    move-result-object v0

    iget-object v1, p0, Lhrb;->a:Lhra;

    invoke-static {v1}, Lhra;->a(Lhra;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhru;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lhrb;->a:Lhra;

    iget-object v0, v0, Lhra;->a:Lhqq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrb;->a:Lhra;

    iget-object v0, v0, Lhra;->a:Lhqq;

    iget-object v1, p0, Lhrb;->a:Lhra;

    iget-object v1, v1, Lhra;->d:Lhsd;

    invoke-interface {v0, v1}, Lhqq;->a(Lhsd;)V

    :cond_1
    iget-object v0, p0, Lhrb;->a:Lhra;

    invoke-static {v0}, Lhra;->b(Lhra;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhrb;->a:Lhra;

    iget-object v0, v0, Lhra;->d:Lhsd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhrb;->a:Lhra;

    iget-object v1, p0, Lhrb;->a:Lhra;

    iget-object v1, v1, Lhra;->d:Lhsd;

    invoke-virtual {v0, v1}, Lhra;->a(Lhsd;)V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lhrb;->a:Lhra;

    iget-object v1, v1, Lhra;->a:Lhqq;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhrb;->a:Lhra;

    iget-object v1, v1, Lhra;->a:Lhqq;

    iget-object v2, p0, Lhrb;->a:Lhra;

    iget-object v2, v2, Lhra;->d:Lhsd;

    invoke-interface {v1, v2}, Lhqq;->a(Lhsd;)V

    :cond_3
    throw v0
.end method
