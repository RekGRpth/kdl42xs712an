.class final Lhyk;
.super Lhyp;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Landroid/app/PendingIntent;

.field public final c:Leqr;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/app/PendingIntent;Leqr;)V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lhyp;-><init>(I)V

    iput-object p1, p0, Lhyk;->a:Ljava/util/List;

    iput-object p2, p0, Lhyk;->b:Landroid/app/PendingIntent;

    iput-object p3, p0, Lhyk;->c:Leqr;

    return-void
.end method

.method public static a(Leqr;ILjava/util/List;)V
    .locals 4

    if-eqz p0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-interface {p0, p1, v2}, Leqr;->a(I[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final a()V
    .locals 3

    iget-object v1, p0, Lhyk;->c:Leqr;

    invoke-virtual {p0}, Lhyk;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lhyk;->a:Ljava/util/List;

    invoke-static {v1, v0, v2}, Lhyk;->a(Leqr;ILjava/util/List;)V

    return-void
.end method
