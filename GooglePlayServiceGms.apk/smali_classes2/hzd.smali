.class final Lhzd;
.super Lhyy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhzd;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyy;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "NotInitializedState"

    return-object v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 14

    const/4 v2, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v0, 0x4

    if-eq v1, v0, :cond_0

    const/4 v0, 0x5

    if-eq v1, v0, :cond_0

    move v0, v2

    :goto_0
    const-string v3, "Received add or remove geofence request before initialized."

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    packed-switch v1, :pswitch_data_0

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhzd;->a:Lhyt;

    invoke-virtual {v3, v1}, Lhyt;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in NotInitializedState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lhyy;->b(Landroid/os/Message;)Z

    move-result v2

    :goto_1
    return v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Initializing GeofencerStateMachine."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lhzd;->c:Lhys;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    iget-object v1, v3, Lhys;->e:Lhyr;

    invoke-virtual {v1}, Lhyr;->a()V

    iget-object v4, v3, Lhys;->f:Lhyi;

    iget-object v1, v3, Lhys;->e:Lhyr;

    invoke-virtual {v1}, Lhyr;->b()Lhpk;

    move-result-object v1

    iget-object v5, v4, Lhyi;->h:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    new-instance v6, Liln;

    new-instance v7, Ljava/io/File;

    iget-object v8, v4, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    const-string v9, "geofencer_ad_state"

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v6, v7, v1}, Liln;-><init>(Ljava/io/File;Lhpk;)V

    iput-object v6, v4, Lhyi;->i:Liln;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, v4, Lhyi;->i:Liln;

    new-instance v6, Lhye;

    invoke-direct {v6}, Lhye;-><init>()V

    invoke-virtual {v1, v6}, Liln;->a(Lizk;)Lizk;

    move-result-object v1

    check-cast v1, Lhye;

    iget-wide v6, v1, Lhye;->a:J

    iget-object v8, v4, Lhyi;->c:Lbpe;

    invoke-interface {v8}, Lbpe;->a()J

    move-result-wide v8

    iget-object v10, v4, Lhyi;->c:Lbpe;

    invoke-interface {v10}, Lbpe;->b()J

    move-result-wide v10

    sub-long/2addr v8, v10

    sub-long v10, v6, v8

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    const-wide/16 v12, 0x2710

    cmp-long v10, v10, v12

    if-gez v10, :cond_2

    invoke-virtual {v1}, Lhye;->c()I

    move-result v10

    if-nez v10, :cond_4

    :cond_2
    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "ActivityDetector"

    const-string v4, "Boot time changed, not recovering activities."

    invoke-static {v1, v4}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    iget-object v1, v3, Lhys;->h:Lhxq;

    iget-object v3, v3, Lhys;->e:Lhyr;

    invoke-virtual {v3}, Lhyr;->b()Lhpk;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lhxq;->a(Landroid/content/Intent;Lhpk;)V

    iget-object v0, p0, Lhzd;->c:Lhys;

    invoke-virtual {v0}, Lhys;->g()V

    iget-object v0, p0, Lhzd;->a:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhzd;->c:Lhys;

    invoke-virtual {v0}, Lhys;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhzd;->a:Lhyt;

    iget-object v1, p0, Lhzd;->a:Lhyt;

    invoke-static {v1}, Lhyt;->k(Lhyt;)Lhzb;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->a(Lhyt;Lbqg;)V

    iget-object v0, p0, Lhzd;->a:Lhyt;

    invoke-static {v0}, Lhyt;->l(Lhyt;)V

    goto/16 :goto_1

    :cond_4
    :try_start_3
    sget-boolean v10, Lhyb;->a:Z

    if-eqz v10, :cond_5

    const-string v10, "ActivityDetector"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "load: loaded "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lhye;->c()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " activities."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-object v4, v4, Lhyi;->g:Lhzo;

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->min(JJ)J

    invoke-virtual {v4, v1}, Lhzo;->a(Lhye;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    :goto_3
    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :catch_0
    move-exception v1

    :try_start_5
    sget-boolean v4, Lhyb;->a:Z

    if-eqz v4, :cond_6

    const-string v4, "ActivityDetector"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unable to read activity state file: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lhzd;->a:Lhyt;

    iget-object v1, p0, Lhzd;->a:Lhyt;

    invoke-static {v1}, Lhyt;->m(Lhyt;)Lhzc;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->b(Lhyt;Lbqg;)V

    goto/16 :goto_1

    :cond_8
    const-string v0, "GeofencerStateMachine"

    const-string v1, "Network location disabled."

    invoke-static {v0, v1}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhzd;->a:Lhyt;

    iget-object v1, p0, Lhzd;->a:Lhyt;

    invoke-static {v1}, Lhyt;->n(Lhyt;)Lhyz;

    move-result-object v1

    invoke-static {v0, v1}, Lhyt;->c(Lhyt;Lbqg;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
