.class public final Lgei;
.super Lbmf;
.source "SourceFile"


# instance fields
.field private final a:Lbmi;


# direct methods
.method public constructor <init>(Lbmi;)V
    .locals 0

    invoke-direct {p0}, Lbmf;-><init>()V

    iput-object p1, p0, Lgei;->a:Lbmi;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;
    .locals 6

    const/4 v2, 0x1

    const-string v0, "people/%1$s/activities"

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Lgei;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p3, :cond_0

    const-string v0, "contextType"

    invoke-static {p3}, Lgei;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p4, :cond_1

    const-string v0, "language"

    invoke-static {p4}, Lgei;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p5, :cond_2

    const-string v0, "notifyCircles"

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz p6, :cond_3

    const-string v0, "preview"

    invoke-static {p6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz p7, :cond_4

    const-string v0, "shareOnGooglePlus"

    invoke-static {p7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz p8, :cond_5

    const-string v0, "source"

    invoke-static {p8}, Lgei;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lgei;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    iget-object v0, p0, Lgei;->a:Lbmi;

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-object v1, p1

    move-object v4, p9

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    return-object v0
.end method
