.class public final Lbsv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbsu;


# instance fields
.field private final a:Lcfc;

.field private final b:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final d:J

.field private final e:Lcom/google/android/gms/drive/DriveId;

.field private final f:Lcmn;


# direct methods
.method public constructor <init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcmn;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbsv;->a:Lcfc;

    iput-object p2, p0, Lbsv;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object p3, p0, Lbsv;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-wide p4, p0, Lbsv;->d:J

    iput-object p6, p0, Lbsv;->e:Lcom/google/android/gms/drive/DriveId;

    iput-object p7, p0, Lbsv;->f:Lcmn;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 8

    new-instance v0, Lcmb;

    iget-object v1, p0, Lbsv;->a:Lcfc;

    iget-object v2, p0, Lbsv;->b:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v4, p0, Lbsv;->c:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-wide v5, p0, Lbsv;->d:J

    iget-object v7, p0, Lbsv;->e:Lcom/google/android/gms/drive/DriveId;

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcmb;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;)V

    iget-object v1, p0, Lbsv;->f:Lcmn;

    invoke-virtual {v1, v0}, Lcmn;->a(Lcml;)Z

    return-object v0
.end method
