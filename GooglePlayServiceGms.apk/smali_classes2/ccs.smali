.class public final Lccs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lal;


# instance fields
.field public a:Z

.field public final synthetic b:Lcom/google/android/gms/drive/data/view/DocListView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/data/view/DocListView;)V
    .locals 1

    iput-object p1, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lccs;->a:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/drive/data/view/DocListView;B)V
    .locals 0

    invoke-direct {p0, p1}, Lccs;-><init>(Lcom/google/android/gms/drive/data/view/DocListView;)V

    return-void
.end method


# virtual methods
.method public final N_()V
    .locals 1

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->g(Lcom/google/android/gms/drive/data/view/DocListView;)V

    return-void
.end method

.method public final a(ILandroid/os/Bundle;)Lcb;
    .locals 5

    new-instance v0, Lbyz;

    iget-object v1, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/data/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v2}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/data/view/DocListView;)Lcfz;

    move-result-object v2

    iget-object v3, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/data/view/DocListView;->c()Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    move-result-object v3

    iget-object v4, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v4}, Lcom/google/android/gms/drive/data/view/DocListView;->f(Lcom/google/android/gms/drive/data/view/DocListView;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbyz;-><init>(Landroid/content/Context;Lcfz;Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;Ljava/lang/String;)V

    return-object v0
.end method

.method public final synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lbzb;

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->g(Lcom/google/android/gms/drive/data/view/DocListView;)V

    iget-boolean v0, p0, Lccs;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->h(Lcom/google/android/gms/drive/data/view/DocListView;)Lbzf;

    move-result-object v0

    invoke-interface {v0, p2}, Lbzf;->a(Lbzb;)V

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->i()V

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Z)V

    iget-object v0, p0, Lccs;->b:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->i(Lcom/google/android/gms/drive/data/view/DocListView;)V

    :cond_0
    return-void
.end method
