.class public Lfpu;
.super Lfqe;
.source "SourceFile"


# instance fields
.field public e:Lfpv;

.field public f:Lfqo;

.field private q:Lbon;

.field private r:Z

.field private final s:Lfqm;

.field private final t:Lboq;

.field private u:Lbon;

.field private final v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v7, v6

    invoke-direct/range {v0 .. v8}, Lfqe;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;)V

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfpu;->q:Lbon;

    iput-boolean v6, p0, Lfpu;->r:Z

    new-instance v0, Lfqm;

    invoke-direct {v0, p0}, Lfqm;-><init>(Lfqe;)V

    iput-object v0, p0, Lfpu;->s:Lfqm;

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfpu;->t:Lboq;

    iput-boolean p5, p0, Lfpu;->v:Z

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lfpu;->e:Lfpv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpu;->e:Lfpv;

    invoke-interface {v0}, Lfpv;->O_()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lfqe;->a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lbgo;)Lbon;
    .locals 1

    new-instance v0, Lfqn;

    invoke-direct {v0, p0, p1}, Lfqn;-><init>(Lfqe;Lbgo;)V

    return-object v0
.end method

.method protected final a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lfqe;->a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->g()V

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 2

    invoke-super/range {p0 .. p12}, Lfqe;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->g()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Z)V

    return-object v0
.end method

.method public final a(Lfpv;)Lfpu;
    .locals 0

    iput-object p1, p0, Lfpu;->e:Lfpv;

    return-object p0
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 8

    const/4 v7, 0x1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v5

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v6, "secondaryText"

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "contactsAvatarUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "contactType"

    invoke-virtual {v2, v3, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    new-instance v0, Lfqh;

    const v2, 0x7f0b035a    # com.google.android.gms.R.string.plus_audience_selection_search_device_results

    new-instance v3, Lfqi;

    invoke-direct {v3, p0, v1}, Lfqi;-><init>(Lfqe;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v3}, Lfqh;-><init>(Lfqe;ILbon;)V

    iput-object v0, p0, Lfpu;->u:Lbon;

    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lfpu;->u:Lbon;

    goto :goto_1
.end method

.method public final a(Lbgo;Z)V
    .locals 3

    iput-boolean p2, p0, Lfpu;->r:Z

    new-instance v0, Lfqn;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v2

    invoke-direct {v0, p0, p1, v1, v2}, Lfqn;-><init>(Lfqe;Lbgo;II)V

    iput-object v0, p0, Lfpu;->q:Lbon;

    invoke-virtual {p0}, Lfpu;->l()V

    return-void
.end method

.method public a(Lfee;)V
    .locals 3

    new-instance v0, Lfqo;

    const/4 v1, 0x1

    new-array v1, v1, [Lfee;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lfqo;-><init>(Lfqe;Ljava/util/Collection;)V

    iput-object v0, p0, Lfpu;->f:Lfqo;

    invoke-virtual {p0}, Lfpu;->l()V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lfpu;->r:Z

    return-void
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfpu;->f:Lfqo;

    invoke-virtual {p0}, Lfpu;->l()V

    return-void
.end method

.method protected final d()Lbon;
    .locals 11

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v1, Lbom;

    const/4 v0, 0x4

    new-array v2, v0, [Lbon;

    iget-object v0, p0, Lfpu;->u:Lbon;

    aput-object v0, v2, v7

    new-instance v0, Lfqh;

    const v3, 0x7f0b035d    # com.google.android.gms.R.string.plus_audience_selection_header_circles

    new-instance v4, Lbom;

    new-array v5, v9, [Lbon;

    iget-object v6, p0, Lfqe;->n:Lbon;

    aput-object v6, v5, v7

    iget-object v6, p0, Lfqe;->o:Lbon;

    aput-object v6, v5, v8

    invoke-direct {v4, p0, v5}, Lbom;-><init>(Lbol;[Lbon;)V

    invoke-direct {v0, p0, v3, v4}, Lfqh;-><init>(Lfqe;ILbon;)V

    aput-object v0, v2, v8

    new-instance v0, Lfqh;

    const v3, 0x7f0b0359    # com.google.android.gms.R.string.plus_audience_selection_search_google_results

    new-instance v4, Lbom;

    new-array v5, v10, [Lbon;

    iget-object v6, p0, Lfpu;->f:Lfqo;

    aput-object v6, v5, v7

    iget-object v6, p0, Lfqe;->p:Lbon;

    aput-object v6, v5, v8

    iget-object v6, p0, Lfpu;->q:Lbon;

    aput-object v6, v5, v9

    invoke-direct {v4, p0, v5}, Lbom;-><init>(Lbol;[Lbon;)V

    invoke-direct {v0, p0, v3, v4}, Lfqh;-><init>(Lfqe;ILbon;)V

    aput-object v0, v2, v9

    iget-boolean v0, p0, Lfpu;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpu;->s:Lfqm;

    :goto_0
    aput-object v0, v2, v10

    invoke-direct {v1, p0, v2}, Lbom;-><init>(Lbol;[Lbon;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lfpu;->t:Lboq;

    goto :goto_0
.end method

.method protected final f()Z
    .locals 1

    iget-boolean v0, p0, Lfpu;->r:Z

    return v0
.end method
