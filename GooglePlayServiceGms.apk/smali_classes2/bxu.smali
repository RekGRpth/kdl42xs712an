.class final Lbxu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lbxs;


# direct methods
.method constructor <init>(Lbxs;)V
    .locals 0

    iput-object p1, p0, Lbxu;->a:Lbxs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbxu;->a:Lbxs;

    invoke-static {v0}, Lbxs;->d(Lbxs;)Lcfz;

    move-result-object v0

    iget-object v1, p0, Lbxu;->a:Lbxs;

    invoke-static {v1}, Lbxs;->b(Lbxs;)Lcfc;

    move-result-object v1

    iget-object v2, p0, Lbxu;->a:Lbxs;

    invoke-static {v2}, Lbxs;->c(Lbxs;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "CreateFileDialogFragment"

    const-string v1, "Cannot open folder picker because application %s is no longer authorized."

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lbxu;->a:Lbxs;

    invoke-static {v3}, Lbxs;->c(Lbxs;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbxu;->a:Lbxs;

    invoke-virtual {v1}, Lbxs;->T_()Lo;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/data/ui/open/OpenFileActivityDelegate;->a(Landroid/content/Context;Lbsp;)Lbyf;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "application/vnd.google-apps.folder"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lbyf;->a([Ljava/lang/String;)Lbyf;

    move-result-object v0

    iget-object v1, p0, Lbxu;->a:Lbxs;

    invoke-static {v1}, Lbxs;->e(Lbxs;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbyf;->a(Lcom/google/android/gms/drive/DriveId;)Lbyf;

    move-result-object v0

    iget-object v1, v0, Lbyf;->a:Landroid/content/Intent;

    const-string v2, "showNewFolder"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lbxu;->a:Lbxs;

    const v2, 0x7f0b0022    # com.google.android.gms.R.string.drive_create_file_pick_folder_dialog_title

    invoke-virtual {v1, v2}, Lbxs;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbyf;->a(Ljava/lang/String;)Lbyf;

    move-result-object v0

    invoke-virtual {v0}, Lbyf;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lbxu;->a:Lbxs;

    invoke-virtual {v1, v0, v4}, Lbxs;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method
