.class final Lgwl;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lgwj;


# direct methods
.method constructor <init>(Lgwj;)V
    .locals 0

    iput-object p1, p0, Lgwl;->a:Lgwj;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method

.method private a(Ljar;Z)V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    iget v2, p1, Ljar;->a:I

    iget-object v0, p1, Ljar;->b:Ljava/lang/String;

    :goto_0
    iget-object v4, p0, Lgwl;->a:Lgwj;

    invoke-virtual {v4}, Lgwj;->j()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lgwl;->a:Lgwj;

    invoke-static {v5}, Lgwj;->b(Lgwj;)[I

    move-result-object v5

    invoke-static {v4, v2, v5}, Lgth;->a(Landroid/content/res/Resources;I[I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lgwl;->a:Lgwj;

    iget-object v0, v0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v3, p0, Lgwl;->a:Lgwj;

    invoke-static {v3}, Lgwj;->c(Lgwj;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->d(Lgwj;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lgwl;->a:Lgwj;

    invoke-static {v2}, Lgwj;->c(Lgwj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->e(Lgwj;)Ljava/lang/String;

    :goto_1
    return-void

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->f(Lgwj;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->h(Lgwj;)Lhcb;

    move-result-object v0

    new-instance v2, Lgwm;

    iget-object v4, p0, Lgwl;->a:Lgwj;

    invoke-direct {v2, v4, v3}, Lgwm;-><init>(Lgwj;B)V

    iget-object v3, p0, Lgwl;->a:Lgwj;

    invoke-static {v3}, Lgwj;->g(Lgwj;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lhcb;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->f(Lgwj;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lgwl;->a:Lgwj;

    invoke-static {v2}, Lgwj;->c(Lgwj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->d(Lgwj;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lgwl;->a:Lgwj;

    invoke-static {v2}, Lgwj;->c(Lgwj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->e(Lgwj;)Ljava/lang/String;

    iget-object v0, p0, Lgwl;->a:Lgwj;

    invoke-static {v0}, Lgwj;->j(Lgwj;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lgwl;->a:Lgwj;

    invoke-static {v1}, Lgwj;->i(Lgwj;)J

    iget-object v1, p0, Lgwl;->a:Lgwj;

    invoke-static {v1}, Lgwj;->d(Lgwj;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lgwl;->a:Lgwj;

    invoke-static {v2}, Lgwj;->c(Lgwj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    move-object v0, v1

    move v2, v3

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgwl;->a(Ljar;Z)V

    return-void
.end method

.method public final a(Lipy;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgwl;->a(Ljar;Z)V

    return-void
.end method

.method public final a(Ljar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgwl;->a(Ljar;Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgwl;->a(Ljar;Z)V

    return-void
.end method

.method public final c()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lgwl;->a(Ljar;Z)V

    return-void
.end method
