.class abstract Lclu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcml;


# instance fields
.field private final a:Lcmr;

.field final b:Lcfc;

.field private final c:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final d:Lcms;


# direct methods
.method protected constructor <init>(Lcmr;Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcms;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "type must not be null"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmr;

    iput-object v0, p0, Lclu;->a:Lcmr;

    const-string v0, "account must not be null"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lclu;->b:Lcfc;

    const-string v0, "app identity must not be null"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    const-string v0, "enforcement mode must not be null"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcms;

    iput-object v0, p0, Lclu;->d:Lcms;

    return-void
.end method

.method protected constructor <init>(Lcmr;Lcfc;Lorg/json/JSONObject;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmr;

    iput-object v0, p0, Lclu;->a:Lcmr;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lclu;->b:Lcfc;

    const-string v0, "requestingAppIdentity"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    iput-object v0, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    const-string v0, "permissionEnforcement"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcms;->a(Ljava/lang/String;)Lcms;

    move-result-object v0

    iput-object v0, p0, Lclu;->d:Lcms;

    return-void
.end method


# virtual methods
.method protected abstract a(Lcfz;Lbsp;)Lcml;
.end method

.method protected abstract a(Lcom/google/android/gms/common/server/ClientContext;Lcoy;)V
.end method

.method protected a(Lcoy;)V
    .locals 0

    return-void
.end method

.method protected a(Lclu;)Z
    .locals 2

    iget-object v0, p0, Lclu;->a:Lcmr;

    iget-object v1, p1, Lclu;->a:Lcmr;

    invoke-virtual {v0, v1}, Lcmr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclu;->b:Lcfc;

    iget-object v1, p1, Lclu;->b:Lcfc;

    invoke-virtual {v0, v1}, Lcfc;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v1, p1, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcfz;)Lbsp;
    .locals 2

    iget-object v0, p0, Lclu;->b:Lcfc;

    iget-object v1, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {p1, v0, v1}, Lcfz;->a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;

    move-result-object v0

    return-object v0
.end method

.method public b()Lorg/json/JSONObject;
    .locals 3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "requestingAppIdentity"

    iget-object v2, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->d()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "operationType"

    iget-object v2, p0, Lclu;->a:Lcmr;

    invoke-virtual {v2}, Lcmr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "permissionEnforcement"

    iget-object v2, p0, Lclu;->d:Lcms;

    invoke-virtual {v2}, Lcms;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-object v0
.end method

.method public final b(Lcoy;)V
    .locals 1

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lclu;->d(Lcfz;)Lbsp;

    move-result-object v0

    invoke-virtual {p0, p1}, Lclu;->a(Lcoy;)V

    invoke-virtual {v0}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lclu;->a(Lcom/google/android/gms/common/server/ClientContext;Lcoy;)V

    return-void
.end method

.method protected c()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lclu;->a:Lcmr;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lclu;->b:Lcfc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final c(Lcfz;)Lclw;
    .locals 2

    invoke-virtual {p0, p1}, Lclu;->d(Lcfz;)Lbsp;

    move-result-object v0

    new-instance v1, Lclw;

    invoke-virtual {p0, p1, v0}, Lclu;->a(Lcfz;Lbsp;)Lcml;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lclw;-><init>(Lcml;Lcml;)V

    return-object v1
.end method

.method protected final d(Lcfz;)Lbsp;
    .locals 2

    iget-object v0, p0, Lclu;->d:Lcms;

    sget-object v1, Lcms;->b:Lcms;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lclu;->b:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, Lclu;->b(Lcfz;)Lbsp;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lclv;

    iget-object v1, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v0, v1}, Lclv;-><init>(Lcom/google/android/gms/drive/auth/AppIdentity;)V

    throw v0
.end method

.method public final d()Lcfc;
    .locals 1

    iget-object v0, p0, Lclu;->b:Lcfc;

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 4

    const-string v0, "type=%s, account=%s, requestingAppIdentity=%s, enforcementMode=%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lclu;->a:Lcmr;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lclu;->b:Lcfc;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lclu;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lclu;->d:Lcms;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
