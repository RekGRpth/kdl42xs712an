.class public final enum Lapy;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lapy;

.field public static final enum b:Lapy;

.field public static final enum c:Lapy;

.field private static final synthetic d:[Lapy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lapy;

    const-string v1, "EMAIL_ONLY"

    invoke-direct {v0, v1, v2}, Lapy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lapy;->a:Lapy;

    new-instance v0, Lapy;

    const-string v1, "PHONE_ONLY"

    invoke-direct {v0, v1, v3}, Lapy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lapy;->b:Lapy;

    new-instance v0, Lapy;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lapy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lapy;->c:Lapy;

    const/4 v0, 0x3

    new-array v0, v0, [Lapy;

    sget-object v1, Lapy;->a:Lapy;

    aput-object v1, v0, v2

    sget-object v1, Lapy;->b:Lapy;

    aput-object v1, v0, v3

    sget-object v1, Lapy;->c:Lapy;

    aput-object v1, v0, v4

    sput-object v0, Lapy;->d:[Lapy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lapy;
    .locals 1

    const-class v0, Lapy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lapy;

    return-object v0
.end method

.method public static values()[Lapy;
    .locals 1

    sget-object v0, Lapy;->d:[Lapy;

    invoke-virtual {v0}, [Lapy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lapy;

    return-object v0
.end method
