.class public final Lcal;
.super Lcai;
.source "SourceFile"


# instance fields
.field private h:I

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/drawable/NinePatchDrawable;

.field private k:I

.field private l:Landroid/graphics/RectF;

.field private m:Lcam;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcaj;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3, p4}, Lcai;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcaj;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcal;->i:Landroid/graphics/Paint;

    const v1, 0x7f0200cb    # com.google.android.gms.R.drawable.drive_menu_submenu_background

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lcal;->j:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcal;->l:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    const/4 v1, 0x3

    iget v0, p0, Lcai;->g:I

    invoke-super {p0, p1}, Lcai;->a(I)V

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eq p1, v1, :cond_2

    if-ne v0, v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcal;->m:Lcam;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcal;->m:Lcam;

    goto :goto_0
.end method

.method public final a(IILandroid/content/res/Resources;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcai;->a:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Lcai;->c(I)I

    move-result v1

    invoke-super {p0, p1}, Lcai;->b(I)I

    move-result v2

    iget v3, p0, Lcai;->c:I

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    iget-object v0, p0, Lcai;->d:Landroid/graphics/RectF;

    iget v1, p0, Lcai;->e:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcai;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcai;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcai;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcai;->b:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    iget-object v0, p0, Lcal;->l:Landroid/graphics/RectF;

    iget v1, p0, Lcal;->h:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcal;->h:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    div-int/lit8 v1, p2, 0xa

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcal;->i:Landroid/graphics/Paint;

    const-string v3, "W"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v5, v4, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcal;->k:I

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcal;->k:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v3, v1

    int-to-float v1, v1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcal;->j:Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcal;->j:Landroid/graphics/drawable/NinePatchDrawable;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    :cond_2
    return-void
.end method

.method public final a(ILandroid/content/res/Resources;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x43960000    # 300.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcal;->h:I

    return-void
.end method

.method final a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcal;->j:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    iget-object v1, p0, Lcal;->l:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v3

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v5, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, v1, Landroid/graphics/RectF;->top:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v5

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, p2, v4, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final a(Landroid/widget/AbsListView;III)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcai;->a(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public final a(Lcam;)V
    .locals 0

    iput-object p1, p0, Lcal;->m:Lcam;

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Lcai;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 0

    invoke-super {p0}, Lcai;->b()V

    return-void
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Lcai;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    invoke-super {p0}, Lcai;->c()Z

    move-result v0

    return v0
.end method

.method public final d(I)V
    .locals 2

    iget-object v0, p0, Lcal;->i:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    return-void
.end method

.method public final e()V
    .locals 1

    const-string v0, ""

    iput-object v0, p0, Lcal;->f:Ljava/lang/String;

    invoke-super {p0}, Lcai;->d()V

    return-void
.end method
