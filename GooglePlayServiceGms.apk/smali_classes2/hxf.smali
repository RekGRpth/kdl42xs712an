.class public final Lhxf;
.super Lhxe;
.source "SourceFile"

# interfaces
.implements Lhlo;


# instance fields
.field public a:Lhlo;

.field private final b:Lhlf;

.field private c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lhlf;)V
    .locals 1

    invoke-direct {p0}, Lhxe;-><init>()V

    iput-object p1, p0, Lhxf;->b:Lhlf;

    iget-object v0, p0, Lhxf;->b:Lhlf;

    iget-boolean v0, v0, Lhlf;->a:Z

    invoke-virtual {p0, v0}, Lhxf;->a(Z)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Lhxf;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lhxf;->c:Landroid/os/Handler;

    :cond_0
    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhxf;->b:Lhlf;

    invoke-virtual {v0, p0}, Lhlf;->a(Lhlo;)Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhxf;->b:Lhlf;

    invoke-virtual {v0}, Lhlf;->a()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhxf;->a:Lhlo;

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lhxf;->c:Landroid/os/Handler;

    new-instance v1, Lhxg;

    invoke-direct {v1, p0}, Lhxg;-><init>(Lhxf;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
