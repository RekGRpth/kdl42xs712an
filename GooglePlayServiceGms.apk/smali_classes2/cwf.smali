.class public abstract Lcwf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldl;

.field public b:Z

.field private final c:[Ljava/lang/String;

.field private final d:J

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/Object;


# direct methods
.method protected constructor <init>([Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcwf;->c:[Ljava/lang/String;

    new-instance v0, Ldl;

    invoke-virtual {p0}, Lcwf;->a()I

    move-result v1

    invoke-direct {v0, v1}, Ldl;-><init>(I)V

    iput-object v0, p0, Lcwf;->a:Ldl;

    iput-wide p2, p0, Lcwf;->d:J

    iput-object p4, p0, Lcwf;->e:Ljava/lang/String;

    iput-object p5, p0, Lcwf;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcwf;->g:Ljava/lang/Object;

    sget-object v0, Lcwh;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcwf;->b:Z

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public final a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-nez v0, :cond_0

    const/4 v0, 0x4

    invoke-static {v0, p2}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lcwg;->a:Lbhk;

    if-nez p2, :cond_1

    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    :cond_1
    iget-object v2, v0, Lcwg;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcwg;->c:Ljava/lang/String;

    iget v3, v0, Lcwg;->d:I

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v2, p0, Lcwf;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcwf;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbhk;->a(Ljava/lang/String;)V

    :cond_3
    iget v0, v0, Lcwg;->b:I

    invoke-virtual {v1, v0, p2, p3}, Lbhk;->a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcwf;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0}, Ldl;->b()V

    :cond_0
    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcwf;->g:Ljava/lang/Object;

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz v0, :cond_0

    iput-object p2, v0, Lcwg;->c:Ljava/lang/String;

    iput p3, v0, Lcwg;->d:I

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V
    .locals 11

    iget-wide v9, p0, Lcwf;->d:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-wide/from16 v7, p7

    invoke-virtual/range {v0 .. v10}, Lcwf;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V
    .locals 7

    invoke-virtual {p0, p1, p7, p8}, Lcwf;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcwf;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "TransientDataCache"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expired; clearing data for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcwf;->c(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-nez v0, :cond_2

    new-instance v1, Lbhk;

    iget-object v0, p0, Lcwf;->c:[Ljava/lang/String;

    iget-object v2, p0, Lcwf;->f:Ljava/lang/String;

    invoke-direct {v1, v0, v2, p4, p5}, Lbhk;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcwg;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-wide v3, p7

    move-wide/from16 v5, p9

    invoke-direct/range {v0 .. v6}, Lcwg;-><init>(Lbhk;Ljava/lang/Integer;JJ)V

    move-object v1, v0

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v4, v1, Lcwg;->a:Lbhk;

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v4, v0}, Lbhk;->a(Landroid/content/ContentValues;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcwg;->a:Lbhk;

    iput-object p5, v1, Lbhk;->b:Ljava/lang/String;

    iput p3, v0, Lcwg;->b:I

    iput-wide p7, v0, Lcwg;->e:J

    move-object v1, v0

    goto :goto_0

    :cond_3
    packed-switch p6, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page direction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, v1, Lcwg;->a:Lbhk;

    iput-object p5, v0, Lbhk;->b:Ljava/lang/String;

    :goto_2
    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1, v1}, Ldl;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcwf;->b:Z

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcwf;->a(Ljava/lang/String;Lcwg;)V

    :cond_4
    return-void

    :pswitch_1
    iget-object v0, v1, Lcwg;->a:Lbhk;

    iput-object p4, v0, Lbhk;->a:Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    iget-object v0, v1, Lcwg;->a:Lbhk;

    iput-object p5, v0, Lbhk;->b:Ljava/lang/String;

    iget-object v0, v1, Lcwg;->a:Lbhk;

    iput-object p4, v0, Lbhk;->a:Ljava/lang/String;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcwg;)V
    .locals 7

    invoke-virtual {p0}, Lcwf;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "TransientDataCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "*** Emitting cache entry for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ***"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lcwg;->a:Lbhk;

    invoke-virtual {v0}, Lbhk;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_1

    const-string v4, "TransientDataCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v6

    invoke-virtual {v2, v1, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;J)Z
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcwg;->a:Lbhk;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcwg;->a:Lbhk;

    invoke-virtual {v2}, Lbhk;->a()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-wide v2, v0, Lcwg;->e:J

    sub-long v2, p2, v2

    iget-wide v4, v0, Lcwg;->f:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;JIZ)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, p3}, Lcwf;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz p5, :cond_2

    iget-object v3, v0, Lcwg;->a:Lbhk;

    invoke-virtual {v3}, Lbhk;->a()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v0, v0, Lcwg;->a:Lbhk;

    iget-object v0, v0, Lbhk;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lcwg;->a:Lbhk;

    invoke-virtual {v3}, Lbhk;->a()I

    move-result v3

    if-ge v3, p4, :cond_3

    iget-object v0, v0, Lcwg;->a:Lbhk;

    iget-object v0, v0, Lbhk;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public final b(Ljava/lang/Object;J)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcwf;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcwg;->a:Lbhk;

    iget-object v0, v0, Lbhk;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcwf;->g:Ljava/lang/Object;

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->b(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcwf;->a:Ldl;

    invoke-virtual {v0, p1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    iput v1, v0, Lcwg;->b:I

    :cond_0
    return-void
.end method
