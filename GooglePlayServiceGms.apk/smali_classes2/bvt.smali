.class public Lbvt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljava/util/concurrent/BlockingQueue;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Lcoy;

.field private final f:Lcfz;

.field private final g:Lcov;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lbvt;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lbvt;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcoy;)V
    .locals 2

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lbvt;-><init>(Lcoy;Ljava/util/concurrent/BlockingQueue;)V

    return-void
.end method

.method private constructor <init>(Lcoy;Ljava/util/concurrent/BlockingQueue;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbvt;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoy;

    iput-object v0, p0, Lbvt;->e:Lcoy;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/BlockingQueue;

    iput-object v0, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbvt;->f:Lcfz;

    new-instance v0, Lcbf;

    invoke-virtual {p1}, Lcoy;->a()Lcon;

    move-result-object v1

    sget-object v2, Lbqs;->j:Lbfy;

    sget-object v3, Lbqs;->k:Lbfy;

    invoke-direct {v0, v1, v2, v3}, Lcbf;-><init>(Lcon;Lbfy;Lbfy;)V

    iput-object v0, p0, Lbvt;->g:Lcov;

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lbvt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvy;

    invoke-virtual {v0}, Lbvy;->interrupt()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lbvt;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvy;

    :try_start_0
    invoke-virtual {v0}, Lbvy;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v0}, Lbvy;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FeedProcessorDriver"

    const-string v2, "Producer not cleaned up correctly."

    invoke-static {v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v0, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwa;

    invoke-virtual {v0}, Lbwa;->a()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    if-eqz v0, :cond_2

    :cond_3
    :goto_2
    return-void

    :catch_1
    move-exception v0

    iget-object v0, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "FeedProcessorDriver"

    const-string v1, "Producer not cleaned up correctly."

    invoke-static {v0, v1}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private a(Landroid/content/SyncResult;Lclm;Lbvs;)V
    .locals 8

    invoke-interface {p3, p2}, Lbvs;->b(Lclm;)V

    invoke-interface {p2}, Lclm;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbvt;->f:Lcfz;

    iget-object v2, p0, Lbvt;->g:Lcov;

    invoke-interface {v1, v2}, Lcfz;->a(Lcov;)V

    iget-object v1, p0, Lbvt;->f:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclk;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-interface {p3, p2, v0}, Lbvs;->a(Lclm;Lclk;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    :try_start_2
    iget-object v0, p0, Lbvt;->f:Lcfz;

    iget-object v2, p0, Lbvt;->g:Lcov;

    invoke-interface {v0, v2}, Lcfz;->b(Lcov;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbvt;->f:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :catch_0
    move-exception v2

    :try_start_3
    iget-object v3, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numParseExceptions:J

    const-string v3, "FeedProcessorDriver"

    const-string v4, "Error parsing entry %s."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v2, v4, v5}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_4
    throw v0

    :cond_1
    invoke-interface {p3, p2}, Lbvs;->a(Lclm;)V

    iget-object v0, p0, Lbvt;->f:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lbvt;->f:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;)V
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v7, 0x1

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbue;

    const-string v1, "No feeds to process"

    invoke-direct {v0, v1}, Lbue;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvs;

    invoke-interface {v0}, Lbvs;->a()V

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljdx; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljdy; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljdw; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljdv; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljdz; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljdu; {:try_start_0 .. :try_end_0} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p1, Landroid/content/SyncResult;->databaseError:Z

    new-instance v1, Lbue;

    const-string v2, "Database corrupted"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lbvt;->a()V

    throw v0

    :cond_1
    move v4, v1

    :goto_1
    :try_start_2
    invoke-static {}, Lbta;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "Interrupted during processing"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljdx; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljdy; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljdw; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljdv; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljdz; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljdu; {:try_start_2 .. :try_end_2} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception v0

    :try_start_3
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    new-instance v1, Lbue;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :try_start_4
    iget-object v0, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwa;
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljdx; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljdy; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljdw; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljdv; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljdz; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljdu; {:try_start_4 .. :try_end_4} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v1, v0, Lbwa;->c:Ljava/lang/Exception;

    if-eqz v1, :cond_3

    move v1, v3

    :goto_2
    if-eqz v1, :cond_4

    iget-object v0, v0, Lbwa;->c:Ljava/lang/Exception;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljdx; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljdy; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljdw; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljdv; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljdz; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljdu; {:try_start_6 .. :try_end_6} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/text/ParseException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v0

    :try_start_7
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    new-instance v1, Lbue;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    :try_start_8
    invoke-virtual {v0}, Lbwa;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    add-int/lit8 v1, v4, 0x1

    iget v4, v0, Lbwa;->a:I

    iget-object v6, v0, Lbwa;->b:Lclm;

    if-eqz v6, :cond_5

    iget-object v0, v0, Lbwa;->b:Lclm;

    invoke-interface {v0}, Lclm;->b()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v5, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-ne v1, v0, :cond_1

    :goto_4
    :try_start_9
    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvs;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lbvs;->a(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljdx; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljdy; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljdw; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljdv; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljdz; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljdu; {:try_start_9 .. :try_end_9} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/text/ParseException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_5
    :try_start_a
    iget-object v0, v0, Lbwa;->d:Ljava/lang/String;

    goto :goto_3

    :cond_6
    invoke-static {}, Lbta;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/InterruptedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Interrupted while processing #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lbwa;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    sget-boolean v1, Lbvt;->a:Z

    if-nez v1, :cond_9

    iget v1, v0, Lbwa;->a:I

    if-ltz v1, :cond_8

    iget v1, v0, Lbwa;->a:I

    iget-object v6, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_9
    iget-object v1, p0, Lbvt;->c:Ljava/util/ArrayList;

    iget v6, v0, Lbwa;->a:I

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvs;

    iget-object v0, v0, Lbwa;->b:Lclm;

    invoke-direct {p0, p1, v0, v1}, Lbvt;->a(Landroid/content/SyncResult;Lclm;Lbvs;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    :cond_a
    invoke-direct {p0}, Lbvt;->a()V

    return-void

    :catch_3
    move-exception v0

    :try_start_b
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    throw v0

    :catch_4
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    new-instance v1, Lbue;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_5
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    throw v0

    :catch_6
    move-exception v0

    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v1, v7

    iput-wide v1, v0, Landroid/content/SyncStats;->numIoExceptions:J

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :catch_7
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    throw v0

    :catch_8
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    throw v0

    :catch_9
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    new-instance v1, Lbue;

    const-string v2, "Feed data contains error"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_a
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    throw v0

    :catch_b
    move-exception v0

    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v7

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    new-instance v1, Lbue;

    const-string v2, "Runtime Exception"

    invoke-direct {v1, v2, v0}, Lbue;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_b
    move v4, v2

    goto/16 :goto_1
.end method

.method public final a(Lbuw;Ljava/lang/String;ILbvs;)V
    .locals 7

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lbvt;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v0, p0, Lbvt;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lbvy;

    iget-object v1, p0, Lbvt;->e:Lcoy;

    iget-object v2, p0, Lbvt;->b:Ljava/util/concurrent/BlockingQueue;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lbvy;-><init>(Lcoy;Ljava/util/concurrent/BlockingQueue;Lbuw;Ljava/lang/String;II)V

    invoke-virtual {v0}, Lbvy;->a()V

    iget-object v1, p0, Lbvt;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
