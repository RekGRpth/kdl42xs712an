.class public final Ljev;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:Ljew;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lizs;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljev;->a:J

    const-string v0, ""

    iput-object v0, p0, Ljev;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljev;->c:Ljava/lang/String;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljev;->d:[Ljava/lang/String;

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljev;->e:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Ljev;->f:Ljew;

    const/4 v0, -0x1

    iput v0, p0, Ljev;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v2, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v3, p0, Ljev;->a:J

    const/4 v1, 0x1

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    iget-object v1, p0, Ljev;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    iget-object v3, p0, Ljev;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljev;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    iget-object v3, p0, Ljev;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljev;->d:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljev;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    :goto_0
    iget-object v5, p0, Ljev;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    iget-object v5, p0, Ljev;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    if-eqz v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    invoke-static {v5}, Lizn;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    add-int/2addr v0, v3

    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ljev;->e:[Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljev;->e:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_7

    move v1, v2

    move v3, v2

    :goto_1
    iget-object v4, p0, Ljev;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_6

    iget-object v4, p0, Ljev;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    if-eqz v4, :cond_5

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4}, Lizn;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Ljev;->f:Ljew;

    if-eqz v1, :cond_8

    const/4 v1, 0x6

    iget-object v2, p0, Ljev;->f:Ljew;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Ljev;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->j()J

    move-result-wide v2

    iput-wide v2, p0, Ljev;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljev;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljev;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljev;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljev;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljev;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljev;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Ljev;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljev;->e:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Ljev;->f:Ljew;

    if-nez v0, :cond_7

    new-instance v0, Ljew;

    invoke-direct {v0}, Ljew;-><init>()V

    iput-object v0, p0, Ljev;->f:Ljew;

    :cond_7
    iget-object v0, p0, Ljev;->f:Ljew;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    iget-wide v2, p0, Ljev;->a:J

    invoke-virtual {p1, v2, v3}, Lizn;->a(J)V

    iget-object v0, p0, Ljev;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iget-object v2, p0, Ljev;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ljev;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    iget-object v2, p0, Ljev;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljev;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljev;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljev;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Ljev;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    :goto_1
    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Ljev;->e:[Ljava/lang/String;

    aget-object v0, v0, v1

    if-eqz v0, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Ljev;->f:Ljew;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Ljev;->f:Ljew;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_6
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljev;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljev;

    iget-wide v2, p0, Ljev;->a:J

    iget-wide v4, p1, Ljev;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljev;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljev;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljev;->b:Ljava/lang/String;

    iget-object v3, p1, Ljev;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljev;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Ljev;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljev;->c:Ljava/lang/String;

    iget-object v3, p1, Ljev;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljev;->d:[Ljava/lang/String;

    iget-object v3, p1, Ljev;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljev;->e:[Ljava/lang/String;

    iget-object v3, p1, Ljev;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljev;->f:Ljew;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljev;->f:Ljew;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljev;->f:Ljew;

    iget-object v3, p1, Ljev;->f:Ljew;

    invoke-virtual {v2, v3}, Ljew;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Ljev;->a:J

    iget-wide v4, p0, Ljev;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljev;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljev;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljev;->d:[Ljava/lang/String;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljev;->e:[Ljava/lang/String;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljev;->f:Ljew;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljev;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljev;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ljev;->f:Ljew;

    invoke-virtual {v1}, Ljew;->hashCode()I

    move-result v1

    goto :goto_2
.end method
