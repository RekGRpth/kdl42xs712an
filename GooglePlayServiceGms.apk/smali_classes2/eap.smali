.class public final Leap;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Ledl;

.field private final j:Ldxk;

.field private k:Ljava/util/ArrayList;

.field private l:Ljava/util/HashMap;

.field private m:Ljava/util/HashSet;

.field private final n:I


# direct methods
.method public constructor <init>(Ldvn;Landroid/view/View$OnClickListener;Ledl;I)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Leap;-><init>(Ldvn;Landroid/view/View$OnClickListener;Ledl;Ldxk;I)V

    return-void
.end method

.method private constructor <init>(Ldvn;Landroid/view/View$OnClickListener;Ledl;Ldxk;I)V
    .locals 2

    sget v0, Lxb;->f:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Ldwx;-><init>(Landroid/content/Context;II)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leap;->k:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leap;->l:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Leap;->m:Ljava/util/HashSet;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ldvn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Leap;->g:Landroid/view/LayoutInflater;

    iput-object p2, p0, Leap;->h:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Leap;->i:Ledl;

    const/4 v0, 0x0

    iput-object v0, p0, Leap;->j:Ldxk;

    iput p5, p0, Leap;->n:I

    return-void
.end method

.method static synthetic a(Leap;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Leap;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic b(Leap;)I
    .locals 1

    iget v0, p0, Leap;->n:I

    return v0
.end method

.method static synthetic c(Leap;)Ledl;
    .locals 1

    iget-object v0, p0, Leap;->i:Ledl;

    return-object v0
.end method

.method static synthetic d(Leap;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Leap;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Leap;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Leap;->l:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 2

    check-cast p4, Lcom/google/android/gms/games/Player;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lear;

    invoke-virtual {v0, p4}, Lear;->a(Lcom/google/android/gms/games/Player;)V

    iget-object v0, p0, Leap;->m:Ljava/util/HashSet;

    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Leap;->j:Ldxk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leap;->j:Ldxk;

    :cond_0
    iget-object v0, p0, Leap;->m:Ljava/util/HashSet;

    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Leap;->n:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid tile type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Leap;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Leap;->g:Landroid/view/LayoutInflater;

    sget v1, Lxc;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Leaq;

    invoke-direct {v0, p0, v1}, Leaq;-><init>(Leap;Landroid/view/View;)V

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v1

    :pswitch_1
    iget-object v0, p0, Leap;->g:Landroid/view/LayoutInflater;

    sget v1, Lxc;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Leas;

    invoke-direct {v0, p0, v1}, Leas;-><init>(Leap;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
