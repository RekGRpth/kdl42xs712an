.class public final Leae;
.super Ldwu;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Leac;
.implements Leag;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Lbeh;

.field private B:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

.field private C:Z

.field private final b:Ldvn;

.field private final c:Leac;

.field private final d:Lebk;

.field private final e:Leag;

.field private final f:Ldws;

.field private final g:Ldwt;

.field private h:Ldxi;

.field private final i:Ldxi;

.field private final j:Ldwp;

.field private final k:Leaa;

.field private final l:I

.field private final m:Ldwp;

.field private final n:Lebj;

.field private final o:I

.field private final p:Ldwp;

.field private final q:Leaf;

.field private final r:I

.field private final s:Ldwp;

.field private final t:Leaf;

.field private final u:I

.field private final v:Ldwp;

.field private final w:Leaf;

.field private final x:I

.field private y:Lbdt;

.field private z:Lbeh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Leae;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leae;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldvn;Leac;Leag;Ldwt;Ldws;Lebk;)V
    .locals 9

    invoke-direct {p0}, Ldwu;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Leae;->C:Z

    iput-object p1, p0, Leae;->b:Ldvn;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leac;

    iput-object v0, p0, Leae;->c:Leac;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leag;

    iput-object v0, p0, Leae;->e:Leag;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwt;

    iput-object v0, p0, Leae;->g:Ldwt;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldws;

    iput-object v0, p0, Leae;->f:Ldws;

    iput-object p6, p0, Leae;->d:Lebk;

    new-instance v0, Ldxi;

    sget v2, Lwz;->D:I

    sget v3, Lxf;->aF:I

    sget v4, Lxf;->be:I

    const-string v6, "bannerNewPlayerTileTextTag"

    const-string v7, "bannerNewPlayerButton"

    const/4 v8, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Leae;->h:Ldxi;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Leae;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leae;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->m()Z

    move-result v0

    if-nez v0, :cond_0

    sget v4, Lxf;->U:I

    const-string v6, "bannerNullStateButton"

    move-object v5, p0

    :cond_0
    new-instance v0, Ldxi;

    sget v2, Lwz;->C:I

    sget v3, Lxf;->aD:I

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;)V

    iput-object v0, p0, Leae;->i:Ldxi;

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leae;->j:Ldwp;

    iget-object v0, p0, Leae;->j:Ldwp;

    sget v1, Lxf;->ay:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Leae;->j:Ldwp;

    const-string v1, "invitationsButton"

    invoke-virtual {v0, p0, v1}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    new-instance v0, Leaa;

    sget v1, Lxb;->b:I

    invoke-direct {v0, p1, p0, v1}, Leaa;-><init>(Ldvn;Leac;I)V

    iput-object v0, p0, Leae;->k:Leaa;

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0}, Leaa;->b()V

    iget-object v0, p0, Leae;->k:Leaa;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Leae;->k:Leaa;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Leae;->l:I

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leae;->m:Ldwp;

    iget-object v0, p0, Leae;->m:Ldwp;

    sget v1, Lxf;->aA:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Leae;->m:Ldwp;

    const-string v1, "requestSummariesButton"

    invoke-virtual {v0, p0, v1}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    new-instance v0, Lebj;

    iget-object v1, p0, Leae;->d:Lebk;

    sget v2, Lxb;->d:I

    invoke-direct {v0, p1, v1, v2}, Lebj;-><init>(Ldvn;Lebk;I)V

    iput-object v0, p0, Leae;->n:Lebj;

    iget-object v0, p0, Leae;->n:Lebj;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Leae;->n:Lebj;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Leae;->o:I

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leae;->p:Ldwp;

    iget-object v0, p0, Leae;->p:Ldwp;

    sget v1, Lxf;->az:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Leae;->p:Ldwp;

    const-string v1, "myTurnButton"

    invoke-virtual {v0, p0, v1}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    new-instance v0, Leaf;

    sget v1, Lxb;->c:I

    invoke-direct {v0, p1, p0, v1}, Leaf;-><init>(Ldvn;Leag;I)V

    iput-object v0, p0, Leae;->q:Leaf;

    iget-object v0, p0, Leae;->q:Leaf;

    invoke-virtual {v0}, Leaf;->b()V

    iget-object v0, p0, Leae;->q:Leaf;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Leae;->q:Leaf;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Leae;->r:I

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leae;->s:Ldwp;

    iget-object v0, p0, Leae;->s:Ldwp;

    sget v1, Lxf;->aB:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Leae;->s:Ldwp;

    const-string v1, "theirTurnButton"

    invoke-virtual {v0, p0, v1}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    new-instance v0, Leaf;

    sget v1, Lxb;->e:I

    invoke-direct {v0, p1, p0, v1}, Leaf;-><init>(Ldvn;Leag;I)V

    iput-object v0, p0, Leae;->t:Leaf;

    iget-object v0, p0, Leae;->t:Leaf;

    invoke-virtual {v0}, Leaf;->b()V

    iget-object v0, p0, Leae;->t:Leaf;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Leae;->t:Leaf;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Leae;->u:I

    new-instance v0, Ldwp;

    invoke-direct {v0, p1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Leae;->v:Ldwp;

    iget-object v0, p0, Leae;->v:Ldwp;

    sget v1, Lxf;->ax:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Leae;->v:Ldwp;

    const-string v1, "completedMatchesButton"

    invoke-virtual {v0, p0, v1}, Ldwp;->a(Landroid/view/View$OnClickListener;Ljava/lang/Object;)V

    new-instance v0, Leaf;

    sget v1, Lxb;->a:I

    invoke-direct {v0, p1, p0, v1}, Leaf;-><init>(Ldvn;Leag;I)V

    iput-object v0, p0, Leae;->w:Leaf;

    iget-object v0, p0, Leae;->w:Leaf;

    invoke-virtual {v0}, Leaf;->b()V

    iget-object v0, p0, Leae;->w:Leaf;

    iget v0, v0, Ldwx;->e:I

    iget-object v1, p0, Leae;->w:Leaf;

    iget v1, v1, Ldwx;->f:I

    mul-int/2addr v0, v1

    iput v0, p0, Leae;->x:I

    const/16 v0, 0xc

    new-array v0, v0, [Landroid/widget/BaseAdapter;

    const/4 v1, 0x0

    iget-object v2, p0, Leae;->h:Ldxi;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Leae;->i:Ldxi;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Leae;->j:Ldwp;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Leae;->k:Leaa;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Leae;->m:Ldwp;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Leae;->n:Lebj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Leae;->p:Ldwp;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Leae;->q:Leaf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Leae;->s:Ldwp;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Leae;->t:Leaf;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, Leae;->v:Ldwp;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Leae;->w:Leaf;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Leae;->a([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0}, Leae;->a()V

    return-void
.end method

.method private b()V
    .locals 9

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0}, Leaa;->d()I

    move-result v3

    iget-object v0, p0, Leae;->j:Ldwp;

    iget v4, p0, Leae;->l:I

    invoke-virtual {v0, v3, v4}, Ldwp;->a(II)Z

    move-result v0

    iget-object v4, p0, Leae;->n:Lebj;

    invoke-virtual {v4}, Lebj;->d()I

    move-result v4

    iget-object v5, p0, Leae;->m:Ldwp;

    iget v6, p0, Leae;->o:I

    invoke-virtual {v5, v4, v6}, Ldwp;->a(II)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v2

    :goto_0
    iget-object v4, p0, Leae;->q:Leaf;

    invoke-virtual {v4}, Leaf;->d()I

    move-result v4

    iget-object v5, p0, Leae;->p:Ldwp;

    iget v6, p0, Leae;->r:I

    invoke-virtual {v5, v4, v6}, Ldwp;->a(II)Z

    move-result v5

    if-nez v5, :cond_1

    if-eqz v0, :cond_7

    :cond_1
    move v0, v2

    :goto_1
    iget-object v5, p0, Leae;->t:Leaf;

    invoke-virtual {v5}, Leaf;->d()I

    move-result v5

    iget-object v6, p0, Leae;->s:Ldwp;

    iget v7, p0, Leae;->u:I

    invoke-virtual {v6, v5, v7}, Ldwp;->a(II)Z

    move-result v6

    if-nez v6, :cond_2

    if-eqz v0, :cond_8

    :cond_2
    move v0, v2

    :goto_2
    iget-object v6, p0, Leae;->w:Leaf;

    invoke-virtual {v6}, Leaf;->d()I

    move-result v6

    iget-object v7, p0, Leae;->v:Ldwp;

    iget v8, p0, Leae;->x:I

    invoke-virtual {v7, v6, v8}, Ldwp;->a(II)Z

    move-result v6

    if-nez v6, :cond_3

    if-eqz v0, :cond_9

    :cond_3
    move v0, v2

    :goto_3
    iget-object v6, p0, Leae;->i:Ldxi;

    if-nez v0, :cond_a

    iget-boolean v0, p0, Leae;->C:Z

    if-eqz v0, :cond_a

    :goto_4
    invoke-virtual {v6, v2}, Ldxi;->b(Z)V

    iget-object v0, p0, Leae;->B:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Leae;->C:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Leae;->B:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-static {v0}, Leeh;->a(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Leae;->B:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    :cond_5
    invoke-virtual {p0}, Leae;->notifyDataSetChanged()V

    return-void

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    move v2, v1

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Leae;->n:Lebj;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Leae;->q:Leaf;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Leae;->t:Leaf;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Leae;->w:Leaf;

    invoke-virtual {v0, v2}, Ldve;->a(Lbgo;)V

    iget-object v0, p0, Leae;->h:Ldxi;

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    iget-object v0, p0, Leae;->i:Ldxi;

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    iget-object v0, p0, Leae;->j:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0}, Leaa;->f()V

    iget-object v0, p0, Leae;->m:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    iget-object v0, p0, Leae;->p:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    iget-object v0, p0, Leae;->s:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    iget-object v0, p0, Leae;->v:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    return-void
.end method

.method public final a(Lbdu;)V
    .locals 4

    iget-object v0, p0, Leae;->b:Ldvn;

    iget-object v1, p0, Leae;->b:Ldvn;

    invoke-virtual {v1}, Ldvn;->l()Z

    move-result v1

    invoke-static {p1, v0, v1}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Leae;->y:Lbdt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leae;->y:Lbdt;

    invoke-virtual {v0}, Lbdt;->e()V

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcte;->j:Ldgs;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v1, p1, v2, v3}, Ldgs;->a(Lbdu;I[I)Lbeh;

    move-result-object v1

    iput-object v1, p0, Leae;->z:Lbeh;

    iget-object v1, p0, Leae;->z:Lbeh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    iput-object v1, p0, Leae;->A:Lbeh;

    new-instance v1, Lbdt;

    invoke-direct {v1, v0}, Lbdt;-><init>(Ljava/util/ArrayList;)V

    iput-object v1, p0, Leae;->y:Lbdt;

    iget-object v0, p0, Leae;->y:Lbdt;

    invoke-virtual {v0, p0}, Lbdt;->a(Lbel;)V

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, p1}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Leae;->k:Leaa;

    invoke-virtual {v2, v0, v1}, Leaa;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Leae;->n:Lebj;

    invoke-virtual {v2, v0, v1}, Lebj;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Leae;->q:Leaf;

    invoke-virtual {v2, v0, v1}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Leae;->t:Leaf;

    invoke-virtual {v2, v0, v1}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Leae;->w:Leaf;

    invoke-virtual {v2, v0, v1}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcte;->n:Lctp;

    const/4 v1, 0x3

    invoke-interface {v0, p1, v1}, Lctp;->a(Lbdu;I)V

    goto :goto_0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Leae;->b:Ldvn;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcte;->n:Lctp;

    invoke-interface {v0, p1}, Lctp;->b(Lbdu;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leae;->h:Ldxi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    :cond_1
    iget-object v0, p0, Leae;->y:Lbdt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leae;->y:Lbdt;

    invoke-virtual {v0}, Lbdt;->e()V

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcte;->j:Ldgs;

    sget-object v2, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v1, p1, p2, v2}, Ldgs;->a(Lbdu;Ljava/lang/String;[I)Lbeh;

    move-result-object v1

    iput-object v1, p0, Leae;->z:Lbeh;

    iget-object v1, p0, Leae;->z:Lbeh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Leae;->d:Lebk;

    if-eqz v1, :cond_4

    sget-object v1, Lcte;->o:Ldlf;

    invoke-interface {v1, p1, p3}, Ldlf;->c(Lbdu;Ljava/lang/String;)Lbeh;

    move-result-object v1

    iput-object v1, p0, Leae;->A:Lbeh;

    iget-object v1, p0, Leae;->A:Lbeh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v1, Lbdt;

    invoke-direct {v1, v0}, Lbdt;-><init>(Ljava/util/ArrayList;)V

    iput-object v1, p0, Leae;->y:Lbdt;

    iget-object v0, p0, Leae;->y:Lbdt;

    invoke-virtual {v0, p0}, Lbdt;->a(Lbel;)V

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0, p3, p4}, Leaa;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Leae;->n:Lebj;

    invoke-virtual {v0, p3, p4}, Lebj;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Leae;->q:Leaf;

    invoke-virtual {v0, p3, p4}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Leae;->t:Leaf;

    invoke-virtual {v0, p3, p4}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Leae;->w:Leaf;

    invoke-virtual {v0, p3, p4}, Leaf;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x3

    iget-object v1, p0, Leae;->d:Lebk;

    if-eqz v1, :cond_3

    const/4 v0, 0x7

    :cond_3
    sget-object v1, Lcte;->n:Lctp;

    invoke-interface {v1, p1, p2, v0}, Lctp;->a(Lbdu;Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, Leae;->A:Lbeh;

    goto :goto_1
.end method

.method public final synthetic a(Lbek;)V
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Leae;->z:Lbeh;

    if-eqz v0, :cond_16

    iget-object v0, p0, Leae;->z:Lbeh;

    invoke-interface {v0}, Lbeh;->b()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leae;->z:Lbeh;

    invoke-interface {v0}, Lbeh;->c()Lbek;

    move-result-object v0

    check-cast v0, Ldgu;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Leae;->A:Lbeh;

    if-eqz v0, :cond_15

    iget-object v0, p0, Leae;->A:Lbeh;

    invoke-interface {v0}, Lbeh;->b()Z

    move-result v0

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Leae;->A:Lbeh;

    invoke-interface {v0}, Lbeh;->c()Lbek;

    move-result-object v0

    check-cast v0, Ldlg;

    move-object v6, v0

    :goto_1
    if-nez v1, :cond_5

    move v5, v4

    :goto_2
    if-nez v6, :cond_6

    move v2, v4

    :goto_3
    if-eqz v1, :cond_14

    invoke-interface {v1}, Ldgu;->g()Ldgo;

    move-result-object v0

    iget-object v7, v0, Ldgo;->a:Ldfu;

    if-eqz v7, :cond_7

    iget-object v7, v0, Ldgo;->a:Ldfu;

    invoke-virtual {v7}, Ldfu;->a()I

    move-result v7

    if-lez v7, :cond_7

    move v0, v3

    :goto_4
    or-int/lit8 v0, v0, 0x0

    :goto_5
    if-eqz v6, :cond_1

    invoke-interface {v6}, Ldlg;->g()Ldlc;

    move-result-object v7

    invoke-virtual {v7}, Ldlc;->a()I

    move-result v7

    if-lez v7, :cond_0

    move v4, v3

    :cond_0
    or-int/2addr v0, v4

    :cond_1
    :try_start_0
    iget-object v4, p0, Leae;->b:Ldvn;

    invoke-virtual {v4, v5}, Ldvn;->d(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Leae;->b:Ldvn;

    invoke-virtual {v4, v2}, Ldvn;->d(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_b

    :cond_2
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ldgu;->g()Ldgo;

    move-result-object v0

    invoke-virtual {v0}, Ldgo;->a()V

    :cond_3
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ldlg;->g()Ldlc;

    move-result-object v0

    invoke-virtual {v0}, Ldlc;->b()V

    :cond_4
    :goto_6
    return-void

    :cond_5
    invoke-interface {v1}, Ldgu;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    move v5, v0

    goto :goto_2

    :cond_6
    invoke-interface {v6}, Ldlg;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    move v2, v0

    goto :goto_3

    :cond_7
    iget-object v7, v0, Ldgo;->b:Ldgq;

    if-eqz v7, :cond_8

    iget-object v7, v0, Ldgo;->b:Ldgq;

    invoke-virtual {v7}, Ldgq;->a()I

    move-result v7

    if-lez v7, :cond_8

    move v0, v3

    goto :goto_4

    :cond_8
    iget-object v7, v0, Ldgo;->c:Ldgq;

    if-eqz v7, :cond_9

    iget-object v7, v0, Ldgo;->c:Ldgq;

    invoke-virtual {v7}, Ldgq;->a()I

    move-result v7

    if-lez v7, :cond_9

    move v0, v3

    goto :goto_4

    :cond_9
    iget-object v7, v0, Ldgo;->d:Ldgq;

    if-eqz v7, :cond_a

    iget-object v0, v0, Ldgo;->d:Ldgq;

    invoke-virtual {v0}, Ldgq;->a()I

    move-result v0

    if-lez v0, :cond_a

    move v0, v3

    goto :goto_4

    :cond_a
    move v0, v4

    goto :goto_4

    :cond_b
    if-nez v0, :cond_e

    :try_start_1
    invoke-static {v5}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Leae;->f:Ldws;

    if-eqz v0, :cond_c

    iget-object v0, p0, Leae;->f:Ldws;

    invoke-interface {v0}, Ldws;->z_()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_c
    if-eqz v1, :cond_d

    invoke-interface {v1}, Ldgu;->g()Ldgo;

    move-result-object v0

    invoke-virtual {v0}, Ldgo;->a()V

    :cond_d
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ldlg;->g()Ldlc;

    move-result-object v0

    invoke-virtual {v0}, Ldlc;->b()V

    goto :goto_6

    :cond_e
    :try_start_2
    iget-object v0, p0, Leae;->z:Lbeh;

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ldgu;->g()Ldgo;

    move-result-object v0

    iget-object v2, v0, Ldgo;->a:Ldfu;

    new-instance v4, Ldzz;

    invoke-direct {v4, v2}, Ldzz;-><init>(Lbgo;)V

    invoke-virtual {v2}, Ldfu;->b()V

    iget-object v2, p0, Leae;->k:Leaa;

    invoke-virtual {v2, v4}, Leaa;->a(Ldzz;)V

    iget-object v2, p0, Leae;->q:Leaf;

    iget-object v4, v0, Ldgo;->b:Ldgq;

    invoke-virtual {v2, v4}, Leaf;->a(Lbgo;)V

    iget-object v2, p0, Leae;->t:Leaf;

    iget-object v4, v0, Ldgo;->c:Ldgq;

    invoke-virtual {v2, v4}, Leaf;->a(Lbgo;)V

    iget-object v2, p0, Leae;->w:Leaf;

    iget-object v0, v0, Ldgo;->d:Ldgq;

    invoke-virtual {v2, v0}, Leaf;->a(Lbgo;)V

    :cond_f
    iget-object v0, p0, Leae;->A:Lbeh;

    if-eqz v0, :cond_10

    iget-object v0, p0, Leae;->n:Lebj;

    invoke-interface {v6}, Ldlg;->g()Ldlc;

    move-result-object v2

    invoke-virtual {v0, v2}, Lebj;->a(Lbgo;)V

    :cond_10
    iget-object v0, p0, Leae;->f:Ldws;

    if-eqz v0, :cond_11

    iget-object v0, p0, Leae;->f:Ldws;

    invoke-interface {v0}, Ldws;->y_()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_11
    iput-boolean v3, p0, Leae;->C:Z

    invoke-direct {p0}, Leae;->b()V

    goto/16 :goto_6

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_12

    invoke-interface {v1}, Ldgu;->g()Ldgo;

    move-result-object v1

    invoke-virtual {v1}, Ldgo;->a()V

    :cond_12
    if-eqz v6, :cond_13

    invoke-interface {v6}, Ldlg;->g()Ldlc;

    move-result-object v1

    invoke-virtual {v1}, Ldlc;->b()V

    :cond_13
    throw v0

    :cond_14
    move v0, v4

    goto/16 :goto_5

    :cond_15
    move-object v6, v2

    goto/16 :goto_1

    :cond_16
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1, p2, p3}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Leae;->k:Leaa;

    invoke-virtual {v0, p1, p2, p3}, Leaa;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1, p2, p3}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    iget-object v0, p0, Leae;->e:Leag;

    invoke-interface {v0, p1}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Leae;->e:Leag;

    invoke-interface {v0, p1, p2, p3}, Leag;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;)V
    .locals 0

    iput-object p1, p0, Leae;->B:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->a_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->b(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    iget-object v0, p0, Leae;->e:Leag;

    invoke-interface {v0, p1}, Leag;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    iget-object v0, p0, Leae;->e:Leag;

    invoke-interface {v0, p1}, Leag;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Leae;->c:Leac;

    invoke-interface {v0, p1}, Leac;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Leae;->b()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    const-string v2, "bannerNewPlayerTileTextTag"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Leae;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->h()V

    :cond_0
    :goto_0
    sget-object v0, Leae;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const-string v2, "bannerNewPlayerButton"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Leae;->h:Ldxi;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldxi;->b(Z)V

    iget-object v0, p0, Leae;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v0, Leae;->a:Ljava/lang/String;

    const-string v2, "setUseNewPlayerNotifications: not connected; ignoring..."

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcte;->n:Lctp;

    invoke-interface {v2, v0}, Lctp;->c(Lbdu;)V

    goto :goto_0

    :cond_3
    const-string v2, "bannerNullStateButton"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, p0, Leae;->b:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Ldvn;->m()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.games.destination.VIEW_SHOP_GAMES_LIST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.games.TAB"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Ldvn;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v0, "UiUtils"

    const-string v2, "showPopularMultiplayer: Trying to show Popular Multiplayer screen when not in the destination app"

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Leae;->g:Ldwt;

    invoke-interface {v1, v0}, Ldwt;->d_(Ljava/lang/String;)V

    goto :goto_1
.end method
