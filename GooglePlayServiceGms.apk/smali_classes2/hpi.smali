.class public final enum Lhpi;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhpi;

.field public static final enum b:Lhpi;

.field private static final synthetic c:[Lhpi;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhpi;

    const-string v1, "GMM"

    invoke-direct {v0, v1, v2}, Lhpi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpi;->a:Lhpi;

    new-instance v0, Lhpi;

    const-string v1, "GMS"

    invoke-direct {v0, v1, v3}, Lhpi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhpi;->b:Lhpi;

    const/4 v0, 0x2

    new-array v0, v0, [Lhpi;

    sget-object v1, Lhpi;->a:Lhpi;

    aput-object v1, v0, v2

    sget-object v1, Lhpi;->b:Lhpi;

    aput-object v1, v0, v3

    sput-object v0, Lhpi;->c:[Lhpi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhpi;
    .locals 1

    const-class v0, Lhpi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhpi;

    return-object v0
.end method

.method public static values()[Lhpi;
    .locals 1

    sget-object v0, Lhpi;->c:[Lhpi;

    invoke-virtual {v0}, [Lhpi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhpi;

    return-object v0
.end method
