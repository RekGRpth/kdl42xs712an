.class final Lhdk;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhdj;


# direct methods
.method constructor <init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhdk;->f:Lhdj;

    iput-object p4, p0, Lhdk;->d:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    iput-object p5, p0, Lhdk;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/4 v0, 0x3

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 11

    iget-object v0, p0, Lhdk;->d:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->b()Lipa;

    move-result-object v0

    iget-object v1, v0, Lipa;->b:Linv;

    iget v2, v1, Linv;->a:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create instrument currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v2, v1, Linv;->b:Lint;

    iget-object v2, v2, Lint;->a:Linu;

    iget-object v6, v2, Linu;->a:Ljava/lang/String;

    iget-object v1, v1, Linv;->b:Lint;

    iget-object v1, v1, Lint;->a:Linu;

    iget-object v7, v1, Linu;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v5

    check-cast v5, Lipa;

    iget-object v0, v5, Lipa;->b:Linv;

    iget-object v0, v0, Linv;->b:Lint;

    const/4 v1, 0x0

    iput-object v1, v0, Lint;->a:Linu;

    iget-object v0, p0, Lhdk;->f:Lhdj;

    invoke-static {v0}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v0, p0, Lhdk;->f:Lhdj;

    iget-object v0, p0, Lhdk;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhdk;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lhdk;->a:Landroid/accounts/Account;

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdk;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v9

    iget-object v0, v5, Lipa;->b:Linv;

    invoke-static {v0}, Lhcv;->a(Linv;)V

    iget-object v10, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhdc;

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lhdc;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Lhgm;Lipa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_instrument"

    invoke-static {v10, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
