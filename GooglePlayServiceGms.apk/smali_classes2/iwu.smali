.class final Liwu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liws;


# instance fields
.field final synthetic a:Liwp;

.field private b:I


# direct methods
.method constructor <init>(Liwp;)V
    .locals 1

    iput-object p1, p0, Liwu;->a:Liwp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Liwu;->b:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final a(J)Liws;
    .locals 8

    iget v0, p0, Liwu;->b:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    new-instance v0, Liwt;

    iget-object v1, p0, Liwu;->a:Liwp;

    invoke-direct {v0, v1}, Liwt;-><init>(Liwp;)V

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Liwu;->a:Liwp;

    iget-wide v0, v0, Liwp;->e:J

    sub-long v0, p1, v0

    long-to-double v2, v0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    iget-object v6, p0, Liwu;->a:Liwp;

    iget-wide v6, v6, Liwp;->d:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const-wide v2, 0x37e11d600L

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Liwu;->a:Liwp;

    iget-wide v0, v0, Liwp;->g:J

    iget-object v2, p0, Liwu;->a:Liwp;

    iget-wide v2, v2, Liwp;->e:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Liwu;->a:Liwp;

    iget-wide v0, v0, Liwp;->h:J

    iget-object v2, p0, Liwu;->a:Liwp;

    iget-wide v2, v2, Liwp;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    :cond_2
    new-instance v0, Liwr;

    iget-object v1, p0, Liwu;->a:Liwp;

    invoke-direct {v0, v1}, Liwr;-><init>(Liwp;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public final a(JLiwe;)V
    .locals 0

    return-void
.end method

.method public final a(Liwz;)V
    .locals 2

    sget-object v0, Liwq;->b:[I

    invoke-virtual {p1}, Liwz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Liwu;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Liwu;->b:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Liwu;->b:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Liwy;
    .locals 1

    iget-object v0, p0, Liwu;->a:Liwp;

    iget-object v0, v0, Liwp;->c:Liwg;

    return-object v0
.end method
