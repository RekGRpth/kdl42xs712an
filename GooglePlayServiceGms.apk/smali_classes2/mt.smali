.class public final Lmt;
.super Landroid/database/DataSetObserver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lms;

.field private b:Landroid/os/Parcelable;


# direct methods
.method public constructor <init>(Lms;)V
    .locals 1

    iput-object p1, p0, Lmt;->a:Lms;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmt;->b:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 2

    iget-object v0, p0, Lmt;->a:Lms;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lms;->u:Z

    iget-object v0, p0, Lmt;->a:Lms;

    iget-object v1, p0, Lmt;->a:Lms;

    iget v1, v1, Lms;->z:I

    iput v1, v0, Lms;->A:I

    iget-object v0, p0, Lmt;->a:Lms;

    iget-object v1, p0, Lmt;->a:Lms;

    invoke-virtual {v1}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    iput v1, v0, Lms;->z:I

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmt;->b:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmt;->a:Lms;

    iget v0, v0, Lms;->A:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lmt;->a:Lms;

    iget v0, v0, Lms;->z:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lmt;->a:Lms;

    iget-object v1, p0, Lmt;->b:Landroid/os/Parcelable;

    invoke-static {v0, v1}, Lms;->a(Lms;Landroid/os/Parcelable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmt;->b:Landroid/os/Parcelable;

    :goto_0
    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->e()V

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->requestLayout()V

    return-void

    :cond_0
    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->h()V

    goto :goto_0
.end method

.method public final onInvalidated()V
    .locals 6

    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lmt;->a:Lms;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lms;->u:Z

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->c()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-static {v0}, Lms;->a(Lms;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lmt;->b:Landroid/os/Parcelable;

    :cond_0
    iget-object v0, p0, Lmt;->a:Lms;

    iget-object v1, p0, Lmt;->a:Lms;

    iget v1, v1, Lms;->z:I

    iput v1, v0, Lms;->A:I

    iget-object v0, p0, Lmt;->a:Lms;

    iput v3, v0, Lms;->z:I

    iget-object v0, p0, Lmt;->a:Lms;

    iput v2, v0, Lms;->x:I

    iget-object v0, p0, Lmt;->a:Lms;

    iput-wide v4, v0, Lms;->y:J

    iget-object v0, p0, Lmt;->a:Lms;

    iput v2, v0, Lms;->v:I

    iget-object v0, p0, Lmt;->a:Lms;

    iput-wide v4, v0, Lms;->w:J

    iget-object v0, p0, Lmt;->a:Lms;

    iput-boolean v3, v0, Lms;->p:Z

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->e()V

    iget-object v0, p0, Lmt;->a:Lms;

    invoke-virtual {v0}, Lms;->requestLayout()V

    return-void
.end method
