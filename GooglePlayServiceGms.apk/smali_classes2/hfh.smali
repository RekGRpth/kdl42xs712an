.class final Lhfh;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhfd;


# direct methods
.method constructor <init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhfh;->f:Lhfd;

    iput-object p4, p0, Lhfh;->d:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    iput-object p5, p0, Lhfh;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/16 v0, 0xa

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v0, p0, Lhfh;->d:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c()Ljao;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v3

    check-cast v3, Ljao;

    invoke-static {}, Lhfx;->a()Ljbf;

    move-result-object v0

    iput-object v0, v3, Ljao;->c:Ljbf;

    iget-object v0, p0, Lhfh;->f:Lhfd;

    invoke-static {v0}, Lhfd;->a(Lhfd;)Lgtm;

    move-result-object v0

    iget-object v1, p0, Lhfh;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lhfh;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgtm;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, v3, Ljao;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhfh;->f:Lhfd;

    invoke-static {v0}, Lhfd;->c(Lhfd;)Lhfo;

    move-result-object v1

    iget-object v0, p0, Lhfh;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhfd;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lhfh;->d:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v1, Lhfo;->a:Landroid/content/Context;

    new-instance v0, Lhfu;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhfu;-><init>(Lhfo;Ljava/lang/String;Ljao;Ljava/lang/String;Lhgm;)V

    const-string v1, "authenticate_instrument"

    invoke-static {v6, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
