.class final Lfse;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/res/Resources;Ljava/util/Locale;ZILjava/util/ArrayList;)Lfsf;
    .locals 8

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    rsub-int/lit8 v0, v0, 0x4

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    new-instance v2, Lfsf;

    invoke-direct {v2}, Lfsf;-><init>()V

    if-nez p2, :cond_2

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0, p1, p3}, Lfse;->a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->d:Ljava/lang/String;

    if-nez p3, :cond_1

    const v0, 0x7f0b033f    # com.google.android.gms.R.string.plus_one_annotation_none

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->a:Ljava/lang/String;

    const v0, 0x7f0b0340    # com.google.android.gms.R.string.plus_one_annotation_none_short

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->b:Ljava/lang/String;

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const v0, 0x7f0b0341    # com.google.android.gms.R.string.plus_one_annotation_count_prefix

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const v4, 0x7f0b033f    # com.google.android.gms.R.string.plus_one_annotation_none

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->a:Ljava/lang/String;

    const v0, 0x7f0b0341    # com.google.android.gms.R.string.plus_one_annotation_count_prefix

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const v4, 0x7f0b0340    # com.google.android.gms.R.string.plus_one_annotation_none_short

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->b:Ljava/lang/String;

    const v0, 0x7f0b0342    # com.google.android.gms.R.string.plus_one_annotation_count_only

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->c:Ljava/lang/String;

    move-object v0, v2

    goto :goto_1

    :cond_2
    if-eqz p2, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, v0

    if-ge p3, v1, :cond_4

    const-string v0, "PlusOneStrings"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v3, "PlusOneStrings"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "Global count is "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " but +1 has been "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p2, :cond_6

    const-string v0, "set by viewer"

    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p2, :cond_7

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, " and "

    :goto_4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "+1\'d by "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " friend(s)"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move p3, v1

    :cond_4
    invoke-static {p0, p1, p3}, Lfse;->a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->d:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p2, :cond_a

    if-eqz p2, :cond_9

    const/4 v0, 0x1

    if-ne p3, v0, :cond_9

    const v0, 0x7f0b0343    # com.google.android.gms.R.string.plus_one_social_annotation_suffix

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0x7f0b0344    # com.google.android.gms.R.string.plus_one_social_annotation_user

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->a:Ljava/lang/String;

    const v0, 0x7f0b0344    # com.google.android.gms.R.string.plus_one_social_annotation_user

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->b:Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_6
    const-string v0, ""

    goto :goto_3

    :cond_7
    const-string v0, ""

    goto :goto_4

    :cond_8
    const-string v0, ""

    goto :goto_5

    :cond_9
    const v0, 0x7f0b0346    # com.google.android.gms.R.string.plus_one_social_annotation_prefix_you

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v5, 0x7f0b0344    # com.google.android.gms.R.string.plus_one_social_annotation_user

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_a
    const/4 v1, 0x0

    move-object v3, v0

    :goto_6
    if-ge v1, v4, :cond_c

    invoke-virtual {p4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v3, :cond_b

    const v3, 0x7f0b0347    # com.google.android.gms.R.string.plus_one_social_annotation_prefix

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {p0, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_7
    add-int/lit8 v1, v1, 0x1

    move-object v3, v0

    goto :goto_6

    :cond_b
    const v5, 0x7f0b0345    # com.google.android.gms.R.string.plus_one_social_annotation_separator

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v0, v6, v3

    invoke-virtual {p0, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    :cond_c
    const v0, 0x7f0b0341    # com.google.android.gms.R.string.plus_one_annotation_count_prefix

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    aput-object v3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->a:Ljava/lang/String;

    const v0, 0x7f0b0342    # com.google.android.gms.R.string.plus_one_annotation_count_only

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lfsf;->b:Ljava/lang/String;

    move-object v0, v2

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/res/Resources;Ljava/util/Locale;I)Ljava/lang/String;
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ltz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Count must be non-negative."

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/MathContext;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Ljava/math/MathContext;-><init>(I)V

    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v4

    const/16 v5, 0x3e8

    if-ge p2, v5, :cond_1

    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x6

    if-gt v3, v5, :cond_2

    const v3, 0x7f0b033b    # com.google.android.gms.R.string.plus_one_count_in_the_thousands

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v5, v0

    const-wide v7, 0x408f400000000000L    # 1000.0

    div-double/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/16 v5, 0x9

    if-gt v3, v5, :cond_3

    const v3, 0x7f0b033c    # com.google.android.gms.R.string.plus_one_count_in_the_millions

    new-array v1, v1, [Ljava/lang/Object;

    int-to-double v5, v0

    const-wide v7, 0x412e848000000000L    # 1000000.0

    div-double/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const v0, 0x7f0b033d    # com.google.android.gms.R.string.plus_one_count_more_than_a_billion

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    const/16 v0, 0x2710

    if-ge p2, v0, :cond_5

    int-to-long v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const v0, 0x7f0b033e    # com.google.android.gms.R.string.plus_one_count_many

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
