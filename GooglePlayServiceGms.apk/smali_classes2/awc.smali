.class public final Lawc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final p:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final a:Lawd;

.field private b:Ljava/nio/channels/SocketChannel;

.field private c:Lawg;

.field private d:Lawg;

.field private final e:Lawe;

.field private final f:I

.field private g:I

.field private h:Ljava/net/InetSocketAddress;

.field private i:J

.field private j:J

.field private k:J

.field private l:J

.field private m:Z

.field private n:Lawp;

.field private final o:Laye;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lawc;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lawd;)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lbbj;

    const-string v1, "CastSocket"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lawc;->o:Laye;

    iget-object v0, p0, Lawc;->o:Laye;

    const-string v1, "instance-%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Lawc;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laye;->a(Ljava/lang/String;)V

    iput-object p2, p0, Lawc;->a:Lawd;

    iput v4, p0, Lawc;->g:I

    const v0, 0x1fffc

    iput v0, p0, Lawc;->f:I

    sget-object v0, Lawe;->a:Lawe;

    if-nez v0, :cond_2

    new-instance v0, Lawe;

    invoke-direct {v0, p1}, Lawe;-><init>(Landroid/content/Context;)V

    sput-object v0, Lawe;->a:Lawe;

    :cond_2
    sget-object v0, Lawe;->a:Lawe;

    iput-object v0, p0, Lawc;->e:Lawe;

    const/4 v0, 0x0

    iput-object v0, p0, Lawc;->n:Lawp;

    return-void
.end method

.method private a(I)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lawc;->o:Laye;

    const-string v3, "doTeardown with reason=%d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v6, p0, Lawc;->n:Lawp;

    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v6, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    :cond_0
    iput-object v6, p0, Lawc;->c:Lawg;

    iput-object v6, p0, Lawc;->d:Lawg;

    iget v0, p0, Lawc;->g:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput v2, p0, Lawc;->g:I

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lawc;->k:J

    iput-wide v2, p0, Lawc;->i:J

    iput-boolean v1, p0, Lawc;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lawc;->a:Lawd;

    invoke-interface {v0, p1}, Lawd;->a(I)V

    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lawc;->a:Lawd;

    invoke-interface {v0, p1}, Lawd;->b(I)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized b(Ljava/net/Inet4Address;I)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lawc;->e:Lawe;

    invoke-virtual {v0}, Lawe;->a()V

    iget-object v0, p0, Lawc;->o:Laye;

    const-string v1, "Connecting to %s:%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v0, p0, Lawc;->h:Ljava/net/InetSocketAddress;

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, Lawc;->j:J

    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lawc;->l:J

    iget-object v0, p0, Lawc;->e:Lawe;

    invoke-virtual {v0, p0}, Lawe;->a(Lawc;)V

    const/4 v0, 0x1

    iput v0, p0, Lawc;->g:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lawc;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private m()V
    .locals 9

    const/4 v2, 0x1

    const/4 v8, -0x1

    const/4 v1, 0x0

    :goto_0
    iget-object v0, p0, Lawc;->c:Lawg;

    iget-boolean v0, v0, Lawg;->e:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lawc;->c:Lawg;

    iget v3, v0, Lawg;->b:I

    iput v3, v0, Lawg;->d:I

    iget-object v0, p0, Lawc;->c:Lawg;

    invoke-virtual {v0}, Lawg;->d()I

    move-result v3

    const/4 v4, 0x4

    if-ge v3, v4, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_9

    iget-object v0, p0, Lawc;->c:Lawg;

    iget v2, v0, Lawg;->d:I

    if-eq v2, v8, :cond_1

    iget v2, v0, Lawg;->b:I

    iget v3, v0, Lawg;->d:I

    if-eq v2, v3, :cond_0

    iget v2, v0, Lawg;->d:I

    iput v2, v0, Lawg;->b:I

    iput-boolean v1, v0, Lawg;->e:Z

    :cond_0
    iput v8, v0, Lawg;->d:I

    :cond_1
    :goto_3
    return-void

    :cond_2
    invoke-virtual {v0}, Lawg;->f()B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    int-to-long v3, v3

    const/16 v5, 0x18

    shl-long/2addr v3, v5

    invoke-virtual {v0}, Lawg;->f()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-long v5, v5

    const/16 v7, 0x10

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    invoke-virtual {v0}, Lawg;->f()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    int-to-long v5, v5

    const/16 v7, 0x8

    shl-long/2addr v5, v7

    or-long/2addr v3, v5

    invoke-virtual {v0}, Lawg;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    int-to-long v5, v0

    or-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget v5, p0, Lawc;->f:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid message size received"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v3, p0, Lawc;->c:Lawg;

    invoke-virtual {v3}, Lawg;->d()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_5

    move v0, v2

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-int v0, v3

    new-array v3, v0, [B

    iget-object v4, p0, Lawc;->c:Lawg;

    array-length v5, v3

    invoke-virtual {v4}, Lawg;->d()I

    move-result v0

    if-lt v0, v5, :cond_6

    iget-boolean v0, v4, Lawg;->e:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v6, v4, Lawg;->a:[B

    iget v7, v4, Lawg;->b:I

    invoke-static {v6, v7, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v4, v0}, Lawg;->a(I)V

    sub-int/2addr v5, v0

    if-lez v5, :cond_6

    add-int/lit8 v0, v0, 0x0

    iget-object v6, v4, Lawg;->a:[B

    iget v7, v4, Lawg;->b:I

    invoke-static {v6, v7, v3, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v4, v5}, Lawg;->a(I)V

    :cond_6
    iget-object v0, p0, Lawc;->a:Lawd;

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-interface {v0, v3}, Lawd;->a(Ljava/nio/ByteBuffer;)V

    goto/16 :goto_0

    :cond_7
    iget v0, v4, Lawg;->b:I

    iget v6, v4, Lawg;->c:I

    if-ge v0, v6, :cond_8

    iget v0, v4, Lawg;->c:I

    iget v6, v4, Lawg;->b:I

    sub-int/2addr v0, v6

    goto :goto_4

    :cond_8
    iget-object v0, v4, Lawg;->a:[B

    array-length v0, v0

    iget v6, v4, Lawg;->b:I

    sub-int/2addr v0, v6

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lawc;->c:Lawg;

    iput v8, v0, Lawg;->d:I

    iget-boolean v2, v0, Lawg;->e:Z

    if-eqz v2, :cond_1

    iput v1, v0, Lawg;->c:I

    iput v1, v0, Lawg;->b:I

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method


# virtual methods
.method final declared-synchronized a()Ljava/nio/channels/SocketChannel;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lawc;->o:Laye;

    const-string v1, "startConnect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lawc;->i:J

    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    iput-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/net/Socket;->setSoLinger(ZI)V

    new-instance v0, Lawg;

    invoke-direct {v0}, Lawg;-><init>()V

    iput-object v0, p0, Lawc;->c:Lawg;

    new-instance v0, Lawg;

    invoke-direct {v0}, Lawg;-><init>()V

    iput-object v0, p0, Lawc;->d:Lawg;

    new-instance v0, Lawp;

    iget-object v1, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    iget-object v2, p0, Lawc;->c:Lawg;

    iget-object v3, p0, Lawc;->d:Lawg;

    invoke-direct {v0, v1, v2, v3}, Lawp;-><init>(Ljava/nio/channels/SocketChannel;Lawg;Lawg;)V

    iput-object v0, p0, Lawc;->n:Lawp;

    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    iget-object v1, p0, Lawc;->h:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawc;->n:Lawp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->b()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x2

    :try_start_1
    iput v0, p0, Lawc;->g:I

    iget-object v0, p0, Lawc;->a:Lawd;

    invoke-interface {v0}, Lawd;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/net/Inet4Address;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lawc;->b(Ljava/net/Inet4Address;I)V

    return-void
.end method

.method public final declared-synchronized a(Ljava/nio/ByteBuffer;)V
    .locals 7

    const-wide/16 v5, 0xff

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lawc;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not connected; state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lawc;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-nez p1, :cond_1

    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "message cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lawc;->d:Lawg;

    invoke-virtual {v0}, Lawg;->c()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    if-ge v0, v1, :cond_2

    new-instance v0, Lavz;

    invoke-direct {v0}, Lavz;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lawc;->d:Lawg;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0}, Lawg;->c()I

    move-result v3

    const/4 v4, 0x4

    if-lt v3, v4, :cond_5

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-ltz v3, :cond_3

    const-wide v3, 0xffffffffL

    cmp-long v3, v1, v3

    if-lez v3, :cond_4

    :cond_3
    new-instance v0, Lawo;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid uint32 value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lawo;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/16 v3, 0x18

    shr-long v3, v1, v3

    and-long/2addr v3, v5

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lawg;->a(B)V

    const/16 v3, 0x10

    shr-long v3, v1, v3

    and-long/2addr v3, v5

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lawg;->a(B)V

    const/16 v3, 0x8

    shr-long v3, v1, v3

    and-long/2addr v3, v5

    long-to-int v3, v3

    int-to-byte v3, v3

    invoke-virtual {v0, v3}, Lawg;->a(B)V

    and-long/2addr v1, v5

    long-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lawg;->a(B)V

    :cond_5
    iget-object v1, p0, Lawc;->d:Lawg;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v1}, Lawg;->c()I

    move-result v0

    if-lt v0, v4, :cond_6

    iget-boolean v0, v1, Lawg;->e:Z

    if-eqz v0, :cond_7

    iget-object v0, v1, Lawg;->a:[B

    array-length v0, v0

    :goto_0
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v5, v1, Lawg;->a:[B

    iget v6, v1, Lawg;->c:I

    invoke-static {v2, v3, v5, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v1, v0}, Lawg;->b(I)V

    add-int/2addr v3, v0

    sub-int v0, v4, v0

    if-lez v0, :cond_6

    iget-object v4, v1, Lawg;->a:[B

    iget v5, v1, Lawg;->c:I

    invoke-static {v2, v3, v4, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v1, v0}, Lawg;->b(I)V

    :cond_6
    iget-object v0, p0, Lawc;->e:Lawe;

    invoke-virtual {v0}, Lawe;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_7
    :try_start_2
    iget v0, v1, Lawg;->c:I

    iget v5, v1, Lawg;->b:I

    if-ge v0, v5, :cond_8

    iget v0, v1, Lawg;->b:I

    iget v5, v1, Lawg;->c:I

    sub-int/2addr v0, v5

    goto :goto_0

    :cond_8
    iget-object v0, v1, Lawg;->a:[B

    array-length v0, v0

    iget v5, v1, Lawg;->c:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-int/2addr v0, v5

    goto :goto_0
.end method

.method final declared-synchronized a(Ljava/nio/channels/SelectionKey;J)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lawc;->m:Z

    if-eqz v2, :cond_0

    iget-object v1, p0, Lawc;->o:Laye;

    const-string v2, "Socket is no longer connected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lawc;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget v2, p0, Lawc;->g:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-wide v2, p0, Lawc;->i:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lawc;->j:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v2, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v0, 0x8

    :cond_2
    :goto_1
    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lawc;->n:Lawp;

    if-eqz v2, :cond_2

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->d()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lawc;->n:Lawp;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->d()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lawc;->c:Lawg;

    invoke-virtual {v2}, Lawg;->e()Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    :cond_5
    iget-object v2, p0, Lawc;->d:Lawg;

    iget-boolean v2, v2, Lawg;->e:Z

    if-nez v2, :cond_2

    or-int/lit8 v0, v0, 0x4

    goto :goto_1

    :pswitch_2
    iget-wide v2, p0, Lawc;->k:J

    sub-long v2, p2, v2

    iget-wide v4, p0, Lawc;->l:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lawc;->a(I)V

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lawc;->n:Lawp;

    if-eqz v2, :cond_7

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->d()I

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lawc;->d:Lawg;

    iget-boolean v2, v2, Lawg;->e:Z

    if-nez v2, :cond_8

    const/4 v0, 0x4

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized b()V
    .locals 5

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    iput v0, p0, Lawc;->g:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lawc;->k:J

    iget-object v0, p0, Lawc;->n:Lawp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->c()V

    :cond_0
    iget-object v0, p0, Lawc;->o:Laye;

    const-string v1, "disconnect. isWriteBufferEmpty: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lawc;->d:Lawg;

    iget-boolean v4, v4, Lawg;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lawc;->e:Lawe;

    invoke-virtual {v0}, Lawe;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lawc;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 2

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lawc;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v0, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lawc;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lawc;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()[B
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lawc;->n:Lawp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawc;->n:Lawp;

    invoke-virtual {v0}, Lawp;->g()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized h()Z
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lawc;->o:Laye;

    const-string v2, "onConnectable"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    iget-object v1, p0, Lawc;->n:Lawp;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lawc;->n:Lawp;

    invoke-virtual {v1}, Lawp;->b()V
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v0, 0x1

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    const/4 v1, 0x2

    :try_start_2
    iput v1, p0, Lawc;->g:I

    iget-object v1, p0, Lawc;->a:Lawd;

    invoke-interface {v1}, Lawd;->a()V
    :try_end_2
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "exception in onConnectable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "exception in onConnectable"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized i()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lawc;->o:Laye;

    const-string v1, "onConnectError"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lawc;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized j()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lawc;->n:Lawp;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lawc;->n:Lawp;

    invoke-virtual {v2}, Lawp;->e()V

    iget v2, p0, Lawc;->g:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lawc;->n:Lawp;

    invoke-virtual {v2}, Lawp;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iput v2, p0, Lawc;->g:I

    iget-object v2, p0, Lawc;->a:Lawd;

    invoke-interface {v2}, Lawd;->a()V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lawc;->m()V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lawc;->c:Lawg;

    iget-object v3, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Lawg;->e()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lawg;->b()[Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->read([Ljava/nio/ByteBuffer;)J

    move-result-wide v3

    long-to-int v3, v3

    if-lez v3, :cond_2

    invoke-virtual {v2, v3}, Lawg;->b(I)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "ClosedChannelException when state was %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lawc;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lawc;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    move v0, v1

    goto :goto_1

    :cond_2
    :try_start_3
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :try_start_4
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "SSLException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lawc;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_2
    move-exception v0

    :try_start_5
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "IOException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lawc;->a(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method final declared-synchronized k()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lawc;->n:Lawp;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lawc;->n:Lawp;

    invoke-virtual {v2}, Lawp;->f()V

    iget v2, p0, Lawc;->g:I

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lawc;->n:Lawp;

    invoke-virtual {v2}, Lawp;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iput v2, p0, Lawc;->g:I

    iget-object v2, p0, Lawc;->a:Lawd;

    invoke-interface {v2}, Lawd;->a()V

    :cond_0
    :goto_0
    iget-object v2, p0, Lawc;->d:Lawg;

    iget-boolean v2, v2, Lawg;->e:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lawc;->g:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v2, p0, Lawc;->d:Lawg;

    iget-object v3, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;

    iget-boolean v4, v2, Lawg;->e:Z

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lawg;->a()[Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/channels/SocketChannel;->write([Ljava/nio/ByteBuffer;)J

    move-result-wide v3

    long-to-int v3, v3

    if-lez v3, :cond_2

    invoke-virtual {v2, v3}, Lawg;->a(I)V
    :try_end_1
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "ClosedChannelException when state was %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lawc;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_3
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
    :try_end_3
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "SSLException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lawc;->a(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lawc;->o:Laye;

    const-string v3, "IOException encountered. Tearing down the socket."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lawc;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized l()Ljava/nio/channels/SocketChannel;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lawc;->b:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
