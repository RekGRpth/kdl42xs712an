.class abstract Lhrx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile a:Z

.field private volatile b:Z

.field protected final c:Limb;

.field final d:Lhqm;

.field protected final e:Lhqq;

.field protected volatile f:Lhpr;

.field private final g:Ljava/lang/Thread;

.field private final h:Lhsk;

.field private volatile i:Lhry;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lhrx;->a:Z

    iput-boolean v3, p0, Lhrx;->b:Z

    const-string v0, "No Handler specified!"

    invoke-static {p2, v0}, Lhsn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    new-instance v0, Lhsk;

    const-string v4, "SignalCollector.Scanner"

    sget-object v5, Lhsk;->a:[S

    invoke-direct/range {v0 .. v5}, Lhsk;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    iput-object v0, p0, Lhrx;->h:Lhsk;

    iget-object v0, p0, Lhrx;->h:Lhsk;

    invoke-virtual {v0, p5}, Lhsk;->a(Lilx;)V

    iput-object p2, p0, Lhrx;->d:Lhqm;

    invoke-static {p4}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhrx;->c:Limb;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lhrx;->g:Ljava/lang/Thread;

    invoke-virtual {p2}, Lhqm;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lhrx;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-ne v1, v0, :cond_1

    :goto_0
    const-string v0, "Scanner should be called in handler\'s thread."

    invoke-static {v2, v0}, Lhsn;->a(ZLjava/lang/Object;)V

    :cond_0
    iput-object p3, p0, Lhrx;->e:Lhqq;

    return-void

    :cond_1
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()V
.end method

.method final declared-synchronized a(Lhpr;Lhry;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lhrx;->f()V

    iget-boolean v2, p0, Lhrx;->a:Z

    if-nez v2, :cond_2

    :goto_0
    const-string v1, "Start should be called only once!"

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhrx;->h:Lhsk;

    invoke-virtual {v0}, Lhsk;->a()V

    iput-object p1, p0, Lhrx;->f:Lhpr;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhrx;->f:Lhpr;

    invoke-virtual {v0}, Lhpr;->a()V

    :cond_0
    iput-object p2, p0, Lhrx;->i:Lhry;

    invoke-virtual {p0}, Lhrx;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrx;->a:Z

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrx;->c:Limb;

    const-string v1, "%s started."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lhry;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lhrx;->a(Lhpr;Lhry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract b()V
.end method

.method protected final b(Lhrz;J)V
    .locals 1

    iget-object v0, p0, Lhrx;->i:Lhry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrx;->i:Lhry;

    invoke-interface {v0, p1, p2, p3}, Lhry;->a(Lhrz;J)V

    :cond_0
    return-void
.end method

.method final declared-synchronized d()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhrx;->a:Z

    const-string v1, "Call start before calling stop!"

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lhrx;->b:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhrx;->b:Z

    iget-object v0, p0, Lhrx;->f:Lhpr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrx;->f:Lhpr;

    invoke-virtual {v0}, Lhpr;->b()V

    :cond_0
    invoke-virtual {p0}, Lhrx;->b()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhrx;->c:Limb;

    const-string v1, "%s stopped."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhrx;->h:Lhsk;

    invoke-virtual {v0}, Lhsk;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhrx;->c:Limb;

    const-string v1, "%s has been stopped before. Skipping"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhrx;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final f()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lhrx;->g:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Could not be called outside owner thread."

    invoke-static {v0, v1}, Lhsn;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
