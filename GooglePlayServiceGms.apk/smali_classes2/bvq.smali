.class public final Lbvq;
.super Lbvv;
.source "SourceFile"


# instance fields
.field private final b:Lcfc;

.field private final c:Lcfz;


# direct methods
.method public constructor <init>(Lcoy;Lcfc;Lbvs;)V
    .locals 1

    invoke-direct {p0, p3}, Lbvv;-><init>(Lbvs;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lbvq;->b:Lcfc;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbvq;->c:Lcfz;

    return-void
.end method


# virtual methods
.method public final a(Lclm;)V
    .locals 6

    invoke-super {p0, p1}, Lbvv;->a(Lclm;)V

    invoke-interface {p1}, Lclm;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lbvq;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_0
    iget-object v1, p0, Lbvq;->c:Lcfz;

    iget-object v2, p0, Lbvq;->b:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Lcfe;->h()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcfe;->c(J)V

    invoke-virtual {v1}, Lcfe;->k()V

    :cond_0
    iget-object v0, p0, Lbvq;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbvq;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbvq;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ChangeFeedProcessor["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lbvq;->b:Lcfc;

    iget-object v1, v1, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
