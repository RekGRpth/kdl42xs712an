.class public final Leid;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:[Leie;

.field public b:Z

.field public c:[Leif;

.field public d:Z

.field public e:[Leih;

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:I

.field public k:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    invoke-static {}, Leie;->c()[Leie;

    move-result-object v0

    iput-object v0, p0, Leid;->a:[Leie;

    iput-boolean v1, p0, Leid;->b:Z

    invoke-static {}, Leif;->c()[Leif;

    move-result-object v0

    iput-object v0, p0, Leid;->c:[Leif;

    iput-boolean v1, p0, Leid;->d:Z

    invoke-static {}, Leih;->c()[Leih;

    move-result-object v0

    iput-object v0, p0, Leid;->e:[Leih;

    iput v1, p0, Leid;->f:I

    iput-boolean v1, p0, Leid;->g:Z

    iput-boolean v1, p0, Leid;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Leid;->i:Z

    iput v1, p0, Leid;->j:I

    iput v1, p0, Leid;->k:I

    const/4 v0, -0x1

    iput v0, p0, Leid;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Leid;->a:[Leie;

    if-eqz v2, :cond_2

    iget-object v2, p0, Leid;->a:[Leie;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Leid;->a:[Leie;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Leid;->a:[Leie;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    invoke-static {v5, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    :cond_2
    iget-boolean v2, p0, Leid;->b:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    iget-boolean v3, p0, Leid;->b:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Leid;->c:[Leif;

    if-eqz v2, :cond_6

    iget-object v2, p0, Leid;->c:[Leif;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Leid;->c:[Leif;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    iget-object v3, p0, Leid;->c:[Leif;

    aget-object v3, v3, v0

    if-eqz v3, :cond_4

    const/4 v4, 0x3

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    :cond_6
    iget-boolean v2, p0, Leid;->d:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x4

    iget-boolean v3, p0, Leid;->d:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Leid;->e:[Leih;

    if-eqz v2, :cond_9

    iget-object v2, p0, Leid;->e:[Leih;

    array-length v2, v2

    if-lez v2, :cond_9

    :goto_2
    iget-object v2, p0, Leid;->e:[Leih;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    iget-object v2, p0, Leid;->e:[Leih;

    aget-object v2, v2, v1

    if-eqz v2, :cond_8

    const/4 v3, 0x5

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    iget v1, p0, Leid;->f:I

    if-eqz v1, :cond_a

    const/4 v1, 0x6

    iget v2, p0, Leid;->f:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Leid;->g:Z

    if-eqz v1, :cond_b

    const/4 v1, 0x7

    iget-boolean v2, p0, Leid;->g:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Leid;->h:Z

    if-eqz v1, :cond_c

    const/16 v1, 0x8

    iget-boolean v2, p0, Leid;->h:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_c
    iget-boolean v1, p0, Leid;->i:Z

    if-eq v1, v5, :cond_d

    const/16 v1, 0x9

    iget-boolean v2, p0, Leid;->i:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Leid;->j:I

    if-eqz v1, :cond_e

    const/16 v1, 0xa

    iget v2, p0, Leid;->j:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Leid;->k:I

    if-eqz v1, :cond_f

    const/16 v1, 0xb

    iget v2, p0, Leid;->k:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Leid;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leid;->a:[Leie;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Leie;

    if-eqz v0, :cond_1

    iget-object v3, p0, Leid;->a:[Leie;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Leie;

    invoke-direct {v3}, Leie;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Leid;->a:[Leie;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Leie;

    invoke-direct {v3}, Leie;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Leid;->a:[Leie;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leid;->b:Z

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leid;->c:[Leif;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Leif;

    if-eqz v0, :cond_4

    iget-object v3, p0, Leid;->c:[Leif;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Leif;

    invoke-direct {v3}, Leif;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Leid;->c:[Leif;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Leif;

    invoke-direct {v3}, Leif;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Leid;->c:[Leif;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leid;->d:Z

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Leid;->e:[Leih;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Leih;

    if-eqz v0, :cond_7

    iget-object v3, p0, Leid;->e:[Leih;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Leih;

    invoke-direct {v3}, Leih;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Leid;->e:[Leih;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Leih;

    invoke-direct {v3}, Leih;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Leid;->e:[Leih;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leid;->f:I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leid;->g:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leid;->h:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leid;->i:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leid;->j:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Leid;->k:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Leid;->a:[Leie;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leid;->a:[Leie;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v2, p0, Leid;->a:[Leie;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Leid;->a:[Leie;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    invoke-virtual {p1, v4, v2}, Lizn;->a(ILizs;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Leid;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iget-boolean v2, p0, Leid;->b:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_2
    iget-object v0, p0, Leid;->c:[Leif;

    if-eqz v0, :cond_4

    iget-object v0, p0, Leid;->c:[Leif;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v2, p0, Leid;->c:[Leif;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Leid;->c:[Leif;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Leid;->d:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    iget-boolean v2, p0, Leid;->d:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_5
    iget-object v0, p0, Leid;->e:[Leih;

    if-eqz v0, :cond_7

    iget-object v0, p0, Leid;->e:[Leih;

    array-length v0, v0

    if-lez v0, :cond_7

    :goto_2
    iget-object v0, p0, Leid;->e:[Leih;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Leid;->e:[Leih;

    aget-object v0, v0, v1

    if-eqz v0, :cond_6

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    iget v0, p0, Leid;->f:I

    if-eqz v0, :cond_8

    const/4 v0, 0x6

    iget v1, p0, Leid;->f:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_8
    iget-boolean v0, p0, Leid;->g:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x7

    iget-boolean v1, p0, Leid;->g:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_9
    iget-boolean v0, p0, Leid;->h:Z

    if-eqz v0, :cond_a

    const/16 v0, 0x8

    iget-boolean v1, p0, Leid;->h:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_a
    iget-boolean v0, p0, Leid;->i:Z

    if-eq v0, v4, :cond_b

    const/16 v0, 0x9

    iget-boolean v1, p0, Leid;->i:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_b
    iget v0, p0, Leid;->j:I

    if-eqz v0, :cond_c

    const/16 v0, 0xa

    iget v1, p0, Leid;->j:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_c
    iget v0, p0, Leid;->k:I

    if-eqz v0, :cond_d

    const/16 v0, 0xb

    iget v1, p0, Leid;->k:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_d
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leid;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leid;

    iget-object v2, p0, Leid;->a:[Leie;

    iget-object v3, p1, Leid;->a:[Leie;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-boolean v2, p0, Leid;->b:Z

    iget-boolean v3, p1, Leid;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Leid;->c:[Leif;

    iget-object v3, p1, Leid;->c:[Leif;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Leid;->d:Z

    iget-boolean v3, p1, Leid;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Leid;->e:[Leih;

    iget-object v3, p1, Leid;->e:[Leih;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Leid;->f:I

    iget v3, p1, Leid;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-boolean v2, p0, Leid;->g:Z

    iget-boolean v3, p1, Leid;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-boolean v2, p0, Leid;->h:Z

    iget-boolean v3, p1, Leid;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-boolean v2, p0, Leid;->i:Z

    iget-boolean v3, p1, Leid;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Leid;->j:I

    iget v3, p1, Leid;->j:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Leid;->k:I

    iget v3, p1, Leid;->k:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    iget-object v0, p0, Leid;->a:[Leie;

    invoke-static {v0}, Lizq;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Leid;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Leid;->c:[Leif;

    invoke-static {v3}, Lizq;->a([Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Leid;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, Leid;->e:[Leih;

    invoke-static {v3}, Lizq;->a([Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Leid;->f:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Leid;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Leid;->h:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Leid;->i:Z

    if-eqz v3, :cond_4

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leid;->j:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leid;->k:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method
