.class public final Lcmn;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcoy;

.field private final c:Lcfz;

.field private final d:Lcdu;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Lcow;

.field private final g:Lcos;

.field private final h:Lclx;

.field private final i:Ljava/util/concurrent/ConcurrentMap;

.field private final j:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lbqs;->t:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcmn;->a:I

    return-void
.end method

.method public constructor <init>(Lcoy;Lcfz;Lcdu;Lcos;)V
    .locals 8

    new-instance v5, Lcmp;

    invoke-direct {v5}, Lcmp;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v6

    new-instance v7, Lclx;

    invoke-direct {v7}, Lclx;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lcmn;-><init>(Lcoy;Lcfz;Lcdu;Lcos;Lcow;Ljava/util/concurrent/ExecutorService;Lclx;)V

    return-void
.end method

.method private constructor <init>(Lcoy;Lcfz;Lcdu;Lcos;Lcow;Ljava/util/concurrent/ExecutorService;Lclx;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcmn;->i:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcmn;->j:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcoy;

    iput-object v0, p0, Lcmn;->b:Lcoy;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lcmn;->c:Lcfz;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lcmn;->d:Lcdu;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcos;

    iput-object v0, p0, Lcmn;->g:Lcos;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcow;

    iput-object v0, p0, Lcmn;->f:Lcow;

    invoke-static {p6}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcmn;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lclx;

    iput-object v0, p0, Lcmn;->h:Lclx;

    return-void
.end method

.method private a(Lclw;)V
    .locals 6

    :try_start_0
    iget-object v0, p1, Lclw;->b:Lcml;

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v0, v1}, Lcml;->c(Lcfz;)Lclw;
    :try_end_0
    .catch Lcly; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OperationQueueImpl"

    const-string v2, "Failed to undo operation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lclw;->a:Lcml;

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method static synthetic a(Lcmn;Lcfc;)V
    .locals 7

    iget-object v0, p0, Lcmn;->j:Ljava/util/concurrent/ConcurrentMap;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcmn;->j:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcmn;->b(Lcfc;)Ljava/util/Queue;

    move-result-object v0

    iget-object v2, p0, Lcmn;->f:Lcow;

    invoke-interface {v2}, Lcow;->a()Lcov;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :goto_0
    :try_start_1
    iget-object v3, p0, Lcmn;->g:Lcos;

    invoke-interface {v3}, Lcos;->a()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "OperationQueueImpl"

    const-string v4, "Suspending sync for account %s because the device is offline."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v3, v4, v5}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    return-void

    :cond_0
    :try_start_3
    invoke-direct {p0, v0}, Lcmn;->a(Ljava/util/Queue;)V

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    :try_start_4
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :cond_1
    :try_start_5
    invoke-interface {v2}, Lcov;->d()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v3

    :try_start_6
    const-string v3, "OperationQueueImpl"

    const-string v4, "Sync interrupted for account: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v3, v4, v5}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0
.end method

.method private a(Ljava/util/Queue;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcmn;->f:Lcow;

    invoke-interface {v0}, Lcow;->a()Lcov;

    move-result-object v2

    :goto_0
    invoke-interface {p1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcmn;->h:Lclx;

    iget-object v1, v0, Lcgc;->a:Lcfc;

    new-instance v3, Lorg/json/JSONObject;

    iget-object v4, v0, Lcgc;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "forward"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "reverse"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v1, v4}, Lclx;->a(Lcfc;Lorg/json/JSONObject;)Lcml;

    move-result-object v4

    invoke-static {v1, v3}, Lclx;->a(Lcfc;Lorg/json/JSONObject;)Lcml;

    move-result-object v1

    new-instance v3, Lclw;

    invoke-direct {v3, v4, v1}, Lclw;-><init>(Lcml;Lcml;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v4, v3, Lclw;->a:Lcml;

    :try_start_1
    iget-object v1, p0, Lcmn;->b:Lcoy;

    invoke-interface {v4, v1}, Lcml;->b(Lcoy;)V

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    invoke-interface {v2}, Lcov;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcme; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcmd; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lclv; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    :catch_0
    move-exception v1

    iget v4, v0, Lcgc;->c:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcgc;->c:I

    invoke-virtual {v0}, Lcgc;->k()V

    iget v4, v0, Lcgc;->c:I

    sget v5, Lcmn;->a:I

    if-gt v4, v5, :cond_0

    const-string v0, "OperationQueueImpl"

    const-string v4, "Applying operation on server failed (will retry): %s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v3, v5, v8

    invoke-static {v0, v1, v4, v5}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v2}, Lcov;->d()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "OperationQueueImpl"

    const-string v4, "Discarded operation that could not be deserialized: %s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v0, v5, v8

    invoke-static {v3, v1, v4, v5}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string v4, "OperationQueueImpl"

    const-string v5, "Too many retries for operation; undoing: %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v3, v6, v8

    invoke-static {v4, v1, v5, v6}, Lcbv;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v3}, Lcmn;->a(Lclw;)V

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto/16 :goto_0

    :catch_2
    move-exception v1

    const-string v1, "OperationQueueImpl"

    const-string v3, "Operation is not yet ready to apply on server; this should never happen, undoing: %s"

    new-array v5, v9, [Ljava/lang/Object;

    aput-object v4, v5, v8

    invoke-static {v1, v3, v5}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v3, "OperationQueueImpl"

    const-string v5, "Dropped operation locally because its entry was deleted: %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v4, v6, v8

    invoke-static {v3, v1, v5, v6}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto/16 :goto_0

    :catch_4
    move-exception v1

    const-string v5, "OperationQueueImpl"

    const-string v6, "Dropped operation locally because the app is no longer authorized; undoing: %s"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-static {v5, v1, v6, v7}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v3}, Lcmn;->a(Lclw;)V

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto/16 :goto_0

    :catch_5
    move-exception v1

    const-string v5, "OperationQueueImpl"

    const-string v6, "Unchecked exception while applying operation; undoing: %s"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v4, v7, v8

    invoke-static {v5, v1, v6, v7}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v3}, Lcmn;->a(Lclw;)V

    invoke-virtual {v0}, Lcgc;->l()V

    invoke-interface {p1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    goto/16 :goto_0

    :cond_1
    return-void
.end method

.method private b(Lcfc;)Ljava/util/Queue;
    .locals 2

    iget-object v0, p0, Lcmn;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcmn;->c:Lcfz;

    invoke-interface {v0, p1}, Lcfz;->d(Lcfc;)Lcgs;

    move-result-object v1

    :try_start_0
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Lcgs;->close()V

    iget-object v1, p0, Lcmn;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcmn;->i:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcgs;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lcfc;)Ljava/util/concurrent/Future;
    .locals 2

    iget-object v0, p0, Lcmn;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcmo;

    invoke-direct {v1, p0, p1}, Lcmo;-><init>(Lcmn;Lcfc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcml;)Z
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_0
    invoke-interface {p1}, Lcml;->d()Lcfc;

    move-result-object v1

    iget-object v2, p0, Lcmn;->c:Lcfz;

    invoke-interface {p1, v2}, Lcml;->c(Lcfz;)Lclw;

    move-result-object v2

    new-instance v3, Lcgc;

    iget-object v4, p0, Lcmn;->d:Lcdu;

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    const-string v6, "forward"

    iget-object v7, v2, Lclw;->a:Lcml;

    invoke-interface {v7}, Lcml;->b()Lorg/json/JSONObject;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v6, "reverse"

    iget-object v2, v2, Lclw;->b:Lcml;

    invoke-interface {v2}, Lcml;->b()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcml;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v5

    invoke-direct {v3, v4, v1, v2, v5}, Lcgc;-><init>(Lcdu;Lcfc;Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v3}, Lcgc;->k()V

    invoke-direct {p0, v1}, Lcmn;->b(Lcfc;)Ljava/util/Queue;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcmn;->c:Lcfz;

    invoke-interface {v2, v1}, Lcfz;->b(Lcfc;)V

    iget-object v2, p0, Lcmn;->c:Lcfz;

    invoke-interface {v2}, Lcfz;->f()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcly; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcmn;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    invoke-virtual {p0, v1}, Lcmn;->a(Lcfc;)Ljava/util/concurrent/Future;

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    :try_start_1
    const-string v2, "OperationQueueImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to commit operation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    goto :goto_0

    :catch_1
    move-exception v1

    :try_start_2
    const-string v2, "OperationQueueImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to apply operation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    goto :goto_0

    :catch_2
    move-exception v1

    :try_start_3
    const-string v2, "OperationQueueImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected exception when appling operation: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcmn;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method
