.class public final Lhud;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhtw;

.field public final b:Lhuu;

.field public final c:Lhtd;

.field public final d:Lhtj;

.field public final e:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lhud;-><init>(Lhtw;Lhuu;Lhtd;Lhtj;Z)V

    return-void
.end method

.method public constructor <init>(Lhtw;Lhuu;Lhtd;Lhtj;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhud;->a:Lhtw;

    iput-object p2, p0, Lhud;->b:Lhuu;

    iput-object p3, p0, Lhud;->c:Lhtd;

    iput-object p4, p0, Lhud;->d:Lhtj;

    iput-boolean p5, p0, Lhud;->e:Z

    if-eqz p1, :cond_0

    iget-object v0, p1, Lhtw;->d:Lhty;

    sget-object v1, Lhty;->a:Lhty;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid Args"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1388

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "NetworkLocation [\n bestResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhud;->a:Lhtw;

    if-nez v1, :cond_1

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    const-string v1, "\n wifiResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhud;->b:Lhuu;

    invoke-static {v0, v1}, Lhuu;->a(Ljava/lang/StringBuilder;Lhuu;)V

    const-string v1, "\n cellResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhud;->c:Lhtd;

    invoke-static {v0, v1}, Lhtd;->a(Ljava/lang/StringBuilder;Lhtd;)V

    const-string v1, "\n glsResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lhud;->d:Lhtj;

    invoke-static {v0, v1}, Lhtj;->a(Ljava/lang/StringBuilder;Lhtj;)V

    const-string v1, "\n isLowPower="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lhud;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\n]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, p0, Lhud;->a:Lhtw;

    iget-object v2, p0, Lhud;->b:Lhuu;

    if-ne v1, v2, :cond_2

    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhud;->a:Lhtw;

    iget-object v2, p0, Lhud;->c:Lhtd;

    if-ne v1, v2, :cond_3

    const-string v1, "CELL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lhud;->a:Lhtw;

    iget-object v2, p0, Lhud;->d:Lhtj;

    if-ne v1, v2, :cond_0

    const-string v1, "GLS"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
