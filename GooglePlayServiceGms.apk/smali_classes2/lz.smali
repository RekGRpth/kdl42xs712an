.class public Llz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lmb;


# static fields
.field static final b:I


# instance fields
.field private a:Landroid/content/Context;

.field c:Lmc;

.field d:Z

.field private e:Landroid/view/LayoutInflater;

.field private f:Lna;

.field private g:Llm;

.field private h:I

.field private i:Landroid/view/View;

.field private j:Z

.field private k:Landroid/view/ViewTreeObserver;

.field private l:Lma;

.field private m:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lkn;->o:I

    sput v0, Llz;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Llm;Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Llz;-><init>(Landroid/content/Context;Llm;Landroid/view/View;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Llm;Landroid/view/View;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Llz;->a:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Llz;->e:Landroid/view/LayoutInflater;

    iput-object p2, p0, Llz;->g:Llm;

    iput-boolean p4, p0, Llz;->j:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Lkk;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Llz;->h:I

    iput-object p3, p0, Llz;->i:Landroid/view/View;

    invoke-virtual {p2, p0}, Llm;->a(Lmb;)V

    return-void
.end method

.method static synthetic a(Llz;)Z
    .locals 1

    iget-boolean v0, p0, Llz;->j:Z

    return v0
.end method

.method static synthetic b(Llz;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Llz;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Llz;)Llm;
    .locals 1

    iget-object v0, p0, Llz;->g:Llm;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-virtual {p0}, Llz;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MenuPopupHelper cannot be used without an anchor"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;Llm;)V
    .locals 0

    return-void
.end method

.method public final a(Llm;Z)V
    .locals 1

    iget-object v0, p0, Llz;->g:Llm;

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Llz;->c()V

    iget-object v0, p0, Llz;->c:Lmc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->c:Lmc;

    invoke-interface {v0, p1, p2}, Lmc;->a(Llm;Z)V

    goto :goto_0
.end method

.method public final a(Lmc;)V
    .locals 0

    iput-object p1, p0, Llz;->c:Lmc;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Llz;->l:Lma;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->l:Lma;

    invoke-virtual {v0}, Lma;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final a(Lmh;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmh;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v3, Llz;

    iget-object v0, p0, Llz;->a:Landroid/content/Context;

    iget-object v4, p0, Llz;->i:Landroid/view/View;

    invoke-direct {v3, v0, p1, v4, v2}, Llz;-><init>(Landroid/content/Context;Llm;Landroid/view/View;Z)V

    iget-object v0, p0, Llz;->c:Lmc;

    iput-object v0, v3, Llz;->c:Lmc;

    invoke-virtual {p1}, Lmh;->size()I

    move-result v4

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_3

    invoke-virtual {p1, v0}, Lmh;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Llz;->d:Z

    invoke-virtual {v3}, Llz;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Llz;->c:Lmc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->c:Lmc;

    invoke-interface {v0, p1}, Lmc;->b(Llm;)Z

    :cond_0
    :goto_2
    return v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final b()Z
    .locals 13

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    new-instance v0, Lna;

    iget-object v4, p0, Llz;->a:Landroid/content/Context;

    sget v5, Lki;->j:I

    invoke-direct {v0, v4, v3, v5}, Lna;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Llz;->f:Lna;

    iget-object v0, p0, Llz;->f:Lna;

    iget-object v0, v0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Llz;->f:Lna;

    iput-object p0, v0, Lna;->i:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lma;

    iget-object v4, p0, Llz;->g:Llm;

    invoke-direct {v0, p0, v4}, Lma;-><init>(Llz;Llm;)V

    iput-object v0, p0, Llz;->l:Lma;

    iget-object v0, p0, Llz;->f:Lna;

    iget-object v4, p0, Llz;->l:Lma;

    invoke-virtual {v0, v4}, Lna;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Llz;->f:Lna;

    invoke-virtual {v0}, Lna;->a()V

    iget-object v4, p0, Llz;->i:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    iput-object v5, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    iget-object v0, p0, Llz;->f:Lna;

    iput-object v4, v0, Lna;->h:Landroid/view/View;

    iget-object v7, p0, Llz;->f:Lna;

    iget-object v8, p0, Llz;->l:Lma;

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    move v5, v2

    move-object v4, v3

    move v6, v2

    :goto_1
    if-ge v5, v11, :cond_4

    invoke-interface {v8, v5}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    if-eq v0, v2, :cond_5

    move-object v2, v3

    :goto_2
    iget-object v4, p0, Llz;->m:Landroid/view/ViewGroup;

    if-nez v4, :cond_1

    new-instance v4, Landroid/widget/FrameLayout;

    iget-object v12, p0, Llz;->a:Landroid/content/Context;

    invoke-direct {v4, v12}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Llz;->m:Landroid/view/ViewGroup;

    :cond_1
    iget-object v4, p0, Llz;->m:Landroid/view/ViewGroup;

    invoke-interface {v8, v5, v2, v4}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v9, v10}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_3
    return v1

    :cond_4
    iget v0, p0, Llz;->h:I

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v7, v0}, Lna;->a(I)V

    iget-object v0, p0, Llz;->f:Lna;

    invoke-virtual {v0}, Lna;->d()V

    iget-object v0, p0, Llz;->f:Lna;

    invoke-virtual {v0}, Lna;->b()V

    iget-object v0, p0, Llz;->f:Lna;

    iget-object v0, v0, Lna;->b:Lnd;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_3

    :cond_5
    move v0, v2

    move-object v2, v4

    goto :goto_2
.end method

.method public final b(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c()V
    .locals 1

    invoke-virtual {p0}, Llz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->f:Lna;

    invoke-virtual {v0}, Lna;->c()V

    :cond_0
    return-void
.end method

.method public final c(Llq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Llz;->f:Lna;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llz;->f:Lna;

    iget-object v0, v0, Lna;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDismiss()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Llz;->f:Lna;

    iget-object v0, p0, Llz;->g:Llm;

    invoke-virtual {v0}, Llm;->close()V

    iget-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Llz;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iput-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    :cond_0
    iget-object v0, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iput-object v1, p0, Llz;->k:Landroid/view/ViewTreeObserver;

    :cond_1
    return-void
.end method

.method public onGlobalLayout()V
    .locals 1

    invoke-virtual {p0}, Llz;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Llz;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Llz;->c()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Llz;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Llz;->f:Lna;

    invoke-virtual {v0}, Lna;->b()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Llz;->l:Lma;

    invoke-static {v0}, Lma;->a(Lma;)Llm;

    move-result-object v1

    invoke-virtual {v0, p3}, Lma;->a(I)Llq;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Llm;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/16 v1, 0x52

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Llz;->c()V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
