.class public final Lbsa;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lbrl;

.field private final d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;


# direct methods
.method public constructor <init>(Lbrc;Lbrl;Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p4}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbsa;->c:Lbrl;

    iput-object p3, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    return-void
.end method

.method static synthetic a(Lbsa;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lbsa;->a:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic a(Lbsa;Lbsp;)V
    .locals 0

    invoke-direct {p0, p1}, Lbsa;->b(Lbsp;)V

    return-void
.end method

.method static synthetic b(Lbsa;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lbsa;->a:Landroid/os/IInterface;

    return-object v0
.end method

.method private b(Lbsp;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iget-object v1, p0, Lbsa;->b:Lbrc;

    invoke-interface {v1, v0}, Lbrc;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v1

    iget-object v2, p0, Lbsa;->c:Lbrl;

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v3

    iget-object v0, p0, Lbsa;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-interface {v0}, Lchq;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v2, p1, v1, v3, v0}, Lbrl;->a(Lbsp;Lcfp;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;
    :try_end_0
    .catch Lbrm; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    :try_start_1
    iget-object v0, p0, Lbsa;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnContentsResponse;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/internal/OnContentsResponse;-><init>(Lcom/google/android/gms/drive/Contents;)V

    invoke-interface {v0, v2}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lbrm; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "OpenContentsOperation"

    const-string v2, "Error returning opened contents."

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catch Lbrm; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_3
    iget-object v0, p0, Lbsa;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    invoke-virtual {v1}, Lbrm;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-interface {v0, v1}, Lchq;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "OpenContentsOperation"

    const-string v2, "Error opening contents."

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 5

    const/high16 v4, 0x30000000

    const/4 v1, 0x0

    const/high16 v3, 0x20000000

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    const-string v2, "Invalid open contents request: no request"

    invoke-static {v0, v2}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v2, "Invalid open contents request: no id"

    invoke-static {v0, v2}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    const/high16 v2, 0x10000000

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v4, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Invalid open contents request: invalid mode"

    invoke-static {v0, v2}, Lbqw;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v3, :cond_3

    :cond_1
    iget-object v0, p0, Lbsa;->b:Lbrc;

    iget-object v2, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-interface {v0, v2}, Lbrc;->b(Lcom/google/android/gms/drive/DriveId;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->G()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lbrm;

    const/16 v2, 0xa

    const-string v3, "The user cannot edit the resource."

    invoke-direct {v0, v2, v3, v1}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b()I

    move-result v0

    if-ne v0, v3, :cond_4

    invoke-direct {p0, p1}, Lbsa;->b(Lbsp;)V

    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lbsa;->b:Lbrc;

    iget-object v1, p0, Lbsa;->d:Lcom/google/android/gms/drive/internal/OpenContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    new-instance v2, Lbsb;

    invoke-direct {v2, p0, p1}, Lbsb;-><init>(Lbsa;Lbsp;)V

    invoke-interface {v0, v1, v2}, Lbrc;->a(Lcom/google/android/gms/drive/DriveId;Lbwk;)V

    goto :goto_1
.end method
