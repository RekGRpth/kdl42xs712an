.class public final Lgcv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

.field private final c:Lftb;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lftb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcv;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgcv;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    iput-object p3, p0, Lgcv;->c:Lftb;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lgcv;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgcv;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    iget-object v2, p2, Lfrx;->c:Lfsj;

    invoke-virtual {v2, v0, v1}, Lfsj;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V

    iget-object v0, p0, Lgcv;->c:Lftb;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lftb;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v3, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcv;->c:Lftb;

    invoke-interface {v0, v4, v1}, Lftb;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcv;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcv;->c:Lftb;

    invoke-interface {v1, v4, v0}, Lftb;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcv;->c:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v5}, Lftb;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lgcv;->c:Lftb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcv;->c:Lftb;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lftb;->a(ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
