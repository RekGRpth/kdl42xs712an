.class final Laex;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Laet;


# direct methods
.method private constructor <init>(Laet;)V
    .locals 0

    iput-object p1, p0, Laex;->a:Laet;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laet;B)V
    .locals 0

    invoke-direct {p0, p1}, Laex;-><init>(Laet;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Laex;->a:Laet;

    iget-object v0, v0, Laet;->b:Laew;

    sget-object v1, Laew;->b:Laew;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Laex;->a:Laet;

    iget-object v0, v0, Laet;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laex;->a:Laet;

    iget-wide v0, v0, Laet;->a:J

    iget-object v2, p0, Laex;->a:Laet;

    iget-wide v2, v2, Laet;->f:J

    add-long/2addr v0, v2

    iget-object v2, p0, Laex;->a:Laet;

    iget-object v2, v2, Laet;->e:Lbpe;

    invoke-interface {v2}, Lbpe;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const-string v0, "Disconnecting due to inactivity"

    invoke-static {v0}, Lafl;->d(Ljava/lang/String;)I

    iget-object v0, p0, Laex;->a:Laet;

    invoke-virtual {v0}, Laet;->g()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Laex;->a:Laet;

    iget-object v0, v0, Laet;->d:Ljava/util/Timer;

    new-instance v1, Laex;

    iget-object v2, p0, Laex;->a:Laet;

    invoke-direct {v1, v2}, Laex;-><init>(Laet;)V

    iget-object v2, p0, Laex;->a:Laet;

    iget-wide v2, v2, Laet;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method
