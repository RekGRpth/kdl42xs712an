.class final Lhdo;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic f:Lhdj;


# direct methods
.method constructor <init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 1

    iput-object p1, p0, Lhdo;->f:Lhdj;

    iput-object p4, p0, Lhdo;->d:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    iput-object p5, p0, Lhdo;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const/4 v0, 0x4

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 10

    const/4 v0, 0x0

    iget-object v1, p0, Lhdo;->d:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->b()Lipp;

    move-result-object v1

    iget-object v2, v1, Lipp;->c:Linv;

    iget v3, v2, Linv;->a:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Update instrument currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v6

    check-cast v6, Lipp;

    iget-object v1, v2, Linv;->b:Lint;

    iget-object v1, v1, Lint;->a:Linu;

    if-eqz v1, :cond_1

    iget-object v1, v2, Linv;->b:Lint;

    iget-object v1, v1, Lint;->a:Linu;

    iget-object v2, v1, Linu;->b:Ljava/lang/String;

    :goto_0
    iget-object v1, v6, Lipp;->c:Linv;

    iget-object v1, v1, Linv;->b:Lint;

    iput-object v0, v1, Lint;->a:Linu;

    iget-object v0, p0, Lhdo;->f:Lhdj;

    invoke-static {v0}, Lhdj;->a(Lhdj;)Lhcv;

    move-result-object v1

    iget-object v0, p0, Lhdo;->f:Lhdj;

    iget-object v0, p0, Lhdo;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lhdj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lhdo;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lhdo;->a:Landroid/accounts/Account;

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lhdo;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v8

    iget-object v0, v6, Lipp;->c:Linv;

    invoke-static {v0}, Lhcv;->a(Linv;)V

    iget-object v9, v1, Lhcv;->a:Landroid/content/Context;

    new-instance v0, Lhdd;

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lhdd;-><init>(Lhcv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lhgm;Lipp;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "update_instrument"

    invoke-static {v9, v0, v1}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method
