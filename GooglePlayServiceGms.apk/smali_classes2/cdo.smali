.class public final enum Lcdo;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcdo;

.field private static final synthetic c:[Lcdo;


# instance fields
.field private final b:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v0, Lcdo;

    const-string v1, "FILENAME"

    invoke-static {}, Lcdn;->d()Lcdn;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    const/4 v3, 0x2

    new-instance v4, Lcei;

    const-string v5, "filename"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    new-array v5, v7, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v4

    iput-boolean v8, v4, Lcei;->g:Z

    invoke-virtual {v2, v3, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcdo;-><init>(Ljava/lang/String;Lcdq;)V

    sput-object v0, Lcdo;->a:Lcdo;

    new-array v0, v8, [Lcdo;

    sget-object v1, Lcdo;->a:Lcdo;

    aput-object v1, v0, v7

    sput-object v0, Lcdo;->c:[Lcdo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcdq;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p2}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcdo;->b:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdo;
    .locals 1

    const-class v0, Lcdo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdo;

    return-object v0
.end method

.method public static values()[Lcdo;
    .locals 1

    sget-object v0, Lcdo;->c:[Lcdo;

    invoke-virtual {v0}, [Lcdo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdo;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcdo;->b:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcdo;->b:Lcdp;

    return-object v0
.end method
