.class final enum Lhjs;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhjs;

.field public static final enum b:Lhjs;

.field public static final enum c:Lhjs;

.field public static final enum d:Lhjs;

.field public static final enum e:Lhjs;

.field public static final enum f:Lhjs;

.field private static final synthetic g:[Lhjs;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lhjs;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v3}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->a:Lhjs;

    new-instance v0, Lhjs;

    const-string v1, "INDOOR_LOW_CONFIDENCE"

    invoke-direct {v0, v1, v4}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->b:Lhjs;

    new-instance v0, Lhjs;

    const-string v1, "OUTDOOR"

    invoke-direct {v0, v1, v5}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->c:Lhjs;

    new-instance v0, Lhjs;

    const-string v1, "OUTDOOR_LOW_CONFIDENCE"

    invoke-direct {v0, v1, v6}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->d:Lhjs;

    new-instance v0, Lhjs;

    const-string v1, "MIX"

    invoke-direct {v0, v1, v7}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->e:Lhjs;

    new-instance v0, Lhjs;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhjs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhjs;->f:Lhjs;

    const/4 v0, 0x6

    new-array v0, v0, [Lhjs;

    sget-object v1, Lhjs;->a:Lhjs;

    aput-object v1, v0, v3

    sget-object v1, Lhjs;->b:Lhjs;

    aput-object v1, v0, v4

    sget-object v1, Lhjs;->c:Lhjs;

    aput-object v1, v0, v5

    sget-object v1, Lhjs;->d:Lhjs;

    aput-object v1, v0, v6

    sget-object v1, Lhjs;->e:Lhjs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhjs;->f:Lhjs;

    aput-object v2, v0, v1

    sput-object v0, Lhjs;->g:[Lhjs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhjs;
    .locals 1

    const-class v0, Lhjs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhjs;

    return-object v0
.end method

.method public static values()[Lhjs;
    .locals 1

    sget-object v0, Lhjs;->g:[Lhjs;

    invoke-virtual {v0}, [Lhjs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhjs;

    return-object v0
.end method
