.class public Lgaj;
.super Lbmi;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {}, Lbmv;->a()Lbmv;

    move-result-object v1

    iget-object v2, p0, Lgaj;->a:Landroid/content/Context;

    iget-object v3, p0, Lgaj;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfrw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lbmy;->a()Lbmy;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lbmy;->a(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lbmv;->a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static {}, Lgal;->a()Lgal;

    iget-object v1, p0, Lgaj;->a:Landroid/content/Context;

    invoke-static {v1, v0, p2}, Lgal;->a(Landroid/content/Context;Ljava/util/HashMap;Lcom/google/android/gms/common/server/ClientContext;)V

    const-string v1, "User-Agent"

    const-string v2, "G+ SDK/1.0.1"

    invoke-static {p1, v2}, Lbmw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method protected final a(Lsc;Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Lsc;->a()I

    move-result v0

    invoke-virtual {p1}, Lsc;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfsr;->a(ILjava/lang/String;)I

    move-result v3

    new-instance v0, Lbml;

    iget-object v1, p0, Lgaj;->a:Landroid/content/Context;

    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lbml;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lsc;->a(Lsm;)Lsc;

    return-void
.end method

.method protected final c(Lcom/google/android/gms/common/server/ClientContext;)Lbmx;
    .locals 2

    new-instance v0, Lbmx;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    return-object v0
.end method
