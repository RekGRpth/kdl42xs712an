.class public abstract Lenc;
.super Lemz;
.source "SourceFile"


# instance fields
.field e:Landroid/os/Handler;

.field private f:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lemz;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v1, Lend;

    invoke-direct {v1, p0, v0}, Lend;-><init>(Lenc;Landroid/os/ConditionVariable;)V

    iput-object v1, p0, Lenc;->f:Ljava/lang/Thread;

    iget-object v1, p0, Lenc;->f:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method


# virtual methods
.method protected final a(Lenb;J)V
    .locals 4

    const/4 v1, 0x0

    const-string v0, "AsyncSchedulerThread.doSchedule: %s %d"

    iget-object v2, p1, Lenb;->a:Lenf;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p1, Lenb;->a:Lenf;

    iget v0, v0, Lenf;->h:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Service tasks can not be delayed."

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lene;

    invoke-direct {v0, p0, p1}, Lene;-><init>(Lenc;Lenb;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lene;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lenc;->e:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    const-string v1, "AsyncSchedulerThread.doSchedule failed."

    invoke-static {v0, v1}, Lbkm;->a(ZLjava/lang/Object;)V

    goto :goto_1
.end method
