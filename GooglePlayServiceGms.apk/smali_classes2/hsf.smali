.class final Lhsf;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private volatile a:Lhqq;

.field private volatile b:Landroid/content/Context;

.field private volatile c:Lhqk;

.field private volatile d:Lhrk;

.field private e:Lhrj;

.field private volatile f:Livi;

.field private g:Lhqz;

.field private h:Lhrl;

.field private final i:Ljava/lang/Integer;

.field private final j:Limb;

.field private final k:Lhsu;

.field private final l:Liea;

.field private final m:Livi;

.field private final n:Ljava/lang/Object;

.field private volatile o:Z

.field private final p:Ljava/util/concurrent/CountDownLatch;

.field private q:Lhqq;


# direct methods
.method constructor <init>(Landroid/content/Context;Lhqk;Ljava/util/concurrent/CountDownLatch;Lhsu;Liea;Livi;Ljava/lang/Integer;Lhqq;Limb;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhsf;->n:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhsf;->o:Z

    new-instance v0, Lhsg;

    invoke-direct {v0, p0}, Lhsg;-><init>(Lhsf;)V

    iput-object v0, p0, Lhsf;->q:Lhqq;

    const-string v0, "SignalCollector.ScannerThread"

    invoke-virtual {p0, v0}, Lhsf;->setName(Ljava/lang/String;)V

    iput-object p2, p0, Lhsf;->c:Lhqk;

    iput-object p1, p0, Lhsf;->b:Landroid/content/Context;

    iput-object p8, p0, Lhsf;->a:Lhqq;

    iput-object p7, p0, Lhsf;->i:Ljava/lang/Integer;

    iput-object p9, p0, Lhsf;->j:Limb;

    iput-object p3, p0, Lhsf;->p:Ljava/util/concurrent/CountDownLatch;

    iput-object p6, p0, Lhsf;->m:Livi;

    iput-object p4, p0, Lhsf;->k:Lhsu;

    iput-object p5, p0, Lhsf;->l:Liea;

    return-void
.end method

.method static synthetic a(Lhsf;Lhrl;)Lhrl;
    .locals 0

    iput-object p1, p0, Lhsf;->h:Lhrl;

    return-object p1
.end method

.method static synthetic a(Lhsf;Livi;)Livi;
    .locals 0

    iput-object p1, p0, Lhsf;->f:Livi;

    return-object p1
.end method

.method static synthetic a(Lhsf;)V
    .locals 3

    iget-object v1, p0, Lhsf;->n:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhsf;->o:Z

    iget-object v0, p0, Lhsf;->e:Lhrj;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhsf;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsf;->e:Lhrj;

    iget-object v2, v0, Lhrj;->b:Lhqr;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lhrj;->c:Lhqm;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lhrj;->c:Lhqm;

    iget-object v0, v0, Lhrj;->b:Lhqr;

    invoke-virtual {v2, v0}, Lhqm;->a(Lhrx;)V

    :cond_0
    iget-object v0, p0, Lhsf;->h:Lhrl;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lhsf;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhsf;->h:Lhrl;

    invoke-virtual {v0}, Lhrl;->a()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lhsf;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lhsf;->n:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lhsf;)Limb;
    .locals 1

    iget-object v0, p0, Lhsf;->j:Limb;

    return-object v0
.end method

.method static synthetic d(Lhsf;)Livi;
    .locals 1

    iget-object v0, p0, Lhsf;->f:Livi;

    return-object v0
.end method

.method static synthetic e(Lhsf;)Lhqz;
    .locals 1

    iget-object v0, p0, Lhsf;->g:Lhqz;

    return-object v0
.end method

.method static synthetic f(Lhsf;)Z
    .locals 1

    iget-boolean v0, p0, Lhsf;->o:Z

    return v0
.end method

.method static synthetic g(Lhsf;)Lhqq;
    .locals 1

    iget-object v0, p0, Lhsf;->a:Lhqq;

    return-object v0
.end method

.method static synthetic h(Lhsf;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lhsf;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic i(Lhsf;)Lhqk;
    .locals 1

    iget-object v0, p0, Lhsf;->c:Lhqk;

    return-object v0
.end method

.method static synthetic j(Lhsf;)Lhsu;
    .locals 1

    iget-object v0, p0, Lhsf;->k:Lhsu;

    return-object v0
.end method

.method static synthetic k(Lhsf;)Liea;
    .locals 1

    iget-object v0, p0, Lhsf;->l:Liea;

    return-object v0
.end method

.method static synthetic l(Lhsf;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lhsf;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic m(Lhsf;)Livi;
    .locals 1

    iget-object v0, p0, Lhsf;->m:Livi;

    return-object v0
.end method

.method static synthetic n(Lhsf;)Lhrl;
    .locals 1

    iget-object v0, p0, Lhsf;->h:Lhrl;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v1, p0, Lhsf;->n:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhsf;->o:Z

    iget-object v0, p0, Lhsf;->h:Lhrl;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhsf;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsf;->h:Lhrl;

    invoke-virtual {v0}, Lhrl;->a()V

    iget-object v2, v0, Lhrl;->b:Lhqm;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lhrl;->b:Lhqm;

    invoke-virtual {v0}, Lhqm;->a()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Livi;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhsf;->h:Lhrl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhsf;->h:Lhrl;

    iget-object v2, v1, Lhrl;->a:Lhqu;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, v1, Lhrl;->a:Lhqu;

    invoke-virtual {v0, p1}, Lhqu;->a(Livi;)Z

    move-result v0

    goto :goto_0
.end method

.method public final run()V
    .locals 24

    move-object/from16 v0, p0

    iget-object v1, v0, Lhsf;->b:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    new-instance v1, Lhsk;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lhsf;->getName()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lhsk;->a:[S

    invoke-direct/range {v1 .. v6}, Lhsk;-><init>(Landroid/os/PowerManager;IZLjava/lang/String;[S)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->c:Lhqk;

    invoke-interface {v2}, Lhqk;->a()Lilx;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhsk;->a(Lilx;)V

    invoke-virtual {v1}, Lhsk;->a()V

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v3, Lhsh;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lhsh;-><init>(Lhsf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v16, 0x0

    const/4 v15, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lhsf;->c:Lhqk;

    invoke-interface {v4}, Lhqk;->k()Lhqk;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lhsf;->c:Lhqk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhsf;->d:Lhrk;

    if-eqz v4, :cond_7

    new-instance v4, Lhrj;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhsf;->j:Limb;

    invoke-direct {v4, v3, v5, v2}, Lhrj;-><init>(Landroid/os/Handler;Limb;Lilx;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lhsf;->e:Lhrj;

    move-object/from16 v0, p0

    iget-object v0, v0, Lhsf;->e:Lhrj;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lhsf;->b:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lhsf;->d:Lhrk;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lhsf;->k:Lhsu;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lhsf;->l:Liea;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v12, v0, Lhsf;->q:Lhqq;

    new-instance v3, Lhrf;

    move-object/from16 v0, v18

    iget-object v2, v0, Lhrj;->d:Limb;

    invoke-direct {v3, v12, v2}, Lhrf;-><init>(Lhqq;Limb;)V

    const/16 v17, 0x0

    new-instance v2, Lhqu;

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v18

    iget-object v8, v0, Lhrj;->d:Limb;

    invoke-direct/range {v2 .. v8}, Lhqu;-><init>(Lhrv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Livi;Limb;)V

    new-instance v3, Lhqm;

    move-object/from16 v0, v18

    iget-object v4, v0, Lhrj;->a:Landroid/os/Handler;

    const/4 v5, 0x1

    move-object/from16 v0, v18

    iget-object v6, v0, Lhrj;->d:Limb;

    invoke-direct {v3, v2, v4, v5, v6}, Lhqm;-><init>(Lhqu;Landroid/os/Handler;ILimb;)V

    move-object/from16 v0, v18

    iput-object v3, v0, Lhrj;->c:Lhqm;

    new-instance v2, Lhqr;

    invoke-interface/range {v20 .. v20}, Lhrk;->b()Ljava/util/Set;

    move-result-object v4

    invoke-interface/range {v20 .. v20}, Lhrk;->d()Ljava/util/Map;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface/range {v20 .. v20}, Lhrk;->c()Z

    move-result v9

    move-object/from16 v0, v18

    iget-object v10, v0, Lhrj;->c:Lhqm;

    const/4 v11, 0x1

    move-object/from16 v0, v18

    iget-object v13, v0, Lhrj;->d:Limb;

    move-object/from16 v0, v18

    iget-object v14, v0, Lhrj;->e:Lilx;

    move-object/from16 v3, v19

    move-object/from16 v7, v21

    move-object/from16 v8, v22

    invoke-direct/range {v2 .. v14}, Lhqr;-><init>(Landroid/content/Context;Ljava/util/Set;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhsu;Liea;ZLhqm;ZLhqq;Limb;Lilx;)V

    move-object/from16 v0, v18

    iput-object v2, v0, Lhrj;->b:Lhqr;

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lhrj;->d:Limb;

    const-string v3, "PreScan started with config: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v20, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, v18

    iget-object v2, v0, Lhrj;->b:Lhqr;

    invoke-virtual {v2}, Lhqr;->c()I

    move-result v2

    if-lez v2, :cond_b

    move-object/from16 v0, v18

    iget-object v2, v0, Lhrj;->b:Lhqr;

    new-instance v3, Lhsj;

    move-object/from16 v0, v18

    iget-object v4, v0, Lhrj;->b:Lhqr;

    invoke-interface/range {v20 .. v20}, Lhrk;->a()J

    move-result-wide v5

    move-object/from16 v0, v18

    iget-object v7, v0, Lhrj;->c:Lhqm;

    invoke-direct {v3, v4, v5, v6, v7}, Lhsj;-><init>(Lhrx;JLhqm;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lhqr;->a(Lhpr;Lhry;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_6

    :try_start_2
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lhsf;->j:Limb;

    const-string v4, "PreScanner started."

    invoke-virtual {v3, v4}, Limb;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v3, v2

    :cond_1
    :goto_1
    if-nez v3, :cond_3

    :try_start_3
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->j:Limb;

    invoke-virtual {v2, v15}, Limb;->a(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->a:Lhqq;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->a:Lhqq;

    invoke-interface {v2, v15}, Lhqq;->a(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    if-eqz v3, :cond_4

    invoke-static {}, Landroid/os/Looper;->loop()V

    :cond_4
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->j:Limb;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lhsf;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is quiting."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_5
    invoke-virtual {v1}, Lhsk;->b()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->b:Landroid/content/Context;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->c:Lhqk;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->h:Lhrl;

    return-void

    :cond_6
    :try_start_4
    const-string v15, "PreScanner: Nothing to scan."
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v3, v2

    goto :goto_1

    :cond_7
    :try_start_5
    new-instance v2, Lhrl;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhsf;->j:Limb;

    invoke-direct {v2, v3, v4}, Lhrl;-><init>(Landroid/os/Handler;Limb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lhsf;->h:Lhrl;

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->h:Lhrl;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhsf;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhsf;->c:Lhqk;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhsf;->k:Lhsu;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhsf;->l:Liea;

    move-object/from16 v0, p0

    iget-object v7, v0, Lhsf;->i:Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lhsf;->m:Livi;

    move-object/from16 v0, p0

    iget-object v9, v0, Lhsf;->a:Lhqq;

    invoke-virtual/range {v2 .. v9}, Lhrl;->a(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v3

    if-eqz v3, :cond_8

    :try_start_6
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->j:Limb;

    const-string v4, "RealCollector started."

    invoke-virtual {v2, v4}, Limb;->a(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    :catch_0
    move-exception v2

    :goto_2
    :try_start_7
    const-string v4, "Failed normalize configuration: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lhsf;->j:Limb;

    invoke-virtual {v2, v15}, Limb;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lhsk;->b()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->b:Landroid/content/Context;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->c:Lhqk;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lhsf;->h:Lhrl;

    throw v2

    :cond_8
    :try_start_8
    const-string v2, "RealCollector: Nothing to scan."
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :goto_3
    move-object v15, v2

    goto/16 :goto_1

    :catch_1
    move-exception v2

    move/from16 v3, v16

    goto :goto_2

    :catch_2
    move-exception v3

    move-object/from16 v23, v3

    move v3, v2

    move-object/from16 v2, v23

    goto :goto_2

    :cond_9
    move-object v2, v15

    goto :goto_3

    :cond_a
    move v3, v2

    move-object v2, v15

    goto :goto_3

    :cond_b
    move/from16 v2, v17

    goto/16 :goto_0
.end method
