.class abstract Lear;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final synthetic b:Leap;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/database/CharArrayBuffer;

.field private final e:Landroid/view/View;


# direct methods
.method protected constructor <init>(Leap;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lear;->b:Leap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lear;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lear;->c:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lear;->d:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lear;->e:Landroid/view/View;

    iget-object v0, p0, Lear;->e:Landroid/view/View;

    invoke-static {p1}, Leap;->a(Leap;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/Player;)V
    .locals 4

    iget-object v0, p0, Lear;->b:Leap;

    invoke-virtual {v0}, Leap;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lear;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lwz;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    iget-object v0, p0, Lear;->d:Landroid/database/CharArrayBuffer;

    invoke-interface {p1, v0}, Lcom/google/android/gms/games/Player;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v0, p0, Lear;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lear;->d:Landroid/database/CharArrayBuffer;

    iget-object v1, v1, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v2, 0x0

    iget-object v3, p0, Lear;->d:Landroid/database/CharArrayBuffer;

    iget v3, v3, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->setText([CII)V

    iget-object v0, p0, Lear;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v0, p0, Lear;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method
