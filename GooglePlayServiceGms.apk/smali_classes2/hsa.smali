.class final Lhsa;
.super Lhrx;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field private final b:Ljava/util/Map;

.field private final g:Landroid/hardware/SensorManager;

.field private final h:Landroid/hardware/SensorEventListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/Map;Lcom/google/android/location/collectionlib/SensorScannerConfig;Lhqm;Lhqq;Limb;Lilx;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lhrx;-><init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V

    new-instance v0, Lhsb;

    invoke-direct {v0, p0}, Lhsb;-><init>(Lhsa;)V

    iput-object v0, p0, Lhsa;->h:Landroid/hardware/SensorEventListener;

    invoke-static {p1}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lhsn;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lhsa;->b:Ljava/util/Map;

    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lhsa;->g:Landroid/hardware/SensorManager;

    iput-object p3, p0, Lhsa;->a:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    iget-object v0, p0, Lhsa;->g:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhsa;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v3, p0, Lhsa;->g:Landroid/hardware/SensorManager;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lhsa;->g:Landroid/hardware/SensorManager;

    iget-object v4, p0, Lhsa;->h:Landroid/hardware/SensorEventListener;

    const/4 v5, 0x0

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Sensor;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v5, p0, Lhrx;->d:Lhqm;

    invoke-virtual {v3, v4, v1, v0, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhsa;->e:Lhqq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhsa;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->e()V

    :cond_2
    return-void
.end method

.method protected final b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lhsa;->g:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lhsa;->h:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lhsa;->e:Lhqq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhsa;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->d()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
