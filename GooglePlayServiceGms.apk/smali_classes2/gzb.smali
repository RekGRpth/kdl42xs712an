.class public final Lgzb;
.super Lgzo;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lgzo;-><init>(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lgth;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v0}, Lgth;->b(I)I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-static {v0}, Lgth;->c(I)I

    move-result v0

    if-le v3, v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v4, v2

    move v5, v2

    :goto_1
    if-ltz v3, :cond_4

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v6, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v4, :cond_2

    mul-int/lit8 v0, v0, 0x2

    const/16 v7, 0x9

    if-le v0, v7, :cond_2

    add-int/lit8 v0, v0, -0x9

    :cond_2
    add-int/2addr v5, v0

    if-nez v4, :cond_3

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, -0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    rem-int/lit8 v0, v5, 0xa

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgzb;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
