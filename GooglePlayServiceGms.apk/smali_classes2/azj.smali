.class abstract Lazj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Laye;

.field private static b:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/List;

.field private final f:Landroid/os/Handler;

.field private g:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lbbj;

    const-string v1, "DeviceFilter"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Lazj;->a:Laye;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lazj;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lazj;->e:Ljava/util/List;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lazj;->f:Landroid/os/Handler;

    iput-object p1, p0, Lazj;->c:Landroid/content/Context;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lazj;->g:Ljava/util/Set;

    iput-object p3, p0, Lazj;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lazj;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lazj;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic a()Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    sget-object v0, Lazj;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method

.method static synthetic b()Laye;
    .locals 1

    sget-object v0, Lazj;->a:Laye;

    return-object v0
.end method

.method static synthetic b(Lazj;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lazj;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lazj;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lazj;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic d(Lazj;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lazj;->f:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/cast/CastDevice;)V
.end method

.method protected abstract a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V
.end method

.method public final a(Ljava/util/Set;)V
    .locals 3

    iget-object v0, p0, Lazj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazl;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lazl;->e:Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lazj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lazj;->g:Ljava/util/Set;

    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v1, Lazl;

    invoke-direct {v1, p0, p1}, Lazl;-><init>(Lazj;Lcom/google/android/gms/cast/CastDevice;)V

    iget-boolean v0, v1, Lazl;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v1, Lazl;->d:Z

    if-nez v0, :cond_1

    :cond_0
    :try_start_0
    sget-object v0, Lazj;->a:Laye;

    const-string v2, "connecting to: %s:%d (%s)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v1, Lazl;->a:Lawc;

    iget-object v2, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v2

    iget-object v3, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lawc;->a(Ljava/net/Inet4Address;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lazj;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :catch_0
    move-exception v0

    sget-object v2, Lazj;->a:Laye;

    const-string v3, "Exception while connecting socket"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Laye;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lazl;->b:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, v1, Lazl;->f:Lazj;

    iget-object v2, v2, Lazj;->g:Ljava/util/Set;

    invoke-virtual {v1, v0, v2}, Lazl;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V

    goto :goto_0
.end method
