.class public final Liuh;
.super Lizp;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Litx;

.field public c:Liuc;

.field public d:[Lium;

.field public e:Liuj;

.field public f:Liuf;

.field public g:Liuf;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizp;-><init>()V

    iput-object v1, p0, Liuh;->a:Ljava/lang/Integer;

    iput-object v1, p0, Liuh;->b:Litx;

    iput-object v1, p0, Liuh;->c:Liuc;

    invoke-static {}, Lium;->c()[Lium;

    move-result-object v0

    iput-object v0, p0, Liuh;->d:[Lium;

    iput-object v1, p0, Liuh;->e:Liuj;

    iput-object v1, p0, Liuh;->f:Liuf;

    iput-object v1, p0, Liuh;->g:Liuf;

    iput-object v1, p0, Liuh;->q:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Liuh;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizp;->a()I

    move-result v0

    iget-object v1, p0, Liuh;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Liuh;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Liuh;->b:Litx;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Liuh;->b:Litx;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Liuh;->c:Liuc;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Liuh;->c:Liuc;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Liuh;->d:[Lium;

    if-eqz v1, :cond_5

    iget-object v1, p0, Liuh;->d:[Lium;

    array-length v1, v1

    if-lez v1, :cond_5

    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Liuh;->d:[Lium;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Liuh;->d:[Lium;

    aget-object v2, v2, v0

    if-eqz v2, :cond_3

    const/4 v3, 0x4

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    :cond_5
    iget-object v1, p0, Liuh;->e:Liuj;

    if-eqz v1, :cond_6

    const/4 v1, 0x5

    iget-object v2, p0, Liuh;->e:Liuj;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Liuh;->f:Liuf;

    if-eqz v1, :cond_7

    const/4 v1, 0x6

    iget-object v2, p0, Liuh;->f:Liuf;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Liuh;->g:Liuf;

    if-eqz v1, :cond_8

    const/4 v1, 0x7

    iget-object v2, p0, Liuh;->g:Liuf;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Liuh;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Liuh;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Liuh;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Liuh;->b:Litx;

    if-nez v0, :cond_1

    new-instance v0, Litx;

    invoke-direct {v0}, Litx;-><init>()V

    iput-object v0, p0, Liuh;->b:Litx;

    :cond_1
    iget-object v0, p0, Liuh;->b:Litx;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liuh;->c:Liuc;

    if-nez v0, :cond_2

    new-instance v0, Liuc;

    invoke-direct {v0}, Liuc;-><init>()V

    iput-object v0, p0, Liuh;->c:Liuc;

    :cond_2
    iget-object v0, p0, Liuh;->c:Liuc;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Liuh;->d:[Lium;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lium;

    if-eqz v0, :cond_3

    iget-object v3, p0, Liuh;->d:[Lium;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lium;

    invoke-direct {v3}, Lium;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Liuh;->d:[Lium;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lium;

    invoke-direct {v3}, Lium;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Liuh;->d:[Lium;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Liuh;->e:Liuj;

    if-nez v0, :cond_6

    new-instance v0, Liuj;

    invoke-direct {v0}, Liuj;-><init>()V

    iput-object v0, p0, Liuh;->e:Liuj;

    :cond_6
    iget-object v0, p0, Liuh;->e:Liuj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Liuh;->f:Liuf;

    if-nez v0, :cond_7

    new-instance v0, Liuf;

    invoke-direct {v0}, Liuf;-><init>()V

    iput-object v0, p0, Liuh;->f:Liuf;

    :cond_7
    iget-object v0, p0, Liuh;->f:Liuf;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Liuh;->g:Liuf;

    if-nez v0, :cond_8

    new-instance v0, Liuf;

    invoke-direct {v0}, Liuf;-><init>()V

    iput-object v0, p0, Liuh;->g:Liuf;

    :cond_8
    iget-object v0, p0, Liuh;->g:Liuf;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Liuh;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liuh;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Liuh;->b:Litx;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liuh;->b:Litx;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Liuh;->c:Liuc;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liuh;->c:Liuc;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Liuh;->d:[Lium;

    if-eqz v0, :cond_4

    iget-object v0, p0, Liuh;->d:[Lium;

    array-length v0, v0

    if-lez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Liuh;->d:[Lium;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Liuh;->d:[Lium;

    aget-object v1, v1, v0

    if-eqz v1, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lizn;->a(ILizs;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Liuh;->e:Liuj;

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget-object v1, p0, Liuh;->e:Liuj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_5
    iget-object v0, p0, Liuh;->f:Liuf;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Liuh;->f:Liuf;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_6
    iget-object v0, p0, Liuh;->g:Liuf;

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-object v1, p0, Liuh;->g:Liuf;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_7
    invoke-super {p0, p1}, Lizp;->a(Lizn;)V

    return-void
.end method
