.class final Lhkt;
.super Lhks;
.source "SourceFile"

# interfaces
.implements Lhkp;


# instance fields
.field final synthetic c:Lhkr;

.field private final e:Ljava/util/List;

.field private final f:Ljava/util/List;

.field private g:I


# direct methods
.method private constructor <init>(Lhkr;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lhkt;->c:Lhkr;

    invoke-direct {p0, p1, v2}, Lhks;-><init>(Lhkr;B)V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lhkt;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lhkt;->f:Ljava/util/List;

    iput v2, p0, Lhkt;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lhkr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhkt;-><init>(Lhkr;)V

    return-void
.end method

.method private static a(Lcom/google/android/gms/location/ActivityRecognitionResult;Ljava/util/List;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/DetectedActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v7

    add-int/2addr v3, v7

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->b()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v2

    if-le v2, v8, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v1, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    invoke-static {v4, v1, v2, v3, v8}, Lhkm;->a(Ljava/util/Map;IDZ)Ljava/util/List;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    return-object v0
.end method

.method private d(Lcom/google/android/gms/location/ActivityRecognitionResult;)I
    .locals 3

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-eq v2, v0, :cond_0

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1
.end method

.method private k()V
    .locals 4

    invoke-static {}, Lhkr;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->s(Lhkr;)Lhln;

    move-result-object v0

    invoke-interface {v0}, Lhln;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkt;->c:Lhkr;

    new-instance v1, Lhkz;

    iget-object v2, p0, Lhkt;->c:Lhkr;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lhkz;-><init>(Lhkr;B)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhkt;->c:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(D)V
    .locals 3

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at sensorDelay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->o(Lhkr;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_4

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->u(Lhkr;)I

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->v(Lhkr;)I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->o(Lhkr;)I

    move-result v0

    if-lez v0, :cond_3

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too slow. Changing sensor delay: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->o(Lhkr;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->w(Lhkr;)I

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->x(Lhkr;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sample rate too slow. consecutiveInsufficientSamplingRate="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->v(Lhkr;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->y(Lhkr;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 14

    const/4 v13, 0x4

    const/4 v8, 0x3

    const/4 v12, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0, p1}, Lhkt;->c(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    iget-boolean v0, p0, Lhkt;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lhkt;->c:Lhkr;

    new-instance v1, Lhky;

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-direct {v1, v2}, Lhky;-><init>(Lhkr;)V

    invoke-static {v0, v1}, Lhkr;->a(Lhkr;Lhla;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhkt;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->m(Lhkr;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    iget-object v1, p0, Lhkt;->c:Lhkr;

    invoke-static {v1}, Lhkr;->m(Lhkr;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    if-ne v0, v1, :cond_4

    move v0, v6

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v6, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v12, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v8, :cond_5

    :cond_2
    move v0, v6

    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, p0, Lhkt;->f:Ljava/util/List;

    invoke-static {p1, v0}, Lhkt;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Ljava/util/List;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "ActivityScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Same as last. Reporting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lhkt;->c:Lhkr;

    invoke-static {v1, v0}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    invoke-direct {p0}, Lhkt;->k()V

    goto/16 :goto_0

    :cond_4
    move v0, v7

    goto :goto_1

    :cond_5
    move v0, v7

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_8

    new-instance v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Ljava/util/List;JJ)V

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_7

    const-string v1, "ActivityScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reporting tilting: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lhkt;->c:Lhkr;

    invoke-static {v1, v0}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    :cond_8
    invoke-direct {p0, p1}, Lhkt;->d(Lcom/google/android/gms/location/ActivityRecognitionResult;)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    if-lt v4, v8, :cond_c

    move v0, v6

    :goto_3
    iget v1, p0, Lhkt;->g:I

    rsub-int/lit8 v2, v4, 0x3

    add-int/2addr v1, v2

    const/4 v2, 0x5

    if-le v1, v2, :cond_d

    move v3, v6

    :goto_4
    if-nez v0, :cond_9

    if-eqz v3, :cond_12

    :cond_9
    iget-object v0, p0, Lhkt;->f:Ljava/util/List;

    invoke-static {p1, v0}, Lhkt;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;Ljava/util/List;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v6, :cond_a

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v12, :cond_e

    :cond_a
    move v0, v6

    :goto_5
    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->l(Lhkr;)Lhlt;

    move-result-object v2

    if-eqz v2, :cond_f

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v2

    invoke-interface {v2}, Lidu;->j()Licm;

    move-result-object v2

    invoke-interface {v2}, Licm;->a()J

    move-result-wide v8

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->t(Lhkr;)J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/32 v10, 0x15f90

    cmp-long v2, v8, v10

    if-gez v2, :cond_f

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->l(Lhkr;)Lhlt;

    move-result-object v2

    iget-wide v8, v2, Lhlt;->b:D

    const-wide v10, 0x3fe6666666666666L    # 0.7

    cmpl-double v2, v8, v10

    if-ltz v2, :cond_f

    move v2, v6

    :goto_6
    if-eqz v2, :cond_10

    iget-object v2, p0, Lhkt;->c:Lhkr;

    invoke-static {v2}, Lhkr;->l(Lhkr;)Lhlt;

    move-result-object v2

    iget-object v2, v2, Lhlt;->a:Lhup;

    sget-object v5, Lhup;->a:Lhup;

    if-ne v2, v5, :cond_10

    move v2, v6

    :goto_7
    if-eqz v2, :cond_11

    if-eqz v0, :cond_11

    :goto_8
    if-eqz v6, :cond_18

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-eq v0, v12, :cond_18

    new-instance v0, Lhkm;

    invoke-direct {v0}, Lhkm;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v7

    invoke-virtual {v0, v7, v8}, Lhkm;->b(J)Lhkm;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->c()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lhkm;->a(J)Lhkm;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lhkm;->a(Ljava/util/List;)Lhkm;

    move-result-object v1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x32

    invoke-virtual {v1, v2, v5}, Lhkm;->a(Ljava/lang/Integer;I)Lhkm;

    invoke-virtual {v0}, Lhkm;->a()Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v0

    :goto_9
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_b

    const-string v1, "ActivityScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Reporting activity: consistent="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " maxAttemptsReached="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " inconsistentWithTravelMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " attempts="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lhkt;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " finalResult="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-object v1, p0, Lhkt;->c:Lhkr;

    invoke-static {v1, v0}, Lhkr;->a(Lhkr;Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    invoke-direct {p0}, Lhkt;->k()V

    goto/16 :goto_0

    :cond_c
    move v0, v7

    goto/16 :goto_3

    :cond_d
    move v3, v7

    goto/16 :goto_4

    :cond_e
    move v0, v7

    goto/16 :goto_5

    :cond_f
    move v2, v7

    goto/16 :goto_6

    :cond_10
    move v2, v7

    goto/16 :goto_7

    :cond_11
    move v6, v7

    goto/16 :goto_8

    :cond_12
    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v12, :cond_15

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_13

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not enough history to decide: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhkt;->e:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    :goto_a
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-eq v0, v13, :cond_14

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_14
    :goto_b
    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v8, :cond_16

    iget-object v0, p0, Lhkt;->e:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_b

    :cond_15
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_13

    const-string v0, "ActivityScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lhkt;->e:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    :cond_16
    invoke-virtual {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v0

    if-ne v0, v12, :cond_17

    invoke-virtual {p0}, Lhkt;->g()V

    goto/16 :goto_0

    :cond_17
    iget-object v0, p0, Lhkt;->c:Lhkr;

    iget-object v1, p0, Lhkt;->c:Lhkr;

    invoke-static {v1}, Lhkr;->b(Lhkr;)Lidu;

    move-result-object v1

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    const-wide/16 v3, 0x1388

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lhkr;->c(Lhkr;J)V

    goto/16 :goto_0

    :cond_18
    move-object v0, v1

    goto/16 :goto_9
.end method

.method protected final b()Lhms;
    .locals 1

    iget-object v0, p0, Lhkt;->c:Lhkr;

    invoke-static {v0}, Lhkr;->j(Lhkr;)Lhms;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    const-string v0, "FullDetector"

    return-object v0
.end method

.method protected final g()V
    .locals 1

    iget v0, p0, Lhkt;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhkt;->g:I

    invoke-super {p0}, Lhks;->g()V

    return-void
.end method

.method protected final j()V
    .locals 0

    invoke-virtual {p0}, Lhkt;->g()V

    return-void
.end method
