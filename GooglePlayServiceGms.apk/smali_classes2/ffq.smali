.class final Lffq;
.super Lffo;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lfbz;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p6}, Lffo;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;)V

    iput-object p4, p0, Lffq;->c:Ljava/lang/String;

    iput-object p5, p0, Lffq;->d:Ljava/lang/String;

    iput p7, p0, Lffq;->e:I

    and-int/lit8 v0, p8, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lffq;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final c()[B
    .locals 10

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lffq;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->m()Lfhp;

    move-result-object v4

    iget-object v5, p0, Lffq;->c:Ljava/lang/String;

    iget-object v6, p0, Lffq;->d:Ljava/lang/String;

    iget v7, p0, Lffq;->e:I

    iget-boolean v8, p0, Lffq;->f:Z

    invoke-static {v5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "avatarSize"

    invoke-static {v7, v0}, Lfbb;->a(ILjava/lang/String;)V

    new-instance v0, Lfip;

    iget-object v9, v4, Lfhp;->a:Landroid/content/Context;

    invoke-direct {v0, v9, v5, v3}, Lfip;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lfip;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    if-eqz v8, :cond_3

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v7, v0}, Lfhp;->a(IZ)[B

    move-result-object v0

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    invoke-virtual {v0, v6}, Lfip;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfjp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v5, v6, v0, v2}, Lfhp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v3, v4, Lfhp;->b:Lfjl;

    invoke-static {v0}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v6, v0, v7}, Lfjl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz v8, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_3
    invoke-virtual {v4, v7, v1}, Lfhp;->a(IZ)[B

    move-result-object v0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3
.end method
