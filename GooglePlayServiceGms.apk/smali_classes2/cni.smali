.class public final Lcni;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcnt;


# static fields
.field private static final a:Lcni;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcni;

    invoke-direct {v0}, Lcni;-><init>()V

    sput-object v0, Lcni;->a:Lcni;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcni;
    .locals 1

    sget-object v0, Lcni;->a:Lcni;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->a()Lcnj;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, p0}, Lcnj;->a(Lcnt;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnk;

    invoke-interface {v0}, Lcnk;->b()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcja;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    sget-object v0, Lcnn;->d:Lcja;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/gms/drive/DriveId;

    new-instance v0, Lcno;

    const/4 v1, 0x0

    invoke-static {v1, p2}, Lcdk;->a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2, p2}, Lcdk;->a(ZLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported field for contains comparison: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcje;)Ljava/lang/Object;
    .locals 8

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-static {p1}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field unavailable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcje;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, v6}, Lcjd;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcno;

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v3, "%s IS NOT NULL"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s IS NULL"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    return-object v1
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Lcje;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-static {p2}, Lcjb;->a(Lcje;)Lcjd;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field unavailable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcje;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0, p3}, Lcjd;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcle;->e:Lcje;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcnn;->b:Lcje;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcno;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s=?"

    new-array v3, v7, [Ljava/lang/Object;

    sget-object v4, Lceg;->l:Lceg;

    invoke-virtual {v4}, Lceg;->a()Lcdp;

    move-result-object v4

    invoke-virtual {v4}, Lcdp;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "application/vnd.google-apps.folder"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v3, "%s!=?"

    new-array v4, v7, [Ljava/lang/Object;

    sget-object v5, Lceg;->l:Lceg;

    invoke-virtual {v5}, Lceg;->a()Lcdp;

    move-result-object v5

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/vnd.google-apps.folder"

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {v0, p3}, Lcjd;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->a:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcle;->e:Lcje;

    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcfs;->a:Lcfs;

    invoke-virtual {v0}, Lcfs;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s!=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    goto :goto_1

    :cond_3
    new-instance v0, Lcno;

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v3, "%s=?"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s!=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "0"

    invoke-direct {v3, v2, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1, v3}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    goto :goto_1

    :cond_4
    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s!=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->b:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s<?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s>=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_6
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->c:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s<=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s>?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_7
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->d:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s>?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s<=?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->e:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s>=?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s<?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_9
    sget-object v1, Lcom/google/android/gms/drive/query/internal/Operator;->i:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lcno;

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v4, "%s LIKE ?"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v5, "%s NOT LIKE ?"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "%"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "%"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v3, v4}, Lcno;-><init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    move-object v0, v1

    goto/16 :goto_1

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported operator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Ljava/util/List;)Ljava/lang/Object;
    .locals 3

    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->f:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcnl;

    sget-object v1, Lcev;->a:Lcev;

    sget-object v2, Lcev;->b:Lcev;

    invoke-direct {v0, v1, v2, p2}, Lcnl;-><init>(Lcev;Lcev;Ljava/util/Collection;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->g:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcnl;

    sget-object v1, Lcev;->b:Lcev;

    sget-object v2, Lcev;->a:Lcev;

    invoke-direct {v0, v1, v2, p2}, Lcnl;-><init>(Lcev;Lcev;Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not implemented logical operation:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcnk;

    invoke-interface {p1}, Lcnk;->a()Lcnk;

    move-result-object v0

    return-object v0
.end method
