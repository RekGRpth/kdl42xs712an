.class public final Lhtk;
.super Lhtf;
.source "SourceFile"


# instance fields
.field private final n:I

.field private final o:I


# direct methods
.method public constructor <init>(JIIIIIIILjava/util/Collection;IIII)V
    .locals 14

    move-object v1, p0

    move-wide v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p10

    move/from16 v9, p11

    move/from16 v10, p12

    move/from16 v11, p13

    move/from16 v12, p14

    move/from16 v13, p9

    invoke-direct/range {v1 .. v13}, Lhtf;-><init>(JIIIILjava/util/Collection;IIIII)V

    move/from16 v0, p7

    iput v0, p0, Lhtk;->n:I

    const v1, 0x7fffffff

    move/from16 v0, p8

    if-eq v0, v1, :cond_0

    if-nez p8, :cond_1

    :cond_0
    const/16 p8, -0x1

    :cond_1
    move/from16 v0, p8

    iput v0, p0, Lhtk;->o:I

    return-void
.end method


# virtual methods
.method public final a(JI)Lhtf;
    .locals 15

    new-instance v0, Lhtk;

    iget v3, p0, Lhtk;->k:I

    iget v4, p0, Lhtk;->b:I

    iget v5, p0, Lhtk;->c:I

    iget v6, p0, Lhtk;->d:I

    iget v7, p0, Lhtk;->n:I

    iget v8, p0, Lhtk;->o:I

    iget-object v10, p0, Lhtk;->j:Ljava/util/Collection;

    iget v11, p0, Lhtk;->e:I

    iget v12, p0, Lhtk;->f:I

    iget v13, p0, Lhtk;->g:I

    iget v14, p0, Lhtk;->h:I

    move-wide/from16 v1, p1

    move/from16 v9, p3

    invoke-direct/range {v0 .. v14}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lhtk;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lhtk;->j()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtk;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtk;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtk;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtk;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhtk;->m:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lhtk;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Livi;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lhtk;->n:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    iget v0, p0, Lhtk;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x8

    iget v1, p0, Lhtk;->o:I

    invoke-virtual {p1, v0, v1}, Livi;->e(II)Livi;

    :cond_0
    return-void
.end method

.method public final a(Lhtf;)Z
    .locals 5

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v0, p1, Lhtk;

    if-eqz v0, :cond_0

    check-cast p1, Lhtk;

    iget v0, p1, Lhtk;->o:I

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    iget v3, p0, Lhtk;->o:I

    if-eq v3, v4, :cond_2

    move v3, v1

    :goto_1
    if-ne v0, v3, :cond_0

    iget v0, p0, Lhtk;->n:I

    iget v3, p1, Lhtk;->n:I

    if-ne v0, v3, :cond_0

    iget v0, p0, Lhtk;->o:I

    iget v3, p1, Lhtk;->o:I

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1
.end method

.method final b()Z
    .locals 2

    iget v0, p0, Lhtk;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " lac: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lhtk;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " psc: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhtk;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    instance-of v0, p1, Lhtk;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lhtk;

    iget v2, p0, Lhtk;->n:I

    iget v3, v0, Lhtk;->n:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lhtk;->o:I

    iget v0, v0, Lhtk;->o:I

    if-ne v2, v0, :cond_0

    invoke-super {p0, p1}, Lhtf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    invoke-super {p0}, Lhtf;->hashCode()I

    move-result v0

    iget v1, p0, Lhtk;->n:I

    mul-int/lit16 v1, v1, 0xe75

    xor-int/2addr v0, v1

    iget v1, p0, Lhtk;->o:I

    mul-int/lit16 v1, v1, 0xa7f

    xor-int/2addr v0, v1

    return v0
.end method
