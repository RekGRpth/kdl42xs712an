.class public final Ljep;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljel;

.field public b:Ljeq;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v1, p0, Ljep;->a:Ljel;

    iput-object v1, p0, Ljep;->b:Ljeq;

    iput v0, p0, Ljep;->c:I

    iput v0, p0, Ljep;->d:I

    const/4 v0, -0x1

    iput v0, p0, Ljep;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ljep;->a:Ljel;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljep;->a:Ljel;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljep;->b:Ljeq;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljep;->b:Ljeq;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Ljep;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Ljep;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Ljep;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Ljep;->d:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Ljep;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljep;->a:Ljel;

    if-nez v0, :cond_1

    new-instance v0, Ljel;

    invoke-direct {v0}, Ljel;-><init>()V

    iput-object v0, p0, Ljep;->a:Ljel;

    :cond_1
    iget-object v0, p0, Ljep;->a:Ljel;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Ljep;->b:Ljeq;

    if-nez v0, :cond_2

    new-instance v0, Ljeq;

    invoke-direct {v0}, Ljeq;-><init>()V

    iput-object v0, p0, Ljep;->b:Ljeq;

    :cond_2
    iget-object v0, p0, Ljep;->b:Ljeq;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljep;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljep;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Ljep;->a:Ljel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljep;->a:Ljel;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Ljep;->b:Ljeq;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljep;->b:Ljeq;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget v0, p0, Ljep;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Ljep;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_2
    iget v0, p0, Ljep;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Ljep;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_3
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljep;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljep;

    iget-object v2, p0, Ljep;->a:Ljel;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljep;->a:Ljel;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljep;->a:Ljel;

    iget-object v3, p1, Ljep;->a:Ljel;

    invoke-virtual {v2, v3}, Ljel;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljep;->b:Ljeq;

    if-nez v2, :cond_5

    iget-object v2, p1, Ljep;->b:Ljeq;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljep;->b:Ljeq;

    iget-object v3, p1, Ljep;->b:Ljeq;

    invoke-virtual {v2, v3}, Ljeq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Ljep;->c:I

    iget v3, p1, Ljep;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Ljep;->d:I

    iget v3, p1, Ljep;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljep;->a:Ljel;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljep;->b:Ljeq;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Ljep;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Ljep;->d:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljep;->a:Ljel;

    invoke-virtual {v0}, Ljel;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ljep;->b:Ljeq;

    invoke-virtual {v1}, Ljeq;->hashCode()I

    move-result v1

    goto :goto_1
.end method
