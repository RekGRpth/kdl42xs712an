.class public final Lixj;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lixh;Lixh;Lixh;)V
    .locals 13

    iget v0, p0, Lixh;->b:I

    iget v1, p1, Lixh;->a:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of columns in A doesn\'t match number of rows in B (%d != %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p2, Lixh;->a:I

    iget v1, p0, Lixh;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lixh;->b:I

    iget v1, p1, Lixh;->b:I

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "C has wrong dimensions (should be %d x %d but is %d x %d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p2, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p2, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-eq p2, p0, :cond_3

    if-ne p2, p1, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "c must be a different object from a or b"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v2, p2, Lixh;->c:I

    if-ge v1, v2, :cond_7

    iget v2, p1, Lixh;->a:I

    add-int v8, v0, v2

    const/4 v2, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    iget v4, p0, Lixh;->a:I

    if-ge v3, v4, :cond_6

    const-wide/16 v4, 0x0

    move-wide v6, v4

    move v5, v3

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_5

    iget-object v9, p0, Lixh;->d:[D

    aget-wide v9, v9, v5

    iget-object v11, p1, Lixh;->d:[D

    aget-wide v11, v11, v4

    mul-double/2addr v9, v11

    add-double/2addr v6, v9

    iget v9, p0, Lixh;->a:I

    add-int/2addr v5, v9

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-object v4, p2, Lixh;->d:[D

    aput-wide v6, v4, v2

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iget v2, p2, Lixh;->a:I

    add-int/2addr v1, v2

    iget v2, p1, Lixh;->a:I

    add-int/2addr v0, v2

    goto :goto_0

    :cond_7
    return-void
.end method

.method public static b(Lixh;Lixh;Lixh;)V
    .locals 13

    iget v0, p0, Lixh;->b:I

    iget v1, p1, Lixh;->a:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of columns in A doesn\'t match number of rows in B (%d != %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p2, Lixh;->a:I

    iget v1, p0, Lixh;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lixh;->b:I

    iget v1, p1, Lixh;->b:I

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "C has wrong dimensions (should be %d x %d but is %d x %d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p2, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p2, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-eq p2, p0, :cond_3

    if-ne p2, p1, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "c must be a different object from a or b"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v2, p2, Lixh;->c:I

    if-ge v1, v2, :cond_7

    iget v2, p1, Lixh;->a:I

    add-int v8, v0, v2

    const/4 v2, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    iget v4, p0, Lixh;->a:I

    if-ge v3, v4, :cond_6

    const-wide/16 v4, 0x0

    move-wide v6, v4

    move v5, v3

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_5

    iget-object v9, p0, Lixh;->d:[D

    aget-wide v9, v9, v5

    iget-object v11, p1, Lixh;->d:[D

    aget-wide v11, v11, v4

    mul-double/2addr v9, v11

    add-double/2addr v6, v9

    iget v9, p0, Lixh;->a:I

    add-int/2addr v5, v9

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-object v4, p2, Lixh;->d:[D

    aget-wide v9, v4, v2

    add-double v5, v9, v6

    aput-wide v5, v4, v2

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iget v2, p2, Lixh;->a:I

    add-int/2addr v1, v2

    iget v2, p1, Lixh;->a:I

    add-int/2addr v0, v2

    goto :goto_0

    :cond_7
    return-void
.end method

.method public static c(Lixh;Lixh;Lixh;)V
    .locals 13

    iget v0, p0, Lixh;->a:I

    iget v1, p1, Lixh;->a:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of columns in A\' doesn\'t match number of rows in B (%d != %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p2, Lixh;->a:I

    iget v1, p0, Lixh;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lixh;->b:I

    iget v1, p1, Lixh;->b:I

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "C has wrong dimensions (should be %d x %d but is %d x %d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p2, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p2, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-eq p2, p0, :cond_3

    if-ne p2, p1, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "c must be a different object from a or b"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v2, p2, Lixh;->c:I

    if-ge v1, v2, :cond_7

    iget v2, p1, Lixh;->a:I

    add-int v8, v0, v2

    const/4 v2, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    iget v4, p0, Lixh;->c:I

    if-ge v3, v4, :cond_6

    const-wide/16 v4, 0x0

    move-wide v6, v4

    move v5, v3

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_5

    iget-object v9, p0, Lixh;->d:[D

    aget-wide v9, v9, v5

    iget-object v11, p1, Lixh;->d:[D

    aget-wide v11, v11, v4

    mul-double/2addr v9, v11

    add-double/2addr v6, v9

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    iget-object v4, p2, Lixh;->d:[D

    aput-wide v6, v4, v2

    iget v4, p0, Lixh;->a:I

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iget v2, p2, Lixh;->a:I

    add-int/2addr v1, v2

    iget v2, p1, Lixh;->a:I

    add-int/2addr v0, v2

    goto :goto_0

    :cond_7
    return-void
.end method

.method public static d(Lixh;Lixh;Lixh;)V
    .locals 13

    iget v0, p0, Lixh;->b:I

    iget v1, p1, Lixh;->b:I

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of columns in A doesn\'t match number of rows in B\' (%d != %d)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p2, Lixh;->a:I

    iget v1, p0, Lixh;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lixh;->b:I

    iget v1, p1, Lixh;->a:I

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "C has wrong dimensions (should be %d x %d but is %d x %d)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p1, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, p2, Lixh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p2, Lixh;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-eq p2, p0, :cond_3

    if-ne p2, p1, :cond_4

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "c must be a different object from a or b"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget v2, p2, Lixh;->c:I

    if-ge v1, v2, :cond_7

    iget v8, p1, Lixh;->c:I

    const/4 v2, 0x0

    move v3, v2

    move v2, v1

    :goto_1
    iget v4, p0, Lixh;->a:I

    if-ge v3, v4, :cond_6

    const-wide/16 v4, 0x0

    move-wide v6, v4

    move v5, v3

    move v4, v0

    :goto_2
    if-ge v4, v8, :cond_5

    iget-object v9, p0, Lixh;->d:[D

    aget-wide v9, v9, v5

    iget-object v11, p1, Lixh;->d:[D

    aget-wide v11, v11, v4

    mul-double/2addr v9, v11

    add-double/2addr v6, v9

    iget v9, p0, Lixh;->a:I

    add-int/2addr v5, v9

    iget v9, p1, Lixh;->a:I

    add-int/2addr v4, v9

    goto :goto_2

    :cond_5
    iget-object v4, p2, Lixh;->d:[D

    aput-wide v6, v4, v2

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iget v2, p2, Lixh;->a:I

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    return-void
.end method

.method public static e(Lixh;Lixh;Lixh;)V
    .locals 6

    invoke-static {p0, p1, p2}, Lixj;->g(Lixh;Lixh;Lixh;)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lixh;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p2, Lixh;->d:[D

    iget-object v2, p0, Lixh;->d:[D

    aget-wide v2, v2, v0

    iget-object v4, p1, Lixh;->d:[D

    aget-wide v4, v4, v0

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static f(Lixh;Lixh;Lixh;)V
    .locals 6

    invoke-static {p0, p1, p2}, Lixj;->g(Lixh;Lixh;Lixh;)V

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lixh;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p2, Lixh;->d:[D

    iget-object v2, p0, Lixh;->d:[D

    aget-wide v2, v2, v0

    iget-object v4, p1, Lixh;->d:[D

    aget-wide v4, v4, v0

    sub-double/2addr v2, v4

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static g(Lixh;Lixh;Lixh;)V
    .locals 8

    const/4 v2, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v0, p0, Lixh;->b:I

    iget v1, p1, Lixh;->b:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lixh;->a:I

    iget v1, p1, Lixh;->a:I

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Dimensions of A and B don\'t match (A is %d x %d and B is %d x %d)"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lixh;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lixh;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p1, Lixh;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, p1, Lixh;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p2, Lixh;->a:I

    iget v1, p0, Lixh;->a:I

    if-ne v0, v1, :cond_2

    iget v0, p2, Lixh;->b:I

    iget v1, p0, Lixh;->b:I

    if-eq v0, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "output has wrong dimensions (should be %d x %d but is %d x %d)"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lixh;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lixh;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p2, Lixh;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    iget v3, p2, Lixh;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method
