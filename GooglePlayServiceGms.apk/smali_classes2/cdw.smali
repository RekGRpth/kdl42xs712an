.class public final Lcdw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcou;


# instance fields
.field final synthetic a:Lcdu;

.field private b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method public constructor <init>(Lcdu;)V
    .locals 0

    iput-object p1, p0, Lcdw;->a:Lcdu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcdw;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcdw;->a:Lcdu;

    iget-object v0, v0, Lcdu;->a:Lcdz;

    invoke-virtual {v0}, Lcdz;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcdw;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcdw;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    sget-object v0, Lcds;->a:Lbgm;

    iget-object v1, p0, Lcdw;->a:Lcdu;

    invoke-virtual {v1}, Lcdu;->g()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbgm;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcdw;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcdw;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method
