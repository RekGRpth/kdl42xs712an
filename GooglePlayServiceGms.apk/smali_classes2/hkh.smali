.class final Lhkh;
.super Lhsi;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhkg;


# direct methods
.method private constructor <init>(Lhkg;)V
    .locals 0

    iput-object p1, p0, Lhkh;->a:Lhkg;

    invoke-direct {p0}, Lhsi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lhkg;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhkh;-><init>(Lhkg;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-boolean v0, v0, Lhkg;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->p:Livi;

    if-eqz v0, :cond_0

    new-instance v0, Livi;

    sget-object v1, Lihj;->F:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iget-object v1, p0, Lhkh;->a:Lhkg;

    iget-object v1, v1, Lhkg;->p:Livi;

    invoke-virtual {v1}, Livi;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Livi;->a([B)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->a(I)V

    iget-object v1, p0, Lhkh;->a:Lhkg;

    iget-object v1, v1, Lhkg;->n:Lhqy;

    invoke-interface {v1, v0}, Lhqy;->a(Livi;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    invoke-interface {v0}, Lhqy;->c()V

    return-void

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->a:Ljava/lang/String;

    const-string v1, "Unable to attach customized data."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-wide v0, v0, Lhkg;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lilv;->b(Z)V

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    invoke-interface {v0}, Lhqy;->b()V

    :cond_0
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to write file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lhsd;)V
    .locals 8

    const/4 v1, 0x1

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-wide v2, v0, Lhkg;->m:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lilv;->b(Z)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->a:Ljava/lang/String;

    const-string v2, "Sensor collection completed successfully or unsuccessfully."

    invoke-static {v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v5

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lhsd;->a()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v7, 0x3

    :goto_1
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v1, p0, Lhkh;->a:Lhkg;

    iget-wide v1, v1, Lhkg;->m:J

    iget-object v3, p0, Lhkh;->a:Lhkg;

    iget-wide v3, v3, Lhkg;->m:J

    invoke-static/range {v0 .. v7}, Lhkg;->a(Lhkg;JJJI)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-boolean v0, v0, Lhkg;->q:Z

    if-eqz v0, :cond_3

    const/4 v7, 0x2

    goto :goto_1

    :cond_3
    move v7, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    const-wide/16 v3, -0x1

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->n:Lhqy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-wide v0, v0, Lhkg;->m:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lilv;->b(Z)V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v0, v0, Lhkg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to collect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhkh;->a:Lhkg;

    iget-object v1, p0, Lhkh;->a:Lhkg;

    iget-wide v1, v1, Lhkg;->m:J

    iget-object v5, p0, Lhkh;->a:Lhkg;

    iget-object v5, v5, Lhkg;->b:Lidu;

    invoke-interface {v5}, Lidu;->j()Licm;

    move-result-object v5

    invoke-interface {v5}, Licm;->a()J

    move-result-wide v5

    const/16 v7, 0x1d

    invoke-static/range {v0 .. v7}, Lhkg;->a(Lhkg;JJJI)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
