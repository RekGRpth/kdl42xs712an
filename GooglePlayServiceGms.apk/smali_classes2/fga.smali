.class final Lfga;
.super Lfgu;
.source "SourceFile"


# instance fields
.field final synthetic c:Z

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-boolean p8, p0, Lfga;->c:Z

    iput-object p9, p0, Lfga;->d:Ljava/lang/String;

    iput-object p10, p0, Lfga;->e:Ljava/lang/String;

    iput-object p11, p0, Lfga;->f:Ljava/lang/String;

    iput-object p12, p0, Lfga;->g:Ljava/lang/String;

    iput-object p13, p0, Lfga;->h:Ljava/lang/String;

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 6

    iget-boolean v0, p0, Lfga;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfga;->d:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lffe;->a(Lffa;Ljava/lang/String;)V

    invoke-static {p1}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    iget-object v2, p0, Lfga;->f:Ljava/lang/String;

    iget-object v3, p0, Lfga;->d:Ljava/lang/String;

    invoke-static {v3}, Lfdl;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    :goto_0
    iget-object v0, p0, Lfga;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfga;->h:Ljava/lang/String;

    move-object v1, v0

    :goto_1
    iget-object v2, p0, Lfga;->e:Ljava/lang/String;

    iget-object v0, p0, Lfga;->d:Ljava/lang/String;

    iget-boolean v3, p0, Lfga;->c:Z

    new-instance v4, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgih;

    invoke-direct {v0}, Lgih;-><init>()V

    iput-object v4, v0, Lgih;->b:Ljava/util/List;

    iget-object v4, v0, Lgih;->c:Ljava/util/Set;

    const/16 v5, 0xd

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lgih;->a()Lgig;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    new-instance v4, Lbmt;

    invoke-direct {v4, p1}, Lbmt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v4

    if-eqz v3, :cond_3

    sget-object v2, Lfnx;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_2
    invoke-virtual {v4, v2}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v2

    sget-object v3, Lfnp;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v2, v3}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-static {p1, v0}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    iget-object v0, p0, Lfga;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfga;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    iget-object v1, p0, Lfga;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfhz;->c(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lffc;->c:Lffc;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    iget-object v0, p0, Lfga;->d:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lffe;->b(Lffa;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfga;->g:Ljava/lang/String;

    move-object v1, v0

    goto :goto_1

    :cond_3
    sget-object v2, Lfnx;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_2
.end method
