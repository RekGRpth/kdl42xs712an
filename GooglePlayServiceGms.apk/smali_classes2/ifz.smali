.class final Lifz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhuf;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 7

    sget-object v0, Ligx;->d:Lhuf;

    invoke-interface {v0, p1}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligx;

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    new-array v3, v2, [Ljava/lang/String;

    new-array v4, v2, [F

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    aput v5, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lify;

    invoke-direct {v1, v0, v3, v4}, Lify;-><init>(Ligx;[Ljava/lang/String;[F)V

    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 4

    check-cast p1, Lify;

    sget-object v0, Ligx;->d:Lhuf;

    invoke-virtual {p1}, Lify;->a()Ligx;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    invoke-virtual {p1}, Lify;->b()I

    move-result v1

    invoke-interface {p2, v1}, Ljava/io/DataOutput;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Lify;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lify;->b(I)F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-interface {p2, v2}, Ljava/io/DataOutput;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
