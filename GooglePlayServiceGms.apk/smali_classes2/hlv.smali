.class final Lhlv;
.super Lhlz;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhlu;

.field private c:J

.field private d:J

.field private e:I


# direct methods
.method private constructor <init>(Lhlu;)V
    .locals 2

    iput-object p1, p0, Lhlv;->a:Lhlu;

    invoke-direct {p0, p1}, Lhlz;-><init>(Lhlu;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhlv;->d:J

    const/4 v0, 0x0

    iput v0, p0, Lhlv;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lhlu;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhlv;-><init>(Lhlu;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhlv;->c:J

    iget-wide v0, p0, Lhlv;->c:J

    iput-wide v0, p0, Lhlv;->d:J

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    const/16 v1, 0x78

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lhkr;->a(IZ)V

    return-void
.end method

.method final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lhlv;->a:Lhlu;

    invoke-static {p1}, Lhlu;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhlv;->d:J

    iget v0, p0, Lhlv;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhlv;->e:I

    iget v0, p0, Lhlv;->e:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lhlv;->c:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x2bf20

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lhlv;->a:Lhlu;

    new-instance v1, Lhlw;

    iget-object v2, p0, Lhlv;->a:Lhlu;

    invoke-direct {v1, v2, v4}, Lhlw;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhlv;->a:Lhlu;

    invoke-static {p1}, Lhlu;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lhlv;->d:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    :cond_2
    iget-object v0, p0, Lhlv;->a:Lhlu;

    new-instance v1, Lhmc;

    iget-object v2, p0, Lhlv;->a:Lhlu;

    invoke-direct {v1, v2, v4}, Lhmc;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    goto :goto_0
.end method

.method protected final a(Lhlz;)V
    .locals 2

    invoke-super {p0, p1}, Lhlz;->a(Lhlz;)V

    iget-object v0, p0, Lhlv;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Lhkr;->b(I)V

    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "ConfirmingInVehicle"

    return-object v0
.end method
