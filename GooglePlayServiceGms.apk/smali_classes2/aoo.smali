.class public final Laoo;
.super Laom;
.source "SourceFile"


# instance fields
.field private final b:Ljby;


# direct methods
.method public constructor <init>(Ljby;)V
    .locals 0

    invoke-direct {p0}, Laom;-><init>()V

    iput-object p1, p0, Laoo;->b:Ljby;

    invoke-virtual {p0}, Laoo;->a()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Laoo;->b:Ljby;

    iget-object v3, v2, Ljby;->e:Ljbo;

    iget-object v2, p0, Laoo;->b:Ljby;

    iget-object v4, v2, Ljby;->c:Ljbx;

    if-eqz v3, :cond_0

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, v3, Ljbo;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v5, Laoh;->b:Ljava/lang/String;

    iget-object v6, v3, Ljbo;->b:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v3}, Ljbo;->c()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v3, v0}, Ljbo;->a(I)Ljbt;

    move-result-object v2

    iget-boolean v2, v2, Ljbt;->a:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v5, Laoh;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljbo;->a(I)Ljbt;

    move-result-object v6

    iget-object v6, v6, Ljbt;->b:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, Ljbo;->c()I

    move-result v2

    if-le v2, v1, :cond_4

    invoke-virtual {v3, v1}, Ljbo;->a(I)Ljbt;

    move-result-object v2

    iget-boolean v2, v2, Ljbt;->a:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v5, Laoh;->d:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljbo;->a(I)Ljbt;

    move-result-object v6

    iget-object v6, v6, Ljbt;->b:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-boolean v2, v4, Ljbx;->c:Z

    if-eqz v2, :cond_9

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2}, Ljbn;->c()I

    move-result v2

    if-lez v2, :cond_9

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v0}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget-boolean v2, v2, Ljbs;->a:Z

    if-eqz v2, :cond_9

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v0}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget v2, v2, Ljbs;->b:I

    const/4 v5, 0x3

    if-ne v2, v5, :cond_9

    move v2, v1

    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {v3}, Ljbo;->c()I

    move-result v2

    if-lez v2, :cond_6

    invoke-virtual {v3, v0}, Ljbo;->a(I)Ljbt;

    move-result-object v2

    iget-boolean v5, v2, Ljbt;->g:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v6, Laog;->b:Ljava/lang/String;

    iget-object v7, v2, Ljbt;->h:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget-boolean v5, v2, Ljbt;->i:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v6, Laog;->c:Ljava/lang/String;

    iget-object v2, v2, Ljbt;->j:Ljava/lang/String;

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget-boolean v2, v4, Ljbx;->c:Z

    if-eqz v2, :cond_a

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2}, Ljbn;->c()I

    move-result v2

    if-lez v2, :cond_a

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v0}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget-boolean v2, v2, Ljbs;->a:Z

    if-eqz v2, :cond_a

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v0}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget v2, v2, Ljbs;->b:I

    const/4 v5, 0x2

    if-ne v2, v5, :cond_a

    move v2, v1

    :goto_2
    if-eqz v2, :cond_7

    invoke-virtual {v3}, Ljbo;->c()I

    move-result v2

    if-lez v2, :cond_7

    invoke-virtual {v3, v0}, Ljbo;->a(I)Ljbt;

    move-result-object v2

    iget-boolean v2, v2, Ljbt;->e:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v5, Laof;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljbo;->a(I)Ljbt;

    move-result-object v6

    iget-object v6, v6, Ljbt;->f:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-boolean v2, v4, Ljbx;->c:Z

    if-eqz v2, :cond_8

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2}, Ljbn;->c()I

    move-result v2

    if-le v2, v1, :cond_8

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v1}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget-boolean v2, v2, Ljbs;->a:Z

    if-eqz v2, :cond_8

    iget-object v2, v4, Ljbx;->d:Ljbn;

    invoke-virtual {v2, v1}, Ljbn;->a(I)Ljbs;

    move-result-object v2

    iget v2, v2, Ljbs;->b:I

    if-ne v2, v1, :cond_8

    move v0, v1

    :cond_8
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Ljbo;->c()I

    move-result v0

    if-le v0, v1, :cond_0

    invoke-virtual {v3, v1}, Ljbo;->a(I)Ljbt;

    move-result-object v0

    iget-boolean v0, v0, Ljbt;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laoo;->a:Landroid/os/Bundle;

    sget-object v2, Laoe;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljbo;->a(I)Ljbt;

    move-result-object v1

    iget-object v1, v1, Ljbt;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move v2, v0

    goto/16 :goto_1

    :cond_a
    move v2, v0

    goto :goto_2
.end method
