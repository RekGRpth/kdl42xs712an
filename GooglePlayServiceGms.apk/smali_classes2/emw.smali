.class final Lemw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbdx;
.implements Lbel;


# instance fields
.field final a:Lbdu;

.field final b:Landroid/os/ConditionVariable;

.field final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lbdw;

    invoke-direct {v0, p1}, Lbdw;-><init>(Landroid/content/Context;)V

    sget-object v1, Lahh;->b:Lbdm;

    invoke-virtual {v0, v1}, Lbdw;->a(Lbdm;)Lbdw;

    move-result-object v0

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    iput-object v0, p0, Lemw;->a:Lbdu;

    iget-object v0, p0, Lemw;->a:Lbdu;

    invoke-interface {v0, p0}, Lbdu;->a(Lbdx;)V

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lemw;->b:Landroid/os/ConditionVariable;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lemw;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public final synthetic a(Lbek;)V
    .locals 2

    check-cast p1, Lajb;

    invoke-interface {p1}, Lajb;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lajb;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lemw;->d:Landroid/os/ParcelFileDescriptor;

    :goto_0
    iget-object v0, p0, Lemw;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void

    :cond_0
    const-string v0, "Failed to get file descriptor. Status code: %d."

    invoke-interface {p1}, Lajb;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lehe;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    const-string v0, "Usage report connection failed."

    invoke-static {v0}, Lehe;->e(Ljava/lang/String;)I

    iget-object v0, p0, Lemw;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lemw;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Connected after timeout expired."

    invoke-static {v0}, Lehe;->e(Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lahh;->c:Laja;

    iget-object v1, p0, Lemw;->a:Lbdu;

    invoke-interface {v0, v1}, Laja;->a(Lbdu;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method
