.class public final Lhxd;
.super Lhxe;
.source "SourceFile"


# instance fields
.field private final a:Landroid/location/LocationListener;

.field private final b:Lhwk;

.field private final c:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Lhwk;Landroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0}, Lhxe;-><init>()V

    iput-object p2, p0, Lhxd;->a:Landroid/location/LocationListener;

    iput-object p1, p0, Lhxd;->b:Lhwk;

    iput-object p3, p0, Lhxd;->c:Landroid/os/Looper;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x0

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhxd;->b:Lhwk;

    invoke-virtual {v0}, Lhwk;->a()Landroid/location/LocationManager;

    move-result-object v0

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhxd;->b:Lhwk;

    const-string v1, "passive"

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lhxd;->a:Landroid/location/LocationListener;

    iget-object v5, p0, Lhxd;->c:Landroid/os/Looper;

    iget-object v6, p0, Lhxe;->i:Ljava/util/Collection;

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lhwk;->a(Ljava/lang/String;JLandroid/location/LocationListener;Landroid/os/Looper;Ljava/util/Collection;Z)V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Passive provider enabled"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhxd;->b:Lhwk;

    invoke-virtual {v0}, Lhwk;->a()Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lhxd;->a:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    const-string v0, "GCoreFlp"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Passive provider Disabled"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Passive location ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhxd;->a(Ljava/lang/StringBuilder;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
