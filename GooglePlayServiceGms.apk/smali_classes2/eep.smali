.class public final Leep;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ldhu;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Ldhu;->a()Ldhv;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "display_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "primary_category"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "secondary_category"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_description"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "developer_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "play_enabled_game"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "gameplay_acl_status"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "achievement_total_count"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "leaderboard_count"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_icon_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_icon_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_hi_res_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "game_hi_res_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "featured_image_uri"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "featured_image_url"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "muted"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "installed"

    sget-object v2, Ldhw;->c:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "package_name"

    sget-object v2, Ldhw;->a:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "real_time_support"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    const-string v1, "turn_based_support"

    sget-object v2, Ldhw;->b:Ldhw;

    invoke-virtual {v0, v1, v2}, Ldhv;->a(Ljava/lang/String;Ldhw;)Ldhv;

    move-result-object v0

    invoke-virtual {v0}, Ldhv;->a()Ldhu;

    move-result-object v0

    sput-object v0, Leep;->a:Ldhu;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcun;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.APP_ID"

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, p0, p1, v1}, Lcun;->k(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "com.google.android.play.games"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GamesDataUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No game ID found for package "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
