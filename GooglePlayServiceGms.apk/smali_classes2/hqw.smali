.class final Lhqw;
.super Lhrx;
.source "SourceFile"

# interfaces
.implements Landroid/location/GpsStatus$Listener;
.implements Landroid/location/LocationListener;


# instance fields
.field private final a:Lhsu;

.field private final b:Ljava/lang/String;

.field private g:Z

.field private final h:Z

.field private final i:Z

.field private j:Landroid/location/GpsStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZZLhsu;Lhqm;Lhqq;Limb;Lilx;)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lhrx;-><init>(Landroid/content/Context;Lhqm;Lhqq;Limb;Lilx;)V

    iput-boolean v6, p0, Lhqw;->g:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhqw;->j:Landroid/location/GpsStatus;

    iput-boolean p2, p0, Lhqw;->h:Z

    iput-boolean p3, p0, Lhqw;->i:Z

    if-nez p4, :cond_0

    new-instance v0, Lhsu;

    invoke-direct {v0, p1, v6}, Lhsu;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lhqw;->a:Lhsu;

    :goto_0
    iget-object v0, p0, Lhqw;->c:Limb;

    iget-object v0, v0, Limb;->a:Ljava/lang/String;

    iput-object v0, p0, Lhqw;->b:Ljava/lang/String;

    return-void

    :cond_0
    iput-object p4, p0, Lhqw;->a:Lhsu;

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 4

    iget-boolean v0, p0, Lhqw;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqw;->a:Lhsu;

    iget-object v1, p0, Lhqw;->b:Ljava/lang/String;

    sget-object v2, Lhsv;->c:Lhsv;

    invoke-virtual {v0, v1, v2}, Lhsu;->a(Ljava/lang/String;Lhsv;)V

    iget-object v0, v0, Lhsu;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    :cond_0
    iget-object v0, p0, Lhqw;->a:Lhsu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhqw;->a:Lhsu;

    iget-object v1, p0, Lhqw;->b:Ljava/lang/String;

    const-string v2, "gps"

    iget-object v3, p0, Lhrx;->d:Lhqm;

    invoke-virtual {v3}, Lhqm;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p0, v3}, Lhsu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    iget-object v0, p0, Lhqw;->e:Lhqq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhqw;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->g()V

    :cond_2
    return-void
.end method

.method protected final b()V
    .locals 3

    iget-boolean v0, p0, Lhqw;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqw;->a:Lhsu;

    iget-object v1, p0, Lhqw;->b:Ljava/lang/String;

    sget-object v2, Lhsv;->d:Lhsv;

    invoke-virtual {v0, v1, v2}, Lhsu;->a(Ljava/lang/String;Lhsv;)V

    iget-object v0, v0, Lhsu;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeGpsStatusListener(Landroid/location/GpsStatus$Listener;)V

    :cond_0
    iget-object v0, p0, Lhqw;->a:Lhsu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhqw;->a:Lhsu;

    iget-object v1, p0, Lhqw;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lhsu;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    :cond_1
    iget-object v0, p0, Lhqw;->e:Lhqq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhqw;->e:Lhqq;

    invoke-interface {v0}, Lhqq;->h()V

    :cond_2
    return-void
.end method

.method public final onGpsStatusChanged(I)V
    .locals 4

    iget-boolean v0, p0, Lhqw;->i:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lhqw;->f()V

    invoke-virtual {p0}, Lhqw;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lhqw;->a:Lhsu;

    iget-object v1, p0, Lhqw;->j:Landroid/location/GpsStatus;

    iget-object v0, v0, Lhsu;->b:Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getGpsStatus(Landroid/location/GpsStatus;)Landroid/location/GpsStatus;

    move-result-object v0

    iput-object v0, p0, Lhqw;->j:Landroid/location/GpsStatus;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lhrx;->d:Lhqm;

    iget-object v3, p0, Lhqw;->j:Landroid/location/GpsStatus;

    invoke-virtual {v2, v3, v0, v1}, Lhqm;->a(Landroid/location/GpsStatus;J)V

    sget-object v2, Lhrz;->h:Lhrz;

    invoke-virtual {p0, v2, v0, v1}, Lhqw;->b(Lhrz;J)V

    goto :goto_0
.end method

.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 6

    iget-boolean v0, p0, Lhqw;->h:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lhqw;->f()V

    invoke-virtual {p0}, Lhqw;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lhrx;->d:Lhqm;

    invoke-virtual {v2, p1, v0, v1}, Lhqm;->a(Landroid/location/Location;J)V

    sget-object v2, Lhrz;->g:Lhrz;

    invoke-virtual {p0, v2, v0, v1}, Lhqw;->b(Lhrz;J)V

    iget-boolean v0, p0, Lhqw;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhqw;->g:Z

    new-instance v0, Landroid/hardware/GeomagneticField;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Landroid/hardware/GeomagneticField;-><init>(FFFJ)V

    iget-object v1, p0, Lhrx;->d:Lhqm;

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getX()F

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getY()F

    move-result v3

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getZ()F

    move-result v4

    invoke-virtual {v0}, Landroid/hardware/GeomagneticField;->getDeclination()F

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lhqm;->a(FFFF)V

    goto :goto_0
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
