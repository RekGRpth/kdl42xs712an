.class public final Lgxs;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field d:Lgwx;

.field e:Lgwy;

.field f:Lgwd;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxs;->g:Z

    return-void
.end method

.method private J()Lint;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lgth;->a(Ljava/lang/String;)I

    move-result v4

    :try_start_0
    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    iget-object v5, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit16 v5, v5, 0x7d0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :goto_1
    new-instance v5, Lint;

    invoke-direct {v5}, Lint;-><init>()V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Lint;->b:I

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Lint;->c:I

    :cond_1
    if-eqz v4, :cond_2

    iput v4, v5, Lint;->f:I

    :cond_2
    iput-object v3, v5, Lint;->e:Ljava/lang/String;

    new-instance v0, Linu;

    invoke-direct {v0}, Linu;-><init>()V

    iput-object v0, v5, Lint;->a:Linu;

    iget-object v0, v5, Lint;->a:Linu;

    iput-object v2, v0, Linu;->a:Ljava/lang/String;

    return-object v5

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v5

    goto :goto_1
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-boolean v1, p0, Lgxs;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxs;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxs;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgxs;->d:Lgwx;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lgxs;->e:Lgwy;

    aput-object v3, v4, v0

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgxs;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgxs;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const v6, 0x7f0a0333    # com.google.android.gms.R.id.exp_year

    const v5, 0x7f0a0332    # com.google.android.gms.R.id.exp_month

    const v0, 0x7f040134    # com.google.android.gms.R.layout.wallet_fragment_no_cvc_credit_card_entry

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a032d    # com.google.android.gms.R.id.card_number

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iput-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v0, Lgww;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v2, v3, v4}, Lgww;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    new-instance v2, Lgwx;

    iget-object v3, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v4, v0}, Lgwx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgxs;->d:Lgwx;

    new-instance v2, Lgwy;

    iget-object v3, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v0}, Lgwy;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgxs;->e:Lgwy;

    new-instance v0, Lgwd;

    iget-object v2, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v2}, Lgwd;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lgxs;->f:Lgwd;

    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->d:Lgwx;

    iget-object v3, p0, Lgxs;->d:Lgwx;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->e:Lgwy;

    iget-object v3, p0, Lgxs;->e:Lgwy;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lgxs;->f:Lgwd;

    iget-object v3, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->d:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->e:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->d:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->e:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iget-object v2, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b()V

    iget-object v2, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    invoke-direct {p0}, Lgxs;->b()V

    return-object v1
.end method

.method public final a()Linv;
    .locals 2

    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Linv;->a:I

    invoke-direct {p0}, Lgxs;->J()Lint;

    move-result-object v1

    iput-object v1, v0, Linv;->b:Lint;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgxs;->g:Z

    invoke-direct {p0}, Lgxs;->b()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgxs;->g:Z

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgxs;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final i()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxs;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lgxs;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lgxs;->c:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
