.class public final Lfbq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/regex/Pattern;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lfbo;

.field private final d:Landroid/content/ContentValues;

.field private e:Z

.field private f:Z

.field private final g:Ljava/util/Set;

.field private final h:[Ljava/lang/String;

.field private final i:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[^a-zA-Z0-9\u0080-\uffff]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lfbq;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lfbo;Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lfbq;->d:Landroid/content/ContentValues;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfbq;->g:Ljava/util/Set;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lfbq;->h:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lfbq;->i:Ljava/lang/StringBuilder;

    iput-object p2, p0, Lfbq;->a:Landroid/content/Context;

    iput-object p1, p0, Lfbq;->b:Lfbo;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfbq;->a(Ljava/util/Locale;)V

    return-void
.end method

.method private a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lfbq;->i:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    array-length v1, p1

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    iget-object v3, p0, Lfbq;->i:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfbq;->i:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 3

    iget-object v0, p0, Lfbq;->b:Lfbo;

    const-string v1, "searchIndexVersion"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lfbn;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v3, 0x0

    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Rebuilding index..."

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbq;->a:Landroid/content/Context;

    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Rebuilding index..."

    invoke-static {v0, v1, v2, v3}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    :try_start_0
    iget-object v0, p0, Lfbq;->b:Lfbo;

    invoke-virtual {v0, p1}, Lfbo;->a(Lfbn;)V

    const-string v0, "people"

    sget-object v1, Lfbr;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :goto_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    move v6, v7

    :goto_1
    const/16 v0, 0x32

    if-ge v6, v0, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;Ljava/lang/String;Ljava/lang/String;J)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lfbn;->c()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lfbq;->a:Landroid/content/Context;

    const-string v2, "PeopleSearchIndexManage"

    const-string v3, "Error rebuilding search index."

    invoke-static {v1, v2, v3, v0}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Error rebuilding search index."

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-void

    :cond_1
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lfbq;->a:Landroid/content/Context;

    const-string v1, "PeopleSearchIndexManage"

    const-string v2, "Rebuilding index done."

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Rebuilding index done."

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2
.end method

.method private a(Lfbn;JILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lfbq;->d:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    iget-object v0, p0, Lfbq;->d:Landroid/content/ContentValues;

    const-string v1, "person_id"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lfbq;->d:Landroid/content/ContentValues;

    const-string v1, "kind"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lfbq;->d:Landroid/content/ContentValues;

    const-string v1, "value"

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_index"

    iget-object v1, p0, Lfbq;->d:Landroid/content/ContentValues;

    invoke-virtual {p1, v0, v1}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method private a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V
    .locals 6

    const/4 v4, 0x1

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v4

    :goto_0
    if-nez v0, :cond_1

    invoke-interface {p4, p5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {p5}, Lfbj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JILjava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lfbn;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 13

    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const-string v1, "SELECT name as data, given_name as data2,family_name as data3,middle_name as data4,1 as kind FROM people WHERE owner_id=?1 AND qualified_id=?2 AND name IS NOT NULL UNION SELECT phone as data, NULL as data2,NULL as data3,NULL as data4,3 as kind FROM phones WHERE owner_id=?1 AND qualified_id=?2 UNION SELECT email as data,NULL as data2,NULL as data3,NULL as data4,2 as kind FROM emails WHERE owner_id=?1 AND qualified_id=?2;"

    invoke-virtual {p1, v1, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lfbq;->h:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    iget-object v1, p0, Lfbq;->h:[Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v1, p0, Lfbq;->h:[Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v3, v1, v2

    iget-object v10, p0, Lfbq;->h:[Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v10, :cond_0

    iget-object v1, p0, Lfbq;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    sget-object v1, Lfbq;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lfbq;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lfbq;->g:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, v5, v0}, Lfbq;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v4, p0, Lfbq;->g:Ljava/util/Set;

    invoke-direct {p0, v0}, Lfbq;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V

    :cond_1
    array-length v11, v10

    const/4 v0, 0x0

    move v8, v0

    :goto_1
    if-ge v8, v11, :cond_0

    aget-object v0, v10, v8

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v5, v0}, Lfbq;->a(Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v6

    array-length v0, v6

    if-lez v0, :cond_3

    iget-object v4, p0, Lfbq;->g:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V

    move-object v7, v6

    :goto_2
    array-length v0, v7

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v4, p0, Lfbq;->g:Ljava/util/Set;

    invoke-direct {p0, v7}, Lfbq;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V

    :cond_2
    array-length v12, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_3
    if-ge v6, v12, :cond_4

    aget-object v5, v7, v6

    iget-object v4, p0, Lfbq;->g:Ljava/util/Set;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JLjava/util/Set;Ljava/lang/String;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_3

    :cond_3
    sget-object v0, Lfbq;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v5}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    :cond_5
    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    sget-object v1, Lfbq;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->split(Ljava/lang/CharSequence;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_4
    if-ge v6, v8, :cond_0

    aget-object v0, v7, v6

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JILjava/lang/String;)V

    :cond_6
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_4

    :cond_7
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x2b

    if-ne v1, v2, :cond_8

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_8
    const/4 v4, 0x3

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v2, p4

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;JILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_9
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private a(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 12

    iget-boolean v0, p0, Lfbq;->e:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfbq;->f:Z

    if-nez v0, :cond_0

    sget-object v0, Lfdl;->c:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lfbj;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_10

    iget-boolean v1, p0, Lfbq;->e:Z

    if-eqz v1, :cond_10

    iget-object v0, p0, Lfbq;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->c()Lfbh;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v0, v3, Lfbh;->c:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lfdl;->c:[Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {}, Lfdl;->b()Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v11

    :goto_1
    if-ge v2, v5, :cond_d

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Lfdl;->a(C)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    invoke-static {v0, v4, v1}, Lfbh;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/16 v7, 0x100

    if-ge v6, v7, :cond_6

    const/4 v7, 0x1

    if-eq v1, v7, :cond_5

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_5

    invoke-static {v0, v4, v1}, Lfbh;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_5
    const/4 v1, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    new-instance v7, Lfbi;

    invoke-direct {v7}, Lfbi;-><init>()V

    invoke-static {v6}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lfbi;->b:Ljava/lang/String;

    const/16 v9, 0x100

    if-ge v6, v9, :cond_9

    const/4 v9, 0x1

    iput v9, v7, Lfbi;->a:I

    iput-object v8, v7, Lfbi;->c:Ljava/lang/String;

    :cond_7
    :goto_3
    iget v8, v7, Lfbi;->a:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_8

    invoke-static {v0, v4, v1}, Lfbh;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_8
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x2

    goto :goto_2

    :cond_9
    const v9, 0x9fff

    if-lt v9, v6, :cond_a

    const/16 v9, 0x4e00

    if-lt v6, v9, :cond_a

    const/4 v8, 0x2

    iput v8, v7, Lfbi;->a:I

    iget-object v8, v3, Lfbh;->a:[Ljava/lang/String;

    iget-object v9, v3, Lfbh;->b:[S

    add-int/lit16 v10, v6, -0x4e00

    aget-short v9, v9, v10

    aget-object v8, v8, v9

    iput-object v8, v7, Lfbi;->c:Ljava/lang/String;

    iget-object v8, v7, Lfbi;->c:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    const/4 v8, 0x3

    iput v8, v7, Lfbi;->a:I

    iget-object v8, v7, Lfbi;->b:Ljava/lang/String;

    iput-object v8, v7, Lfbi;->c:Ljava/lang/String;

    goto :goto_3

    :cond_a
    const/4 v9, 0x3

    iput v9, v7, Lfbi;->a:I

    iput-object v8, v7, Lfbi;->c:Ljava/lang/String;

    goto :goto_3

    :cond_b
    iget v8, v7, Lfbi;->a:I

    if-eq v1, v8, :cond_c

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_c

    invoke-static {v0, v4, v1}, Lfbh;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_c
    iget v1, v7, Lfbi;->a:I

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_e

    invoke-static {v0, v4, v1}, Lfbh;->a(Ljava/lang/StringBuilder;Ljava/util/ArrayList;I)V

    :cond_e
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_f

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbi;

    iget-object v0, v0, Lfbi;->c:Ljava/lang/String;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_f
    move-object v0, v2

    goto/16 :goto_0

    :cond_10
    const/4 v1, 0x5

    if-ne v0, v1, :cond_11

    iget-boolean v0, p0, Lfbq;->f:Z

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Lfbg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto/16 :goto_0

    :cond_11
    sget-object v0, Lfdl;->c:[Ljava/lang/String;

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const-string v0, "PeopleSearchIndexManage"

    const-string v1, "Marking for index update."

    invoke-static {v0, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbq;->b:Lfbo;

    invoke-virtual {v0}, Lfbo;->d()Lfbn;

    move-result-object v0

    iget-object v0, v0, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfbq;->a(I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfbq;->b:Lfbo;

    invoke-virtual {v0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V

    :try_start_0
    iget-object v0, p0, Lfbq;->a:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->a(Landroid/content/Context;)Lfbo;

    move-result-object v0

    invoke-virtual {v0}, Lfbo;->e()Lfbk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lfbk;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {}, Lfdl;->d()[Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object p3, v0, v4

    const-string v4, "SELECT _id FROM people WHERE owner_id=? AND qualified_id=?"

    invoke-virtual {v1, v4, v0}, Lfbn;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {}, Lfdl;->c()[Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v6

    const-string v6, "search_index"

    const-string v7, "person_id=?"

    invoke-virtual {v1, v6, v7, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lfbq;->a(Lfbn;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lfbn;->e()V

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Specified person doesn\'t exist."

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
.end method

.method public final a(Ljava/util/Locale;)V
    .locals 3

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/Locale;->TAIWAN:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfbq;->e:Z

    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lfbq;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v0, 0x1

    iget-object v1, p0, Lfbq;->b:Lfbo;

    invoke-virtual {v1}, Lfbo;->d()Lfbn;

    move-result-object v2

    iget-object v1, v2, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v1

    invoke-static {v1}, Lbkm;->a(Z)V

    const/4 v1, 0x0

    iget-object v3, p0, Lfbq;->b:Lfbo;

    const-string v4, "indexIcuVersion"

    const-string v5, "unknown"

    invoke-virtual {v3, v4, v5}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.icu.library.version"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "PeopleService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "PeopleSearchIndexManage"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ICU version: old="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " new="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "ICU version changed from "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PeopleSearchIndexManage"

    invoke-static {v3, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfbq;->a:Landroid/content/Context;

    const-string v5, "PeopleSearchIndexManage"

    invoke-static {v3, v5, v1, v9}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v0

    :cond_1
    if-nez v1, :cond_3

    iget-object v3, p0, Lfbq;->b:Lfbo;

    const-string v5, "searchIndexVersion"

    const-string v6, "0"

    invoke-virtual {v3, v5, v6}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v8, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Index version changed from "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "PeopleSearchIndexManage"

    invoke-static {v3, v1}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfbq;->a:Landroid/content/Context;

    const-string v5, "PeopleSearchIndexManage"

    invoke-static {v3, v5, v1, v9}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_2
    invoke-direct {p0, v2}, Lfbq;->a(Lfbn;)V

    invoke-direct {p0, v8}, Lfbq;->a(I)V

    iget-object v0, p0, Lfbq;->b:Lfbo;

    const-string v1, "indexIcuVersion"

    invoke-virtual {v0, v1, v4}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method
