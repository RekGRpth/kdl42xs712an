.class public final Lbtr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbtq;


# instance fields
.field private a:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

.field private final b:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lbtr;->b:Ljava/lang/Thread;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lbtr;->b:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;
    .locals 1

    invoke-direct {p0}, Lbtr;->c()V

    iget-object v0, p0, Lbtr;->a:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V
    .locals 0

    invoke-direct {p0}, Lbtr;->c()V

    iput-object p1, p0, Lbtr;->a:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    return-void
.end method

.method public final b()Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;
    .locals 1

    invoke-direct {p0}, Lbtr;->c()V

    iget-object v0, p0, Lbtr;->a:Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    return-object v0
.end method
