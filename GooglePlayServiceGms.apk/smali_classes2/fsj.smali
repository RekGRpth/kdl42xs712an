.class public final Lfsj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lgez;


# instance fields
.field private final b:Lgeg;

.field private final c:Lgei;

.field private final d:Lgej;

.field private final e:Lgeq;

.field private final f:Lget;

.field private final g:Lgex;

.field private final h:Lgff;

.field private final i:Lgfi;

.field private final j:Lbmi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lgez;

    invoke-direct {v0}, Lgez;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lgez;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgez;

    const-string v1, "displayName"

    invoke-virtual {v0, v1}, Lgez;->a(Ljava/lang/String;)Lbmg;

    move-result-object v0

    check-cast v0, Lgez;

    sput-object v0, Lfsj;->a:Lgez;

    return-void
.end method

.method public constructor <init>(Lbmi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfsj;->j:Lbmi;

    new-instance v0, Lgeg;

    invoke-direct {v0, p1}, Lgeg;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->b:Lgeg;

    new-instance v0, Lgei;

    invoke-direct {v0, p1}, Lgei;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->c:Lgei;

    new-instance v0, Lgeq;

    invoke-direct {v0, p1}, Lgeq;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->e:Lgeq;

    new-instance v0, Lgff;

    invoke-direct {v0, p1}, Lgff;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->h:Lgff;

    new-instance v0, Lgej;

    invoke-direct {v0, p1}, Lgej;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->d:Lgej;

    new-instance v0, Lget;

    invoke-direct {v0, p1}, Lget;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->f:Lget;

    new-instance v0, Lgex;

    invoke-direct {v0, p1}, Lgex;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->g:Lgex;

    new-instance v0, Lgfi;

    invoke-direct {v0, p1}, Lgfi;-><init>(Lbmi;)V

    iput-object v0, p0, Lfsj;->i:Lgfi;

    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3

    new-instance v1, Landroid/content/ContentValues;

    sget-object v0, Lfst;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/content/ContentValues;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lfst;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lfst;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "url"

    invoke-virtual {v1, v0, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Landroid/content/pm/ApplicationInfo;
    .locals 9

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_0
    move v6, v3

    move-object v4, v5

    :goto_1
    if-ge v6, v2, :cond_0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    const-string v3, "android"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->f()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {p0, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->e()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x40

    invoke-virtual {p0, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v8, :cond_4

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v8, v8

    if-lez v8, :cond_4

    iget-object v7, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lbox;->a([BZ)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v4, :cond_2

    move-object v4, v5

    :cond_0
    return-object v4

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_2
    move-object v4, v1

    :cond_3
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_4
    move-object v1, v4

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Landroid/os/Bundle;)Lggo;
    .locals 2

    new-instance v0, Lggx;

    invoke-direct {v0}, Lggx;-><init>()V

    invoke-virtual {v0, p0}, Lggx;->a(Ljava/lang/String;)Lggx;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lgqa;->a(Landroid/os/Bundle;)Lgqa;

    move-result-object v1

    invoke-virtual {v1, v0}, Lgqa;->a(Lggx;)Lggx;

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lggx;->a()Lggs;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lghe;

    invoke-direct {v0}, Lghe;-><init>()V

    invoke-virtual {v0, v1}, Lghe;->a(Ljava/util/List;)Lghe;

    move-result-object v0

    invoke-virtual {v0}, Lghe;->a()Lggq;

    move-result-object v0

    new-instance v1, Lggp;

    invoke-direct {v1}, Lggp;-><init>()V

    invoke-virtual {v1, v0}, Lggp;->a(Lggq;)Lggp;

    move-result-object v0

    invoke-virtual {v0}, Lggp;->a()Lggo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentValues;Lggo;)V
    .locals 3

    invoke-interface {p1}, Lggo;->c()Lggq;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lsp;

    const-string v1, "Link preview requires object."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v0}, Lggq;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    new-instance v0, Lsp;

    const-string v1, "Link preview requires object.attachments[]."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;

    if-nez v0, :cond_3

    new-instance v0, Lsp;

    const-string v1, "Link preview requires attachments."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const-string v1, "title"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "type"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "description"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "callToActionDisplayName"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->d()Lggt;

    move-result-object v2

    invoke-interface {v2}, Lggt;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity;->l()Lgha;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    if-eqz v0, :cond_5

    const-string v1, "thumbnailUrl"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 1

    invoke-static {}, Lfsb;->a()Lfsb;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lfsb;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Post;)Landroid/os/Bundle;
    .locals 11

    const/4 v7, 0x0

    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "me"

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->k()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v10, v0}, Lfsj;->a(Ljava/lang/String;Landroid/os/Bundle;)Lggo;

    move-result-object v9

    iget-object v0, p0, Lfsj;->c:Lgei;

    invoke-virtual {p3}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-object v1, p2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lgei;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    invoke-static {v10}, Lfsj;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-static {v1, v0}, Lfsj;->a(Landroid/content/ContentValues;Lggo;)V

    invoke-static {v10, v1}, Lfsj;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    new-instance v0, Lfst;

    invoke-direct {v0, v1}, Lfst;-><init>(Landroid/content/ContentValues;)V

    invoke-virtual {v0}, Lfst;->f()Landroid/os/Bundle;

    move-result-object v7

    :cond_1
    return-object v7
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Landroid/util/Pair;
    .locals 11

    iget-object v0, p0, Lfsj;->d:Lgej;

    const-string v2, "me"

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p2

    move-object v3, p3

    move-object/from16 v6, p5

    invoke-virtual/range {v0 .. v6}, Lgej;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v0, "SHA1"

    invoke-static {v0}, Lbox;->a(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v8

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "display_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->e()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "application_id"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "icon_url"

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v9, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "is_aspen"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v7, v8, v0}, Lfsj;->a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "display_name"

    invoke-virtual {v7, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "application_info"

    invoke-static {v0}, Lfwm;->a(Landroid/content/pm/ApplicationInfo;)[B

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_0
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->f()Lcom/google/android/gms/plus/service/v1whitelisted/models/Application$Icon;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application$Icon;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbkf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v0, Lfsk;

    invoke-direct {v0}, Lfsk;-><init>()V

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object v0, Lfwm;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_4

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    invoke-virtual {v2, v0}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Applications;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;
    .locals 14

    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->c()V

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    new-instance v13, Lboy;

    invoke-direct {v13}, Lboy;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13}, Lboy;->a()Lboy;

    move-result-object v5

    iget-object v0, p0, Lfsj;->b:Lgeg;

    const-string v3, "shared"

    move-object/from16 v1, p2

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lgeg;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    invoke-virtual {v13}, Lboy;->a()Lboy;

    move-result-object v11

    iget-object v6, p0, Lfsj;->b:Lgeg;

    const-string v9, "visible"

    move-object/from16 v7, p2

    move-object v8, v2

    move-object v10, v4

    move-object v12, v11

    invoke-virtual/range {v6 .. v12}, Lgeg;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    :try_start_0
    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v13}, Lboy;->b()V

    invoke-virtual {v5}, Lboy;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v11}, Lboy;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lsp;

    const-string v1, "Interrupted."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    invoke-static {v0}, Lbky;->b(Lgge;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgld;

    const-string v6, "allCircles"

    invoke-interface {v3}, Lgld;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    new-instance v4, Lfwk;

    invoke-direct {v4}, Lfwk;-><init>()V

    iput-object v5, v4, Lfwk;->b:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v1, v4, Lfwk;->c:Ljava/util/ArrayList;

    iput-boolean v3, v4, Lfwk;->d:Z

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lfwk;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lfwk;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    new-instance v1, Lfwi;

    invoke-direct {v1}, Lfwi;-><init>()V

    iput-object v0, v1, Lfwi;->b:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iput-object v2, v1, Lfwi;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lfwi;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-static {v1}, Lbky;->a(Lgge;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_2

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Comment;)Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 15

    new-instance v2, Lgjg;

    invoke-direct {v2}, Lgjg;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lgjg;->a:Ljava/lang/String;

    iget-object v1, v2, Lgjg;->b:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v10, Lgjd;

    invoke-direct {v10}, Lgjd;-><init>()V

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v3, v2, Lgjg;->b:Ljava/util/Set;

    iget-object v2, v2, Lgjg;->a:Ljava/lang/String;

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iput-object v1, v10, Lgjd;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v1, v10, Lgjd;->i:Ljava/util/Set;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "me"

    move-object v11, v1

    :goto_0
    iget-object v12, p0, Lfsj;->e:Lgeq;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->b()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->e()Ljava/lang/String;

    move-result-object v14

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    iget-object v2, v10, Lgjd;->i:Ljava/util/Set;

    iget-object v3, v10, Lgjd;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ActorEntity;

    iget-object v4, v10, Lgjd;->b:Ljava/lang/String;

    iget-object v5, v10, Lgjd;->c:Ljava/util/List;

    iget-object v6, v10, Lgjd;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;

    iget-object v7, v10, Lgjd;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$PlusonersEntity;

    iget-object v8, v10, Lgjd;->f:Ljava/lang/String;

    iget-object v9, v10, Lgjd;->g:Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$StatusForViewerEntity;

    iget-object v10, v10, Lgjd;->h:Ljava/lang/String;

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ActorEntity;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$ObjectEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$PlusonersEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity$StatusForViewerEntity;Ljava/lang/String;)V

    move-object v5, v1

    check-cast v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    const/4 v1, 0x0

    invoke-static {v1, v13, v14, v11}, Lgeq;->a(Lger;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, v12, Lgeq;->a:Lbmi;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;

    if-eqz v1, :cond_2

    new-instance v7, Lfxw;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Lfxw;-><init>(Lcom/google/android/gms/plus/model/posts/Comment;)V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CommentEntity;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lfxw;->b:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/plus/model/posts/Comment;

    const/4 v2, 0x1

    iget-object v3, v7, Lfxw;->b:Ljava/lang/String;

    iget-object v4, v7, Lfxw;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/model/posts/Comment;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v7, Lfxw;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/model/posts/Comment;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v7, Lfxw;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/model/posts/Comment;->e()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v7, Lfxw;->a:Lcom/google/android/gms/plus/model/posts/Comment;

    invoke-virtual {v7}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/plus/model/posts/Comment;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v1

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Comment;->f()Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;
    .locals 10

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "me"

    :goto_0
    iget-object v0, p0, Lfsj;->c:Lgei;

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v1, p2

    move-object v3, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lgei;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p3

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;
    .locals 10

    if-nez p5, :cond_0

    const/4 v7, 0x0

    :goto_0
    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lfsj;->f:Lget;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v0, 0x0

    const/4 v3, 0x0

    move-object/from16 v1, p7

    move-object/from16 v2, p8

    move-object v6, p4

    move-object/from16 v8, p6

    invoke-static/range {v0 .. v8}, Lget;->a(Lgev;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v9, Lget;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    return-object v0

    :cond_0
    invoke-virtual {p5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    .locals 8

    const/4 v7, 0x0

    packed-switch p2, :pswitch_data_0

    const-string v6, "alphabetical"

    :goto_0
    :try_start_0
    iget-object v0, p0, Lfsj;->g:Lgex;

    const-string v1, "me"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3, v1, v2, v6, p4}, Lgex;->a(Lgfb;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lgex;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v0

    :pswitch_0
    const-string v6, "best"

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlusWhitelistedAgent"

    const-string v2, "listForSharingBlocking VolleyError"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lfsj;->g:Lgex;

    const-string v2, "me"

    const-string v3, "circled"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-object v4, v7

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;
    .locals 6

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfsj;->h:Lgff;

    const-string v2, "androidGms"

    const-string v3, "me"

    const/4 v4, 0x0

    invoke-static {v4, v2, p3, v0, v3}, Lgff;->a(Lgfh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lgff;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-object v1, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lfst;
    .locals 10

    const/4 v3, 0x0

    invoke-static {p3, v3}, Lfsj;->a(Ljava/lang/String;Landroid/os/Bundle;)Lggo;

    move-result-object v9

    iget-object v0, p0, Lfsj;->c:Lgei;

    const-string v2, "me"

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-object v1, p2

    move-object v7, v3

    move-object v8, v3

    invoke-virtual/range {v0 .. v9}, Lgei;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v0

    invoke-static {p3}, Lfsj;->a(Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-static {v1, v0}, Lfsj;->a(Landroid/content/ContentValues;Lggo;)V

    invoke-static {p3, v1}, Lfsj;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    new-instance v0, Lfst;

    invoke-direct {v0, v1}, Lfst;-><init>(Landroid/content/ContentValues;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 4

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0}, Lbky;->a(Lcom/google/android/gms/common/people/data/Audience;)Lgge;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lfsj;->b:Lgeg;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "shared"

    invoke-virtual {v1, p1, v2, v3, v0}, Lgeg;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lfsj;->f:Lget;

    invoke-static {v3, p2}, Lget;->a(Lgew;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lget;->a:Lbmi;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1, v3}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    invoke-static {p3}, Lbky;->a(Lcom/google/android/gms/common/people/data/Audience;)Lgge;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lfsj;->b:Lgeg;

    const-string v2, "shared"

    invoke-virtual {v1, p1, p2, v2, v0}, Lgeg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p4, :cond_0

    new-instance v1, Lgle;

    invoke-direct {v1}, Lgle;-><init>()V

    const-string v2, "allCircles"

    invoke-virtual {v1, v2}, Lgle;->b(Ljava/lang/String;)Lgle;

    move-result-object v1

    invoke-virtual {v1}, Lgle;->a()Lgld;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v1, Lggf;

    invoke-direct {v1}, Lggf;-><init>()V

    invoke-virtual {v1, v0}, Lggf;->a(Ljava/util/List;)Lggf;

    move-result-object v0

    invoke-virtual {v0}, Lggf;->a()Lgge;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lfsj;->b:Lgeg;

    const-string v2, "visible"

    invoke-virtual {v1, p1, p2, v2, v0}, Lgeg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-void

    :cond_0
    invoke-static {p3}, Lbky;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lfsj;->f:Lget;

    invoke-virtual {v0, p1, p2}, Lget;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfsj;->d:Lgej;

    invoke-static {v3, p2}, Lgej;->a(Lgek;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lgej;->a:Lbmi;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1, v3}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;IZLjava/lang/String;)V
    .locals 8

    if-eqz p4, :cond_0

    const-string v0, "10"

    :goto_0
    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    new-instance v6, Lgiz;

    invoke-direct {v6}, Lgiz;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v6, Lgiz;->a:Ljava/lang/String;

    iget-object v3, v6, Lgiz;->f:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p2, v6, Lgiz;->b:Ljava/util/List;

    iget-object v3, v6, Lgiz;->f:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v0, v6, Lgiz;->c:Ljava/lang/String;

    iget-object v0, v6, Lgiz;->f:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lgiz;->d:Ljava/lang/String;

    iget-object v0, v6, Lgiz;->f:Ljava/util/Set;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-wide v1, v6, Lgiz;->e:J

    iget-object v0, v6, Lgiz;->f:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;

    iget-object v1, v6, Lgiz;->f:Ljava/util/Set;

    iget-object v2, v6, Lgiz;->a:Ljava/lang/String;

    iget-object v3, v6, Lgiz;->b:Ljava/util/List;

    iget-object v4, v6, Lgiz;->c:Ljava/lang/String;

    iget-object v5, v6, Lgiz;->d:Ljava/lang/String;

    iget-wide v6, v6, Lgiz;->e:J

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;J)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzExtensionEntity;

    iget-object v1, p0, Lfsj;->h:Lgff;

    const/4 v2, 0x0

    invoke-static {v2, p5}, Lgff;->a(Lgfg;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lgff;->a:Lbmi;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v3, v2, v0}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_0
    const-string v0, "4"

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 14

    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->c()V

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    new-instance v13, Lboy;

    invoke-direct {v13}, Lboy;-><init>()V

    invoke-virtual {v13}, Lboy;->a()Lboy;

    move-result-object v5

    iget-object v0, p0, Lfsj;->b:Lgeg;

    const-string v3, "shared"

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lgeg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    invoke-virtual {v13}, Lboy;->a()Lboy;

    move-result-object v11

    iget-object v6, p0, Lfsj;->b:Lgeg;

    const-string v9, "visible"

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object v10, v4

    move-object v12, v11

    invoke-virtual/range {v6 .. v12}, Lgeg;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    :try_start_0
    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v13}, Lboy;->b()V

    invoke-virtual {v5}, Lboy;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v11}, Lboy;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lsp;

    const-string v1, "Interrupted."

    invoke-direct {v0, v1}, Lsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    invoke-static {v0}, Lbky;->b(Lgge;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v4

    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x1

    if-ne v2, v5, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->d()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgld;

    const-string v5, "allCircles"

    invoke-interface {v2}, Lgld;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    new-instance v3, Lfwk;

    invoke-direct {v3}, Lfwk;-><init>()V

    iput-object v4, v3, Lfwk;->b:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v1, v3, Lfwk;->c:Ljava/util/ArrayList;

    iput-boolean v2, v3, Lfwk;->d:Z

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lfwk;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lfwk;->a()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-static {v1}, Lbky;->a(Lgge;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/model/posts/Post;)Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 15

    new-instance v6, Lboy;

    invoke-direct {v6}, Lboy;-><init>()V

    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->c()V

    invoke-static/range {p1 .. p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lboy;->a()Lboy;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "me"

    :goto_0
    iget-object v3, p0, Lfsj;->i:Lgfi;

    const-string v2, "sharing"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-static {v7, v0, v2, v5, v1}, Lgfi;->a(Lgfj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v3, Lgfi;->a:Lbmi;

    const-class v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    move-object/from16 v1, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lsk;Lsj;)V

    invoke-virtual {v6}, Lboy;->a()Lboy;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lfsj;->g:Lgex;

    invoke-static {v0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lfsj;->a:Lgez;

    invoke-static {v6, v0, v11}, Lgex;->a(Lgez;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v5, v5, Lgex;->a:Lbmi;

    const-class v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-object/from16 v6, p2

    move-object v10, v9

    invoke-virtual/range {v5 .. v10}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lsk;Lsj;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move-object v0, v11

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lfsj;->j:Lbmi;

    invoke-virtual {v0}, Lbmi;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v4}, Lboy;->b()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v0, Lfsr;->U:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    sget-object v0, Lfsr;->V:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v0, Lfsr;->W:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v0, Lfsr;->X:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v4}, Lboy;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->d()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    const/4 v0, 0x0

    move v4, v1

    move v1, v7

    move v7, v0

    move v13, v5

    move v5, v3

    move v3, v13

    move v14, v6

    move-object v6, v2

    move v2, v14

    :goto_2
    if-ge v7, v10, :cond_8

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglh;

    const-string v11, "sharing.defaultAccess"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v0}, Lglh;->c()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v0}, Lglh;->b()Lgge;

    move-result-object v0

    invoke-static {v0}, Lbky;->b(Lgge;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    move v13, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v0

    move v0, v13

    :goto_3
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "PlusWhitelistedAgent"

    const-string v2, "Network operation interrupted"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lsp;

    invoke-direct {v1, v0}, Lsp;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    const-string v11, "sharing.underageWarning"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v0}, Lglh;->g()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v0}, Lglh;->d()Z

    move-result v0

    move-object v5, v6

    move v13, v2

    move v2, v3

    move v3, v4

    move v4, v0

    move v0, v1

    move v1, v13

    goto :goto_3

    :cond_4
    const-string v11, "sharing.showAclPickerFirst"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v0}, Lglh;->g()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v0}, Lglh;->d()Z

    move-result v0

    move v4, v5

    move-object v5, v6

    move v13, v3

    move v3, v0

    move v0, v1

    move v1, v2

    move v2, v13

    goto :goto_3

    :cond_5
    const-string v11, "sharing.showcasedSuggestionCount"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v0}, Lglh;->j()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v0}, Lglh;->i()I

    move-result v0

    move v3, v4

    move v4, v5

    move-object v5, v6

    move v13, v1

    move v1, v2

    move v2, v0

    move v0, v13

    goto :goto_3

    :cond_6
    const-string v11, "sharing.suggestionCount"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v0}, Lglh;->j()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v0}, Lglh;->i()I

    move-result v0

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    move v13, v1

    move v1, v0

    move v0, v13

    goto/16 :goto_3

    :cond_7
    const-string v11, "sharing.clientSuggestionCount"

    invoke-interface {v0}, Lglh;->h()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v0}, Lglh;->j()Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v0}, Lglh;->i()I

    move-result v0

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_3

    :cond_8
    move v7, v1

    move-object v1, v6

    move v6, v2

    move v13, v5

    move v5, v3

    move v3, v13

    :goto_4
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Lboy;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    move v2, v0

    :goto_5
    iget-object v0, v9, Lboy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    invoke-virtual {v9, v2}, Lboy;->a(I)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v11, 0x0

    invoke-static {v10, v0, v11}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_a
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v2, 0x0

    :goto_6
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/model/posts/Settings;-><init>(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;ZZIII)V

    return-object v0

    :cond_b
    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-virtual {v0, v8}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lbkw;->a(I)Lbkw;

    move-result-object v0

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    goto :goto_6

    :cond_c
    move v0, v1

    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move-object v5, v6

    goto/16 :goto_3

    :cond_d
    move v4, v1

    move-object v1, v2

    goto :goto_4
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;
    .locals 6

    const/4 v4, 0x0

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfsj;->f:Lget;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, p3, v0, v2, p5}, Lget;->a(Lgeu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lget;->a:Lbmi;

    const/4 v2, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;
    .locals 1

    iget-object v0, p0, Lfsj;->g:Lgex;

    invoke-virtual {v0, p1, p2}, Lgex;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lgle;

    invoke-direct {v1}, Lgle;-><init>()V

    const-string v2, "allCircles"

    invoke-virtual {v1, v2}, Lgle;->b(Ljava/lang/String;)Lgle;

    move-result-object v1

    invoke-virtual {v1}, Lgle;->a()Lgld;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    new-instance v1, Lggf;

    invoke-direct {v1}, Lggf;-><init>()V

    invoke-virtual {v1, v0}, Lggf;->a(Ljava/util/List;)Lggf;

    move-result-object v0

    invoke-virtual {v0}, Lggf;->a()Lgge;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lfsj;->b:Lgeg;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "visible"

    invoke-virtual {v1, p1, v2, v3, v0}, Lgeg;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->g()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->d()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lbky;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;
    .locals 6

    const/4 v4, 0x0

    invoke-static {p1}, Lgak;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lfsj;->b:Lgeg;

    const-string v2, "shared"

    invoke-static {v4, p3, v2, v0}, Lgeg;->a(Lgeh;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lgeg;->a:Lbmi;

    const/4 v2, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    return-object v0
.end method
