.class public abstract Lcaq;
.super Landroid/database/DataSetObserver;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field protected a:Landroid/database/Cursor;

.field protected b:I

.field private final c:Z

.field private final d:I

.field private final e:Landroid/util/SparseIntArray;

.field private final f:[Lcap;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;I[Lcap;Z)V
    .locals 2

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    iput-boolean p4, p0, Lcaq;->c:Z

    iput-object p1, p0, Lcaq;->a:Landroid/database/Cursor;

    iput p2, p0, Lcaq;->b:I

    array-length v0, p3

    iput v0, p0, Lcaq;->d:I

    iput-object p3, p0, Lcaq;->f:[Lcap;

    new-instance v0, Landroid/util/SparseIntArray;

    iget v1, p0, Lcaq;->d:I

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcaq;->e:Landroid/util/SparseIntArray;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    invoke-virtual {p0}, Lcaq;->a()Ljava/util/Comparator;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    iget-boolean v1, p0, Lcaq;->c:Z

    if-eqz v1, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method


# virtual methods
.method protected abstract a(Landroid/database/Cursor;I)Ljava/lang/Object;
.end method

.method protected abstract a()Ljava/util/Comparator;
.end method

.method public getPositionForSection(I)I
    .locals 10

    const/4 v0, 0x0

    const/high16 v8, -0x80000000

    iget-object v4, p0, Lcaq;->e:Landroid/util/SparseIntArray;

    iget-object v5, p0, Lcaq;->a:Landroid/database/Cursor;

    if-eqz v5, :cond_0

    iget-object v1, p0, Lcaq;->f:[Lcap;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-lez p1, :cond_0

    iget v1, p0, Lcaq;->d:I

    if-lt p1, v1, :cond_2

    iget v1, p0, Lcaq;->d:I

    add-int/lit8 p1, v1, -0x1

    :cond_2
    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iget-object v1, p0, Lcaq;->f:[Lcap;

    aget-object v1, v1, p1

    invoke-interface {v1}, Lcap;->a()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v4, p1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    if-eq v8, v1, :cond_a

    if-gez v1, :cond_4

    neg-int v2, v1

    :goto_1
    if-lez p1, :cond_3

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v4, v1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    if-eq v1, v8, :cond_3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    :cond_3
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    move v9, v2

    move v2, v0

    move v0, v9

    :goto_2
    if-ge v1, v0, :cond_6

    invoke-interface {v5, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget v8, p0, Lcaq;->b:I

    invoke-virtual {p0, v5, v8}, Lcaq;->a(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_5

    if-eqz v1, :cond_6

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    invoke-direct {p0, v8, v7}, Lcaq;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    if-eqz v8, :cond_9

    if-gez v8, :cond_7

    add-int/lit8 v1, v1, 0x1

    if-lt v1, v3, :cond_8

    move v1, v3

    :cond_6
    invoke-virtual {v4, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v1, v2

    :cond_8
    :goto_3
    add-int v2, v1, v0

    div-int/lit8 v2, v2, 0x2

    move v9, v2

    move v2, v1

    move v1, v9

    goto :goto_2

    :cond_9
    if-eq v2, v1, :cond_6

    move v0, v1

    move v1, v2

    goto :goto_3

    :cond_a
    move v2, v3

    goto :goto_1
.end method

.method public getSectionForPosition(I)I
    .locals 3

    iget-object v0, p0, Lcaq;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget-object v1, p0, Lcaq;->a:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v1, p0, Lcaq;->a:Landroid/database/Cursor;

    iget v2, p0, Lcaq;->b:I

    invoke-virtual {p0, v1, v2}, Lcaq;->a(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcaq;->a:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget v0, p0, Lcaq;->d:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcaq;->f:[Lcap;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcap;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcaq;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcaq;->f:[Lcap;

    return-object v0
.end method

.method public onChanged()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    iget-object v0, p0, Lcaq;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    return-void
.end method

.method public onInvalidated()V
    .locals 1

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    iget-object v0, p0, Lcaq;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    return-void
.end method
