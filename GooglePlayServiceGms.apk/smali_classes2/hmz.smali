.class public final enum Lhmz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhmz;

.field public static final enum b:Lhmz;

.field public static final enum c:Lhmz;

.field public static final enum d:Lhmz;

.field public static final enum e:Lhmz;

.field public static final enum f:Lhmz;

.field public static final enum g:Lhmz;

.field public static final enum h:Lhmz;

.field private static final synthetic i:[Lhmz;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lhmz;

    const-string v1, "IN_CAR"

    invoke-direct {v0, v1, v3}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->a:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "ON_BICYCLE"

    invoke-direct {v0, v1, v4}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->b:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "ON_FOOT"

    invoke-direct {v0, v1, v5}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->c:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "STILL"

    invoke-direct {v0, v1, v6}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->d:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v7}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->e:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "TILTING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->f:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "LEG_SHAKE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->g:Lhmz;

    new-instance v0, Lhmz;

    const-string v1, "INCONSISTENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lhmz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhmz;->h:Lhmz;

    const/16 v0, 0x8

    new-array v0, v0, [Lhmz;

    sget-object v1, Lhmz;->a:Lhmz;

    aput-object v1, v0, v3

    sget-object v1, Lhmz;->b:Lhmz;

    aput-object v1, v0, v4

    sget-object v1, Lhmz;->c:Lhmz;

    aput-object v1, v0, v5

    sget-object v1, Lhmz;->d:Lhmz;

    aput-object v1, v0, v6

    sget-object v1, Lhmz;->e:Lhmz;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhmz;->f:Lhmz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhmz;->g:Lhmz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhmz;->h:Lhmz;

    aput-object v2, v0, v1

    sput-object v0, Lhmz;->i:[Lhmz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhmz;
    .locals 1

    const-class v0, Lhmz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhmz;

    return-object v0
.end method

.method public static values()[Lhmz;
    .locals 1

    sget-object v0, Lhmz;->i:[Lhmz;

    invoke-virtual {v0}, [Lhmz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhmz;

    return-object v0
.end method
