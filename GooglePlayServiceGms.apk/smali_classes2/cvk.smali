.class final Lcvk;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:Landroid/content/ContentValues;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvk;->a:Ljava/util/concurrent/locks/ReentrantLock;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcvk;->b:Landroid/content/ContentValues;

    const-string v1, "acknowledged"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Lcve;)V
    .locals 2

    const-string v0, "NotificationAgent"

    sget-object v1, Lcvk;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const/4 v2, 0x0

    invoke-static {p1}, Ldjm;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "alert_level DESC,timestamp DESC"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)V
    .locals 1

    const-string v0, "com.google.android.gms.games.background"

    invoke-static {p1, v0}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-static {p0, p1}, Ldrn;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    const/4 v0, 0x1

    invoke-static {p1, v0}, Ldrn;->a(Landroid/accounts/Account;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    invoke-static {p1}, Leej;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldrc;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p2}, Ldrc;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p0, p1, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {p2}, Ldjm;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcvk;->b:Landroid/content/ContentValues;

    invoke-virtual {v1, v0, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    const/4 v2, 0x0

    invoke-static {p1}, Ldjm;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lblt;

    invoke-direct {v1, v0}, Lblt;-><init>(Landroid/net/Uri;)V

    if-eqz p3, :cond_1

    invoke-static {p0, v0, p3}, Lcum;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-gtz v5, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to resolve internal game ID for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v5, "game_id"

    invoke-virtual {v1, v5, v3, v4}, Lblt;->a(Ljava/lang/String;J)V

    :cond_1
    if-eqz p4, :cond_2

    const-string v3, "external_sub_id"

    invoke-virtual {v1, v3, p4}, Lblt;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz p6, :cond_3

    const-string v3, "type"

    invoke-virtual {v1, v3, p6}, Lblt;->a(Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v1}, Lblt;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v1, Lblt;->c:[Ljava/lang/String;

    invoke-virtual {v3, v0, v4, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-static {p1}, Ldjm;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v4, "alert_level DESC,timestamp DESC"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcum;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    new-instance v1, Ldeu;

    invoke-direct {v1, v0}, Ldeu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-static {p0, p1, p2, p5, v1}, Ldrc;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldeu;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ldeu;->b()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldeu;->b()V

    throw v0
.end method
