.class public final Leky;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:[Lbhl;


# instance fields
.field private final e:Landroid/content/Context;

.field private final f:Lejk;

.field private final g:Leji;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x0

    const-string v0, "extension_download_hysteresis_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leky;->b:Ljava/lang/String;

    const-string v0, "extension_download_enabled"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leky;->c:Ljava/lang/String;

    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v0, v9}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leky;->a:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v7, v0, [Lbhl;

    const/4 v8, 0x0

    new-instance v0, Lbhl;

    const-string v1, "armeabi"

    invoke-static {v1, v9}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://dl.google.com/android/appdatasearch/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "armeabi"

    const-string v4, "3"

    invoke-static {v3, v4}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/32 v3, 0x3a5530

    const-string v5, "6c01c83fdfe351d6c6563a5288950a679bd27589"

    const-string v6, ""

    invoke-direct/range {v0 .. v6}, Lbhl;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v7, v8

    const/4 v8, 0x1

    new-instance v0, Lbhl;

    const-string v1, "armeabi-v7a"

    invoke-static {v1, v9}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://dl.google.com/android/appdatasearch/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "armeabi-v7a"

    const-string v4, "3"

    invoke-static {v3, v4}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/32 v3, 0x3a2538

    const-string v5, "fe729e308260f5835779c1176762aa243d165068"

    const-string v6, ""

    invoke-direct/range {v0 .. v6}, Lbhl;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v7, v8

    const/4 v8, 0x2

    new-instance v0, Lbhl;

    const-string v1, "mips"

    invoke-static {v1, v9}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://dl.google.com/android/appdatasearch/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "mips"

    const-string v4, "3"

    invoke-static {v3, v4}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/32 v3, 0x3e5afc

    const-string v5, "9f12271dd42fd5477a454734a07c501130ea2c95"

    const-string v6, ""

    invoke-direct/range {v0 .. v6}, Lbhl;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v7, v8

    const/4 v8, 0x3

    new-instance v0, Lbhl;

    const-string v1, "x86"

    invoke-static {v1, v9}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://dl.google.com/android/appdatasearch/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "x86"

    const-string v4, "3"

    invoke-static {v3, v4}, Leky;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/32 v3, 0x3c34cc

    const-string v5, "2fe798a546ed0482c3421b49f332093079c9025b"

    const-string v6, ""

    invoke-direct/range {v0 .. v6}, Lbhl;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v7, v8

    sput-object v7, Leky;->d:[Lbhl;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lejk;Leji;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Leky;->e:Landroid/content/Context;

    iput-object p2, p0, Leky;->f:Lejk;

    iput-object p3, p0, Leky;->g:Leji;

    invoke-direct {p0}, Leky;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leky;->h:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "libAppDataSearchExt_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2d

    const/16 v2, 0x5f

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ".v"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Lehd;Ljava/io/PrintWriter;)V
    .locals 4

    if-nez p0, :cond_0

    const-string v0, "\tnull\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    :goto_0
    return-void

    :cond_0
    const-string v0, "\tv%d policy %d min %d/%d\n"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lehd;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lehd;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lehd;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x41fea6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto :goto_0
.end method

.method private d()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Leky;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    sget-object v2, Leky;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private e()Z
    .locals 3

    iget-object v0, p0, Leky;->e:Landroid/content/Context;

    sget-object v1, Leky;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leky;->f:Lejk;

    invoke-virtual {v0}, Lejk;->h()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Leky;->f:Lejk;

    invoke-virtual {v1, v0}, Lejk;->a(Lehd;)V

    goto :goto_0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v0

    iget-object v1, p0, Leky;->f:Lejk;

    invoke-virtual {v1}, Lejk;->g()Lehd;

    move-result-object v1

    const-string v2, "Enabled: "

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    invoke-direct {p0}, Leky;->e()Z

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->print(Z)V

    const-string v2, "\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    const-string v2, "Downloaded:\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    invoke-static {v0, p1}, Leky;->a(Lehd;Ljava/io/PrintWriter;)V

    const-string v0, "Current:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    invoke-static {v1, p1}, Leky;->a(Lehd;Ljava/io/PrintWriter;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 10

    const/4 v1, 0x0

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    if-eqz p1, :cond_5

    invoke-direct {p0}, Leky;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Leky;->e:Landroid/content/Context;

    sget-object v2, Leky;->b:Ljava/lang/String;

    const-wide/32 v6, 0x5265c00

    invoke-static {v0, v2, v6, v7}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v6

    iget-object v0, p0, Leky;->f:Lejk;

    iget-object v0, v0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v2, "extension-delete"

    const-wide/16 v8, 0x0

    invoke-interface {v0, v2, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v0, v6, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Leky;->g:Leji;

    const-string v2, "ext_download_enabled"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    iget-object v4, p0, Leky;->e:Landroid/content/Context;

    sget-object v5, Leky;->d:[Lbhl;

    array-length v6, v5

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_3

    aget-object v0, v5, v2

    iget-object v7, v0, Lbhl;->a:Ljava/lang/String;

    sget-object v8, Leky;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    invoke-static {v4, v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Lbhl;)Z

    :cond_0
    if-eqz v3, :cond_4

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Leky;->f:Lejk;

    invoke-virtual {v0}, Lejk;->g()Lehd;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, v0, Lehd;->a:I

    iget v2, v3, Lehd;->a:I

    if-eq v0, v2, :cond_4

    :cond_1
    iget-object v0, p0, Leky;->g:Leji;

    const-string v2, "ext_state_change"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    iget v0, v3, Lehd;->b:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Leky;->f:Lejk;

    invoke-virtual {v0, v3}, Lejk;->a(Lehd;)V

    move v0, v1

    :goto_2
    return v0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_2

    :pswitch_1
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Leky;->g:Leji;

    const-string v2, "ext_download_disabled"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    iget-object v0, p0, Leky;->f:Lejk;

    invoke-virtual {v0}, Lejk;->h()V

    invoke-direct {p0}, Leky;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Leky;->f:Lejk;

    iget-object v2, v2, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "extension-delete"

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_6
    iget-object v2, p0, Leky;->e:Landroid/content/Context;

    sget-object v3, Leky;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/download/DownloadService;->d(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->a()V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Lehd;
    .locals 1

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v1

    iget-object v2, p0, Leky;->f:Lejk;

    invoke-virtual {v2}, Lejk;->g()Lehd;

    move-result-object v2

    if-eqz v1, :cond_1

    if-eqz v2, :cond_0

    iget v2, v2, Lehd;->a:I

    iget v3, v1, Lehd;->a:I

    if-eq v2, v3, :cond_1

    :cond_0
    iget v2, v1, Lehd;->b:I

    packed-switch v2, :pswitch_data_0

    iget-object v2, p0, Leky;->f:Lejk;

    invoke-virtual {v2, v1}, Lejk;->a(Lehd;)V

    move p1, v0

    :goto_0
    :pswitch_0
    return p1

    :pswitch_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    move p1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Leky;->h:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(Ljava/lang/String;)Lehd;

    move-result-object v0

    iget-object v1, p0, Leky;->f:Lejk;

    invoke-virtual {v1}, Lejk;->g()Lehd;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-eqz v1, :cond_1

    iget v1, v1, Lehd;->a:I

    iget v0, v0, Lehd;->a:I

    if-eq v1, v0, :cond_2

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_0
.end method
