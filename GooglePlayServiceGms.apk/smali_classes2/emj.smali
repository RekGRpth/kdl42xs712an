.class public Lemj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field private final c:[C


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lemj;->c:[C

    return-void
.end method

.method protected static a(C)Z
    .locals 1

    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    iget v0, p0, Lemj;->a:I

    invoke-virtual {p0, p1, v0}, Lemj;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    new-instance v0, Lemk;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lemj;->c:[C

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-direct {v0, p2, v1, p1}, Lemk;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget v0, p0, Lemj;->a:I

    iget-object v1, p0, Lemj;->c:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget v0, p0, Lemj;->a:I

    iput v0, p0, Lemj;->b:I

    return-void
.end method

.method public final c()Leml;
    .locals 7

    new-instance v0, Leml;

    iget v1, p0, Lemj;->b:I

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lemj;->c:[C

    iget v4, p0, Lemj;->b:I

    iget v5, p0, Lemj;->a:I

    iget v6, p0, Lemj;->b:I

    sub-int/2addr v5, v6

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {v0, v1, v2}, Leml;-><init>(ILjava/lang/Object;)V

    return-object v0
.end method

.method public final d()C
    .locals 2

    invoke-virtual {p0}, Lemj;->e()C

    move-result v0

    iget v1, p0, Lemj;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lemj;->a:I

    return v0
.end method

.method public final e()C
    .locals 2

    iget-object v0, p0, Lemj;->c:[C

    iget v1, p0, Lemj;->a:I

    aget-char v0, v0, v1

    return v0
.end method

.method public final f()V
    .locals 1

    :goto_0
    invoke-virtual {p0}, Lemj;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lemj;->e()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lemj;->d()C

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final g()Leml;
    .locals 3

    invoke-virtual {p0}, Lemj;->f()V

    invoke-virtual {p0}, Lemj;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lemj;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lemj;->e()C

    move-result v1

    const/16 v2, 0x2d

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lemj;->d()C

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lemj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lemj;->e()C

    move-result v1

    invoke-static {v1}, Lemj;->a(C)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lemj;->d()C

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    const-string v0, "Expected an integer"

    iget v1, p0, Lemj;->b:I

    invoke-virtual {p0, v0, v1}, Lemj;->a(Ljava/lang/String;I)V

    :cond_2
    new-instance v1, Leml;

    iget v2, p0, Lemj;->b:I

    invoke-virtual {p0}, Lemj;->c()Leml;

    move-result-object v0

    iget-object v0, v0, Leml;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Leml;-><init>(ILjava/lang/Object;)V

    return-object v1
.end method
