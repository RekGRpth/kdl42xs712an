.class final Lbwf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbvw;

.field final synthetic b:Lbwe;


# direct methods
.method constructor <init>(Lbwe;Lbvw;)V
    .locals 0

    iput-object p1, p0, Lbwf;->b:Lbwe;

    iput-object p2, p0, Lbwf;->a:Lbvw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    new-instance v0, Landroid/content/SyncResult;

    invoke-direct {v0}, Landroid/content/SyncResult;-><init>()V

    iget-object v1, p0, Lbwf;->b:Lbwe;

    iget-object v1, v1, Lbwe;->b:Lbwd;

    iget-object v2, p0, Lbwf;->b:Lbwe;

    iget-object v2, v2, Lbwe;->a:Ljava/lang/String;

    sget-object v3, Lccw;->a:Lccw;

    sget-object v4, Lccw;->b:Lccw;

    invoke-virtual {v1, v2, v3, v4}, Lbwd;->a(Ljava/lang/String;Lccw;Lccw;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "SyncSchedulerImpl"

    const-string v1, "%s sync not pending."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "Ignoring sync request: "

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lbwf;->a:Lbvw;

    invoke-virtual {v1}, Lbvw;->a()Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;

    move-result-object v1

    iget-object v2, p0, Lbwf;->b:Lbwe;

    iget-object v2, v2, Lbwe;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/data/sync/syncadapter/MetadataSyncService;->a(Ljava/lang/String;Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbwf;->b:Lbwe;

    iget-object v0, v0, Lbwe;->b:Lbwd;

    iget-object v1, p0, Lbwf;->b:Lbwe;

    iget-object v1, v1, Lbwe;->a:Ljava/lang/String;

    sget-object v2, Lccw;->b:Lccw;

    sget-object v3, Lccw;->c:Lccw;

    invoke-virtual {v0, v1, v2, v3}, Lbwd;->a(Ljava/lang/String;Lccw;Lccw;)Z

    iget-object v0, p0, Lbwf;->b:Lbwe;

    iget-object v0, v0, Lbwe;->b:Lbwd;

    invoke-static {v0}, Lbwd;->b(Lbwd;)Lcnf;

    move-result-object v0

    invoke-virtual {v0}, Lcnf;->b()Ljava/util/concurrent/Future;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbwf;->b:Lbwe;

    iget-object v1, v1, Lbwe;->b:Lbwd;

    iget-object v2, p0, Lbwf;->b:Lbwe;

    iget-object v2, v2, Lbwe;->a:Ljava/lang/String;

    sget-object v3, Lccw;->b:Lccw;

    sget-object v4, Lccw;->c:Lccw;

    invoke-virtual {v1, v2, v3, v4}, Lbwd;->a(Ljava/lang/String;Lccw;Lccw;)Z

    throw v0
.end method
