.class public final Lhkl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private final b:Lidu;

.field private final c:Lhof;

.field private final d:Lhoj;

.field private final e:Lhjf;

.field private final f:Liha;

.field private final g:Lhtu;


# direct methods
.method public constructor <init>(Lidu;Lhof;Lhoj;Lhjf;Lhtu;Liha;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhkl;->a:J

    iput-object p1, p0, Lhkl;->b:Lidu;

    iput-object p2, p0, Lhkl;->c:Lhof;

    iput-object p3, p0, Lhkl;->d:Lhoj;

    iput-object p4, p0, Lhkl;->e:Lhjf;

    iput-object p5, p0, Lhkl;->g:Lhtu;

    iput-object p6, p0, Lhkl;->f:Liha;

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    iget-object v0, p0, Lhkl;->c:Lhof;

    iget-object v0, v0, Lhof;->d:Lhou;

    invoke-virtual {v0}, Lhou;->a()Livi;

    iget-object v0, p0, Lhkl;->c:Lhof;

    iget-object v0, v0, Lhof;->e:Lhou;

    invoke-virtual {v0}, Lhou;->a()Livi;

    iget-object v0, p0, Lhkl;->g:Lhtu;

    invoke-virtual {v0}, Lhtu;->a()V

    iput-wide p1, p0, Lhkl;->a:J

    return-void
.end method

.method public final a(Lidu;Livi;)V
    .locals 16

    invoke-interface/range {p1 .. p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lhkl;->a:J

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lhkl;->a(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lhkl;->a:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0x6ddd00

    cmp-long v1, v4, v6

    if-ltz v1, :cond_0

    const/4 v4, 0x5

    invoke-interface/range {p1 .. p1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->b()J

    move-result-wide v5

    new-instance v7, Livi;

    sget-object v1, Lihj;->ab:Livk;

    invoke-direct {v7, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    const-wide/16 v8, 0x3e8

    div-long v8, v5, v8

    invoke-virtual {v7, v1, v8, v9}, Livi;->a(IJ)Livi;

    const/4 v1, 0x2

    move-object/from16 v0, p0

    iget-wide v8, v0, Lhkl;->a:J

    sub-long v8, v2, v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v8, v8

    invoke-virtual {v7, v1, v8}, Livi;->e(II)Livi;

    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget-object v8, v0, Lhkl;->c:Lhof;

    iget-object v8, v8, Lhof;->d:Lhou;

    invoke-virtual {v8}, Lhou;->a()Livi;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Livi;->b(ILivi;)Livi;

    const/4 v1, 0x4

    move-object/from16 v0, p0

    iget-object v8, v0, Lhkl;->c:Lhof;

    iget-object v8, v8, Lhof;->e:Lhou;

    invoke-virtual {v8}, Lhou;->a()Livi;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Livi;->b(ILivi;)Livi;

    const/16 v8, 0x10

    move-object/from16 v0, p0

    iget-object v9, v0, Lhkl;->g:Lhtu;

    new-instance v10, Livi;

    sget-object v1, Lihj;->aa:Livk;

    invoke-direct {v10, v1}, Livi;-><init>(Livk;)V

    invoke-static {}, Lhtv;->values()[Lhtv;

    move-result-object v11

    array-length v12, v11

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v12, :cond_3

    aget-object v13, v11, v1

    iget-object v14, v9, Lhtu;->a:[I

    invoke-virtual {v13}, Lhtv;->ordinal()I

    move-result v15

    aget v14, v14, v15

    if-lez v14, :cond_2

    iget v13, v13, Lhtv;->e:I

    invoke-virtual {v10, v13, v14}, Livi;->e(II)Livi;

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v9}, Lhtu;->a()V

    invoke-virtual {v7, v8, v10}, Livi;->b(ILivi;)Livi;

    sub-long/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lhkl;->e:Lhjf;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lhkl;->a:J

    add-long/2addr v5, v8

    invoke-virtual {v1, v5, v6}, Lhjf;->a(J)Livi;

    move-result-object v1

    if-eqz v1, :cond_4

    const/16 v5, 0xc

    invoke-virtual {v7, v5, v1}, Livi;->b(ILivi;)Livi;

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lhkl;->f:Liha;

    iget-object v5, v1, Liha;->b:Lihd;

    invoke-virtual {v5}, Lihd;->a()Livi;

    move-result-object v5

    iget-object v6, v1, Liha;->c:Lihd;

    invoke-virtual {v6}, Lihd;->a()Livi;

    move-result-object v6

    iget-object v1, v1, Liha;->d:Lihd;

    invoke-virtual {v1}, Lihd;->a()Livi;

    move-result-object v8

    if-nez v5, :cond_8

    if-nez v6, :cond_8

    if-nez v8, :cond_8

    const/4 v1, 0x0

    :cond_5
    :goto_2
    if-eqz v1, :cond_6

    const/16 v5, 0xd

    invoke-virtual {v7, v5, v1}, Livi;->b(ILivi;)Livi;

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lhkl;->d:Lhoj;

    if-eqz v1, :cond_7

    const/16 v1, 0xb

    move-object/from16 v0, p0

    iget-object v5, v0, Lhkl;->d:Lhoj;

    iget-object v6, v5, Lhoj;->a:Lhom;

    iget-object v8, v5, Lhoj;->b:Lhok;

    invoke-static {v8}, Lhok;->a(Lhok;)I

    move-result v8

    iget-object v5, v5, Lhoj;->b:Lhok;

    invoke-virtual {v5}, Lhok;->size()I

    move-result v5

    invoke-virtual {v6, v8, v5}, Lhom;->a(II)Livi;

    move-result-object v5

    invoke-virtual {v7, v1, v5}, Livi;->b(ILivi;)Livi;

    :cond_7
    const/16 v1, 0xe

    move-object/from16 v0, p0

    iget-object v5, v0, Lhkl;->b:Lidu;

    invoke-interface {v5}, Lidu;->v()I

    move-result v5

    invoke-virtual {v7, v1, v5}, Livi;->e(II)Livi;

    const/16 v1, 0xf

    move-object/from16 v0, p0

    iget-object v5, v0, Lhkl;->b:Lidu;

    invoke-interface {v5}, Lidu;->w()I

    move-result v5

    invoke-virtual {v7, v1, v5}, Livi;->e(II)Livi;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v7}, Livi;->a(ILivi;)V

    move-object/from16 v0, p0

    iput-wide v2, v0, Lhkl;->a:J

    goto/16 :goto_0

    :cond_8
    new-instance v1, Livi;

    sget-object v9, Lihj;->Z:Livk;

    invoke-direct {v1, v9}, Livi;-><init>(Livk;)V

    if-eqz v5, :cond_9

    const/4 v9, 0x1

    invoke-virtual {v1, v9, v5}, Livi;->b(ILivi;)Livi;

    :cond_9
    if-eqz v6, :cond_a

    const/4 v5, 0x2

    invoke-virtual {v1, v5, v6}, Livi;->b(ILivi;)Livi;

    :cond_a
    if-eqz v8, :cond_5

    const/4 v5, 0x3

    invoke-virtual {v1, v5, v8}, Livi;->b(ILivi;)Livi;

    goto :goto_2
.end method
