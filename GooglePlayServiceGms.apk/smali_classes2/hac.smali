.class public final Lhac;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Map;

.field private c:Ljava/util/Map;

.field private d:Z

.field private e:[Lipv;

.field private f:[Lioj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lhac;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lioj;)Lion;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, Lioj;->a:Ljava/lang/String;

    iget-boolean v0, p0, Lhac;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lion;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lipv;Lioj;)Lion;
    .locals 4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lipv;->b:Ljava/lang/String;

    move-object v1, v0

    :goto_1
    iget-object v3, p2, Lioj;->a:Ljava/lang/String;

    iget-boolean v0, p0, Lhac;->d:Z

    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_1
    sget-object v0, Lhac;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lion;

    goto :goto_0
.end method

.method public final a([Lion;Z)V
    .locals 7

    iput-boolean p2, p0, Lhac;->d:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhac;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhac;->c:Ljava/util/Map;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_4

    aget-object v6, p1, v1

    const/4 v0, 0x0

    iget-object v2, v6, Lion;->a:Lioj;

    if-eqz v2, :cond_7

    iget-object v0, v6, Lion;->a:Lioj;

    iget-object v0, v0, Lioj;->a:Ljava/lang/String;

    move-object v3, v0

    :goto_1
    if-eqz p2, :cond_3

    sget-object v0, Lhac;->a:Ljava/lang/String;

    iget-object v2, v6, Lion;->b:Lipv;

    if-eqz v2, :cond_6

    iget-object v0, v6, Lion;->b:Lipv;

    iget-object v0, v0, Lipv;->b:Ljava/lang/String;

    move-object v2, v0

    :goto_2
    iget-object v0, v6, Lion;->b:Lipv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v6, Lion;->b:Lipv;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, v6, Lion;->a:Lioj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    :goto_3
    invoke-interface {v0, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_3

    :cond_3
    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lion;->a:Lioj;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhac;->c:Ljava/util/Map;

    invoke-interface {v0, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lhac;->b:Ljava/util/Map;

    iget-object v1, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lhac;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhac;->b:Ljava/util/Map;

    iget-object v0, p0, Lhac;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhac;->c:Ljava/util/Map;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lipv;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lipv;

    iput-object v0, p0, Lhac;->e:[Lipv;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lioj;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lioj;

    iput-object v0, p0, Lhac;->f:[Lioj;

    return-void

    :cond_6
    move-object v2, v0

    goto/16 :goto_2

    :cond_7
    move-object v3, v0

    goto/16 :goto_1
.end method

.method public final a(Lipv;)[Lioj;
    .locals 5

    iget-boolean v0, p0, Lhac;->d:Z

    invoke-static {v0}, Lbkm;->a(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lipv;->b:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhac;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v3, v0, [Lioj;

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lion;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Lion;->a:Lioj;

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_1

    :cond_0
    sget-object v0, Lhac;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v3

    :goto_2
    return-object v0

    :cond_2
    invoke-static {}, Lioj;->c()[Lioj;

    move-result-object v0

    goto :goto_2
.end method

.method public final a()[Lipv;
    .locals 1

    iget-boolean v0, p0, Lhac;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhac;->e:[Lipv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->e:[Lipv;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()[Lioj;
    .locals 1

    iget-boolean v0, p0, Lhac;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lhac;->f:[Lioj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    iget-object v0, p0, Lhac;->f:[Lioj;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
