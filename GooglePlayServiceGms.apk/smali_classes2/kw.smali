.class final Lkw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/view/Menu;

.field b:I

.field c:I

.field d:I

.field e:I

.field f:Z

.field g:Z

.field h:Z

.field i:I

.field j:I

.field k:Ljava/lang/CharSequence;

.field l:Ljava/lang/CharSequence;

.field m:I

.field n:C

.field o:C

.field p:I

.field q:Z

.field r:Z

.field s:Z

.field t:I

.field u:I

.field v:Ljava/lang/String;

.field w:Ljava/lang/String;

.field x:Ljava/lang/String;

.field y:Lei;

.field final synthetic z:Lku;


# direct methods
.method public constructor <init>(Lku;Landroid/view/Menu;)V
    .locals 0

    iput-object p1, p0, Lkw;->z:Lku;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lkw;->a:Landroid/view/Menu;

    invoke-virtual {p0}, Lkw;->a()V

    return-void
.end method

.method static a(Ljava/lang/String;)C
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lkw;->z:Lku;

    invoke-static {v0}, Lku;->a(Lku;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "SupportMenuInflater"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot instantiate class: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lkw;->b:I

    iput v0, p0, Lkw;->c:I

    iput v0, p0, Lkw;->d:I

    iput v0, p0, Lkw;->e:I

    iput-boolean v1, p0, Lkw;->f:Z

    iput-boolean v1, p0, Lkw;->g:Z

    return-void
.end method

.method final a(Landroid/view/MenuItem;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lkw;->q:Z

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lkw;->r:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lkw;->s:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v3

    iget v0, p0, Lkw;->p:I

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v3, p0, Lkw;->l:Ljava/lang/CharSequence;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iget v3, p0, Lkw;->m:I

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Lkw;->n:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    move-result-object v0

    iget-char v3, p0, Lkw;->o:C

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setNumericShortcut(C)Landroid/view/MenuItem;

    iget v0, p0, Lkw;->t:I

    if-ltz v0, :cond_0

    iget v0, p0, Lkw;->t:I

    invoke-static {p1, v0}, Leu;->a(Landroid/view/MenuItem;I)V

    :cond_0
    iget-object v0, p0, Lkw;->x:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkw;->z:Lku;

    invoke-static {v0}, Lku;->a(Lku;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The android:onClick attribute cannot be used within a restricted context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v0, Lkv;

    iget-object v3, p0, Lkw;->z:Lku;

    invoke-static {v3}, Lku;->b(Lku;)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lkw;->x:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Lkv;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    :cond_3
    iget v0, p0, Lkw;->p:I

    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    instance-of v0, p1, Llq;

    if-eqz v0, :cond_7

    move-object v0, p1

    check-cast v0, Llq;

    invoke-virtual {v0, v1}, Llq;->a(Z)V

    :cond_4
    :goto_1
    iget-object v0, p0, Lkw;->v:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lkw;->v:Ljava/lang/String;

    invoke-static {}, Lku;->a()[Ljava/lang/Class;

    move-result-object v2

    iget-object v3, p0, Lkw;->z:Lku;

    invoke-static {v3}, Lku;->c(Lku;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lkw;->a(Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {p1, v0}, Leu;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    :goto_2
    iget v0, p0, Lkw;->u:I

    if-lez v0, :cond_5

    if-nez v1, :cond_8

    iget v0, p0, Lkw;->u:I

    invoke-static {p1, v0}, Leu;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    :cond_5
    :goto_3
    iget-object v0, p0, Lkw;->y:Lei;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkw;->y:Lei;

    invoke-static {p1, v0}, Leu;->a(Landroid/view/MenuItem;Lei;)Landroid/view/MenuItem;

    :cond_6
    return-void

    :cond_7
    instance-of v0, p1, Lls;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lls;

    invoke-virtual {v0}, Lls;->a()V

    goto :goto_1

    :cond_8
    const-string v0, "SupportMenuInflater"

    const-string v1, "Ignoring attribute \'itemActionViewLayout\'. Action view already specified."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_9
    move v1, v2

    goto :goto_2
.end method
