.class public Lfqe;
.super Lbos;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfra;
.implements Lfrc;


# instance fields
.field private A:I

.field private B:Ljava/util/List;

.field private final C:Lfqm;

.field private final D:Lboq;

.field private E:Lbon;

.field private F:Lbon;

.field private G:I

.field private H:I

.field private I:I

.field private final J:Lfaj;

.field private final K:Ljava/util/Map;

.field private final L:Lbkt;

.field private final e:Lftz;

.field private final f:Lfrb;

.field protected final g:I

.field protected final h:I

.field protected final i:I

.field protected final j:I

.field protected final k:I

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field n:Lbon;

.field o:Lbon;

.field p:Lbon;

.field private final q:Z

.field private final r:I

.field private final s:I

.field private final t:I

.field private u:Z

.field private v:Lbgo;

.field private final w:Ljava/util/List;

.field private x:Landroid/database/Cursor;

.field private y:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

.field private z:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;)V
    .locals 10

    sget-object v9, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lfqe;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;Lftz;)V
    .locals 6

    invoke-direct {p0, p1}, Lbos;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lfqe;->a()I

    move-result v0

    iput v0, p0, Lfqe;->g:I

    invoke-virtual {p0}, Lfqe;->a()I

    move-result v0

    iput v0, p0, Lfqe;->h:I

    invoke-virtual {p0}, Lfqe;->a()I

    move-result v0

    iput v0, p0, Lfqe;->i:I

    invoke-virtual {p0}, Lfqe;->a()I

    move-result v0

    iput v0, p0, Lfqe;->j:I

    invoke-virtual {p0}, Lfqe;->a()I

    move-result v0

    iput v0, p0, Lfqe;->k:I

    new-instance v0, Lfqm;

    invoke-direct {v0, p0}, Lfqm;-><init>(Lfqe;)V

    iput-object v0, p0, Lfqe;->C:Lfqm;

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfqe;->D:Lboq;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lfqe;->K:Ljava/util/Map;

    new-instance v0, Lbkt;

    const/high16 v1, 0x500000

    invoke-direct {v0, v1}, Lbkt;-><init>(I)V

    iput-object v0, p0, Lfqe;->L:Lbkt;

    iput-object p2, p0, Lfqe;->f:Lfrb;

    iput-object p3, p0, Lfqe;->l:Ljava/lang/String;

    iput-object p4, p0, Lfqe;->m:Ljava/lang/String;

    iput-boolean p5, p0, Lfqe;->q:Z

    iput p6, p0, Lfqe;->s:I

    iput p7, p0, Lfqe;->t:I

    iput-object p9, p0, Lfqe;->e:Lftz;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqe;->w:Ljava/util/List;

    iput-object p8, p0, Lfqe;->B:Ljava/util/List;

    iget-object v0, p0, Lfqe;->e:Lftz;

    invoke-static {p3}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lfqe;->m:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p0

    move-object v3, p0

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lfqe;->J:Lfaj;

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfqe;->E:Lbon;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090020    # com.google.android.gms.R.integer.plus_audience_selection_num_suggested_image_columns

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lfqe;->r:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfqe;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lfqe;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lbol;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lbon;
    .locals 11

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfqe;->z:Ljava/util/List;

    :goto_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p2, :cond_3

    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget v1, p0, Lfqe;->A:I

    if-ge v0, v1, :cond_3

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v8, v4}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "contactsAvatarUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v4

    const-string v8, "secondaryText"

    invoke-virtual {v4, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "contactType"

    invoke-virtual {v0, v4, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v4

    move v1, v6

    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    invoke-interface {v0}, Lgkp;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-static {v0}, Lfqe;->a(Lgkp;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v7, p0, Lfqe;->B:Ljava/util/List;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lfqe;->B:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    :cond_4
    iget-object v7, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    iput v6, p0, Lfqe;->G:I

    iput v6, p0, Lfqe;->H:I

    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    iget v0, p0, Lfqe;->s:I

    if-lez v0, :cond_9

    iget v0, p0, Lfqe;->r:I

    if-lez v0, :cond_9

    iget v0, p0, Lfqe;->s:I

    iget v1, p0, Lfqe;->r:I

    div-int/2addr v0, v1

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lfqe;->s:I

    iget v3, p0, Lfqe;->r:I

    mul-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfqe;->G:I

    iget v0, p0, Lfqe;->G:I

    iget-object v1, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfqe;->G:I

    new-instance v0, Lfqp;

    iget-object v1, p0, Lfqe;->z:Ljava/util/List;

    iget v3, p0, Lfqe;->G:I

    invoke-direct {v0, p0, v1, v3}, Lfqp;-><init>(Lfqe;Ljava/util/List;I)V

    move-object v7, v0

    :goto_3
    iget v0, p0, Lfqe;->t:I

    if-lez v0, :cond_7

    iget v0, p0, Lfqe;->G:I

    iget-object v1, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    iget v0, p0, Lfqe;->t:I

    iget-object v1, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lfqe;->G:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lfqe;->H:I

    new-instance v0, Lfqi;

    iget-object v2, p0, Lfqe;->z:Ljava/util/List;

    iget v3, p0, Lfqe;->G:I

    iget v1, p0, Lfqe;->G:I

    iget v4, p0, Lfqe;->H:I

    add-int/2addr v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfqi;-><init>(Lfqe;Ljava/util/List;III)V

    move-object v2, v0

    :cond_7
    new-instance v0, Lfqh;

    const v1, 0x7f0b035c    # com.google.android.gms.R.string.plus_audience_selection_header_suggested

    const-string v3, "\u2605"

    new-instance v4, Lbom;

    new-array v5, v5, [Lbon;

    aput-object v7, v5, v6

    aput-object v2, v5, v10

    invoke-direct {v4, p0, v5}, Lbom;-><init>(Lbol;[Lbon;)V

    invoke-direct {v0, p0, v1, v3, v4}, Lfqh;-><init>(Lfqe;ILjava/lang/String;Lbon;)V

    move-object v2, v0

    :cond_8
    return-object v2

    :cond_9
    move-object v7, v2

    goto :goto_3
.end method

.method private static a(Lgkp;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-interface {p0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {p0}, Lgkp;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lgkp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lgkp;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Lgkp;->h()Lgks;

    move-result-object v0

    invoke-interface {v0}, Lgks;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v2, v3, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    :cond_0
    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "contactType"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-interface {p0}, Lgkp;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Lgkp;->l()Lgkv;

    move-result-object v0

    invoke-interface {v0}, Lgkv;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "isCircled"

    invoke-interface {p0}, Lgkp;->l()Lgkv;

    move-result-object v3

    invoke-interface {v3}, Lgkv;->b()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    invoke-interface {p0}, Lgkp;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "objectType"

    invoke-interface {p0}, Lgkp;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-interface {p0}, Lgkp;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lgkp;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p0}, Lgkp;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkr;

    invoke-interface {v0}, Lgkr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lgkp;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkr;

    invoke-interface {v0}, Lgkr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lgkp;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lgkp;->i()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p0}, Lgkp;->h()Lgks;

    move-result-object v1

    invoke-interface {v1}, Lgks;->b()Ljava/lang/String;

    move-result-object v1

    :cond_5
    invoke-static {v2, v3, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-interface {p0}, Lgkp;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "secondaryText"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static synthetic b(Lfqe;)Lbkt;
    .locals 1

    iget-object v0, p0, Lfqe;->L:Lbkt;

    return-object v0
.end method

.method static synthetic c(Lfqe;)Lfrb;
    .locals 1

    iget-object v0, p0, Lfqe;->f:Lfrb;

    return-object v0
.end method

.method static synthetic d(Lfqe;)I
    .locals 1

    iget v0, p0, Lfqe;->r:I

    return v0
.end method

.method static synthetic e(Lfqe;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lbol;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic f(Lfqe;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lbol;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public final P_()V
    .locals 0

    return-void
.end method

.method protected a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4

    const/4 v3, 0x1

    const v0, 0x7f0400c6    # com.google.android.gms.R.layout.plus_audience_selection_list_loading

    invoke-virtual {p0, p1, v0}, Lfqe;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0262    # com.google.android.gms.R.id.top_border

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    return-object v1

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected a(Lbgo;)Lbon;
    .locals 9

    const/4 v4, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v6

    iput v6, p0, Lfqe;->I:I

    move v1, v4

    move v2, v4

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {p1, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfee;

    invoke-interface {v0}, Lfee;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v7, 0x1

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    new-instance v7, Lfqh;

    new-instance v8, Lfqn;

    invoke-direct {v8, p0, p1, v2, v1}, Lfqn;-><init>(Lfqe;Lbgo;II)V

    invoke-direct {v7, p0, v3, v8}, Lfqh;-><init>(Lfqe;Ljava/lang/String;Lbon;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    move-object v3, v2

    move v2, v0

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_1

    new-instance v0, Lfqh;

    new-instance v1, Lfqn;

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v4

    invoke-direct {v1, p0, p1, v2, v4}, Lfqn;-><init>(Lfqe;Lbgo;II)V

    invoke-direct {v0, p0, v3, v1}, Lfqh;-><init>(Lfqe;Ljava/lang/String;Lbon;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, Lbom;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbon;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbon;

    invoke-direct {v1, p0, v0}, Lbom;-><init>(Lbol;[Lbon;)V

    return-object v1

    :cond_2
    move v0, v2

    move-object v2, v3

    goto :goto_1
.end method

.method protected a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 3

    const v0, 0x7f0400c3    # com.google.android.gms.R.layout.plus_audience_selection_list_circle

    invoke-virtual {p0, p2, v0}, Lfqe;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Ljava/lang/Object;)V

    invoke-interface {p1}, Lfea;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Z)V

    invoke-interface {p1}, Lfea;->f()I

    move-result v1

    if-gez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b()V

    :goto_0
    iget-object v1, p0, Lfqe;->f:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lbla;->a(Lfea;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setChecked(Z)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Lfra;)V

    return-object v0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(I)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/view/View;)Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;
    .locals 1

    const v0, 0x7f0400c5    # com.google.android.gms.R.layout.plus_audience_selection_list_header

    invoke-virtual {p0, p2, v0}, Lfqe;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->a(Ljava/lang/String;)V

    return-object v0
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 6

    invoke-virtual {p0, p9, p8}, Lfqe;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a()V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Ljava/lang/Object;)V

    invoke-virtual {v1, p2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Ljava/lang/String;)V

    move/from16 v0, p10

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Z)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e(Z)V

    invoke-virtual {v1, p3}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Ljava/lang/String;)V

    invoke-virtual {v1, p7}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->setChecked(Z)V

    move/from16 v0, p12

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c(Z)V

    invoke-virtual {v1, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Lfra;)V

    invoke-static {p4}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v2, p0, Lfqe;->L:Lbkt;

    invoke-virtual {v2, p6}, Lbkt;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    new-instance v3, Lfqf;

    invoke-direct {v3, p0, p6, p1, v1}, Lfqf;-><init>(Lfqe;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lfqf;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    if-eqz p11, :cond_4

    const v2, 0x7f0200bf    # com.google.android.gms.R.drawable.default_avatar

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    goto :goto_1

    :cond_4
    const v2, 0x7f020094    # com.google.android.gms.R.drawable.common_ic_email_black_24

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    goto :goto_1

    :cond_5
    if-eqz p5, :cond_1

    iget-object v2, p0, Lfqe;->L:Lbkt;

    invoke-virtual {v2, p5}, Lbkt;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_6

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lfqe;->J:Lfaj;

    iget-object v2, v2, Lfaj;->a:Lfch;

    invoke-virtual {v2}, Lfch;->d_()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lfqg;

    invoke-direct {v2, p0, p1, v1, p5}, Lfqg;-><init>(Lfqe;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Ljava/lang/String;)V

    iget-object v3, p0, Lfqe;->J:Lfaj;

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-virtual {v3, v2, p5, v4, v5}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    goto :goto_1
.end method

.method public final a(Landroid/database/Cursor;I)V
    .locals 2

    iput-object p1, p0, Lfqe;->x:Landroid/database/Cursor;

    iput p2, p0, Lfqe;->A:I

    iget-object v0, p0, Lfqe;->y:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    iget-object v1, p0, Lfqe;->x:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1}, Lfqe;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lbon;

    move-result-object v0

    iput-object v0, p0, Lfqe;->F:Lbon;

    invoke-virtual {p0}, Lfqe;->l()V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 1

    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lfqe;->I:I

    invoke-virtual {p0, p1}, Lfqe;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    return-void
.end method

.method public final a(Lfqz;Z)V
    .locals 7

    const/4 v2, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lfqz;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Lfee;

    if-eqz v3, :cond_3

    check-cast v0, Lfee;

    invoke-static {v0}, Lbla;->a(Lfee;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v4

    invoke-virtual {p1}, Lfqz;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "selectionSource"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "contactType"

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v0, v4

    :goto_1
    if-eqz p2, :cond_5

    iget-object v1, p0, Lfqe;->f:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Lfqe;->f:Lfrb;

    invoke-virtual {v1, v0, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "selectionSource"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v1, "contactType"

    invoke-interface {v0}, Lfee;->d()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x2

    move-object v2, v3

    goto :goto_0

    :cond_2
    move v0, v2

    move-object v2, v3

    goto :goto_0

    :cond_3
    instance-of v1, v0, Lfea;

    if-eqz v1, :cond_4

    check-cast v0, Lfea;

    invoke-static {v0}, Lbla;->a(Lfea;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_1

    :cond_4
    instance-of v1, v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lfqe;->f:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v0}, Lblc;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    if-eq p1, p0, :cond_2

    iget-object v0, p0, Lfqe;->f:Lfrb;

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfqe;->K:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lfqe;->u:Z

    iget-object v2, p0, Lfqe;->K:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lbla;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lfee;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lfqe;->l()V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lfqe;->notifyDataSetChanged()V

    goto :goto_1
.end method

.method protected final b(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 2

    invoke-virtual {p0, p1, p2, p3, p4}, Lfqe;->a(Lfea;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    invoke-interface {p1}, Lfea;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const v1, 0x7f020216    # com.google.android.gms.R.drawable.plus_iconic_ic_public_16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f02020a    # com.google.android.gms.R.drawable.plus_iconic_ic_circles_black_16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f02020d    # com.google.android.gms.R.drawable.plus_iconic_ic_extended_circles_black_16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f02020c    # com.google.android.gms.R.drawable.plus_iconic_ic_domain_16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lbgo;)V
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfqe;->o:Lbon;

    :goto_0
    invoke-virtual {p0}, Lfqe;->l()V

    return-void

    :cond_0
    new-instance v0, Lfqj;

    invoke-direct {v0, p0, p1}, Lfqj;-><init>(Lfqe;Lbgo;)V

    iput-object v0, p0, Lfqe;->o:Lbon;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 13

    const/4 v7, 0x1

    const/4 v5, 0x0

    iput-boolean v7, p0, Lfqe;->u:Z

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v5

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkp;

    invoke-static {v0}, Lfqe;->a(Lgkp;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lfqe;->I:I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v6, v7

    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    move v4, v5

    move v3, v5

    :goto_2
    if-ge v4, v11, :cond_4

    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    :goto_3
    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v12, Lfqh;

    new-instance v0, Lfqi;

    iget-object v2, p0, Lfqe;->w:Ljava/util/List;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfqi;-><init>(Lfqe;Ljava/util/List;III)V

    invoke-direct {v12, p0, v8, v0}, Lfqh;-><init>(Lfqe;Ljava/lang/String;Lbon;)V

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    move v3, v4

    :goto_4
    add-int/lit8 v4, v4, 0x1

    move-object v8, v0

    goto :goto_2

    :cond_2
    move v6, v5

    goto :goto_1

    :cond_3
    const-string v0, ""

    move-object v9, v0

    goto :goto_3

    :cond_4
    if-eqz v8, :cond_5

    new-instance v9, Lfqh;

    new-instance v0, Lfqi;

    iget-object v2, p0, Lfqe;->w:Ljava/util/List;

    iget-object v1, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfqi;-><init>(Lfqe;Ljava/util/List;III)V

    invoke-direct {v9, p0, v8, v0}, Lfqh;-><init>(Lfqe;Ljava/lang/String;Lbon;)V

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v1, Lbom;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbon;

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbon;

    invoke-direct {v1, p0, v0}, Lbom;-><init>(Lbol;[Lbon;)V

    if-eqz v6, :cond_6

    new-instance v0, Lbom;

    const/4 v2, 0x2

    new-array v2, v2, [Lbon;

    aput-object v1, v2, v5

    iget-object v1, p0, Lfqe;->C:Lfqm;

    aput-object v1, v2, v7

    invoke-direct {v0, p0, v2}, Lbom;-><init>(Lbol;[Lbon;)V

    :goto_5
    iput-object v0, p0, Lfqe;->p:Lbon;

    invoke-virtual {p0}, Lfqe;->l()V

    return-void

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move-object v0, v8

    goto :goto_4
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0}, Lfqe;->notifyDataSetChanged()V

    return-void
.end method

.method public final c(Lbgo;)V
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Lboq;

    invoke-direct {v0, p0}, Lboq;-><init>(Lbol;)V

    iput-object v0, p0, Lfqe;->n:Lbon;

    :goto_0
    invoke-virtual {p0}, Lfqe;->l()V

    return-void

    :cond_0
    new-instance v0, Lfql;

    invoke-direct {v0, p0, p1}, Lfql;-><init>(Lfqe;Lbgo;)V

    iput-object v0, p0, Lfqe;->n:Lbon;

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 2

    iput-object p1, p0, Lfqe;->y:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    iget-object v0, p0, Lfqe;->y:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    iget-object v1, p0, Lfqe;->x:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1}, Lfqe;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lbon;

    move-result-object v0

    iput-object v0, p0, Lfqe;->F:Lbon;

    invoke-virtual {p0}, Lfqe;->l()V

    return-void
.end method

.method protected d()Lbon;
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lfqe;->u:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lfqe;->u:Z

    iget-object v0, p0, Lfqe;->v:Lbgo;

    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lfqe;->v:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lfqe;->K:Ljava/util/Map;

    iget-object v0, p0, Lfqe;->v:Lbgo;

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfee;

    invoke-interface {v0}, Lfee;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    if-eqz v0, :cond_1

    move v1, v2

    :goto_1
    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lfqe;->K:Ljava/util/Map;

    iget-object v0, p0, Lfqe;->w:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    if-eqz v0, :cond_2

    move v1, v2

    :goto_2
    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v3, p0, Lfqe;->K:Ljava/util/Map;

    iget-object v0, p0, Lfqe;->z:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    new-instance v0, Lfqh;

    const v1, 0x7f0b035b    # com.google.android.gms.R.string.plus_audience_selection_header_search

    const-string v3, "\ud83d\udd0d"

    new-instance v4, Lfqo;

    iget-object v5, p0, Lfqe;->K:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lfqo;-><init>(Lfqe;Ljava/util/Collection;)V

    invoke-direct {v0, p0, v1, v3, v4}, Lfqh;-><init>(Lfqe;ILjava/lang/String;Lbon;)V

    iput-object v0, p0, Lfqe;->E:Lbon;

    :cond_3
    new-instance v1, Lbom;

    const/4 v0, 0x5

    new-array v3, v0, [Lbon;

    iget-object v0, p0, Lfqe;->E:Lbon;

    aput-object v0, v3, v2

    iget-object v0, p0, Lfqe;->F:Lbon;

    aput-object v0, v3, v9

    new-instance v4, Lfqh;

    iget-boolean v0, p0, Lfqe;->q:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0b035d    # com.google.android.gms.R.string.plus_audience_selection_header_circles

    :goto_3
    const-string v5, "\u25ef"

    new-instance v6, Lbom;

    new-array v7, v10, [Lbon;

    iget-object v8, p0, Lfqe;->n:Lbon;

    aput-object v8, v7, v2

    iget-object v2, p0, Lfqe;->o:Lbon;

    aput-object v2, v7, v9

    invoke-direct {v6, p0, v7}, Lbom;-><init>(Lbol;[Lbon;)V

    invoke-direct {v4, p0, v0, v5, v6}, Lfqh;-><init>(Lfqe;ILjava/lang/String;Lbon;)V

    aput-object v4, v3, v10

    const/4 v0, 0x3

    iget-object v2, p0, Lfqe;->p:Lbon;

    aput-object v2, v3, v0

    const/4 v2, 0x4

    invoke-virtual {p0}, Lfqe;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfqe;->C:Lfqm;

    :goto_4
    aput-object v0, v3, v2

    invoke-direct {v1, p0, v3}, Lbom;-><init>(Lbol;[Lbon;)V

    return-object v1

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lfqe;->D:Lboq;

    goto :goto_4
.end method

.method public final d(Lbgo;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqe;->u:Z

    iput-object p1, p0, Lfqe;->v:Lbgo;

    invoke-virtual {p0, p1}, Lfqe;->a(Lbgo;)Lbon;

    move-result-object v0

    iput-object v0, p0, Lfqe;->p:Lbon;

    invoke-virtual {p0}, Lfqe;->l()V

    return-void
.end method

.method protected f()Z
    .locals 1

    iget-object v0, p0, Lfqe;->n:Lbon;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqe;->n:Lbon;

    invoke-virtual {v0}, Lbon;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lfqe;->o:Lbon;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfqe;->o:Lbon;

    invoke-virtual {v0}, Lbon;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lfqe;->p:Lbon;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfqe;->p:Lbon;

    invoke-virtual {v0}, Lbon;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfqe;->F:Lbon;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfqe;->F:Lbon;

    invoke-virtual {v0}, Lbon;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lfqe;->G:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lfqe;->H:I

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lfqe;->I:I

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lfqe;->getItemViewType(I)I

    move-result v0

    iget v1, p0, Lfqe;->c:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lfqe;->f:Lfrb;

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    iget-object v0, p0, Lfqe;->J:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lfqe;->f:Lfrb;

    invoke-virtual {v0, p0}, Lfrb;->b(Lfrc;)V

    iget-object v0, p0, Lfqe;->J:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    return-void
.end method

.method protected final l()V
    .locals 1

    invoke-virtual {p0}, Lfqe;->d()Lbon;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfqe;->a(Lbon;)V

    return-void
.end method
