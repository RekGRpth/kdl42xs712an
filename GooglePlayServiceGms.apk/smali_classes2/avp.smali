.class public final Lavp;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:Z

.field private d:Lavq;

.field private e:Z

.field private f:Lavr;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lavp;->b:I

    iput-object v1, p0, Lavp;->d:Lavq;

    iput-object v1, p0, Lavp;->f:Lavr;

    const/4 v0, -0x1

    iput v0, p0, Lavp;->g:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lavp;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lavp;->b()I

    :cond_0
    iget v0, p0, Lavp;->g:I

    return v0
.end method

.method public final a(I)Lavp;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavp;->a:Z

    iput p1, p0, Lavp;->b:I

    return-object p0
.end method

.method public final a(Lavq;)Lavp;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavp;->c:Z

    iput-object p1, p0, Lavp;->d:Lavq;

    return-object p0
.end method

.method public final a(Lavr;)Lavp;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavp;->e:Z

    iput-object p1, p0, Lavp;->f:Lavr;

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lavp;->a(I)Lavp;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lavq;

    invoke-direct {v0}, Lavq;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lavp;->a(Lavq;)Lavp;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lavr;

    invoke-direct {v0}, Lavr;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lavp;->a(Lavr;)Lavp;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 2

    iget-boolean v0, p0, Lavp;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lavp;->b:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_0
    iget-boolean v0, p0, Lavp;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lavp;->d:Lavq;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Lavp;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lavp;->f:Lavr;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lavp;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lavp;->b:I

    invoke-static {v0, v1}, Lizh;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lavp;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lavp;->d:Lavq;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lavp;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lavp;->f:Lavr;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lavp;->g:I

    return v0
.end method
