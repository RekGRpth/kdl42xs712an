.class final Lifc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Z

.field final synthetic b:Liev;


# direct methods
.method private constructor <init>(Liev;)V
    .locals 1

    iput-object p1, p0, Lifc;->b:Liev;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lifc;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Liev;B)V
    .locals 0

    invoke-direct {p0, p1}, Lifc;-><init>(Liev;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x4

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v3, v0, Liev;->v:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lifc;->b:Liev;

    new-instance v4, Liez;

    iget-object v5, p0, Lifc;->b:Liev;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Liez;-><init>(Liev;B)V

    iput-object v4, v0, Liev;->x:Landroid/os/Handler;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.net.wifi.BATCHED_RESULTS"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->v:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v4, p0, Lifc;->b:Liev;

    iget-boolean v4, v4, Liev;->w:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lifc;->b:Liev;

    new-instance v5, Liex;

    iget-object v6, p0, Lifc;->b:Liev;

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Liex;-><init>(Liev;B)V

    iput-object v5, v4, Liev;->y:Liex;

    iget-object v4, p0, Lifc;->b:Liev;

    iget-object v4, v4, Liev;->m:Landroid/content/Context;

    iget-object v5, p0, Lifc;->b:Liev;

    iget-object v5, v5, Liev;->y:Liex;

    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->q:Lifb;

    const/16 v4, 0x550

    invoke-virtual {v0, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v3, "location"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->r:Liey;

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    const-string v3, "passive"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->getProvider(Ljava/lang/String;)Landroid/location/LocationProvider;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->m:Landroid/content/Context;

    const-string v4, "passive"

    iget-object v5, p0, Lifc;->b:Liev;

    iget-object v5, v5, Liev;->o:Lifa;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-virtual {v0, v3, v4, v5, v6}, Lifs;->a(Landroid/content/Context;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_1
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lifc;->a:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->n:Licp;

    sget-object v3, Licn;->a:Licn;

    invoke-virtual {v0, v3}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0}, Lick;->a()V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    move v3, v2

    :goto_0
    if-nez v3, :cond_2

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v4

    iget-object v5, p0, Lifc;->b:Liev;

    iget-object v5, v5, Liev;->m:Landroid/content/Context;

    invoke-virtual {v4, v0, v5}, Lifs;->a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v0, v2

    :goto_1
    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->n:Licp;

    invoke-virtual {v1, v0}, Licp;->c(Z)V

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->z:Lick;

    invoke-interface {v1, v0, v3}, Lick;->a(ZZ)V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->n:Licp;

    invoke-virtual {v1, v0}, Licp;->b(Z)V

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->z:Lick;

    invoke-interface {v1, v0}, Lick;->b(Z)V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    invoke-static {v0}, Liev;->a(Landroid/content/Context;)Z

    move-result v0

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->n:Licp;

    invoke-virtual {v1, v0}, Licp;->a(Z)V

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->z:Lick;

    invoke-interface {v1, v0}, Lick;->c(Z)V

    iget-object v2, p0, Lifc;->b:Liev;

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->m:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->m:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lifc;->b:Liev;

    iget-object v3, v3, Liev;->z:Lick;

    invoke-static {v2, v0, v1, v3}, Liev;->a(Liev;Landroid/net/ConnectivityManager;Landroid/net/wifi/WifiManager;Lick;)V

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    iget-object v1, p0, Lifc;->b:Liev;

    iget-object v1, v1, Liev;->u:Lids;

    invoke-interface {v0, v1}, Lick;->a(Lids;)V

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    invoke-virtual {v0}, Lifs;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0}, Lick;->b()V

    :goto_2
    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v3, v1

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lifc;->b:Liev;

    iget-object v0, v0, Liev;->z:Lick;

    invoke-interface {v0}, Lick;->c()V

    goto :goto_2
.end method
