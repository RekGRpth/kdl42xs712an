.class final Lbwj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwk;


# instance fields
.field private a:I

.field private b:J

.field private d:J

.field private final e:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbwj;->a:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbwj;->d:J

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lbwj;->e:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lbwj;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget v0, p0, Lbwj;->a:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lbwj;->a:I

    iget-object v0, p0, Lbwj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwk;

    iget v2, p0, Lbwj;->a:I

    invoke-interface {v0, v2}, Lbwk;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JJ)V
    .locals 2

    iget v0, p0, Lbwj;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iput-wide p1, p0, Lbwj;->b:J

    iput-wide p3, p0, Lbwj;->d:J

    iget-object v0, p0, Lbwj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbwk;

    invoke-interface {v0, p1, p2, p3, p4}, Lbwk;->a(JJ)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Lbwk;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lbwj;->b:J

    iget-wide v2, p0, Lbwj;->d:J

    invoke-interface {p1, v0, v1, v2, v3}, Lbwk;->a(JJ)V

    iget v0, p0, Lbwj;->a:I

    invoke-interface {p1, v0}, Lbwk;->a(I)V

    iget-object v0, p0, Lbwj;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
