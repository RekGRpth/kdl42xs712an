.class public final Lheb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Landroid/net/Uri;

.field private final f:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "com.android.chrome"

    aput-object v1, v0, v3

    const-string v1, "com.chrome.beta"

    aput-object v1, v0, v4

    const-string v1, "com.google.android.apps.chrome_dev"

    aput-object v1, v0, v5

    const-string v1, "com.google.android.apps.chrome"

    aput-object v1, v0, v6

    sput-object v0, Lheb;->a:[Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GUID"

    aput-object v1, v0, v3

    const-string v1, "NAME_FULL"

    aput-object v1, v0, v4

    const-string v1, "COMPANY_NAME"

    aput-object v1, v0, v5

    const-string v1, "ADDRESS_LINE1"

    aput-object v1, v0, v6

    const-string v1, "ADDRESS_LINE2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ADDRESS_LINE3"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ADDRESS_CITY"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ADDRESS_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ADDRESS_ZIP"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "ADDRESS_COUNTRY_CODE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "PHONE_WHOLE_NUMBER"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "EMAIL_ADDRESS"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "IS_DEFAULT_BILLING_ADDRESS"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "IS_DEFAULT_SHIPPING_ADDRESS"

    aput-object v2, v0, v1

    sput-object v0, Lheb;->b:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "GUID"

    aput-object v1, v0, v3

    const-string v1, "NAME_FULL"

    aput-object v1, v0, v4

    const-string v1, "CREDIT_CARD_NUMBER"

    aput-object v1, v0, v5

    const-string v1, "EXPIRATION_MONTH"

    aput-object v1, v0, v6

    const-string v1, "EXPIRATION_YEAR"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "IS_DEFAULT_CREDIT_CARD"

    aput-object v2, v0, v1

    sput-object v0, Lheb;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lheb;->d:Landroid/content/Context;

    sget-object v0, Lheb;->a:[Ljava/lang/String;

    invoke-static {v0, p2}, Lboz;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string p2, "com.google.android.apps.chrome"

    :cond_0
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".AutofillDataProvider"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "addresses"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "creditcards"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lheb;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lheb;->f:Landroid/net/Uri;

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lheb;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, p3, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Linu;Lioj;)Landroid/content/ContentValues;
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GUID"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v1, "CREDIT_CARD_NUMBER"

    iget-object v2, p1, Linu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "EXPIRATION_MONTH"

    iget v2, p2, Lioj;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "EXPIRATION_YEAR"

    iget v2, p2, Lioj;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "IS_DEFAULT_CREDIT_CARD"

    iget-boolean v2, p2, Lioj;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "NAME_FULL"

    iget-object v2, p2, Lioj;->e:Lipv;

    iget-object v2, v2, Lipv;->a:Lixo;

    iget-object v2, v2, Lixo;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lheb;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lheb;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lixo;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lheb;->c(Landroid/content/Context;Lixo;)Landroid/util/SparseBooleanArray;

    move-result-object v4

    if-eqz v4, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    move v3, v2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    if-ge v3, v5, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    if-nez v0, :cond_3

    const/16 v0, 0x5a

    invoke-virtual {v4, v0, v2}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    return v1

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    move v1, v0

    goto :goto_3
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lipv;)Landroid/content/ContentValues;
    .locals 7

    const/4 v6, 0x2

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "GUID"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p1, Lipv;->a:Lixo;

    const-string v2, "PHONE_WHOLE_NUMBER"

    iget-object v3, p1, Lipv;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "IS_DEFAULT_BILLING_ADDRESS"

    iget-boolean v3, p1, Lipv;->j:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "IS_DEFAULT_SHIPPING_ADDRESS"

    iget-boolean v3, p1, Lipv;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v2, "NAME_FULL"

    iget-object v3, v1, Lixo;->s:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "COMPANY_NAME"

    iget-object v3, v1, Lixo;->r:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lixo;->q:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    const-string v3, "ADDRESS_LINE1"

    iget-object v4, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-lt v2, v6, :cond_1

    const-string v3, "ADDRESS_LINE2"

    iget-object v4, v1, Lixo;->q:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    const-string v2, "ADDRESS_LINE3"

    iget-object v3, v1, Lixo;->q:[Ljava/lang/String;

    aget-object v3, v3, v6

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v2, "ADDRESS_CITY"

    iget-object v3, v1, Lixo;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ADDRESS_STATE"

    iget-object v3, v1, Lixo;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ADDRESS_ZIP"

    iget-object v3, v1, Lixo;->k:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ADDRESS_COUNTRY_CODE"

    iget-object v3, v1, Lixo;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, Lixo;->t:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "EMAIL_ADDRESS"

    iget-object v1, v1, Lixo;->t:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-object v0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;Lixo;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lheb;->c(Landroid/content/Context;Lixo;)Landroid/util/SparseBooleanArray;

    move-result-object v4

    if-eqz v4, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v4}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-eqz v0, :cond_2

    if-ge v3, v5, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    return v0
.end method

.method private static c(Landroid/database/Cursor;Ljava/lang/String;)I
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;Lixo;)Landroid/util/SparseBooleanArray;
    .locals 12

    const/4 v11, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    iget-object v0, p1, Lixo;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v10, Landroid/util/SparseBooleanArray;

    invoke-direct {v10}, Landroid/util/SparseBooleanArray;-><init>()V

    invoke-static {}, Lth;->a()Lth;

    move-result-object v6

    new-instance v0, Lguc;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v2

    iget-object v1, p1, Lixo;->a:Ljava/lang/String;

    invoke-static {v1}, Lhgq;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lguc;-><init>(Landroid/content/Context;Lsf;ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    invoke-virtual {v0}, Lguc;->a()V

    :try_start_0
    invoke-virtual {v6}, Lth;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    const-string v1, "ChromeAutofillDataStore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got country metatadata="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "require"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ChromeAutofillDataStore"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Required fields="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    move v1, v9

    :goto_1
    if-ge v1, v3, :cond_9

    aget-char v4, v2, v1

    sparse-switch v4, :sswitch_data_0

    move v0, v9

    :goto_2
    invoke-virtual {v10, v4, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :sswitch_0
    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v0, v0, v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    goto :goto_2

    :cond_2
    move v0, v9

    goto :goto_2

    :sswitch_1
    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    array-length v0, v0

    if-lt v0, v11, :cond_8

    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v0, v0, v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v8

    goto :goto_2

    :cond_3
    move v0, v9

    goto :goto_2

    :sswitch_2
    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    array-length v0, v0

    const/4 v5, 0x3

    if-lt v0, v5, :cond_8

    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v0, v0, v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v8

    goto :goto_2

    :cond_4
    move v0, v9

    goto :goto_2

    :sswitch_3
    iget-object v0, p1, Lixo;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v8

    goto :goto_2

    :cond_5
    move v0, v9

    goto :goto_2

    :sswitch_4
    iget-object v0, p1, Lixo;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v8

    goto :goto_2

    :cond_6
    move v0, v9

    goto :goto_2

    :sswitch_5
    iget-object v0, p1, Lixo;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v8

    goto :goto_2

    :cond_7
    move v0, v9

    goto :goto_2

    :sswitch_6
    iget-object v0, p1, Lixo;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v8

    goto :goto_2

    :cond_8
    move v0, v9

    goto :goto_2

    :cond_9
    iget-object v0, p1, Lixo;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    iget-object v1, p1, Lixo;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    :cond_a
    const/16 v0, 0x45

    invoke-virtual {v10, v0, v9}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_3
    move-object v5, v10

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "Interrupted while getting address metadata"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "Exception while getting address metadata"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
        0x33 -> :sswitch_2
        0x41 -> :sswitch_0
        0x43 -> :sswitch_5
        0x44 -> :sswitch_4
        0x53 -> :sswitch_3
        0x5a -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Linu;
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Linu;

    invoke-direct {v1}, Linu;-><init>()V

    :try_start_0
    iget-object v2, p0, Lheb;->f:Landroid/net/Uri;

    sget-object v3, Lheb;->c:[Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lheb;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_3

    :cond_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "GUID"

    invoke-static {v2, v3}, Lheb;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "CREDIT_CARD_NUMBER"

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Linu;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v1

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 7

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "getAddresses"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v2, p0, Lheb;->e:Landroid/net/Uri;

    sget-object v3, Lheb;->b:[Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lheb;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lixo;

    invoke-direct {v2}, Lixo;-><init>()V

    const-string v3, "COMPANY_NAME"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->r:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, v2, Lixo;->q:[Ljava/lang/String;

    iget-object v3, v2, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "ADDRESS_LINE1"

    const-string v6, ""

    invoke-static {v1, v5, v6}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v3, v2, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x1

    const-string v5, "ADDRESS_LINE2"

    const-string v6, ""

    invoke-static {v1, v5, v6}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    iget-object v3, v2, Lixo;->q:[Ljava/lang/String;

    const/4 v4, 0x2

    const-string v5, "ADDRESS_LINE3"

    const-string v6, ""

    invoke-static {v1, v5, v6}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v3, "ADDRESS_CITY"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->f:Ljava/lang/String;

    const-string v3, "ADDRESS_STATE"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->d:Ljava/lang/String;

    const-string v3, "ADDRESS_ZIP"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->k:Ljava/lang/String;

    const-string v3, "ADDRESS_COUNTRY_CODE"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->a:Ljava/lang/String;

    const-string v3, "NAME_FULL"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->s:Ljava/lang/String;

    const-string v3, "EMAIL_ADDRESS"

    const-string v4, ""

    invoke-static {v1, v3, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lixo;->t:Ljava/lang/String;

    new-instance v3, Lipv;

    invoke-direct {v3}, Lipv;-><init>()V

    const-string v4, "GUID"

    invoke-static {v1, v4}, Lheb;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lipv;->b:Ljava/lang/String;

    const-string v4, "IS_DEFAULT_SHIPPING_ADDRESS"

    invoke-static {v1, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lipv;->h:Z

    const-string v4, "IS_DEFAULT_BILLING_ADDRESS"

    invoke-static {v1, v4}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lipv;->j:Z

    const-string v4, "PHONE_WHOLE_NUMBER"

    const-string v5, ""

    invoke-static {v1, v4, v5}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lipv;->d:Ljava/lang/String;

    iput-object v2, v3, Lipv;->a:Lixo;

    iget-object v4, p0, Lheb;->d:Landroid/content/Context;

    invoke-static {v4, v2}, Lheb;->a(Landroid/content/Context;Lixo;)Z

    move-result v4

    iput-boolean v4, v3, Lipv;->f:Z

    iget-object v4, p0, Lheb;->d:Landroid/content/Context;

    invoke-static {v4, v2}, Lheb;->b(Landroid/content/Context;Lixo;)Z

    move-result v2

    iput-boolean v2, v3, Lipv;->g:Z

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    :cond_1
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v0
.end method

.method public final a(Lioj;Linu;)V
    .locals 2

    iget-object v0, p1, Lioj;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "addPaymentInstrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lheb;->f:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-static {v1, p2, p1}, Lheb;->a(Ljava/lang/String;Linu;Lioj;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lheb;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public final a(Lipv;)V
    .locals 2

    iget-object v0, p1, Lipv;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "addAddress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lheb;->e:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-static {v1, p1}, Lheb;->b(Ljava/lang/String;Lipv;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lheb;->a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method public final a(Ljava/lang/String;Lioj;Linu;)V
    .locals 2

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "updatePaymentInstrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lheb;->f:Landroid/net/Uri;

    invoke-static {p1, p3, p2}, Lheb;->a(Ljava/lang/String;Linu;Lioj;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lheb;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)I

    return-void
.end method

.method public final a(Ljava/lang/String;Lipv;)V
    .locals 2

    const-string v0, "ChromeAutofillDataStore"

    const-string v1, "updateAddress"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lheb;->e:Landroid/net/Uri;

    invoke-static {p1, p2}, Lheb;->b(Ljava/lang/String;Lipv;)Landroid/content/ContentValues;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lheb;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)I

    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 9

    const/4 v1, 0x0

    const/4 v8, -0x1

    const-string v0, "ChromeAutofillDataStore"

    const-string v2, "getPaymentInstruments"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v0, p0, Lheb;->f:Landroid/net/Uri;

    sget-object v2, Lheb;->c:[Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lheb;->a(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_5

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "CREDIT_CARD_NUMBER"

    const-string v2, ""

    invoke-static {v1, v0, v2}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v4, 0x4

    if-le v0, v4, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v4, "EXPIRATION_MONTH"

    invoke-static {v1, v4}, Lheb;->c(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    const-string v5, "EXPIRATION_YEAR"

    invoke-static {v1, v5}, Lheb;->c(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    new-instance v6, Lioj;

    invoke-direct {v6}, Lioj;-><init>()V

    const/4 v7, 0x0

    iput v7, v6, Lioj;->d:I

    invoke-static {v2}, Lgth;->a(Ljava/lang/String;)I

    move-result v7

    iput v7, v6, Lioj;->c:I

    const-string v7, "GUID"

    invoke-static {v1, v7}, Lheb;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lioj;->a:Ljava/lang/String;

    iput-object v0, v6, Lioj;->m:Ljava/lang/String;

    iput v4, v6, Lioj;->j:I

    iput v5, v6, Lioj;->k:I

    const/4 v0, 0x1

    iput v0, v6, Lioj;->h:I

    const-string v0, "IS_DEFAULT_CREDIT_CARD"

    invoke-static {v1, v0}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lioj;->f:Z

    invoke-static {v2}, Lgzb;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    iput v0, v6, Lioj;->h:I

    iget-object v0, v6, Lioj;->g:[I

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v6, Lioj;->g:[I

    :cond_0
    const/4 v0, -0x1

    invoke-static {v4, v5, v0}, Lgth;->a(III)I

    move-result v0

    if-ne v0, v8, :cond_1

    iget-object v0, v6, Lioj;->g:[I

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v6, Lioj;->g:[I

    :cond_1
    const-string v0, "NAME_FULL"

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lheb;->a(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Lipv;

    invoke-direct {v2}, Lipv;-><init>()V

    iput-object v2, v6, Lioj;->e:Lipv;

    iget-object v2, v6, Lioj;->e:Lipv;

    new-instance v4, Lixo;

    invoke-direct {v4}, Lixo;-><init>()V

    iput-object v4, v2, Lipv;->a:Lixo;

    iget-object v2, v6, Lioj;->e:Lipv;

    iget-object v2, v2, Lipv;->a:Lixo;

    iput-object v0, v2, Lixo;->s:Ljava/lang/String;

    :cond_2
    invoke-static {v6}, Lgth;->b(Lioj;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lioj;->b:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move-object v0, v2

    goto/16 :goto_1

    :cond_5
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_6
    return-object v3
.end method
