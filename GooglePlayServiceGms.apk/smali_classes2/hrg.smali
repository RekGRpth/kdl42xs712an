.class public final Lhrg;
.super Lizc;
.source "SourceFile"


# static fields
.field private static final a:[B


# instance fields
.field private b:Lixv;

.field private n:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lhrg;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lizc;-><init>(Ljava/lang/String;I)V

    const/16 v0, 0x101

    invoke-virtual {p0, v0}, Lhrg;->b(I)V

    invoke-direct {p0, p2}, Lhrg;->a([B)V

    return-void
.end method

.method private declared-synchronized a([B)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    array-length v0, p1

    if-lez v0, :cond_0

    new-instance v0, Lhrh;

    invoke-direct {v0, p1}, Lhrh;-><init>([B)V

    iput-object v0, p0, Lhrg;->b:Lixv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lhrg;->b:Lixv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Licj;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MultipartRequest.generateBlockData(): monitor on \'this\' must be held by the current thread"

    invoke-static {v0, p0}, Livu;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lhrg;->n:[B

    if-nez v0, :cond_2

    sget-boolean v0, Licj;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MultipartRequest.generatePayloadHeader(): monitor on \'this\' must be held by the current thread"

    invoke-static {v0, p0}, Livu;->a(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Lhrg;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {p0}, Lhrg;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhrg;->k()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    const-string v2, "POST"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    const/16 v2, 0x6d72

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    const-string v2, "ROOT"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    iget-object v2, p0, Lhrg;->b:Lixv;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhrg;->b:Lixv;

    invoke-interface {v2}, Lixv;->Y_()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lhrg;->b:Lixv;

    invoke-interface {v2}, Lixv;->Y_()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const-string v2, "g"

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lhrg;->n:[B

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lizc;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhrg;->n:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lhrg;->b:Lixv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Lizc;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lhrg;->n:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final b()Ljava/io/InputStream;
    .locals 5

    invoke-direct {p0}, Lhrg;->l()V

    iget-object v0, p0, Lhrg;->b:Lixv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrg;->b:Lixv;

    invoke-interface {v0}, Lixv;->Y_()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Livd;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lhrg;->n:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v2, Ljava/io/ByteArrayInputStream;

    sget-object v3, Lhrg;->a:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2}, Livd;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Livd;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lhrg;->n:[B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v2, p0, Lhrg;->b:Lixv;

    invoke-interface {v2}, Lixv;->Z_()Ljava/io/InputStream;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayInputStream;

    sget-object v4, Lhrg;->a:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1, v2, v3}, Livd;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;)V

    goto :goto_0
.end method

.method protected final c()I
    .locals 2

    invoke-direct {p0}, Lhrg;->l()V

    iget-object v0, p0, Lhrg;->n:[B

    array-length v0, v0

    sget-object v1, Lhrg;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lhrg;->b:Lixv;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrg;->b:Lixv;

    invoke-interface {v1}, Lixv;->Y_()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method
