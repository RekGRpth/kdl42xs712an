.class final Lbsb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwk;


# instance fields
.field final synthetic a:Lbsp;

.field final synthetic b:Lbsa;


# direct methods
.method constructor <init>(Lbsa;Lbsp;)V
    .locals 0

    iput-object p1, p0, Lbsb;->b:Lbsa;

    iput-object p2, p0, Lbsb;->a:Lbsp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lbsb;->b:Lbsa;

    iget-object v1, p0, Lbsb;->a:Lbsp;

    invoke-static {v0, v1}, Lbsa;->a(Lbsa;Lbsp;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lbsb;->b:Lbsa;

    invoke-static {v0}, Lbsa;->b(Lbsa;)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lchq;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const-string v3, "Error downloading file"

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lchq;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OpenContentsOperation"

    const-string v2, "Error downloading file."

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(JJ)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lbsb;->b:Lbsa;

    invoke-static {v0}, Lbsa;->a(Lbsa;)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lchq;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;-><init>(JJ)V

    invoke-interface {v0, v1}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "OpenContentsOperation"

    const-string v2, "Error syncing file."

    invoke-static {v1, v0, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
