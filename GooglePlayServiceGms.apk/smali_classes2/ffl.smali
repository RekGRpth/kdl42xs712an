.class public final Lffl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[B

.field public static final b:[B

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v2, [B

    sput-object v0, Lffl;->a:[B

    new-array v0, v2, [B

    sput-object v0, Lffl;->b:[B

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "photo_id"

    aput-object v1, v0, v2

    sput-object v0, Lffl;->c:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "data15"

    aput-object v1, v0, v2

    sput-object v0, Lffl;->d:[Ljava/lang/String;

    return-void
.end method

.method private static a(Landroid/content/Context;J)J
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v3, 0x0

    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lffl;->c:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v0, "PeopleService"

    const-string v1, "Contacts query failed."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v6

    :goto_0
    return-wide v0

    :cond_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    int-to-long v0, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic a(Lfbz;Lffc;Lfju;)V
    .locals 3

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lfju;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    :goto_0
    iget v1, p1, Lffc;->a:I

    iget-object v2, p1, Lffc;->b:Landroid/os/Bundle;

    invoke-interface {p0, v1, v2, v0}, Lfbz;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PeopleService"

    const-string v2, "Client died?"

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;JZ)V
    .locals 11

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "loadContactImage: cid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " thumbnail="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lffn;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p1

    move-wide/from16 v8, p5

    move/from16 v10, p7

    invoke-direct/range {v2 .. v10}, Lffn;-><init>(Landroid/content/Context;Ljava/lang/String;ILfbz;Landroid/content/Context;JZ)V

    invoke-interface {p0, p1, v2}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;)V
    .locals 7

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadRemoteImage: url="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lffr;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lffr;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lfbz;Z)V

    invoke-interface {p0, p1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;II)V
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "loadAvatarByUrl: url="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " opts="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 v2, p7, 0x1

    if-eqz v2, :cond_1

    move v6, v0

    :goto_0
    new-instance v2, Lbki;

    invoke-direct {v2, p5}, Lbki;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lbki;->b()Lbkh;

    invoke-static {p1, p6}, Lfjp;->a(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lbki;->a(I)Lbkh;

    if-nez v6, :cond_2

    :goto_1
    iput-boolean v0, v2, Lbki;->d:Z

    new-instance v0, Lffr;

    invoke-virtual {v2}, Lbki;->a()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lffr;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lfbz;Z)V

    invoke-interface {p0, p1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void

    :cond_1
    move v6, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadOwnerCoverPhoto: account="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pageId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " minimumWidth="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lffp;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p5

    move-object v5, p6

    move-object v6, p4

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lffp;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lfbz;I)V

    invoke-interface {p0, p1, v0}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method public static a(Lffd;Landroid/content/Context;Ljava/lang/String;ILfbz;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 10

    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PeopleService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "loadOwnerAvatar: account="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pageId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p7

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " opts="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p8

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lffq;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    move-object/from16 v6, p6

    move-object v7, p4

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lffq;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lfbz;II)V

    invoke-interface {p0, p1, v1}, Lffd;->a(Landroid/content/Context;Lffb;)V

    return-void
.end method

.method static synthetic a(Lfju;[B)V
    .locals 1

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lffm;

    invoke-direct {v0, p0, p1}, Lffm;-><init>(Lfju;[B)V

    invoke-static {v0}, Lfjr;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;JZ)[B
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lffl;->b(Landroid/content/Context;JZ)[B

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/Context;JZ)[B
    .locals 6

    const/4 v3, 0x0

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Full size images not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lffl;->a(Landroid/content/Context;J)J

    move-result-wide v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-gez v4, :cond_1

    const-string v0, "PeopleService"

    const-string v1, "Contact has no thumbnail."

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v3

    :cond_1
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lffl;->d:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "PeopleService"

    const-string v1, "Contacts query failed."

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v0, "PeopleService"

    const-string v2, "Unable to load thumbnail."

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method
