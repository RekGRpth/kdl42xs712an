.class public final Liqz;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:I

.field public e:Liqa;

.field public f:[Lirb;

.field public g:Liqv;

.field public h:[Liqe;

.field public i:Z

.field public j:Liqp;

.field public k:[Liqo;

.field public l:[Liqt;

.field public m:Liqs;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Liqz;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Liqz;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liqz;->c:J

    iput v3, p0, Liqz;->d:I

    iput-object v2, p0, Liqz;->e:Liqa;

    invoke-static {}, Lirb;->c()[Lirb;

    move-result-object v0

    iput-object v0, p0, Liqz;->f:[Lirb;

    iput-object v2, p0, Liqz;->g:Liqv;

    invoke-static {}, Liqe;->c()[Liqe;

    move-result-object v0

    iput-object v0, p0, Liqz;->h:[Liqe;

    iput-boolean v3, p0, Liqz;->i:Z

    iput-object v2, p0, Liqz;->j:Liqp;

    invoke-static {}, Liqo;->c()[Liqo;

    move-result-object v0

    iput-object v0, p0, Liqz;->k:[Liqo;

    invoke-static {}, Liqt;->c()[Liqt;

    move-result-object v0

    iput-object v0, p0, Liqz;->l:[Liqt;

    iput-object v2, p0, Liqz;->m:Liqs;

    const/4 v0, -0x1

    iput v0, p0, Liqz;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    const/4 v2, 0x1

    iget-object v3, p0, Liqz;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    const/4 v2, 0x2

    iget-object v3, p0, Liqz;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    const/4 v2, 0x6

    iget-wide v3, p0, Liqz;->c:J

    invoke-static {v2, v3, v4}, Lizn;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, Liqz;->d:I

    if-eqz v2, :cond_0

    const/4 v2, 0x7

    iget v3, p0, Liqz;->d:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Liqz;->e:Liqa;

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    iget-object v3, p0, Liqz;->e:Liqa;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Liqz;->f:[Lirb;

    if-eqz v2, :cond_4

    iget-object v2, p0, Liqz;->f:[Lirb;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Liqz;->f:[Lirb;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Liqz;->f:[Lirb;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/16 v4, 0xc

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    :cond_4
    iget-object v2, p0, Liqz;->g:Liqv;

    if-eqz v2, :cond_5

    const/16 v2, 0xd

    iget-object v3, p0, Liqz;->g:Liqv;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Liqz;->h:[Liqe;

    if-eqz v2, :cond_8

    iget-object v2, p0, Liqz;->h:[Liqe;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Liqz;->h:[Liqe;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    iget-object v3, p0, Liqz;->h:[Liqe;

    aget-object v3, v3, v0

    if-eqz v3, :cond_6

    const/16 v4, 0x10

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v2

    :cond_8
    iget-boolean v2, p0, Liqz;->i:Z

    if-eqz v2, :cond_9

    const/16 v2, 0x13

    iget-boolean v3, p0, Liqz;->i:Z

    invoke-static {v2}, Lizn;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Liqz;->j:Liqp;

    if-eqz v2, :cond_a

    const/16 v2, 0x65

    iget-object v3, p0, Liqz;->j:Liqp;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Liqz;->k:[Liqo;

    if-eqz v2, :cond_d

    iget-object v2, p0, Liqz;->k:[Liqo;

    array-length v2, v2

    if-lez v2, :cond_d

    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Liqz;->k:[Liqo;

    array-length v3, v3

    if-ge v0, v3, :cond_c

    iget-object v3, p0, Liqz;->k:[Liqo;

    aget-object v3, v3, v0

    if-eqz v3, :cond_b

    const/16 v4, 0x66

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    move v0, v2

    :cond_d
    iget-object v2, p0, Liqz;->l:[Liqt;

    if-eqz v2, :cond_f

    iget-object v2, p0, Liqz;->l:[Liqt;

    array-length v2, v2

    if-lez v2, :cond_f

    :goto_3
    iget-object v2, p0, Liqz;->l:[Liqt;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    iget-object v2, p0, Liqz;->l:[Liqt;

    aget-object v2, v2, v1

    if-eqz v2, :cond_e

    const/16 v3, 0x67

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_f
    iget-object v1, p0, Liqz;->m:Liqs;

    if-eqz v1, :cond_10

    const/16 v1, 0x68

    iget-object v2, p0, Liqz;->m:Liqs;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iput v0, p0, Liqz;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liqz;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liqz;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Liqz;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput v0, p0, Liqz;->d:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Liqz;->e:Liqa;

    if-nez v0, :cond_1

    new-instance v0, Liqa;

    invoke-direct {v0}, Liqa;-><init>()V

    iput-object v0, p0, Liqz;->e:Liqa;

    :cond_1
    iget-object v0, p0, Liqz;->e:Liqa;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Liqz;->f:[Lirb;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lirb;

    if-eqz v0, :cond_2

    iget-object v3, p0, Liqz;->f:[Lirb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lirb;

    invoke-direct {v3}, Lirb;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Liqz;->f:[Lirb;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lirb;

    invoke-direct {v3}, Lirb;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Liqz;->f:[Lirb;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Liqz;->g:Liqv;

    if-nez v0, :cond_5

    new-instance v0, Liqv;

    invoke-direct {v0}, Liqv;-><init>()V

    iput-object v0, p0, Liqz;->g:Liqv;

    :cond_5
    iget-object v0, p0, Liqz;->g:Liqv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Liqz;->h:[Liqe;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Liqe;

    if-eqz v0, :cond_6

    iget-object v3, p0, Liqz;->h:[Liqe;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Liqe;

    invoke-direct {v3}, Liqe;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Liqz;->h:[Liqe;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Liqe;

    invoke-direct {v3}, Liqe;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Liqz;->h:[Liqe;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Liqz;->i:Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Liqz;->j:Liqp;

    if-nez v0, :cond_9

    new-instance v0, Liqp;

    invoke-direct {v0}, Liqp;-><init>()V

    iput-object v0, p0, Liqz;->j:Liqp;

    :cond_9
    iget-object v0, p0, Liqz;->j:Liqp;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x332

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Liqz;->k:[Liqo;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Liqo;

    if-eqz v0, :cond_a

    iget-object v3, p0, Liqz;->k:[Liqo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_c

    new-instance v3, Liqo;

    invoke-direct {v3}, Liqo;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Liqz;->k:[Liqo;

    array-length v0, v0

    goto :goto_5

    :cond_c
    new-instance v3, Liqo;

    invoke-direct {v3}, Liqo;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Liqz;->k:[Liqo;

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x33a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Liqz;->l:[Liqt;

    if-nez v0, :cond_e

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Liqt;

    if-eqz v0, :cond_d

    iget-object v3, p0, Liqz;->l:[Liqt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_f

    new-instance v3, Liqt;

    invoke-direct {v3}, Liqt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_e
    iget-object v0, p0, Liqz;->l:[Liqt;

    array-length v0, v0

    goto :goto_7

    :cond_f
    new-instance v3, Liqt;

    invoke-direct {v3}, Liqt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Liqz;->l:[Liqt;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Liqz;->m:Liqs;

    if-nez v0, :cond_10

    new-instance v0, Liqs;

    invoke-direct {v0}, Liqs;-><init>()V

    iput-object v0, p0, Liqz;->m:Liqs;

    :cond_10
    iget-object v0, p0, Liqz;->m:Liqs;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x30 -> :sswitch_3
        0x38 -> :sswitch_4
        0x5a -> :sswitch_5
        0x62 -> :sswitch_6
        0x6a -> :sswitch_7
        0x82 -> :sswitch_8
        0x98 -> :sswitch_9
        0x32a -> :sswitch_a
        0x332 -> :sswitch_b
        0x33a -> :sswitch_c
        0x342 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Liqz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    const/4 v0, 0x2

    iget-object v2, p0, Liqz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    const/4 v0, 0x6

    iget-wide v2, p0, Liqz;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    iget v0, p0, Liqz;->d:I

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    iget v2, p0, Liqz;->d:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Liqz;->e:Liqa;

    if-eqz v0, :cond_1

    const/16 v0, 0xb

    iget-object v2, p0, Liqz;->e:Liqa;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Liqz;->f:[Lirb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Liqz;->f:[Lirb;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Liqz;->f:[Lirb;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Liqz;->f:[Lirb;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/16 v3, 0xc

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Liqz;->g:Liqv;

    if-eqz v0, :cond_4

    const/16 v0, 0xd

    iget-object v2, p0, Liqz;->g:Liqv;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_4
    iget-object v0, p0, Liqz;->h:[Liqe;

    if-eqz v0, :cond_6

    iget-object v0, p0, Liqz;->h:[Liqe;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    :goto_1
    iget-object v2, p0, Liqz;->h:[Liqe;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Liqz;->h:[Liqe;

    aget-object v2, v2, v0

    if-eqz v2, :cond_5

    const/16 v3, 0x10

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Liqz;->i:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x13

    iget-boolean v2, p0, Liqz;->i:Z

    invoke-virtual {p1, v0, v2}, Lizn;->a(IZ)V

    :cond_7
    iget-object v0, p0, Liqz;->j:Liqp;

    if-eqz v0, :cond_8

    const/16 v0, 0x65

    iget-object v2, p0, Liqz;->j:Liqp;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_8
    iget-object v0, p0, Liqz;->k:[Liqo;

    if-eqz v0, :cond_a

    iget-object v0, p0, Liqz;->k:[Liqo;

    array-length v0, v0

    if-lez v0, :cond_a

    move v0, v1

    :goto_2
    iget-object v2, p0, Liqz;->k:[Liqo;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Liqz;->k:[Liqo;

    aget-object v2, v2, v0

    if-eqz v2, :cond_9

    const/16 v3, 0x66

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    iget-object v0, p0, Liqz;->l:[Liqt;

    if-eqz v0, :cond_c

    iget-object v0, p0, Liqz;->l:[Liqt;

    array-length v0, v0

    if-lez v0, :cond_c

    :goto_3
    iget-object v0, p0, Liqz;->l:[Liqt;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    iget-object v0, p0, Liqz;->l:[Liqt;

    aget-object v0, v0, v1

    if-eqz v0, :cond_b

    const/16 v2, 0x67

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_c
    iget-object v0, p0, Liqz;->m:Liqs;

    if-eqz v0, :cond_d

    const/16 v0, 0x68

    iget-object v1, p0, Liqz;->m:Liqs;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_d
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liqz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liqz;

    iget-object v2, p0, Liqz;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Liqz;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Liqz;->a:Ljava/lang/String;

    iget-object v3, p1, Liqz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liqz;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Liqz;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Liqz;->b:Ljava/lang/String;

    iget-object v3, p1, Liqz;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Liqz;->c:J

    iget-wide v4, p1, Liqz;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Liqz;->d:I

    iget v3, p1, Liqz;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Liqz;->e:Liqa;

    if-nez v2, :cond_9

    iget-object v2, p1, Liqz;->e:Liqa;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Liqz;->e:Liqa;

    iget-object v3, p1, Liqz;->e:Liqa;

    invoke-virtual {v2, v3}, Liqa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Liqz;->f:[Lirb;

    iget-object v3, p1, Liqz;->f:[Lirb;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Liqz;->g:Liqv;

    if-nez v2, :cond_c

    iget-object v2, p1, Liqz;->g:Liqv;

    if-eqz v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Liqz;->g:Liqv;

    iget-object v3, p1, Liqz;->g:Liqv;

    invoke-virtual {v2, v3}, Liqv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Liqz;->h:[Liqe;

    iget-object v3, p1, Liqz;->h:[Liqe;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-boolean v2, p0, Liqz;->i:Z

    iget-boolean v3, p1, Liqz;->i:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Liqz;->j:Liqp;

    if-nez v2, :cond_10

    iget-object v2, p1, Liqz;->j:Liqp;

    if-eqz v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Liqz;->j:Liqp;

    iget-object v3, p1, Liqz;->j:Liqp;

    invoke-virtual {v2, v3}, Liqp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Liqz;->k:[Liqo;

    iget-object v3, p1, Liqz;->k:[Liqo;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p0, Liqz;->l:[Liqt;

    iget-object v3, p1, Liqz;->l:[Liqt;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p0, Liqz;->m:Liqs;

    if-nez v2, :cond_14

    iget-object v2, p1, Liqz;->m:Liqs;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v2, p0, Liqz;->m:Liqs;

    iget-object v3, p1, Liqz;->m:Liqs;

    invoke-virtual {v2, v3}, Liqs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Liqz;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liqz;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Liqz;->c:J

    iget-wide v4, p0, Liqz;->c:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Liqz;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liqz;->e:Liqa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqz;->f:[Lirb;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liqz;->g:Liqv;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqz;->h:[Liqe;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Liqz;->i:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x4cf

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liqz;->j:Liqp;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqz;->k:[Liqo;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqz;->l:[Liqt;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Liqz;->m:Liqs;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Liqz;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Liqz;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Liqz;->e:Liqa;

    invoke-virtual {v0}, Liqa;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Liqz;->g:Liqv;

    invoke-virtual {v0}, Liqv;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    const/16 v0, 0x4d5

    goto :goto_4

    :cond_5
    iget-object v0, p0, Liqz;->j:Liqp;

    invoke-virtual {v0}, Liqp;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v1, p0, Liqz;->m:Liqs;

    invoke-virtual {v1}, Liqs;->hashCode()I

    move-result v1

    goto :goto_6
.end method
