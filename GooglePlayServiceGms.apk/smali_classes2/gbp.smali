.class public final Lgbp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Lftb;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLftb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbp;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgbp;->b:Ljava/lang/String;

    iput-object p3, p0, Lgbp;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lgbp;->d:Z

    iput-object p5, p0, Lgbp;->e:Lftb;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLftb;)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lgbp;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLftb;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v1, p0, Lgbp;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Lgbp;->b:Ljava/lang/String;

    iget-object v2, p0, Lgbp;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lgbp;->d:Z

    if-eqz v0, :cond_1

    iget-object v4, p2, Lfrx;->e:Lfrz;

    if-eqz v3, :cond_0

    iget-object v3, v4, Lfrz;->a:Lget;

    invoke-virtual {v3, v1, v2}, Lget;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_0
    iget-object v2, v4, Lfrz;->b:Lgbh;

    const/4 v3, 0x0

    invoke-static {v3, v0}, Lgbh;->a(Lgbi;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lgbh;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/lso/RevokeToken;

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    :goto_0
    iget-object v0, p0, Lgbp;->e:Lftb;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lftb;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p2, Lfrx;->c:Lfsj;

    invoke-virtual {v0, v1, v2, v3}, Lfsj;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgbp;->e:Lftb;

    invoke-interface {v0, v8, v1, v6}, Lftb;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgbp;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgbp;->e:Lftb;

    invoke-interface {v1, v8, v0, v6}, Lftb;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgbp;->e:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6, v6}, Lftb;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgbp;->e:Lftb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbp;->e:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
