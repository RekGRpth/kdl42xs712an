.class public final Lhrz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lhrz;

.field public static final b:Lhrz;

.field public static final c:Lhrz;

.field public static final d:Lhrz;

.field public static final e:Lhrz;

.field public static final f:Lhrz;

.field public static final g:Lhrz;

.field public static final h:Lhrz;

.field public static final i:Lhrz;

.field public static final j:Lhrz;

.field public static final k:Lhrz;

.field public static final l:Ljava/util/Set;

.field public static final m:Ljava/util/Set;


# instance fields
.field private final n:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lhrz;

    invoke-direct {v0, v3}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->a:Lhrz;

    new-instance v0, Lhrz;

    invoke-direct {v0, v4}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->b:Lhrz;

    new-instance v0, Lhrz;

    invoke-direct {v0, v5}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->c:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->d:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->e:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->f:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->g:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->h:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->i:Lhrz;

    new-instance v0, Lhrz;

    const/16 v1, 0x200

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->j:Lhrz;

    new-instance v0, Lhrz;

    const/high16 v1, -0x80000000

    invoke-direct {v0, v1}, Lhrz;-><init>(I)V

    sput-object v0, Lhrz;->k:Lhrz;

    const/16 v0, 0xa

    new-array v0, v0, [Lhrz;

    sget-object v1, Lhrz;->a:Lhrz;

    aput-object v1, v0, v6

    sget-object v1, Lhrz;->b:Lhrz;

    aput-object v1, v0, v3

    sget-object v1, Lhrz;->c:Lhrz;

    aput-object v1, v0, v4

    sget-object v1, Lhrz;->d:Lhrz;

    aput-object v1, v0, v7

    sget-object v1, Lhrz;->e:Lhrz;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, Lhrz;->f:Lhrz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhrz;->g:Lhrz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhrz;->h:Lhrz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhrz;->i:Lhrz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhrz;->j:Lhrz;

    aput-object v2, v0, v1

    invoke-static {v0}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lhrz;->l:Ljava/util/Set;

    const/4 v0, 0x6

    new-array v0, v0, [Lhrz;

    sget-object v1, Lhrz;->c:Lhrz;

    aput-object v1, v0, v6

    sget-object v1, Lhrz;->d:Lhrz;

    aput-object v1, v0, v3

    sget-object v1, Lhrz;->e:Lhrz;

    aput-object v1, v0, v4

    sget-object v1, Lhrz;->f:Lhrz;

    aput-object v1, v0, v7

    sget-object v1, Lhrz;->i:Lhrz;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, Lhrz;->j:Lhrz;

    aput-object v2, v0, v1

    invoke-static {v0}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lhrz;->m:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lhrz;->n:I

    return-void
.end method

.method public static a(Ljava/util/Set;)I
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrz;

    iget v0, v0, Lhrz;->n:I

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static a(I)Ljava/util/Set;
    .locals 4

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v0, Lhrz;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrz;

    iget v3, v0, Lhrz;->n:I

    and-int/2addr v3, p0

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static varargs a([Lhrz;)Ljava/util/Set;
    .locals 4

    new-instance v1, Ljava/util/HashSet;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static b(I)Lhrz;
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x0

    sget-object v0, Lhrz;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrz;

    iget v5, v0, Lhrz;->n:I

    and-int/2addr v5, p0

    if-eqz v5, :cond_2

    add-int/lit8 v1, v1, 0x1

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1
    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-ne v1, v0, :cond_1

    :goto_2
    return-object v2

    :cond_1
    move-object v2, v3

    goto :goto_2

    :cond_2
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lhrz;->n:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lhrz;->n:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
