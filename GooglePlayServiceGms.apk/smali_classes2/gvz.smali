.class final Lgvz;
.super Landroid/widget/Filter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lgvy;


# direct methods
.method constructor <init>(Lgvy;)V
    .locals 0

    iput-object p1, p0, Lgvz;->a:Lgvy;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 7

    if-eqz p1, :cond_1

    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-static {v0}, Lgvy;->a(Lgvy;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtv;

    :try_start_0
    iget-object v1, p0, Lgvz;->a:Lgvy;

    invoke-static {v1}, Lgvy;->b(Lgvy;)C

    move-result v2

    iget-object v1, p0, Lgvz;->a:Lgvy;

    iget-object v3, v1, Lgvy;->a:[C

    iget-object v1, p0, Lgvz;->a:Lgvy;

    invoke-static {v1}, Lgvy;->c(Lgvy;)I

    move-result v4

    iget-object v1, p0, Lgvz;->a:Lgvy;

    invoke-static {v1}, Lgvy;->d(Lgvy;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lgtv;->a(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-interface {v0}, Lgtv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lgvz;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_1
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "AddressSourceResultAdap"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not fetch addresses from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lgtv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7

    const/4 v6, -0x1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtw;

    invoke-virtual {v0}, Lgtw;->b()Lixo;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lgtw;->b()Lixo;

    move-result-object v4

    invoke-direct {p0, v4}, Lgvz;->a(Lixo;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lgtw;->b()Lixo;

    move-result-object v4

    invoke-direct {p0, v4}, Lgvz;->b(Lixo;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lgtw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lgtw;->c()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lgtw;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lgtw;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v6, :cond_4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Lgtw;

    invoke-direct {v5, v0, p2}, Lgtw;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    add-int v0, v1, v2

    invoke-virtual {v3, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v3

    goto/16 :goto_0
.end method

.method private a(Lixo;)Z
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-static {v0}, Lgvy;->e(Lgvy;)[C

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lixo;->q:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-static {v0}, Lgvy;->e(Lgvy;)[C

    move-result-object v3

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-char v5, v3, v2

    sparse-switch v5, :sswitch_data_0

    invoke-static {v5}, Lhgp;->a(C)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v0, 0x1

    invoke-static {p1, v5}, Lgvs;->a(Lixo;C)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    :sswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x41 -> :sswitch_0
        0x4e -> :sswitch_0
    .end sparse-switch
.end method

.method private b(Lixo;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-static {v0}, Lgvy;->e(Lgvy;)[C

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-static {v0}, Lgvy;->e(Lgvy;)[C

    move-result-object v5

    array-length v6, v5

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_5

    aget-char v0, v5, v4

    const/16 v3, 0x41

    if-ne v0, v3, :cond_1

    const/16 v0, 0x31

    :cond_1
    iget-object v3, p0, Lgvz;->a:Lgvy;

    iget-object v7, v3, Lgvy;->a:[C

    if-eqz v7, :cond_3

    array-length v8, v7

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_3

    aget-char v9, v7, v3

    if-ne v9, v0, :cond_2

    move v3, v1

    :goto_3
    if-eqz v3, :cond_4

    invoke-static {p1, v0}, Lgvs;->a(Lixo;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    instance-of v0, p1, Lgtw;

    if-eqz v0, :cond_0

    check-cast p1, Lgtw;

    invoke-virtual {p1}, Lgtw;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 2

    invoke-direct {p0, p1}, Lgvz;->a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    return-object v1
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lgvz;->a:Lgvy;

    iput-object p1, v1, Lgvy;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Lgvz;->a:Lgvy;

    iput-object v0, v1, Lgvy;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lgvz;->a:Lgvy;

    iget-object v0, v0, Lgvy;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvz;->a:Lgvy;

    iget-object v0, v0, Lgvy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-virtual {v0}, Lgvy;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgvz;->a:Lgvy;

    invoke-virtual {v0}, Lgvy;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
