.class final Lhza;
.super Lhyx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;

.field private e:Z


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhza;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyx;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "FastMovingActivityState"

    return-object v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    invoke-super {p0, p1}, Lhyx;->a(Ljava/io/PrintWriter;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\n    Using hardware geofencing="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lhza;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Z)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    const-string v2, "GeofencerStateMachine"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GeofencerStateMachine"

    const-string v3, "Geofence hardware became unavailable."

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-boolean v1, p0, Lhza;->e:Z

    iget-object v1, p0, Lhza;->c:Lhys;

    iget-object v1, v1, Lhys;->l:Lhyl;

    invoke-interface {v1}, Lhyl;->a()Z

    invoke-virtual {p0, v0}, Lhza;->b(Z)V

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    invoke-super {p0}, Lhyx;->b()V

    iget-object v0, p0, Lhza;->a:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->b()Z

    move-result v0

    iput-boolean v0, p0, Lhza;->e:Z

    iget-boolean v0, p0, Lhza;->e:Z

    if-nez v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    const-string v1, "Not using hardware geofence because GPS is disabled."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final b(Z)V
    .locals 6

    const/4 v5, 0x3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhza;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v2, v0, Lhys;->l:Lhyl;

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v0, v0, Lhys;->h:Lhxq;

    iget-object v0, v0, Lhxq;->c:Lhxr;

    iget-object v3, v0, Lhxr;->d:Lhxk;

    iget-object v0, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    const/16 v4, 0x32

    invoke-interface {v3, v0, v4}, Lhxk;->b(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v3

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v0, v0, Lhys;->d:Lhxw;

    iget-object v0, v0, Lhxw;->c:Landroid/util/Pair;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-interface {v2, v3, v0}, Lhyl;->a(Ljava/util/List;Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "GeofencerStateMachine"

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "GeofencerStateMachine"

    const-string v3, "Unable to push geofences to hardware."

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iput-boolean v1, p0, Lhza;->e:Z

    :cond_1
    invoke-super {p0, p1}, Lhyx;->b(Z)V

    return-void

    :cond_2
    const-string v0, "GeofencerStateMachine"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GeofencerStateMachine"

    const-string v2, "Not using geofence hardware because unknown location."

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    invoke-super {p0}, Lhyx;->c()V

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v0, v0, Lhys;->l:Lhyl;

    invoke-interface {v0}, Lhyl;->a()Z

    return-void
.end method

.method protected final d()I
    .locals 5

    iget-object v0, p0, Lhza;->c:Lhys;

    const-wide v1, 0x41024f8000000000L    # 150000.0

    invoke-virtual {p0}, Lhza;->i()D

    move-result-wide v3

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lhys;->c(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x12c

    goto :goto_0
.end method

.method protected final e()I
    .locals 6

    const/16 v0, 0x708

    iget-boolean v1, p0, Lhza;->e:Z

    if-eqz v1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhza;->c:Lhys;

    invoke-virtual {v1}, Lhys;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v0, 0x3c

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lhza;->c:Lhys;

    const-wide v2, 0x41024f8000000000L    # 150000.0

    invoke-virtual {p0}, Lhza;->i()D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Lhys;->c(D)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x14

    iget-object v2, p0, Lhza;->c:Lhys;

    iget v2, v2, Lhys;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method protected final f()I
    .locals 2

    iget-object v0, p0, Lhza;->c:Lhys;

    iget v0, v0, Lhys;->b:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method protected final i()D
    .locals 15

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v0, v0, Lhys;->d:Lhxw;

    iget-object v1, p0, Lhza;->a:Lhyt;

    invoke-static {v1}, Lhyt;->s(Lhyt;)Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->b()J

    move-result-wide v1

    const-wide/32 v3, 0x57e40

    const-wide/32 v5, 0x2bf20

    const/4 v7, 0x3

    invoke-virtual/range {v0 .. v7}, Lhxw;->a(JJJI)Z

    move-result v10

    if-eqz v10, :cond_2

    const-wide/high16 v0, 0x3ff8000000000000L    # 1.5

    :cond_0
    :goto_0
    if-eqz v10, :cond_1

    sget-boolean v2, Lhyb;->a:Z

    if-eqz v2, :cond_1

    const-string v2, "GeofencerStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Stationary detected when in vehicle, lowering speed to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-wide v0

    :cond_2
    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v4, v0, Lhys;->d:Lhxw;

    iget-object v0, v0, Lhys;->c:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v5

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    const/4 v8, 0x0

    const/4 v2, 0x0

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_6

    iget-object v0, v4, Lhxw;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    const/high16 v7, 0x42480000    # 50.0f

    cmpl-float v1, v1, v7

    if-gtz v1, :cond_5

    if-nez v8, :cond_3

    :goto_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    move-object v8, v0

    goto :goto_1

    :cond_3
    iget-object v1, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    sub-long/2addr v11, v13

    const-wide/16 v13, 0x2710

    cmp-long v1, v11, v13

    if-ltz v1, :cond_5

    move-object v9, v0

    :goto_3
    if-eqz v8, :cond_4

    if-eqz v9, :cond_4

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v5, v0

    const-wide/32 v2, 0x249f0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v0, v1, v3

    const-wide/32 v2, 0x249f0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_4

    iget-object v0, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v4, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    iget-object v6, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v7}, Liba;->c(DDDD)D

    move-result-wide v2

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v4, v0

    long-to-double v0, v0

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v4, v0, v4

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    cmpl-double v0, v4, v0

    if-ltz v0, :cond_4

    const-wide v0, 0x40363851eb851eb8L    # 22.22

    const-wide v6, 0x402370a3d70a3d71L    # 9.72

    div-double v8, v2, v4

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    sget-boolean v6, Lhyb;->a:Z

    if-eqz v6, :cond_0

    const-string v6, "LocationHistory"

    const-string v7, "Distance moved: %.2fm, time gap: %.2fs, Estimated speed: %.2fm/s."

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x1

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v8, v2

    const/4 v2, 0x2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    const-wide v0, 0x40363851eb851eb8L    # 22.22

    goto/16 :goto_0

    :cond_5
    move-object v0, v8

    goto/16 :goto_2

    :cond_6
    move-object v9, v2

    goto/16 :goto_3
.end method

.method protected final j()V
    .locals 1

    iget-boolean v0, p0, Lhza;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhza;->e:Z

    iget-object v0, p0, Lhza;->c:Lhys;

    iget-object v0, v0, Lhys;->l:Lhyl;

    invoke-interface {v0}, Lhyl;->a()Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhza;->b(Z)V

    :cond_0
    return-void
.end method
