.class public final Ljcw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lizf;

.field private b:Lizf;

.field private c:Lizf;

.field private d:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljcw;->b:Lizf;

    iput-object v0, p0, Ljcw;->c:Lizf;

    iput-object v0, p0, Ljcw;->a:Lizf;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Ljcw;->d:Ljava/security/SecureRandom;

    return-void
.end method

.method static a(Ljava/security/Key;Ljcr;Ljava/security/Key;)Z
    .locals 2

    invoke-virtual {p1}, Ljcr;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljdc;[B)[B
    .locals 2

    new-instance v0, Ljdd;

    invoke-direct {v0}, Ljdd;-><init>()V

    invoke-virtual {v0, p0}, Ljdd;->a(Ljdc;)Ljdd;

    move-result-object v0

    invoke-static {p1}, Lizf;->a([B)Lizf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljdd;->a(Lizf;)Ljdd;

    move-result-object v0

    invoke-virtual {v0}, Ljdd;->d()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a([B)Ljcw;
    .locals 1

    invoke-static {p1}, Lizf;->a([B)Lizf;

    move-result-object v0

    iput-object v0, p0, Ljcw;->b:Lizf;

    return-object p0
.end method

.method public final a(Ljcr;Ljcq;[B)Ljdc;
    .locals 2

    new-instance v0, Ljdc;

    invoke-direct {v0}, Ljdc;-><init>()V

    invoke-virtual {p1}, Ljcr;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljdc;->a(I)Ljdc;

    move-result-object v0

    invoke-virtual {p2}, Ljcq;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljdc;->b(I)Ljdc;

    move-result-object v0

    iget-object v1, p0, Ljcw;->c:Lizf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljcw;->c:Lizf;

    invoke-virtual {v0, v1}, Ljdc;->a(Lizf;)Ljdc;

    :cond_0
    iget-object v1, p0, Ljcw;->a:Lizf;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljcw;->a:Lizf;

    invoke-virtual {v0, v1}, Ljdc;->b(Lizf;)Ljdc;

    :cond_1
    iget-object v1, p0, Ljcw;->b:Lizf;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljcw;->b:Lizf;

    invoke-virtual {v0, v1}, Ljdc;->d(Lizf;)Ljdc;

    :cond_2
    if-eqz p3, :cond_3

    invoke-static {p3}, Lizf;->a([B)Lizf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljdc;->c(Lizf;)Ljdc;

    :cond_3
    return-object v0
.end method

.method public final a(Ljava/security/Key;Ljcr;Ljava/security/Key;Ljcq;[B)Ljde;
    .locals 3

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    sget-object v0, Ljcq;->a:Ljcq;

    if-ne p4, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported for encrypted messages"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p2}, Ljcr;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljcw;->c:Lizf;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must set a verificationKeyId when using public key signature with encryption"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Ljcw;->d:Ljava/security/SecureRandom;

    invoke-static {p4, v0}, Ljcp;->a(Ljcq;Ljava/security/SecureRandom;)[B

    move-result-object v0

    invoke-virtual {p0, p2, p4, v0}, Ljcw;->a(Ljcr;Ljcq;[B)Ljdc;

    move-result-object v1

    invoke-static {p1, p2, p3}, Ljcw;->a(Ljava/security/Key;Ljcr;Ljava/security/Key;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ljdc;->d()[B

    move-result-object v2

    invoke-static {v2}, Ljcp;->a([B)[B

    move-result-object v2

    invoke-static {v2, p5}, Ljcp;->b([B[B)[B

    move-result-object p5

    :cond_4
    iget-object v2, p0, Ljcw;->d:Ljava/security/SecureRandom;

    invoke-static {p3, p4, v2, v0, p5}, Ljcp;->a(Ljava/security/Key;Ljcq;Ljava/security/SecureRandom;[B[B)[B

    move-result-object v0

    invoke-static {v1, v0}, Ljcw;->a(Ljdc;[B)[B

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Ljcw;->a(Ljava/security/Key;Ljcr;[B)Ljde;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/security/Key;Ljcr;[B)Ljde;
    .locals 3

    iget-object v0, p0, Ljcw;->d:Ljava/security/SecureRandom;

    invoke-static {p2, p1, v0, p3}, Ljcp;->a(Ljcr;Ljava/security/Key;Ljava/security/SecureRandom;[B)[B

    move-result-object v0

    new-instance v1, Ljde;

    invoke-direct {v1}, Ljde;-><init>()V

    invoke-static {p3}, Lizf;->a([B)Lizf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljde;->a(Lizf;)Ljde;

    move-result-object v1

    invoke-static {v0}, Lizf;->a([B)Lizf;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljde;->b(Lizf;)Ljde;

    move-result-object v0

    return-object v0
.end method

.method public final b([B)Ljcw;
    .locals 1

    invoke-static {p1}, Lizf;->a([B)Lizf;

    move-result-object v0

    iput-object v0, p0, Ljcw;->c:Lizf;

    return-object p0
.end method
