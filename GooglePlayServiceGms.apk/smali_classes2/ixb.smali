.class public final Lixb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:F

.field public final c:Lixf;

.field public final d:Liwo;

.field public final e:Liwo;

.field public final f:Lixe;

.field public final g:Lixe;

.field public final h:[F

.field public i:Lixc;

.field public j:Lixd;

.field public k:J

.field public l:J

.field public m:F

.field private final n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lixb;-><init>(I)V

    return-void
.end method

.method private constructor <init>(I)V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lixf;

    invoke-direct {v0}, Lixf;-><init>()V

    iput-object v0, p0, Lixb;->c:Lixf;

    new-instance v0, Lixe;

    invoke-direct {v0}, Lixe;-><init>()V

    iput-object v0, p0, Lixb;->f:Lixe;

    new-instance v0, Lixe;

    invoke-direct {v0}, Lixe;-><init>()V

    iput-object v0, p0, Lixb;->g:Lixe;

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lixb;->h:[F

    iput-wide v1, p0, Lixb;->k:J

    iput-wide v1, p0, Lixb;->l:J

    const/4 v0, 0x0

    iput v0, p0, Lixb;->m:F

    const/16 v0, 0x96

    iput v0, p0, Lixb;->n:I

    new-instance v0, Liwo;

    iget v1, p0, Lixb;->n:I

    invoke-direct {v0, v1}, Liwo;-><init>(I)V

    iput-object v0, p0, Lixb;->d:Liwo;

    new-instance v0, Liwo;

    iget v1, p0, Lixb;->n:I

    invoke-direct {v0, v1}, Liwo;-><init>(I)V

    iput-object v0, p0, Lixb;->e:Liwo;

    const/16 v0, 0x19

    iput v0, p0, Lixb;->a:I

    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lixb;->b:F

    return-void
.end method

.method public static a(Lixe;)F
    .locals 2

    invoke-virtual {p0}, Lixe;->b()F

    move-result v0

    invoke-virtual {p0}, Lixe;->a()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(Lixe;F)F
    .locals 2

    invoke-virtual {p0}, Lixe;->a()I

    move-result v0

    invoke-virtual {p0}, Lixe;->b()F

    move-result v1

    int-to-float v0, v0

    div-float v0, v1, v0

    mul-float v1, p1, p1

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method
