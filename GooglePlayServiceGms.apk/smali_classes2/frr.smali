.class public final Lfrr;
.super Lcb;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfuj;


# instance fields
.field private final a:Lftz;

.field private b:Lftx;

.field private c:Z

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 9

    sget-object v8, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lfrr;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Lftz;)V
    .locals 0

    invoke-direct {p0, p1}, Lcb;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lfrr;->d:Ljava/lang/String;

    iput-object p3, p0, Lfrr;->e:Ljava/lang/String;

    iput-object p4, p0, Lfrr;->f:Ljava/lang/String;

    iput p5, p0, Lfrr;->g:I

    iput p6, p0, Lfrr;->h:I

    iput-object p7, p0, Lfrr;->i:Ljava/lang/String;

    iput-object p8, p0, Lfrr;->a:Lftz;

    return-void
.end method

.method private a(Lftx;)V
    .locals 3

    iget v0, p0, Lfrr;->g:I

    iget v1, p0, Lfrr;->h:I

    iget-object v2, p0, Lfrr;->i:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lftx;->a(Lfuj;IILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-boolean v0, p0, Lcb;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfrr;->m_()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrr;->c:Z

    return-void
.end method

.method protected final a()V
    .locals 1

    invoke-super {p0}, Lcb;->a()V

    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-direct {p0, v0}, Lfrr;->a(Lftx;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lfrr;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final a(Lbbo;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrr;->c:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfrr;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 0

    iput-object p1, p0, Lfrr;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    invoke-virtual {p0, p1}, Lfrr;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfrr;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrr;->c:Z

    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-direct {p0, v0}, Lfrr;->a(Lftx;)V

    return-void
.end method

.method protected final f()V
    .locals 1

    iget-object v0, p0, Lfrr;->b:Lftx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lfrr;->c:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lfrr;->b:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrr;->c:Z

    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    invoke-super {p0}, Lcb;->g()V

    invoke-virtual {p0}, Lfrr;->f()V

    return-void
.end method

.method protected final m_()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lfrr;->b:Lftx;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcb;->o:Landroid/content/Context;

    new-instance v1, Lfwa;

    invoke-direct {v1, v0}, Lfwa;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lfrr;->f:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    aput-object v3, v2, v5

    const-string v3, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v3, v2, v6

    const/4 v3, 0x2

    const-string v4, "https://www.googleapis.com/auth/plus.me"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "service_googleme"

    aput-object v3, v2, v5

    iput-object v2, v1, Lfwa;->d:[Ljava/lang/String;

    iget-object v2, p0, Lfrr;->e:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->c:Ljava/lang/String;

    iget-object v2, p0, Lfrr;->d:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v1

    iget-object v2, p0, Lfrr;->a:Lftz;

    invoke-interface {v2, v0, v1, p0, p0}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfrr;->b:Lftx;

    :cond_0
    iget-object v0, p0, Lfrr;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfrr;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    invoke-virtual {p0, v0}, Lfrr;->b(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p0}, Lfrr;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfrr;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcb;->a()V

    :cond_3
    return-void
.end method
