.class final Lekp;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lejm;


# direct methods
.method constructor <init>(Lejm;)V
    .locals 0

    iput-object p1, p0, Lekp;->a:Lejm;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method

.method private b(Lbjv;I)V
    .locals 3

    if-nez p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lekp;->a:Lejm;

    const/4 v1, 0x0

    invoke-interface {p1, p2, v0, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-interface {p1, p2, v0, v1}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Service broker callback failed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->m:Leji;

    const-string v1, "postinit_failed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->b:Landroid/content/Context;

    invoke-static {v0, p3}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lekp;->a:Lejm;

    invoke-virtual {v0}, Lejm;->j()Z

    move-result v0

    invoke-virtual {p0, v0, p1, p3}, Lekp;->a(ZLbjv;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    :goto_0
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lekp;->a:Lejm;

    iget-object v1, v1, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0

    :cond_0
    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->c:Lemz;

    new-instance v1, Lekq;

    invoke-direct {v1, p0, p1, p3}, Lekq;-><init>(Lekp;Lbjv;Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0
.end method

.method final a(Lels;Lbjv;)V
    .locals 3

    :try_start_0
    iget-object v1, p1, Lels;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Lemb; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p1, Lels;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lemb;

    iget-object v2, p1, Lels;->i:Ljava/lang/String;

    invoke-direct {v0, v2}, Lemb;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catch Lemb; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lemb;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    const/16 v0, 0xa

    invoke-direct {p0, p2, v0}, Lekp;->b(Lbjv;I)V

    :goto_0
    return-void

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v0, 0x0

    :try_start_4
    invoke-direct {p0, p2, v0}, Lekp;->b(Lbjv;I)V
    :try_end_4
    .catch Lemb; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0
.end method

.method final a(ZLbjv;Ljava/lang/String;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x1

    if-nez p1, :cond_0

    const-string v0, "Not initialized"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    invoke-direct {p0, p2, v2}, Lekp;->b(Lbjv;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->i:Lelt;

    invoke-virtual {v0, p3}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v0

    invoke-virtual {v0}, Lels;->d()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "App "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not allowed to use icing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->m:Leji;

    const-string v1, "app_disallowed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    invoke-direct {p0, p2, v2}, Lekp;->b(Lbjv;I)V

    goto :goto_0

    :cond_1
    iput-boolean v3, v0, Lels;->h:Z

    iget-object v1, v0, Lels;->b:Lelu;

    iget-object v1, v1, Lelu;->a:Lejk;

    iget-object v2, v0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lejk;->a(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lels;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lekp;->a:Lejm;

    iget-object v1, v1, Lejm;->c:Lemz;

    new-instance v2, Lekr;

    invoke-direct {v2, p0, p3, v0, p2}, Lekr;-><init>(Lekp;Ljava/lang/String;Lels;Lbjv;)V

    invoke-virtual {v1, v2, v4, v5}, Lemz;->a(Lenf;J)Lenf;

    :goto_1
    iget-object v0, p0, Lekp;->a:Lejm;

    iget-object v0, v0, Lejm;->c:Lemz;

    new-instance v1, Leks;

    invoke-direct {v1, p0}, Leks;-><init>(Lekp;)V

    invoke-virtual {v0, v1, v4, v5}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0, p2}, Lekp;->a(Lels;Lbjv;)V

    goto :goto_1
.end method
