.class public final Lhjp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lidz;


# instance fields
.field final a:Lhjl;

.field final b:Lidu;

.field final c:Lhjf;


# direct methods
.method public constructor <init>(Lhjf;Lidu;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhjl;

    invoke-direct {v0}, Lhjl;-><init>()V

    iput-object v0, p0, Lhjp;->a:Lhjl;

    iput-object p2, p0, Lhjp;->b:Lidu;

    iput-object p1, p0, Lhjp;->c:Lhjf;

    return-void
.end method

.method static a(Ljava/util/TimeZone;Livi;)I
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, v6}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    move v2, v1

    :goto_1
    invoke-virtual {p1, v6}, Livi;->k(I)I

    move-result v3

    if-ge v0, v3, :cond_5

    invoke-virtual {p1, v6, v0}, Livi;->c(II)Livi;

    move-result-object v5

    invoke-virtual {v5, v6}, Livi;->i(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v5, v7}, Livi;->i(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v5, v7}, Livi;->c(I)I

    move-result v3

    invoke-virtual {v5, v6}, Livi;->g(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v2, v1

    :cond_5
    add-int/lit8 v0, v2, 0x1

    new-instance v2, Livi;

    sget-object v3, Lihj;->aP:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    invoke-virtual {v2, v6, v4}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v2, v7, v0}, Livi;->e(II)Livi;

    invoke-virtual {p1, v6, v2}, Livi;->a(ILivi;)V

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "IOHistory"

    const-string v3, "Creating mapping: %s: %d."

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v4, v5, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static b(Livi;)V
    .locals 3

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Livi;->k(I)I

    move-result v0

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_0

    const/16 v1, 0xc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Livi;->j(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Livi;)Z
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
