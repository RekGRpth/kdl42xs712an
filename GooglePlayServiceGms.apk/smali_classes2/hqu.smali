.class final Lhqu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:J

.field private B:J

.field private C:J

.field private D:J

.field private E:J

.field private F:Lhqv;

.field private final G:Livi;

.field a:Z

.field b:Livi;

.field c:Livi;

.field d:Livi;

.field e:I

.field f:J

.field g:Limb;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/Integer;

.field private k:Z

.field private l:J

.field private m:I

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private final r:Lhrv;

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Lhrv;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Livi;Limb;)V
    .locals 6

    const/4 v5, 0x0

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v3, -0x1

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v5, p0, Lhqu;->k:Z

    iput-wide v1, p0, Lhqu;->l:J

    const/4 v0, 0x1

    iput v0, p0, Lhqu;->m:I

    iput-boolean v5, p0, Lhqu;->a:Z

    iput-wide v1, p0, Lhqu;->n:J

    iput-wide v1, p0, Lhqu;->o:J

    iput-wide v1, p0, Lhqu;->p:J

    iput-wide v1, p0, Lhqu;->q:J

    iput v3, p0, Lhqu;->s:I

    iput v3, p0, Lhqu;->t:I

    iput v3, p0, Lhqu;->u:I

    iput v3, p0, Lhqu;->v:I

    iput v3, p0, Lhqu;->e:I

    iput v3, p0, Lhqu;->w:I

    iput v4, p0, Lhqu;->x:F

    iput v4, p0, Lhqu;->y:F

    iput v4, p0, Lhqu;->z:F

    iput-wide v1, p0, Lhqu;->A:J

    iput-wide v1, p0, Lhqu;->B:J

    iput-wide v1, p0, Lhqu;->C:J

    iput-wide v1, p0, Lhqu;->D:J

    iput-wide v1, p0, Lhqu;->f:J

    iput-wide v1, p0, Lhqu;->E:J

    new-instance v0, Lhqv;

    invoke-direct {v0, p0}, Lhqv;-><init>(Lhqu;)V

    iput-object v0, p0, Lhqu;->F:Lhqv;

    iput-object p1, p0, Lhqu;->r:Lhrv;

    invoke-static {p6}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhqu;->g:Limb;

    invoke-static {p2}, Lhsn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqu;->h:Ljava/lang/String;

    iput-object p3, p0, Lhqu;->i:Ljava/lang/String;

    iput-object p4, p0, Lhqu;->j:Ljava/lang/Integer;

    if-eqz p5, :cond_0

    sget-object v0, Lihj;->v:Livk;

    invoke-virtual {p5}, Livi;->c()Livk;

    move-result-object v1

    if-eq v1, v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wront protocol buffer type. Expected "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " but was "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p5}, Livi;->c()Livk;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iput-object p5, p0, Lhqu;->G:Livi;

    invoke-direct {p0}, Lhqu;->d()V

    invoke-direct {p0}, Lhqu;->e()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhqu;->n:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lhqu;->o:J

    return-void
.end method

.method private a(FFFJJJ)Livi;
    .locals 9

    move-wide v0, p6

    invoke-virtual {p0, p4, p5, v0, v1}, Lhqu;->a(JJ)V

    new-instance v4, Livi;

    sget-object v2, Lihj;->aC:Livk;

    const/16 v3, 0x9

    invoke-direct {v4, v2, v3}, Livi;-><init>(Livk;I)V

    const/4 v2, 0x1

    invoke-virtual {v4, v2, p1}, Livi;->a(IF)Livi;

    const/4 v2, 0x2

    invoke-virtual {v4, v2, p2}, Livi;->a(IF)Livi;

    const/4 v2, 0x3

    invoke-virtual {v4, v2, p3}, Livi;->a(IF)Livi;

    iget-object v2, p0, Lhqu;->d:Livi;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lhqu;->d:Livi;

    const/4 v3, 0x1

    invoke-virtual {p0, p4, p5}, Lhqu;->a(J)J

    move-result-wide v5

    invoke-virtual {v2, v3, v5, v6}, Livi;->a(IJ)Livi;

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p8, v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lhqu;->d:Livi;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Livi;->d(I)J

    move-result-wide v5

    invoke-virtual {p0, p4, p5}, Lhqu;->a(J)J

    move-result-wide v7

    invoke-virtual {p0, v5, v6, v7, v8}, Lhqu;->b(JJ)I

    move-result v3

    invoke-virtual {p0, v5, v6, v7, v8}, Lhqu;->c(JJ)I

    move-result v2

    :goto_0
    if-eqz v3, :cond_1

    const/16 v5, 0x8

    invoke-virtual {v4, v5, v3}, Livi;->e(II)Livi;

    :cond_1
    if-eqz v2, :cond_2

    const/16 v3, 0x9

    invoke-virtual {v4, v3, v2}, Livi;->e(II)Livi;

    :cond_2
    return-object v4

    :cond_3
    move-wide/from16 v0, p8

    invoke-virtual {p0, v0, v1, p4, p5}, Lhqu;->b(JJ)I

    move-result v3

    move-wide/from16 v0, p8

    invoke-virtual {p0, v0, v1, p4, p5}, Lhqu;->c(JJ)I

    move-result v2

    goto :goto_0
.end method

.method private declared-synchronized a(Livi;Z)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lhqu;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lhqu;->l:J

    new-instance v0, Livi;

    sget-object v1, Lihj;->P:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    iget-wide v2, p0, Lhqu;->l:J

    invoke-virtual {v0, v1, v2, v3}, Livi;->a(IJ)Livi;

    const/4 v1, 0x4

    iget-wide v2, p0, Lhqu;->n:J

    invoke-virtual {v0, v1, v2, v3}, Livi;->a(IJ)Livi;

    if-eqz p2, :cond_1

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    :goto_0
    iget-object v1, p0, Lhqu;->r:Lhrv;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhqu;->r:Lhrv;

    invoke-virtual {v1, p1, v0}, Lhrv;->b(Livi;Livi;)Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lhqu;->r:Lhrv;

    invoke-virtual {v0}, Lhrv;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v1, 0x3

    :try_start_1
    iget v2, p0, Lhqu;->m:I

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    iget v1, p0, Lhqu;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lhqu;->m:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Livi;J)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add location after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x6

    :try_start_1
    iget-wide v1, p0, Lhqu;->o:J

    sub-long v1, p2, v1

    invoke-virtual {p1, v0, v1, v2}, Livi;->a(IJ)Livi;

    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, p1}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lhqu;->c:Livi;

    const/16 v1, 0x9

    iget-object v2, p0, Lhqu;->d:Livi;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILivi;)Livi;

    :cond_1
    iget-object v0, p0, Lhqu;->c:Livi;

    invoke-virtual {v0}, Livi;->e()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhqu;->b:Livi;

    const/4 v1, 0x4

    iget-object v2, p0, Lhqu;->c:Livi;

    invoke-virtual {v0, v1, v2}, Livi;->a(ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhqu;->F:Lhqv;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lhqv;->a:J

    const-wide/16 v1, 0xc8

    iput-wide v1, v0, Lhqv;->b:J

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lhqv;->c:J

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lhqv;->d:J

    new-instance v0, Livi;

    sget-object v1, Lihj;->Q:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iput-object v0, p0, Lhqu;->b:Livi;

    new-instance v0, Livi;

    sget-object v1, Lihj;->y:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x2

    sget-object v2, Lhri;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v1, 0x5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v1, 0x1

    const-string v2, "1.0"

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    iget-object v1, p0, Lhqu;->G:Livi;

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    iget-object v2, p0, Lhqu;->G:Livi;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILivi;)Livi;

    :cond_0
    iget-object v1, p0, Lhqu;->b:Livi;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;

    new-instance v0, Livi;

    sget-object v1, Lihj;->z:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lhqu;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    iget-object v1, p0, Lhqu;->i:Ljava/lang/String;

    invoke-static {v1}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Livi;

    sget-object v2, Lihj;->A:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    iget-object v3, p0, Lhqu;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Livi;->b(ILjava/lang/String;)Livi;

    iget-object v2, p0, Lhqu;->b:Livi;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    :cond_1
    iget-object v1, p0, Lhqu;->b:Livi;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Livi;

    sget-object v1, Lihj;->F:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iput-object v0, p0, Lhqu;->c:Livi;

    new-instance v0, Livi;

    sget-object v1, Lihj;->ay:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iput-object v0, p0, Lhqu;->d:Livi;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->A:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->B:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->C:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->D:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->f:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhqu;->E:J

    iget-object v0, p0, Lhqu;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhqu;->k:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhqu;->k:Z

    new-instance v0, Livi;

    sget-object v1, Lihj;->o:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lhqu;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    iget-object v1, p0, Lhqu;->c:Livi;

    const/16 v2, 0x63

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()J
    .locals 9

    const-wide/16 v7, 0x1

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lhqu;->n:J

    iget-wide v2, p0, Lhqu;->o:J

    sub-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p0, Lhqu;->l:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhqu;->g:Limb;

    const-string v3, "Timestaime changed from %d to %d."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    add-long/2addr v0, v7

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    iget-wide v0, p0, Lhqu;->l:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v7

    :cond_1
    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final a(J)J
    .locals 6

    iget-wide v0, p0, Lhqu;->q:J

    sub-long v0, p1, v0

    iget-wide v2, p0, Lhqu;->p:J

    iget-wide v4, p0, Lhqu;->o:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lhqu;->a:Z

    invoke-direct {p0}, Lhqu;->c()V

    iget-object v0, p0, Lhqu;->b:Livi;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lhqu;->a(Livi;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lhqu;->b:Livi;

    const/4 v0, 0x0

    iput-object v0, p0, Lhqu;->c:Livi;

    const/4 v0, 0x0

    iput-object v0, p0, Lhqu;->d:Livi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(FFFFFFIJJ)V
    .locals 12

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lhqu;->a:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhqu;->g:Limb;

    const-string v3, "Could not add uncalibrated magnetic field snapshot after the composer is closed."

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-wide v10, p0, Lhqu;->E:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide/from16 v6, p8

    move-wide/from16 v8, p10

    invoke-direct/range {v2 .. v11}, Lhqu;->a(FFFJJJ)Livi;

    move-result-object v2

    move-wide/from16 v0, p8

    iput-wide v0, p0, Lhqu;->E:J

    iget-object v3, p0, Lhqu;->d:Livi;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Livi;->k(I)I

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lhqu;->w:I

    move/from16 v0, p7

    if-eq v3, v0, :cond_3

    :cond_2
    const/4 v3, 0x4

    move/from16 v0, p7

    invoke-virtual {v2, v3, v0}, Livi;->e(II)Livi;

    move/from16 v0, p7

    iput v0, p0, Lhqu;->w:I

    :cond_3
    new-instance v3, Livi;

    sget-object v4, Lihj;->aD:Livk;

    const/4 v5, 0x4

    invoke-direct {v3, v4, v5}, Livi;-><init>(Livk;I)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Livi;->b(ILivi;)Livi;

    iget-object v2, p0, Lhqu;->d:Livi;

    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Livi;->k(I)I

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, Lhqu;->x:F

    sub-float v2, p4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_4

    iget v2, p0, Lhqu;->y:F

    sub-float v2, p5, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_4

    iget v2, p0, Lhqu;->z:F

    sub-float v2, p6, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x358637bd    # 1.0E-6f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_7

    :cond_4
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_6

    :cond_5
    const/4 v2, 0x2

    move/from16 v0, p4

    invoke-virtual {v3, v2, v0}, Livi;->a(IF)Livi;

    const/4 v2, 0x3

    move/from16 v0, p5

    invoke-virtual {v3, v2, v0}, Livi;->a(IF)Livi;

    const/4 v2, 0x4

    move/from16 v0, p6

    invoke-virtual {v3, v2, v0}, Livi;->a(IF)Livi;

    move/from16 v0, p4

    iput v0, p0, Lhqu;->x:F

    move/from16 v0, p5

    iput v0, p0, Lhqu;->y:F

    move/from16 v0, p6

    iput v0, p0, Lhqu;->z:F

    :cond_6
    iget-object v2, p0, Lhqu;->d:Livi;

    const/16 v4, 0xc

    invoke-virtual {v2, v4, v3}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_7
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a(FFFIJJ)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add orientation snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, p5, p6, p7, p8}, Lhqu;->a(JJ)V

    new-instance v2, Livi;

    sget-object v0, Lihj;->az:Livk;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Livi;-><init>(Livk;I)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Livi;->a(IF)Livi;

    const/4 v0, 0x3

    invoke-virtual {v2, v0, p2}, Livi;->a(IF)Livi;

    const/4 v0, 0x2

    invoke-virtual {v2, v0, p3}, Livi;->a(IF)Livi;

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Livi;->a(IJ)Livi;

    :cond_2
    iget-wide v0, p0, Lhqu;->B:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v3

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v5

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->b(JJ)I

    move-result v1

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->c(JJ)I

    move-result v0

    :goto_1
    if-eqz v1, :cond_3

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Livi;->e(II)Livi;

    :cond_3
    if-eqz v0, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Livi;->e(II)Livi;

    :cond_4
    iput-wide p5, p0, Lhqu;->B:J

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lhqu;->t:I

    if-eq v0, p4, :cond_6

    :cond_5
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Livi;->e(II)Livi;

    iput p4, p0, Lhqu;->t:I

    :cond_6
    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_2
    iget-wide v0, p0, Lhqu;->B:J

    invoke-virtual {p0, v0, v1, p5, p6}, Lhqu;->b(JJ)I

    move-result v1

    iget-wide v3, p0, Lhqu;->B:J

    invoke-virtual {p0, v3, v4, p5, p6}, Lhqu;->c(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method public final declared-synchronized a(ILjava/lang/String;Landroid/telephony/CellLocation;ILjava/util/List;J)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lhqu;->a:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhqu;->g:Limb;

    const-string v2, "Could not add cell profile after the composer is closed."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v4, -0x1

    const/4 v3, -0x1

    const/4 v2, -0x1

    const/4 v1, -0x1

    if-eqz p2, :cond_2

    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x3

    if-le v5, v6, :cond_2

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v3, 0x3

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_2
    if-eqz p3, :cond_3

    instance-of v5, p3, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v5, :cond_5

    move-object v0, p3

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v2

    check-cast p3, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {p3}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v1

    :cond_3
    :goto_1
    new-instance v5, Livi;

    sget-object v6, Lihj;->k:Livk;

    invoke-direct {v5, v6}, Livi;-><init>(Livk;)V

    new-instance v6, Livi;

    sget-object v7, Lihj;->j:Livk;

    invoke-direct {v6, v7}, Livi;-><init>(Livk;)V

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v2}, Livi;->e(II)Livi;

    const/4 v2, 0x2

    invoke-virtual {v6, v2, v1}, Livi;->e(II)Livi;

    const/4 v1, 0x3

    invoke-virtual {v6, v1, v3}, Livi;->e(II)Livi;

    const/4 v1, 0x4

    invoke-virtual {v6, v1, v4}, Livi;->e(II)Livi;

    const/4 v1, 0x5

    invoke-virtual {v6, v1, p4}, Livi;->e(II)Livi;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v1, -0x1

    :goto_2
    if-ltz v1, :cond_4

    const/16 v2, 0xa

    invoke-virtual {v6, v2, v1}, Livi;->e(II)Livi;

    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v5, v1, v6}, Livi;->b(ILivi;)Livi;

    const/4 v1, 0x2

    iget-wide v2, p0, Lhqu;->o:J

    sub-long v2, p6, v2

    invoke-virtual {v5, v1, v2, v3}, Livi;->a(IJ)Livi;

    if-eqz p5, :cond_6

    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/NeighboringCellInfo;

    new-instance v3, Livi;

    sget-object v4, Lihj;->j:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    const/4 v4, 0x1

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getLac()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Livi;->e(II)Livi;

    const/4 v4, 0x2

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getCid()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Livi;->e(II)Livi;

    const/16 v4, 0x8

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getPsc()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Livi;->e(II)Livi;

    const/4 v4, 0x5

    invoke-virtual {v1}, Landroid/telephony/NeighboringCellInfo;->getRssi()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Livi;->e(II)Livi;

    const/4 v1, 0x3

    invoke-virtual {v5, v1, v3}, Livi;->a(ILivi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_5
    :try_start_2
    instance-of v5, p3, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v5, :cond_3

    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/cdma/CdmaCellLocation;->getSystemId()I

    move-result v3

    move-object v0, p3

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/telephony/cdma/CdmaCellLocation;->getNetworkId()I

    move-result v2

    check-cast p3, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {p3}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I

    move-result v1

    goto/16 :goto_1

    :pswitch_1
    const/4 v1, 0x3

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x5

    goto :goto_2

    :pswitch_3
    const/4 v1, 0x4

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lhqu;->d:Livi;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, v5}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method final a(JJ)V
    .locals 4

    iget-wide v0, p0, Lhqu;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iput-wide p1, p0, Lhqu;->q:J

    iput-wide p3, p0, Lhqu;->p:J

    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Landroid/location/GpsStatus;J)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add GPS status after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {p1}, Landroid/location/GpsStatus;->getSatellites()Ljava/lang/Iterable;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->aF:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    iget-wide v3, p0, Lhqu;->o:J

    sub-long v3, p2, v3

    long-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/GpsSatellite;

    new-instance v3, Livi;

    sget-object v4, Lihj;->aE:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    const/4 v4, 0x4

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getAzimuth()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Livi;->a(IF)Livi;

    const/4 v4, 0x3

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getElevation()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Livi;->a(IF)Livi;

    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getPrn()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    const/4 v4, 0x2

    invoke-virtual {v0}, Landroid/location/GpsSatellite;->getSnr()F

    move-result v0

    invoke-virtual {v3, v4, v0}, Livi;->a(IF)Livi;

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Livi;->a(ILivi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    iget-object v0, p0, Lhqu;->d:Livi;

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/util/List;J)V
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add wifi profile after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v1, Livi;

    sget-object v0, Lihj;->h:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    new-instance v3, Livi;

    sget-object v4, Lihj;->f:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    iget-object v4, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v4}, Lhsn;->c(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Livi;->b(ILjava/lang/String;)Livi;

    const/16 v6, 0x8

    invoke-virtual {v3, v6, v4, v5}, Livi;->a(IJ)Livi;

    const/16 v4, 0x9

    iget v5, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-static {v3, v0}, Lhsn;->a(Livi;I)V

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v3}, Livi;->a(ILivi;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x1

    :try_start_2
    iget-wide v2, p0, Lhqu;->o:J

    sub-long v2, p2, v2

    invoke-virtual {v1, v0, v2, v3}, Livi;->a(IJ)Livi;

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v1}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Landroid/location/Location;J)Z
    .locals 7

    const/4 v0, 0x0

    const-wide v5, 0x416312d000000000L    # 1.0E7

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_0
    new-instance v0, Livi;

    sget-object v1, Lihj;->r:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    new-instance v1, Livi;

    sget-object v2, Lihj;->i:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Livi;->b(ILivi;)Livi;

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xd

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_4

    const/16 v1, 0x10

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Livi;->a(IF)Livi;

    :cond_4
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    invoke-direct {p0, v0, p2, p3}, Lhqu;->a(Livi;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Livi;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add customized data after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lhqu;->b:Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(JJ)I
    .locals 4

    sub-long v0, p3, p1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhqu;->g:Limb;

    const-string v3, "Later event arrive earlier"

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method final declared-synchronized b()V
    .locals 13

    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lhqu;->F:Lhqv;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iget-wide v0, v6, Lhqv;->d:J

    sub-long v0, v7, v0

    iget-wide v2, v6, Lhqv;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->b:Livi;

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    move-wide v4, v0

    :goto_0
    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->c:Livi;

    if-nez v0, :cond_4

    const-wide/16 v0, 0x0

    move-wide v2, v0

    :goto_1
    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->d:Livi;

    if-nez v0, :cond_5

    const-wide/16 v0, 0x0

    :goto_2
    add-long/2addr v2, v4

    add-long/2addr v2, v0

    iget-wide v0, v6, Lhqv;->d:J

    iget-wide v4, v6, Lhqv;->c:J

    iget-wide v9, v6, Lhqv;->b:J

    const-wide/16 v11, 0x64

    cmp-long v9, v9, v11

    if-gez v9, :cond_6

    iget-wide v0, v6, Lhqv;->b:J

    :goto_3
    iput-wide v0, v6, Lhqv;->b:J

    iput-wide v2, v6, Lhqv;->c:J

    iput-wide v7, v6, Lhqv;->d:J

    :cond_0
    iget-wide v0, v6, Lhqv;->c:J

    const-wide/16 v2, 0x7800

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-wide v0, v6, Lhqv;->a:J

    sub-long v0, v7, v0

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    :cond_1
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lhqu;->c()V

    invoke-direct {p0}, Lhqu;->e()V

    iget-object v0, p0, Lhqu;->b:Livi;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lhqu;->a(Livi;Z)V

    invoke-direct {p0}, Lhqu;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->b:Livi;

    invoke-virtual {v0}, Livi;->e()I

    move-result v0

    int-to-long v0, v0

    move-wide v4, v0

    goto :goto_0

    :cond_4
    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->c:Livi;

    invoke-virtual {v0}, Livi;->e()I

    move-result v0

    int-to-long v0, v0

    move-wide v2, v0

    goto :goto_1

    :cond_5
    iget-object v0, v6, Lhqv;->e:Lhqu;

    iget-object v0, v0, Lhqu;->d:Livi;

    invoke-virtual {v0}, Livi;->e()I

    move-result v0

    int-to-long v0, v0

    goto :goto_2

    :cond_6
    sub-long v4, v2, v4

    long-to-double v4, v4

    sub-long v9, v7, v0

    long-to-double v9, v9

    div-double/2addr v4, v9

    const-wide/16 v9, 0x0

    cmp-long v0, v0, v9

    if-lez v0, :cond_7

    const-wide/16 v0, 0x0

    cmpl-double v0, v4, v0

    if-lez v0, :cond_7

    const-wide/16 v0, 0x7800

    cmp-long v0, v2, v0

    if-gez v0, :cond_7

    const-wide/16 v0, 0x7800

    sub-long/2addr v0, v2

    long-to-double v0, v0

    div-double/2addr v0, v4

    const-wide/16 v4, 0x7d0

    double-to-long v0, v0

    const-wide/16 v9, 0x2

    mul-long/2addr v0, v9

    const-wide/16 v9, 0x3

    div-long/2addr v0, v9

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_3

    :cond_7
    const-wide/16 v0, 0xc8

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(FFFIJJ)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add accelerometer snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, p5, p6, p7, p8}, Lhqu;->a(JJ)V

    new-instance v2, Livi;

    sget-object v0, Lihj;->aA:Livk;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Livi;-><init>(Livk;I)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Livi;->a(IF)Livi;

    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Livi;->a(IF)Livi;

    const/4 v0, 0x3

    invoke-virtual {v2, v0, p3}, Livi;->a(IF)Livi;

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Livi;->a(IJ)Livi;

    :cond_2
    iget-wide v0, p0, Lhqu;->A:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v3

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v5

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->b(JJ)I

    move-result v1

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->c(JJ)I

    move-result v0

    :goto_1
    if-eqz v1, :cond_3

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Livi;->e(II)Livi;

    :cond_3
    if-eqz v0, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Livi;->e(II)Livi;

    :cond_4
    iput-wide p5, p0, Lhqu;->A:J

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lhqu;->s:I

    if-eq v0, p4, :cond_6

    :cond_5
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Livi;->e(II)Livi;

    iput p4, p0, Lhqu;->s:I

    :cond_6
    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_2
    iget-wide v0, p0, Lhqu;->A:J

    invoke-virtual {p0, v0, v1, p5, p6}, Lhqu;->b(JJ)I

    move-result v1

    iget-wide v3, p0, Lhqu;->A:J

    invoke-virtual {p0, v3, v4, p5, p6}, Lhqu;->c(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method

.method final c(JJ)I
    .locals 4

    sub-long v0, p3, p1

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhqu;->g:Limb;

    const-string v3, "Later event arrive earlier"

    invoke-virtual {v2, v3}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    const-wide/16 v2, 0x3e8

    rem-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final declared-synchronized c(FFFIJJ)V
    .locals 10

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add magnetic field snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-wide v8, p0, Lhqu;->C:J

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p5

    move-wide/from16 v6, p7

    invoke-direct/range {v0 .. v9}, Lhqu;->a(FFFJJJ)Livi;

    move-result-object v0

    iput-wide p5, p0, Lhqu;->C:J

    iget-object v1, p0, Lhqu;->d:Livi;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Livi;->k(I)I

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lhqu;->u:I

    if-eq v1, p4, :cond_3

    :cond_2
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p4}, Livi;->e(II)Livi;

    iput p4, p0, Lhqu;->u:I

    :cond_3
    iget-object v1, p0, Lhqu;->d:Livi;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(FFFIJJ)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhqu;->a:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqu;->g:Limb;

    const-string v1, "Could not add gyroscope snapshot after the composer is closed."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, p5, p6, p7, p8}, Lhqu;->a(JJ)V

    new-instance v2, Livi;

    sget-object v0, Lihj;->aB:Livk;

    const/16 v1, 0x8

    invoke-direct {v2, v0, v1}, Livi;-><init>(Livk;I)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0, p1}, Livi;->a(IF)Livi;

    const/4 v0, 0x2

    invoke-virtual {v2, v0, p2}, Livi;->a(IF)Livi;

    const/4 v0, 0x3

    invoke-virtual {v2, v0, p3}, Livi;->a(IF)Livi;

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Livi;->a(IJ)Livi;

    :cond_2
    iget-wide v0, p0, Lhqu;->D:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_7

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v3

    invoke-virtual {p0, p5, p6}, Lhqu;->a(J)J

    move-result-wide v5

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->b(JJ)I

    move-result v1

    invoke-virtual {p0, v3, v4, v5, v6}, Lhqu;->c(JJ)I

    move-result v0

    :goto_1
    if-eqz v1, :cond_3

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v1}, Livi;->e(II)Livi;

    :cond_3
    if-eqz v0, :cond_4

    const/16 v1, 0x8

    invoke-virtual {v2, v1, v0}, Livi;->e(II)Livi;

    :cond_4
    iput-wide p5, p0, Lhqu;->D:J

    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lhqu;->v:I

    if-eq v0, p4, :cond_6

    :cond_5
    const/4 v0, 0x4

    invoke-virtual {v2, v0, p4}, Livi;->e(II)Livi;

    iput p4, p0, Lhqu;->v:I

    :cond_6
    iget-object v0, p0, Lhqu;->d:Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Livi;->a(ILivi;)V

    invoke-virtual {p0}, Lhqu;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_2
    iget-wide v0, p0, Lhqu;->D:J

    invoke-virtual {p0, v0, v1, p5, p6}, Lhqu;->b(JJ)I

    move-result v1

    iget-wide v3, p0, Lhqu;->D:J

    invoke-virtual {p0, v3, v4, p5, p6}, Lhqu;->c(JJ)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_1
.end method
