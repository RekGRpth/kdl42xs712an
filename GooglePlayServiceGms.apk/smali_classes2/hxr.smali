.class public final Lhxr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field public b:Ljava/util/HashMap;

.field c:Ljava/util/HashMap;

.field public d:Lhxk;


# direct methods
.method public constructor <init>(II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lhxr;->c:Ljava/util/HashMap;

    iput p1, p0, Lhxr;->a:I

    iget v0, p0, Lhxr;->a:I

    if-lez v0, :cond_0

    new-instance v0, Lhxx;

    invoke-direct {v0, p2}, Lhxx;-><init>(I)V

    :goto_0
    iput-object v0, p0, Lhxr;->d:Lhxk;

    return-void

    :cond_0
    new-instance v0, Lhxj;

    invoke-direct {v0}, Lhxj;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lhxt;
    .locals 2

    iget-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    if-nez v0, :cond_0

    new-instance v0, Lhxt;

    invoke-direct {v0, p1}, Lhxt;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public final a()V
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-virtual {v0}, Lhxt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lhxr;->c:Ljava/util/HashMap;

    return-void
.end method

.method final a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v1}, Lhxr;->b(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhxr;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Need to call backup() before calling this."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhxr;->c:Ljava/util/HashMap;

    iput-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v0, p2}, Lhxt;->a(Ljava/util/List;)Z

    move-result v1

    invoke-virtual {v0}, Lhxt;->a()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lhxr;->d:Lhxk;

    iget-object v2, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {p0}, Lhxr;->c()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lhxk;->a(Ljava/lang/Iterable;I)V

    :cond_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-virtual {v0}, Lhxt;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method
