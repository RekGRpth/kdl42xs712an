.class public final Lija;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    :goto_0
    const-string v1, "Caller must handle DEFINED itself"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    packed-switch p0, :pswitch_data_0

    const v0, 0x7f0b043c    # com.google.android.gms.R.string.location_settings_ulr_summary_unknown

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0b0438    # com.google.android.gms.R.string.location_settings_ulr_summary_loading

    goto :goto_1

    :pswitch_1
    const v0, 0x7f0b0439    # com.google.android.gms.R.string.location_settings_ulr_summary_offline

    goto :goto_1

    :pswitch_2
    const v0, 0x7f0b043a    # com.google.android.gms.R.string.location_settings_ulr_summary_error

    goto :goto_1

    :pswitch_3
    const v0, 0x7f0b043b    # com.google.android.gms.R.string.location_settings_ulr_summary_auth_error

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
