.class public final Lhoi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhod;
.implements Lidz;


# instance fields
.field private final a:Lidq;

.field private final b:Ljava/io/File;

.field private final c:Lidx;

.field private final d:Lhox;

.field private final e:Lhnp;


# direct methods
.method public constructor <init>(Lidq;Ljava/io/File;[B)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    invoke-static {v0}, Lilv;->a(Z)V

    iput-object p1, p0, Lhoi;->a:Lidq;

    new-instance v0, Ljava/io/File;

    const-string v1, "nlpstats"

    invoke-direct {v0, p2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lhoi;->b:Ljava/io/File;

    new-instance v0, Lidx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-object v5, Lihj;->aW:Livk;

    iget-object v6, p0, Lhoi;->b:Ljava/io/File;

    move-object v4, p3

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lidx;-><init>(ILjavax/crypto/SecretKey;I[BLivk;Ljava/io/File;Lidz;Lidq;)V

    iput-object v0, p0, Lhoi;->c:Lidx;

    new-instance v0, Lhox;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lhox;-><init>(I)V

    iput-object v0, p0, Lhoi;->d:Lhox;

    new-instance v0, Lhob;

    invoke-direct {v0}, Lhob;-><init>()V

    new-instance v1, Lhnp;

    invoke-direct {v1, v0}, Lhnp;-><init>(Lhny;)V

    iput-object v1, p0, Lhoi;->e:Lhnp;

    return-void
.end method

.method private b(Ljava/lang/String;J)Lhoa;
    .locals 2

    iget-object v1, p0, Lhoi;->d:Lhox;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0, p1}, Lhox;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lhuo;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoa;

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)I
    .locals 4

    iget-object v1, p0, Lhoi;->d:Lhox;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lhoi;->b(Ljava/lang/String;J)Lhoa;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lhoa;

    invoke-direct {v0}, Lhoa;-><init>()V

    new-instance v2, Lhuo;

    invoke-direct {v2, v0, p2, p3}, Lhuo;-><init>(Ljava/lang/Object;J)V

    iget-object v3, p0, Lhoi;->d:Lhox;

    invoke-virtual {v3, p1, v2}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0}, Lhoa;->a()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v0, v0, Lhoa;->a:Livi;

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v2}, Livi;->e(II)Livi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(J)V
    .locals 7

    invoke-virtual {p0, p1, p2}, Lhoi;->c(J)V

    new-instance v1, Livi;

    sget-object v0, Lihj;->aW:Livk;

    invoke-direct {v1, v0}, Livi;-><init>(Livk;)V

    iget-object v2, p0, Lhoi;->d:Lhox;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lhoi;->e:Lhnp;

    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0}, Lhox;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuo;

    iget-object v6, v3, Lhnp;->a:Lhny;

    invoke-interface {v6, v5, v0}, Lhny;->a(Ljava/lang/Object;Lhuo;)Livi;

    move-result-object v0

    iget v5, v3, Lhnp;->b:I

    invoke-virtual {v1, v5, v0}, Livi;->a(ILivi;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, Lhoi;->c:Lidx;

    monitor-enter v2

    :try_start_2
    iget-object v0, p0, Lhoi;->c:Lidx;

    invoke-virtual {v0, v1}, Lidx;->b(Livi;)V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0}, Lhox;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final a(Livi;)Z
    .locals 2

    sget-object v0, Lihj;->aW:Livk;

    invoke-virtual {p1}, Livi;->c()Livk;

    move-result-object v1

    invoke-virtual {v0, v1}, Livk;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/util/List;
    .locals 7

    const/4 v6, 0x3

    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0}, Lhox;->entrySet()Ljava/util/Set;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuo;

    iget-object v0, v0, Lhuo;->a:Ljava/lang/Object;

    check-cast v0, Lhoa;

    iget-object v0, v0, Lhoa;->a:Livi;

    invoke-virtual {v0}, Livi;->a()Livi;

    move-result-object v0

    new-instance v4, Livi;

    sget-object v5, Lihj;->I:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v6}, Livi;->e(II)Livi;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v1}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v4, v6, v0}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public final b(J)V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lhoi;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    iget-object v0, p0, Lhoi;->a:Lidq;

    iget-object v1, p0, Lhoi;->b:Ljava/io/File;

    invoke-interface {v0, v1}, Lidq;->a(Ljava/io/File;)V

    iget-object v1, p0, Lhoi;->c:Lidx;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, p0, Lhoi;->c:Lidx;

    invoke-virtual {v0}, Lidx;->a()Livi;

    move-result-object v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lhoi;->d:Lhox;

    monitor-enter v1
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    iget-object v3, p0, Lhoi;->e:Lhnp;

    iget-object v4, p0, Lhoi;->d:Lhox;

    iget v0, v3, Lhnp;->b:I

    invoke-virtual {v2, v0}, Livi;->k(I)I

    move-result v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    iget v6, v3, Lhnp;->b:I

    invoke-virtual {v2, v6, v0}, Livi;->c(II)Livi;

    move-result-object v6

    iget-object v7, v3, Lhnp;->a:Lhny;

    invoke-interface {v7, v6}, Lhny;->a(Livi;)Ljava/lang/Object;

    move-result-object v7

    iget-object v8, v3, Lhnp;->a:Lhny;

    invoke-interface {v8, v6}, Lhny;->b(Livi;)Lhuo;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lhox;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_0

    const-string v1, "NlpStats"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " loading cache from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lhoi;->c:Lidx;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    invoke-virtual {p0, p1, p2}, Lhoi;->c(J)V

    return-void

    :cond_1
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v1

    throw v0
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception v0

    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_2

    const-string v1, "NlpStats"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " loading cache from "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lhoi;->c:Lidx;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lhoi;->c()V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    iget-object v1, p0, Lhoi;->d:Lhox;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0}, Lhox;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lhoi;->c:Lidx;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lhoi;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(J)V
    .locals 4

    const-wide/32 v0, 0x240c8400

    sub-long v0, p1, v0

    iget-object v2, p0, Lhoi;->d:Lhox;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lhoi;->d:Lhox;

    invoke-virtual {v3, v0, v1, v0, v1}, Lhox;->a(JJ)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhoi;->d:Lhox;

    invoke-virtual {v0}, Lhox;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
