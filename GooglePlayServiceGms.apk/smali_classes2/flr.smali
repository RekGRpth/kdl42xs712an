.class public final Lflr;
.super Lflj;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lfly;

.field private final c:Lfmd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfly;Lfmd;)V
    .locals 0

    invoke-direct {p0}, Lflj;-><init>()V

    iput-object p1, p0, Lflr;->a:Landroid/content/Context;

    iput-object p2, p0, Lflr;->b:Lfly;

    iput-object p3, p0, Lflr;->c:Lfmd;

    return-void
.end method

.method private a(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    const-string v0, "PlayLogBrokerService"

    const-string v1, "--> failed to write"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlayLogBrokerService"

    const-string v1, "--> success, request regular upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lflr;->c:Lfmd;

    invoke-virtual {v0}, Lfmd;->f()V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PlayLogBrokerService"

    const-string v1, "--> success, request immediate upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lflr;->c:Lfmd;

    invoke-virtual {v0}, Lfmd;->e()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlayLogBrokerService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Writing log: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/google/android/gms/playlog/internal/LogEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PlayLogBrokerService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    context: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lflr;->b:Lfly;

    invoke-virtual {v0, p2, p3}, Lfly;->a(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)I

    move-result v0

    invoke-direct {p0, v0}, Lflr;->a(I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Ljava/util/List;)V
    .locals 2

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/playlog/internal/LogEvent;

    invoke-virtual {p0, p1, p2, v0}, Lflr;->a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;[B)V
    .locals 3

    invoke-static {}, Lcom/google/android/gms/playlog/service/PlayLogBrokerService;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlayLogBrokerService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Writing log: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "PlayLogBrokerService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    context: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lflr;->b:Lfly;

    invoke-virtual {v0, p2, p3}, Lfly;->a(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;[B)I

    move-result v0

    invoke-direct {p0, v0}, Lflr;->a(I)V

    return-void
.end method
