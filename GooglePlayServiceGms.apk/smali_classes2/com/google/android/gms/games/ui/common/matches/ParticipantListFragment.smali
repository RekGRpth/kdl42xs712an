.class public Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbbr;
.implements Lbbs;


# instance fields
.field private Y:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private a:[Lcom/google/android/gms/games/multiplayer/Participant;

.field private aa:Lo;

.field private ab:Leal;

.field private ac:Leaj;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/games/multiplayer/Participant;

.field private f:Lfaj;

.field private g:Leai;

.field private h:Ljava/util/HashMap;

.field private i:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)Leal;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;)[Lcom/google/android/gms/games/multiplayer/Participant;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    return-object v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lxc;->j:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:Lcom/google/android/gms/games/multiplayer/Participant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_2

    :cond_0
    const-string v0, "ParticipantListFrag"

    const-string v1, "no mManagedParticipant or data for manage circles operation."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    invoke-static {p3}, Lbex;->a(Landroid/content/Intent;)Lbey;

    move-result-object v3

    invoke-interface {v3}, Lbey;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Lbey;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_3
    move v0, v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b:Ljava/lang/String;

    invoke-static {v4, v5, v6, v2, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v3}, Lbey;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3}, Lbey;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lxf;->bd:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Leai;

    invoke-virtual {v0}, Leai;->notifyDataSetChanged()V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lbbo;)V
    .locals 3

    const-string v0, "ParticipantListFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PeopleClient connection failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Leai;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    instance-of v0, v0, Leal;

    const-string v2, "Parent activity did not implement ParticipantListMetaDataProvider"

    invoke-static {v0, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    instance-of v0, v0, Leaj;

    const-string v2, "Parent activity did not implement ParticipantListListener"

    invoke-static {v0, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    check-cast v0, Leal;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    check-cast v0, Leaj;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ac:Leaj;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->a()[Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->e()Landroid/net/Uri;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    invoke-interface {v0}, Leal;->f()Landroid/net/Uri;

    move-result-object v3

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    aget-object v4, v4, v0

    if-eqz v4, :cond_0

    invoke-interface {v4}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->h:Ljava/util/HashMap;

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->k()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    sget v4, Lxa;->L:I

    invoke-virtual {v0, v4}, Lo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->j()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lxf;->aJ:I

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    array-length v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    sget v4, Lxa;->K:I

    invoke-virtual {v0, v4}, Lo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    new-instance v0, Leai;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->a:[Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-direct {v0, p0, v1, v4}, Leai;-><init>(Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;Landroid/content/Context;[Lcom/google/android/gms/games/multiplayer/Participant;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Leai;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    sget v1, Lxa;->w:I

    invoke-virtual {v0, v1}, Lo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->g:Leai;

    invoke-static {v0, v1}, Leee;->a(Landroid/content/Context;Landroid/widget/Adapter;)D

    move-result-wide v0

    const-wide v6, 0x3ff0cccccccccccdL    # 1.05

    mul-double/2addr v0, v6

    double-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ab:Leal;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->i:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    sget v1, Lxa;->E:I

    invoke-virtual {v0, v1}, Lo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Y:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Y:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Y:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v1, Lwz;->e:I

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :cond_3
    new-instance v0, Lfaj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    const/16 v4, 0x76

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:Lfaj;

    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->Z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final g_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g_()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lxa;->R:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:Lfaj;

    iget-object v2, v2, Lfaj;->a:Lfch;

    invoke-virtual {v2}, Lfch;->d_()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->e:Lcom/google/android/gms/games/multiplayer/Participant;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->aa:Lo;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->f:Lfaj;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->b:Ljava/lang/String;

    new-instance v0, Ledu;

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Ledu;-><init>(Lcom/google/android/gms/games/Player;Ledv;Landroid/app/Activity;Lfaj;Ljava/lang/String;I)V

    invoke-virtual {v0}, Ledu;->a()V

    goto :goto_0

    :cond_2
    sget v0, Lxa;->at:I

    if-eq v2, v0, :cond_3

    sget v0, Lxa;->as:I

    if-ne v2, v0, :cond_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ParticipantListFragment;->ac:Leaj;

    goto :goto_0
.end method
