.class public final Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lebg;
.implements Ledl;


# instance fields
.field private ad:Ldwp;

.field private ae:Leav;

.field private af:Ldwp;

.field private ag:Leba;

.field private ah:Ldxi;

.field private ai:Lebg;

.field private aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private ak:Ljava/lang/String;

.field private al:Lcom/google/android/gms/games/Player;

.field private am:Ljava/util/ArrayList;

.field private an:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldxe;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    return-void
.end method

.method private V()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    invoke-virtual {v0}, Leba;->d()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ah:Ldxi;

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ldxi;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->j()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    if-le v3, v1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Ldwp;->a(Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3}, Ldxe;->a(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->al:Lcom/google/android/gms/games/Player;

    invoke-static {v0, p3}, Leav;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ae:Leav;

    invoke-virtual {v0, v1}, Leav;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ad:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lbdu;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    if-eqz v0, :cond_1

    new-instance v0, Lbhb;

    invoke-direct {v0}, Lbhb;-><init>()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    invoke-virtual {v1, v0}, Leba;->a(Lbgo;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->V()V

    return-void

    :cond_1
    new-instance v0, Lbhb;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Lbhb;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbhb;->b(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    invoke-interface {v1, p1}, Lebg;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->V()V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    invoke-virtual {v1}, Leba;->d()I

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    const/16 v2, 0x384

    invoke-virtual {v1, v2, v0}, Ldvn;->setResult(ILandroid/content/Intent;)V

    :cond_4
    return-void

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    if-eqz v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    invoke-interface {v0, p1}, Lebg;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    return-void
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 4

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/Player;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lxa;->aj:I

    if-ne v2, v3, :cond_0

    invoke-static {v1, v0}, Lbjt;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->a(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    invoke-interface {v0, p1}, Lebg;->b_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 12

    const/4 v8, 0x5

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    instance-of v0, v0, Leaz;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    check-cast v0, Leaz;

    invoke-interface {v0}, Leaz;->u()Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-interface {v0}, Leaz;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ak:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    instance-of v0, v0, Lebe;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    check-cast v0, Lebe;

    invoke-interface {v0}, Lebe;->v()Lebd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v0, Ldwp;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ad:Ldwp;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ad:Ldwp;

    sget v1, Lxf;->aS:I

    new-array v2, v10, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v3}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->p_()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldwp;->a(Ljava/lang/String;)V

    new-instance v0, Leav;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v2}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v2

    sget v3, Lxf;->aQ:I

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Leav;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/Player;ILandroid/view/View$OnClickListener;Ledl;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ae:Leav;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->aj:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->j()I

    move-result v0

    new-instance v1, Ldwp;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    invoke-direct {v1, v2}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    sget v1, Lxf;->aT:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    sget v1, Lxf;->aU:I

    const-string v2, "openAllButton"

    invoke-virtual {v0, p0, v1, v2}, Ldwp;->a(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    invoke-virtual {v0, v10}, Ldwp;->a(Z)V

    :goto_0
    new-instance v0, Leba;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    invoke-direct {v0, v1, p0}, Leba;-><init>(Ldvn;Lebg;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    new-instance v0, Ldxi;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    sget v2, Lwz;->B:I

    sget v3, Lxf;->aW:I

    move v4, v9

    move-object v5, v11

    move-object v6, v11

    move-object v7, v11

    invoke-direct/range {v0 .. v8}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ah:Ldxi;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ah:Ldxi;

    invoke-virtual {v0, v9}, Ldxi;->b(Z)V

    new-instance v0, Ldwu;

    new-array v1, v8, [Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ad:Ldwp;

    aput-object v2, v1, v9

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ae:Leav;

    aput-object v2, v1, v10

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ah:Ldxi;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    if-eqz p1, :cond_0

    const-string v0, "savedStateRemovedIdList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    const-string v0, "savedStateRemoveCluster"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    :cond_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    sget v1, Lxf;->aV:I

    invoke-virtual {v0, v1}, Ldwp;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->af:Ldwp;

    invoke-virtual {v0, v9}, Ldwp;->a(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxe;->e(Landroid/os/Bundle;)V

    const-string v0, "savedStateRemovedIdList"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->am:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "savedStateRemoveCluster"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->an:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    invoke-virtual {v0}, Leba;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    instance-of v0, v1, Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lxa;->az:I

    if-ne v2, v3, :cond_2

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Player;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->al:Lcom/google/android/gms/games/Player;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->Y:Ldvn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ak:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Leee;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Leee;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, v1, Ljava/lang/String;

    if-eqz v0, :cond_2

    check-cast v1, Ljava/lang/String;

    const-string v0, "openAllButton"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ag:Leba;

    invoke-virtual {v0}, Leba;->e()Lbgo;

    move-result-object v0

    invoke-static {v0}, Leay;->a(Lbgo;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/PublicRequestListFragment;->ai:Lebg;

    invoke-interface {v1, v0}, Lebg;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    goto :goto_0

    :cond_2
    const-string v0, "PublicReqListFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
