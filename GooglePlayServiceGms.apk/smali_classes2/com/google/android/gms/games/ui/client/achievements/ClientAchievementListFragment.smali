.class public final Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Lbel;


# instance fields
.field private ad:Ldzh;

.field private ae:Ldzj;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method


# virtual methods
.method public final R()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->g:Lcub;

    invoke-interface {v1, v0}, Lcub;->a(Lbdu;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Z:Leds;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientAchievementList"

    const-string v1, "onRetry(): client not connected yet..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lbdu;)V
    .locals 1

    sget-object v0, Lcte;->g:Lcub;

    invoke-interface {v0, p1}, Lcub;->a(Lbdu;)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    return-void
.end method

.method public final synthetic a(Lbek;)V
    .locals 6

    const/4 v0, 0x0

    check-cast p1, Lcuc;

    invoke-interface {p1}, Lcuc;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v2

    invoke-interface {p1}, Lcuc;->a()Lctz;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v1, "ClientAchievementList"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onAchievementsLoaded: got non-SUCCESS statusCode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", data = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v3}, Lctz;->b()V

    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Y:Ldvn;

    invoke-virtual {v1, v2}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v3}, Lctz;->b()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ae:Ldzj;

    invoke-virtual {v1, v3}, Ldzj;->a(Lbgo;)V

    invoke-virtual {v3}, Lctz;->a()I

    move-result v4

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, Lctz;->b(I)Lcty;

    move-result-object v5

    invoke-interface {v5}, Lcty;->f()I

    move-result v5

    if-nez v5, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ad:Ldzh;

    invoke-virtual {v1, v0, v4}, Ldzh;->a(II)V

    if-lez v4, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lctz;->b()V

    throw v0

    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Z:Leds;

    const v1, 0x7f0b0295    # com.google.android.gms.R.string.games_achievement_list_generic_error

    const v4, 0x7f0b0294    # com.google.android.gms.R.string.games_achievement_empty

    invoke-virtual {v0, v2, v1, v4}, Leds;->a(III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    new-instance v0, Ldzh;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldzh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ad:Ldzh;

    new-instance v0, Ldzj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldzj;-><init>(Ldvn;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ae:Ldzj;

    new-instance v0, Ldwu;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ad:Ldzh;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ae:Ldzj;

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/achievements/ClientAchievementListFragment;->ae:Ldzj;

    invoke-virtual {v0}, Ldzj;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method
