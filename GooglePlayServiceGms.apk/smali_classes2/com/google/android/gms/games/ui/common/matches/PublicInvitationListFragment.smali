.class public final Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Leab;
.implements Ledl;


# instance fields
.field private ad:Ldwp;

.field private ae:Leav;

.field private af:Lean;

.field private ag:Ldxi;

.field private ah:Leab;

.field private ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private aj:Ljava/lang/String;

.field private ak:Lcom/google/android/gms/games/Player;

.field private al:Ljava/util/ArrayList;

.field private am:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldxe;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    return-void
.end method

.method private V()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    invoke-virtual {v0}, Lean;->d()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ag:Ldxi;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ldxi;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    invoke-virtual {v1}, Lean;->d()I

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    const/16 v2, 0x384

    invoke-virtual {v1, v2, v0}, Ldvn;->setResult(ILandroid/content/Intent;)V

    return-void

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    if-eqz v1, :cond_3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3}, Ldxe;->a(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ak:Lcom/google/android/gms/games/Player;

    invoke-static {v0, p3}, Leav;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ae:Leav;

    invoke-virtual {v0, v1}, Leav;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ad:Ldwp;

    invoke-virtual {v0, v1}, Ldwp;->b(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lbdu;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    if-eqz v0, :cond_1

    new-instance v0, Lbhb;

    invoke-direct {v0}, Lbhb;-><init>()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    invoke-virtual {v1, v0}, Lean;->a(Lbgo;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->V()V

    return-void

    :cond_1
    new-instance v0, Lbhb;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Lbhb;-><init>(Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbhb;->b(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1}, Leab;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1, p2, p3}, Leab;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 4

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/games/Player;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lxa;->aj:I

    if-ne v2, v3, :cond_0

    invoke-static {v1, v0}, Lbjt;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->a(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1}, Leab;->a_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1}, Leab;->b(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1}, Leab;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->V()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    instance-of v0, v0, Leam;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    move-object v6, v0

    check-cast v6, Leam;

    invoke-interface {v6}, Leam;->u()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-interface {v6}, Leam;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->aj:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ai:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    const-string v1, "Must have a valid player to show cluster!"

    invoke-static {v0, v1}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    instance-of v0, v0, Ldzy;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    check-cast v0, Ldzy;

    invoke-interface {v0}, Ldzy;->D_()Ldzx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v0, Ldwp;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    invoke-direct {v0, v1}, Ldwp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ad:Ldwp;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ad:Ldwp;

    sget v1, Lxf;->aR:I

    new-array v3, v9, [Ljava/lang/Object;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldwp;->a(Ljava/lang/String;)V

    new-instance v0, Leav;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    sget v3, Lxf;->aP:I

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Leav;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/Player;ILandroid/view/View$OnClickListener;Ledl;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ae:Leav;

    new-instance v7, Ldwp;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    invoke-direct {v7, v0}, Ldwp;-><init>(Landroid/content/Context;)V

    sget v0, Lxf;->ay:I

    invoke-virtual {v7, v0}, Ldwp;->a(I)V

    new-instance v0, Lean;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    invoke-direct {v0, v1, p0}, Lean;-><init>(Ldvn;Leab;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    invoke-interface {v6}, Leam;->v()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lean;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ldxi;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    sget v2, Lwz;->C:I

    sget v3, Lxf;->W:I

    move v4, v8

    move-object v5, v10

    move-object v6, v10

    invoke-direct/range {v0 .. v6}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ag:Ldxi;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ag:Ldxi;

    invoke-virtual {v0, v8}, Ldxi;->b(Z)V

    new-instance v0, Ldwu;

    const/4 v1, 0x5

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ad:Ldwp;

    aput-object v2, v1, v8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ae:Leav;

    aput-object v2, v1, v9

    const/4 v2, 0x2

    aput-object v7, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ag:Ldxi;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    if-eqz p1, :cond_0

    const-string v0, "savedStateRemovedIdList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    const-string v0, "savedStateRemoveCluster"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ah:Leab;

    invoke-interface {v0, p1}, Leab;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->V()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxe;->e(Landroid/os/Bundle;)V

    const-string v0, "savedStateRemovedIdList"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->al:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "savedStateRemoveCluster"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->am:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->af:Lean;

    invoke-virtual {v0}, Lean;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v0, v1, Lcom/google/android/gms/games/Player;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v3, Lxa;->az:I

    if-ne v2, v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/Player;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->ak:Lcom/google/android/gms/games/Player;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->Y:Ldvn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationListFragment;->aj:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Leee;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Leee;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "PublicInvitFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; View: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
