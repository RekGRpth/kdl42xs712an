.class public Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
.super Lfqz;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfqz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const v0, 0x7f0200bf    # com.google.android.gms.R.drawable.default_avatar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    invoke-super {p0}, Lfqz;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Lfra;)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->a(Lfra;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final bridge synthetic a(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->a(Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic b(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->b(Z)V

    return-void
.end method

.method public final bridge synthetic c(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->c(Z)V

    return-void
.end method

.method public final bridge synthetic d(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->d(Z)V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final bridge synthetic e()Z
    .locals 1

    invoke-super {p0}, Lfqz;->e()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lfqz;->f()V

    return-void
.end method

.method public final bridge synthetic g()V
    .locals 0

    invoke-super {p0}, Lfqz;->g()V

    return-void
.end method

.method public final bridge synthetic h()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lfqz;->h()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic isChecked()Z
    .locals 1

    invoke-super {p0}, Lfqz;->isChecked()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfqz;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public bridge synthetic onClick(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lfqz;->onFinishInflate()V

    const v0, 0x7f0a0269    # com.google.android.gms.R.id.audience_selection_person_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a:Landroid/widget/TextView;

    const v0, 0x7f0a026b    # com.google.android.gms.R.id.audience_selection_secondary_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b:Landroid/widget/TextView;

    const v0, 0x7f0a026a    # com.google.android.gms.R.id.audience_selection_person_avatar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c:Landroid/widget/ImageView;

    return-void
.end method

.method public bridge synthetic setChecked(Z)V
    .locals 0

    invoke-super {p0, p1}, Lfqz;->setChecked(Z)V

    return-void
.end method

.method public bridge synthetic toggle()V
    .locals 0

    invoke-super {p0}, Lfqz;->toggle()V

    return-void
.end method
