.class public Lcom/google/android/gms/plus/location/MarkerIconView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:Lfxf;

.field private b:Landroid/graphics/Bitmap;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->a:Lfxf;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->a:Lfxf;

    iget-object v0, v2, Lfxf;->m:Lfxg;

    sget-object v1, Lfxg;->c:Lfxg;

    if-ne v0, v1, :cond_2

    sget-object v0, Lfxf;->k:Landroid/graphics/Paint;

    :goto_0
    sget v1, Lfxf;->g:I

    sget v3, Lfxf;->h:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-boolean v1, v2, Lfxf;->n:Z

    if-eqz v1, :cond_3

    sget-object v1, Lfxf;->b:Landroid/graphics/Bitmap;

    :goto_1
    sget v5, Lfxf;->e:I

    int-to-float v5, v5

    invoke-virtual {v4, v1, v9, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v1, v2, Lfxf;->o:Landroid/graphics/Bitmap;

    if-nez v1, :cond_4

    sget-object v1, Lfxf;->f:Landroid/graphics/Bitmap;

    :goto_2
    sget v5, Lfxf;->d:I

    sget v6, Lfxf;->d:I

    sget v7, Lfxf;->e:I

    add-int/2addr v6, v7

    int-to-float v7, v5

    int-to-float v8, v6

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v7, v2, Lfxf;->n:Z

    if-eqz v7, :cond_5

    iget-object v2, v2, Lfxf;->l:Landroid/content/Context;

    sget-object v7, Lfxf;->j:Landroid/graphics/Paint;

    invoke-static {v2, v1, v7}, Lbpc;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v4, v1, v9, v9, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_3
    neg-int v0, v5

    int-to-float v0, v0

    neg-int v1, v6

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iput-object v3, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->b:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->b:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->c:I

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lfxf;->j:Landroid/graphics/Paint;

    goto :goto_0

    :cond_3
    sget-object v1, Lfxf;->a:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_4
    iget-object v1, v2, Lfxf;->o:Landroid/graphics/Bitmap;

    sget v5, Lfxf;->c:I

    invoke-static {v1, v5}, Lbpc;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    :cond_5
    invoke-virtual {v4, v1, v9, v9, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/location/MarkerIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lfxf;->a(Landroid/content/Context;)V

    sget v1, Lfxf;->g:I

    invoke-static {v0}, Lfxf;->a(Landroid/content/Context;)V

    sget v2, Lfxf;->h:I

    invoke-static {v0}, Lfxf;->a(Landroid/content/Context;)V

    sget-object v0, Lfxf;->i:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    float-to-double v3, v0

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v3, v5

    int-to-double v5, v1

    mul-double/2addr v3, v5

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    mul-double/2addr v3, v5

    double-to-int v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    add-int/2addr v1, v3

    if-gez v0, :cond_0

    neg-int v0, v0

    :goto_0
    iput v0, p0, Lcom/google/android/gms/plus/location/MarkerIconView;->c:I

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/plus/location/MarkerIconView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
