.class public Lcom/google/android/gms/plus/location/DeviceLocationSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/Boolean;

.field public final b:[Lcom/google/android/gms/common/people/data/AudienceMember;

.field public final c:[Lcom/google/android/gms/common/people/data/AudienceMember;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfxc;

    invoke-direct {v0}, Lfxc;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->a:Ljava/lang/Boolean;

    const-class v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->b:[Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/people/data/AudienceMember;

    iput-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->c:[Lcom/google/android/gms/common/people/data/AudienceMember;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/location/DeviceLocationSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->b:[Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/location/DeviceLocationSettings;->c:[Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
