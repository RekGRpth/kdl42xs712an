.class public final Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "SourceFile"


# instance fields
.field private a:Lgor;

.field private b:Lfrb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    iget-object v1, v1, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, p2}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    new-instance v3, Lbkw;

    invoke-direct {v3, v1}, Lbkw;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v3, v2}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v1

    invoke-virtual {v1}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-static {v0, p2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/ArrayList;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    iget-object v4, v4, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v4, v0}, Lblc;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v3, v0, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/ArrayList;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private c()Ljava/util/ArrayList;
    .locals 14

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v6

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-interface {v5, v3, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/plus/sharebox/MentionSpan;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    array-length v9, v0

    move v4, v3

    :goto_0
    if-ge v4, v9, :cond_3

    aget-object v1, v0, v4

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    aget-object v1, v0, v4

    invoke-interface {v5, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    aget-object v11, v0, v4

    invoke-interface {v5, v11}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-static {v6, v11}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-interface {v5, v1, v11}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "+"

    invoke-virtual {v1, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    const/4 v12, 0x0

    invoke-static {v10, v1, v12}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "checkboxEnabled"

    if-nez v11, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v12, v13, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    return-object v7
.end method


# virtual methods
.method public final a()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Lak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfrb;)V
    .locals 9

    const/4 v8, 0x0

    iput-object p6, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    new-instance v0, Lgor;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b:Lfrb;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lgor;-><init>(Landroid/content/Context;Lak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfrb;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lgox;

    invoke-direct {v0}, Lgox;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setThreshold(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    invoke-virtual {v0}, Lgor;->j()V

    new-instance v0, Lgow;

    invoke-direct {v0, p0, v8}, Lgow;-><init>(Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 10

    const/4 v2, 0x0

    const v9, 0x44098000    # 550.0f

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getInputType()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v0, Lbqb;->a:Lbqb;

    if-eqz v0, :cond_2

    sget-object v0, Lbqb;->a:Lbqb;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v1, :cond_0

    iget v0, v0, Lbqb;->b:I

    if-eq v0, v1, :cond_0

    if-nez p1, :cond_6

    :cond_0
    const v0, -0x10001

    and-int/2addr v0, v3

    :goto_1
    if-eq v3, v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setRawInputType(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbpo;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    const-string v0, "window"

    invoke-virtual {v4, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v6, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eqz v6, :cond_3

    if-eqz v7, :cond_3

    iget v0, v5, Landroid/util/DisplayMetrics;->density:F

    const/4 v8, 0x0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    :goto_2
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d01e3    # com.google.android.gms.R.dimen.common_screen_metrics_margin_percentage

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    float-to-int v4, v2

    new-instance v2, Lbqb;

    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-direct {v2, v5, v6, v0, v4}, Lbqb;-><init>(IIII)V

    sput-object v2, Lbqb;->a:Lbqb;

    move-object v0, v2

    goto :goto_0

    :cond_4
    int-to-float v0, v6

    iget v8, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v8

    int-to-float v8, v7

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float v5, v8, v5

    cmpl-float v0, v0, v9

    if-ltz v0, :cond_5

    cmpl-float v0, v5, v9

    if-ltz v0, :cond_5

    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    const/high16 v0, 0x10000

    or-int/2addr v0, v3

    goto :goto_1
.end method

.method public final b()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    goto :goto_0
.end method

.method protected final convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 5

    instance-of v0, p1, Lfee;

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    check-cast p1, Lfee;

    :try_start_0
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lfee;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lfee;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v2, v1}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Spannable;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v2, v1, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "ShareBox"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "ShareBox"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to convert +mention selection to String: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    invoke-virtual {v0}, Lgor;->j()V

    :cond_0
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a:Lgor;

    invoke-virtual {v0}, Lgor;->k()V

    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 9

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a(Landroid/text/style/URLSpan;)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-direct {v5, v4}, Lcom/google/android/gms/plus/sharebox/MentionSpan;-><init>(Landroid/text/style/URLSpan;)V

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {v2, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v2, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    invoke-interface {v2, v5, v6, v7, v8}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final replaceText(Ljava/lang/CharSequence;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v0

    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    return-void
.end method
