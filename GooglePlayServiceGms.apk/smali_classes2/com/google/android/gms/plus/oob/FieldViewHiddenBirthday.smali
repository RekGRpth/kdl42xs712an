.class public final Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;
.super Lfye;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    const v0, 0x7f0400e5    # com.google.android.gms.R.layout.plus_oob_field_hidden_birthday

    return v0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0b0331    # com.google.android.gms.R.string.plus_oob_field_view_tag_hidden_birthday

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lgfm;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lgfm;->v()Lgft;

    move-result-object v1

    invoke-interface {v1}, Lgft;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lgfm;
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lfsr;->P:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->i()Lgfn;

    move-result-object v1

    new-instance v2, Lgfu;

    invoke-direct {v2}, Lgfu;-><init>()V

    iput-object v0, v2, Lgfu;->b:Ljava/lang/String;

    iget-object v0, v2, Lgfu;->f:Ljava/util/Set;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lgfu;->a()Lgft;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Lfye;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->a:Ljava/lang/String;

    return-object v1
.end method
