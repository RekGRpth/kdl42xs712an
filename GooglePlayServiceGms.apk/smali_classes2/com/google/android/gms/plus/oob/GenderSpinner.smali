.class public Lcom/google/android/gms/plus/oob/GenderSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"


# instance fields
.field private a:Lfyr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final a(Lfyr;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lfyr;

    return-void
.end method

.method public setSelection(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lfyr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lfyr;

    invoke-interface {v0, p1}, Lfyr;->b(I)V

    :cond_0
    return-void
.end method
