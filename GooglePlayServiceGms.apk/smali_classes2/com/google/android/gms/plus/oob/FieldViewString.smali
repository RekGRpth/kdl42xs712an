.class public final Lcom/google/android/gms/plus/oob/FieldViewString;
.super Lfye;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfye;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400ec    # com.google.android.gms.R.layout.plus_oob_field_string_setup_wizard

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0400eb    # com.google.android.gms.R.layout.plus_oob_field_string

    goto :goto_0
.end method

.method public final a(Lgfm;Lfyf;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lfye;->a(Lgfm;Lfyf;)V

    const v0, 0x7f0b0335    # com.google.android.gms.R.string.plus_oob_field_view_tag_string_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b0334    # com.google.android.gms.R.string.plus_oob_field_view_tag_string

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewString;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->n()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    new-instance v1, Lfyp;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lfyp;-><init>(Lcom/google/android/gms/plus/oob/FieldViewString;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->b:Lgfm;

    invoke-interface {v0}, Lgfm;->v()Lgft;

    move-result-object v0

    invoke-interface {v0}, Lgft;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lgfm;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewString;->i()Lgfn;

    move-result-object v0

    new-instance v1, Lgfu;

    invoke-direct {v1}, Lgfu;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lgfu;->e:Ljava/lang/String;

    iget-object v2, v1, Lgfu;->f:Ljava/util/Set;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lgfu;->a()Lgft;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgfn;->a(Lgft;)Lgfn;

    move-result-object v0

    invoke-virtual {v0}, Lgfn;->a()Lgfm;

    move-result-object v0

    return-object v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lfye;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Lfye;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewString;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/oob/FieldViewString$SavedState;->a:Ljava/lang/String;

    return-object v1
.end method
