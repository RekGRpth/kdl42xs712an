.class public Lcom/google/android/gms/drive/data/view/CustomListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# instance fields
.field private a:Lcal;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final a(Lcal;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    return-void
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    invoke-virtual {v1}, Lcal;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected layoutChildren()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/CustomListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    instance-of v2, v0, Lccz;

    if-eqz v2, :cond_0

    check-cast v0, Lccz;

    invoke-interface {v0}, Lccz;->a()Ljava/lang/Integer;

    move-result-object v0

    move-object v1, v0

    :cond_0
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/CustomListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Lccz;

    if-eqz v2, :cond_1

    check-cast v0, Lccz;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lccz;->a(I)V

    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    invoke-virtual {v0, p1}, Lcal;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/view/CustomListView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/view/CustomListView;->a:Lcal;

    invoke-virtual {v0, p1}, Lcal;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
