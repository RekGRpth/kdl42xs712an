.class public Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbtj;

    invoke-direct {v0}, Lbtj;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final a(Lcfz;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lbtl;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load account for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbtl;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lbzw;->a(Lcfc;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-void
.end method

.method public final b(Lcfz;)Lbvi;
    .locals 1

    sget-object v0, Lbvi;->a:Lbvi;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "AccountCriterion {accountName=%s}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
