.class public Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/SqlWhereClause;

.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbza;

    invoke-direct {v0}, Lbza;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-gez v0, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    iput-object p2, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b:Ljava/lang/String;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p3, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "DocListQuery[whereClause=%s, sortingClause=%s, columns=%s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method
