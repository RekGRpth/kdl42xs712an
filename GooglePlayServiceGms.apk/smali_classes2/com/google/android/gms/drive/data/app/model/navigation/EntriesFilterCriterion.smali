.class public Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Lbzk;

.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private e:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbto;

    invoke-direct {v0}, Lbto;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbzk;->valueOf(Ljava/lang/String;)Lbzk;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzk;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {p1}, Landroid/os/Parcel;->createBooleanArray()[Z

    move-result-object v0

    const/4 v1, 0x0

    aget-boolean v1, v0, v1

    iput-boolean v1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    const/4 v1, 0x1

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lbzk;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzk;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->e:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->e:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final a(Lcfz;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {v0}, Lbzk;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->e:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-void
.end method

.method public final b(Lcfz;)Lbvi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {v0}, Lbzk;->a()Lbvi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    return v0
.end method

.method public final c()Lbzk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    iget-object v3, p1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {v2, v3}, Lbzk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v0, "EntriesFilterCriterion {accountName=%s, filter=%s, isInheritable=%s, isMainFilter=%s}"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {v3}, Lbzk;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->a:Lbzk;

    invoke-virtual {v0}, Lbzk;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Z

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->b:Z

    aput-boolean v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;->d:Z

    aput-boolean v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    return-void
.end method
