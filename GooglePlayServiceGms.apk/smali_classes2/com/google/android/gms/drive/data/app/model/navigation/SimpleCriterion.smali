.class public final Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lbvi;

.field private final d:Lcom/google/android/gms/drive/database/SqlWhereClause;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    sget-object v0, Lceg;->o:Lceg;

    invoke-virtual {v0}, Lceg;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    const-string v2, "notInTrash"

    new-instance v3, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    const-string v4, "notInTrash"

    sget-object v5, Lbvi;->a:Lbvi;

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;-><init>(Ljava/lang/String;Lbvi;Lcom/google/android/gms/drive/database/SqlWhereClause;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lbtt;

    invoke-direct {v0}, Lbtt;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lbvi;Lcom/google/android/gms/drive/database/SqlWhereClause;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->c:Lbvi;

    iput-object p3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->d:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;
    .locals 1

    sget-object v0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->d:Lcom/google/android/gms/drive/database/SqlWhereClause;

    return-object v0
.end method

.method public final a(Lcfz;)V
    .locals 0

    return-void
.end method

.method public final b(Lcfz;)Lbvi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->c:Lbvi;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "SimpleCriterion {kind = \"%s\"}"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
