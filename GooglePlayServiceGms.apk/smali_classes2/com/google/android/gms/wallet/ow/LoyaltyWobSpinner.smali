.class public Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private a:Lhbf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final a(Lhbf;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a:Lhbf;

    return-void
.end method

.method public final a(Ljai;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lhbe;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lhbe;->a(Ljai;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public final a([Ljai;)V
    .locals 2

    invoke-virtual {p0, p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Lhbe;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lhbe;-><init>(Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;Landroid/content/Context;[Ljai;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a:Lhbf;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a:Lhbf;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljai;

    invoke-interface {v1, v0}, Lhbf;->a(Ljai;)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a:Lhbf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->a:Lhbf;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhbf;->a(Ljai;)V

    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->isEnabled()Z

    move-result v0

    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lhbe;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhbe;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
