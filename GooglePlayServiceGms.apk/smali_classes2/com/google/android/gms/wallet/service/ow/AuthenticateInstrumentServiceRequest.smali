.class public Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private final b:Ljava/lang/String;

.field private c:Ljao;

.field private d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lheh;

    invoke-direct {v0}, Lheh;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;Ljao;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Ljao;

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;Ljava/lang/String;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;Ljava/lang/String;[BB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;-><init>(Landroid/accounts/Account;Ljava/lang/String;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljao;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Ljao;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    const-class v1, Ljao;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljao;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Ljao;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Ljao;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->a:Landroid/accounts/Account;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c:Ljao;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->d:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
