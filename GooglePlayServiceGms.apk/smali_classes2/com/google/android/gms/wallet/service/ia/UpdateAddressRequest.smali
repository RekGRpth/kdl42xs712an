.class public Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lipo;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhdz;

    invoke-direct {v0}, Lhdz;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lipo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b:Lipo;

    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c:[B

    return-void
.end method

.method public synthetic constructor <init>(Landroid/accounts/Account;[BB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;-><init>(Landroid/accounts/Account;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lipo;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b:Lipo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c:[B

    const-class v1, Lipo;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipo;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b:Lipo;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b:Lipo;

    return-object v0
.end method

.method public final c()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->b:Lipo;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c:[B

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a:Landroid/accounts/Account;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->c()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
