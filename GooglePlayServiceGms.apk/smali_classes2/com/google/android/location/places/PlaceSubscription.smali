.class public final Lcom/google/android/location/places/PlaceSubscription;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Ligo;


# instance fields
.field public final a:Lcom/google/android/gms/location/places/PlaceRequest;

.field public final b:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field public final c:Landroid/app/PendingIntent;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ligo;

    invoke-direct {v0}, Ligo;-><init>()V

    sput-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Ligo;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/places/PlaceSubscription;->d:I

    iput-object p2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    iput-object p3, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iput-object p4, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/location/places/PlaceSubscription;-><init>(ILcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;
    .locals 1

    new-instance v0, Lcom/google/android/location/places/PlaceSubscription;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/location/places/PlaceSubscription;-><init>(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/places/PlaceSubscription;->d:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Ligo;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/places/PlaceSubscription;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/places/PlaceSubscription;

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/places/PlaceRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/location/places/internal/PlacesParams;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    iget-object v3, p1, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v2, v3}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    invoke-static {p0}, Lbkj;->a(Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "request"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->a:Lcom/google/android/gms/location/places/PlaceRequest;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "params"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->b:Lcom/google/android/gms/location/places/internal/PlacesParams;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    const-string v1, "callbackIntent"

    iget-object v2, p0, Lcom/google/android/location/places/PlaceSubscription;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Lbkk;->a(Ljava/lang/String;Ljava/lang/Object;)Lbkk;

    move-result-object v0

    invoke-virtual {v0}, Lbkk;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/location/places/PlaceSubscription;->CREATOR:Ligo;

    invoke-static {p0, p1, p2}, Ligo;->a(Lcom/google/android/location/places/PlaceSubscription;Landroid/os/Parcel;I)V

    return-void
.end method
