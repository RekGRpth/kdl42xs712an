.class public Lcom/google/android/location/os/real/SdkSpecific18;
.super Lcom/google/android/location/os/real/SdkSpecific17;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/location/os/real/SdkSpecific17;-><init>()V

    return-void
.end method

.method private static a(JLandroid/telephony/CellInfoGsm;Ljava/util/Collection;)Lhtf;
    .locals 18

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p3, :cond_2

    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v16

    if-eqz v0, :cond_4

    new-instance v15, Ljava/util/HashSet;

    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v15, v0}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_1
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v4

    const v0, 0x7fffffff

    if-eq v4, v0, :cond_1

    const/4 v0, -0x1

    if-eq v4, v0, :cond_1

    new-instance v0, Lhtk;

    const/4 v3, 0x1

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v7

    const v8, 0x7fffffff

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v9

    sget-object v10, Lhtf;->a:Ljava/util/Collection;

    const/4 v11, -0x1

    const/4 v12, -0x1

    const v13, 0x7fffffff

    const v14, 0x7fffffff

    move-wide/from16 v1, p0

    invoke-direct/range {v0 .. v14}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    invoke-interface {v15, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v10, v15

    :goto_3
    new-instance v0, Lhtk;

    const/4 v3, 0x1

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v4

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v7

    const v8, 0x7fffffff

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v9

    const/4 v11, -0x1

    const/4 v12, -0x1

    const v13, 0x7fffffff

    const v14, 0x7fffffff

    move-wide/from16 v1, p0

    invoke-direct/range {v0 .. v14}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    goto/16 :goto_0

    :cond_4
    sget-object v10, Lhtf;->a:Ljava/util/Collection;

    goto :goto_3
.end method

.method private static a(JLandroid/telephony/CellInfoWcdma;Ljava/util/List;)Lhtf;
    .locals 18

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p3, :cond_2

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v16

    if-eqz v0, :cond_4

    new-instance v15, Ljava/util/HashSet;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v15, v0}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_1
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v8

    const v0, 0x7fffffff

    if-eq v8, v0, :cond_1

    const/4 v0, -0x1

    if-eq v8, v0, :cond_1

    new-instance v0, Lhtk;

    const/4 v3, 0x3

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v4

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v7

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v9

    sget-object v10, Lhtf;->a:Ljava/util/Collection;

    const/4 v11, -0x1

    const/4 v12, -0x1

    const v13, 0x7fffffff

    const v14, 0x7fffffff

    move-wide/from16 v1, p0

    invoke-direct/range {v0 .. v14}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    invoke-interface {v15, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v10, v15

    :goto_3
    new-instance v0, Lhtk;

    const/4 v3, 0x3

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v4

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v7

    invoke-virtual/range {v16 .. v16}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v8

    invoke-virtual/range {p2 .. p2}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v9

    const/4 v11, -0x1

    const/4 v12, -0x1

    const v13, 0x7fffffff

    const v14, 0x7fffffff

    move-wide/from16 v1, p0

    invoke-direct/range {v0 .. v14}, Lhtk;-><init>(JIIIIIIILjava/util/Collection;IIII)V

    goto/16 :goto_0

    :cond_4
    sget-object v10, Lhtf;->a:Ljava/util/Collection;

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/hardware/SensorManager;Lidu;)Lhln;
    .locals 1

    new-instance v0, Lhlf;

    invoke-direct {v0, p1, p2}, Lhlf;-><init>(Landroid/hardware/SensorManager;Lidu;)V

    return-object v0
.end method

.method public final a(Landroid/telephony/TelephonyManager;IJ)[Lhtf;
    .locals 15

    invoke-static/range {p1 .. p4}, Lifl;->a(Landroid/telephony/TelephonyManager;IJ)Lhtf;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v2

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_0

    const-string v3, "SdkSpecific18"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cellLocation "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "SdkSpecific18"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "NetworkOperator: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v2, :cond_3

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_2

    const-string v3, "SdkSpecific18"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cellLocation is class "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_3

    const-string v2, "SdkSpecific18"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getNetworkType returns "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_4

    const-string v2, "SdkSpecific18"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cellInfo "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    if-nez v10, :cond_6

    if-eqz v9, :cond_5

    const/4 v2, 0x1

    new-array v2, v2, [Lhtf;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    :goto_0
    return-object v2

    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    :cond_6
    const/4 v7, 0x0

    sget-object v6, Lhtf;->a:Ljava/util/Collection;

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move-object v8, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    move-object v4, v2

    :cond_7
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/CellInfo;

    instance-of v12, v2, Landroid/telephony/CellInfoLte;

    if-eqz v12, :cond_c

    check-cast v2, Landroid/telephony/CellInfoLte;

    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v12

    if-eqz v12, :cond_b

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v13

    const v14, 0x7fffffff

    if-eq v13, v14, :cond_b

    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->isRegistered()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v13

    const v14, 0x7fffffff

    if-eq v13, v14, :cond_8

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getPci()I

    move-result v13

    const v14, 0x7fffffff

    if-eq v13, v14, :cond_8

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v13

    const v14, 0x7fffffff

    if-eq v13, v14, :cond_8

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v13

    const v14, 0x7fffffff

    if-eq v13, v14, :cond_8

    invoke-virtual {v12}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v12

    const v13, 0x7fffffff

    if-eq v12, v13, :cond_8

    move-object v8, v2

    goto :goto_1

    :cond_8
    sget-boolean v12, Licj;->b:Z

    if-eqz v12, :cond_7

    const-string v12, "SdkSpecific18"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Rejecting LTE primary cell "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    sget-object v12, Lhtf;->a:Ljava/util/Collection;

    if-ne v7, v12, :cond_a

    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    invoke-direct {v7, v12}, Ljava/util/ArrayList;-><init>(I)V

    :cond_a
    sget-object v12, Lhtf;->a:Ljava/util/Collection;

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/location/os/real/SdkSpecific18;->a(JLandroid/telephony/CellInfoLte;Ljava/util/Collection;)Lhtz;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_b
    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_7

    const-string v2, "SdkSpecific18"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Rejecting CellIdentity of "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v2, v12}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    instance-of v12, v2, Landroid/telephony/CellInfoWcdma;

    if-eqz v12, :cond_f

    check-cast v2, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v2}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    invoke-virtual {v2}, Landroid/telephony/CellInfoWcdma;->isRegistered()Z

    move-result v12

    if-eqz v12, :cond_d

    move-object v6, v2

    goto/16 :goto_1

    :cond_d
    if-nez v5, :cond_e

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    invoke-direct {v5, v12}, Ljava/util/ArrayList;-><init>(I)V

    :cond_e
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_f
    instance-of v12, v2, Landroid/telephony/CellInfoGsm;

    if-eqz v12, :cond_12

    check-cast v2, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v2}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    invoke-virtual {v2}, Landroid/telephony/CellInfoGsm;->isRegistered()Z

    move-result v12

    if-eqz v12, :cond_10

    move-object v4, v2

    goto/16 :goto_1

    :cond_10
    if-nez v3, :cond_11

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    invoke-direct {v3, v12}, Ljava/util/ArrayList;-><init>(I)V

    :cond_11
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_12
    sget-boolean v12, Licj;->b:Z

    if-eqz v12, :cond_7

    const-string v12, "SdkSpecific18"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Ignoring non-LTE cellInfo: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_13
    if-eqz v4, :cond_15

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v4, v3}, Lcom/google/android/location/os/real/SdkSpecific18;->a(JLandroid/telephony/CellInfoGsm;Ljava/util/Collection;)Lhtf;

    move-result-object v2

    :goto_2
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_14

    const-string v3, "SdkSpecific18"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "new API CellState is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_14
    if-nez v9, :cond_19

    if-nez v2, :cond_18

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_15
    if-eqz v6, :cond_16

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v6, v5}, Lcom/google/android/location/os/real/SdkSpecific18;->a(JLandroid/telephony/CellInfoWcdma;Ljava/util/List;)Lhtf;

    move-result-object v2

    goto :goto_2

    :cond_16
    if-eqz v8, :cond_17

    move-wide/from16 v0, p3

    invoke-static {v0, v1, v8, v7}, Lcom/google/android/location/os/real/SdkSpecific18;->a(JLandroid/telephony/CellInfoLte;Ljava/util/Collection;)Lhtz;

    move-result-object v2

    goto :goto_2

    :cond_17
    const/4 v2, 0x0

    goto :goto_2

    :cond_18
    const/4 v3, 0x1

    new-array v3, v3, [Lhtf;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    move-object v2, v3

    goto/16 :goto_0

    :cond_19
    if-nez v2, :cond_1b

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_1a

    const-string v2, "SdkSpecific18"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "returning old-api singleton, cellState is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a
    const/4 v2, 0x1

    new-array v2, v2, [Lhtf;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {v9}, Lhtf;->i()Z

    move-result v3

    if-eqz v3, :cond_1c

    invoke-virtual {v2, v9}, Lhtf;->b(Lhtf;)Z

    move-result v3

    if-eqz v3, :cond_1d

    :cond_1c
    const/4 v3, 0x1

    new-array v3, v3, [Lhtf;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    move-object v2, v3

    goto/16 :goto_0

    :cond_1d
    const/4 v3, 0x2

    new-array v3, v3, [Lhtf;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v2, 0x1

    aput-object v9, v3, v2

    move-object v2, v3

    goto/16 :goto_0
.end method
