.class public final Lcom/google/android/location/collectionlib/RealCollectorConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lhqk;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field static a:Z

.field public static final b:Ljava/util/Map;

.field public static final c:Ljava/util/Map;


# instance fields
.field final d:Ljava/lang/String;

.field final e:Ljava/util/Map;

.field private final f:Ljava/util/Set;

.field private final g:Lhql;

.field private final h:Ljava/lang/String;

.field private final i:[B

.field private final j:J

.field private final k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Z

.field private final o:Z

.field private volatile p:Z

.field private q:Lilx;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->c:Ljava/util/Map;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->c:Lhrz;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->d:Lhrz;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->e:Lhrz;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->f:Lhrz;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->i:Lhrz;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    sget-object v1, Lhrz;->j:Lhrz;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    sget-object v2, Lcom/google/android/location/collectionlib/RealCollectorConfig;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Lhrn;

    invoke-direct {v0}, Lhrn;-><init>()V

    sput-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    iput-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    iput-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lhrz;->a(I)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhql;->valueOf(Ljava/lang/String;)Lhql;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/collectionlib/SensorScannerConfig;

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    if-eqz v0, :cond_4

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    invoke-static {v0, v1}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a(Landroid/os/Bundle;Ljava/util/Map;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->m:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    :goto_4
    iput-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, Lilx;->a(Landroid/os/Parcelable;)Lilx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    return-void

    :cond_0
    iput-object v5, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    goto :goto_1

    :cond_1
    move v0, v4

    goto :goto_2

    :cond_2
    move v0, v4

    goto :goto_3

    :cond_3
    move v3, v4

    goto :goto_4

    :cond_4
    move-wide v0, v1

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Lilx;)V
    .locals 10

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Ljava/lang/String;[BZLilx;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Ljava/lang/String;[BZLilx;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    if-eqz p4, :cond_0

    const-wide/16 p2, 0x0

    :cond_0
    sget-object v0, Lhql;->b:Lhql;

    if-ne p5, v0, :cond_1

    const-string v0, "dataPath could not be null if you want to write data to local storage"

    invoke-static {p6, v0}, Lhsn;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget-object v0, Lhql;->a:Lhql;

    if-ne p5, v0, :cond_7

    if-nez p4, :cond_5

    move-wide v0, p2

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_6

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Invalid scan duration for MEMORY collection destination."

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    :goto_2
    if-eqz p4, :cond_2

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    sget-object v0, Lhrz;->d:Lhrz;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_3
    const-string v1, "sensorConfig supports acclerometer scanner only."

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    :cond_2
    invoke-static {p6}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-static {v0}, Lhsn;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p7, :cond_a

    array-length v0, p7

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    :cond_3
    const/4 v0, 0x1

    :goto_4
    const-string v1, "You must specify a valid key for encryption when writing data to persistent storage."

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a:Z

    if-nez v0, :cond_4

    if-nez p7, :cond_4

    new-instance v0, Ljava/io/File;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/data/data"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "The key should be in the /data/data partition."

    invoke-static {v1, v2}, Lhsn;->b(ZLjava/lang/Object;)V

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_5
    const-string v1, "%s does not exist."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhsn;->b(ZLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    iput-object p1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    iput-wide p2, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    iput-object p4, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    iput-object p5, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    iput-object p6, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    iput-object p9, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    return-void

    :cond_5
    invoke-virtual {p4}, Lcom/google/android/location/collectionlib/SensorScannerConfig;->c()J

    move-result-wide v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    const-string v1, "Scan duration should be >= 0"

    invoke-static {v0, v1}, Lhsn;->a(ZLjava/lang/Object;)V

    goto/16 :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :cond_b
    const/4 v0, 0x0

    goto :goto_5

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to parse the key path."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Landroid/os/Bundle;Ljava/util/Map;)V
    .locals 3

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lhrz;->b(I)Lhrz;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 5

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v0, 0x80

    new-array v0, v0, [B

    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lilx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    return-object v0
.end method

.method public final a(Lhrz;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    return-object v0
.end method

.method public final d()Lhql;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    return-wide v0
.end method

.method public final g()Lcom/google/android/location/collectionlib/SensorScannerConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    return-object v0
.end method

.method public final h()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    return-object v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    return v0
.end method

.method public final k()Lhqk;
    .locals 10

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;

    iget-object v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    iget-wide v2, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    iget-object v4, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    iget-object v5, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    iget-object v6, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->a(Ljava/lang/String;)[B

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    iget-object v9, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Ljava/lang/String;[BZLilx;)V

    iget v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    iput v1, v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    iget-object v1, v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->m:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->m:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    iput-boolean v1, v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    iget-boolean v1, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    iput-boolean v1, v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method public final l()Ljava/util/Map;
    .locals 5

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrz;

    sget-object v1, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    :cond_2
    return-object v2
.end method

.method public final m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "Scanner types: %s; Dest: %s; ScanDuration: %d, SensorConfig: %s, SensorDelay: %s; Data path: %s; Key path: %s; View opted out WIFI APs: %s; AutomaticShutDown: %s; ForceUpload: %s"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l()Ljava/util/Map;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->f:Ljava/util/Set;

    invoke-static {v0}, Lhrz;->a(Ljava/util/Set;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->g:Lhql;

    invoke-virtual {v0}, Lhql;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->k:Lcom/google/android/location/collectionlib/SensorScannerConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->e:Ljava/util/Map;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhrz;

    invoke-virtual {v1}, Lhrz;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->n:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->o:Z

    if-eqz v0, :cond_4

    move v0, v3

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->p:Z

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->q:Lilx;

    invoke-virtual {v0}, Lilx;->b()Landroid/os/Parcelable;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->i:[B

    array-length v0, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v3, v2

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method
