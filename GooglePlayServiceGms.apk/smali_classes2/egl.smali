.class public final Legl;
.super Lexk;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "network_quality"

    sput-object v0, Legl;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V
    .locals 0

    invoke-direct {p0}, Lexk;-><init>()V

    iput-object p1, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)Litn;
    .locals 12

    const-wide v10, 0x412e848000000000L    # 1000000.0

    const/16 v9, 0x12

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v5, Litn;

    invoke-direct {v5}, Litn;-><init>()V

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    return-object v3

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v9, :cond_8

    move-object v1, v3

    :cond_2
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v9, :cond_d

    :cond_3
    :goto_3
    if-eqz v4, :cond_0

    :cond_4
    if-eqz p1, :cond_25

    iput-object p1, v5, Litn;->a:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0, p1}, Lbox;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    iput-object v0, v5, Litn;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    const-string v0, "latency_micros"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "latency_micros"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v5, Litn;->j:I

    :cond_6
    const-string v0, "latency_bps"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "latency_bps"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, v5, Litn;->k:J

    :cond_7
    new-instance v1, Lego;

    invoke-direct {v1}, Lego;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    iput-wide v2, v1, Lego;->a:J

    sget-object v0, Legk;->f:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lego;->c:Ljava/lang/Integer;

    sget-object v0, Legk;->g:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lego;->d:Ljava/lang/Long;

    sget-object v0, Legj;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lego;->b:J

    invoke-virtual {v1}, Lego;->a()I

    move-result v0

    iput v0, v5, Litn;->l:I

    move-object v3, v5

    goto/16 :goto_1

    :cond_8
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_9

    move-object v1, v3

    goto/16 :goto_2

    :cond_9
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v1, v3

    move v2, v4

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v8

    if-eqz v8, :cond_b

    add-int/lit8 v1, v2, 0x1

    if-le v1, v6, :cond_c

    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_a

    const-string v0, "Herrevad"

    const-string v1, "more than one registered CellInfo.  Can\'t tell which is active.  Skipping."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move-object v1, v3

    goto/16 :goto_2

    :cond_b
    move-object v0, v1

    move v1, v2

    :cond_c
    move v2, v1

    move-object v1, v0

    goto :goto_4

    :cond_d
    instance-of v0, v1, Landroid/telephony/CellInfoCdma;

    if-eqz v0, :cond_10

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getDbm()I

    move-result v1

    iput v1, v5, Litn;->e:I

    move-object v1, v0

    :goto_5
    if-eqz v1, :cond_1b

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v9, :cond_14

    move-object v2, v3

    :goto_6
    if-eqz v2, :cond_1a

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v7, "connectivity"

    invoke-virtual {v1, v7}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v7

    iput v7, v2, Litk;->e:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    iput-boolean v0, v2, Litk;->f:Z

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    iput-boolean v0, v2, Litk;->g:Z

    :cond_e
    iput-object v2, v5, Litn;->c:Litk;

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-lt v4, v7, :cond_f

    invoke-virtual {v0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v7

    sub-long/2addr v1, v7

    long-to-double v1, v1

    const-wide v7, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v1, v7

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    mul-double/2addr v7, v10

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-int v2, v7

    iput v2, v5, Litn;->f:I

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    mul-double/2addr v7, v10

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-int v2, v7

    iput v2, v5, Litn;->g:I

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v5, Litn;->h:I

    iput v1, v5, Litn;->i:I

    :cond_f
    move v4, v6

    goto/16 :goto_3

    :cond_10
    instance-of v0, v1, Landroid/telephony/CellInfoGsm;

    if-eqz v0, :cond_11

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v1

    iput v1, v5, Litn;->e:I

    move-object v1, v0

    goto/16 :goto_5

    :cond_11
    instance-of v0, v1, Landroid/telephony/CellInfoLte;

    if-eqz v0, :cond_12

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoLte;

    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoLte;

    invoke-virtual {v1}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v1

    iput v1, v5, Litn;->e:I

    move-object v1, v0

    goto/16 :goto_5

    :cond_12
    instance-of v0, v1, Landroid/telephony/CellInfoWcdma;

    if-eqz v0, :cond_13

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v1

    iput v1, v5, Litn;->e:I

    move-object v1, v0

    goto/16 :goto_5

    :cond_13
    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "Herrevad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Registered cellinfo is unrecognized type "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_14
    new-instance v2, Litk;

    invoke-direct {v2}, Litk;-><init>()V

    instance-of v0, v1, Landroid/telephony/CellIdentityCdma;

    if-eqz v0, :cond_15

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityCdma;

    new-instance v7, Litj;

    invoke-direct {v7}, Litj;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v8

    iput v8, v7, Litj;->a:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v8

    iput v8, v7, Litj;->b:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v0

    iput v0, v7, Litj;->c:I

    iput-object v7, v2, Litk;->a:Litj;

    goto/16 :goto_6

    :cond_15
    instance-of v0, v1, Landroid/telephony/CellIdentityGsm;

    if-eqz v0, :cond_16

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityGsm;

    new-instance v7, Litl;

    invoke-direct {v7}, Litl;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Litl;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Litl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v8

    iput v8, v7, Litl;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v0

    iput v0, v7, Litl;->d:I

    iput-object v7, v2, Litk;->b:Litl;

    goto/16 :goto_6

    :cond_16
    instance-of v0, v1, Landroid/telephony/CellIdentityLte;

    if-eqz v0, :cond_17

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityLte;

    new-instance v7, Litm;

    invoke-direct {v7}, Litm;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Litm;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Litm;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v8

    iput v8, v7, Litm;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v0

    iput v0, v7, Litm;->d:I

    iput-object v7, v2, Litk;->c:Litm;

    goto/16 :goto_6

    :cond_17
    instance-of v0, v1, Landroid/telephony/CellIdentityWcdma;

    if-eqz v0, :cond_18

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityWcdma;

    new-instance v7, Lito;

    invoke-direct {v7}, Lito;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lito;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lito;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v8

    iput v8, v7, Lito;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v0

    iput v0, v7, Lito;->d:I

    iput-object v7, v2, Litk;->d:Lito;

    goto/16 :goto_6

    :cond_18
    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_19

    const-string v0, "Herrevad"

    const-string v2, "Registered cellinfo is unrecognized"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    move-object v2, v3

    goto/16 :goto_6

    :cond_1a
    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "Herrevad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "unable to create cell identity proto from cellid: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_1b
    const-string v0, "Herrevad"

    const-string v1, "mobile connection type with no cell id"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Legq;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Legq;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1c

    new-instance v1, Litp;

    invoke-direct {v1}, Litp;-><init>()V

    iput-object v0, v1, Litp;->b:Ljava/lang/String;

    iput-boolean v6, v1, Litp;->c:Z

    iput-object v1, v5, Litn;->d:Litp;

    :goto_7
    if-eqz v6, :cond_1d

    move-object v3, v5

    goto/16 :goto_1

    :cond_1c
    const-string v0, "Herrevad"

    const-string v1, "hashedBssid null, can\'t report that it\'s nomap"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v4

    goto :goto_7

    :cond_1d
    const-string v0, "Herrevad"

    const-string v1, "Unable to create nomap event"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_1e
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Legq;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_20

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v5, Litn;->e:I

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_22

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_22

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_22

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Legq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Legr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_9
    if-eqz v1, :cond_1f

    if-nez v2, :cond_23

    :cond_1f
    const-string v0, "Herrevad"

    const-string v1, "hashedBssid or hashedSsidBssid null, can\'t report on this"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_a
    if-nez v4, :cond_4

    goto/16 :goto_1

    :cond_20
    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_21

    const-string v0, "Herrevad"

    const-string v1, "Asked for wifi rssi without an active wifi connection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    move-object v0, v3

    goto :goto_8

    :cond_22
    move-object v1, v3

    goto :goto_9

    :cond_23
    new-instance v7, Litp;

    invoke-direct {v7}, Litp;-><init>()V

    iput-object v1, v7, Litp;->a:Ljava/lang/String;

    iput-object v2, v7, Litp;->b:Ljava/lang/String;

    iput-boolean v4, v7, Litp;->c:Z

    sget-object v0, Legk;->c:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_24

    sget-object v0, Legk;->d:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    sget-object v0, Legk;->e:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v7, Litp;->e:Z

    :cond_24
    iput-object v7, v5, Litn;->d:Litp;

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v0}, Lcv;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    iput-boolean v0, v7, Litp;->f:Z

    move v4, v6

    goto :goto_a

    :cond_25
    const-string v0, "Herrevad"

    const-string v1, "No source package sent.  Not logging this event."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v0, "Herrevad"

    const-string v1, "Package name not recognized after it was verified"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Legl;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x6

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t call reportNetworkQualityBlocking on main thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0, p1}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-static {p2}, Lirg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Legk;->a:Lbgm;

    sget-object v4, Legk;->b:Lbgm;

    sget-object v0, Legj;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v3, v4, v0}, Legp;->a(Lbgm;Lbgm;I)V

    if-ltz v5, :cond_1

    invoke-virtual {v3}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, v5, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "Herrevad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "over daily limit of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Legj;->a:Lbfy;

    invoke-virtual {v2}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " per day, skipping this upload"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Herrevad"

    const-string v2, "Could not verify the package name of the caller.  Dropping log event."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "Herrevad"

    const-string v2, "Calling package is not allowed to use."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, Legl;->a(Ljava/lang/String;Landroid/os/Bundle;)Litn;

    move-result-object v3

    if-nez v3, :cond_5

    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "Herrevad"

    const-string v1, "could not create proto from information provided by client app.  Not uploading."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    if-eqz p3, :cond_9

    iget-object v0, p0, Legl;->b:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Legq;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {p3}, Legl;->a(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v0

    :goto_2
    sget-boolean v4, Legn;->a:Z

    if-eqz v4, :cond_6

    const-string v4, "Herrevad"

    const-string v5, "successfully built payload, queue event for upload"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v4, Lfko;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v5

    const/16 v6, 0xd

    invoke-direct {v4, v5, v6, v1, v2}, Lfko;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    if-eqz v0, :cond_8

    sget-object v1, Legl;->a:Ljava/lang/String;

    invoke-static {v3}, Lizs;->a(Lizs;)[B

    move-result-object v2

    invoke-virtual {v4, v1, v2, v0}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    :goto_3
    sget-boolean v0, Legn;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "Herrevad"

    const-string v1, "NetworkQualityService - send to playlog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v4}, Lfko;->a()V

    sget-object v1, Legk;->a:Lbgm;

    sget-object v0, Legk;->b:Lbgm;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v1, v0, v2}, Legp;->a(Lbgm;Lbgm;I)V

    invoke-virtual {v1}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbgm;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_8
    sget-object v0, Legl;->a:Ljava/lang/String;

    invoke-static {v3}, Lizs;->a(Lizs;)[B

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v4, v0, v1, v2}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Landroid/os/Bundle;)[Ljava/lang/String;
    .locals 6

    if-nez p0, :cond_0

    const-string v0, "Herrevad"

    const-string v1, "null extraStats, returning null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "Herrevad"

    const-string v3, "null key, skipping"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v3, "Herrevad"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "null value for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", skipping."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2

    new-instance v0, Legm;

    invoke-direct {v0, p0, p1, p2, p3}, Legm;-><init>(Legl;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Legm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
