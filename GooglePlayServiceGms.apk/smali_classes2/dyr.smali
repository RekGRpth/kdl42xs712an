.class public final Ldyr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private b:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V
    .locals 1

    iput-object p1, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Ldyr;->b:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-object v0, p0, Ldyr;->b:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldyx;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyx;->a(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldyx;

    move-result-object v0

    invoke-virtual {v0}, Ldyx;->W()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldyr;->b:Ljava/lang/CharSequence;

    return-void

    :cond_1
    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Ldyx;

    move-result-object v0

    invoke-virtual {v0}, Ldyx;->Y()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Ldyr;->a:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
