.class public final Lgod;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private Y:Lgpp;

.field a:Landroid/widget/CheckBox;

.field b:Landroid/widget/TextView;

.field c:Lgoh;

.field d:Landroid/widget/Spinner;

.field private e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lgod;->f:I

    return-void
.end method

.method static synthetic a(Lgod;)Lgpp;
    .locals 1

    iget-object v0, p0, Lgod;->Y:Lgpp;

    return-object v0
.end method

.method static synthetic a(Lgod;Z)Z
    .locals 0

    iput-boolean p1, p0, Lgod;->g:Z

    return p1
.end method

.method static synthetic b(Lgod;Z)Z
    .locals 0

    iput-boolean p1, p0, Lgod;->h:Z

    return p1
.end method


# virtual methods
.method public final E_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lgod;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    :cond_0
    return-void
.end method

.method public final J()V
    .locals 3

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgod;->Y:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    iget-object v2, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lgpr;->a(Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    return-void
.end method

.method public final K()Z
    .locals 1

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f04010a    # com.google.android.gms.R.layout.plus_sharebox_add_to_circle_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a02c4    # com.google.android.gms.R.id.circles_checkbox

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    const v0, 0x7f0a02c5    # com.google.android.gms.R.id.circles_checkbox_text

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgod;->b:Landroid/widget/TextView;

    const v0, 0x7f0a02c6    # com.google.android.gms.R.id.circles_spinner

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    return-object v1
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lgod;->d:Landroid/widget/Spinner;

    iget v0, p0, Lgod;->f:I

    if-ltz v0, :cond_0

    iget v0, p0, Lgod;->f:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpp;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpp;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lgpp;

    iput-object p1, p0, Lgod;->Y:Lgpp;

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v2, p0, Lgod;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p0}, Lgod;->j()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f0023    # com.google.android.gms.R.plurals.plus_sharebox_circles_checkbox_number_of_people

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lgod;->j()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b03b6    # com.google.android.gms.R.string.plus_sharebox_circles_checkbox_label

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v6, Lgoe;

    invoke-direct {v6, p0}, Lgoe;-><init>(Lgod;)V

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v5, v6, v4, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lgod;->b:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    if-eqz p2, :cond_0

    iget-boolean v2, p0, Lgod;->i:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgod;->a:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lgod;->g:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v2, p0, Lgod;->a:Landroid/widget/CheckBox;

    iget-boolean v3, p0, Lgod;->h:Z

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    :goto_0
    iget-object v2, p0, Lgod;->a:Landroid/widget/CheckBox;

    iget-object v3, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->m()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    new-instance v0, Lgoh;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lgoh;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lgod;->c:Lgoh;

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    iget-object v2, p0, Lgod;->c:Lgoh;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    if-eqz p2, :cond_2

    iget v0, p0, Lgod;->f:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    iget v1, p0, Lgod;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :goto_2
    return-void

    :cond_0
    iget-object v2, p0, Lgod;->a:Landroid/widget/CheckBox;

    iget-object v3, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->k()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    iget-object v1, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 2

    iget-object v0, p0, Lgod;->c:Lgoh;

    invoke-virtual {v0, p1}, Lgoh;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    iget-object v0, p0, Lgod;->c:Lgoh;

    invoke-virtual {v0}, Lgoh;->a()I

    move-result v0

    iput v0, p0, Lgod;->f:I

    iget-object v0, p0, Lgod;->d:Landroid/widget/Spinner;

    iget v1, p0, Lgod;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    const-string v0, "last_position"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgod;->f:I

    const-string v0, "last_checked"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lgod;->g:Z

    const-string v0, "last_enabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lgod;->h:Z

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lgod;->i:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    iget-object v0, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "add_to_circle_data"

    iget-object v2, p0, Lgod;->e:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "last_position"

    iget v2, p0, Lgod;->f:I

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "last_checked"

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "last_enabled"

    iget-object v2, p0, Lgod;->a:Landroid/widget/CheckBox;

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v1

    goto :goto_1
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Lcom/google/android/gms/plus/sharebox/Circle;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lgod;->Y:Lgpp;

    invoke-interface {v0}, Lgpp;->l()V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lgod;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_3

    iget v1, p0, Lgod;->f:I

    if-eq v1, p3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgod;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_3
    iput p3, p0, Lgod;->f:I

    goto :goto_1
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
