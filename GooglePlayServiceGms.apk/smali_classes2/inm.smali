.class public final Linm;
.super Lizk;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:I

.field private C:Z

.field private D:J

.field private E:I

.field public a:Ljava/util/List;

.field public b:Ljava/util/List;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Link;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:J

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Linm;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linm;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linm;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linm;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linm;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linm;->n:Ljava/lang/String;

    iput-wide v2, p0, Linm;->p:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Linm;->a:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Linm;->b:Ljava/util/List;

    iput-boolean v1, p0, Linm;->r:Z

    const/4 v0, 0x0

    iput-object v0, p0, Linm;->t:Link;

    iput-boolean v1, p0, Linm;->v:Z

    iput-wide v2, p0, Linm;->x:J

    const/4 v0, 0x2

    iput v0, p0, Linm;->z:I

    iput v1, p0, Linm;->B:I

    iput-wide v2, p0, Linm;->D:J

    const/4 v0, -0x1

    iput v0, p0, Linm;->E:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Linm;->E:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Linm;->b()I

    :cond_0
    iget v0, p0, Linm;->E:I

    return v0
.end method

.method public final a(I)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->y:Z

    iput p1, p0, Linm;->z:I

    return-object p0
.end method

.method public final a(J)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->w:Z

    iput-wide p1, p0, Linm;->x:J

    return-object p0
.end method

.method public final a(Linp;)Linm;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Linm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Linm;->a:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Linm;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->c:Z

    iput-object p1, p0, Linm;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->u:Z

    iput-boolean p1, p0, Linm;->v:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->a(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->b(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->c(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->d(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->e(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->f(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Linm;->o:Z

    iput-wide v0, p0, Linm;->p:J

    goto :goto_0

    :sswitch_8
    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Linm;->a(Linp;)Linm;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Linm;->g(Ljava/lang/String;)Linm;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v2, p0, Linm;->q:Z

    iput-boolean v0, p0, Linm;->r:Z

    goto :goto_0

    :sswitch_b
    new-instance v0, Link;

    invoke-direct {v0}, Link;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Linm;->s:Z

    iput-object v0, p0, Linm;->t:Link;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Linm;->a(Z)Linm;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Linm;->a(J)Linm;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Linm;->a(I)Linm;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Linm;->b(I)Linm;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Linm;->b(J)Linm;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x52 -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x70 -> :sswitch_c
        0x78 -> :sswitch_d
        0x80 -> :sswitch_e
        0x88 -> :sswitch_f
        0x90 -> :sswitch_10
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Linm;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Linm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Linm;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Linm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Linm;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Linm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Linm;->i:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Linm;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Linm;->k:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Linm;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Linm;->m:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Linm;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Linm;->o:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-wide v1, p0, Linm;->p:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_6
    iget-object v0, p0, Linm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linp;

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Linm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILjava/lang/String;)V

    goto :goto_1

    :cond_8
    iget-boolean v0, p0, Linm;->q:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xc

    iget-boolean v1, p0, Linm;->r:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_9
    iget-boolean v0, p0, Linm;->s:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xd

    iget-object v1, p0, Linm;->t:Link;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_a
    iget-boolean v0, p0, Linm;->u:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xe

    iget-boolean v1, p0, Linm;->v:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_b
    iget-boolean v0, p0, Linm;->w:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xf

    iget-wide v1, p0, Linm;->x:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_c
    iget-boolean v0, p0, Linm;->y:Z

    if-eqz v0, :cond_d

    const/16 v0, 0x10

    iget v1, p0, Linm;->z:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_d
    iget-boolean v0, p0, Linm;->A:Z

    if-eqz v0, :cond_e

    const/16 v0, 0x11

    iget v1, p0, Linm;->B:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_e
    iget-boolean v0, p0, Linm;->C:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x12

    iget-wide v1, p0, Linm;->D:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_f
    return-void
.end method

.method public final b()I
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Linm;->c:Z

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    iget-object v2, p0, Linm;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Linm;->e:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Linm;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lizh;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Linm;->g:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Linm;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lizh;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Linm;->i:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Linm;->j:Ljava/lang/String;

    invoke-static {v2, v3}, Lizh;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-boolean v2, p0, Linm;->k:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Linm;->l:Ljava/lang/String;

    invoke-static {v2, v3}, Lizh;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Linm;->m:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Linm;->n:Ljava/lang/String;

    invoke-static {v2, v3}, Lizh;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-boolean v2, p0, Linm;->o:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-wide v3, p0, Linm;->p:J

    invoke-static {v2, v3, v4}, Lizh;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Linm;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linp;

    const/16 v4, 0x8

    invoke-static {v4, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Linm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lizh;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_2

    :cond_7
    add-int v0, v2, v1

    iget-object v1, p0, Linm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v1, p0, Linm;->q:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xc

    iget-boolean v2, p0, Linm;->r:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Linm;->s:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xd

    iget-object v2, p0, Linm;->t:Link;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Linm;->u:Z

    if-eqz v1, :cond_a

    const/16 v1, 0xe

    iget-boolean v2, p0, Linm;->v:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Linm;->w:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xf

    iget-wide v2, p0, Linm;->x:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Linm;->y:Z

    if-eqz v1, :cond_c

    const/16 v1, 0x10

    iget v2, p0, Linm;->z:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-boolean v1, p0, Linm;->A:Z

    if-eqz v1, :cond_d

    const/16 v1, 0x11

    iget v2, p0, Linm;->B:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-boolean v1, p0, Linm;->C:Z

    if-eqz v1, :cond_e

    const/16 v1, 0x12

    iget-wide v2, p0, Linm;->D:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iput v0, p0, Linm;->E:I

    return v0

    :cond_f
    move v0, v1

    goto/16 :goto_0
.end method

.method public final b(I)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->A:Z

    iput p1, p0, Linm;->B:I

    return-object p0
.end method

.method public final b(J)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->C:Z

    iput-wide p1, p0, Linm;->D:J

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->e:Z

    iput-object p1, p0, Linm;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->g:Z

    iput-object p1, p0, Linm;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->i:Z

    iput-object p1, p0, Linm;->j:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->k:Z

    iput-object p1, p0, Linm;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Linm;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Linm;->m:Z

    iput-object p1, p0, Linm;->n:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Linm;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Linm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Linm;->b:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Linm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
