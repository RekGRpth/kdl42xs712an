.class public final Lfju;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Landroid/os/ParcelFileDescriptor;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/os/ParcelFileDescriptor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfju;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {}, Lfjt;->b()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    return-void
.end method


# virtual methods
.method public final a()Landroid/os/ParcelFileDescriptor;
    .locals 1

    iget-object v0, p0, Lfju;->a:Landroid/os/ParcelFileDescriptor;

    return-object v0
.end method

.method public final close()V
    .locals 1

    iget-boolean v0, p0, Lfju;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfju;->b:Z

    invoke-static {}, Lfjt;->b()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    iget-object v0, p0, Lfju;->a:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method
