.class final Lhmn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0x42

    const/16 v8, 0xc

    const-wide v6, 0x3fc3333333333333L    # 0.15

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fed4f5903a7546dL    # 0.915936

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9304ccee5abc0eL    # 0.018573

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd40cf5b1c86488L    # 0.313291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f8d566cf41f212dL    # 0.014325

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f612fd32c625e9aL    # 0.002098

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fce36113404ea4bL    # 0.236025

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f312ba16e7a311fL    # 2.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff7da9435ac8a37L    # 1.490864

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL    # 1.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3a9b8cb8e086beL    # 4.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f68c7e28240b780L    # 0.003025

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0d5c31593e5fb7L    # 5.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe276273929ed39L    # 0.576923

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5611ba3ca7503cL    # 0.001347

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023c96872b020c5L    # 9.893375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff30644523f67f5L    # 1.18903

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0040bfe3b03e21L    # 3.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f713836a832b991L    # 0.004204

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f63e3e29307af21L    # 0.002428

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3b54195cc857f3L    # 4.17E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe04f4623d0bfa1L    # 0.509677

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb25119ce075f70L    # 0.07155

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f230164840e171aL    # 1.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4023fd66f0cfe154L    # 9.994926

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402311682f944242L    # 9.533998

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9b940fecdd0d8dL    # 0.026932

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6ec918e325d4a6L    # 0.003758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fde304ccee5abc1L    # 0.471698

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0bc98a222d5172L    # 5.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5144cbe1eb4203L    # 0.001054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f09b0ab2e1693c0L    # 4.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2754b05b7cfe58L    # 1.78E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f504d551d68c693L    # 9.95E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f06052502eec7c9L    # 4.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f23879c4113c686L    # 1.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3eed5c31593e5fb7L    # 1.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402541f559b3d07dL    # 10.628825

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef4f8b588e368f1L    # 2.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f283f91e646f156L    # 1.85E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd6db6e503fb374L    # 0.357143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b43526527a205L    # 1.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40d1b71758e22L    # 0.15665

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6fff79c842fa51L    # 0.003906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40003c55000c953aL    # 2.029459

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f463779e9d0e992L    # 6.78E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe1068fd199bb28L    # 0.532051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faf748a159817b9L    # 0.061436

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff76949a5657fb7L    # 1.463205

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f70f840181e03f7L    # 0.004143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef711947cfa26a2L    # 2.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f74801f75104d55L    # 0.005005

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f25a07b352a8438L    # 1.65E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf07bfe7e1fc09L    # 0.484848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f39b0ab2e1693c0L    # 3.92E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f18611fd5885d31L    # 9.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f06052502eec7c9L    # 4.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402552ba9d1f6018L    # 10.66158

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1083dbc23315d7L    # 6.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c4fc1df3300deL    # 1.08E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9225742dcf4624L    # 0.017721

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f894a6eb91b3f21L    # 0.012349

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa2ab474107314dL    # 0.036463

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f413c68661ae70cL    # 5.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0c4fc1df3300deL    # 5.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f129cbab649d389L    # 7.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f23465625a68L    # 0.155829

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc8b90214ad362fL    # 0.193146

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3248d7e02645e5L    # 2.79E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0b215fcc1871eL    # 0.521739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3754b05b7cfe58L    # 3.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40211da426bb55acL    # 8.557893

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fb8755ffe6d58c9L    # 0.095541

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa265492ff4ba52L    # 0.035929

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd88db0574b4070L    # 0.383648

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd7a4b124d099e1L    # 0.369427

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f588b11409a2403L    # 0.001498

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcc8c5004fb1184L    # 0.223032

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402685c054ef459eL    # 11.261233

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86883771865519L    # 0.011002

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f22599ed7c6fbd2L    # 1.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fac05d52c16df40L    # 0.054732

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f611ada76d97b31L    # 0.002088

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffb3d5bab21815aL    # 1.70248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4127b2cc70868L    # 0.156814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f357eed45e9185dL    # 3.28E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f216ebd4cfd08d5L    # 1.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd425edd052934bL    # 0.314815

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f49439de481f538L    # 7.71E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4669ced0b30b5bL    # 6.84E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd74f54d1e96c40L    # 0.364217

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3205bc01a36e2fL    # 2.75E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcc4fc1df3300deL    # 0.221184

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f317f84449dbec2L    # 2.67E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3bc98a222d5172L    # 4.24E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9893b7d84901d2L    # 0.024001

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f30d7be9856a37bL    # 2.57E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f22be48a58b3f64L    # 1.43E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4026694bdfd2630fL    # 11.205657

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f47225b749adc90L    # 7.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7d56f32bdc26ddL    # 0.007163

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa51e96c3fc43b3L    # 0.041249

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022327d45a5fc7eL    # 9.098612

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37763e4abe6a33L    # 3.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7193708aac96ccL    # 0.004291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4711947cfa26a2L    # 7.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdd333333333333L    # 0.45625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4d6494d50ebaaeL    # 8.97E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbe94141e9af5baL    # 0.119447

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff8b397dd00f777L    # 1.543846

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc2c5fbf83382e4L    # 0.146667

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f84b380cb6c7a7dL    # 0.010108

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3eef75104d551d69L    # 1.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f45b1422ccb3a26L    # 6.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd01b21c475e636L    # 0.251656

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8fa1a0cf1800a8L    # 0.015445

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd01d8a54823854L    # 0.251803

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5f79420b3d4ae4L    # 0.001921

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1083dbc23315d7L    # 6.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f456e264e48626fL    # 6.54E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f33ec460ed80a18L    # 3.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fba6687f455a7d2L    # 0.103127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5290257c914b16L    # 0.001133

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9e40c8472c0e7cL    # 0.029544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d6cf850df15a5L    # 4.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd6a88ace24bba1L    # 0.354037

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6782d38476f2a6L    # 0.00287

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4001e5510d38cda7L    # 2.236971

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402beaafbc1acde2L    # 13.958372

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a236bb1290258L    # 13.069181

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402c16b7457c0b13L    # 14.044367

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4023d1d12cadddf4L    # 9.909799

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f589374bc6a7efaL    # 0.0015

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f47dae81882adc5L    # 7.28E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402107b506dd69d3L    # 8.515053

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x40019759ee88df37L    # 2.198902

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f24f8b588e368f1L    # 1.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9b4677b395c422L    # 0.026636

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4007e240b780346eL    # 2.985475

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7859c8c9320d99L    # 0.005945

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f89d99029ae4f33L    # 0.012622

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8219220ff54089L    # 0.008837

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f73c9abb01c92deL    # 0.004831

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6bd61f5be5d9e4L    # 0.003398

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020843c5bd0e12fL    # 8.258273

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9c426351deefe5L    # 0.027597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f307314ca925fe9L    # 2.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd555821294574L    # 0.229167

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f44cec41dd1a21fL    # 6.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3ffb480a5accd5L    # 4.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd45aaf78feef5fL    # 0.318035

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f230164840e171aL    # 1.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b003686a4ca4fL    # 1.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f358fb43d89ce4aL    # 3.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3bb8c32a8c9b84L    # 4.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f19b0ab2e1693c0L    # 9.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff25dc2f405f6baL    # 1.147891

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd91044f1a1986cL    # 0.391618

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03660e51d25aabL    # 3.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb3b3e9a6f826eeL    # 0.076964

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f422fad6cb53501L    # 5.55E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f826c7eae5bc87eL    # 0.008996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f78bc59b8023a6dL    # 0.006039

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe00ceb356da017L    # 0.501577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa219eb6390c910L    # 0.035354

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb32ce89656eefaL    # 0.074904

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc1b5f1bef49cf5L    # 0.138365

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f815b573eab367aL    # 0.008475

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd276295208e150L    # 0.288462

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffa4cb6c7a7c9deL    # 1.643729

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f744ed6fda836ebL    # 0.004958

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f71f1cfbb949625L    # 0.004381

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f66659d12cadddfL    # 0.002734

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f650b955f78359cL    # 0.002569

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8a2be0589acbc9L    # 0.012779

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dafd976ff3aeL    # 2.356929

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403071a4f00ef135L    # 16.443923

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022a5573647baaaL    # 9.322931

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba759ab6d00b46L    # 0.103357

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7018e757928e0dL    # 0.00393

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fa13e5fb71fbc5eL    # 0.033679

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d19157abb8801L    # 4.44E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6620685553ef6bL    # 0.002701

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f9c8605681ecd4bL    # 0.027855

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f24727dcbddb984L    # 1.56E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc403254e6e221dL    # 0.156346

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f896e9bbf0dc769L    # 0.012418

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5ae70c1333b96bL    # 0.001642

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f326a65cf67b1c0L    # 2.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f75842b734b5137L    # 0.005253

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4522a6f3f52fc2L    # 6.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3eff75104d551d69L    # 3.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5409a240314877L    # 0.001223

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f400e6afcce1c58L    # 4.9E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3feaffae3608d089L    # 0.843711

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2fb82c2bd7f51fL    # 2.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fde45e4e69f05eaL    # 0.473016

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f476ddaceee0f3dL    # 7.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f242f61ed5ae1ceL    # 1.54E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f468b5cbff47736L    # 6.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd7f36ef8055fbbL    # 0.374233

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdfe679463cfb33L    # 0.498442

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4c39ffd60e94fL    # 0.040555

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6aa821f2990f30L    # 0.003254

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f860f1b25f633ceL    # 0.010771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4000f07e9d94d0ddL    # 2.117429

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc7b4245f5ad96aL    # 0.185185

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f068b5cbff47736L    # 4.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f50c6f7a0b5ed8dL    # 0.001024

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6991361dc93ea3L    # 0.003121

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc721ee675147f1L    # 0.180723

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f416ebd4cfd08d5L    # 5.32E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc0369fcf3dc055L    # 0.126667

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f216ebd4cfd08d5L    # 1.33E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fca5154866a11edL    # 0.205607

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc6666666666666L    # 0.175

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f340dd3fe1975f3L    # 3.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdb99999999999aL    # 0.43125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe109fa54c55433L    # 0.532468

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa0eef1bac2df0dL    # 0.033073

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9b47c73eee5259L    # 0.026641

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8d5d3dc8b86b16L    # 0.014338

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4003976f6d762520L    # 2.448943

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f46616b54e2b064L    # 6.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f967b5f1bef49cfL    # 0.021955

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fcf50c1b97353b5L    # 0.244652

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbdd72367e414e8L    # 0.116564

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67763e4abe6a33L    # 0.002864

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402293f6c2699c7cL    # 9.288992

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402eff566490a351L    # 15.498706

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402c8112ba16e7a3L    # 14.252096

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40225a0a52695960L    # 9.17586

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c2e33eff19503L    # 2.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f516659d12caddeL    # 0.001062

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f925bb7b6bb1290L    # 0.017928

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4132b55ef1fdeL    # 0.156835

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f37ca2120e1f7d7L    # 3.63E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd7792d1287c201L    # 0.366771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f378705425f2021L    # 3.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f04727dcbddb984L    # 3.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f02dfd694ccab3fL    # 3.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44ad362e903644L    # 6.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40388dc0010c6f7aL    # 24.553711

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4426fe718a86d7L    # 6.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4719f7f8ca8199L    # 7.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61a75cd0bb6ed6L    # 0.002155

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4031009906cca2dbL    # 17.002335

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3d3aa369fcf3dcL    # 4.46E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a79fec99f1ae3L    # 2.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f93cd573647baaaL    # 0.019338

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2a79fec99f1ae3L    # 2.02E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f18a43bb40b34e7L    # 9.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4309c7ffde7211L    # 5.81E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc2bebe1650a45dL    # 0.146446

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff82a24cc6822ffL    # 1.510289

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fda5a5b9628cbd1L    # 0.411765

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4024214573a79789L    # 10.064983

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb2c743201040c0L    # 0.073353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f695ee136e71cdaL    # 0.003097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f89274e22a2c237L    # 0.012282

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40186efe93187619L    # 6.108393

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f34727dcbddb984L    # 3.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f28a43bb40b34e7L    # 1.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f47e34b945308bcL    # 7.29E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1711947cfa26a2L    # 8.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddd8f0c77dd873L    # 0.466366

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fde01f75104d552L    # 0.46887

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2f31f46ed245b3L    # 2.38E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f416659d12caddeL    # 5.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb6666666666666L    # 0.0875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f27fc7607c419a0L    # 1.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f44488c60cbf2b2L    # 6.19E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb6af038e29f9cfL    # 0.088608

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022328a82a5614eL    # 9.098713

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbef9a8cdea033eL    # 0.120997

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f67e77d523b3637L    # 0.002918

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40117e54f7a91969L    # 4.373371

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021ef689ca18bd6L    # 8.967595

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f716fc9bc771434L    # 0.004257

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe4bc2fc69728a6L    # 0.647972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f40c6f7a0b5ed8dL    # 5.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f302ff8ec0f8833L    # 2.47E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x400b86149c6f36f0L    # 3.440469

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f59157abb8800ebL    # 0.001531

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb019c17225b74aL    # 0.062893

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40005c8b86b15f89L    # 2.045188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40224fe846a5d6bfL    # 9.156069

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5bda5119ce075fL    # 0.0017

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffb8ac5c13fd0d0L    # 1.72138

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402412aa7ded6ba9L    # 10.036457

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402fb092684cf074L    # 15.844867

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402313e89a88ace2L    # 9.538884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe15885d31337ebL    # 0.542056

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f312ba16e7a311fL    # 2.62E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4008fe62dc6e2a80L    # 3.124212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402274a0c282c6efL    # 9.227789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4004fab4b72c5198L    # 2.622415

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fce412cf0f9d2bfL    # 0.236364

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4028b231e7d9988dL    # 12.348037

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa023a6ce358299L    # 0.031522

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021ffb6134ce3deL    # 8.999436

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f40b630a91537a0L    # 5.1E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a975afaf85943L    # 0.001623

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f33122b7baecd08L    # 2.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0040bfe3b03e21L    # 3.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1e68a0d349be90L    # 1.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f94e11dbca9691aL    # 0.02039

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc78eeae9ee45c3L    # 0.184049

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40118c83a96d4c34L    # 4.387221

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f625785f8d2e515L    # 0.002239

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc29e44fa05143cL    # 0.145455

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd2eb0fadf2ecf2L    # 0.295597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86188b11409a24L    # 0.010789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x40001c4c9c90c4dfL    # 2.013818

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3f53825e13b18eL    # 4.78E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40141abd1aa821f3L    # 5.026112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7c1a47a9e2bcf9L    # 0.006861

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40212e29d8409e56L    # 8.590163

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3cd5f99c38b04bL    # 4.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc7ae0c17657753L    # 0.184999

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40c9d9d3458cdL    # 0.156635

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f545953586ca8a0L    # 0.001242

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f32be48a58b3f64L    # 2.86E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f83f6c2699c7bccL    # 0.009748

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa9ee2435696e59L    # 0.050645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5dda059a73b42dL    # 0.001822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5815a07b352a84L    # 0.00147

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8161a1db877ab3L    # 0.008487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff1761aa3f034b1L    # 1.091334

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4062072d40aaeafbL    # 144.224274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffc82a8869c66d3L    # 1.781899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40229e9b5a63f9a5L    # 9.309779

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ef2dfd694ccab3fL    # 1.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f210a137f38c543L    # 1.3E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402e0edd8b60f1b2L    # 15.029034

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fce065300581494L    # 0.234568

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f912513b5bf6a0eL    # 0.016743

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbd511dffc5479dL    # 0.114519

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5b3aeee957470fL    # 0.001662

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f85a07b352a8438L    # 0.01056

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4060780fdc161L    # 0.156434

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f84b8bef8ceb357L    # 0.010118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa5eb0fadf2ecf2L    # 0.042809

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f731db445ed4a1bL    # 0.004667

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401b8dcf893faf42L    # 6.888487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc411a11233df2bL    # 0.156788

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    iget-wide v0, p1, Lhmx;->d:D

    const-wide/high16 v2, 0x3fcc000000000000L    # 0.21875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9fa22706d50657L    # 0.030892

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcc18f81e8a2ec3L    # 0.219512

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6a8cdea033e78eL    # 0.003241

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc786ca89fc6da4L    # 0.183801

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f409d0635a426bbL    # 5.07E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff4919ef954eb14L    # 1.285552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40331ccb3a2595bcL    # 19.112476

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032c347735c182fL    # 18.762809

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faec15d2d01c0caL    # 0.060069

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc249235f809918L    # 0.142857

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401b0d9b1b79d90aL    # 6.763287

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff91fe86833c600L    # 1.57029

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0cd5f99c38b04bL    # 5.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdab76b3bb83cf3L    # 0.417445

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc0f301eabbcb1dL    # 0.132416

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f71fa333764f11bL    # 0.004389

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403db15ebfa8f7dbL    # 29.692852

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8289dadfb506ddL    # 0.009052

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40354cfc9363f573L    # 21.300729

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f447ae147ae147bL    # 6.25E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4006a1ef73c0c1fdL    # 2.82907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f670b49e01de269L    # 0.002813

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40010319c5a3e39fL    # 2.126514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb8b2745bf26f1eL    # 0.096473

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400a67e28240b780L    # 3.300725

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fd9fb86f47b677fL    # 0.405977

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4010fcc20d5629d8L    # 4.246834

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc4c77dd872f33dL    # 0.162338

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f940789613d31baL    # 0.01956

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40685553ef6b6L    # 0.156449

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404e052d234eb9a1L    # 60.04044

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6960fa15db3398L    # 0.003098

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a60d4562e09ffL    # 0.00161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc3c0ca600b0293L    # 0.154321

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62b7fe08aefb2bL    # 0.002285

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9171e29b6b2af1L    # 0.017036

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc03d25247cb70bL    # 0.126866

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41cd5f99c38b0L    # 0.15713

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa6e8af81626b2fL    # 0.044744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402ecf6683c297c0L    # 15.405079

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f110a137f38c543L    # 6.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402b2e7ddca4b125L    # 13.590804

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f212ba16e7a311fL    # 1.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f444e0daa0cbL    # 0.155892

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f52e40852b4d8baL    # 0.001153

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff031feeb2d0a24L    # 1.012206

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4021dd479d4d8341L    # 8.932187

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7205bc01a36e2fL    # 0.0044

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f48d6909aed56b0L    # 7.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd1a7b9170d62bfL    # 0.275862

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fab009b30728e93L    # 0.052739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff40daa0cae642cL    # 1.253336

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f864e8b7e4de3b9L    # 0.010892

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022d72606fac604L    # 9.420212

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa6974a3400b88dL    # 0.044123

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7ece5710880d80L    # 0.007521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020aa10be9424e6L    # 8.332159

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401db4ba0e842742L    # 7.426491

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9adc8fb86f47b6L    # 0.026232

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3a2d72ffd1dcdL    # 0.153407

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8ee99a62ed3522L    # 0.015094

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4039598fb43d89ceL    # 25.349849

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f96b09635610addL    # 0.022158

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40234753e707e176L    # 9.639312

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd18f9b13165d3aL    # 0.27439

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e7

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff6a31a4bdba0a5L    # 1.41482

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f86787ce95faa8bL    # 0.010972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f90285ec3dab5c4L    # 0.015779

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403819d12cadddf4L    # 24.100848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa82ba5a038194cL    # 0.047208

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b43526527a205L    # 8.32E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fbb333333333333L    # 0.10625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb26bc621b7e0acL    # 0.071957

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cb

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb908e581cf7879L    # 0.097792

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd2d2d44dca8e2eL    # 0.294118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcefb19e731d2e1L    # 0.242038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400d002ea960b6faL    # 3.625089

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc9968512231833L    # 0.199906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe7780dc33721d5L    # 0.733405

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f92f7f498c3b0c4L    # 0.018524

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc799999999999aL    # 0.184375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff85ccea28fe261L    # 1.522658

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402cd0f451761459L    # 14.408114

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6adea897635e74L    # 0.00328

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fadafd113837L    # 0.156093

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc42d59d55e6bc6L    # 0.157634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fcf781c714fce74L    # 0.245853

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401ea0d51f81a587L    # 7.657063

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4055e585015c2092L    # 87.586243

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa7e542e557de0dL    # 0.046671

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb0374ff865d7cbL    # 0.063344

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f582235b4edb2f6L    # 0.001473

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f52cf0f9d2bf551L    # 0.001148

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v9}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f474c4cdfaca362L    # 7.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91f53825e13b19L    # 0.017537

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff93177a7008a69L    # 1.574577

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f81afc04c8bc9cdL    # 0.008636

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fae4020817fc760L    # 0.059083

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ca

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd04e4430b178b3L    # 0.254777

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cb
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fef31a08bfc2225L    # 0.974808

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d0

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4043c8ef5ec80c74L    # 39.569805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ce

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fdcf8055fbb517aL    # 0.452638

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cd

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40154cf4a558ea7dL    # 5.325152

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ce
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd86dbd72bcb5feL    # 0.381698

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1cf
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d0
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fea325918a009f6L    # 0.818646

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e3

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff82f30a4e379b7L    # 1.511521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e2

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f91a65061416378L    # 0.017236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d9

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fd64de7ea5f84cbL    # 0.348505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d1
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034dbad9f0a1be3L    # 20.858118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d8

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fee7ab7564302b4L    # 0.95248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d3

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f52599ed7c6fbd2L    # 0.00112

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d3
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9bc98a222d5172L    # 0.027136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d6

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41672324c8366L    # 0.156935

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d4
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f83a797891e2153L    # 0.009597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d6
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f95a250f840181eL    # 0.021127

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d9
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc85c5f7c6759abL    # 0.190319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1da

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1da
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa7bbf93ff25e57L    # 0.046356

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc23bcd35a85879L    # 0.14245

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1df

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404383717df19d67L    # 39.026901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1dc

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403bd9363f572de4L    # 27.848484

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1db

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1db
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1dc
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f60539fba450accL    # 0.001993

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1de

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ffe0e325d4a5df2L    # 1.878466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1dd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1dd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1df
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e0
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403653b43d89ce4aL    # 22.326969

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa55b4677b395c4L    # 0.041712

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e6

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40687ae1bf37b8d4L    # 195.840057

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e5

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0fadf2ecf2064L    # 0.066328

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e7
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4039863ec02f2f98L    # 25.524395

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e8
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdab76b3bb83cf3L    # 0.417445

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ea

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffb7a3ca7503b82L    # 1.717343

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ea
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
