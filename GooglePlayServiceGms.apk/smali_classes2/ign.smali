.class final Lign;
.super Leqd;
.source "SourceFile"


# instance fields
.field final synthetic a:Ligi;


# direct methods
.method private constructor <init>(Ligi;)V
    .locals 0

    iput-object p1, p0, Lign;->a:Ligi;

    invoke-direct {p0}, Leqd;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ligi;B)V
    .locals 0

    invoke-direct {p0, p1}, Lign;-><init>(Ligi;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 3

    const-string v0, "PlaceReporter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "received location change for subscription: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lign;->a:Ligi;

    iget-object v2, v2, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {v2}, Lcom/google/android/location/places/PlaceSubscription;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ligj;

    iget-object v1, p0, Lign;->a:Ligi;

    iget-object v2, p0, Lign;->a:Ligi;

    invoke-direct {v0, v1, v2, p1}, Ligj;-><init>(Ligi;Ligi;Landroid/location/Location;)V

    iget-object v1, p0, Lign;->a:Ligi;

    iget-object v1, v1, Ligi;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
