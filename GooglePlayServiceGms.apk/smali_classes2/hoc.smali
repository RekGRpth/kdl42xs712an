.class public final Lhoc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhnz;

.field public final b:Lhnt;

.field public final c:Lhnt;


# direct methods
.method public constructor <init>(Lhnz;Lhnt;Lhnt;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhoc;->a:Lhnz;

    iput-object p2, p0, Lhoc;->b:Lhnt;

    iput-object p3, p0, Lhoc;->c:Lhnt;

    return-void
.end method

.method public static a(Livi;)Z
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, Livi;->c(I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v3}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0, v3, v1}, Livi;->c(II)Livi;

    move-result-object v2

    invoke-virtual {v2, v0}, Livi;->c(I)I

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x6

    invoke-virtual {v2, v0}, Livi;->i(I)Z

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v0}, Livi;->c(I)I

    move-result v2

    const/16 v3, 0x17

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    iget-object v3, p0, Lhoc;->a:Lhnz;

    iget-object v0, v3, Lhnz;->d:Lhpk;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, v3, Lhnz;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    iget-object v0, v3, Lhnz;->e:Lidq;

    iget-object v1, v3, Lhnz;->c:Ljava/io/File;

    invoke-interface {v0, v1}, Lidq;->a(Ljava/io/File;)V

    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v4, v3, Lhnz;->c:Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    monitor-enter v3
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v2, v3, Lhnz;->b:Lhpc;

    iget-object v4, v3, Lhnz;->a:Lhox;

    invoke-interface {v2, v4, v0}, Lhpc;->a(Lhox;Ljava/io/OutputStream;)V

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    iget-object v2, v3, Lhnz;->d:Lhpk;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lhpk;->a(Ljava/io/DataOutputStream;[B)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v1}, Lhnz;->a(Ljava/io/Closeable;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lhoc;->b:Lhnt;

    invoke-virtual {v0}, Lhnt;->b()V

    iget-object v0, p0, Lhoc;->c:Lhnt;

    invoke-virtual {v0}, Lhnt;->b()V

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "ModelState"

    const-string v1, "Wrote ModelState to disk"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catch_0
    move-exception v0

    :goto_1
    :try_start_5
    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "FileTemporalCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find LRU cache file to write to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_2
    invoke-static {v1}, Lhnz;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_6
    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_3

    const-string v2, "FileTemporalCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException while writing LRU cache file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, v3, Lhnz;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    invoke-static {v1}, Lhnz;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lhnz;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_2
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lhoc;->a:Lhnz;

    iget-object v0, v0, Lhnz;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iget-object v0, p0, Lhoc;->b:Lhnt;

    invoke-virtual {v0}, Lhnt;->c()V

    iget-object v0, p0, Lhoc;->c:Lhnt;

    invoke-virtual {v0}, Lhnt;->c()V

    return-void
.end method
