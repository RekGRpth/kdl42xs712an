.class public final Leij;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lehs;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Leij;->a:Ljava/lang/String;

    iput-boolean v1, p0, Leij;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Leij;->c:Lehs;

    iput v1, p0, Leij;->d:I

    const-string v0, ""

    iput-object v0, p0, Leij;->e:Ljava/lang/String;

    iput-boolean v1, p0, Leij;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leij;->g:J

    const/4 v0, -0x1

    iput v0, p0, Leij;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Leij;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Leij;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-boolean v1, p0, Leij;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Leij;->b:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Leij;->c:Lehs;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Leij;->c:Lehs;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Leij;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Leij;->d:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Leij;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Leij;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Leij;->f:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Leij;->f:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-wide v1, p0, Leij;->g:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-wide v2, p0, Leij;->g:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Leij;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leij;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leij;->b:Z

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Leij;->c:Lehs;

    if-nez v0, :cond_1

    new-instance v0, Lehs;

    invoke-direct {v0}, Lehs;-><init>()V

    iput-object v0, p0, Leij;->c:Lehs;

    :cond_1
    iget-object v0, p0, Leij;->c:Lehs;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leij;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leij;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Leij;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Leij;->g:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-object v0, p0, Leij;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Leij;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Leij;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Leij;->b:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_1
    iget-object v0, p0, Leij;->c:Lehs;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Leij;->c:Lehs;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget v0, p0, Leij;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Leij;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_3
    iget-object v0, p0, Leij;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Leij;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Leij;->f:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Leij;->f:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_5
    iget-wide v0, p0, Leij;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-wide v1, p0, Leij;->g:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_6
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leij;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leij;

    iget-object v2, p0, Leij;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Leij;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Leij;->a:Ljava/lang/String;

    iget-object v3, p1, Leij;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Leij;->b:Z

    iget-boolean v3, p1, Leij;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Leij;->c:Lehs;

    if-nez v2, :cond_6

    iget-object v2, p1, Leij;->c:Lehs;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Leij;->c:Lehs;

    iget-object v3, p1, Leij;->c:Lehs;

    invoke-virtual {v2, v3}, Lehs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Leij;->d:I

    iget v3, p1, Leij;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Leij;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Leij;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Leij;->e:Ljava/lang/String;

    iget-object v3, p1, Leij;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-boolean v2, p0, Leij;->f:Z

    iget-boolean v3, p1, Leij;->f:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-wide v2, p0, Leij;->g:J

    iget-wide v4, p1, Leij;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget-object v0, p0, Leij;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Leij;->b:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Leij;->c:Lehs;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Leij;->d:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Leij;->e:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Leij;->f:Z

    if-eqz v1, :cond_4

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Leij;->g:J

    iget-wide v3, p0, Leij;->g:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Leij;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v0, p0, Leij;->c:Lehs;

    invoke-virtual {v0}, Lehs;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Leij;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_4
.end method
