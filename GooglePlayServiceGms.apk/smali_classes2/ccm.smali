.class public final Lccm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:J


# instance fields
.field private final a:J

.field private final b:Landroid/text/format/Time;

.field private final c:Landroid/content/Context;

.field private final d:Landroid/text/format/Time;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x6

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lccm;->f:J

    return-void
.end method

.method public constructor <init>(Lcoy;Landroid/text/format/Time;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lccm;->c:Landroid/content/Context;

    iput-object p2, p0, Lccm;->b:Landroid/text/format/Time;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iput-wide v0, p0, Lccm;->a:J

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lccm;->d:Landroid/text/format/Time;

    iget-object v0, p0, Lccm;->c:Landroid/content/Context;

    const v1, 0x7f0b002b    # com.google.android.gms.R.string.drive_doclist_date_never_label

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lccm;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lccm;->d:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    iget-object v0, p0, Lccm;->d:Landroid/text/format/Time;

    invoke-static {v0}, Landroid/text/format/Time;->isEpoch(Landroid/text/format/Time;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccm;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lccm;->a:J

    sget-wide v2, Lccm;->f:J

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    iget-object v0, p0, Lccm;->d:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->year:I

    iget-object v1, p0, Lccm;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    if-eq v0, v1, :cond_2

    const v0, 0x10a14

    :goto_2
    iget-object v1, p0, Lccm;->c:Landroid/content/Context;

    invoke-static {v1, p1, p2, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lccm;->d:Landroid/text/format/Time;

    iget v0, v0, Landroid/text/format/Time;->yearDay:I

    iget-object v1, p0, Lccm;->b:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->yearDay:I

    if-eq v0, v1, :cond_3

    const v0, 0x10a18

    goto :goto_2

    :cond_3
    const v0, 0x10a01

    goto :goto_2
.end method
