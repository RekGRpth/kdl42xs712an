.class public final Lgwy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lgwi;
.implements Lgyo;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final b:Lgyo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object p2, p0, Lgwy;->b:Lgyo;

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 3

    iget-object v0, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->R_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgwy;->b:Lgyo;

    invoke-interface {v0}, Lgyo;->R_()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final S_()Z
    .locals 1

    iget-object v0, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwy;->b:Lgyo;

    invoke-interface {v0}, Lgyo;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    iget-object v0, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lgwy;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->selectAll()V

    :cond_0
    return-void
.end method
