.class public final Lbsc;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/QueryRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/QueryRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbsc;->c:Lcom/google/android/gms/drive/internal/QueryRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 3

    iget-object v0, p0, Lbsc;->c:Lcom/google/android/gms/drive/internal/QueryRequest;

    const-string v1, "Invalid query request: no request"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsc;->c:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    const-string v1, "Invalid query request: no query"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbsc;->c:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    iget-object v1, p0, Lbsc;->b:Lbrc;

    invoke-interface {v1, v0}, Lbrc;->a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lbsc;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-interface {v0, v2}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnListEntriesResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method
