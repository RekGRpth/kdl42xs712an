.class abstract Lfgu;
.super Lfgt;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lfgt;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final c()Landroid/util/Pair;
    .locals 3

    iget-object v0, p0, Lfgu;->a:Landroid/content/Context;

    iget-object v1, p0, Lffb;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lfgy;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfgu;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lfgu;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    new-instance v2, Lfmu;

    invoke-direct {v2, v0, v1}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x1

    iput v1, v2, Lfmu;->b:I

    invoke-virtual {v2}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {v0, v1, v2}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x6

    invoke-static {v0, v1}, Lffc;->a(ILandroid/os/Bundle;)Lffc;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lfgt;->c()Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method
