.class final Limu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/impl/conn/tsccm/PoolEntryRequest;


# instance fields
.field final synthetic a:Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

.field final synthetic b:Lorg/apache/http/conn/routing/HttpRoute;

.field final synthetic c:Ljava/lang/Object;

.field final synthetic d:Limt;


# direct methods
.method constructor <init>(Limt;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Limu;->d:Limt;

    iput-object p2, p0, Limu;->a:Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

    iput-object p3, p0, Limu;->b:Lorg/apache/http/conn/routing/HttpRoute;

    iput-object p4, p0, Limu;->c:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final abortRequest()V
    .locals 2

    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Limu;->a:Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

    invoke-virtual {v0}, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->b(Limt;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Limu;->d:Limt;

    invoke-static {v1}, Limt;->b(Limt;)Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final getPoolEntry(JLjava/util/concurrent/TimeUnit;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;
    .locals 7

    iget-object v0, p0, Limu;->d:Limt;

    iget-object v1, p0, Limu;->b:Lorg/apache/http/conn/routing/HttpRoute;

    iget-object v2, p0, Limu;->c:Ljava/lang/Object;

    iget-object v6, p0, Limu;->a:Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

    move-wide v3, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Limt;->getEntryBlocking(Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v0

    return-object v0
.end method
