.class final Lgwk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lgwj;

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lgwj;)V
    .locals 1

    iput-object p1, p0, Lgwk;->a:Lgwj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lgwk;->b:I

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    iget-object v0, p0, Lgwk;->a:Lgwj;

    iget-object v0, v0, Lgwj;->b:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgth;->a(Ljava/lang/String;)I

    move-result v0

    iget v1, p0, Lgwk;->b:I

    if-eq v1, v0, :cond_1

    iget-object v1, p0, Lgwk;->a:Lgwj;

    iget-object v1, v1, Lgwj;->f:Lcom/google/android/gms/wallet/common/ui/CvcImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/CvcImageView;->a(I)V

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v0}, Lgth;->d(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    iget-object v2, p0, Lgwk;->a:Lgwj;

    iget-object v2, v2, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/text/InputFilter;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v1, p0, Lgwk;->a:Lgwj;

    iget-object v1, v1, Lgwj;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgwk;->a:Lgwj;

    iget-object v1, v1, Lgwj;->Y:Lgwn;

    invoke-virtual {v1}, Lgwn;->R_()Z

    :cond_0
    iput v0, p0, Lgwk;->b:I

    :cond_1
    iget-object v0, p0, Lgwk;->a:Lgwj;

    invoke-static {v0}, Lgwj;->a(Lgwj;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lgwk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lgwk;->a:Lgwj;

    invoke-static {v1, v0}, Lgwj;->a(Lgwj;Ljava/lang/String;)V

    :cond_3
    iput-object v0, p0, Lgwk;->c:Ljava/lang/String;

    return-void
.end method
