.class public final Lhec;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ldl;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lheb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhed;

    invoke-direct {v0}, Lhed;-><init>()V

    sput-object v0, Lhec;->a:Ldl;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lheb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhec;->b:Ljava/lang/String;

    iput-object p2, p0, Lhec;->c:Lheb;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Linu;
    .locals 2

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-virtual {v0, p1}, Lhee;->c(Ljava/lang/String;)Linu;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhec;->c:Lheb;

    invoke-virtual {v0, p1}, Lheb;->a(Ljava/lang/String;)Linu;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final a(Lioj;Linu;Z)Lioj;
    .locals 2

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    if-eqz p3, :cond_0

    iget-object v1, p0, Lhec;->c:Lheb;

    invoke-virtual {v1, p1, p2}, Lheb;->a(Lioj;Linu;)V

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lioj;->a:Ljava/lang/String;

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0, p1}, Lhee;->a(Lioj;)V

    iget-object v1, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lhee;->a(Ljava/lang/String;Linu;)V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object p1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final a(Lipv;Z)Lipv;
    .locals 2

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lhec;->c:Lheb;

    invoke-virtual {v1, p1}, Lheb;->a(Lipv;)V

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lhee;->a(Lipv;)V

    return-object p1
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 3

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lhee;->a:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lhee;->a()V

    iget-object v1, p0, Lhec;->c:Lheb;

    invoke-virtual {v1}, Lheb;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lipv;

    invoke-virtual {v0, v1}, Lhee;->a(Lipv;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, v0, Lhee;->a:Z

    :cond_1
    invoke-virtual {v0}, Lhee;->d()Ljava/util/ArrayList;

    move-result-object v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final b(Ljava/lang/String;)Lioj;
    .locals 2

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-virtual {v0, p1}, Lhee;->a(Ljava/lang/String;)Lioj;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 3

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, v0, Lhee;->b:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lhee;->b()V

    iget-object v1, p0, Lhec;->c:Lheb;

    invoke-virtual {v1}, Lheb;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lioj;

    invoke-virtual {v0, v1}, Lhee;->a(Lioj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, v0, Lhee;->b:Z

    :cond_1
    invoke-virtual {v0}, Lhee;->c()Ljava/util/ArrayList;

    move-result-object v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final b(Lioj;Linu;Z)V
    .locals 3

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    if-eqz p3, :cond_0

    iget-object v1, p0, Lhec;->c:Lheb;

    iget-object v2, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, p2}, Lheb;->a(Ljava/lang/String;Lioj;Linu;)V

    :cond_0
    monitor-enter v0

    :try_start_0
    invoke-virtual {v0, p1}, Lhee;->a(Lioj;)V

    iget-object v1, p1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lhee;->a(Ljava/lang/String;Linu;)V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final b(Lipv;Z)V
    .locals 3

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lhec;->c:Lheb;

    iget-object v2, p1, Lipv;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lheb;->a(Ljava/lang/String;Lipv;)V

    :cond_0
    invoke-virtual {v0, p1}, Lhee;->a(Lipv;)V

    return-void
.end method

.method public final c(Ljava/lang/String;)Lipv;
    .locals 2

    sget-object v0, Lhec;->a:Ldl;

    iget-object v1, p0, Lhec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldl;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-virtual {v0, p1}, Lhee;->b(Ljava/lang/String;)Lipv;

    move-result-object v0

    return-object v0
.end method
