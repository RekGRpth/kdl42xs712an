.class public final Lhou;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhuf;


# instance fields
.field public final a:Lhov;

.field final b:Lhow;

.field private final c:Lhuf;

.field private final d:Lhuf;

.field private final e:Lhom;


# direct methods
.method public constructor <init>(ILhuf;Lhuf;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhou;-><init>(ILhuf;Lhuf;B)V

    return-void
.end method

.method private constructor <init>(ILhuf;Lhuf;B)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lhou;->c:Lhuf;

    iput-object p3, p0, Lhou;->d:Lhuf;

    new-instance v0, Lhom;

    invoke-direct {v0}, Lhom;-><init>()V

    iput-object v0, p0, Lhou;->e:Lhom;

    iput-object v2, p0, Lhou;->b:Lhow;

    new-instance v0, Lhov;

    iget-object v1, p0, Lhou;->e:Lhom;

    invoke-direct {v0, p1, v1, v2}, Lhov;-><init>(ILhom;Lhow;)V

    iput-object v0, p0, Lhou;->a:Lhov;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lhno;
    .locals 3

    iget-object v0, p0, Lhou;->a:Lhov;

    invoke-virtual {v0, p1}, Lhov;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    iget-object v2, p0, Lhou;->e:Lhom;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lhom;->a(Z)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;J)Lhno;
    .locals 3

    iget-object v0, p0, Lhou;->a:Lhov;

    invoke-virtual {v0, p1}, Lhov;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    if-eqz v0, :cond_0

    iput-wide p2, v0, Lhno;->b:J

    :cond_0
    iget-object v2, p0, Lhou;->e:Lhom;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lhom;->a(Z)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a()Livi;
    .locals 3

    iget-object v0, p0, Lhou;->e:Lhom;

    iget-object v1, p0, Lhou;->a:Lhov;

    iget v1, v1, Lhov;->a:I

    iget-object v2, p0, Lhou;->a:Lhov;

    invoke-virtual {v2}, Lhov;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lhom;->a(II)Livi;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/io/DataInput;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lhou;->b(Ljava/io/DataInput;)Lhou;

    move-result-object v0

    return-object v0
.end method

.method public final a(JJ)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhno;

    iget-wide v3, v1, Lhno;->b:J

    cmp-long v3, v3, p3

    if-gez v3, :cond_2

    iget-object v3, p0, Lhou;->e:Lhom;

    invoke-virtual {v3, v9}, Lhom;->a(I)V

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_1

    const-string v3, "TemporalCache"

    const-string v4, "Discarding %s because never seen recently."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    iget-object v3, p0, Lhou;->b:Lhow;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhou;->b:Lhow;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    iget-object v0, v1, Lhno;->d:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-wide v3, v1, Lhno;->a:J

    cmp-long v3, v3, p1

    if-gez v3, :cond_0

    iget-object v3, p0, Lhou;->e:Lhom;

    invoke-virtual {v3, v9}, Lhom;->a(I)V

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_3

    const-string v3, "TemporalCache"

    const-string v4, "Discarding %s because result too old."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    iget-object v3, p0, Lhou;->b:Lhow;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lhou;->b:Lhow;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    iget-object v0, v1, Lhno;->d:Ljava/lang/Object;

    goto :goto_0

    :cond_4
    return-void
.end method

.method public final a(Lhou;Ljava/io/DataOutput;)V
    .locals 4

    iget-object v0, p1, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->size()I

    move-result v0

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p1, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v2, p0, Lhou;->c:Lhuf;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    iget-object v2, p0, Lhou;->d:Lhuf;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, p2}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/io/DataOutput;)V
    .locals 0

    check-cast p1, Lhou;

    invoke-virtual {p0, p1, p2}, Lhou;->a(Lhou;Ljava/io/DataOutput;)V

    return-void
.end method

.method public final a(ZLjava/lang/Object;ILjava/lang/Object;J)V
    .locals 2

    iget-object v0, p0, Lhou;->a:Lhov;

    invoke-virtual {v0, p2}, Lhov;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    new-instance v0, Lhno;

    invoke-direct {v0, p3, p4, p5, p6}, Lhno;-><init>(ILjava/lang/Object;J)V

    iget-object v1, p0, Lhou;->a:Lhov;

    invoke-virtual {v1, p2, v0}, Lhov;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lhou;->e:Lhom;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lhom;->a(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position may not be null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-wide p5, v0, Lhno;->a:J

    iput-object p4, v0, Lhno;->d:Ljava/lang/Object;

    iput p3, v0, Lhno;->c:I

    goto :goto_0
.end method

.method public final b(Ljava/io/DataInput;)Lhou;
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    iget-object v1, p0, Lhou;->a:Lhov;

    invoke-virtual {v1}, Lhov;->clear()V

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lhou;->c:Lhuf;

    invoke-interface {v0, p1}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v3

    iget-object v0, p0, Lhou;->d:Lhuf;

    invoke-interface {v0, p1}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhno;

    iget-object v4, p0, Lhou;->a:Lhov;

    invoke-virtual {v4, v3, v0}, Lhov;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_0

    const-string v4, "TemporalCache"

    const-string v5, "Loaded entry, key=[%s], value=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lhou;->a:Lhov;

    invoke-virtual {v1}, Lhov;->clear()V

    throw v0

    :cond_1
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhou;->a:Lhov;

    invoke-virtual {v0}, Lhov;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
