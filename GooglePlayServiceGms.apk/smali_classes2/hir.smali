.class public final enum Lhir;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhir;

.field public static final enum b:Lhir;

.field public static final enum c:Lhir;

.field public static final enum d:Lhir;

.field public static final enum e:Lhir;

.field public static final enum f:Lhir;

.field public static final enum g:Lhir;

.field public static final enum h:Lhir;

.field public static final enum i:Lhir;

.field private static final synthetic j:[Lhir;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lhir;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->a:Lhir;

    new-instance v0, Lhir;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->b:Lhir;

    new-instance v0, Lhir;

    const-string v1, "GPS_WAIT"

    invoke-direct {v0, v1, v5}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->c:Lhir;

    new-instance v0, Lhir;

    const-string v1, "SCAN_WAIT"

    invoke-direct {v0, v1, v6}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->d:Lhir;

    new-instance v0, Lhir;

    const-string v1, "POST_SCAN_GPS_WAIT"

    invoke-direct {v0, v1, v7}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->e:Lhir;

    new-instance v0, Lhir;

    const-string v1, "UPLOAD_WAIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->f:Lhir;

    new-instance v0, Lhir;

    const-string v1, "CALIBRATION_WAIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->g:Lhir;

    new-instance v0, Lhir;

    const-string v1, "SENSOR_COLLECTION_WAIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->h:Lhir;

    new-instance v0, Lhir;

    const-string v1, "IN_OUT_DOOR_COLLECTION_WAIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lhir;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhir;->i:Lhir;

    const/16 v0, 0x9

    new-array v0, v0, [Lhir;

    sget-object v1, Lhir;->a:Lhir;

    aput-object v1, v0, v3

    sget-object v1, Lhir;->b:Lhir;

    aput-object v1, v0, v4

    sget-object v1, Lhir;->c:Lhir;

    aput-object v1, v0, v5

    sget-object v1, Lhir;->d:Lhir;

    aput-object v1, v0, v6

    sget-object v1, Lhir;->e:Lhir;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhir;->f:Lhir;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhir;->g:Lhir;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhir;->h:Lhir;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhir;->i:Lhir;

    aput-object v2, v0, v1

    sput-object v0, Lhir;->j:[Lhir;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhir;
    .locals 1

    const-class v0, Lhir;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhir;

    return-object v0
.end method

.method public static values()[Lhir;
    .locals 1

    sget-object v0, Lhir;->j:[Lhir;

    invoke-virtual {v0}, [Lhir;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhir;

    return-object v0
.end method
