.class public final Leiw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lele;

.field b:Z

.field final c:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leiw;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Leiw;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiw;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Leiw;->a:Lele;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lele;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Leiw;->d()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p8, v0, v1

    invoke-static {p1, p2, v0}, Lele;->a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Lele;

    move-result-object v0

    iput-object v0, p0, Leiw;->a:Lele;

    iget-object v0, p0, Leiw;->a:Lele;

    if-nez v0, :cond_0

    const-string v0, "Cursor for %s is null, %s"

    invoke-static {v0, p3, p2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leiw;->b:Z

    array-length v1, p4

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    aget-object v2, p4, v0

    iget-object v3, p0, Leiw;->a:Lele;

    invoke-virtual {v3, v2}, Lele;->a(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    const-string v0, "Column %s was not returned by client, refusing to index"

    invoke-static {v0, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Leiw;->c()Z

    goto :goto_0

    :cond_1
    iget-object v4, p0, Leiw;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    array-length v1, p5

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_4

    aget-object v2, p5, v0

    iget-object v3, p0, Leiw;->a:Lele;

    invoke-virtual {v3, v2}, Lele;->a(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    iget-object v4, p0, Leiw;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Leiw;->d()V

    goto :goto_0
.end method

.method final a()Z
    .locals 1

    iget-object v0, p0, Leiw;->a:Lele;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 3

    const-wide v0, 0x7fffffffffffffffL

    iget-boolean v2, p0, Leiw;->b:Z

    if-eqz v2, :cond_0

    iget-object v1, p0, Leiw;->a:Lele;

    iget-object v0, p0, Leiw;->c:Ljava/util/Map;

    const-string v2, "seqno"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lele;->c(I)J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method

.method public final c()Z
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Leiw;->b:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Leiw;->a:Lele;

    invoke-virtual {v0}, Lele;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiw;->a:Lele;

    invoke-virtual {v0}, Lele;->b()Z
    :try_end_0
    .catch Lelf; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Leiw;->d()V

    :goto_1
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Could not advance cursor"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Leiw;->b:Z

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Leiw;->b:Z

    iget-object v0, p0, Leiw;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Leiw;->a:Lele;

    if-eqz v0, :cond_0

    iget-object v1, p0, Leiw;->a:Lele;

    :try_start_0
    iget-object v0, v1, Lele;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v1, Lele;->a:Landroid/content/ContentProviderClient;

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v0, 0x0

    iput-object v0, p0, Leiw;->a:Lele;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lelf;

    invoke-direct {v2, v0}, Lelf;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, v1, Lele;->a:Landroid/content/ContentProviderClient;

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
.end method
