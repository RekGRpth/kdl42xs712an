.class public abstract Laxb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Laye;


# instance fields
.field final b:Landroid/os/Handler;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/List;

.field private e:I

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Landroid/net/ConnectivityManager;

.field private h:Laxh;

.field private final i:Landroid/net/wifi/WifiManager;

.field private j:Ljava/lang/String;

.field private volatile k:Z

.field private l:Z

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbbj;

    const-string v1, "DeviceScanner"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    sput-object v0, Laxb;->a:Laye;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Laxb;->e:I

    iput-object p1, p0, Laxb;->c:Landroid/content/Context;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laxb;->d:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Laxb;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Laxb;->g:Landroid/net/ConnectivityManager;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Laxb;->i:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic a(Laxb;)Landroid/net/ConnectivityManager;
    .locals 1

    iget-object v0, p0, Laxb;->g:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    iget v0, p0, Laxb;->e:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Laxb;->e:I

    invoke-virtual {p0}, Laxb;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v2, Laxc;

    invoke-direct {v2, p0, v0, p1}, Laxc;-><init>(Laxb;Ljava/util/List;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Laxb;)Z
    .locals 1

    iget-boolean v0, p0, Laxb;->n:Z

    return v0
.end method

.method static synthetic c(Laxb;)V
    .locals 0

    invoke-direct {p0}, Laxb;->l()V

    return-void
.end method

.method static synthetic d(Laxb;)Z
    .locals 1

    iget-boolean v0, p0, Laxb;->m:Z

    return v0
.end method

.method static synthetic e(Laxb;)V
    .locals 0

    invoke-direct {p0}, Laxb;->k()V

    return-void
.end method

.method static synthetic f(Laxb;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Laxb;->m:Z

    return v0
.end method

.method static synthetic g(Laxb;)V
    .locals 0

    invoke-direct {p0}, Laxb;->j()V

    return-void
.end method

.method static synthetic h()Laye;
    .locals 1

    sget-object v0, Laxb;->a:Laye;

    return-object v0
.end method

.method private static i()Ljava/util/List;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v3

    if-eqz v3, :cond_2

    :cond_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isLoopback()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isPointToPoint()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->supportsMulticast()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/InterfaceAddress;

    invoke-virtual {v1}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    instance-of v1, v1, Ljava/net/Inet4Address;

    if-eqz v1, :cond_1

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Laxb;->a:Laye;

    const-string v3, "Exception while selecting network interface"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v3, v4}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    return-object v2
.end method

.method private j()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "startScanInit"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v3, p0, Laxb;->n:Z

    iput-boolean v3, p0, Laxb;->k:Z

    invoke-direct {p0}, Laxb;->l()V

    invoke-static {}, Laxb;->i()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Laxb;->a(Ljava/util/List;)V

    return-void
.end method

.method private k()V
    .locals 3

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "stopScanInit"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Laxb;->k:Z

    invoke-virtual {p0}, Laxb;->c()V

    return-void
.end method

.method private l()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Laxb;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Laxb;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Laxb;->j:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Laxb;->a:Laye;

    const-string v2, "BSSID changed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Laxb;->d()V

    :cond_2
    iput-object v0, p0, Laxb;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p0, Laxb;->l:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Laxb;->l:Z

    invoke-direct {p0, v1}, Laxb;->a(I)V

    iget-object v0, p0, Laxb;->h:Laxh;

    if-nez v0, :cond_1

    new-instance v0, Laxh;

    invoke-direct {v0, p0, v3}, Laxh;-><init>(Laxb;B)V

    iput-object v0, p0, Laxb;->h:Laxh;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Laxb;->c:Landroid/content/Context;

    iget-object v2, p0, Laxb;->h:Laxh;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    invoke-direct {p0}, Laxb;->j()V

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "scan started"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Laxi;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Laxb;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Laxb;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "the same listener cannot be added twice"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Laxb;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "notifyDeviceOffline: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Laxb;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v2, Laxe;

    invoke-direct {v2, p0, v0, p1}, Laxe;-><init>(Laxb;Ljava/util/List;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method protected abstract a(Ljava/util/List;)V
.end method

.method public final b()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-boolean v0, p0, Laxb;->l:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Laxb;->h:Laxh;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Laxb;->c:Landroid/content/Context;

    iget-object v1, p0, Laxb;->h:Laxh;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iput-object v3, p0, Laxb;->h:Laxh;

    :cond_1
    invoke-direct {p0}, Laxb;->k()V

    iput-boolean v2, p0, Laxb;->m:Z

    iget-object v0, p0, Laxb;->b:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iput-boolean v2, p0, Laxb;->l:Z

    invoke-direct {p0, v2}, Laxb;->a(I)V

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "scan stopped"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected abstract c()V
.end method

.method public abstract d()V
.end method

.method e()Ljava/util/List;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Laxb;->d:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Laxb;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Laxb;->d:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final f()V
    .locals 3

    iget-boolean v0, p0, Laxb;->n:Z

    if-nez v0, :cond_0

    sget-object v0, Laxb;->a:Laye;

    const-string v1, "reportNetworkError; errorState now true"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Laxb;->n:Z

    invoke-virtual {p0}, Laxb;->d()V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Laxb;->a(I)V

    :cond_0
    return-void
.end method

.method protected final g()V
    .locals 2

    iget-object v0, p0, Laxb;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laxb;->b:Landroid/os/Handler;

    new-instance v1, Laxg;

    invoke-direct {v1, p0}, Laxg;-><init>(Laxb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method
