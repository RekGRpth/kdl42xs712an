.class final Lekc;
.super Leku;
.source "SourceFile"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Ljava/util/Set;

.field final synthetic d:Lejm;


# direct methods
.method constructor <init>(Lejm;ZZLjava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lekc;->d:Lejm;

    iput-boolean p2, p0, Lekc;->a:Z

    iput-boolean p3, p0, Lekc;->b:Z

    iput-object p4, p0, Lekc;->c:Ljava/util/Set;

    invoke-direct {p0, p1}, Leku;-><init>(Lejm;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 11

    const/4 v10, 0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    new-instance v3, Landroid/util/TimingLogger;

    const-string v0, "Icing"

    const-string v4, "post-init"

    invoke-direct {v3, v0, v4}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->b:Landroid/content/Context;

    new-array v4, v10, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, ""

    invoke-static {v6}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Lbhv;->a(Landroid/content/Context;[Ljava/lang/String;)V

    const-string v0, "gservices-cached"

    invoke-virtual {v3, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    iget-boolean v0, p0, Lekc;->a:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lekc;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v5, p0, Lekc;->d:Lejm;

    iget-object v5, v5, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-wide v6, v0, Lehh;->j:J

    iget v8, v0, Lehh;->a:I

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JI)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "Add corpus from %s failed"

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    invoke-static {v5, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b()Lehr;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v4, p0, Lekc;->d:Lejm;

    iget-object v4, v4, Lejm;->g:Leiy;

    invoke-virtual {v4, v0}, Leiy;->a(Lehr;)Z

    :cond_2
    const-string v0, "index-restored"

    invoke-virtual {v3, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->e:Lejk;

    invoke-virtual {v0}, Lejk;->d()J

    move-result-wide v4

    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->b:Landroid/content/Context;

    sget-object v6, Lejm;->a:Ljava/lang/String;

    const-wide v7, 0x90321000L

    invoke-static {v0, v6, v7, v8}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    add-long/2addr v4, v6

    cmp-long v0, v8, v4

    if-lez v0, :cond_3

    const-string v0, "No maintenance for too long"

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    iget-object v0, p0, Lekc;->d:Lejm;

    iget-object v0, v0, Lejm;->m:Leji;

    const-string v4, "no_maint_too_long"

    invoke-interface {v0, v4}, Leji;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lekc;->d:Lejm;

    invoke-virtual {v0}, Lejm;->u()V

    const-string v0, "forced-maintenance"

    invoke-virtual {v3, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lekc;->c:Ljava/util/Set;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lekc;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "%s corpora need re-polling"

    iget-object v4, p0, Lekc;->c:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v4}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lekc;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lekc;->d:Lejm;

    iget-object v5, v5, Lejm;->g:Leiy;

    invoke-virtual {v5, v0}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v6, v5, Lehj;->b:Lehk;

    iget v6, v6, Lehk;->d:I

    if-nez v6, :cond_4

    iget-object v6, p0, Lekc;->d:Lejm;

    iget-object v5, v5, Lehj;->a:Lehh;

    invoke-static {v6, v0, v5}, Lejm;->a(Lejm;Ljava/lang/String;Lehh;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lekc;->d:Lejm;

    invoke-virtual {v0, v10, v1, v2}, Lejm;->a(IJ)V

    invoke-virtual {v3}, Landroid/util/TimingLogger;->dumpToLog()V

    const-string v0, "Post-init done"

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    return-void
.end method
