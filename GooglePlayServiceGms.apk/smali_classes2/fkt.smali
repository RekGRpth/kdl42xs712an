.class public final Lfkt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "gms:playlog:logstore:debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfkt;->a:Lbfy;

    const-string v0, "gms:playlog:logstore:max_log_directory_storage_bytes"

    const-wide/32 v1, 0x200000

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkt;->b:Lbfy;

    const-string v0, "gms:playlog:logstore:target_log_file_bytes"

    const-wide/32 v1, 0xc800

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkt;->c:Lbfy;

    return-void
.end method
