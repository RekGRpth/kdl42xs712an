.class public final Lhiu;
.super Lhin;
.source "SourceFile"


# instance fields
.field g:J

.field h:F

.field i:Z

.field j:Lhiw;

.field k:Z

.field l:Ljava/util/ArrayList;

.field m:Z

.field n:Lidr;

.field o:J

.field p:Lhuv;

.field q:Z

.field r:Livi;

.field s:J

.field t:Lhuv;

.field u:J


# direct methods
.method public constructor <init>(Lidu;Lhof;Lhkl;Lhiq;)V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v7, 0x0

    const-string v1, "ActiveCollector"

    sget-object v6, Lhir;->a:Lhir;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lhin;-><init>(Ljava/lang/String;Lidu;Lhof;Lhkl;Lhiq;Lhir;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhiu;->g:J

    const/4 v0, 0x0

    iput v0, p0, Lhiu;->h:F

    iput-boolean v7, p0, Lhiu;->i:Z

    sget-object v0, Lhiw;->a:Lhiw;

    iput-object v0, p0, Lhiu;->j:Lhiw;

    iput-boolean v7, p0, Lhiu;->k:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    iput-boolean v7, p0, Lhiu;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhiu;->q:Z

    iput-wide v8, p0, Lhiu;->s:J

    iput-wide v8, p0, Lhiu;->u:J

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xa4cb80

    add-long/2addr v0, v2

    const-wide/32 v2, 0x112a880

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lhiu;->g:J

    return-void
.end method

.method private static b(Lidr;)Z
    .locals 2

    invoke-interface {p0}, Lidr;->a()F

    move-result v0

    const/high16 v1, 0x42200000    # 40.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    invoke-interface {p0}, Lidr;->e()F

    move-result v0

    const/high16 v1, 0x3fc00000    # 1.5f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    sget-object v0, Lhir;->a:Lhir;

    iput-object v0, p0, Lhiu;->f:Lhir;

    iget-object v0, p0, Lhiu;->j:Lhiw;

    sget-object v1, Lhiw;->a:Lhiw;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lhiu;->j:Lhiw;

    sget-object v1, Lhiw;->b:Lhiw;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lhiu;->b:Lidu;

    iget-object v1, p0, Lhiu;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lidu;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lhiu;->b:Lidu;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lidu;->b(I)V

    sget-object v0, Lhiw;->a:Lhiw;

    iput-object v0, p0, Lhiu;->j:Lhiw;

    iget-object v0, p0, Lhiu;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lhiu;->g:J

    :cond_1
    invoke-direct {p0}, Lhiu;->h()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhiu;->p:Lhuv;

    return-void
.end method

.method private g(J)V
    .locals 5

    iget-wide v0, p0, Lhiu;->u:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lhiu;->u:J

    iget-object v0, p0, Lhiu;->b:Lidu;

    const/4 v1, 0x1

    iget-wide v2, p0, Lhiu;->u:J

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lidu;->a(IJLilx;)V

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhiu;->a:Ljava/lang/String;

    const-string v1, "moved"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhiu;->m:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhiu;->n:Lidr;

    return-void
.end method

.method private h(J)Z
    .locals 6

    const-wide/32 v4, 0x1d4c0

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lhiv;->a:[I

    iget-object v3, p0, Lhiu;->f:Lhir;

    invoke-virtual {v3}, Lhir;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-wide v2, p0, Lhiu;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lhiu;->m:Z

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    :pswitch_2
    iget-object v2, p0, Lhiu;->b:Lidu;

    invoke-interface {v2}, Lidu;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lhiu;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    :pswitch_3
    iget-boolean v2, p0, Lhiu;->k:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lhiu;->q:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lhiu;->r:Livi;

    if-nez v2, :cond_4

    iget v3, p0, Lhiu;->h:F

    iget-boolean v2, p0, Lhiu;->i:Z

    if-eqz v2, :cond_5

    const v2, 0x3e99999a    # 0.3f

    :goto_1
    cmpl-float v2, v3, v2

    if-ltz v2, :cond_6

    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    const v2, 0x3f333333    # 0.7f

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method final a(I)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhiu;->u:J

    :cond_0
    return-void
.end method

.method final a(IIZ)V
    .locals 2

    iput-boolean p3, p0, Lhiu;->i:Z

    invoke-static {p1, p2}, Lilv;->a(II)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    iput v0, p0, Lhiu;->h:F

    :cond_0
    return-void
.end method

.method final a(Lhtf;)V
    .locals 0

    return-void
.end method

.method final a(Lidr;)V
    .locals 4

    const/16 v3, 0xa

    const/4 v2, 0x0

    sget-object v0, Lhiv;->a:[I

    iget-object v1, p0, Lhiu;->f:Lhir;

    invoke-virtual {v1}, Lhir;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {p1}, Lhiu;->b(Lidr;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lhiu;->h()V

    :goto_1
    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhiu;->m:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    invoke-static {v0, p1, v3}, Lhiu;->a(Lidr;Lidr;I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lhiu;->h()V

    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-static {p1}, Lhiu;->b(Lidr;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    invoke-static {v0, p1, v3}, Lhiu;->a(Lidr;Lidr;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-direct {p0}, Lhiu;->h()V

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lhiu;->b(Lidr;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    invoke-static {v0, p1, v3}, Lhiu;->a(Lidr;Lidr;I)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    invoke-direct {p0}, Lhiu;->h()V

    goto :goto_0

    :cond_6
    iput-object p1, p0, Lhiu;->n:Lidr;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method final a(Lids;)V
    .locals 0

    return-void
.end method

.method final a(Livi;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhiu;->r:Livi;

    return-void
.end method

.method final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lhiu;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lhiu;->k:Z

    :cond_0
    return-void
.end method

.method protected final a(J)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lhir;->b:Lhir;

    iput-object v0, p0, Lhiu;->f:Lhir;

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method final b(Lhuv;)V
    .locals 6

    iget-object v0, p0, Lhiu;->f:Lhir;

    sget-object v1, Lhir;->d:Lhir;

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x3e8

    iget-wide v2, p1, Lhuv;->a:J

    iget-wide v4, p0, Lhiu;->o:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lhiu;->b:Lidu;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lidu;->a(Lilx;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lhiu;->p:Lhuv;

    goto :goto_0
.end method

.method final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lhiu;->q:Z

    return-void
.end method

.method protected final b(J)Z
    .locals 6

    const-wide/32 v4, 0x112a880

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lhiu;->g()V

    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lhiu;->g:J

    sub-long v2, p1, v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_1

    iget-object v1, p0, Lhiu;->b:Lidu;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lidu;->a(ILilx;)V

    sget-object v1, Lhiw;->b:Lhiw;

    iput-object v1, p0, Lhiu;->j:Lhiw;

    iget-object v1, p0, Lhiu;->b:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lhiu;->g:J

    iget-object v1, p0, Lhiu;->b:Lidu;

    iget-object v2, p0, Lhiu;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lidu;->a(Ljava/lang/String;Z)V

    sget-object v1, Lhir;->c:Lhir;

    iput-object v1, p0, Lhiu;->f:Lhir;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lhiu;->g:J

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lhiu;->g(J)V

    move v0, v1

    goto :goto_0
.end method

.method final c(Z)V
    .locals 0

    return-void
.end method

.method protected final c(J)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lhiu;->g()V

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Lhiu;->m:Z

    if-eqz v2, :cond_1

    iput-wide p1, p0, Lhiu;->o:J

    iget-object v1, p0, Lhiu;->b:Lidu;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lidu;->a(Lilx;)V

    sget-object v1, Lhir;->d:Lhir;

    iput-object v1, p0, Lhiu;->f:Lhir;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lhiu;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lhiu;->g(J)V

    move v0, v1

    goto :goto_0
.end method

.method protected final d(J)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lhiu;->g()V

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lhiu;->p:Lhuv;

    if-eqz v2, :cond_1

    sget-object v1, Lhir;->e:Lhir;

    iput-object v1, p0, Lhiu;->f:Lhir;

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lhiu;->g:J

    const-wide/32 v4, 0x1d4c0

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lhiu;->g(J)V

    move v0, v1

    goto :goto_0
.end method

.method protected final e(J)Z
    .locals 9

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lhiu;->g()V

    :goto_0
    return v7

    :cond_0
    iget-object v0, p0, Lhiu;->n:Lidr;

    if-eqz v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhiu;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Success: location "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lhiu;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lidr;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " scan "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lhiu;->p:Lhuv;

    invoke-virtual {v2}, Lhuv;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lhiu;->n:Lidr;

    iget-object v4, p0, Lhiu;->p:Lhuv;

    iget-boolean v5, p0, Lhiu;->i:Z

    iget-object v0, p0, Lhiu;->b:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->c()J

    move-result-wide v0

    const/16 v6, 0xd

    invoke-static/range {v0 .. v6}, Lhiu;->a(JLidr;Lhtf;Lhuv;ZI)Livi;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->Q:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V

    iget-object v0, p0, Lhiu;->d:Lhkl;

    iget-object v2, p0, Lhiu;->b:Lidu;

    invoke-virtual {v0, v2, v1}, Lhkl;->a(Lidu;Livi;)V

    iput-object v1, p0, Lhiu;->r:Livi;

    iput-wide p1, p0, Lhiu;->s:J

    iget-object v0, p0, Lhiu;->p:Lhuv;

    iput-object v0, p0, Lhiu;->t:Lhuv;

    iget-object v0, p0, Lhiu;->b:Lidu;

    iget-object v1, p0, Lhiu;->r:Livi;

    invoke-interface {v0, v1}, Lidu;->b(Livi;)V

    iget-object v0, p0, Lhiu;->e:Lhiq;

    iget-object v1, p0, Lhiu;->n:Lidr;

    iget-object v2, p0, Lhiu;->p:Lhuv;

    invoke-virtual {v0, v1, v3, v2}, Lhiq;->a(Lidr;Lhtf;Lhuv;)V

    sget-object v0, Lhir;->f:Lhir;

    iput-object v0, p0, Lhiu;->f:Lhir;

    iget-object v0, p0, Lhiu;->b:Lidu;

    iget-object v1, p0, Lhiu;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v8}, Lidu;->a(Ljava/lang/String;Z)V

    sget-object v0, Lhiw;->c:Lhiw;

    iput-object v0, p0, Lhiu;->j:Lhiw;

    move v0, v7

    :goto_1
    move v7, v0

    goto/16 :goto_0

    :cond_2
    iget-wide v0, p0, Lhiu;->g:J

    const-wide/32 v2, 0x1d4c0

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lhiu;->g(J)V

    move v0, v8

    goto :goto_1
.end method

.method public final bridge synthetic f()V
    .locals 0

    invoke-super {p0}, Lhin;->f()V

    return-void
.end method

.method protected final f(J)Z
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lhiu;->h(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhiu;->r:Livi;

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lhiu;->g()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-wide v1, p0, Lhiu;->g:J

    const-wide/32 v3, 0x1d4c0

    add-long/2addr v1, v3

    invoke-direct {p0, v1, v2}, Lhiu;->g(J)V

    goto :goto_0
.end method
