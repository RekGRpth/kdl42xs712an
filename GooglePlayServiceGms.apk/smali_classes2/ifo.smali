.class public final Lifo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lidu;
.implements Lils;


# static fields
.field private static final n:[Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:Licp;

.field private final d:Liev;

.field private final e:Lifq;

.field private final f:Lce;

.field private final g:Z

.field private final h:Landroid/app/AlarmManager;

.field private final i:[Landroid/app/PendingIntent;

.field private final j:Life;

.field private final k:Liha;

.field private final l:Lids;

.field private final m:Licm;

.field private final o:Ljava/util/List;

.field private final p:Landroid/net/wifi/WifiManager;

.field private final q:Landroid/hardware/SensorManager;

.field private final r:Landroid/location/LocationManager;

.field private final s:Lhsu;

.field private final t:Ljava/util/concurrent/ExecutorService;

.field private final u:[B

.field private final v:J

.field private final w:Landroid/os/PowerManager;

.field private final x:Lhni;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NetworkLocationLocator"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NetworkLocationActiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "NetworkLocationBurstCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NetworkLocationPassiveCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "NetworkLocationCacheUpdater"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "NetworkLocationCalibrationCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "NetworkLocationSCollector"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "NetworkLocationSensorUploader"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "NetworkLocationActivityDetection"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "NetworkLocationInOutCollector"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "NetworkLocationBurstCollectionTrigger"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "NetworkLocationVehicleExitDetector"

    aput-object v2, v0, v1

    sput-object v0, Lifo;->n:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Licp;Lifq;Z)V
    .locals 6

    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v5, [Landroid/app/PendingIntent;

    iput-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    new-instance v0, Lifm;

    invoke-direct {v0}, Lifm;-><init>()V

    iput-object v0, p0, Lifo;->m:Licm;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lifo;->o:Ljava/util/List;

    iput-object p1, p0, Lifo;->b:Landroid/content/Context;

    iput-object p2, p0, Lifo;->c:Licp;

    iput-object p3, p0, Lifo;->e:Lifq;

    iput-boolean p4, p0, Lifo;->g:Z

    new-instance v0, Lhsu;

    invoke-direct {v0, p1, v4}, Lhsu;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lifo;->s:Lhsu;

    invoke-static {p1}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v0

    iput-object v0, p0, Lifo;->f:Lce;

    new-instance v0, Lids;

    iget-object v2, p0, Lifo;->m:Licm;

    invoke-direct {v0, v2, p0, p0}, Lids;-><init>(Licm;Lidq;Lils;)V

    iput-object v0, p0, Lifo;->l:Lids;

    iget-object v0, p0, Lifo;->l:Lids;

    invoke-virtual {v0}, Lids;->a()V

    new-instance v0, Liev;

    iget-object v2, p0, Lifo;->l:Lids;

    invoke-direct {v0, p1, v2, p2}, Liev;-><init>(Landroid/content/Context;Lids;Licp;)V

    iput-object v0, p0, Lifo;->d:Liev;

    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lifo;->h:Landroid/app/AlarmManager;

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lifo;->d:Liev;

    iget-object v3, v3, Liev;->a:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v2, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lifo;->d:Liev;

    iget-object v3, v3, Liev;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v2, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    aput-object v2, v0, v4

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x2

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->c:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x3

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x4

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->e:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x5

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->f:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x6

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->g:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/4 v2, 0x7

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/16 v2, 0x8

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->i:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/16 v2, 0x9

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->j:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/16 v2, 0xa

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lifo;->i:[Landroid/app/PendingIntent;

    const/16 v2, 0xb

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lifo;->d:Liev;

    iget-object v4, v4, Liev;->l:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v3, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    aput-object v3, v0, v2

    new-instance v0, Lifp;

    invoke-direct {v0, p0}, Lifp;-><init>(Lifo;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lifo;->t:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lhnn;

    invoke-direct {v0}, Lhnn;-><init>()V

    iput-object v0, p0, Lifo;->x:Lhni;

    new-instance v0, Liha;

    iget-object v2, p0, Lifo;->l:Lids;

    invoke-direct {v0, p0, v2}, Liha;-><init>(Lidu;Lids;)V

    iput-object v0, p0, Lifo;->k:Liha;

    new-instance v0, Life;

    iget-object v2, p0, Lifo;->l:Lids;

    iget-object v3, p0, Lifo;->k:Liha;

    iget-object v4, p0, Lifo;->d:Liev;

    invoke-direct {v0, p1, v2, v3, v4}, Life;-><init>(Landroid/content/Context;Lids;Liha;Liev;)V

    iput-object v0, p0, Lifo;->j:Life;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lifo;->w:Landroid/os/PowerManager;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lifo;->p:Landroid/net/wifi/WifiManager;

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    new-instance v1, Lhsl;

    iget-object v2, p0, Lifo;->w:Landroid/os/PowerManager;

    iget-object v3, p0, Lifo;->p:Landroid/net/wifi/WifiManager;

    sget-object v4, Lifo;->n:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-direct {v1, v2, v3, v4}, Lhsl;-><init>(Landroid/os/PowerManager;Landroid/net/wifi/WifiManager;Ljava/lang/String;)V

    iget-object v2, p0, Lifo;->o:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lifo;->q:Landroid/hardware/SensorManager;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lifo;->r:Landroid/location/LocationManager;

    invoke-direct {p0}, Lifo;->I()[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_2

    :cond_1
    invoke-direct {p0}, Lifo;->H()[B

    move-result-object v0

    :cond_2
    iput-object v0, p0, Lifo;->u:[B

    sget-object v0, Lhjv;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lifo;->v:J

    return-void
.end method

.method private H()[B
    .locals 6

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    const/16 v1, 0x20

    new-array v2, v1, [B

    invoke-virtual {v0, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lifo;->b:Landroid/content/Context;

    invoke-static {v4}, Lifo;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v0, v2}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v0}, Lilv;->a(Ljava/io/Closeable;)V

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_2
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkLocationRealOs"

    const-string v3, "Can not open key file."

    invoke-static {v1, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    invoke-static {v0}, Lilv;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_2
    :try_start_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "NetworkLocationRealOs"

    const-string v3, "Failed to write key."

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_3

    :catch_2
    move-exception v1

    move-object v1, v0

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method private I()[B
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    iget-object v4, p0, Lifo;->b:Landroid/content/Context;

    invoke-static {v4}, Lifo;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    const/16 v1, 0x20

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v3

    if-ltz v3, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_1
    :try_start_2
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkLocationRealOs"

    const-string v3, "Can not open key file."

    invoke-static {v1, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    invoke-static {v0}, Lilv;->a(Ljava/io/Closeable;)V

    :goto_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_1
    invoke-static {v0}, Lilv;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catch_1
    move-exception v0

    :goto_3
    :try_start_3
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "NetworkLocationRealOs"

    const-string v3, "Failed to read from key file."

    invoke-static {v0, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :goto_4
    invoke-static {v1}, Lilv;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_4

    :catch_2
    move-exception v1

    move-object v1, v0

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_state"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_devices"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lifo;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    invoke-static {p0}, Lifo;->f(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    invoke-static {p0}, Lifo;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lilv;->a(Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    invoke-static {}, Life;->a()V

    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete nlp state file"

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "NetworkLocationRealOs"

    const-string v2, "Unable to delete scache dir"

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    const-string v0, "cache.cell"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "cache.wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "gls.platform.key"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v0, "nlp_GlsPlatformKey"

    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    return-void
.end method

.method private static c(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ck"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static d(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_s"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_ioh"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static f(Landroid/content/Context;)Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "nlp_acd"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final A()Licp;
    .locals 1

    iget-object v0, p0, Lifo;->c:Licp;

    return-object v0
.end method

.method public final B()Lhln;
    .locals 2

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    iget-object v1, p0, Lifo;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v1, p0}, Lifs;->a(Landroid/hardware/SensorManager;Lidu;)Lhln;

    move-result-object v0

    return-object v0
.end method

.method public final C()Z
    .locals 1

    iget-object v0, p0, Lifo;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->reconnect()Z

    move-result v0

    return v0
.end method

.method public final D()Z
    .locals 3

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    iget-object v1, p0, Lifo;->p:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lifs;->a(Landroid/net/wifi/WifiManager;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final E()Liha;
    .locals 1

    iget-object v0, p0, Lifo;->k:Liha;

    return-object v0
.end method

.method public final F()Lids;
    .locals 1

    iget-object v0, p0, Lifo;->l:Lids;

    return-object v0
.end method

.method public final G()V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lifo;->d:Liev;

    iget-object v2, v1, Liev;->s:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    :goto_0
    :try_start_0
    iget-object v2, v1, Liev;->s:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_1
    iget-object v2, v1, Liev;->t:Lhsk;

    invoke-virtual {v2}, Lhsk;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Liev;->t:Lhsk;

    invoke-virtual {v2}, Lhsk;->b()V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lifo;->t:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    :try_start_1
    iget-object v1, p0, Lifo;->t:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    invoke-virtual {p0, v0}, Lifo;->a(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lifo;->a(I)V

    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lifo;->a(I)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lifo;->a(I)V

    move v1, v0

    :goto_3
    const/16 v0, 0xc

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lifo;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsl;

    invoke-virtual {v0}, Lhsl;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1}, Lifo;->b(I)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    return-void

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lhqf;Ljava/lang/String;)Lhqx;
    .locals 11

    const/4 v10, 0x0

    invoke-static {}, Life;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocationRealOs"

    const-string v1, "client ID not assigned yet."

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v10

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v0, Lhpt;

    iget-object v1, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lifo;->g()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v4

    invoke-virtual {p0}, Lifo;->i()[B

    move-result-object v5

    iget-object v2, p0, Lifo;->d:Liev;

    iget-object v2, v2, Liev;->x:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v7

    new-instance v8, Limb;

    sget-object v2, Lilz;->a:Lima;

    invoke-direct {v8, p3, v2}, Limb;-><init>(Ljava/lang/String;Lima;)V

    const/4 v9, 0x0

    move-object v2, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v9}, Lhpt;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[B[BLhqf;Landroid/os/Looper;Limb;Lilx;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_2

    const-string v1, "NetworkLocationRealOs"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move-object v0, v10

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhqq;Ljava/lang/String;Lilx;)Lhqy;
    .locals 11

    new-instance v9, Liec;

    move-object/from16 v0, p6

    invoke-direct {v9, v0, p0}, Liec;-><init>(Lhqq;Lidu;)V

    new-instance v1, Lcom/google/android/location/collectionlib/RealCollectorConfig;

    sget-object v6, Lhql;->a:Lhql;

    move-object v2, p1

    move-wide v3, p3

    move-object/from16 v5, p5

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Lilx;)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhrz;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v3, v2}, Lhqk;->a(Lhrz;I)V

    goto :goto_0

    :cond_0
    new-instance v2, Lhse;

    iget-object v3, p0, Lifo;->b:Landroid/content/Context;

    iget-object v5, p0, Lifo;->s:Lhsu;

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v10, Limb;

    sget-object v4, Lilz;->a:Lima;

    move-object/from16 v0, p7

    invoke-direct {v10, v0, v4}, Limb;-><init>(Ljava/lang/String;Lima;)V

    move-object v4, v1

    move-object v6, p0

    invoke-direct/range {v2 .. v10}, Lhse;-><init>(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;Limb;)V

    return-object v2
.end method

.method public final a(Ljava/util/Set;Ljava/util/Map;Ljava/lang/String;Ljava/lang/Integer;ZLivi;Lhqq;Ljava/lang/String;)Lhqy;
    .locals 12

    new-instance v11, Liec;

    move-object/from16 v0, p7

    invoke-direct {v11, v0, p0}, Liec;-><init>(Lhqq;Lidu;)V

    new-instance v1, Lcom/google/android/location/collectionlib/RealCollectorConfig;

    const-wide/32 v3, 0x493e0

    const/4 v5, 0x0

    sget-object v6, Lhql;->b:Lhql;

    invoke-virtual {p0}, Lifo;->i()[B

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v2, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v10}, Lcom/google/android/location/collectionlib/RealCollectorConfig;-><init>(Ljava/util/Set;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhql;Ljava/lang/String;[BZLilx;)V

    move/from16 v0, p5

    invoke-interface {v1, v0}, Lhqk;->a(Z)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhrz;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v3, v2}, Lhqk;->a(Lhrz;I)V

    goto :goto_0

    :cond_0
    new-instance v2, Lhse;

    iget-object v3, p0, Lifo;->b:Landroid/content/Context;

    iget-object v5, p0, Lifo;->s:Lhsu;

    new-instance v10, Limb;

    sget-object v4, Lilz;->a:Lima;

    move-object/from16 v0, p8

    invoke-direct {v10, v0, v4}, Limb;-><init>(Ljava/lang/String;Lima;)V

    move-object v4, v1

    move-object v6, p0

    move-object/from16 v7, p4

    move-object/from16 v8, p6

    move-object v9, v11

    invoke-direct/range {v2 .. v10}, Lhse;-><init>(Landroid/content/Context;Lhqk;Lhsu;Liea;Ljava/lang/Integer;Livi;Lhqq;Limb;)V

    return-object v2
.end method

.method public final a()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->A:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_0

    const-string v0, "location/"

    :goto_0
    :try_start_0
    iget-object v1, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "NetworkLocationRealOs"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not load asset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(I)V
    .locals 6

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licv;

    sget-object v2, Licn;->u:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Licv;-><init>(Licp;Licn;JI)V

    invoke-virtual {v1, v0, p1}, Licp;->a(Lido;I)V

    iget-object v0, p0, Lifo;->h:Landroid/app/AlarmManager;

    iget-object v1, p0, Lifo;->i:[Landroid/app/PendingIntent;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public final a(II)V
    .locals 7

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licx;

    sget-object v2, Licn;->K:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Licx;-><init>(Licp;Licn;JII)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Lifo;->e:Lifq;

    invoke-interface {v0, p1, p2}, Lifq;->a(II)V

    return-void
.end method

.method public final a(IIIZLilx;)V
    .locals 8

    iget-object v6, p0, Lifo;->d:Liev;

    const/4 v7, 0x3

    new-instance v0, Lifd;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lifd;-><init>(IIIZLilx;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v7, v0, v1}, Liev;->a(ILjava/lang/Object;Z)Z

    return-void
.end method

.method public final a(IIZZLilx;)V
    .locals 7

    iget-object v6, p0, Lifo;->d:Liev;

    new-instance v0, Liew;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Liew;-><init>(IIZZLilx;)V

    const/16 v1, 0x21

    const/4 v2, 0x1

    invoke-virtual {v6, v1, v0, v2}, Liev;->a(ILjava/lang/Object;Z)Z

    return-void
.end method

.method public final a(IJLilx;)V
    .locals 8

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licu;

    sget-object v2, Licn;->t:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v0 .. v7}, Licu;-><init>(Licp;Licn;JIJ)V

    invoke-virtual {v1, v0, p1}, Licp;->a(Lido;I)V

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    iget-object v1, p0, Lifo;->h:Landroid/app/AlarmManager;

    iget-object v2, p0, Lifo;->i:[Landroid/app/PendingIntent;

    aget-object v4, v2, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lifs;->a(Landroid/app/AlarmManager;JLandroid/app/PendingIntent;Lilx;)V

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x28

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Liev;->a(ILjava/lang/Object;Z)Z

    return-void
.end method

.method public final a(ILilx;)V
    .locals 6

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licz;

    sget-object v2, Licn;->M:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Licz;-><init>(Licp;Licn;JI)V

    invoke-virtual {v1, v0, p1}, Licp;->a(Lido;I)V

    iget-object v0, p0, Lifo;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsk;

    iget-object v1, p0, Lifo;->b:Landroid/content/Context;

    const-string v2, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, Lhsk;->a(Lilx;)V

    :cond_0
    invoke-virtual {v0}, Lhsk;->a()V

    return-void
.end method

.method public final a(J)V
    .locals 1

    iget-object v0, p0, Lifo;->x:Lhni;

    invoke-virtual {v0, p1, p2}, Lhni;->a(J)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 3

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x20

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Liev;->a(ILjava/lang/Object;Z)Z

    iget-object v0, p0, Lifo;->e:Lifq;

    invoke-interface {v0, p1}, Lifq;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    return-void
.end method

.method public final a(Lhlt;)V
    .locals 3

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x22

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Liev;->a(ILjava/lang/Object;Z)Z

    return-void
.end method

.method public final a(Lhud;Lhup;)V
    .locals 6

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licw;

    sget-object v2, Licn;->J:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Licw;-><init>(Licp;Licn;JLhud;)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lhud;->a:Lhtw;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhud;->a:Lhtw;

    iget-object v0, v0, Lhtw;->c:Lhug;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhud;->a:Lhtw;

    iget-object v0, v0, Lhtw;->d:Lhty;

    sget-object v1, Lhty;->a:Lhty;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lifo;->e:Lifq;

    invoke-interface {v0, p1, p2}, Lifq;->a(Lhud;Lhup;)V

    goto :goto_0
.end method

.method public final a(Lick;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lifo;->d:Liev;

    iput-object p1, v0, Liev;->z:Lick;

    new-instance v2, Lifc;

    invoke-direct {v2, v0, v1}, Lifc;-><init>(Liev;B)V

    new-instance v3, Ljava/lang/Thread;

    const/4 v4, 0x0

    const-string v5, "NetworkLocationCallbackRunner"

    invoke-direct {v3, v4, v2, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v3, v0, Liev;->s:Ljava/lang/Thread;

    iget-object v0, v0, Liev;->s:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    monitor-enter v2

    :goto_0
    :try_start_0
    iget-boolean v0, v2, Lifc;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v2, p0, Lifo;->d:Liev;

    iget-boolean v0, p0, Lifo;->g:Z

    const/16 v3, 0x1a

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, Liev;->a(IIZ)Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lilx;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->O:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    iget-object v1, p0, Lifo;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1, p1}, Lifs;->a(Landroid/net/wifi/WifiManager;Lilx;)V

    return-void
.end method

.method public final a(Livi;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->x:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->j:Life;

    invoke-virtual {v0, p1}, Life;->a(Livi;)V

    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->B:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    invoke-virtual {v0, p1}, Lifs;->a(Ljava/io/File;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lids;

    iget-object v0, p0, Lifo;->d:Liev;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x1e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Liev;->a(ILjava/lang/Object;Z)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 7

    iget-object v0, p0, Lifo;->x:Lhni;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lhni;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Licy;

    sget-object v2, Licn;->H:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Licy;-><init>(Licp;Licn;JZ)V

    invoke-virtual {v1, v0}, Licp;->a(Lido;)V

    iget-object v0, p0, Lifo;->d:Liev;

    iget-object v1, p0, Lifo;->s:Lhsu;

    iget-boolean v2, v0, Liev;->A:Z

    if-eq v2, p2, :cond_0

    iput-boolean p2, v0, Liev;->A:Z

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eqz p2, :cond_1

    const/4 v3, 0x0

    iget-object v4, v0, Liev;->o:Lifa;

    invoke-virtual {v1, p1, v3, v4}, Lhsu;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v3, "gps"

    iget-object v0, v0, Liev;->p:Lifa;

    invoke-virtual {v1, p1, v3, v0, v2}, Lhsu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v3, 0x1

    iget-object v4, v0, Liev;->p:Lifa;

    invoke-virtual {v1, p1, v3, v4}, Lhsu;->a(Ljava/lang/String;ZLandroid/location/LocationListener;)V

    const-string v3, "passive"

    iget-object v0, v0, Liev;->o:Lifa;

    invoke-virtual {v1, p1, v3, v0, v2}, Lhsu;->a(Ljava/lang/String;Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    goto :goto_0
.end method

.method public final a(Ljava/text/Format;Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v0, p0, Lifo;->s:Lhsu;

    iget-object v1, p0, Lifo;->m:Licm;

    invoke-interface {v1}, Licm;->c()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2, p2}, Lhsu;->a(Ljava/text/Format;JLjava/io/PrintWriter;)V

    return-void
.end method

.method public final a(Z)V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lifo;->d:Liev;

    iget-object v3, v2, Liev;->v:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v4, v2, Liev;->w:Z

    if-eqz v4, :cond_0

    monitor-exit v3

    :goto_0
    return-void

    :cond_0
    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_1

    const-string v4, "NetworkLocationCallbackRunner"

    const-string v5, "quit"

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v4, v2, Liev;->y:Liex;

    if-eqz v4, :cond_2

    iget-object v4, v2, Liev;->m:Landroid/content/Context;

    iget-object v5, v2, Liev;->y:Liex;

    invoke-virtual {v4, v5}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object v4, v2, Liev;->x:Landroid/os/Handler;

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    if-eqz p1, :cond_4

    :goto_1
    const/4 v1, 0x0

    invoke-virtual {v2, v4, v0, v1}, Liev;->a(IIZ)Z

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, v2, Liev;->w:Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2, p2}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Liev;->a(ILjava/lang/Object;Z)Z

    return-void
.end method

.method public final a(Lhrz;)Z
    .locals 2

    sget-object v0, Lcom/google/android/location/collectionlib/RealCollectorConfig;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lifo;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lift;->a(Landroid/hardware/SensorManager;I)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lidw;)Z
    .locals 4

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x24

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lhue;->a(Ljava/lang/Object;Ljava/lang/Object;)Lhue;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Liev;->a(ILjava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .locals 3

    iget-object v0, p0, Lifo;->d:Liev;

    const/16 v1, 0x25

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Liev;->a(ILjava/lang/Object;Z)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->C:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 6

    iget-object v1, p0, Lifo;->c:Licp;

    new-instance v0, Lida;

    sget-object v2, Licn;->N:Licn;

    iget-object v3, v1, Licp;->a:Lidp;

    invoke-interface {v3}, Lidp;->a()J

    move-result-wide v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lida;-><init>(Licp;Licn;JI)V

    invoke-virtual {v1, v0, p1}, Licp;->a(Lido;I)V

    iget-object v0, p0, Lifo;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsk;

    invoke-virtual {v0}, Lhsk;->b()V

    return-void
.end method

.method public final b(Livi;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->y:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->j:Life;

    invoke-virtual {v0, p1}, Life;->b(Livi;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lifo;->f:Lce;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lce;->a(Landroid/content/Intent;)Z

    return-void
.end method

.method public final c()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->D:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final c(Livi;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->z:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->j:Life;

    invoke-virtual {v0, p1}, Life;->c(Livi;)V

    return-void
.end method

.method public final d()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->E:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final d(Livi;)V
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->w:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->j:Life;

    invoke-virtual {v0, p1}, Life;->d(Livi;)V

    return-void
.end method

.method public final e()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->F:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->v:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-object v0, p0, Lifo;->d:Liev;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2, v2}, Liev;->a(IIZ)Z

    return-void
.end method

.method public final g()Ljavax/crypto/SecretKey;
    .locals 12
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v11, 0x18

    const/16 v10, 0x10

    const/16 v9, 0x8

    const/4 v8, 0x0

    const-wide/16 v6, 0xff

    iget-object v0, p0, Lifo;->c:Licp;

    sget-object v1, Licn;->G:Licn;

    invoke-virtual {v0, v1}, Licp;->a(Licn;)V

    iget-wide v0, p0, Lifo;->v:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "no android ID; can\'t access encrypted cache"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v2, 0x20

    new-array v2, v2, [B

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v8

    const/4 v3, 0x1

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x4

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x5

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x6

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x7

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v9

    const/16 v3, 0x9

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xa

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xb

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xc

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xd

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xe

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0xf

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v10

    const/16 v3, 0x11

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x12

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x13

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x14

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x15

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x16

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x17

    ushr-long v4, v0, v8

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x38

    ushr-long v3, v0, v3

    and-long/2addr v3, v6

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v11

    const/16 v3, 0x19

    const/16 v4, 0x30

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1a

    const/16 v4, 0x28

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1b

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1c

    ushr-long v4, v0, v11

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1d

    ushr-long v4, v0, v10

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1e

    ushr-long v4, v0, v9

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    const/16 v3, 0x1f

    ushr-long/2addr v0, v8

    and-long/2addr v0, v6

    long-to-int v0, v0

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, v2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public final h()Ljavax/crypto/SecretKey;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lifo;->g()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "NetworkLocationRealOs"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()[B
    .locals 3

    iget-object v0, p0, Lifo;->u:[B

    iget-object v1, p0, Lifo;->u:[B

    array-length v1, v1

    if-gez v1, :cond_0

    new-instance v0, Ljava/lang/NegativeArraySizeException;

    invoke-direct {v0}, Ljava/lang/NegativeArraySizeException;-><init>()V

    throw v0

    :cond_0
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lilv;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public final j()Licm;
    .locals 1

    iget-object v0, p0, Lifo;->m:Licm;

    return-object v0
.end method

.method public final k()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    :try_start_0
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "NetworkLocationRealOs"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "can\'t check GPS "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final l()Ljava/io/InputStream;
    .locals 3

    new-instance v0, Ljava/io/BufferedInputStream;

    iget-object v1, p0, Lifo;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.google.android.gms.R.raw.nlp_metricmodel

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public final m()Lidr;
    .locals 1

    iget-object v0, p0, Lifo;->e:Lifq;

    invoke-interface {v0}, Lifq;->d()Lidr;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lidv;
    .locals 5

    new-instance v0, Lidv;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-direct {v0, v1, v2, v3, v4}, Lidv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final o()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Lifo;->t:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()Z
    .locals 2

    iget-object v0, p0, Lifo;->q:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lifo;->r:Landroid/location/LocationManager;

    invoke-static {v0, v1}, Lift;->a(Landroid/hardware/SensorManager;Landroid/location/LocationManager;)Z

    move-result v0

    return v0
.end method

.method public final s()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-static {v0}, Lifo;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-static {v0}, Lifo;->e(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    invoke-static {v0}, Lifo;->f(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final v()I
    .locals 1

    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    move-result v0

    return v0
.end method

.method public final w()I
    .locals 4

    new-instance v1, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v1}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    iget-object v0, p0, Lifo;->b:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    iget-wide v0, v1, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    const-wide/16 v2, 0x400

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final x()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lifo;->q:Landroid/hardware/SensorManager;

    invoke-virtual {v1, v0}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()J
    .locals 2

    iget-wide v0, p0, Lifo;->v:J

    return-wide v0
.end method

.method public final z()Z
    .locals 1

    invoke-static {}, Lifs;->a()Lifs;

    move-result-object v0

    invoke-virtual {v0}, Lifs;->d()Z

    move-result v0

    return v0
.end method
