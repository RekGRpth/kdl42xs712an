.class public final Lhol;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Livi;

.field public b:J


# direct methods
.method public constructor <init>(Livi;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhol;->b:J

    iput-object p1, p0, Lhol;->a:Livi;

    return-void
.end method

.method static a(JJJJ)J
    .locals 2

    add-long v0, p2, p0

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static b(J)I
    .locals 6

    long-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v0, v2

    const-wide v2, 0x4194997000000000L    # 8.64E7

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v2, v0

    sub-long/2addr v2, p0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    const-wide/32 v4, 0x51d3440

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    :cond_0
    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a()F
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lhol;->a:Livi;

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhol;->a:Livi;

    invoke-virtual {v0, v1}, Livi;->e(I)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public final a(F)V
    .locals 2

    iget-object v0, p0, Lhol;->a:Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Livi;->a(IF)Livi;

    return-void
.end method

.method final a(J)V
    .locals 3

    iget-object v0, p0, Lhol;->a:Livi;

    const/4 v1, 0x2

    invoke-static {p1, p2}, Lhol;->b(J)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Livi;->e(II)Livi;

    return-void
.end method
