.class final Lgpu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfar;


# instance fields
.field final synthetic a:Lgpr;


# direct methods
.method private constructor <init>(Lgpr;)V
    .locals 0

    iput-object p1, p0, Lgpu;->a:Lgpr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lgpr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgpu;-><init>(Lgpr;)V

    return-void
.end method

.method private a()V
    .locals 7

    const/4 v4, 0x1

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->t(Lgpr;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpu;->a:Lgpr;

    invoke-static {v3}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfaj;->a(Lfao;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpu;->a:Lgpr;

    iget-object v1, p0, Lgpu;->a:Lgpr;

    invoke-static {v1}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgpr;->b(Lgpr;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->u(Lgpr;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpu;->a:Lgpr;

    invoke-static {v3}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lfaj;->a(Lfaw;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    invoke-interface {v0}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpu;->a:Lgpr;

    invoke-static {v3}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lfaj;->a(Lfaq;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lgpu;->a:Lgpr;

    sget-object v1, Lbbo;->a:Lbbo;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->t(Lgpr;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lgpr;->a(Lgpr;Lbbo;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->v(Lgpr;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->u(Lgpr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4, v4}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    invoke-interface {v0}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    sget-object v1, Lbbo;->a:Lbbo;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v2

    invoke-interface {v2}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    goto :goto_2
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->u(Lgpr;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->v(Lgpr;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->a(Lgpr;)Lgpw;

    move-result-object v0

    invoke-interface {v0}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_1
    return-void
.end method

.method public final Q_()V
    .locals 0

    invoke-direct {p0}, Lgpu;->a()V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 5

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    invoke-static {v1}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, p0, v1, v2, v3}, Lfaj;->a(Lfar;Ljava/lang/String;Ljava/lang/String;I)Z

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    invoke-static {v1}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpu;->a:Lgpr;

    invoke-static {v3}, Lgpr;->m(Lgpr;)J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lfaj;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    invoke-direct {p0}, Lgpu;->a()V

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->n(Lgpr;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->o(Lgpr;)Z

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->h(Lgpr;)Lfaj;

    move-result-object v0

    iget-object v1, p0, Lgpu;->a:Lgpr;

    iget-object v2, p0, Lgpu;->a:Lgpr;

    invoke-static {v2}, Lgpr;->d(Lgpr;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpu;->a:Lgpr;

    invoke-static {v3}, Lgpr;->c(Lgpr;)Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgpu;->a:Lgpr;

    invoke-static {v4}, Lgpr;->p(Lgpr;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lfaj;->a(Lfam;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->q(Lgpr;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->r(Lgpr;)Z

    iget-object v0, p0, Lgpu;->a:Lgpr;

    invoke-static {v0}, Lgpr;->s(Lgpr;)V

    :cond_1
    return-void
.end method
