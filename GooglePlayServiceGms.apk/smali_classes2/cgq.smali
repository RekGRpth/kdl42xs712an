.class public final Lcgq;
.super Lcgp;
.source "SourceFile"


# instance fields
.field private final a:Lcdu;

.field private b:Landroid/database/Cursor;

.field private final c:Lcgr;


# direct methods
.method public constructor <init>(Lcdu;Landroid/database/Cursor;Lcgr;)V
    .locals 1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcgp;-><init>(I)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lcgq;->a:Lcdu;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcgq;->b:Landroid/database/Cursor;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgr;

    iput-object v0, p0, Lcgq;->c:Lcgr;

    return-void
.end method


# virtual methods
.method protected final a(I)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcgq;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to move delegate cursor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcgq;->c:Lcgr;

    iget-object v1, p0, Lcgq;->a:Lcdu;

    iget-object v2, p0, Lcgq;->b:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2}, Lcgr;->a(Lcdu;Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "Result decoder returned null."

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 1

    iget-object v0, p0, Lcgq;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcgq;->b:Landroid/database/Cursor;

    return-void
.end method
