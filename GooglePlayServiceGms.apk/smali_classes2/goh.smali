.class public final Lgoh;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lgoh;->a:Landroid/content/Context;

    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgoh;->b:Landroid/view/LayoutInflater;

    if-nez p2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    :goto_0
    return-void

    :cond_0
    iput-object p2, p0, Lgoh;->c:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private a(I)Lcom/google/android/gms/plus/sharebox/Circle;
    .locals 2

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->a:Lcom/google/android/gms/plus/sharebox/Circle;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lgoh;->b()Z

    move-result v0

    if-nez v0, :cond_4

    if-nez p1, :cond_2

    sget-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->a:Lcom/google/android/gms/plus/sharebox/Circle;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p1, v0, :cond_3

    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, p1, :cond_5

    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/plus/sharebox/Circle;Z)V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, -0x2

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    if-eqz p3, :cond_4

    const v0, 0x7f0a02bd    # com.google.android.gms.R.id.dropdown_item_text

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a02d3    # com.google.android.gms.R.id.dropdown_item_icon

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0, p2}, Lgoh;->b(Lcom/google/android/gms/plus/sharebox/Circle;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lgoh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020208    # com.google.android.gms.R.drawable.plus_iconic_ic_add_to_circles_darkgrey_16

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lgoh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v3, v4

    add-float/2addr v3, v6

    float-to-int v3, v3

    const/16 v4, 0x190

    if-gt v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x1e

    if-le v1, v4, :cond_3

    const/16 v1, 0x3c

    if-lt v3, v1, :cond_3

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    add-int/lit8 v3, v3, -0x3c

    int-to-float v3, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    add-float/2addr v2, v6

    float-to-int v2, v2

    invoke-direct {v1, v2, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    :goto_1
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->d()I

    move-result v1

    if-gtz v1, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lgoh;->a:Landroid/content/Context;

    const v3, 0x7f0b03b7    # com.google.android.gms.R.string.plus_sharebox_circles_option

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, p2}, Lgoh;->b(Lcom/google/android/gms/plus/sharebox/Circle;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private b()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Lgof;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private b(Lcom/google/android/gms/plus/sharebox/Circle;)Z
    .locals 1

    iget-object v0, p0, Lgoh;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()I
    .locals 1

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgoh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 1

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lgoh;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgoh;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lgoh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    check-cast p2, Landroid/widget/LinearLayout;

    :goto_0
    invoke-direct {p0, p1}, Lgoh;->a(I)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, Lgoh;->a(Landroid/view/View;Lcom/google/android/gms/plus/sharebox/Circle;Z)V

    return-object p2

    :cond_0
    iget-object v0, p0, Lgoh;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f04010f    # com.google.android.gms.R.layout.plus_sharebox_spinner_dropdown_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lgoh;->a(I)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    check-cast p2, Landroid/widget/TextView;

    :goto_0
    invoke-direct {p0, p1}, Lgoh;->a(I)Lcom/google/android/gms/plus/sharebox/Circle;

    move-result-object v0

    invoke-direct {p0, p2, v0, v2}, Lgoh;->a(Landroid/view/View;Lcom/google/android/gms/plus/sharebox/Circle;Z)V

    return-object p2

    :cond_0
    iget-object v0, p0, Lgoh;->b:Landroid/view/LayoutInflater;

    const v1, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    move-object p2, v0

    goto :goto_0
.end method
