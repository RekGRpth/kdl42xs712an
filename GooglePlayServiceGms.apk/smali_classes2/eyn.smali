.class public final Leyn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field final b:I

.field c:Ljava/nio/FloatBuffer;

.field d:Ljava/nio/FloatBuffer;

.field e:Ljava/nio/ShortBuffer;

.field f:[Leyk;

.field g:Z

.field private final h:I

.field private i:I


# direct methods
.method public constructor <init>(II)V
    .locals 10

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Leyn;->i:I

    iput-boolean v1, p0, Leyn;->g:Z

    iput p1, p0, Leyn;->a:I

    add-int/lit8 v4, p2, 0x1

    mul-int v0, v4, v4

    iput v0, p0, Leyn;->h:I

    add-int/lit8 v0, v4, -0x1

    add-int/lit8 v2, v4, -0x1

    mul-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Leyn;->b:I

    iget v0, p0, Leyn;->h:I

    mul-int/lit8 v0, v0, 0x2

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Leyn;->d:Ljava/nio/FloatBuffer;

    iget v0, p0, Leyn;->b:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    new-array v0, v1, [Leyk;

    iput-object v0, p0, Leyn;->f:[Leyk;

    move v3, v1

    move v0, v1

    :goto_0
    add-int/lit8 v2, v4, -0x1

    if-ge v3, v2, :cond_1

    mul-int v5, v3, v4

    move v2, v0

    move v0, v1

    :goto_1
    add-int/lit8 v6, v4, -0x1

    if-ge v0, v6, :cond_0

    add-int v6, v5, v0

    iget-object v7, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v8, v2, 0x1

    add-int/lit8 v9, v6, 0x0

    int-to-short v9, v9

    invoke-virtual {v7, v2, v9}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v2, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v7, v8, 0x1

    add-int/lit8 v9, v6, 0x1

    int-to-short v9, v9

    invoke-virtual {v2, v8, v9}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v2, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v8, v7, 0x1

    add-int v9, v6, v4

    int-to-short v9, v9

    invoke-virtual {v2, v7, v9}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v2, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v7, v8, 0x1

    add-int v9, v6, v4

    int-to-short v9, v9

    invoke-virtual {v2, v8, v9}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v2, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v8, v7, 0x1

    add-int/lit8 v9, v6, 0x1

    int-to-short v9, v9

    invoke-virtual {v2, v7, v9}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    iget-object v7, p0, Leyn;->e:Ljava/nio/ShortBuffer;

    add-int/lit8 v2, v8, 0x1

    add-int/2addr v6, v4

    add-int/lit8 v6, v6, 0x1

    int-to-short v6, v6

    invoke-virtual {v7, v8, v6}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v1

    move v0, v1

    :goto_2
    if-ge v3, v4, :cond_3

    move v2, v0

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_2

    int-to-float v5, v0

    add-int/lit8 v6, v4, -0x1

    int-to-float v6, v6

    div-float/2addr v5, v6

    const/high16 v6, 0x3f800000    # 1.0f

    int-to-float v7, v3

    add-int/lit8 v8, v4, -0x1

    int-to-float v8, v8

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget-object v7, p0, Leyn;->d:Ljava/nio/FloatBuffer;

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v7, v2, v5}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v5, p0, Leyn;->d:Ljava/nio/FloatBuffer;

    add-int/lit8 v2, v8, 0x1

    invoke-virtual {v5, v8, v6}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    :cond_3
    return-void
.end method


# virtual methods
.method public final a([Lezc;)V
    .locals 6

    const/4 v0, 0x0

    iget v1, p0, Leyn;->h:I

    mul-int/lit8 v1, v1, 0x3

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Leyn;->c:Ljava/nio/FloatBuffer;

    iput v0, p0, Leyn;->i:I

    array-length v1, p1

    new-array v1, v1, [Leyk;

    iput-object v1, p0, Leyn;->f:[Leyk;

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    aget-object v1, p1, v0

    iget-object v2, p0, Leyn;->c:Ljava/nio/FloatBuffer;

    iget v3, p0, Leyn;->i:I

    iget v4, v1, Lezc;->a:F

    invoke-virtual {v2, v3, v4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v4, v3, 0x1

    iget v5, v1, Lezc;->b:F

    invoke-virtual {v2, v4, v5}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    add-int/lit8 v3, v3, 0x2

    iget v4, v1, Lezc;->c:F

    invoke-virtual {v2, v3, v4}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget v2, p0, Leyn;->i:I

    add-int/lit8 v2, v2, 0x3

    iput v2, p0, Leyn;->i:I

    new-instance v2, Leyk;

    iget v3, v1, Lezc;->a:F

    iget v4, v1, Lezc;->b:F

    iget v1, v1, Lezc;->c:F

    invoke-direct {v2, v3, v4, v1}, Leyk;-><init>(FFF)V

    invoke-virtual {v2}, Leyk;->a()F

    iget-object v1, p0, Leyn;->f:[Leyk;

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
