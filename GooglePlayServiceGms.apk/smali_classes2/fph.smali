.class public final Lfph;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lfpi;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Ljava/util/ArrayList;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfpi;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfph;->b:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lfph;->a:Lfpi;

    return-void
.end method


# virtual methods
.method public final a(I)Lgkf;
    .locals 1

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkf;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    invoke-virtual {p0}, Lfph;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 5

    iput-boolean p2, p0, Lfph;->d:Z

    iget-object v0, p0, Lfph;->a:Lfpi;

    invoke-interface {v0}, Lfpi;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkf;

    invoke-interface {v0}, Lgkf;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Lgkf;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lfph;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lfph;->a:Lfpi;

    invoke-interface {v1}, Lfpi;->d()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v1, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkf;

    invoke-interface {v0}, Lgkf;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lfph;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfph;->d:Z

    invoke-virtual {p0}, Lfph;->notifyDataSetChanged()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfph;->d:Z

    invoke-virtual {p0}, Lfph;->notifyDataSetInvalidated()V

    return-void
.end method

.method public final getCount()I
    .locals 2

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-boolean v0, p0, Lfph;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lfph;->a(I)Lgkf;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_6

    if-nez p2, :cond_0

    iget-object v0, p0, Lfph;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400d9    # com.google.android.gms.R.layout.plus_list_moments_item

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lfph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkf;

    invoke-interface {v0}, Lgkf;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v2

    const v1, 0x7f0a005d    # com.google.android.gms.R.id.icon

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->d()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v4, p0, Lfph;->a:Lfpi;

    invoke-interface {v4, v2}, Lfpi;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    const v1, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lgkf;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {v0}, Lgkf;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    :try_start_0
    invoke-interface {v0}, Lgkf;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfol;->a(Ljava/lang/String;)Lfol;

    move-result-object v1

    invoke-virtual {v1}, Lfol;->a()J

    move-result-wide v1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1, v2}, Lfom;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    invoke-interface {v0}, Lgkf;->b()Lgge;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Lgge;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    if-nez v1, :cond_5

    move-object v1, v0

    :cond_1
    :goto_3
    const v0, 0x7f0a027e    # com.google.android.gms.R.id.time_acl

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    return-object p2

    :cond_2
    move-object v2, v3

    goto :goto_0

    :catch_0
    move-exception v1

    :cond_3
    move-object v1, v3

    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0386    # com.google.android.gms.R.string.plus_list_moments_time_acl_format

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_6
    if-nez p2, :cond_7

    iget-object v0, p0, Lfph;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400d8    # com.google.android.gms.R.layout.plus_list_apps_item_loading

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_7
    iget-object v0, p0, Lfph;->a:Lfpi;

    invoke-interface {v0}, Lfpi;->e()V

    goto :goto_4
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v1, p0, Lfph;->a:Lfpi;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkf;

    invoke-interface {v1, v0}, Lfpi;->a(Lgkf;)V

    return-void
.end method
