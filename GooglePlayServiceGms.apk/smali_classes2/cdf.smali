.class public final enum Lcdf;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcdf;

.field public static final enum b:Lcdf;

.field public static final enum c:Lcdf;

.field public static final enum d:Lcdf;

.field public static final enum e:Lcdf;

.field private static final synthetic g:[Lcdf;


# instance fields
.field private final f:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v0, Lcdf;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcde;->d()Lcde;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "accountId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-static {}, Lcdc;->a()Lcdc;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcdf;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdf;->a:Lcdf;

    new-instance v0, Lcdf;

    const-string v1, "PACKAGING_ID"

    invoke-static {}, Lcde;->d()Lcde;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "packagingId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcdf;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdf;->b:Lcdf;

    new-instance v0, Lcdf;

    const-string v1, "EXPIRY_TIMESTAMP"

    invoke-static {}, Lcde;->d()Lcde;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "expiryTimestamp"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcdf;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdf;->c:Lcdf;

    new-instance v0, Lcdf;

    const-string v1, "PACKAGE_NAME"

    invoke-static {}, Lcde;->d()Lcde;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "packageName"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcdf;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdf;->d:Lcdf;

    new-instance v0, Lcdf;

    const-string v1, "CERTIFICATE_HASH"

    invoke-static {}, Lcde;->d()Lcde;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "certificateHash"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v6, v3, Lcei;->g:Z

    invoke-virtual {v3}, Lcei;->a()Lcei;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/String;

    sget-object v5, Lcdf;->a:Lcdf;

    iget-object v5, v5, Lcdf;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Lcdf;->d:Lcdf;

    iget-object v5, v5, Lcdf;->f:Lcdp;

    invoke-virtual {v5}, Lcdp;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcei;->a([Ljava/lang/String;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcdf;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcdf;->e:Lcdf;

    const/4 v0, 0x5

    new-array v0, v0, [Lcdf;

    sget-object v1, Lcdf;->a:Lcdf;

    aput-object v1, v0, v7

    sget-object v1, Lcdf;->b:Lcdf;

    aput-object v1, v0, v6

    sget-object v1, Lcdf;->c:Lcdf;

    aput-object v1, v0, v8

    sget-object v1, Lcdf;->d:Lcdf;

    aput-object v1, v0, v9

    sget-object v1, Lcdf;->e:Lcdf;

    aput-object v1, v0, v10

    sput-object v0, Lcdf;->g:[Lcdf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcdf;->f:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcdf;
    .locals 1

    const-class v0, Lcdf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcdf;

    return-object v0
.end method

.method public static values()[Lcdf;
    .locals 1

    sget-object v0, Lcdf;->g:[Lcdf;

    invoke-virtual {v0}, [Lcdf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcdf;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcdf;->f:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcdf;->f:Lcdp;

    return-object v0
.end method
