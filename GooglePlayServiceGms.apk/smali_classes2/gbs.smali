.class public final Lgbs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lftb;

.field private final f:[Ljava/lang/String;

.field private final g:Lgau;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lftb;[Ljava/lang/String;Lgau;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbs;->a:Ljava/lang/String;

    iput-object p2, p0, Lgbs;->b:Ljava/lang/String;

    iput p3, p0, Lgbs;->c:I

    iput-object p4, p0, Lgbs;->d:Ljava/lang/String;

    iput-object p5, p0, Lgbs;->e:Lftb;

    iput-object p6, p0, Lgbs;->f:[Ljava/lang/String;

    iput-object p7, p0, Lgbs;->g:Lgau;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    :try_start_0
    iget-object v0, p0, Lgbs;->g:Lgau;

    iget-object v2, p0, Lgbs;->b:Ljava/lang/String;

    iget v3, p0, Lgbs;->c:I

    iget-object v4, p0, Lgbs;->d:Ljava/lang/String;

    iget-object v5, p0, Lgbs;->a:Ljava/lang/String;

    iget-object v6, p0, Lgbs;->f:[Ljava/lang/String;

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lgau;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lbbo;

    iget-object v0, p0, Lgbs;->e:Lftb;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgbs;->e:Lftb;

    invoke-interface {v0, v9, v1, v7}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "NetworkError"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbs;->e:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v7, v7}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lgbs;->e:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v7, v7}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgbs;->e:Lftb;

    invoke-interface {v0, v9, v7, v7}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgbs;->e:Lftb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbs;->e:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
