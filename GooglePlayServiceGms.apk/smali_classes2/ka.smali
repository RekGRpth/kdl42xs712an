.class final Lka;
.super Ljy;
.source "SourceFile"

# interfaces
.implements Lnj;


# instance fields
.field final d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

.field e:Landroid/view/ActionMode;


# direct methods
.method public constructor <init>(Ljp;Ljk;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljy;-><init>(Ljp;Ljk;)V

    sget v0, Lkl;->e:I

    invoke-virtual {p1, v0}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    iput-object v0, p0, Lka;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    iget-object v0, p0, Lka;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lka;->d:Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/widget/NativeActionModeAwareLayout;->a(Lnj;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;
    .locals 1

    new-instance v0, Lkb;

    invoke-direct {v0, p0, p1}, Lkb;-><init>(Lka;Landroid/view/ActionMode$Callback;)V

    return-object v0
.end method

.method public final c()V
    .locals 1

    invoke-super {p0}, Ljy;->c()V

    iget-object v0, p0, Lka;->e:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lka;->e:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    return-void
.end method

.method final f()Z
    .locals 1

    iget-object v0, p0, Lka;->e:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    invoke-super {p0}, Ljy;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
