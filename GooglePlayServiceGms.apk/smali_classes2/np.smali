.class final Lnp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lnt;


# instance fields
.field final synthetic a:Lno;

.field private b:Landroid/app/AlertDialog;

.field private c:Landroid/widget/ListAdapter;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lno;)V
    .locals 0

    iput-object p1, p0, Lnp;->a:Lno;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lno;B)V
    .locals 0

    invoke-direct {p0, p1}, Lnp;-><init>(Lno;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ListAdapter;)V
    .locals 0

    iput-object p1, p0, Lnp;->c:Landroid/widget/ListAdapter;

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lnp;->d:Ljava/lang/CharSequence;

    return-void
.end method

.method public final b()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lnp;->a:Lno;

    invoke-virtual {v1}, Lno;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lnp;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnp;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_0
    iget-object v1, p0, Lnp;->c:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lnp;->a:Lno;

    invoke-virtual {v2}, Lno;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lnp;->b:Landroid/app/AlertDialog;

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lnp;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lnp;->b:Landroid/app/AlertDialog;

    return-void
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lnp;->b:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnp;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lnp;->a:Lno;

    invoke-virtual {v0, p2}, Lno;->a(I)V

    iget-object v0, p0, Lnp;->a:Lno;

    iget-object v0, v0, Lno;->t:Lmu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnp;->a:Lno;

    const/4 v1, 0x0

    iget-object v2, p0, Lnp;->c:Landroid/widget/ListAdapter;

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    invoke-virtual {v0, v1, p2}, Lno;->a(Landroid/view/View;I)Z

    :cond_0
    invoke-virtual {p0}, Lnp;->c()V

    return-void
.end method
