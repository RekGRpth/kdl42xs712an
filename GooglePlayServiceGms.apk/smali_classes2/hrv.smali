.class abstract Lhrv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lhqq;

.field protected final b:Limb;

.field protected volatile c:Z

.field protected final d:Lhsd;


# direct methods
.method constructor <init>(Lhqq;Limb;Lhsd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhrv;->c:Z

    iput-object p1, p0, Lhrv;->a:Lhqq;

    invoke-static {p2}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhrv;->b:Limb;

    iput-object p3, p0, Lhrv;->d:Lhsd;

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Livi;Livi;)Z
.end method

.method public final declared-synchronized b(Livi;Livi;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhrv;->c:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhrv;->b:Limb;

    const-string v1, "Writer closed, no data can be appended."

    invoke-virtual {v0, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lhrv;->d:Lhsd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhrv;->d:Lhsd;

    invoke-virtual {v0, p1}, Lhsd;->a(Livi;)V

    :cond_2
    invoke-virtual {p0, p1, p2}, Lhrv;->a(Livi;Livi;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhrv;->c:Z

    invoke-virtual {p0}, Lhrv;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
