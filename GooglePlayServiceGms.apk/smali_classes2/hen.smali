.class final Lhen;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhal;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhal;

.field private c:Luu;

.field private d:Lut;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lhal;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhen;->a:Landroid/content/Context;

    iput-object p2, p0, Lhen;->b:Lhal;

    new-instance v0, Luu;

    invoke-direct {v0, p3}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhen;->c:Luu;

    iget-object v0, p0, Lhen;->c:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhen;->d:Lut;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lhal;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lhen;-><init>(Landroid/content/Context;Lhal;Ljava/lang/String;)V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lhen;->c:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhen;->d:Lut;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "result_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v1, p0, Lhen;->c:Luu;

    iget-object v2, p0, Lhen;->d:Lut;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lhen;->a:Landroid/content/Context;

    iget-object v1, p0, Lhen;->c:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lhen;->c:Luu;

    iput-object v5, p0, Lhen;->d:Lut;

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhen;->b:Lhal;

    invoke-interface {v0, p1, p2}, Lhal;->a(ILandroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhen;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhen;->b:Lhal;

    invoke-interface {v0, p1, p2, p3}, Lhal;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhen;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhen;->b:Lhal;

    invoke-interface {v0, p1, p2, p3}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhen;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final a(IZLandroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhen;->b:Lhal;

    invoke-interface {v0, p1, p2, p3}, Lhal;->a(IZLandroid/os/Bundle;)V

    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhen;->a(ILjava/lang/String;)V

    return-void
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lhen;->b:Lhal;

    invoke-interface {v0}, Lhal;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method
