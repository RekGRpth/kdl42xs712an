.class public final Lhno;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field b:J

.field public c:I

.field public d:Ljava/lang/Object;


# direct methods
.method constructor <init>(ILjava/lang/Object;J)V
    .locals 7

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p3

    invoke-direct/range {v0 .. v6}, Lhno;-><init>(ILjava/lang/Object;JJ)V

    return-void
.end method

.method private constructor <init>(ILjava/lang/Object;JJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lhno;->c:I

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Position many not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lhno;->d:Ljava/lang/Object;

    iput p1, p0, Lhno;->c:I

    iput-wide p3, p0, Lhno;->a:J

    iput-wide p5, p0, Lhno;->b:J

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lhuf;)Lhno;
    .locals 7

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v5

    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v3

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p1, p0}, Lhuf;->a(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v2

    new-instance v0, Lhno;

    invoke-direct/range {v0 .. v6}, Lhno;-><init>(ILjava/lang/Object;JJ)V

    iput-wide v5, v0, Lhno;->b:J

    return-object v0
.end method

.method public static a(Lhno;Ljava/io/DataOutput;Lhuf;)V
    .locals 2

    iget-wide v0, p0, Lhno;->b:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    iget-wide v0, p0, Lhno;->a:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    iget v0, p0, Lhno;->c:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v0, p0, Lhno;->d:Ljava/lang/Object;

    invoke-interface {p2, v0, p1}, Lhuf;->a(Ljava/lang/Object;Ljava/io/DataOutput;)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CacheResult ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhno;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " databaseVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhno;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " readingTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhno;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lastSeenTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lhno;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
