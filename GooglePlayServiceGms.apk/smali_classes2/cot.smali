.class public final enum Lcot;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcot;

.field public static final enum b:Lcot;

.field public static final enum c:Lcot;

.field private static final synthetic e:[Lcot;


# instance fields
.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v0, Lcot;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v3, v3}, Lcot;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcot;->a:Lcot;

    new-instance v0, Lcot;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v2, v2}, Lcot;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcot;->b:Lcot;

    new-instance v0, Lcot;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4, v2}, Lcot;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcot;->c:Lcot;

    const/4 v0, 0x3

    new-array v0, v0, [Lcot;

    sget-object v1, Lcot;->a:Lcot;

    aput-object v1, v0, v3

    sget-object v1, Lcot;->b:Lcot;

    aput-object v1, v0, v2

    sget-object v1, Lcot;->c:Lcot;

    aput-object v1, v0, v4

    sput-object v0, Lcot;->e:[Lcot;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lcot;->d:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcot;
    .locals 1

    const-class v0, Lcot;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcot;

    return-object v0
.end method

.method public static values()[Lcot;
    .locals 1

    sget-object v0, Lcot;->e:[Lcot;

    invoke-virtual {v0}, [Lcot;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcot;

    return-object v0
.end method
