.class public final Lbzc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcdb;


# instance fields
.field private final a:Lbzp;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbzp;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzp;

    iput-object v0, p0, Lbzc;->a:Lbzp;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0001    # com.google.android.gms.R.dimen.drive_doclist_group_separator_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbzc;->b:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lbzc;->b:I

    return v0
.end method

.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040049    # com.google.android.gms.R.layout.drive_doc_entry_group_title_sticky

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbzd;

    invoke-direct {v1, v0}, Lbzd;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v1, Lbzd;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lbzc;->a:Lbzp;

    invoke-interface {v0, p1}, Lbzp;->c(Landroid/view/View;)V

    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzd;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    iget-object v0, v0, Lbzd;->b:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lcar;Landroid/content/Context;)V
    .locals 3

    invoke-interface {p2, p3}, Lcar;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzd;

    iget-object v0, v0, Lbzd;->b:Lcom/google/android/gms/drive/data/view/FixedSizeTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/data/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Lcda;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzd;

    sget-object v1, Lcda;->b:Lcda;

    invoke-virtual {p2, v1}, Lcda;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    :goto_0
    iget-object v0, v0, Lbzd;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lbzc;->a:Lbzp;

    invoke-interface {v0, p1}, Lbzp;->d(Landroid/view/View;)V

    return-void
.end method

.method public final c(Landroid/view/View;)Lcar;
    .locals 1

    iget-object v0, p0, Lbzc;->a:Lbzp;

    invoke-interface {v0, p1}, Lbzp;->a(Landroid/view/View;)Lcar;

    move-result-object v0

    return-object v0
.end method

.method public final d(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Lbzc;->a:Lbzp;

    invoke-interface {v0, p1}, Lbzp;->b(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
