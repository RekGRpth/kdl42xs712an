.class final Ldwk;
.super Ldve;
.source "SourceFile"


# instance fields
.field final synthetic e:Ldvv;


# direct methods
.method public constructor <init>(Ldvv;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Ldwk;->e:Ldvv;

    invoke-direct {p0, p2}, Ldve;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040071    # com.google.android.gms.R.layout.games_muted_app_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    check-cast p3, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz p3, :cond_0

    const v0, 0x7f0a0172    # com.google.android.gms.R.id.app_name

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    const v2, 0x7f0200d5    # com.google.android.gms.R.drawable.games_default_game_img

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :cond_0
    return-void
.end method
