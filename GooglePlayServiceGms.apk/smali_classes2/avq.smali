.class public final Lavq;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lavs;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lavq;->b:J

    iput-boolean v2, p0, Lavq;->d:Z

    iput-boolean v2, p0, Lavq;->f:Z

    iput-boolean v2, p0, Lavq;->h:Z

    iput-boolean v2, p0, Lavq;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lavq;->l:Lavs;

    const/4 v0, -0x1

    iput v0, p0, Lavq;->m:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lavq;->m:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lavq;->b()I

    :cond_0
    iget v0, p0, Lavq;->m:I

    return v0
.end method

.method public final a(J)Lavq;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->a:Z

    iput-wide p1, p0, Lavq;->b:J

    return-object p0
.end method

.method public final a(Lavs;)Lavq;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->k:Z

    iput-object p1, p0, Lavq;->l:Lavs;

    return-object p0
.end method

.method public final a(Z)Lavq;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->c:Z

    iput-boolean p1, p0, Lavq;->d:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lavq;->a(J)Lavq;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavq;->a(Z)Lavq;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavq;->b(Z)Lavq;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavq;->c(Z)Lavq;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lavq;->d(Z)Lavq;

    goto :goto_0

    :sswitch_6
    new-instance v0, Lavs;

    invoke-direct {v0}, Lavs;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Lavq;->a(Lavs;)Lavq;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Lavq;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lavq;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_0
    iget-boolean v0, p0, Lavq;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lavq;->d:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Lavq;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lavq;->f:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Lavq;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lavq;->h:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lavq;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lavq;->j:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_4
    iget-boolean v0, p0, Lavq;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lavq;->l:Lavs;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_5
    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lavq;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lavq;->b:J

    invoke-static {v0, v1, v2}, Lizh;->c(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lavq;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lavq;->d:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lavq;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lavq;->f:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lavq;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lavq;->h:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lavq;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lavq;->j:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lavq;->k:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lavq;->l:Lavs;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lavq;->m:I

    return v0
.end method

.method public final b(Z)Lavq;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->e:Z

    iput-boolean p1, p0, Lavq;->f:Z

    return-object p0
.end method

.method public final c(Z)Lavq;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->g:Z

    iput-boolean p1, p0, Lavq;->h:Z

    return-object p0
.end method

.method public final d(Z)Lavq;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->i:Z

    iput-boolean p1, p0, Lavq;->j:Z

    return-object p0
.end method
