.class final Layw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:D

.field final synthetic c:Layo;


# direct methods
.method constructor <init>(Layo;Ljava/lang/String;D)V
    .locals 0

    iput-object p1, p0, Layw;->c:Layo;

    iput-object p2, p0, Layw;->a:Ljava/lang/String;

    iput-wide p3, p0, Layw;->b:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Layw;->c:Layo;

    iget-object v0, v0, Layo;->a:Laze;

    invoke-virtual {v0}, Laze;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Layw;->c:Layo;

    iget-object v1, v1, Layo;->a:Laze;

    iget-object v1, v1, Laze;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layx;

    iget-object v3, p0, Layw;->a:Ljava/lang/String;

    iget-object v6, v0, Layx;->b:Lbac;

    if-eqz v6, :cond_2

    iget-object v6, v0, Layx;->d:Ljava/lang/String;

    invoke-static {v3, v6}, Laxy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iput-object v3, v0, Layx;->d:Ljava/lang/String;

    move v3, v4

    :goto_2
    or-int/2addr v1, v3

    iget-wide v6, p0, Layw;->b:D

    iget-object v3, v0, Layx;->b:Lbac;

    if-eqz v3, :cond_3

    iget-object v3, v0, Layx;->k:Laye;

    const-string v8, "onVolumeChanged to %f, was %f"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v2

    iget-wide v10, v0, Layx;->c:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    aput-object v10, v9, v4

    invoke-virtual {v3, v8, v9}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-wide v8, v0, Layx;->c:D

    cmpl-double v3, v6, v8

    if-eqz v3, :cond_3

    iput-wide v6, v0, Layx;->c:D

    move v0, v4

    :goto_3
    or-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    if-eqz v1, :cond_0

    iget-object v0, p0, Layw;->c:Layo;

    iget-object v0, v0, Layo;->b:Layl;

    invoke-static {v0}, Layl;->a(Layl;)V

    goto :goto_0
.end method
