.class final Lebi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Landroid/widget/ImageView;

.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/widget/TextView;

.field final f:Landroid/widget/TextView;

.field final g:Landroid/view/View;

.field final h:Landroid/view/View;

.field final synthetic i:Lebf;


# direct methods
.method public constructor <init>(Lebf;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lebi;->i:Lebf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aQ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lebi;->a:Landroid/widget/ImageView;

    sget v0, Lxa;->aP:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebi;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebi;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, p0, Lebi;->c:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d()V

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebi;->d:Landroid/widget/TextView;

    sget v0, Lxa;->bf:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebi;->e:Landroid/widget/TextView;

    sget v0, Lxa;->t:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebi;->f:Landroid/widget/TextView;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebi;->g:Landroid/view/View;

    iget-object v0, p0, Lebi;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebi;->h:Landroid/view/View;

    iget-object v0, p0, Lebi;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Lebi;->i:Lebf;

    invoke-virtual {v0}, Lebf;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v3, v0, Lcom/google/android/gms/games/request/GameRequest;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v4, Lxa;->ah:I

    if-ne v3, v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->d()Lcom/google/android/gms/games/Game;

    move-result-object v0

    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-static {v2}, Lebf;->b(Lebf;)Lebh;

    move-result-object v2

    invoke-interface {v2, v0}, Lebh;->b_(Lcom/google/android/gms/games/Game;)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget v4, Lxa;->af:I

    if-ne v3, v4, :cond_2

    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-static {v2}, Lebf;->c(Lebf;)Leax;

    move-result-object v2

    invoke-virtual {v2, v0}, Leax;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-virtual {v2}, Lebf;->notifyDataSetChanged()V

    instance-of v2, v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-static {v2}, Lebf;->b(Lebf;)Lebh;

    move-result-object v2

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-interface {v2, v0}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-static {v2}, Lebf;->b(Lebf;)Lebh;

    move-result-object v2

    invoke-interface {v2, v0}, Lebh;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Lcom/google/android/gms/games/request/GameRequest;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lxa;->aq:I

    if-ne v1, v2, :cond_2

    instance-of v1, v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lebi;->i:Lebf;

    invoke-static {v1}, Lebf;->b(Lebf;)Lebh;

    move-result-object v1

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iget-object v2, p0, Lebi;->i:Lebf;

    invoke-static {v2}, Lebf;->a(Lebf;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lebh;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lebi;->i:Lebf;

    invoke-static {v1}, Lebf;->b(Lebf;)Lebh;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/request/GameRequest;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, Lebh;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    goto :goto_0

    :cond_2
    sget v2, Lxa;->ap:I

    if-ne v1, v2, :cond_0

    new-instance v1, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    instance-of v0, v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    if-eqz v0, :cond_3

    sget v0, Lxd;->d:I

    invoke-virtual {v1, v0}, Lov;->a(I)V

    :goto_1
    new-instance v0, Ledk;

    invoke-direct {v0, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v0, v1, Lov;->c:Lox;

    iget-object v0, v1, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto :goto_0

    :cond_3
    sget v0, Lxd;->e:I

    invoke-virtual {v1, v0}, Lov;->a(I)V

    goto :goto_1

    :cond_4
    const-string v1, "RequestListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
