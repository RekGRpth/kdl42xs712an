.class public final Lezb;
.super Leyq;
.source "SourceFile"


# instance fields
.field private k:Leyr;

.field private l:Lezk;

.field private m:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 11

    const v3, 0x3eaaaaab

    const/high16 v10, 0x42000000    # 32.0f

    const/16 v9, 0x24

    const/4 v1, 0x0

    const/high16 v8, 0x3e800000    # 0.25f

    invoke-direct {p0}, Leyq;-><init>()V

    iput v1, p0, Lezb;->m:I

    :try_start_0
    new-instance v0, Lezk;

    invoke-direct {v0}, Lezk;-><init>()V

    iput-object v0, p0, Lezb;->l:Lezk;
    :try_end_0
    .catch Leyu; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Leyr;

    invoke-direct {v0}, Leyr;-><init>()V

    iput-object v0, p0, Lezb;->k:Leyr;

    iput v9, p0, Lezb;->m:I

    iget v0, p0, Lezb;->m:I

    invoke-virtual {p0, v9, v0}, Lezb;->a(II)V

    const/16 v0, 0x18

    new-array v2, v0, [F

    fill-array-data v2, :array_0

    new-array v4, v9, [S

    fill-array-data v4, :array_1

    move v0, v1

    :goto_1
    if-ge v0, v9, :cond_0

    aget-short v5, v4, v0

    mul-int/lit8 v5, v5, 0x3

    aget v6, v2, v5

    mul-float/2addr v6, v10

    add-int/lit8 v7, v5, 0x1

    aget v7, v2, v7

    mul-float/2addr v7, v10

    add-int/lit8 v5, v5, 0x2

    aget v5, v2, v5

    mul-float/2addr v5, v10

    invoke-virtual {p0, v0, v6, v7, v5}, Lezb;->a(IFFF)V

    int-to-short v5, v0

    invoke-virtual {p0, v0, v5}, Lezb;->a(IS)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Leyu;->printStackTrace()V

    goto :goto_0

    :cond_0
    move v6, v1

    :goto_2
    const/4 v0, 0x4

    if-ge v6, v0, :cond_1

    int-to-float v0, v6

    mul-float v2, v0, v8

    add-float v4, v2, v8

    const v5, 0x3f2aaaab

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lezb;->a(IFFFF)I

    move-result v1

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_1
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    move-object v0, p0

    move v2, v8

    invoke-direct/range {v0 .. v5}, Lezb;->a(IFFFF)I

    move-result v1

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const v5, 0x3f2aaaab

    move-object v0, p0

    move v2, v8

    invoke-direct/range {v0 .. v5}, Lezb;->a(IFFFF)I

    :try_start_1
    iget-object v0, p0, Lezb;->k:Leyr;

    invoke-virtual {v0, p1}, Leyr;->a(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Leyu; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    return-void

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Leyu;->printStackTrace()V

    goto :goto_3

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_1
    .array-data 2
        0x0s
        0x7s
        0x3s
        0x0s
        0x4s
        0x7s
        0x4s
        0x6s
        0x7s
        0x4s
        0x5s
        0x6s
        0x5s
        0x2s
        0x6s
        0x5s
        0x1s
        0x2s
        0x1s
        0x3s
        0x2s
        0x1s
        0x0s
        0x3s
        0x3s
        0x6s
        0x7s
        0x3s
        0x2s
        0x6s
        0x4s
        0x1s
        0x0s
        0x4s
        0x5s
        0x1s
    .end array-data
.end method

.method private a(IFFFF)I
    .locals 1

    invoke-direct {p0, p1, p2, p5}, Lezb;->a(IFF)V

    add-int/lit8 v0, p1, 0x2

    invoke-direct {p0, v0, p4, p3}, Lezb;->a(IFF)V

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0, p2, p3}, Lezb;->a(IFF)V

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0, p2, p5}, Lezb;->a(IFF)V

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0, p4, p5}, Lezb;->a(IFF)V

    add-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0, p4, p3}, Lezb;->a(IFF)V

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private a(IFF)V
    .locals 2

    iget-object v0, p0, Lezb;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    iget-object v0, p0, Lezb;->b:Ljava/nio/FloatBuffer;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1, p3}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    return-void
.end method


# virtual methods
.method public final a([F)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lezb;->l:Lezk;

    invoke-virtual {v0}, Lezk;->a()V

    iget-object v0, p0, Lezb;->l:Lezk;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lezk;->a(F)V

    iget-object v0, p0, Lezb;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lezb;->l:Lezk;

    iget-object v1, p0, Lezb;->a:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lezk;->a(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lezb;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lezb;->l:Lezk;

    iget-object v1, p0, Lezb;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Lezk;->b(Ljava/nio/FloatBuffer;)V

    iget-object v0, p0, Lezb;->k:Leyr;

    iget-object v1, p0, Lezb;->l:Lezk;

    invoke-virtual {v0}, Leyr;->a()V

    iget-object v0, p0, Lezb;->l:Lezk;

    invoke-virtual {v0, p1}, Lezk;->a([F)V

    const/4 v0, 0x4

    iget v1, p0, Lezb;->m:I

    const/16 v2, 0x1403

    iget-object v3, p0, Lezb;->c:Ljava/nio/ShortBuffer;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    return-void
.end method
