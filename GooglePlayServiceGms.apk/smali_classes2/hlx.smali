.class final Lhlx;
.super Lhlz;
.source "SourceFile"

# interfaces
.implements Lhkx;


# instance fields
.field final synthetic a:Lhlu;

.field private c:I

.field private d:I


# direct methods
.method private constructor <init>(Lhlu;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lhlx;->a:Lhlu;

    invoke-direct {p0, p1}, Lhlz;-><init>(Lhlu;)V

    iput v0, p0, Lhlx;->c:I

    iput v0, p0, Lhlx;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lhlu;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhlx;-><init>(Lhlu;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0}, Lhlz;->a()V

    iget-object v0, p0, Lhlx;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    invoke-virtual {v0, v1, v1}, Lhkr;->a(IZ)V

    iget-object v0, p0, Lhlx;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    invoke-virtual {v0, p0}, Lhkr;->a(Lhkx;)V

    iget-object v0, p0, Lhlx;->a:Lhlu;

    iget-object v1, p0, Lhlx;->a:Lhlu;

    iget-object v1, v1, Lhlu;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    const-wide/32 v3, 0x15f90

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lhlu;->a(J)V

    return-void
.end method

.method protected final a(Lhlz;)V
    .locals 2

    invoke-super {p0, p1}, Lhlz;->a(Lhlz;)V

    iget-object v0, p0, Lhlx;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhkr;->b(I)V

    iget-object v0, p0, Lhlx;->a:Lhlu;

    iget-object v0, v0, Lhlu;->b:Lhkr;

    invoke-virtual {v0, p0}, Lhkr;->b(Lhkx;)V

    return-void
.end method

.method public final a_(Lcom/google/android/gms/location/ActivityRecognitionResult;)V
    .locals 4

    iget-object v0, p0, Lhlx;->a:Lhlu;

    invoke-static {p1}, Lhlu;->b(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lhlx;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhlx;->c:I

    iget v0, p0, Lhlx;->c:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lhlx;->a:Lhlu;

    invoke-static {v0}, Lhlu;->a(Lhlu;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhlx;->a:Lhlu;

    invoke-static {p1}, Lhlu;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhlx;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhlx;->d:I

    iget v0, p0, Lhlx;->d:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lhlx;->a:Lhlu;

    new-instance v1, Lhlw;

    iget-object v2, p0, Lhlx;->a:Lhlu;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lhlw;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "MaybeLeavingVehicleState"

    return-object v0
.end method

.method protected final c()V
    .locals 4

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "VehExitDetector"

    const-string v1, "Timed out waiting for exit"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhlx;->a:Lhlu;

    new-instance v1, Lhlw;

    iget-object v2, p0, Lhlx;->a:Lhlu;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lhlw;-><init>(Lhlu;B)V

    invoke-static {v0, v1}, Lhlu;->a(Lhlu;Lhlz;)V

    return-void
.end method
