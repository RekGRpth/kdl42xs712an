.class public abstract enum Lcat;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcat;

.field public static final enum b:Lcat;

.field public static final enum c:Lcat;

.field public static final d:Ljava/util/Map;

.field private static final synthetic f:[Lcat;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcau;

    const-string v1, "FOLDERS_THEN_TITLE"

    invoke-direct {v0, v1}, Lcau;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcat;->a:Lcat;

    new-instance v0, Lcav;

    const-string v1, "LAST_MODIFIED"

    invoke-direct {v0, v1}, Lcav;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcat;->b:Lcat;

    new-instance v0, Lcaw;

    const-string v1, "SHARED_WITH_ME_DATE"

    invoke-direct {v0, v1}, Lcaw;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcat;->c:Lcat;

    const/4 v0, 0x3

    new-array v0, v0, [Lcat;

    const/4 v1, 0x0

    sget-object v2, Lcat;->a:Lcat;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcat;->b:Lcat;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcat;->c:Lcat;

    aput-object v2, v0, v1

    sput-object v0, Lcat;->f:[Lcat;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcat;->d:Ljava/util/Map;

    const-class v0, Lcat;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcat;

    sget-object v2, Lcat;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lcat;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcat;->e:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcat;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcat;
    .locals 1

    const-class v0, Lcat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcat;

    return-object v0
.end method

.method public static values()[Lcat;
    .locals 1

    sget-object v0, Lcat;->f:[Lcat;

    invoke-virtual {v0}, [Lcat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcat;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcoy;)Lcag;
.end method
