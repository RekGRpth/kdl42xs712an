.class public final Lcps;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I


# instance fields
.field a:Lcpy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcps;->b:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x66

    aput v2, v0, v1

    sput-object v0, Lcps;->c:[I

    const/16 v0, 0x28

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcps;->d:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcps;->e:[I

    const/16 v0, 0x1a

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcps;->f:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xde
        0x66
        0xc4
        0x9d
        0xd3
        0xc4
        0xcc
        0xe0
        0xba
        0xca
        0xb7
        0x26
        0xc9
        0xcd
        0xce
        0x66
        0xca
        0xd7
        0x75
        0xc3
        0x85
        0x66
        0xb7
        0xb6
        0x75
    .end array-data

    :array_1
    .array-data 4
        0xd2
        0x66
        0xcb
        0xde
        0xc4
        0x9d
        0xc4
        0xdb
        0xba
        0xb7
        0x26
        0xd8
        0xd2
        0x66
        0x75
        0xda
        0xc3
        0xd2
        0x85
        0xd9
        0x66
        0xdb
        0xb7
        0xb6
        0x75
        0xe0
        0xd0
        0x66
        0xb7
        0xb6
        0xd6
        0xd9
        0x75
        0xb5
        0xcb
        0xbc
        0x25
        0xe4
        0xda
        0xb6
    .end array-data

    :array_2
    .array-data 4
        0xd3
        0x7f
        0x4
        0xcc
        0xce
        0xde
        0xc5
        0x13
        0x35
    .end array-data

    :array_3
    .array-data 4
        0xcd
        0xdd
        0x66
        0xd9
        0xb0
        0xce
        0xb7
        0x95
        0xc0
        0xbe
        0x9a
        0xd6
        0xc9
        0xd5
        0x66
        0xd1
        0x75
        0xc3
        0x85
        0x66
        0xd4
        0xcb
        0xd5
        0xb7
        0xb6
        0x75
    .end array-data
.end method

.method public constructor <init>(Lcpy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcps;->a:Lcpy;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 7

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v1, Lcpt;

    invoke-direct {v1, p0}, Lcpt;-><init>(Lcps;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcps;->a:Lcpy;

    sget-object v6, Lcps;->c:[I

    invoke-virtual {v5, v6}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcps;->a:Lcpy;

    sget-object v6, Lcps;->c:[I

    invoke-virtual {v5, v6}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcps;->a:Lcpy;

    sget-object v6, Lcps;->e:[I

    invoke-virtual {v5, v6}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lbpv;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcps;->a:Lcpy;

    sget-object v2, Lcps;->f:[I

    invoke-virtual {v1, v2}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcps;->a(Ljava/lang/String;Ljava/util/List;)V

    iget-object v1, p0, Lcps;->a:Lcpy;

    sget-object v2, Lcps;->b:[I

    invoke-virtual {v1, v2}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcps;->a(Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcps;->a:Lcpy;

    sget-object v3, Lcps;->d:[I

    invoke-virtual {v2, v3}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcps;->a:Lcpy;

    sget-object v3, Lcps;->e:[I

    invoke-virtual {v2, v3}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbpv;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lcps;->a:Lcpy;

    sget-object v2, Lcps;->e:[I

    invoke-virtual {v1, v2}, Lcpy;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbpv;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
