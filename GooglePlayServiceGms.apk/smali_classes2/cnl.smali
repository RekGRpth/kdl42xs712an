.class public final Lcnl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcnk;


# instance fields
.field private final a:Lcev;

.field private final b:Lcev;

.field private final c:Ljava/util/Collection;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcev;Lcev;Ljava/util/Collection;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcnl;-><init>(Lcev;Lcev;Ljava/util/Collection;Z)V

    return-void
.end method

.method private constructor <init>(Lcev;Lcev;Ljava/util/Collection;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcnl;->a:Lcev;

    iput-object p2, p0, Lcnl;->b:Lcev;

    iput-object p3, p0, Lcnl;->c:Ljava/util/Collection;

    iput-boolean p4, p0, Lcnl;->d:Z

    return-void
.end method


# virtual methods
.method public final a()Lcnk;
    .locals 5

    new-instance v1, Lcnl;

    iget-object v2, p0, Lcnl;->b:Lcev;

    iget-object v3, p0, Lcnl;->a:Lcev;

    iget-object v4, p0, Lcnl;->c:Ljava/util/Collection;

    iget-boolean v0, p0, Lcnl;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v2, v3, v4, v0}, Lcnl;-><init>(Lcev;Lcev;Ljava/util/Collection;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcnl;->c:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnk;

    iget-boolean v3, p0, Lcnl;->d:Z

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcnk;->a()Lcnk;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Lcnk;->b()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcnl;->a:Lcev;

    invoke-virtual {v0, v1}, Lcev;->a(Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method
