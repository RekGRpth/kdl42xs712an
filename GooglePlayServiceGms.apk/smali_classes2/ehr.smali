.class public final Lehr;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:J

.field public e:J

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v2, p0, Lehr;->a:I

    iput-wide v0, p0, Lehr;->b:J

    iput v2, p0, Lehr;->c:I

    iput-wide v0, p0, Lehr;->d:J

    iput-wide v0, p0, Lehr;->e:J

    iput-wide v0, p0, Lehr;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lehr;->C:I

    return-void
.end method

.method public static a([B)Lehr;
    .locals 1

    new-instance v0, Lehr;

    invoke-direct {v0}, Lehr;-><init>()V

    invoke-static {v0, p0}, Lizs;->a(Lizs;[B)Lizs;

    move-result-object v0

    check-cast v0, Lehr;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 6

    const-wide/16 v4, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Lehr;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lehr;->a:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-wide v1, p0, Lehr;->b:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lehr;->b:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lehr;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lehr;->c:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-wide v1, p0, Lehr;->d:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Lehr;->d:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-wide v1, p0, Lehr;->e:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lehr;->e:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-wide v1, p0, Lehr;->f:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-wide v2, p0, Lehr;->f:J

    invoke-static {v1, v2, v3}, Lizn;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lehr;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehr;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehr;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lehr;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehr;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehr;->e:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lehr;->f:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 5

    const-wide/16 v3, 0x0

    iget v0, p0, Lehr;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lehr;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_0
    iget-wide v0, p0, Lehr;->b:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lehr;->b:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_1
    iget v0, p0, Lehr;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lehr;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_2
    iget-wide v0, p0, Lehr;->d:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-wide v1, p0, Lehr;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_3
    iget-wide v0, p0, Lehr;->e:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lehr;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_4
    iget-wide v0, p0, Lehr;->f:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lehr;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->a(IJ)V

    :cond_5
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lehr;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lehr;

    iget v2, p0, Lehr;->a:I

    iget v3, p1, Lehr;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lehr;->b:J

    iget-wide v4, p1, Lehr;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lehr;->c:I

    iget v3, p1, Lehr;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lehr;->d:J

    iget-wide v4, p1, Lehr;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lehr;->e:J

    iget-wide v4, p1, Lehr;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-wide v2, p0, Lehr;->f:J

    iget-wide v4, p1, Lehr;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/16 v5, 0x20

    iget v0, p0, Lehr;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehr;->b:J

    iget-wide v3, p0, Lehr;->b:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lehr;->c:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehr;->d:J

    iget-wide v3, p0, Lehr;->d:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehr;->e:J

    iget-wide v3, p0, Lehr;->e:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lehr;->f:J

    iget-wide v3, p0, Lehr;->f:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method
