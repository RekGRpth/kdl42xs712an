.class final Lhbl;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhbk;


# direct methods
.method constructor <init>(Lhbk;)V
    .locals 0

    iput-object p1, p0, Lhbl;->a:Lhbk;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Lipy;)V
    .locals 0

    return-void
.end method

.method public final a(Lizz;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lhcb;->a(Lizz;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iput-boolean v2, v0, Lhbk;->Y:Z

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iput-object p1, v0, Lhbk;->ab:Lizz;

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iget-boolean v0, v0, Lhbk;->aa:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0, p1}, Lhbk;->a(Lhbk;Lizz;)V

    iget-object v0, p1, Lizz;->b:[Lioj;

    array-length v0, v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lizz;->c:[Lipv;

    array-length v0, v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->a(Lhbk;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0, p1}, Lhbk;->b(Lhbk;Lizz;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0, p1}, Lhbk;->c(Lhbk;Lizz;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0, p1}, Lhbk;->d(Lhbk;Lizz;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iget-object v0, v0, Lhbk;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->b(Lhbk;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->c(Lhbk;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->d(Lhbk;)Luu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->e(Lhbk;)Lut;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->d(Lhbk;)Luu;

    move-result-object v0

    iget-object v1, p0, Lhbl;->a:Lhbk;

    invoke-static {v1}, Lhbk;->e(Lhbk;)Lut;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-virtual {v0}, Lhbk;->T_()Lo;

    move-result-object v0

    iget-object v1, p0, Lhbl;->a:Lhbk;

    invoke-static {v1}, Lhbk;->d(Lhbk;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->f(Lhbk;)Luu;

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->g(Lhbk;)Lut;

    goto :goto_0
.end method

.method public final a(Ljav;J)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iget-object v0, v0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p1, Ljav;->i:[I

    array-length v0, v0

    if-lez v0, :cond_0

    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Got required actions"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Ljav;->i:[I

    aget v0, v0, v2

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iget-object v0, v0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v1, p0, Lhbl;->a:Lhbk;

    iget-object v1, v1, Lhbk;->ad:Lipv;

    iput-object v1, v0, Lioj;->e:Lipv;

    iget-object v0, p0, Lhbl;->a:Lhbk;

    invoke-static {v0}, Lhbk;->i(Lhbk;)Lhbp;

    move-result-object v0

    iget-object v1, p0, Lhbl;->a:Lhbk;

    invoke-static {v1}, Lhbk;->h(Lhbk;)Ljau;

    move-result-object v1

    iget-object v2, p0, Lhbl;->a:Lhbk;

    iget-object v2, v2, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-interface {v0, v1, v2}, Lhbp;->a(Ljau;Lioj;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Expected a CVV challenge!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lhbl;->a:Lhbk;

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lhbk;->a(Lhbk;I)V

    goto :goto_0
.end method

.method public final a(Ljax;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lhbl;->a:Lhbk;

    iget-object v0, v0, Lhbk;->i:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    iget-object v0, p0, Lhbl;->a:Lhbk;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lhbk;->a(Lhbk;I)V

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method
