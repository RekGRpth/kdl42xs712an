.class public final Lhxq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# instance fields
.field volatile a:Lhon;

.field b:Liln;

.field public c:Lhxr;

.field private final d:Ljava/lang/Class;

.field private final e:Landroid/content/Context;

.field private final f:Lbpe;

.field private final g:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(ILbpe;Landroid/content/Context;Landroid/os/PowerManager$WakeLock;Ljava/lang/Class;)V
    .locals 4

    const/16 v0, 0x64

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lhxq;->f:Lbpe;

    iput-object p3, p0, Lhxq;->e:Landroid/content/Context;

    iput-object p4, p0, Lhxq;->g:Landroid/os/PowerManager$WakeLock;

    iput-object p5, p0, Lhxq;->d:Ljava/lang/Class;

    invoke-static {p3}, Lbox;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "gcore_geofencer_geofence_limit_per_app"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    new-instance v1, Lhxr;

    invoke-direct {v1, v0, p1}, Lhxr;-><init>(II)V

    iput-object v1, p0, Lhxq;->c:Lhxr;

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "GeofenceStateCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Geofence limit per package="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public constructor <init>(Lbpe;Landroid/content/Context;)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lhxq;-><init>(ILbpe;Landroid/content/Context;Landroid/os/PowerManager$WakeLock;Ljava/lang/Class;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lhyh;Ljava/util/List;Landroid/content/pm/PackageManager;)V
    .locals 11

    iget-object v0, p0, Lhxq;->a:Lhon;

    invoke-virtual {v0}, Lhon;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lhyh;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofenceStateCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "First service restart after reboot: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p1, Lhyh;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyg;

    iget-object v2, v0, Lhyg;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v0, "GeofenceStateCache"

    const-string v2, "Incomplete geofence information: lack PendingIntent key."

    invoke-static {v0, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_3
    invoke-static {v2, p2}, Lhxq;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;

    move-result-object v8

    if-nez v8, :cond_4

    if-nez v1, :cond_1

    const-string v0, "GeofenceStateCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to find pending intent for key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Registered geofence will be dropped."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v8}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b()Landroid/app/PendingIntent;

    move-result-object v2

    iget-boolean v3, v0, Lhyg;->a:Z

    if-eqz v3, :cond_5

    iget-boolean v3, v0, Lhyg;->c:Z

    if-eqz v3, :cond_5

    iget-boolean v3, v0, Lhyg;->e:Z

    if-eqz v3, :cond_5

    iget v3, v0, Lhyg;->f:I

    invoke-static {v3}, Lhxp;->a(I)B

    move-result v3

    if-gez v3, :cond_7

    :cond_5
    const-string v0, "GeofenceState"

    const-string v2, "Incomplete geofence state."

    invoke-static {v0, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move-object v2, v0

    :goto_2
    if-eqz v2, :cond_d

    iget-object v0, v2, Lhxp;->a:Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v8}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v8}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->e()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v8}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-interface {v6, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_7
    iget-object v3, v0, Lhyg;->b:Lhyf;

    invoke-static {v3}, Lhxp;->a(Lhyf;)Lcom/google/android/gms/location/internal/ParcelableGeofence;

    move-result-object v3

    if-nez v3, :cond_8

    const-string v0, "GeofenceState"

    const-string v2, "Invalid geofence from protocol buffer."

    invoke-static {v0, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_2

    :cond_8
    new-instance v5, Lhxp;

    invoke-direct {v5, v3, v2}, Lhxp;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;Landroid/app/PendingIntent;)V

    iget-object v2, v5, Lhxp;->e:Lhxv;

    iget v3, v0, Lhyg;->f:I

    invoke-static {v3}, Lhxp;->a(I)B

    move-result v3

    invoke-virtual {v2, v3}, Lhxv;->a(B)V

    iget-boolean v2, v0, Lhyg;->g:Z

    if-eqz v2, :cond_a

    iget v2, v0, Lhyg;->h:I

    invoke-static {v2}, Lhxp;->a(I)B

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_9

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    :cond_9
    iput-byte v2, v5, Lhxp;->c:B

    :cond_a
    :goto_3
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    iget-boolean v9, v0, Lhyg;->k:Z

    if-eqz v9, :cond_14

    iget-byte v9, v5, Lhxp;->c:B

    const/4 v10, 0x1

    if-ne v9, v10, :cond_14

    iget-boolean v4, v0, Lhyg;->l:Z

    if-nez v4, :cond_13

    iget-boolean v9, v0, Lhyg;->i:Z

    if-eqz v9, :cond_13

    iget-wide v2, v0, Lhyg;->j:J

    move v0, v4

    :goto_4
    iput-boolean v0, v5, Lhxp;->g:Z

    iput-wide v2, v5, Lhxp;->f:J

    move-object v2, v5

    goto :goto_2

    :cond_b
    const/4 v2, 0x0

    iput-byte v2, v5, Lhxp;->c:B

    goto :goto_3

    :cond_c
    const-string v2, "GeofenceStateCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Request ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " not found in memory cache. Probably caused by I/O failure during removeGeofences call."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    const-string v0, "GeofenceStateCache"

    const-string v2, "Protocol buffer does not convert to a valid geofence state."

    invoke-static {v0, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->e()I

    move-result v3

    if-nez v3, :cond_11

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lhxq;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_f

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    iget-object v4, p0, Lhxq;->c:Lhxr;

    iget-object v5, v0, Lhxp;->b:Landroid/app/PendingIntent;

    invoke-virtual {v5}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lhxr;->a(Ljava/lang/String;)Lhxt;

    move-result-object v4

    invoke-virtual {v4, v0}, Lhxt;->a(Lhxp;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_f
    move v0, v1

    move v1, v0

    goto :goto_5

    :cond_10
    const-string v0, "GeofenceStateCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Not recovering removed package: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_11
    const-string v0, "GeofenceStateCache"

    const-string v3, "Some geofence coudn\'t found on disk."

    invoke-static {v0, v3}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_12
    const-string v0, "GeofenceStateCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Recovered "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " geofences."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_13
    move v0, v4

    goto/16 :goto_4

    :cond_14
    move v0, v4

    goto/16 :goto_4
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private f()V
    .locals 4

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofenceStateCache"

    const-string v1, "Sync-ing geofence state cache."

    invoke-static {v0, v1}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhxq;->a:Lhon;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhxq;->c:Lhxr;

    iget-object v1, p0, Lhxq;->a:Lhon;

    invoke-virtual {v1}, Lhon;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    new-instance v3, Lhyh;

    invoke-direct {v3}, Lhyh;-><init>()V

    invoke-virtual {v3, v1}, Lhyh;->a(Ljava/lang/String;)Lhyh;

    iget-object v0, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-virtual {v0, v2, v3}, Lhxt;->a(Ljava/util/Map;Lhyh;)V

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lhxq;->b:Liln;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lizk;

    invoke-virtual {v2, v0}, Liln;->b(Lizk;)V

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "GeofenceStateCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting new system memory cache: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lhxq;->a:Lhon;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v2, v0}, Lhon;->a(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GeofenceStateCache"

    const-string v2, "Unable to save geofence states on disk, PendingIntent memory cache was not changed"

    invoke-static {v1, v2}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/PendingIntent;)I
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->a()V

    iget-object v3, p0, Lhxq;->c:Lhxr;

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lhxt;->a()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual {v0, p1}, Lhxt;->b(Landroid/app/PendingIntent;)Z

    move-result v2

    invoke-virtual {v0}, Lhxt;->a()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, v3, Lhxr;->d:Lhxk;

    iget-object v4, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v3}, Lhxr;->c()I

    move-result v3

    invoke-interface {v2, v4, v3}, Lhxk;->a(Ljava/lang/Iterable;I)V

    :cond_1
    if-eqz v0, :cond_3

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v1}, Lhxr;->b()V

    :cond_2
    iget-object v1, p0, Lhxq;->c:Lhxr;

    const/4 v2, 0x0

    iput-object v2, v1, Lhxr;->c:Ljava/util/HashMap;

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->a()V

    iget-object v3, p0, Lhxq;->c:Lhxr;

    iget-object v0, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lhxt;->a()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v4, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    if-eqz v0, :cond_0

    iget-object v4, v3, Lhxr;->d:Lhxk;

    iget-object v5, v3, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v3}, Lhxr;->c()I

    move-result v3

    invoke-interface {v4, v5, v3}, Lhxk;->a(Ljava/lang/Iterable;I)V

    :cond_0
    if-eqz v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    if-eqz v2, :cond_2

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->b()V

    :cond_2
    iget-object v0, p0, Lhxq;->c:Lhxr;

    const/4 v1, 0x0

    iput-object v1, v0, Lhxr;->c:Ljava/util/HashMap;

    return v2

    :cond_3
    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Landroid/app/PendingIntent;)I
    .locals 8

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->a()V

    iget-object v4, p0, Lhxq;->c:Lhxr;

    new-instance v5, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    if-nez v0, :cond_0

    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->f()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :cond_4
    invoke-virtual {p2}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1, p1}, Lhxr;->a(Ljava/lang/String;Ljava/util/List;)Z

    iget-object v0, v4, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    if-nez v0, :cond_6

    move v0, v3

    :goto_2
    if-nez v0, :cond_7

    const/16 v0, 0x3ea

    :goto_3
    if-nez v0, :cond_c

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    if-eqz v2, :cond_5

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->b()V

    :cond_5
    iget-object v0, p0, Lhxq;->c:Lhxr;

    const/4 v1, 0x0

    iput-object v1, v0, Lhxr;->c:Ljava/util/HashMap;

    return v2

    :cond_6
    invoke-virtual {v0, p2}, Lhxt;->a(Landroid/app/PendingIntent;)Z

    move-result v0

    goto :goto_2

    :cond_7
    iget-object v0, v4, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-nez v0, :cond_9

    iget v0, v4, Lhxr;->a:I

    if-gt v5, v0, :cond_8

    move v0, v3

    :goto_5
    if-nez v0, :cond_a

    const/16 v0, 0x3e9

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    iget v6, v4, Lhxr;->a:I

    invoke-virtual {v0, v5, v6}, Lhxt;->a(II)Z

    move-result v0

    goto :goto_5

    :cond_a
    invoke-virtual {v4, v1}, Lhxr;->a(Ljava/lang/String;)Lhxt;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    new-instance v5, Lhxp;

    invoke-direct {v5, v0, p2}, Lhxp;-><init>(Lcom/google/android/gms/location/internal/ParcelableGeofence;Landroid/app/PendingIntent;)V

    invoke-virtual {v1, v5}, Lhxt;->a(Lhxp;)V

    goto :goto_6

    :cond_b
    move v0, v2

    goto :goto_3

    :catch_0
    move-exception v0

    move v2, v3

    goto :goto_4

    :cond_c
    move v2, v0

    goto :goto_4
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->a()V

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0, p2, p1}, Lhxr;->b(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v1}, Lhxr;->b()V

    :cond_1
    iget-object v1, p0, Lhxq;->c:Lhxr;

    const/4 v2, 0x0

    iput-object v2, v1, Lhxr;->c:Ljava/util/HashMap;

    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(JLandroid/location/Location;D)Lhxn;
    .locals 8

    iget-object v2, p0, Lhxq;->c:Lhxr;

    iget-object v0, v2, Lhxr;->d:Lhxk;

    iget-object v1, v2, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v2}, Lhxr;->c()I

    move-result v2

    move-wide v3, p1

    move-object v5, p3

    move-wide v6, p4

    invoke-interface/range {v0 .. v7}, Lhxk;->a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v1, "GeofenceStateCache"

    const-string v2, "Unable to save geofence exit/enter state."

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(DI)Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lhxq;->c:Lhxr;

    iget-object v1, v0, Lhxr;->d:Lhxk;

    iget-object v0, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0, p1, p2, p3}, Lhxk;->a(Ljava/lang/Iterable;DI)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lhxq;->c:Lhxr;

    iget-object v1, v0, Lhxr;->d:Lhxk;

    iget-object v0, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Lhxk;->c(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    iget-object v0, p0, Lhxq;->d:Ljava/lang/Class;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lhxq;->a:Lhon;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lhon;

    iget-object v1, p0, Lhxq;->e:Landroid/content/Context;

    iget-object v2, p0, Lhxq;->d:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lhxq;->a:Lhon;

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Lhpk;)V
    .locals 6

    const-string v0, "geofencer_state_list"

    iget-object v1, p0, Lhxq;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lhxq;->a:Lhon;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lhxq;->a:Lhon;

    invoke-virtual {v2, p1}, Lhon;->c(Landroid/content/Intent;)V

    iget-object v2, p0, Lhxq;->a:Lhon;

    invoke-virtual {v2}, Lhon;->c()Ljava/util/List;

    move-result-object v2

    sget-boolean v3, Lhyb;->a:Z

    if-eqz v3, :cond_0

    const-string v3, "GeofenceStateCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Recovered PendingIntent cache. Cache Id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lhxq;->a:Lhon;

    invoke-virtual {v5}, Lhon;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " cache="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v3, Liln;

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lhxq;->e:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v4, p2}, Liln;-><init>(Ljava/io/File;Lhpk;)V

    iput-object v3, p0, Lhxq;->b:Liln;

    :try_start_0
    iget-object v0, p0, Lhxq;->b:Liln;

    new-instance v3, Lhyh;

    invoke-direct {v3}, Lhyh;-><init>()V

    invoke-virtual {v0, v3}, Liln;->a(Lizk;)Lizk;

    move-result-object v0

    check-cast v0, Lhyh;

    invoke-direct {p0, v0, v2, v1}, Lhxq;->a(Lhyh;Ljava/util/List;Landroid/content/pm/PackageManager;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GeofenceStateCache"

    const-string v1, "Unable to load state, all registered geofences are lost."

    invoke-static {v0, v1}, Lhyb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "GeofenceStateCache"

    const-string v1, "Unable to do clean up after restarted."

    invoke-static {v0, v1}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    iget-object v1, p0, Lhxq;->c:Lhxr;

    iget-object v0, v1, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxt;

    invoke-virtual {v0, p1}, Lhxt;->a(Ljava/io/PrintWriter;)V

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v1, Lhxr;->d:Lhxk;

    invoke-interface {v0, p1}, Lhxk;->a(Ljava/io/PrintWriter;)V

    return-void
.end method

.method public final b()V
    .locals 7

    iget-object v0, p0, Lhxq;->a:Lhon;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhxq;->a:Lhon;

    invoke-virtual {v0}, Lhon;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;

    invoke-virtual {v0}, Lcom/google/android/location/geofencer/data/PendingIntentCacheItem;->b()Landroid/app/PendingIntent;

    move-result-object v0

    sget-boolean v1, Lhyb;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "GeofenceStateCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending error 1000"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v1, "gms_error_code"

    const/16 v2, 0x3e8

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lhxq;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :try_start_0
    iget-object v1, p0, Lhxq;->e:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lhxq;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhxq;->c:Lhxr;

    iget-object v1, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, v0, Lhxr;->d:Lhxk;

    iget-object v2, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0}, Lhxr;->c()I

    move-result v0

    invoke-interface {v1, v2, v0}, Lhxk;->a(Ljava/lang/Iterable;I)V

    :try_start_1
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    return-void

    :catch_1
    move-exception v0

    iget-object v0, p0, Lhxq;->a:Lhon;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhxq;->a:Lhon;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lhon;->a(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method public final c()Z
    .locals 8

    iget-object v4, p0, Lhxq;->c:Lhxr;

    iget-object v0, p0, Lhxq;->f:Lbpe;

    invoke-interface {v0}, Lbpe;->b()J

    move-result-wide v5

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, v4, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhxt;

    invoke-virtual {v1, v5, v6}, Lhxt;->a(J)Z

    move-result v1

    or-int/2addr v3, v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhxt;

    invoke-virtual {v1}, Lhxt;->a()I

    move-result v1

    if-nez v1, :cond_5

    if-nez v2, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v2, v1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, v4, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_1
    if-eqz v3, :cond_2

    iget-object v0, v4, Lhxr;->d:Lhxk;

    iget-object v1, v4, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v4}, Lhxr;->c()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lhxk;->a(Ljava/lang/Iterable;I)V

    :cond_2
    if-eqz v3, :cond_3

    :try_start_0
    invoke-direct {p0}, Lhxq;->f()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_4
    return v3

    :catch_0
    move-exception v0

    goto :goto_4

    :cond_4
    move-object v1, v2

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lhxq;->c:Lhxr;

    invoke-virtual {v0}, Lhxr;->c()I

    move-result v0

    return v0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lhxq;->c:Lhxr;

    iget-object v0, v0, Lhxr;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lhxq;->g:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method
