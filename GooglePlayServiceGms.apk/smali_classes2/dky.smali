.class final Ldky;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Ldkx;


# direct methods
.method private constructor <init>(Ldkx;)V
    .locals 0

    iput-object p1, p0, Ldky;->a:Ldkx;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ldkx;B)V
    .locals 0

    invoke-direct {p0, p1}, Ldky;-><init>(Ldkx;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    iget-object v1, p0, Ldky;->a:Ldkx;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldky;->a:Ldkx;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Ldky;->a:Ldkx;

    iget-object v1, p0, Ldky;->a:Ldkx;

    iget-object v1, v1, Ldkx;->e:Landroid/net/LocalServerSocket;

    invoke-virtual {v1}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v1

    iput-object v1, v0, Ldkx;->f:Landroid/net/LocalSocket;

    iget-object v1, p0, Ldky;->a:Ldkx;

    iget-object v0, v1, Ldkx;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v1, Ldkx;->c:Ljava/lang/String;

    aput-object v3, v0, v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v2, v1, Ldkx;->f:Landroid/net/LocalSocket;

    invoke-virtual {v2}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iget-object v3, v1, Ldkx;->f:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getReceiveBufferSize()I

    move-result v3

    const/16 v4, 0x490

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-array v3, v3, [B

    :goto_0
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_0

    const-string v5, "SocketProxy"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Forwarding "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " bytes."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Ldav;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-array v5, v4, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v6, v5, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v4, v1, Ldkx;->a:Ldkk;

    invoke-virtual {v4, v5, v0}, Ldkk;->a([B[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v2, v1, Ldkx;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {v1}, Ldkx;->a()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "SocketProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IOException in readLoop"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldav;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldky;->a:Ldkx;

    iget-object v0, v0, Ldkx;->a:Ldkk;

    iget-object v1, p0, Ldky;->a:Ldkx;

    iget-object v1, v1, Ldkx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldkk;->b(Ljava/lang/String;)V

    :goto_1
    return-void

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_4
    iget-object v0, v1, Ldkx;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    invoke-virtual {v1}, Ldkx;->a()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method
