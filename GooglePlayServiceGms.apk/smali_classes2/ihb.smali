.class public final Lihb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:J

.field final b:Lihd;

.field final c:Lihd;

.field public final d:Lihd;

.field public final e:Lihd;

.field final f:Lidu;


# direct methods
.method public constructor <init>(Lidu;Lids;JJ)V
    .locals 14

    new-instance v0, Lihd;

    const-string v1, "bandwidth"

    const-wide/16 v2, -0x1

    invoke-virtual/range {p2 .. p2}, Lids;->q()Lidt;

    move-result-object v4

    move-wide/from16 v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lihd;-><init>(Ljava/lang/String;JLidt;JJ)V

    new-instance v1, Lihd;

    const-string v2, "general-gps"

    const-wide/16 v3, -0x1

    invoke-virtual/range {p2 .. p2}, Lids;->r()Lidt;

    move-result-object v5

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lihd;-><init>(Ljava/lang/String;JLidt;JJ)V

    new-instance v2, Lihd;

    const-string v3, "sensor-gps"

    const-wide/16 v4, -0x1

    invoke-virtual/range {p2 .. p2}, Lids;->s()Lidt;

    move-result-object v6

    move-wide/from16 v7, p3

    move-wide/from16 v9, p5

    invoke-direct/range {v2 .. v10}, Lihd;-><init>(Ljava/lang/String;JLidt;JJ)V

    new-instance v3, Lihd;

    const-string v4, "burst-gps"

    const-wide/16 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lids;->t()Lidt;

    move-result-object v7

    move-wide/from16 v8, p3

    move-wide/from16 v10, p5

    invoke-direct/range {v3 .. v11}, Lihd;-><init>(Ljava/lang/String;JLidt;JJ)V

    move-object v4, p0

    move-object v5, p1

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-wide/from16 v10, p3

    move-wide/from16 v12, p5

    invoke-direct/range {v4 .. v13}, Lihb;-><init>(Lidu;Lihd;Lihd;Lihd;Lihd;JJ)V

    return-void
.end method

.method private constructor <init>(Lidu;Lihd;Lihd;Lihd;Lihd;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lihb;->f:Lidu;

    iput-wide p6, p0, Lihb;->a:J

    iput-object p2, p0, Lihb;->b:Lihd;

    iput-object p3, p0, Lihb;->c:Lihd;

    iput-object p4, p0, Lihb;->d:Lihd;

    iput-object p5, p0, Lihb;->e:Lihd;

    invoke-virtual {p0, p8, p9}, Lihb;->b(J)V

    return-void
.end method

.method private static a(Lihd;Livi;I)V
    .locals 2

    new-instance v0, Livi;

    sget-object v1, Lihj;->aI:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    invoke-virtual {p0, v0}, Lihd;->a(Livi;)V

    invoke-virtual {p1, p2, v0}, Livi;->b(ILivi;)Livi;

    return-void
.end method

.method static a(Ljava/io/Closeable;)V
    .locals 1

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lihb;->f:Lidu;

    invoke-interface {v0}, Lidu;->o()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lihc;

    invoke-direct {v1, p0, p1, p2}, Lihc;-><init>(Lihb;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(JLjava/io/ByteArrayOutputStream;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    new-instance v1, Livi;

    sget-object v2, Lihj;->aH:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    iget-wide v3, p0, Lihb;->a:J

    invoke-virtual {v1, v2, v3, v4}, Livi;->a(IJ)Livi;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1, p2}, Livi;->a(IJ)Livi;

    iget-object v2, p0, Lihb;->b:Lihd;

    const/4 v3, 0x3

    invoke-static {v2, v1, v3}, Lihb;->a(Lihd;Livi;I)V

    iget-object v2, p0, Lihb;->c:Lihd;

    const/4 v3, 0x4

    invoke-static {v2, v1, v3}, Lihb;->a(Lihd;Livi;I)V

    iget-object v2, p0, Lihb;->d:Lihd;

    const/4 v3, 0x5

    invoke-static {v2, v1, v3}, Lihb;->a(Lihd;Livi;I)V

    iget-object v2, p0, Lihb;->e:Lihd;

    const/4 v3, 0x6

    invoke-static {v2, v1, v3}, Lihb;->a(Lihd;Livi;I)V

    invoke-virtual {v1}, Livi;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lihb;->b:Lihd;

    invoke-virtual {v0, p1, p2}, Lihd;->a(J)V

    iget-object v0, p0, Lihb;->c:Lihd;

    invoke-virtual {v0, p1, p2}, Lihd;->a(J)V

    iget-object v0, p0, Lihb;->d:Lihd;

    invoke-virtual {v0, p1, p2}, Lihd;->a(J)V

    iget-object v0, p0, Lihb;->e:Lihd;

    invoke-virtual {v0, p1, p2}, Lihd;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
