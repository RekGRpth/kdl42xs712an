.class public final Lfma;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

.field public final b:Lflw;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lflw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfma;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iput-object p2, p0, Lfma;->b:Lflw;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lflw;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lfma;-><init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lflw;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lfma;->b:Lflw;

    iget-boolean v1, v0, Lflw;->c:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only one call to delete() or save() is permitted."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflw;->c:Z

    iget-object v1, v0, Lflw;->d:Lflt;

    iget-object v0, v0, Lflw;->a:Ljava/io/File;

    invoke-static {v1, v0}, Lflt;->a(Lflt;Ljava/io/File;)V

    return-void
.end method
