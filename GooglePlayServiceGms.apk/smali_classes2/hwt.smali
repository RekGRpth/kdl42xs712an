.class final Lhwt;
.super Lhww;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhws;

.field private c:Landroid/location/Location;

.field private d:J

.field private e:J

.field private f:I


# direct methods
.method private constructor <init>(Lhws;)V
    .locals 1

    iput-object p1, p0, Lhwt;->a:Lhws;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhww;-><init>(Lhws;B)V

    const/4 v0, 0x0

    iput-object v0, p0, Lhwt;->c:Landroid/location/Location;

    return-void
.end method

.method synthetic constructor <init>(Lhws;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhwt;-><init>(Lhws;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 6

    iget v0, p0, Lhwt;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhwt;->f:I

    iget-object v0, p0, Lhwt;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v0

    iget-wide v2, p0, Lhwt;->e:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lhwt;->c:Landroid/location/Location;

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x3

    iget-object v4, p0, Lhwt;->a:Lhws;

    iget-object v4, v4, Lhws;->e:Lhwm;

    iget-wide v4, v4, Lhwm;->b:J

    mul-long/2addr v2, v4

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lhwt;->c:Landroid/location/Location;

    invoke-virtual {v0, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v0

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lhwt;->f:I

    iput-object p1, p0, Lhwt;->c:Landroid/location/Location;

    iget-object v0, p0, Lhwt;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v0

    iput-wide v0, p0, Lhwt;->d:J

    :cond_1
    iget-object v0, p0, Lhwt;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {p1}, Lhuz;->a(Landroid/location/Location;)J

    move-result-wide v0

    iput-wide v0, p0, Lhwt;->e:J

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhwt;->a(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    iget-object v0, p0, Lhwt;->a:Lhws;

    iget-object v0, v0, Lhws;->d:Lhuz;

    invoke-static {}, Lhuz;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lhwt;->d:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lhwt;->c:Landroid/location/Location;

    if-eqz v2, :cond_0

    iget v2, p0, Lhwt;->f:I

    const/4 v3, 0x5

    if-lt v2, v3, :cond_0

    const-wide v2, 0x4a817c800L

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lhwt;->a:Lhws;

    invoke-static {v0}, Lhws;->a(Lhws;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lhwv;

    iget-object v1, p0, Lhwt;->a:Lhws;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhwv;-><init>(Lhws;B)V

    invoke-virtual {p0, v0}, Lhwt;->a(Lhww;)Lhww;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhww;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhwt;->a:Lhws;

    invoke-virtual {v0, p1}, Lhws;->a(Z)V

    goto :goto_0
.end method
