.class public final Lhyi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field final b:Landroid/content/Context;

.field final c:Lbpe;

.field final d:I

.field final e:Ljava/lang/String;

.field final f:Lhyj;

.field final g:Lhzo;

.field final h:Ljava/lang/Object;

.field i:Liln;

.field j:I

.field k:Ljava/util/Collection;

.field private final l:Lhzr;

.field private final m:Lhyt;

.field private final n:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhyn;Lbpe;Lhyt;Lhzr;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lhyi;->a:Z

    new-instance v0, Lhyj;

    invoke-direct {v0, p0}, Lhyj;-><init>(Lhyi;)V

    iput-object v0, p0, Lhyi;->f:Lhyj;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhyi;->h:Ljava/lang/Object;

    const/4 v0, -0x1

    iput v0, p0, Lhyi;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lhyi;->k:Ljava/util/Collection;

    iput-object p1, p0, Lhyi;->b:Landroid/content/Context;

    iput-object p3, p0, Lhyi;->c:Lbpe;

    iput-object p4, p0, Lhyi;->m:Lhyt;

    new-instance v0, Lhzo;

    invoke-direct {v0, p2, p3, p4}, Lhzo;-><init>(Lhyn;Lbpe;Lhyt;)V

    iput-object v0, p0, Lhyi;->g:Lhzo;

    iput-object p5, p0, Lhyi;->l:Lhzr;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lhyi;->d:I

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhyi;->e:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    const-class v2, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lhyi;->l:Lhzr;

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v3, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lhyi;->n:Landroid/app/PendingIntent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.location.internal.server.ACTION_RESTARTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lhyi;->f:Lhyj;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {p1}, Lce;->a(Landroid/content/Context;)Lce;

    move-result-object v0

    iget-object v1, p0, Lhyi;->f:Lhyj;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.location.geofencer.service.ACTION_ACTIVITY_RESULT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lce;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private b()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lhpi;->b:Lhpi;

    invoke-static {v1}, Lhpg;->a(Lhpi;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    iget-object v2, p0, Lhyi;->n:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_REMOVE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    const/4 v3, -0x1

    iget-object v1, p0, Lhyi;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "ActivityDetector"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ActivityDetector"

    const-string v2, "cancelActivityDetection"

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lhyi;->j:I

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lhyi;->g:Lhzo;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lhzo;->a(Z)V

    iget-object v0, p0, Lhyi;->m:Lhyt;

    invoke-virtual {v0}, Lhyt;->e()V

    const/4 v0, -0x1

    iput v0, p0, Lhyi;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Lhyi;->k:Ljava/util/Collection;

    :cond_1
    iget-object v0, p0, Lhyi;->n:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lhyi;->b()V

    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(IZLjava/util/Collection;)V
    .locals 6

    iget-object v0, p0, Lhyi;->n:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhyi;->b()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lhpi;->b:Lhpi;

    invoke-static {v1}, Lhpg;->a(Lhpi;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_PERIOD_MILLIS"

    int-to-long v2, p1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_PENDING_INTENT"

    iget-object v2, p0, Lhyi;->n:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.internal.EXTRA_ACTIVITY_FORCE_DETECTION_NOW"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p3}, Lile;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v1

    const-string v2, "com.google.android.location.internal.EXTRA_LOCATION_WORK_SOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lhyi;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
