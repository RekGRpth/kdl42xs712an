.class public Ligz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ligz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ligz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;
    .locals 8

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {p1}, Ligh;->a(Lcom/google/android/gms/location/places/PlaceFilter;)Livi;

    move-result-object v0

    new-instance v3, Livi;

    sget-object v4, Lihj;->aq:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    iget-object v4, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Livi;->b(ILjava/lang/String;)Livi;

    if-eqz v0, :cond_0

    invoke-virtual {v3, v7, v0}, Livi;->b(ILivi;)Livi;

    :cond_0
    sget-object v0, Lhjh;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v4, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    const/16 v5, 0x80

    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v4, "com.google.android.geo.API_KEY"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lbox;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    if-eqz v4, :cond_3

    move v0, v1

    :goto_0
    const-string v6, "API key not found.  Check that <meta-data android:name=\"com.google.android.geo.API_KEY\" android:value=\"your API key\"/> is in the <application> element of AndroidManifest.xml"

    invoke-static {v0, v6}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz v5, :cond_1

    move v2, v1

    :cond_1
    const-string v0, "Package is not signed."

    invoke-static {v2, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Livi;

    sget-object v2, Lihj;->z:Livk;

    invoke-direct {v0, v2}, Livi;-><init>(Livk;)V

    iget-object v2, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    invoke-virtual {v0, v7, v4}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v5}, Livi;->b(ILjava/lang/String;)Livi;

    const/16 v1, 0x9

    invoke-virtual {v3, v1, v0}, Livi;->b(ILivi;)Livi;

    :cond_2
    return-object v3

    :catch_0
    move-exception v0

    sget-object v1, Ligz;->a:Ljava/lang/String;

    const-string v2, "Package name not found"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Package name not found"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;
    .locals 11

    const/4 v10, 0x3

    const-wide v8, 0x416312d000000000L    # 1.0E7

    const/4 v7, 0x1

    invoke-static {p0, p2, p3}, Ligz;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->ak:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    new-instance v2, Landroid/location/Location;

    const-string v3, "places api"

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v2, v3, v4}, Landroid/location/Location;->setLatitude(D)V

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v2, v3, v4}, Landroid/location/Location;->setLongitude(D)V

    new-instance v3, Livi;

    sget-object v4, Lihj;->i:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    mul-double/2addr v4, v8

    double-to-int v4, v4

    invoke-virtual {v3, v7, v4}, Livi;->e(II)Livi;

    const/4 v4, 0x2

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    mul-double/2addr v5, v8

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    new-instance v4, Livi;

    sget-object v5, Lihj;->r:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    invoke-virtual {v4, v7, v3}, Livi;->b(ILivi;)Livi;

    const/4 v3, 0x6

    invoke-virtual {v2}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    invoke-virtual {v4, v3, v5, v6}, Livi;->a(IJ)Livi;

    invoke-virtual {v2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4, v10, v3}, Livi;->e(II)Livi;

    :cond_0
    invoke-virtual {v2}, Landroid/location/Location;->hasAltitude()Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0xa

    invoke-virtual {v2}, Landroid/location/Location;->getAltitude()D

    move-result-wide v5

    double-to-int v5, v5

    invoke-virtual {v4, v3, v5}, Livi;->e(II)Livi;

    :cond_1
    invoke-virtual {v2}, Landroid/location/Location;->hasBearing()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0xd

    invoke-virtual {v2}, Landroid/location/Location;->getBearing()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v4, v3, v5}, Livi;->e(II)Livi;

    :cond_2
    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x10

    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v2

    invoke-virtual {v4, v3, v2}, Livi;->a(IF)Livi;

    :cond_3
    invoke-virtual {v1, v7, v4}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v0, v10, v1}, Livi;->b(ILivi;)Livi;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const-wide v6, 0x416312d000000000L    # 1.0E7

    invoke-static {p0, p3, p4}, Ligz;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->am:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    invoke-virtual {v1, v9, p2}, Livi;->e(II)Livi;

    if-eqz p1, :cond_0

    new-instance v2, Livi;

    sget-object v3, Lihj;->i:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v3, v6

    double-to-int v3, v3

    invoke-virtual {v2, v8, v3}, Livi;->e(II)Livi;

    iget-object v3, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v3, v6

    double-to-int v3, v3

    invoke-virtual {v2, v9, v3}, Livi;->e(II)Livi;

    new-instance v3, Livi;

    sget-object v4, Lihj;->i:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    iget-object v4, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v3, v8, v4}, Livi;->e(II)Livi;

    iget-object v4, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v3, v9, v4}, Livi;->e(II)Livi;

    new-instance v4, Livi;

    sget-object v5, Lihj;->l:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    invoke-virtual {v4, v8, v2}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v4, v9, v3}, Livi;->b(ILivi;)Livi;

    invoke-virtual {v1, v8, v4}, Livi;->b(ILivi;)Livi;

    :cond_0
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Livi;->b(ILivi;)Livi;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Ligz;->a(Landroid/content/Context;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;)Livi;

    move-result-object v0

    new-instance v1, Livi;

    sget-object v2, Lihj;->an:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Livi;->b(ILivi;)Livi;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/location/places/internal/PlacesParams;Livi;)Livi;
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Livi;

    sget-object v1, Lihj;->Q:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {v1}, Life;->a(Ljava/lang/String;)Livi;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Livi;->b(ILivi;)Livi;

    iget-object v1, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Livi;

    sget-object v2, Lihj;->z:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Livi;->b(ILivi;)Livi;

    :cond_0
    new-instance v1, Livi;

    sget-object v2, Lihj;->ar:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    invoke-virtual {v1, v3, p1}, Livi;->a(ILivi;)V

    new-instance v2, Livi;

    sget-object v3, Lihj;->F:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/16 v3, 0xf

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Livi;->a(ILivi;)V

    return-object v0
.end method
