.class public final Lgbu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lfsy;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgbu;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgbu;->c:Ljava/util/List;

    iput-object p3, p0, Lgbu;->b:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 7

    const/4 v6, 0x7

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    :try_start_0
    iget-object v0, p0, Lgbu;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgbu;->c:Ljava/util/List;

    invoke-virtual {p2, v0, v1}, Lfrx;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iget-object v1, p0, Lgbu;->b:Lfsy;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgbu;->b:Lfsy;

    invoke-static {v5, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgbu;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgbu;->b:Lfsy;

    invoke-static {v5, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v3}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgbu;->b:Lfsy;

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    :catch_3
    move-exception v0

    iget-object v0, p0, Lgbu;->b:Lfsy;

    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgbu;->b:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbu;->b:Lfsy;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lfsy;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    :cond_0
    return-void
.end method
