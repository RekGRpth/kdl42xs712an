.class public final enum Lcec;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcec;

.field public static final enum b:Lcec;

.field public static final enum c:Lcec;

.field public static final enum d:Lcec;

.field public static final enum e:Lcec;

.field public static final enum f:Lcec;

.field public static final enum g:Lcec;

.field public static final enum h:Lcec;

.field public static final enum i:Lcec;

.field public static final enum j:Lcec;

.field public static final enum k:Lcec;

.field public static final enum l:Lcec;

.field public static final enum m:Lcec;

.field public static final enum n:Lcec;

.field private static final synthetic p:[Lcec;


# instance fields
.field private final o:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    new-instance v0, Lcec;

    const-string v1, "CONTENT_ETAG"

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "contentETag"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->a:Lcec;

    new-instance v0, Lcec;

    const-string v1, "CONTENT_TYPE"

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "contentType"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v8, v3, Lcei;->g:Z

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->b:Lcec;

    new-instance v0, Lcec;

    const-string v1, "ENCRYPTION_KEY"

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "encryptionKey"

    sget-object v5, Lcek;->d:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->c:Lcec;

    new-instance v0, Lcec;

    const-string v1, "ENCRYPTION_ALGORITHM"

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "encryptionAlgorithm"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->d:Lcec;

    new-instance v0, Lcec;

    const-string v1, "OWNED_FILE_PATH"

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "filePath"

    sget-object v5, Lcek;->c:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v2, v8, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->e:Lcec;

    new-instance v0, Lcec;

    const-string v1, "NOT_OWNED_FILE_PATH"

    const/4 v2, 0x5

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "notOwnedFilePath"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->f:Lcec;

    new-instance v0, Lcec;

    const-string v1, "LAST_OPENED_TIME"

    const/4 v2, 0x6

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "lastOpenedTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->g:Lcec;

    new-instance v0, Lcec;

    const-string v1, "LAST_MODIFIED_TIME"

    const/4 v2, 0x7

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "lastModifiedTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->h:Lcec;

    new-instance v0, Lcec;

    const-string v1, "SERVER_SIDE_LAST_MODIFIED_TIME"

    const/16 v2, 0x8

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "serverSideLastModifiedTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->i:Lcec;

    new-instance v0, Lcec;

    const-string v1, "MD5_CHECKSUM"

    const/16 v2, 0x9

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "md5Checksum"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->j:Lcec;

    new-instance v0, Lcec;

    const-string v1, "IS_TEMPORARY"

    const/16 v2, 0xa

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isTemporary"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->k:Lcec;

    new-instance v0, Lcec;

    const-string v1, "REFERENCED_CONTENT_ID"

    const/16 v2, 0xb

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "referencedContentId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Lcej;->b:Lcej;

    invoke-virtual {v4, v5, v6, v7}, Lcei;->a(Lcdt;Lcdp;Lcej;)Lcei;

    move-result-object v4

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->l:Lcec;

    new-instance v0, Lcec;

    const-string v1, "IS_DIRTY"

    const/16 v2, 0xc

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isDirty"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v8, v4, Lcei;->g:Z

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->m:Lcec;

    new-instance v0, Lcec;

    const-string v1, "CREATOR_IDENTITY"

    const/16 v2, 0xd

    invoke-static {}, Lceb;->d()Lceb;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "creatorIdentity"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lceb;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    iput-boolean v8, v4, Lcei;->g:Z

    invoke-virtual {v3, v8, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcec;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcec;->n:Lcec;

    const/16 v0, 0xe

    new-array v0, v0, [Lcec;

    sget-object v1, Lcec;->a:Lcec;

    aput-object v1, v0, v9

    sget-object v1, Lcec;->b:Lcec;

    aput-object v1, v0, v8

    sget-object v1, Lcec;->c:Lcec;

    aput-object v1, v0, v10

    sget-object v1, Lcec;->d:Lcec;

    aput-object v1, v0, v11

    sget-object v1, Lcec;->e:Lcec;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcec;->f:Lcec;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcec;->g:Lcec;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcec;->h:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcec;->i:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcec;->j:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcec;->k:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcec;->l:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcec;->m:Lcec;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcec;->n:Lcec;

    aput-object v2, v0, v1

    sput-object v0, Lcec;->p:[Lcec;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcec;->o:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcec;
    .locals 1

    const-class v0, Lcec;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcec;

    return-object v0
.end method

.method public static values()[Lcec;
    .locals 1

    sget-object v0, Lcec;->p:[Lcec;

    invoke-virtual {v0}, [Lcec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcec;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcec;->o:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcec;->o:Lcdp;

    return-object v0
.end method
