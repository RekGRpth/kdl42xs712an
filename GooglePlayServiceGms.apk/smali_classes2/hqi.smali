.class final Lhqi;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhqh;


# direct methods
.method constructor <init>(Lhqh;)V
    .locals 0

    iput-object p1, p0, Lhqi;->a:Lhqh;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCellLocationChanged(Landroid/telephony/CellLocation;)V
    .locals 1

    iget-object v0, p0, Lhqi;->a:Lhqh;

    invoke-virtual {v0}, Lhqh;->f()V

    iget-object v0, p0, Lhqi;->a:Lhqh;

    invoke-static {v0, p1}, Lhqh;->a(Lhqh;Landroid/telephony/CellLocation;)V

    return-void
.end method

.method public final onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 0

    return-void
.end method

.method public final onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    iget-object v0, p0, Lhqi;->a:Lhqh;

    invoke-virtual {v0}, Lhqh;->f()V

    iget-object v1, p0, Lhqi;->a:Lhqh;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    :goto_0
    iput v0, v1, Lhqh;->b:I

    iget-object v0, p0, Lhqi;->a:Lhqh;

    iget-object v1, p0, Lhqi;->a:Lhqh;

    iget-object v1, v1, Lhqh;->a:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v1

    invoke-static {v0, v1}, Lhqh;->a(Lhqh;Landroid/telephony/CellLocation;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    goto :goto_0
.end method
