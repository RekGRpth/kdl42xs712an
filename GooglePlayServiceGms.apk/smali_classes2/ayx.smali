.class public final Layx;
.super Lod;
.source "SourceFile"

# interfaces
.implements Layi;
.implements Lazp;


# instance fields
.field private A:Z

.field private B:Lawv;

.field private final C:Ljava/util/List;

.field private D:Lazc;

.field public final a:Lcom/google/android/gms/cast/CastDevice;

.field b:Lbac;

.field c:D

.field d:Ljava/lang/String;

.field e:Lazo;

.field public f:I

.field public g:Ljava/lang/String;

.field h:Z

.field public i:Z

.field j:Lcom/google/android/gms/cast_mirroring/JGCastService;

.field public final k:Laye;

.field final synthetic l:Layl;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:Z

.field private p:Lazd;

.field private q:Lazd;

.field private r:Lazd;

.field private s:Lazd;

.field private t:Lazd;

.field private u:Lazd;

.field private v:Lazd;

.field private w:Landroid/app/PendingIntent;

.field private x:Layf;

.field private y:J

.field private final z:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method public constructor <init>(Layl;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput-object p1, p0, Layx;->l:Layl;

    invoke-direct {p0}, Lod;-><init>()V

    sget-object v0, Layl;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Layx;->m:Ljava/lang/String;

    iput-boolean v4, p0, Layx;->n:Z

    new-instance v0, Lbbj;

    const-string v1, "CastRouteController"

    invoke-direct {v0, v1}, Lbbj;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Layx;->k:Laye;

    const-string v0, "instance-%d"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {}, Layl;->b()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Layx;->k:Laye;

    invoke-virtual {v1, v0}, Laye;->a(Ljava/lang/String;)V

    iput-object p2, p0, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Layx;->c:D

    iput v3, p0, Layx;->f:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Layx;->C:Ljava/util/List;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Layx;->y:J

    iget-object v1, p1, Lnz;->a:Landroid/content/Context;

    const-string v0, "wifi"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".CastRouteController"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Layx;->z:Landroid/net/wifi/WifiManager$WifiLock;

    iget-object v0, p0, Layx;->z:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0, v3}, Landroid/net/wifi/WifiManager$WifiLock;->setReferenceCounted(Z)V

    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 3

    const/4 v1, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const-string v0, "httpStatus"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :try_start_0
    const-string v0, "httpStatus"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v1, "android.media.status.extra.HTTP_STATUS_CODE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    const-string v1, "httpHeaders"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :try_start_2
    const-string v1, "httpHeaders"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lazu;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    if-nez v0, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v1

    :cond_1
    const-string v1, "android.media.status.extra.HTTP_RESPONSE_HEADERS"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_2
    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->h()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "com.google.android.gms.cast.EXTRA_CAST_STOP_APPLICATION_WHEN_SESSION_ENDS"

    iget-boolean v4, p0, Layx;->o:Z

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Layx;->o:Z

    const-string v3, "com.google.android.gms.cast.EXTRA_CAST_RELAUNCH_APPLICATION"

    iget-boolean v4, p0, Layx;->n:Z

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Layx;->n:Z

    const-string v3, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.google.android.gms.cast.EXTRA_DEBUG_LOGGING_ENABLED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v3, 0x1

    or-long/2addr v0, v3

    :goto_0
    iget-object v3, p0, Layx;->k:Laye;

    invoke-virtual {v3, v2}, Laye;->a(Z)V

    :cond_0
    iget-object v2, p0, Layx;->b:Lbac;

    invoke-virtual {v2, v0, v1}, Lbac;->a(J)V

    return-void

    :cond_1
    const-wide/16 v3, -0x2

    and-long/2addr v0, v3

    goto :goto_0
.end method

.method private a(Lazc;)V
    .locals 1

    iget-object v0, p0, Layx;->D:Lazc;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->D:Lazc;

    :cond_0
    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lazc;ILandroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "sendPlaybackStateForItem for item: %s, playbackState: %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p1, Lazc;->d:Landroid/app/PendingIntent;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.media.intent.extra.ITEM_ID"

    iget-object v2, p1, Lazc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lnv;

    invoke-direct {v1, p2}, Lnv;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lnv;->a(J)Lnv;

    move-result-object v1

    if-eqz p3, :cond_1

    invoke-virtual {v1, p3}, Lnv;->a(Landroid/os/Bundle;)Lnv;

    :cond_1
    invoke-virtual {v1}, Lnv;->a()Lnu;

    move-result-object v1

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    iget-object v1, v1, Lnu;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p1, Lazc;->d:Landroid/app/PendingIntent;

    iget-object v2, p0, Layx;->l:Layl;

    iget-object v2, v2, Lnz;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lazd;)Z
    .locals 15

    const/4 v3, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "processRemotePlaybackRequest()"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p1

    iget-object v7, v0, Lazd;->a:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    const-string v1, "com.google.android.gms.cast.EXTRA_CUSTOM_DATA"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2e

    invoke-static {v1, v3}, Lazu;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v1

    move-object v2, v1

    :goto_0
    iget-object v1, p0, Layx;->k:Laye;

    const-string v4, "got remote playback request; action=%s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v10, v5, v9

    invoke-virtual {v1, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "android.media.intent.action.PLAY"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_18

    const-string v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v1, Layl;->i:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_0
    iput-object v1, p0, Layx;->m:Ljava/lang/String;

    :cond_1
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v8

    :goto_1
    return v1

    :cond_2
    invoke-virtual {v7}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_3

    move v1, v9

    goto :goto_1

    :cond_3
    iget-object v1, p0, Layx;->k:Laye;

    const-string v4, "Device received play request, uri %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v5, v6, v11

    invoke-virtual {v1, v4, v6}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    if-nez v6, :cond_5

    :cond_4
    :goto_2
    new-instance v1, Lawj;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lawj;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lawj;->a:Lawi;

    const/4 v5, 0x1

    iput v5, v4, Lawi;->b:I

    invoke-virtual {v7}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lawj;->a:Lawi;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_12

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "content type cannot be null or empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "can\'t process command; %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v9

    goto :goto_1

    :cond_5
    :try_start_1
    const-string v1, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "android.media.metadata.COMPOSER"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v1, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v4, v1

    :goto_3
    const-string v1, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v1, v3

    :goto_4
    if-nez v11, :cond_6

    if-nez v4, :cond_6

    if-eqz v1, :cond_11

    :cond_6
    new-instance v3, Lawk;

    const/4 v14, 0x3

    invoke-direct {v3, v14}, Lawk;-><init>(I)V

    if-eqz v11, :cond_7

    const-string v14, "com.google.android.gms.cast.metadata.ALBUM_TITLE"

    invoke-virtual {v3, v14, v11}, Lawk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-eqz v12, :cond_8

    const-string v11, "com.google.android.gms.cast.metadata.ALBUM_ARTIST"

    invoke-virtual {v3, v11, v12}, Lawk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    if-eqz v13, :cond_9

    const-string v11, "com.google.android.gms.cast.metadata.COMPOSER"

    invoke-virtual {v3, v11, v13}, Lawk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    if-eqz v4, :cond_a

    const-string v11, "com.google.android.gms.cast.metadata.DISC_NUMBER"

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v11, v4}, Lawk;->a(Ljava/lang/String;I)V

    :cond_a
    if-eqz v1, :cond_b

    const-string v4, "com.google.android.gms.cast.metadata.TRACK_NUMBER"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lawk;->a(Ljava/lang/String;I)V

    :cond_b
    :goto_5
    const-string v1, "android.media.metadata.TITLE"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_c

    const-string v4, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v3, v4, v1}, Lawk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v1, "android.media.metadata.ARTIST"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    const-string v4, "com.google.android.gms.cast.metadata.ARTIST"

    invoke-virtual {v3, v4, v1}, Lawk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    const-string v1, "android.media.metadata.YEAR"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "android.media.metadata.YEAR"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    const/4 v11, 0x1

    invoke-virtual {v4, v11, v1}, Ljava/util/Calendar;->set(II)V

    const-string v1, "com.google.android.gms.cast.metadata.RELEASE_DATE"

    invoke-virtual {v3, v1, v4}, Lawk;->a(Ljava/lang/String;Ljava/util/Calendar;)V

    :cond_e
    const-string v1, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v4, Lcom/google/android/gms/common/images/WebImage;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/google/android/gms/common/images/WebImage;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v3, v4}, Lawk;->a(Lcom/google/android/gms/common/images/WebImage;)V

    goto/16 :goto_2

    :cond_f
    move-object v4, v3

    goto/16 :goto_3

    :cond_10
    move-object v1, v3

    goto/16 :goto_4

    :cond_11
    new-instance v3, Lawk;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, Lawk;-><init>(I)V

    goto :goto_5

    :cond_12
    iput-object v4, v5, Lawi;->c:Ljava/lang/String;

    iget-object v4, v1, Lawj;->a:Lawi;

    iput-object v3, v4, Lawi;->d:Lawk;

    iget-object v3, v1, Lawj;->a:Lawi;

    iget-object v4, v3, Lawi;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_13

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "content ID cannot be null or empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_13
    iget-object v4, v3, Lawi;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_14

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "content type cannot be null or empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_14
    iget v3, v3, Lawi;->b:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_15

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "a valid stream type must be specified"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_15
    iget-object v3, v1, Lawj;->a:Lawi;

    const-string v1, "android.media.intent.extra.HTTP_HEADERS"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    if-eqz v1, :cond_2d

    const/4 v4, 0x0

    :try_start_2
    invoke-static {v1, v4}, Lazu;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v4

    if-nez v2, :cond_16

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    move-object v2, v1

    :cond_16
    const-string v1, "httpHeaders"

    invoke-virtual {v2, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v6, v2

    :goto_6
    :try_start_3
    const-string v1, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v4, 0x0

    invoke-virtual {v7, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/app/PendingIntent;

    move-object v7, v0
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0

    :try_start_4
    iget-object v1, p0, Layx;->x:Layf;

    move-object v2, p0

    invoke-virtual/range {v1 .. v6}, Layf;->a(Layi;Lawi;JLorg/json/JSONObject;)J

    move-result-wide v1

    new-instance v3, Lazc;

    invoke-direct {v3, p0, v1, v2}, Lazc;-><init>(Layx;J)V

    iput-object v7, v3, Lazc;->d:Landroid/app/PendingIntent;

    iget-object v1, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "loading media with item id assigned as %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v3, Lazc;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-direct {p0}, Layx;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, v3, Lazc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lnv;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Lnv;-><init>(I)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lnv;->a(J)Lnv;

    move-result-object v2

    invoke-virtual {v2}, Lnv;->a()Lnu;

    move-result-object v2

    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    iget-object v2, v2, Lnu;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(Landroid/os/Bundle;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_17
    :goto_7
    move v1, v8

    goto/16 :goto_1

    :catch_1
    move-exception v1

    move-object v6, v2

    goto :goto_6

    :catch_2
    move-exception v1

    :try_start_5
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto :goto_7

    :cond_18
    const-string v1, "android.media.intent.action.PAUSE"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_0

    move-result v1

    if-nez v1, :cond_19

    move v1, v8

    goto/16 :goto_1

    :cond_19
    :try_start_6
    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1, p0, v2}, Layf;->a(Layi;Lorg/json/JSONObject;)J

    move-result-wide v1

    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->t:Lazd;

    iget-object v3, p0, Layx;->t:Lazd;

    iput-wide v1, v3, Lazd;->c:J
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_7

    :catch_3
    move-exception v1

    :try_start_7
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto :goto_7

    :cond_1a
    const-string v1, "android.media.intent.action.RESUME"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_0

    move-result v1

    if-nez v1, :cond_1b

    move v1, v8

    goto/16 :goto_1

    :cond_1b
    :try_start_8
    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1, p0, v2}, Layf;->c(Layi;Lorg/json/JSONObject;)J

    move-result-wide v1

    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->u:Lazd;

    iget-object v3, p0, Layx;->u:Lazd;

    iput-wide v1, v3, Lazd;->c:J
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_7

    :catch_4
    move-exception v1

    :try_start_9
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_1c
    const-string v1, "android.media.intent.action.STOP"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_0

    move-result v1

    if-nez v1, :cond_1d

    move v1, v8

    goto/16 :goto_1

    :cond_1d
    :try_start_a
    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1, p0, v2}, Layf;->b(Layi;Lorg/json/JSONObject;)J

    move-result-wide v1

    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->v:Lazd;

    iget-object v3, p0, Layx;->v:Lazd;

    iput-wide v1, v3, Lazd;->c:J
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_7

    :catch_5
    move-exception v1

    :try_start_b
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_1e
    const-string v1, "android.media.intent.action.SEEK"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    move-result v1

    if-nez v1, :cond_1f

    move v1, v8

    goto/16 :goto_1

    :cond_1f
    const-string v1, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Layx;->d(Ljava/lang/String;)V

    const-string v1, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v3, 0x0

    invoke-virtual {v7, v1, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_0

    move-result-wide v3

    :try_start_c
    iget-object v1, p0, Layx;->k:Laye;

    const-string v5, "seeking to %d ms"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v6, v7

    invoke-virtual {v1, v5, v6}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1, p0, v3, v4, v2}, Layf;->a(Layi;JLorg/json/JSONObject;)J

    move-result-wide v1

    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->s:Lazd;

    iget-object v3, p0, Layx;->s:Lazd;

    iput-wide v1, v3, Lazd;->c:J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_7

    :catch_6
    move-exception v1

    :try_start_d
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_20
    const-string v1, "android.media.intent.action.GET_STATUS"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    move-result v1

    if-nez v1, :cond_21

    move v1, v8

    goto/16 :goto_1

    :cond_21
    const-string v1, "android.media.intent.extra.ITEM_ID"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Layx;->d(Ljava/lang/String;)V

    iget-object v1, p0, Layx;->x:Layf;

    if-eqz v1, :cond_22

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(Landroid/os/Bundle;)V

    goto/16 :goto_7

    :cond_22
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_23
    const-string v1, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    move-result v1

    if-nez v1, :cond_24

    move v1, v8

    goto/16 :goto_1

    :cond_24
    iget-object v1, p0, Layx;->x:Layf;
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_0

    if-eqz v1, :cond_26

    :try_start_e
    iget-wide v1, p0, Layx;->y:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_25

    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1, p0}, Layf;->a(Layi;)J

    move-result-wide v1

    iput-wide v1, p0, Layx;->y:J

    :cond_25
    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->r:Lazd;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7
    .catch Ljava/lang/IllegalStateException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_7

    :catch_7
    move-exception v1

    const/4 v2, 0x0

    :try_start_f
    iput-object v2, p0, Layx;->r:Lazd;

    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "exception while processing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_26
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto/16 :goto_7

    :cond_27
    const-string v1, "android.media.intent.action.START_SESSION"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    const-string v1, "com.google.android.gms.cast.EXTRA_CAST_APPLICATION_ID"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2c

    sget-object v1, Layl;->i:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    :goto_8
    const-string v1, "android.media.intent.extra.SESSION_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    if-nez v1, :cond_28

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "No status update receiver supplied to %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v9

    goto/16 :goto_1

    :cond_28
    iput-object v1, p0, Layx;->w:Landroid/app/PendingIntent;

    iput-object v2, p0, Layx;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v0, p0, Layx;->q:Lazd;

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    move-result v1

    if-nez v1, :cond_17

    move v1, v8

    goto/16 :goto_1

    :cond_29
    const-string v1, "android.media.intent.action.GET_SESSION_STATUS"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(Landroid/os/Bundle;)V

    goto/16 :goto_7

    :cond_2a
    const-string v1, "android.media.intent.action.END_SESSION"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v1}, Layx;->a(Lazd;I)Z

    invoke-direct {p0}, Layx;->i()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Layx;->b(Ljava/lang/String;I)V

    const/4 v1, 0x0

    iput-object v1, p0, Layx;->w:Landroid/app/PendingIntent;

    invoke-virtual {p0}, Layx;->e()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lazd;->a(Landroid/os/Bundle;)V
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_f .. :try_end_f} :catch_0

    goto/16 :goto_7

    :cond_2b
    move v1, v9

    goto/16 :goto_1

    :cond_2c
    move-object v2, v1

    goto/16 :goto_8

    :cond_2d
    move-object v6, v2

    goto/16 :goto_6

    :cond_2e
    move-object v2, v3

    goto/16 :goto_0
.end method

.method private a(Lazd;I)Z
    .locals 8

    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p1, Lazd;->a:Landroid/content/Intent;

    const-string v3, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Layx;->i()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Layx;->k:Laye;

    const-string v5, "checkSession() sessionId=%s, currentSessionId=%s"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v1

    aput-object v3, v6, v0

    invoke-virtual {v4, v5, v6}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v2, p0, Layx;->h:Z

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    iput-boolean v1, p0, Layx;->h:Z

    :goto_0
    return v0

    :cond_0
    if-ne p2, v0, :cond_5

    iput-object p1, p0, Layx;->p:Lazd;

    iput-boolean v0, p0, Layx;->h:Z

    iget-object v0, p1, Lazd;->a:Landroid/content/Intent;

    invoke-direct {p0, v0}, Layx;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Layx;->c(I)V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iput v7, p0, Layx;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->g:Ljava/lang/String;

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput-boolean v1, p0, Layx;->h:Z

    goto :goto_0

    :cond_3
    if-nez v3, :cond_5

    iget-object v0, p1, Lazd;->a:Landroid/content/Intent;

    invoke-direct {p0, v0}, Layx;->a(Landroid/content/Intent;)V

    iput-object p1, p0, Layx;->p:Lazd;

    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, v2}, Layx;->c(Ljava/lang/String;)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    iput v7, p0, Layx;->f:I

    iput-object v2, p0, Layx;->g:Ljava/lang/String;

    goto :goto_2

    :cond_5
    invoke-virtual {p1, v7}, Lazd;->a(I)V

    move v0, v1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)V
    .locals 6

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Layx;->w:Landroid/app/PendingIntent;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, p2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Invoking session status PendingIntent with: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Layx;->w:Landroid/app/PendingIntent;

    iget-object v2, p0, Layx;->l:Layl;

    iget-object v2, v2, Lnz;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private c(I)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Layl;->a()Laye;

    move-result-object v0

    const-string v1, "startSession()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->e:Lazo;

    if-nez v0, :cond_0

    new-instance v0, Lazo;

    iget-object v1, p0, Layx;->b:Lbac;

    iget-object v2, p0, Layx;->l:Layl;

    iget-object v2, v2, Lnz;->c:Lob;

    iget-object v3, p0, Layx;->k:Laye;

    invoke-virtual {v3}, Laye;->a()Z

    move-result v3

    invoke-direct {v0, v1, p0, v2, v3}, Lazo;-><init>(Lbac;Lazp;Landroid/os/Handler;Z)V

    iput-object v0, p0, Layx;->e:Lazo;

    :cond_0
    if-ne p1, v4, :cond_1

    iget-object v0, p0, Layx;->e:Lazo;

    const-string v1, "674A0243"

    invoke-virtual {v0, v1, v4}, Lazo;->a(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Layx;->e:Lazo;

    iget-object v1, p0, Layx;->m:Ljava/lang/String;

    iget-boolean v2, p0, Layx;->n:Z

    invoke-virtual {v0, v1, v2}, Lazo;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "resumeSession()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->e:Lazo;

    if-nez v0, :cond_0

    new-instance v0, Lazo;

    iget-object v1, p0, Layx;->b:Lbac;

    iget-object v2, p0, Layx;->l:Layl;

    iget-object v2, v2, Lnz;->c:Lob;

    iget-object v3, p0, Layx;->k:Laye;

    invoke-virtual {v3}, Laye;->a()Z

    move-result v3

    invoke-direct {v0, v1, p0, v2, v3}, Lazo;-><init>(Lbac;Lazp;Landroid/os/Handler;Z)V

    iput-object v0, p0, Layx;->e:Lazo;

    :cond_0
    iget-object v0, p0, Layx;->e:Lazo;

    iget-object v1, p0, Layx;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lazo;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazc;

    invoke-direct {p0, v0, p1, v2}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v2, p0, Layx;->D:Lazc;

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Layx;->D:Lazc;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no current item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Layx;->D:Lazc;

    iget-object v0, v0, Lazc;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "item ID does not match current item"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private e(I)Landroid/os/Bundle;
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Los;

    invoke-direct {v2, p1}, Los;-><init>(I)V

    iget-object v0, p0, Layx;->x:Layf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0}, Layf;->c()Lawm;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, v0, Lawm;->d:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v3, v2, Los;->a:Landroid/os/Bundle;

    const-string v4, "queuePaused"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Los;->a(J)Los;

    move-result-object v0

    new-instance v2, Lor;

    iget-object v0, v0, Los;->a:Landroid/os/Bundle;

    invoke-direct {v2, v0, v1}, Lor;-><init>(Landroid/os/Bundle;B)V

    iget-object v0, v2, Lor;->a:Landroid/os/Bundle;

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Layx;->e:Lazo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Layx;->e:Lazo;

    invoke-virtual {v0}, Lazo;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private j()Landroid/os/Bundle;
    .locals 6

    const/4 v0, 0x5

    const/4 v1, 0x7

    iget-object v2, p0, Layx;->x:Layf;

    invoke-virtual {v2}, Layf;->c()Lawm;

    move-result-object v2

    if-eqz v2, :cond_1

    iget v3, v2, Lawm;->d:I

    iget v4, v2, Lawm;->e:I

    packed-switch v3, :pswitch_data_0

    move v0, v1

    :goto_0
    :pswitch_0
    new-instance v1, Lnv;

    invoke-direct {v1, v0}, Lnv;-><init>(I)V

    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0}, Layf;->b()J

    move-result-wide v3

    iget-object v0, v1, Lnv;->a:Landroid/os/Bundle;

    const-string v5, "contentDuration"

    invoke-virtual {v0, v5, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0}, Layf;->a()J

    move-result-wide v3

    iget-object v0, v1, Lnv;->a:Landroid/os/Bundle;

    const-string v5, "contentPosition"

    invoke-virtual {v0, v5, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lnv;->a(J)Lnv;

    move-result-object v0

    iget-object v1, v2, Lawm;->g:Lorg/json/JSONObject;

    invoke-static {v1}, Layx;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lnv;->a(Landroid/os/Bundle;)Lnv;

    :cond_0
    invoke-virtual {v0}, Lnv;->a()Lnu;

    move-result-object v0

    iget-object v0, v0, Lnu;->a:Landroid/os/Bundle;

    :goto_1
    return-object v0

    :pswitch_1
    packed-switch v4, :pswitch_data_1

    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "*** media status is null!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lnv;

    invoke-direct {v1, v0}, Lnv;-><init>(I)V

    invoke-virtual {v1}, Lnv;->a()Lnu;

    move-result-object v0

    iget-object v0, v0, Lnu;->a:Landroid/os/Bundle;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized k()V
    .locals 3

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    sget-object v0, Laxa;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->l:Layl;

    iget-object v0, v0, Lnz;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->z:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized l()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Layx;->z:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->z:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onRelease"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->l:Layl;

    invoke-static {v0, p0}, Layl;->a(Layl;Layx;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->b:Lbac;

    invoke-direct {p0}, Layx;->l()V

    return-void
.end method

.method public final a(I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onSetVolume() volume=%d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->b:Lbac;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    int-to-double v0, p1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    div-double v1, v0, v2

    :try_start_0
    iget-object v0, p0, Layx;->b:Lbac;

    iget-wide v3, p0, Layx;->c:D

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lbac;->a(DDZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Unable to set volume: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 5

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onApplicationDisconnected: statusCode=%d, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->e:Lazo;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Layx;->e:Lazo;

    invoke-virtual {v0}, Lazo;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Layx;->e:Lazo;

    invoke-virtual {v0, p1}, Lazo;->b(I)V

    :cond_1
    return-void
.end method

.method public final a(JILorg/json/JSONObject;)V
    .locals 8

    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazc;

    iget-wide v2, v0, Lazc;->b:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    :goto_0
    if-eqz v0, :cond_3

    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1}, Layf;->h()J

    move-result-wide v1

    packed-switch p3, :pswitch_data_0

    :pswitch_0
    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "unknown status %d; sending error state"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x7

    invoke-static {p4}, Layx;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    invoke-direct {p0, v0}, Layx;->a(Lazc;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Layx;->k:Laye;

    const-string v4, "Load completed; mediaSessionId=%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v3, -0x1

    iput-wide v3, v0, Lazc;->b:J

    iput-wide v1, v0, Lazc;->c:J

    iput-object v0, p0, Layx;->D:Lazc;

    invoke-virtual {p0}, Layx;->h()V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "STATUS_CANCELED; sending error state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    invoke-direct {p0, v0}, Layx;->a(Lazc;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "STATUS_TIMED_OUT; sending error state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    invoke-direct {p0, v0}, Layx;->a(Lazc;)V

    goto :goto_1

    :cond_3
    iget-wide v0, p0, Layx;->y:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_d

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "requestStatus has completed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Layx;->y:J

    :try_start_0
    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0}, Layf;->h()J

    move-result-wide v1

    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazc;

    iget-wide v4, v0, Lazc;->c:J

    cmp-long v4, v4, v1

    if-nez v4, :cond_4

    :goto_2
    iget-object v3, p0, Layx;->D:Lazc;

    if-eqz v3, :cond_5

    iget-object v3, p0, Layx;->D:Lazc;

    if-eq v3, v0, :cond_5

    iget-object v0, p0, Layx;->D:Lazc;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {p0, v0, v3, v4}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    iget-object v0, p0, Layx;->D:Lazc;

    invoke-direct {p0, v0}, Layx;->a(Lazc;)V

    :cond_5
    iget-object v0, p0, Layx;->r:Lazd;

    if-eqz v0, :cond_6

    new-instance v3, Lazc;

    invoke-direct {v3, p0}, Lazc;-><init>(Layx;)V

    iput-wide v1, v3, Lazc;->c:J

    iget-object v0, p0, Layx;->r:Lazd;

    iget-object v0, v0, Lazd;->a:Landroid/content/Intent;

    const-string v1, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, v3, Lazc;->d:Landroid/app/PendingIntent;

    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v3, p0, Layx;->D:Lazc;

    :cond_6
    iget-object v0, p0, Layx;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazc;

    iget-wide v2, v0, Lazc;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    iget-object v2, p0, Layx;->D:Lazc;

    if-eqz v2, :cond_8

    iget-wide v2, v0, Lazc;->c:J

    iget-object v4, p0, Layx;->D:Lazc;

    iget-wide v4, v4, Lazc;->c:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_7

    :cond_8
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Layx;->a(Lazc;ILandroid/os/Bundle;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Layx;->d(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->D:Lazc;

    :cond_9
    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "mSyncStatusRequest = %s, status=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Layx;->r:Lazd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->r:Lazd;

    if-eqz v0, :cond_1

    if-nez p3, :cond_c

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "requestStatus completed; sending response"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Layx;->D:Lazc;

    if-eqz v1, :cond_a

    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v1}, Layf;->c()Lawm;

    move-result-object v1

    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Layx;->D:Lazc;

    iget-object v3, v3, Lazc;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, v1, Lawm;->b:Lawi;

    if-eqz v1, :cond_a

    invoke-static {v1}, Lazu;->a(Lawi;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "adding metadata bundle: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_a
    iget-object v1, p0, Layx;->r:Lazd;

    invoke-virtual {v1, v0}, Lazd;->a(Landroid/os/Bundle;)V

    :goto_4
    const/4 v0, 0x0

    iput-object v0, p0, Layx;->r:Lazd;

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Layx;->r:Lazd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    goto :goto_4

    :cond_d
    iget-object v0, p0, Layx;->s:Lazd;

    if-eqz v0, :cond_e

    iget-object v0, p0, Layx;->s:Lazd;

    iget-wide v0, v0, Lazd;->c:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_e

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Layx;->s:Lazd;

    invoke-virtual {v1, v0}, Lazd;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->s:Lazd;

    goto/16 :goto_1

    :cond_e
    iget-object v0, p0, Layx;->t:Lazd;

    if-eqz v0, :cond_f

    iget-object v0, p0, Layx;->t:Lazd;

    iget-wide v0, v0, Lazd;->c:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_f

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Layx;->t:Lazd;

    invoke-virtual {v1, v0}, Lazd;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->t:Lazd;

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Layx;->u:Lazd;

    if-eqz v0, :cond_10

    iget-object v0, p0, Layx;->u:Lazd;

    iget-wide v0, v0, Lazd;->c:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_10

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Layx;->u:Lazd;

    invoke-virtual {v1, v0}, Lazd;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->u:Lazd;

    goto/16 :goto_1

    :cond_10
    iget-object v0, p0, Layx;->v:Lazd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layx;->v:Lazd;

    iget-wide v0, v0, Lazd;->c:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "android.media.intent.extra.SESSION_STATUS"

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Layx;->e(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Layx;->v:Lazd;

    invoke-virtual {v1, v0}, Lazd;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->v:Lazd;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Layx;->e:Lazo;

    invoke-virtual {v0}, Lazo;->a()Lcom/google/android/gms/cast/ApplicationMetadata;

    move-result-object v0

    iget-object v1, p0, Layx;->q:Lazd;

    if-eqz v1, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_ID"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Layx;->q:Lazd;

    invoke-virtual {v2, v1}, Lazd;->a(Landroid/os/Bundle;)V

    iput-object v3, p0, Layx;->q:Lazd;

    :cond_0
    invoke-direct {p0, p1, v4}, Layx;->b(Ljava/lang/String;I)V

    iget-object v1, p0, Layx;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "attachMediaChannel"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Layy;

    invoke-direct {v0, p0}, Layy;-><init>(Layx;)V

    iput-object v0, p0, Layx;->x:Layf;

    iget-object v0, p0, Layx;->b:Lbac;

    iget-object v1, p0, Layx;->x:Layf;

    invoke-virtual {v0, v1}, Lbac;->a(Laxx;)V

    iget-object v0, p0, Layx;->p:Lazd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layx;->p:Lazd;

    invoke-direct {p0, v0}, Layx;->a(Lazd;)Z

    iput-object v3, p0, Layx;->p:Lazd;

    :cond_1
    :goto_0
    iget-wide v0, p0, Layx;->y:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    iget-object v0, p0, Layx;->x:Layf;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0, p0}, Layf;->a(Layi;)J

    move-result-wide v0

    iput-wide v0, p0, Layx;->y:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v1, "674A0243"

    invoke-virtual {v0}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Layx;->A:Z

    new-instance v0, Layz;

    iget-object v1, p0, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Layx;->b:Lbac;

    invoke-virtual {v2}, Lbac;->p()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Layz;-><init>(Layx;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    iput-object v0, p0, Layx;->B:Lawv;

    :try_start_1
    iget-object v0, p0, Layx;->b:Lbac;

    iget-object v1, p0, Layx;->B:Lawv;

    invoke-virtual {v0, v1}, Lbac;->a(Laxx;)V

    iget-object v0, p0, Layx;->B:Lawv;

    invoke-virtual {v0}, Lawv;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Failed to send offer"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Exception while requesting media status"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v3, "onSessionEnded: sessionId=%s, stautsCode=%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    const/4 v0, 0x5

    :goto_1
    invoke-direct {p0, v0}, Layx;->d(I)V

    invoke-direct {p0, p1, v1}, Layx;->b(Ljava/lang/String;I)V

    iget-boolean v0, p0, Layx;->i:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Layx;->k:Laye;

    const-string v3, "shutting down mirroring"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p2, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {p0, v0}, Layx;->a(Z)V

    invoke-virtual {p0}, Layx;->a()V

    :cond_0
    :goto_3
    iput-object v6, p0, Layx;->q:Lazd;

    iput-object v6, p0, Layx;->r:Lazd;

    iput-object v6, p0, Layx;->s:Lazd;

    iput-object v6, p0, Layx;->t:Lazd;

    iput-object v6, p0, Layx;->u:Lazd;

    iput-object v6, p0, Layx;->v:Lazd;

    iput-object v6, p0, Layx;->e:Lazo;

    iput-boolean v1, p0, Layx;->n:Z

    iput-boolean v2, p0, Layx;->A:Z

    iput-boolean v2, p0, Layx;->o:Z

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x6

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Layx;->k:Laye;

    const-string v3, "detaching media channel"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->k:Laye;

    const-string v3, "detachMediaChannel"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->x:Layf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->b:Lbac;

    if-eqz v0, :cond_5

    iget-object v0, p0, Layx;->b:Lbac;

    iget-object v3, p0, Layx;->x:Layf;

    invoke-virtual {v0, v3}, Lbac;->b(Laxx;)V

    :cond_5
    iput-object v6, p0, Layx;->x:Layf;

    goto :goto_3
.end method

.method final a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "shutDownMirroringIfNeeded. error:%b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Layx;->i:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Layx;->j:Lcom/google/android/gms/cast_mirroring/JGCastService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "Destroying mirroring client"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->j:Lcom/google/android/gms/cast_mirroring/JGCastService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast_mirroring/JGCastService;->disconnect()V

    iput-object v5, p0, Layx;->j:Lcom/google/android/gms/cast_mirroring/JGCastService;

    :cond_0
    iget-object v0, p0, Layx;->B:Lawv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Layx;->b:Lbac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layx;->b:Lbac;

    iget-object v1, p0, Layx;->B:Lawv;

    invoke-virtual {v0, v1}, Lbac;->b(Laxx;)V

    :cond_1
    iput-object v5, p0, Layx;->B:Lawv;

    :cond_2
    iget-object v0, p0, Layx;->l:Layl;

    invoke-static {v0}, Layl;->f(Layl;)Lazg;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Layx;->l:Layl;

    invoke-static {v0}, Layl;->f(Layl;)Lazg;

    move-result-object v0

    iget-object v1, p0, Layx;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-interface {v0, v1, p1}, Lazg;->a(Lcom/google/android/gms/cast/CastDevice;Z)V

    :cond_3
    iput-boolean v4, p0, Layx;->i:Z

    :cond_4
    return-void
.end method

.method public final a(Landroid/content/Intent;Loq;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Received control request %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lazd;

    iget-object v2, p0, Layx;->l:Layl;

    invoke-direct {v1, v2, p1, p2}, Lazd;-><init>(Layl;Landroid/content/Intent;Loq;)V

    const-string v2, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Layx;->a(Lazd;)Z

    move-result v0

    :cond_1
    return v0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onSelect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Layx;->k()V

    invoke-virtual {p0}, Layx;->d()V

    return-void
.end method

.method public final b(I)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onUpdateVolume() delta=%d"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->b:Lbac;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-wide v0, p0, Layx;->c:D

    int-to-double v2, p1

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    div-double/2addr v2, v4

    add-double v1, v0, v2

    iget-object v0, p0, Layx;->b:Lbac;

    iget-wide v3, p0, Layx;->c:D

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lbac;->a(DDZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "Unable to update volume: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Layx;->q:Lazd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layx;->q:Lazd;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lazd;->a(I)V

    iput-object v3, p0, Layx;->q:Lazd;

    :cond_0
    :goto_0
    invoke-direct {p0, p1, v2}, Layx;->b(Ljava/lang/String;I)V

    return-void

    :cond_1
    iget-object v0, p0, Layx;->p:Lazd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->p:Lazd;

    iget-object v0, v0, Lazd;->a:Landroid/content/Intent;

    if-eqz v0, :cond_2

    const-string v1, "android.media.intent.action.PLAY"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Layx;->p:Lazd;

    invoke-virtual {v0, v2}, Lazd;->a(I)V

    :cond_2
    iput-object v3, p0, Layx;->p:Lazd;

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "onUnselect"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Layx;->e()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Layx;->l:Layl;

    invoke-static {v0, p0}, Layl;->b(Layl;Layx;)Lbac;

    move-result-object v0

    iput-object v0, p0, Layx;->b:Lbac;

    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Layx;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Layx;->b:Lbac;

    invoke-virtual {v0}, Lbac;->b()V

    goto :goto_0
.end method

.method public final e()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "endSession() voluntary=%b"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->e:Lazo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "mStopApplicationWhenSessionEnds: %b"

    new-array v2, v4, [Ljava/lang/Object;

    iget-boolean v3, p0, Layx;->o:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->e:Lazo;

    iget-boolean v1, p0, Layx;->A:Z

    iget-boolean v2, p0, Layx;->o:Z

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lazo;->a(Z)V

    :cond_0
    return-void
.end method

.method final f()V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget v0, p0, Layx;->f:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iput v4, p0, Layx;->f:I

    return-void

    :pswitch_0
    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "starting pending session for mirroring"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v3}, Layx;->c(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "starting pending session for media with session ID %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Layx;->g:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Layx;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Layx;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Layx;->g:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-direct {p0, v4}, Layx;->c(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Layx;->a(Z)V

    iget-object v0, p0, Layx;->l:Layl;

    invoke-static {v0, p0}, Layl;->a(Layl;Layx;)V

    return-void
.end method

.method final h()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "sendItemStatusUpdate(); current item is %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Layx;->D:Lazc;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->D:Lazc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Layx;->D:Lazc;

    iget-object v0, v0, Lazc;->d:Landroid/app/PendingIntent;

    if-eqz v0, :cond_1

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "found a PendingIntent for item %s"

    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Layx;->D:Lazc;

    aput-object v4, v3, v7

    invoke-virtual {v1, v2, v3}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Layx;->D:Lazc;

    iget-object v3, v3, Lazc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Layx;->j()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Layx;->x:Layf;

    invoke-virtual {v2}, Layf;->d()Lawi;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Lazu;->a(Lawi;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Layx;->k:Laye;

    const-string v4, "adding metadata bundle: %s"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_0
    :try_start_0
    iget-object v2, p0, Layx;->k:Laye;

    const-string v3, "Invoking item status PendingIntent with: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Layx;->l:Layl;

    iget-object v2, v2, Lnz;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Layx;->x:Layf;

    invoke-virtual {v0}, Layf;->c()Lawm;

    move-result-object v0

    iget v0, v0, Lawm;->d:I

    if-ne v0, v8, :cond_2

    iget-object v0, p0, Layx;->k:Laye;

    const-string v1, "player state is now IDLE; removing tracked item %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v3, p0, Layx;->D:Lazc;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Layx;->D:Lazc;

    invoke-direct {p0, v0}, Layx;->a(Lazc;)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Layx;->k:Laye;

    const-string v2, "exception while sending PendingIntent"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Laye;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
