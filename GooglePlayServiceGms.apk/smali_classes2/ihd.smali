.class public final Lihd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:J

.field final c:J

.field d:Lidt;

.field public e:J

.field f:J

.field g:Lihf;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLidt;JJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lihf;

    invoke-direct {v0}, Lihf;-><init>()V

    iput-object v0, p0, Lihd;->g:Lihf;

    iput-object p1, p0, Lihd;->a:Ljava/lang/String;

    iput-wide p2, p0, Lihd;->c:J

    iput-object p4, p0, Lihd;->d:Lidt;

    iput-wide p5, p0, Lihd;->b:J

    invoke-virtual {p0, p7, p8}, Lihd;->a(J)V

    return-void
.end method

.method private static a(JJJJ)J
    .locals 2

    add-long v0, p0, p2

    sub-long/2addr v0, p4

    invoke-static {p6, p7, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private b()Z
    .locals 6

    const-wide/32 v4, 0x5265c00

    iget-object v0, p0, Lihd;->d:Lidt;

    iget-wide v0, v0, Lidt;->c:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lihd;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lihd;->c:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(J)V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lihd;->e:J

    invoke-direct {p0}, Lihd;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-wide v1, p0, Lihd;->b:J

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-wide v1, p0, Lihd;->c:J

    invoke-static {v0, v1, v2}, Lilv;->a(Ljava/util/Calendar;J)V

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lihd;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lihd;->f:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lihd;->f:J

    goto :goto_0
.end method

.method private declared-synchronized e(J)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lihd;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f(J)Z
    .locals 10

    const-wide/16 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lihd;->f:J

    iget-wide v4, p0, Lihd;->f:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    iget-object v0, p0, Lihd;->d:Lidt;

    iget-wide v0, v0, Lidt;->b:J

    iput-wide p1, p0, Lihd;->f:J

    :cond_0
    :goto_0
    iget-wide v4, p0, Lihd;->e:J

    iget-wide v6, p0, Lihd;->e:J

    add-long/2addr v0, v6

    iget-object v6, p0, Lihd;->d:Lidt;

    iget-wide v6, v6, Lidt;->a:J

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lihd;->e:J

    iget-wide v0, p0, Lihd;->e:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget-wide v0, p0, Lihd;->f:J

    cmp-long v0, v2, v0

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "TokenBucket"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adjusted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lihd;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return v0

    :cond_3
    :try_start_1
    iget-wide v4, p0, Lihd;->f:J

    sub-long v4, p1, v4

    iget-object v6, p0, Lihd;->d:Lidt;

    iget-wide v6, v6, Lidt;->c:J

    div-long/2addr v4, v6

    cmp-long v6, v4, v0

    if-ltz v6, :cond_0

    iget-object v0, p0, Lihd;->d:Lidt;

    iget-wide v0, v0, Lidt;->b:J

    mul-long/2addr v0, v4

    iget-wide v6, p0, Lihd;->f:J

    iget-object v8, p0, Lihd;->d:Lidt;

    iget-wide v8, v8, Lidt;->c:J

    mul-long/2addr v4, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lihd;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()Livi;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lihd;->g:Lihf;

    invoke-virtual {v0}, Lihf;->a()Livi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(J)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lihd;->d(J)V

    invoke-direct {p0, p1, p2}, Lihd;->f(J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(JJJLivi;)V
    .locals 13

    monitor-enter p0

    if-nez p7, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lihd;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v1, 0x2

    :try_start_1
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v9

    const/4 v1, 0x1

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Livi;->d(I)J

    move-result-wide v11

    iget-wide v5, p0, Lihd;->b:J

    move-wide/from16 v1, p3

    move-wide/from16 v3, p5

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lihd;->a(JJJJ)J

    move-result-wide v1

    const-wide/32 v3, 0x5265c00

    add-long/2addr v1, v3

    cmp-long v1, v1, p1

    if-ltz v1, :cond_1

    const-wide/16 v1, -0x1

    cmp-long v1, v9, v1

    if-nez v1, :cond_2

    :cond_1
    invoke-direct {p0, p1, p2}, Lihd;->d(J)V

    :goto_1
    invoke-direct {p0, p1, p2}, Lihd;->f(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    :try_start_2
    iget-wide v5, p0, Lihd;->b:J

    move-wide/from16 v1, p3

    move-wide v3, v9

    move-wide v7, p1

    invoke-static/range {v1 .. v8}, Lihd;->a(JJJJ)J

    move-result-wide v1

    iput-wide v1, p0, Lihd;->f:J

    invoke-direct {p0}, Lihd;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-wide v1, p0, Lihd;->f:J

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iget-wide v4, p0, Lihd;->b:J

    add-long/2addr v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v3}, Lilv;->a(Ljava/util/Calendar;)J

    move-result-wide v4

    iget-wide v6, p0, Lihd;->c:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_4

    :goto_2
    iput-wide v1, p0, Lihd;->f:J

    :cond_3
    iget-object v1, p0, Lihd;->d:Lidt;

    iget-wide v1, v1, Lidt;->a:J

    invoke-static {v1, v2, v11, v12}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lihd;->e:J

    goto :goto_1

    :cond_4
    iget-wide v1, p0, Lihd;->c:J

    invoke-static {v3, v1, v2}, Lilv;->a(Ljava/util/Calendar;J)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-wide v4, p0, Lihd;->b:J

    add-long/2addr v4, p1

    cmp-long v1, v1, v4

    if-lez v1, :cond_5

    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v3, v1, v2}, Ljava/util/Calendar;->add(II)V

    :cond_5
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lihd;->b:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    sub-long/2addr v1, v3

    goto :goto_2
.end method

.method public final declared-synchronized a(Lidt;J)V
    .locals 4

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0, p2, p3}, Lihd;->f(J)Z

    iget-wide v0, p0, Lihd;->e:J

    iget-wide v2, p1, Lidt;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "TokenBucket"

    const-string v1, "Bucket size shrinked"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-wide v0, p1, Lidt;->a:J

    iput-wide v0, p0, Lihd;->e:J

    :cond_2
    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "TokenBucket"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated bucket parameters to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lidt;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iput-object p1, p0, Lihd;->d:Lidt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Livi;)V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iget-wide v1, p0, Lihd;->e:J

    invoke-virtual {p1, v0, v1, v2}, Livi;->a(IJ)Livi;

    const/4 v0, 0x2

    iget-wide v1, p0, Lihd;->f:J

    invoke-virtual {p1, v0, v1, v2}, Livi;->a(IJ)Livi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JJ)Z
    .locals 5

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_0
    invoke-direct {p0, p3, p4}, Lihd;->f(J)Z

    invoke-direct {p0, p1, p2}, Lihd;->e(J)Z

    move-result v0

    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lihd;->e:J

    sub-long/2addr v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    iput-wide v1, p0, Lihd;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JJZ)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p3, p4}, Lihd;->f(J)Z

    invoke-direct {p0, p1, p2}, Lihd;->e(J)Z

    move-result v1

    if-eqz p5, :cond_0

    iget-object v2, p0, Lihd;->g:Lihf;

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lihf;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(J)J
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lihd;->f(J)Z

    iget-wide v0, p0, Lihd;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(JJZ)Lihe;
    .locals 4

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    monitor-enter p0

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_0
    invoke-direct {p0, p3, p4}, Lihd;->f(J)Z

    invoke-virtual/range {p0 .. p5}, Lihd;->a(JJZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lihd;->e:J

    sub-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lihd;->e:J

    new-instance v0, Lihe;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lihe;-><init>(Lihd;JB)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized c(J)J
    .locals 6

    const-wide/16 v0, 0x0

    monitor-enter p0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    :goto_0
    monitor-exit p0

    return-wide v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lihd;->d:Lidt;

    iget-wide v0, v0, Lidt;->a:J

    iget-wide v2, p0, Lihd;->e:J

    sub-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lihd;->e:J

    add-long/2addr v2, v0

    iget-object v4, p0, Lihd;->d:Lidt;

    iget-wide v4, v4, Lidt;->a:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lihd;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 9

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const-string v1, "%s - current tokens: %d, last refill: %s, params: %s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lihd;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lihd;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/util/Date;

    iget-wide v5, p0, Lihd;->b:J

    iget-wide v7, p0, Lihd;->f:J

    add-long/2addr v5, v7

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x3

    iget-object v3, p0, Lihd;->d:Lidt;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
