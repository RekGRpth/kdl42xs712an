.class public final Lejk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lelh;


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field b:Lejl;

.field c:Lejl;

.field public final d:Ljava/util/Map;

.field public final e:Ljava/lang/Object;

.field private final f:Landroid/content/Context;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lejk;->e:Ljava/lang/Object;

    iput-object p1, p0, Lejk;->f:Landroid/content/Context;

    iput-object p2, p0, Lejk;->g:Ljava/lang/String;

    const-string v0, "-icing-settings"

    invoke-virtual {p0, v0}, Lejk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v0, "settings-version"

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_0

    :goto_0
    iput-object v3, p0, Lejk;->a:Landroid/content/SharedPreferences;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lejk;->d:Ljava/util/Map;

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lejk;->d:Ljava/util/Map;

    invoke-static {v0, v2}, Lejk;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v2, "experiment-config-key"

    invoke-static {v0, v2}, Lejl;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lejl;

    move-result-object v0

    iput-object v0, p0, Lejk;->b:Lejl;

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v2, "pending-experiment-config-key"

    invoke-static {v0, v2}, Lejl;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lejl;

    move-result-object v0

    iput-object v0, p0, Lejk;->c:Lejl;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    :cond_0
    new-instance v0, Lelg;

    iget-object v1, p0, Lejk;->f:Landroid/content/Context;

    iget-object v5, p0, Lejk;->g:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lelg;-><init>(Landroid/content/Context;Lelh;Landroid/content/SharedPreferences;ILjava/lang/String;)V

    iget v1, v0, Lelg;->c:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "settings-version"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lelg;->a()V

    :pswitch_1
    invoke-virtual {v0}, Lelg;->b()V

    :pswitch_2
    iget-object v1, v0, Lelg;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last-maintenance"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :pswitch_3
    invoke-virtual {v0}, Lelg;->c()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static a(Landroid/content/SharedPreferences;Ljava/util/Map;)V
    .locals 4

    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "pkg-info-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Leij;

    invoke-direct {v3}, Leij;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v3}, Lell;->a(Ljava/lang/String;Lizs;)Lizs;

    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Leij;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pkg-info-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v2, p2, Leij;->b:Z

    if-nez v2, :cond_0

    iget-object v2, p2, Leij;->c:Lehs;

    if-nez v2, :cond_0

    iget v2, p2, Leij;->d:I

    if-nez v2, :cond_0

    iget-object v2, p2, Leij;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p2, Leij;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p2, Leij;->f:Z

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-interface {p0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_1
    return v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 3

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v1, "index-version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 3

    iget-object v0, p0, Lejk;->f:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lejk;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lehd;)V
    .locals 3

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "extension-info"

    invoke-static {p1}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final a(Ljava/lang/String;Leij;)V
    .locals 2

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lejk;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Leij;)Z

    move-result v1

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-nez v1, :cond_0

    iget-object v0, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->c(Ljava/lang/String;)Leij;

    move-result-object v0

    iget-boolean v2, v0, Leij;->f:Z

    if-eq p2, v2, :cond_0

    iput-boolean p2, v0, Leij;->f:Z

    invoke-virtual {p0, p1, v0}, Lejk;->a(Ljava/lang/String;Leij;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z)V
    .locals 5

    if-eqz p1, :cond_0

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lejk;->b:Lejl;

    const-string v3, "experiment-config-key"

    invoke-virtual {v2, v0, v3}, Lejl;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    iget-object v2, p0, Lejk;->c:Lejl;

    const-string v3, "pending-experiment-config-key"

    iget-object v4, v2, Lejl;->b:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    iget-object v4, v2, Lejl;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    invoke-virtual {v2, v0, v3}, Lejl;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a([B)Z
    .locals 12

    const/4 v1, 0x0

    iget-object v3, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    invoke-static {p1}, Lixl;->a([B)Lixl;

    move-result-object v4

    new-instance v5, Lejl;

    invoke-direct {v5}, Lejl;-><init>()V

    new-instance v0, Lejl;

    invoke-direct {v0}, Lejl;-><init>()V

    iput-object v0, p0, Lejk;->c:Lejl;

    iget-object v6, v4, Lixl;->a:[Lixm;

    array-length v7, v6

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_5

    aget-object v8, v6, v2

    iget-object v9, v8, Lixm;->a:Ljava/lang/String;

    const-string v0, "icing__"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "icing__indexing__"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lejk;->b:Lejl;

    invoke-virtual {v0, v9}, Lejl;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejk;->b:Lejl;

    iget-object v0, v0, Lejl;->b:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixm;

    iget-object v10, v0, Lixm;->a:Ljava/lang/String;

    iget-object v11, v8, Lixm;->a:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, v0, Lixm;->c:Ljava/lang/String;

    iget-object v11, v8, Lixm;->c:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-boolean v10, v0, Lixm;->b:Z

    iget-boolean v11, v8, Lixm;->b:Z

    if-ne v10, v11, :cond_2

    iget v0, v0, Lixm;->d:I

    iget v10, v8, Lixm;->d:I

    if-ne v0, v10, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    :cond_0
    iget-object v0, p0, Lejk;->c:Lejl;

    invoke-virtual {v0, v9, v8}, Lejl;->a(Ljava/lang/String;Lixm;)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v5, v9, v8}, Lejl;->a(Ljava/lang/String;Lixm;)V
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "Unable to parse experiment config."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    :goto_3
    return v0

    :cond_5
    :try_start_2
    iget-object v2, v4, Lixl;->c:[I

    array-length v4, v2

    move v0, v1

    :goto_4
    if-ge v0, v4, :cond_8

    aget v6, v2, v0

    const v7, 0x9a1d20

    if-lt v6, v7, :cond_6

    const v7, 0x9ba3bf

    if-ge v6, v7, :cond_6

    iget-object v7, p0, Lejk;->b:Lejl;

    iget-object v7, v7, Lejl;->a:Ljava/util/Set;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-virtual {v5, v6}, Lejl;->a(I)V

    :cond_6
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v7, p0, Lejk;->c:Lejl;

    invoke-virtual {v7, v6}, Lejl;->a(I)V
    :try_end_2
    .catch Lizr; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_8
    :try_start_3
    new-instance v0, Lejl;

    invoke-direct {v0, v5}, Lejl;-><init>(Lejl;)V

    iput-object v0, p0, Lejk;->b:Lejl;

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lejk;->b:Lejl;

    const-string v4, "experiment-config-key"

    invoke-virtual {v2, v0, v4}, Lejl;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    iget-object v2, p0, Lejk;->c:Lejl;

    const-string v4, "pending-experiment-config-key"

    invoke-virtual {v2, v0, v4}, Lejl;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catch Lizr; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)Leij;
    .locals 1

    iget-object v0, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leij;

    return-object v0
.end method

.method public final b()V
    .locals 3

    const-string v0, "index-version"

    iget-object v1, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/16 v2, 0x26

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final c(Ljava/lang/String;)Leij;
    .locals 2

    iget-object v0, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leij;

    if-nez v0, :cond_0

    new-instance v0, Leij;

    invoke-direct {v0}, Leij;-><init>()V

    iget-object v1, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public final c()Z
    .locals 10

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last-maintenance"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v0, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v6, p0, Lejk;->b:Lejl;

    iget-object v7, p0, Lejk;->c:Lejl;

    iget-object v0, v7, Lejl;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v4

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, v6, Lejl;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v6, Lejl;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lixm;

    iget-object v9, v7, Lejl;->b:Ljava/util/Map;

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v1, v9}, Lixm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    :cond_0
    iget-object v1, v6, Lejl;->b:Ljava/util/Map;

    iget-object v2, v7, Lejl;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v3

    :goto_1
    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v0, v7, Lejl;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v4

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v8, v6, Lejl;->a:Ljava/util/Set;

    invoke-interface {v8, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v1, v6, Lejl;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v3

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_5

    if-nez v2, :cond_5

    const-string v0, "Observed an experimentId change without accompanying flag change"

    invoke-static {v0}, Lehe;->e(Ljava/lang/String;)I

    :cond_3
    :goto_4
    if-nez v2, :cond_4

    if-eqz v1, :cond_6

    :cond_4
    :goto_5
    monitor-exit v5

    return v3

    :cond_5
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    const-string v0, "Observed a flag change without accompanying experimentId change"

    invoke-static {v0}, Lehe;->e(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_6
    move v3, v4

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_1
.end method

.method public final d()J
    .locals 4

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v1, "last-maintenance"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Leij;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Leij;->a:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Leij;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Leij;->e:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Ljava/util/Set;
    .locals 2

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 6

    iget-object v2, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v0, p0, Lejk;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leij;

    const-string v5, ""

    iput-object v5, v1, Leij;->e:Ljava/lang/String;

    const/4 v5, 0x0

    iput-boolean v5, v1, Leij;->b:Z

    const/4 v5, 0x0

    iput-object v5, v1, Leij;->c:Lehs;

    const/4 v5, 0x0

    iput v5, v1, Leij;->d:I

    const/4 v5, 0x0

    iput-boolean v5, v1, Leij;->f:Z

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leij;

    invoke-static {v3, v1, v0}, Lejk;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Leij;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Leij;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 3

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, v0, Leij;->c:Lehs;

    if-eqz v2, :cond_0

    iget-object v0, v0, Leij;->c:Lehs;

    invoke-static {v0}, Lell;->a(Lehs;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Lehd;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lejk;->a:Landroid/content/SharedPreferences;

    const-string v2, "extension-info"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lehd;

    invoke-direct {v0}, Lehd;-><init>()V

    invoke-static {v1, v0}, Lell;->a(Ljava/lang/String;Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehd;

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)I
    .locals 2

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Leij;->d:I

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lejk;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "extension-info"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final i(Ljava/lang/String;)J
    .locals 3

    iget-object v2, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, v0, Leij;->g:J

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final j(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lejk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lejk;->b(Ljava/lang/String;)Leij;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Leij;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
