.class public final Liwh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:D

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Liwh;->b:I

    iput p2, p0, Liwh;->c:I

    int-to-double v0, p1

    invoke-static {v0, v1}, Liwd;->b(D)D

    move-result-wide v0

    iput-wide v0, p0, Liwh;->a:D

    return-void
.end method


# virtual methods
.method public final a(DLiwh;)D
    .locals 2

    invoke-virtual {p3, p1, p2}, Liwh;->b(D)I

    move-result v0

    invoke-virtual {p0, v0}, Liwh;->b(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final a(I)D
    .locals 4

    iget v0, p0, Liwh;->b:I

    sub-int v0, p1, v0

    int-to-double v0, v0

    const-wide v2, 0x40fb2151d0ca9d81L    # 111125.11347447896

    mul-double/2addr v0, v2

    const-wide v2, 0x416312d000000000L    # 1.0E7

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public final a(D)I
    .locals 3

    iget v0, p0, Liwh;->b:I

    const-wide v1, 0x40567f46328ec073L    # 89.98865951481439

    mul-double/2addr v1, p1

    double-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(DLiwh;)D
    .locals 2

    invoke-virtual {p3, p1, p2}, Liwh;->a(D)I

    move-result v0

    invoke-virtual {p0, v0}, Liwh;->a(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final b(I)D
    .locals 4

    iget v0, p0, Liwh;->c:I

    sub-int v0, p1, v0

    int-to-double v0, v0

    iget-wide v2, p0, Liwh;->a:D

    mul-double/2addr v0, v2

    const-wide v2, 0x416312d000000000L    # 1.0E7

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public final b(D)I
    .locals 5

    iget v0, p0, Liwh;->c:I

    const-wide v1, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v1, p1

    iget-wide v3, p0, Liwh;->a:D

    div-double/2addr v1, v3

    double-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method
