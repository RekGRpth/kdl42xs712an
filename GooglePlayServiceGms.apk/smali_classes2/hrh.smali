.class final Lhrh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lixv;


# instance fields
.field private a:[B

.field private b:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhrh;->a:[B

    iput-object v0, p0, Lhrh;->b:[B

    iput-object p1, p0, Lhrh;->a:[B

    return-void
.end method

.method private d()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lhrh;->b:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lhrh;->a:[B

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v2, v0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    iput-object v3, p0, Lhrh;->a:[B

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lhrh;->b:[B

    iput-object v3, p0, Lhrh;->a:[B

    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized Y_()I
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lhrh;->d()V

    iget-object v0, p0, Lhrh;->b:[B

    array-length v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized Z_()Ljava/io/InputStream;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lhrh;->d()V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lhrh;->b:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhrh;->a:[B

    iput-object v0, p0, Lhrh;->b:[B

    return-void
.end method
