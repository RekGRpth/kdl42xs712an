.class public final Ldyp;
.super Ldwz;
.source "SourceFile"


# instance fields
.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Ldzc;

.field private final j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private final p:I

.field private final q:I

.field private r:Z

.field private s:Ljava/lang/String;

.field private t:Lbhc;

.field private final u:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Ldzc;Landroid/view/View$OnClickListener;)V
    .locals 2

    invoke-direct {p0, p1}, Ldwz;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyp;->n:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldyp;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ldyp;->s:Ljava/lang/String;

    iput-object p3, p0, Ldyp;->i:Ldzc;

    iput-object p2, p0, Ldyp;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldyp;->g:Landroid/view/LayoutInflater;

    iput-object p4, p0, Ldyp;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110009    # com.google.android.gms.R.bool.games_select_players_is_square_tile

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Ldyp;->o:Z

    const v1, 0x7f0c00de    # com.google.android.gms.R.color.games_tile_text_color_primary_text

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Ldyp;->p:I

    const v1, 0x7f0c00c4    # com.google.android.gms.R.color.games_tile_text_color_primary_text_disabled

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Ldyp;->q:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldyp;->u:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Ldyp;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Ldyp;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 11

    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    check-cast p4, Lcom/google/android/gms/games/Player;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyq;

    if-nez p3, :cond_7

    if-nez p4, :cond_7

    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-boolean v1, v1, Ldyp;->l:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-boolean v1, v1, Ldyp;->m:Z

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v5, 0x7f0201e7    # com.google.android.gms.R.drawable.pict_auto_pick_player_default

    invoke-virtual {v1, v9, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v5, v0, Ldyq;->i:Ldyp;

    iget v5, v5, Ldyp;->p:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    iget-object v1, v0, Ldyq;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-object v1, v1, Ldyp;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, v0, Ldyq;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->f:Landroid/widget/TextView;

    iget-object v5, v0, Ldyq;->i:Ldyp;

    iget-object v5, v5, Ldyp;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->L()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v5, v0, Ldyq;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v5, v0, Ldyq;->e:Landroid/widget/ImageView;

    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-object v1, v1, Ldyp;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->P()I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v5, 0x7f0c00c3    # com.google.android.gms.R.color.games_select_players_selected_tint

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    :goto_2
    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-boolean v1, v1, Ldyp;->l:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    const v2, 0x7f0b0223    # com.google.android.gms.R.string.games_select_players_add_auto_pick_item_label

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    const-string v2, "auto_pick_item_add_tag"

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_3
    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    iget-object v0, p0, Ldyp;->s:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-nez v1, :cond_12

    :cond_1
    :goto_5
    return-void

    :cond_2
    iget-object v1, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v5, 0x7f0201e8    # com.google.android.gms.R.drawable.pict_auto_pick_player_disabled

    invoke-virtual {v1, v9, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v5, v0, Ldyq;->i:Ldyp;

    iget v5, v5, Ldyp;->q:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    iget-object v1, v0, Ldyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v5, v0, Ldyq;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    goto :goto_2

    :cond_5
    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-boolean v1, v1, Ldyp;->m:Z

    if-eqz v1, :cond_6

    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    const v2, 0x7f0b0224    # com.google.android.gms.R.string.games_select_players_del_auto_pick_item_label

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    const-string v2, "auto_pick_item_remove_tag"

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    :cond_6
    iget-object v1, v0, Ldyq;->b:Landroid/widget/TextView;

    const v4, 0x7f0b0225    # com.google.android.gms.R.string.games_select_players_auto_pick_chip_name

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    iget-object v1, v0, Ldyq;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->i:Ldyp;

    iget-object v1, v1, Ldyp;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->K()Leed;

    move-result-object v1

    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Leed;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v5, v0, Ldyq;->i:Ldyp;

    iget-boolean v5, v5, Ldyp;->n:Z

    if-eqz v5, :cond_9

    :cond_8
    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Ldyq;->i:Ldyp;

    iget-object v6, v6, Ldyp;->k:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_9
    move v5, v4

    :goto_6
    if-eqz v5, :cond_c

    iget-object v6, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v7, 0x7f0c00e4    # com.google.android.gms.R.color.games_tile_white_color_filter

    invoke-virtual {v6, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldyq;->i:Ldyp;

    iget v7, v7, Ldyp;->q:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v6, v0, Ldyq;->d:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_7
    iget-object v6, v0, Ldyq;->i:Ldyp;

    iget-boolean v6, v6, Ldvj;->d:Z

    if-eqz v6, :cond_e

    iget-object v6, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v7

    const v8, 0x7f0200d6    # com.google.android.gms.R.drawable.games_default_profile_img

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_8
    iget-object v6, v0, Ldyq;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {p4, v6}, Lcom/google/android/gms/games/Player;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v6, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldyq;->c:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Ldyq;->c:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v6, v7, v2, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v6, v0, Ldyq;->e:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldyq;->b:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    if-eqz v1, :cond_f

    move v1, v4

    :goto_9
    invoke-virtual {v6, v7, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    if-eqz v5, :cond_10

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_a
    invoke-interface {p4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v5, v0, Ldyq;->i:Ldyp;

    iget-object v5, v5, Ldyp;->k:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iget-object v6, v0, Ldyq;->i:Ldyp;

    iget-object v6, v6, Ldyp;->u:Ljava/util/HashMap;

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v5, :cond_a

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_11

    :cond_a
    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_b
    move v5, v2

    goto/16 :goto_6

    :cond_c
    iget-object v6, v0, Ldyq;->b:Landroid/widget/TextView;

    iget-object v7, v0, Ldyq;->i:Ldyp;

    iget v7, v7, Ldyp;->p:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz v1, :cond_d

    iget-object v6, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const v7, 0x7f0c00c3    # com.google.android.gms.R.color.games_select_players_selected_tint

    invoke-virtual {v6, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Ldyq;->d:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_d
    iget-object v6, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v6, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Ldyq;->d:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    :cond_e
    iget-object v6, v0, Ldyq;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto/16 :goto_8

    :cond_f
    move v1, v2

    goto :goto_9

    :cond_10
    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Ldyq;->h:Landroid/view/View;

    invoke-virtual {v1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_a

    :cond_11
    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, p4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Ldyq;->g:Landroid/widget/ImageView;

    iget-object v2, v0, Ldyq;->i:Ldyp;

    iget-object v2, v2, Ldyp;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_12
    invoke-static {v1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Ldyp;->s:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_5
.end method

.method public final a(Lbgo;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Ldyp;->r:Z

    if-nez v1, :cond_1

    :cond_0
    new-array v1, v4, [Lbgo;

    aput-object p1, v1, v0

    invoke-super {p0, v1}, Ldwz;->a([Lbgo;)V

    :goto_0
    iget-object v1, p0, Ldyp;->u:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lbgo;->a()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iget-object v3, p0, Ldyp;->u:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    new-instance v1, Lbhc;

    new-instance v2, Lbhb;

    new-array v3, v4, [Lcom/google/android/gms/games/Player;

    invoke-direct {v2, v3}, Lbhb;-><init>([Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Lbhc;-><init>(Lbgo;)V

    iput-object v1, p0, Ldyp;->t:Lbhc;

    const/4 v1, 0x2

    new-array v1, v1, [Lbgo;

    iget-object v2, p0, Ldyp;->t:Lbhc;

    aput-object v2, v1, v0

    aput-object p1, v1, v4

    invoke-super {p0, v1}, Ldwz;->a([Lbgo;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ldyp;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldyp;->k:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Ldyp;->u:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Ldyp;->u:Ljava/util/HashMap;

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Ldyp;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([Lbgo;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use setDataBuffer instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ldyp;->s:Ljava/lang/String;

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Ldyp;->t:Lbhc;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    iget-object v0, p0, Ldyp;->t:Lbhc;

    invoke-virtual {v0}, Lbhc;->d()V

    :goto_0
    invoke-virtual {p0}, Ldyp;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Ldyp;->t:Lbhc;

    invoke-virtual {v0}, Lbhc;->e()V

    goto :goto_0
.end method

.method public final b(ZZ)V
    .locals 1

    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-boolean v0, p0, Ldyp;->r:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Ldyp;->l:Z

    if-ne v0, p1, :cond_1

    iget-boolean v0, p0, Ldyp;->m:Z

    if-eq v0, p2, :cond_2

    :cond_1
    iput-boolean p1, p0, Ldyp;->l:Z

    iput-boolean p2, p0, Ldyp;->m:Z

    invoke-virtual {p0}, Ldyp;->notifyDataSetChanged()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Ldyp;->r:Z

    return-void
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Ldyp;->n:Z

    invoke-virtual {p0}, Ldyp;->notifyDataSetChanged()V

    return-void
.end method

.method public final l()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Ldyp;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040095    # com.google.android.gms.R.layout.games_tile_select_players

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldyq;

    invoke-direct {v1, p0, v0}, Ldyq;-><init>(Ldyp;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final m()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Ldyp;->s:Ljava/lang/String;

    return-void
.end method
