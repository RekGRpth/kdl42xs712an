.class public final Lchd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcfz;

.field public c:Lcdu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcdu;Lcfz;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lchd;->a:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lchd;->c:Lcdu;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lchd;->b:Lcfz;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;)Lcgs;
    .locals 5

    instance-of v0, p1, Lcom/google/android/gms/drive/events/ResourceEvent;

    const-string v1, "Only resource subscriptions are currently supported"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    check-cast p1, Lcom/google/android/gms/drive/events/ResourceEvent;

    invoke-interface {p1}, Lcom/google/android/gms/drive/events/ResourceEvent;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lchd;->b:Lcfz;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcfz;->a(Lcom/google/android/gms/drive/database/data/EntrySpec;I)Lcgs;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v1

    new-instance v1, Lbrm;

    const/16 v2, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to query subscriptions: id = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", type = 1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v1
.end method
