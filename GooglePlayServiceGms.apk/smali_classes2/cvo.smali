.class final Lcvo;
.super Lcve;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/String;

.field static final b:[Ljava/lang/String;

.field static final c:[Ljava/lang/String;

.field private static final d:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final e:Ldpj;

.field private final f:Ldpk;

.field private final g:Ljava/util/Random;

.field private h:Lcvp;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvo;->d:Ljava/util/concurrent/locks/ReentrantLock;

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "external_match_id"

    aput-object v1, v0, v4

    const-string v1, "creator_external"

    aput-object v1, v0, v3

    const-string v1, "creation_timestamp"

    aput-object v1, v0, v5

    const-string v1, "last_updater_external"

    aput-object v1, v0, v6

    const-string v1, "last_updated_timestamp"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "pending_participant_external"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "variant"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "version"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "has_automatch_criteria"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "automatch_min_players"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "automatch_max_players"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "automatch_bit_mask"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "automatch_wait_estimate_sec"

    aput-object v2, v0, v1

    sput-object v0, Lcvo;->a:[Ljava/lang/String;

    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "external_participant_id"

    aput-object v1, v0, v4

    const-string v1, "default_display_image_id"

    aput-object v1, v0, v3

    const-string v1, "default_display_image_uri"

    aput-object v1, v0, v5

    const-string v1, "default_display_image_url"

    aput-object v1, v0, v6

    const-string v1, "default_display_hi_res_image_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "default_display_hi_res_image_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "default_display_hi_res_image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "default_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "player_status"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "client_address"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "connected"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "capabilities"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "player_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "result_type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "placing"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "external_player_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "profile_name"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "profile_icon_image_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "profile_icon_image_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "profile_icon_image_url"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "profile_hi_res_image_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "profile_hi_res_image_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "profile_hi_res_image_url"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "last_updated"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "is_in_circles"

    aput-object v2, v0, v1

    sput-object v0, Lcvo;->b:[Ljava/lang/String;

    sget-object v1, Lcvo;->a:[Ljava/lang/String;

    sget-object v2, Lcvo;->b:[Ljava/lang/String;

    array-length v0, v1

    array-length v3, v2

    add-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    array-length v1, v1

    array-length v3, v2

    invoke-static {v2, v4, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcvo;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;Lbmi;)V
    .locals 3

    const-string v0, "RealTimeAgent"

    sget-object v1, Lcvo;->d:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Ldpj;

    invoke-direct {v0, p2}, Ldpj;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvo;->e:Ldpj;

    new-instance v0, Ldpk;

    invoke-direct {v0, p3}, Ldpk;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvo;->f:Ldpk;

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v1

    invoke-interface {v1}, Lbpe;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcvo;->g:Ljava/util/Random;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    new-instance v0, Lcvp;

    invoke-virtual {p3}, Ldow;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2, p3, p4}, Lcvi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcvp;-><init>(Ljava/lang/String;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcvo;->h:Lcvp;

    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {v0}, Lcvp;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;I)Ldnu;
    .locals 4

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ldnu;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v2, v1, v3}, Ldnu;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ldje;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v0, Ldei;->a:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcum;->d(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 8

    const/16 v7, 0x1b58

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvo;->f:Ldpk;

    invoke-static {p3}, Ldpk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpk;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Ldow;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move v0, v6

    :goto_0
    if-ne v0, v7, :cond_1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcvo;->e:Ldpj;

    invoke-static {p3}, Ldpj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpj;->a:Lbmi;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Ldow;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    const-string v2, "Failed to decline invitation"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "RealTimeAgent"

    invoke-static {v1, v0, v7}, Lcvi;->a(Ljava/lang/String;Lsp;I)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {p1, p2, p3}, Lcvo;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ldpc;)I
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcvo;->e:Ldpj;

    new-instance v4, Ldpd;

    invoke-direct {v4, p4, p3}, Ldpd;-><init>(Ldpc;Ljava/lang/String;)V

    invoke-static {p2}, Ldpj;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpj;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Ldow;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcvo;->h:Lcvp;

    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to leave match: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "RealTimeAgent"

    const/16 v2, 0x1b58

    invoke-static {v1, v0, v2}, Lcvi;->a(Ljava/lang/String;Lsp;I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldpi;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcvo;->h:Lcvp;

    if-nez v0, :cond_0

    const-string v0, "RealTimeAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRoomCache is null when receving status update for room "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {p3}, Ldpi;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcvp;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "RealTimeAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mRoomCache.mRoomId = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcvo;->h:Lcvp;

    iget-object v2, v2, Lcvp;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when receiving status update for room "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ldpi;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcvo;->h:Lcvp;

    iget-object v1, v0, Lcvp;->b:Ljava/util/HashMap;

    iget-object v0, p0, Lcvo;->h:Lcvp;

    iget-object v2, v0, Lcvp;->b:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcvp;->b:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :goto_1
    invoke-static {p1, v1, v0, p3}, Lcvi;->a(Landroid/content/Context;Ljava/util/HashMap;Landroid/content/ContentValues;Ldpi;)V

    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {v0, v1}, Lcvp;->a(Ljava/util/HashMap;)V

    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {v0}, Lcvp;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcvp;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v2, "No base Room entry in cache!"

    invoke-static {v0, v2}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    const-string v2, "No base Room entry values in cache!"

    invoke-static {v0, v2}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    sget-object v2, Lcvo;->a:[Ljava/lang/String;

    invoke-static {v0, v2}, Lcum;->a(Landroid/content/ContentValues;[Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/util/ArrayList;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    if-nez p6, :cond_0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    invoke-virtual/range {p7 .. p7}, Lcom/google/android/gms/games/internal/ConnectionInfo;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v4, 0x0

    :goto_1
    const/4 v1, -0x1

    if-ne p4, v1, :cond_3

    const/4 v8, 0x0

    :goto_2
    new-instance v1, Ldpa;

    invoke-static {}, Lddz;->a()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/gms/games/internal/ConnectionInfo;->c()I

    move-result v5

    invoke-static {p1, v5}, Lcvo;->a(Landroid/content/Context;I)Ldnu;

    move-result-object v6

    iget-object v5, p0, Lcvo;->g:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextLong()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v5, p5

    invoke-direct/range {v1 .. v8}, Ldpa;-><init>(Ldoy;Ljava/util/ArrayList;Ldoz;Ljava/util/ArrayList;Ldnu;Ljava/lang/Long;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcvo;->e:Ldpj;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ldpj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Ldpj;->a:Lbmi;

    const/4 v4, 0x1

    const-class v7, Ldow;

    move-object v3, p2

    move-object v6, v1

    invoke-virtual/range {v2 .. v7}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Ldow;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0, p1, p2, v1, p3}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :goto_3
    return-object v1

    :cond_0
    const/4 v1, 0x0

    const-string v2, "exclusive_bit_mask"

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "exclusive_bit_mask"

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :cond_1
    new-instance v2, Ldoy;

    const-string v3, "max_automatch_players"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "min_automatch_players"

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v1, v3, v4}, Ldoy;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0

    :cond_2
    :try_start_1
    new-instance v4, Ldoz;

    invoke-direct {v4, v1}, Ldoz;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "RealTimeAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to create room: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "RealTimeAgent"

    const/16 v3, 0x1b58

    invoke-static {v2, v1, v3}, Lcvi;->a(Ljava/lang/String;Lsp;I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    goto :goto_3

    :cond_3
    :try_start_2
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2
    .catch Lsp; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v8

    goto/16 :goto_2
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    const/16 v8, 0x1b58

    const/4 v7, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p5}, Lcom/google/android/gms/games/internal/ConnectionInfo;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ldpb;

    invoke-static {}, Lddz;->a()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ldoz;

    invoke-direct {v2, v0}, Ldoz;-><init>(Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/google/android/gms/games/internal/ConnectionInfo;->c()I

    move-result v0

    invoke-static {p1, v0}, Lcvo;->a(Landroid/content/Context;I)Ldnu;

    move-result-object v0

    invoke-direct {v4, v1, v2, v0}, Ldpb;-><init>(Ljava/util/ArrayList;Ldoz;Ldnu;)V

    iget-object v0, p0, Lcvo;->e:Ldpj;

    invoke-static {p4}, Ldpj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpj;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Ldow;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldow;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v7

    :goto_0
    if-ne v1, v8, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    const-string v2, "Failed to accept invitation"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "RealTimeAgent"

    invoke-static {v1, v0, v8}, Lcvi;->a(Ljava/lang/String;Lsp;I)I

    move-result v0

    move v1, v0

    move-object v0, v6

    goto :goto_0

    :cond_0
    invoke-static {p1, p2, p4}, Lcvo;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    if-nez v0, :cond_1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1, p2, v0, p3}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Ldpi;
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcvo;->e:Ldpj;

    new-instance v4, Ldpg;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v4, v1}, Ldpg;-><init>(Ljava/util/ArrayList;)V

    invoke-static {p2}, Ldpj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpj;->a:Lbmi;

    const/4 v2, 0x1

    const-class v5, Ldpi;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldpi;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to report successful peer connections: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcvo;->f:Ldpk;

    invoke-static {p3}, Ldpk;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Ldpk;->a:Lbmi;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {p1, p2, p3}, Lcvo;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcvo;->e:Ldpj;

    invoke-static {p3}, Ldpj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Ldpj;->a:Lbmi;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, p2, v3, v2, v4}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lsp; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    const-string v2, "Failed to dismiss invitation"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "RealTimeAgent"

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lcvi;->a(Ljava/lang/String;Lsp;I)I

    move-result v0

    goto :goto_0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v0, p0, Lcvo;->h:Lcvp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {v0, p3}, Lcvp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvo;->h:Lcvp;

    invoke-virtual {v0}, Lcvp;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcvo;->e:Ldpj;

    invoke-static {p1}, Lcum;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v1}, Ldpj;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldpj;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldow;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldow;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ldow;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcvo;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldow;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RealTimeAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to retrieve room: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lsp;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "RealTimeAgent"

    invoke-static {v0, v1}, Lbng;->a(Lsp;Ljava/lang/String;)Lbnf;

    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
