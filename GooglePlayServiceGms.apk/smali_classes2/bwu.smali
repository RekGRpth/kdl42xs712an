.class public final Lbwu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwz;


# instance fields
.field private final a:Lcfz;

.field private final b:Lcfc;

.field private final c:Lbvu;


# direct methods
.method public constructor <init>(Lcoy;Lbvu;Lcfc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lbwu;->b:Lcfc;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbwu;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvu;

    iput-object v0, p0, Lbwu;->c:Lbvu;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 0

    return-void
.end method

.method public final a(Lbvt;Landroid/content/SyncResult;)V
    .locals 8

    const/4 v4, 0x1

    iget-object v0, p0, Lbwu;->c:Lbvu;

    iget-object v1, p0, Lbwu;->b:Lcfc;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Lbvu;->a(Landroid/content/SyncResult;Lcfc;Ljava/lang/Boolean;Z)Lbvs;

    move-result-object v0

    iget-object v1, p0, Lbwu;->c:Lbvu;

    iget-object v2, p0, Lbwu;->b:Lcfc;

    new-instance v3, Lbvq;

    iget-object v1, v1, Lbvu;->a:Lcoy;

    invoke-direct {v3, v1, v2, v0}, Lbvq;-><init>(Lcoy;Lcfc;Lbvs;)V

    iget-object v0, p0, Lbwu;->a:Lcfz;

    iget-object v1, p0, Lbwu;->b:Lcfc;

    iget-object v1, v1, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwu;->a:Lcfz;

    iget-object v2, p0, Lbwu;->b:Lcfc;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Lcfz;->a(Lcfc;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v0

    :goto_0
    new-instance v2, Lbuu;

    invoke-virtual {v1}, Lcfe;->h()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5, v0}, Lbuu;-><init>(JLjava/util/Set;)V

    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    invoke-static {v2, v0, v4, v5}, Lbuw;->a(Lbuv;Ljava/lang/String;J)Lbuw;

    move-result-object v0

    invoke-virtual {v1}, Lcfe;->i()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-virtual {p1, v0, v1, v2, v3}, Lbvt;->a(Lbuw;Ljava/lang/String;ILbvs;)V

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_0
.end method
