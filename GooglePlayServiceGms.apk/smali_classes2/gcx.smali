.class public final Lgcx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private final d:Lftb;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;Lftb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Lgcx;->b:Ljava/lang/String;

    iput-object p3, p0, Lgcx;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object p4, p0, Lgcx;->d:Lftb;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UpgradeAccountOperation. gpsrc: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lgcx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :try_start_0
    iget-object v0, p0, Lgcx;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lgcx;->b:Ljava/lang/String;

    iget-object v2, p0, Lgcx;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-virtual {p2, p1, v0, v1, v2}, Lfrx;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-result-object v0

    iget-object v1, p0, Lgcx;->d:Lftb;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v5, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcx;->d:Lftb;

    invoke-interface {v0, v6, v1, v4}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcx;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcx;->d:Lftb;

    invoke-interface {v1, v6, v0, v4}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcx;->d:Lftb;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v4, v4}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgcx;->d:Lftb;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lftb;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    return-void
.end method
