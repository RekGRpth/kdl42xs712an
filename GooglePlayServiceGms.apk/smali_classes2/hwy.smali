.class public final Lhwy;
.super Lhxc;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lhwp;Lhvw;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhxc;-><init>(Lhwp;Lhvw;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 9

    const/4 v8, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x0

    iget-boolean v0, p0, Lhxe;->g:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lhxe;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhwy;->e:Lhwp;

    iget-wide v1, p0, Lhxa;->a:J

    iget-boolean v3, p0, Lhxa;->b:Z

    iget-object v4, p0, Lhxe;->i:Ljava/util/Collection;

    new-instance v5, Liac;

    invoke-direct {v5}, Liac;-><init>()V

    invoke-virtual {v0, v6}, Lhwp;->a(I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v5, v1, v2, v3, v6}, Liac;->a(JZLandroid/app/PendingIntent;)Liac;

    invoke-static {v4}, Lile;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v1

    invoke-virtual {v5, v1}, Liac;->a(Landroid/os/WorkSource;)Liac;

    iget-object v0, v0, Lhwp;->a:Landroid/content/Context;

    invoke-virtual {v5, v0}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to bind to GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "GCoreFlp"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Activity recognition enabled with interval %s[ms]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lhxa;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lhwy;->e:Lhwp;

    new-instance v1, Liac;

    invoke-direct {v1}, Liac;-><init>()V

    invoke-virtual {v0, v6}, Lhwp;->a(I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Liac;->a(Landroid/app/PendingIntent;)Liac;

    iget-object v0, v0, Lhwp;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "GCoreFlp"

    const-string v1, "Unable to start the GMS NLP"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/app/PendingIntent;->cancel()V

    :cond_3
    const-string v0, "GCoreFlp"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Activity recognition disabled"

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activity recognition ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lhwy;->a(Ljava/lang/StringBuilder;)V

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
