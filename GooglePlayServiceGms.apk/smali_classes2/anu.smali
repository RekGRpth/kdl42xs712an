.class public Lanu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/security/KeyPair;

.field private final d:Lano;

.field private final e:Ljava/security/KeyPair;

.field private final f:Landroid/os/PowerManager$WakeLock;

.field private final g:Lasu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/security/KeyPair;Lano;)V
    .locals 10

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lanu;->a:Landroid/content/Context;

    iput-object p2, p0, Lanu;->b:Ljava/lang/String;

    iput-object p3, p0, Lanu;->c:Ljava/security/KeyPair;

    iput-object p4, p0, Lanu;->d:Lano;

    iget-boolean v0, p4, Lano;->a:Z

    invoke-static {v0}, Ljch;->a(Z)Ljava/security/KeyPair;

    move-result-object v0

    iput-object v0, p0, Lanu;->e:Ljava/security/KeyPair;

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/os/PowerManager;

    new-instance v9, Lasu;

    new-instance v0, Lbmi;

    iget-object v1, p0, Lanu;->a:Landroid/content/Context;

    invoke-static {p1}, Lanh;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cryptauth/v1/"

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lasu;-><init>(Lbmi;)V

    iput-object v9, p0, Lanu;->g:Lasu;

    const-class v0, Lanu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v5, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lanu;->f:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lanu;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 5

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lanu;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v2, p0, Lanu;->b:Ljava/lang/String;

    iget-object v3, p0, Lanu;->b:Ljava/lang/String;

    iget-object v4, p0, Lanu;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "auth_token"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lanj;
    .locals 11

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iget-object v3, p0, Lanu;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lanu;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-direct {v2, v3, v4, v1, v1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v3, p0, Lanu;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "oauth2:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lanu;->a:Landroid/content/Context;

    invoke-static {v5}, Lanh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lapp;

    iget-object v4, p0, Lanu;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lapp;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v1, v4}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    new-instance v1, Lanv;

    const-string v2, "Failed to get a token for authzen enrollment"

    invoke-direct {v1, v2}, Lanv;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lanu;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/gcm/GcmProvisioning;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lanu;->a:Landroid/content/Context;

    const-string v3, "com.google.android.gms.gcm.GmsAutoStarter"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lanu;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    new-instance v1, Lanv;

    const-string v2, "Failed to get GCM registeration id"

    invoke-direct {v1, v2}, Lanv;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lanu;->f:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x7530

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    :try_start_0
    const-string v1, "AuthZen"

    const-string v2, "Starting authzen enrollment"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljci;->a()Z

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "gcmV1"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Latg;

    invoke-direct {v3}, Latg;-><init>()V

    const-string v4, "c.g.a.gms"

    iput-object v4, v3, Latg;->a:Ljava/lang/String;

    iget-object v4, v3, Latg;->d:Ljava/util/Set;

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v2, v3, Latg;->b:Ljava/util/List;

    iget-object v2, v3, Latg;->d:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-boolean v1, v3, Latg;->c:Z

    iget-object v1, v3, Latg;->d:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;

    iget-object v1, v3, Latg;->d:Ljava/util/Set;

    iget-object v2, v3, Latg;->a:Ljava/lang/String;

    iget-object v4, v3, Latg;->b:Ljava/util/List;

    iget-boolean v3, v3, Latg;->c:Z

    invoke-direct {v5, v1, v2, v4, v3}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/util/List;Z)V

    check-cast v5, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentRequestEntity;

    iget-object v1, p0, Lanu;->g:Lasu;

    invoke-direct {p0, v9}, Lanu;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    const-string v4, "enrollment/setup"

    iget-object v1, v1, Lasu;->a:Lbmi;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentResponseEntity;

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/SetupEnrollmentResponseEntity;

    invoke-interface {v1}, Lati;->b()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Latd;

    move-object v8, v0

    const-string v1, "AuthZen"

    const-string v2, "Enrollment phase1 complete."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lanu;->d:Lano;

    iget-object v1, p0, Lanu;->e:Ljava/security/KeyPair;

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    invoke-interface {v8}, Latd;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Ljci;->c([B)Ljava/security/PublicKey;

    move-result-object v2

    invoke-static {v1, v2}, Ljch;->a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    const/16 v2, 0xc

    const/4 v5, 0x2

    invoke-virtual {v1, v2, v5}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    new-instance v1, Lank;

    iget-object v2, p0, Lanu;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lank;-><init>(Ljava/lang/String;Ljavax/crypto/SecretKey;JJ)V

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v10, Lano;->b:Lanr;

    invoke-virtual {v2, v1}, Lanr;->a(Lank;)Lanj;

    move-result-object v7

    const-string v1, "AuthZen"

    const-string v2, "Successfully generated encryption key."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v7, Lanj;->a:Lank;

    iget-object v1, v1, Lank;->b:Ljavax/crypto/SecretKey;

    new-instance v2, Lanw;

    iget-object v3, p0, Lanu;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lanw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lanw;->a()Ljck;

    move-result-object v2

    invoke-interface {v8}, Latd;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lizf;->a([B)Lizf;

    move-result-object v3

    iget-object v4, p0, Lanu;->c:Ljava/security/KeyPair;

    invoke-virtual {v4}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v4

    invoke-static {v4}, Ljci;->a(Ljava/security/PublicKey;)[B

    move-result-object v4

    invoke-static {v4}, Lizf;->a([B)Lizf;

    move-result-object v4

    invoke-static {v1}, Ljch;->a(Ljavax/crypto/SecretKey;)[B

    move-result-object v5

    invoke-static {v5}, Lizf;->a([B)Lizf;

    move-result-object v5

    invoke-virtual {v7}, Lanj;->a()[B

    move-result-object v6

    invoke-static {v6}, Lizf;->a([B)Lizf;

    move-result-object v6

    invoke-virtual {v2, v3}, Ljck;->e(Lizf;)Ljck;

    invoke-virtual {v2, v4}, Ljck;->c(Lizf;)Ljck;

    invoke-virtual {v2, v5}, Ljck;->b(Lizf;)Ljck;

    invoke-virtual {v2, v9}, Ljck;->i(Ljava/lang/String;)Ljck;

    iget-object v3, p0, Lanu;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/gcm/GcmProvisioning;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lizf;->a(Ljava/lang/String;)Lizf;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljck;->a(Lizf;)Ljck;

    invoke-virtual {v2, v6}, Ljck;->d(Lizf;)Ljck;

    iget-object v3, p0, Lanu;->c:Ljava/security/KeyPair;

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v3

    invoke-static {v2, v1, v3}, Ljch;->a(Ljck;Ljavax/crypto/SecretKey;Ljava/security/PrivateKey;)[B

    move-result-object v1

    iget-object v2, p0, Lanu;->e:Ljava/security/KeyPair;

    invoke-virtual {v2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    invoke-static {v2}, Ljci;->b(Ljava/security/PublicKey;)[B

    move-result-object v2

    invoke-static {v2}, Lbpd;->c([B)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lbpd;->c([B)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Lasw;

    invoke-direct {v6}, Lasw;-><init>()V

    iput-object v2, v6, Lasw;->a:Ljava/lang/String;

    iget-object v2, v6, Lasw;->e:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v1, v6, Lasw;->b:Ljava/lang/String;

    iget-object v1, v6, Lasw;->e:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v8}, Latd;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lasw;->c:Ljava/lang/String;

    iget-object v1, v6, Lasw;->e:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v8, p0, Lanu;->g:Lasu;

    invoke-direct {p0, v9}, Lanu;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v9

    new-instance v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/FinishEnrollmentRequestEntity;

    iget-object v2, v6, Lasw;->e:Ljava/util/Set;

    iget-object v3, v6, Lasw;->a:Ljava/lang/String;

    iget-object v4, v6, Lasw;->b:Ljava/lang/String;

    iget-object v5, v6, Lasw;->c:Ljava/lang/String;

    iget-object v6, v6, Lasw;->d:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/gencode/authzen/server/api/FinishEnrollmentRequestEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/FinishEnrollmentRequestEntity;

    move-object v5, v0

    const-string v4, "enrollment/finish"

    iget-object v1, v8, Lasu;->a:Lbmi;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/FinishEnrollmentResponseEntity;

    move-object v2, v9

    invoke-virtual/range {v1 .. v6}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    const-string v1, "AuthZen"

    const-string v2, "Enrollment phase2 complete."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x2

    iget-object v3, p0, Lanu;->a:Landroid/content/Context;

    new-instance v4, Lbhv;

    invoke-direct {v4, v3}, Lbhv;-><init>(Landroid/content/Context;)V

    const-string v3, "authzen_encryption_key_validity_period_months"

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Lbhv;->a(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-string v3, "AuthZen"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Increasing encryption key\'s expiration time to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lanu;->d:Lano;

    invoke-static {v7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v3, Lano;->b:Lanr;

    invoke-virtual {v3, v7, v1, v2}, Lanr;->a(Lanj;J)Z
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lanu;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    return-object v7

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v2, Lanv;

    const-string v3, "Key is not valid"

    invoke-direct {v2, v3, v1}, Lanv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lanu;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v1

    :catch_1
    move-exception v1

    :try_start_2
    new-instance v2, Lanv;

    const-string v3, "KeySpec is not valid"

    invoke-direct {v2, v3, v1}, Lanv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v1

    new-instance v2, Lanv;

    const-string v3, "Failed to fetch an auth token"

    invoke-direct {v2, v3, v1}, Lanv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_3
    move-exception v1

    new-instance v2, Lanv;

    const-string v3, "Error while talking to apiary"

    invoke-direct {v2, v3, v1}, Lanv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_4
    move-exception v1

    new-instance v2, Lanv;

    const-string v3, "Encryption algorithm not found"

    invoke-direct {v2, v3, v1}, Lanv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
