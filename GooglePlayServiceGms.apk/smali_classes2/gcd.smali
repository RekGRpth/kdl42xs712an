.class public final Lgcd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgas;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lfsy;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Landroid/net/Uri;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfsy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgcd;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput p2, p0, Lgcd;->c:I

    iput-object p3, p0, Lgcd;->d:Ljava/lang/String;

    iput-object p4, p0, Lgcd;->e:Landroid/net/Uri;

    iput-object p5, p0, Lgcd;->f:Ljava/lang/String;

    iput-object p6, p0, Lgcd;->g:Ljava/lang/String;

    iput-object p7, p0, Lgcd;->h:Ljava/lang/String;

    iput-object p8, p0, Lgcd;->b:Lfsy;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lfrx;)V
    .locals 11

    const/4 v10, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x0

    :try_start_0
    iget-object v1, p0, Lgcd;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v2, p0, Lgcd;->c:I

    iget-object v3, p0, Lgcd;->d:Ljava/lang/String;

    iget-object v4, p0, Lgcd;->e:Landroid/net/Uri;

    iget-object v5, p0, Lgcd;->f:Ljava/lang/String;

    iget-object v6, p0, Lgcd;->g:Ljava/lang/String;

    iget-object v7, p0, Lgcd;->h:Ljava/lang/String;

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Lfrx;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    sget-object v0, Lftv;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v5

    move v1, v8

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;

    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x1

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    const-string v7, "momentImpl"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/moments/MomentEntity;->k()[B

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    invoke-virtual {v5, v6}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lbgt;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iget-object v1, p0, Lgcd;->b:Lfsy;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1/MomentsFeed;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v3, v2}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lane; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lane;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgcd;->b:Lfsy;

    invoke-static {v10, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v9, v9}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v0, p0, Lgcd;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lfmt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lgcd;->b:Lfsy;

    invoke-static {v10, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v9, v9}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_2
    move-exception v0

    iget-object v0, p0, Lgcd;->b:Lfsy;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v9, v9}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lgcd;->b:Lfsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgcd;->b:Lfsy;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v2, v2}, Lfsy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
