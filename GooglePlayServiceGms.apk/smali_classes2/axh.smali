.class final Laxh;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Laxb;


# direct methods
.method private constructor <init>(Laxb;)V
    .locals 0

    iput-object p1, p0, Laxh;->a:Laxb;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laxb;B)V
    .locals 0

    invoke-direct {p0, p1}, Laxh;-><init>(Laxb;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Laxh;->a:Laxb;

    invoke-static {v0}, Laxb;->a(Laxb;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {}, Laxb;->h()Laye;

    move-result-object v3

    const-string v4, "connectivity state changed. connected? %b, errorState? %b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v6, p0, Laxh;->a:Laxb;

    invoke-static {v6}, Laxb;->b(Laxb;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Laxh;->a:Laxb;

    invoke-static {v1}, Laxb;->c(Laxb;)V

    if-nez v0, :cond_0

    iget-object v1, p0, Laxh;->a:Laxb;

    invoke-virtual {v1}, Laxb;->d()V

    :cond_0
    iget-object v1, p0, Laxh;->a:Laxb;

    invoke-static {v1}, Laxb;->d(Laxb;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Laxh;->a:Laxb;

    invoke-static {v1}, Laxb;->e(Laxb;)V

    iget-object v1, p0, Laxh;->a:Laxb;

    invoke-static {v1}, Laxb;->f(Laxb;)Z

    :cond_1
    if-eqz v0, :cond_4

    invoke-static {}, Laxb;->h()Laye;

    move-result-object v0

    const-string v1, "re-established connectivity after connectivity changed;  restarting scan"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxh;->a:Laxb;

    invoke-static {v0}, Laxb;->g(Laxb;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Laxh;->a:Laxb;

    invoke-static {v0}, Laxb;->b(Laxb;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Laxb;->h()Laye;

    move-result-object v0

    const-string v1, "lost connectivity while scanning;"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Laye;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Laxh;->a:Laxb;

    invoke-virtual {v0}, Laxb;->g()V

    goto :goto_1
.end method
