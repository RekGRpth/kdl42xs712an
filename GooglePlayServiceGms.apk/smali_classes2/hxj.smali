.class public final Lhxj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhxk;


# instance fields
.field final a:Ljava/util/ArrayList;

.field private d:Landroid/location/Location;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhxj;->d:Landroid/location/Location;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhxj;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;IJLandroid/location/Location;D)Lhxn;
    .locals 10

    iget-object v0, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->a()D

    move-result-wide v6

    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v6, v6, v8

    if-eqz v6, :cond_2

    iget-object v6, p0, Lhxj;->d:Landroid/location/Location;

    if-eq v6, p5, :cond_1

    :cond_2
    invoke-virtual {v0, p3, p4, p5}, Lhxp;->a(JLandroid/location/Location;)B

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    if-nez v3, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :cond_3
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/4 v7, 0x2

    if-ne v6, v7, :cond_6

    if-nez v2, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    const/4 v7, 0x4

    if-ne v6, v7, :cond_1

    if-nez v1, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_7
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    iput-object p5, p0, Lhxj;->d:Landroid/location/Location;

    if-nez v3, :cond_9

    if-nez v2, :cond_9

    if-nez v1, :cond_9

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_9
    new-instance v0, Lhxn;

    invoke-direct {v0, v3, v2, v1}, Lhxn;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Iterable;DI)Ljava/util/List;
    .locals 1

    new-instance v0, Lhxo;

    invoke-direct {v0, p2, p3}, Lhxo;-><init>(D)V

    invoke-virtual {p0, p1, p4, v0}, Lhxj;->a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;
    .locals 7

    iget-object v0, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->a()D

    move-result-wide v3

    const-wide v5, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_1

    iget-object v3, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_4
    invoke-static {p3}, Lisp;->a(Ljava/util/Comparator;)Lisp;

    move-result-object v0

    iget-object v1, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p2}, Lisp;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 1

    const-string v0, "Location updater: AllGeofenceLocationUpdater."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/Iterable;I)V
    .locals 1

    iget-object v0, p0, Lhxj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final b(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 2

    const/16 v0, 0x32

    sget-object v1, Lhxj;->b:Ljava/util/Comparator;

    invoke-virtual {p0, p1, v0, v1}, Lhxj;->a(Ljava/lang/Iterable;ILjava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Iterable;I)Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxp;

    invoke-virtual {v0}, Lhxp;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_3
    sget-object v0, Lhxj;->c:Ljava/util/Comparator;

    invoke-static {v0}, Lisp;->a(Ljava/util/Comparator;)Lisp;

    move-result-object v0

    invoke-virtual {v0, v1, p2}, Lisp;->a(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method
