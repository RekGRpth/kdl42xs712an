.class public final Ljeg;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljea;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lizs;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Ljeg;->a:Ljea;

    const/4 v0, -0x1

    iput v0, p0, Ljeg;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Ljeg;->a:Ljea;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Ljeg;->a:Ljea;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iput v0, p0, Ljeg;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Ljeg;->a:Ljea;

    if-nez v0, :cond_1

    new-instance v0, Ljea;

    invoke-direct {v0}, Ljea;-><init>()V

    iput-object v0, p0, Ljeg;->a:Ljea;

    :cond_1
    iget-object v0, p0, Ljeg;->a:Ljea;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Ljeg;->a:Ljea;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljeg;->a:Ljea;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljeg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljeg;

    iget-object v2, p0, Ljeg;->a:Ljea;

    if-nez v2, :cond_3

    iget-object v2, p1, Ljeg;->a:Ljea;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljeg;->a:Ljea;

    iget-object v3, p1, Ljeg;->a:Ljea;

    invoke-virtual {v2, v3}, Ljea;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Ljeg;->a:Ljea;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    return v0

    :cond_0
    iget-object v0, p0, Ljeg;->a:Ljea;

    invoke-virtual {v0}, Ljea;->hashCode()I

    move-result v0

    goto :goto_0
.end method
