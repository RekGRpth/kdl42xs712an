.class final Lbrh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final a:I

.field final b:Landroid/os/ParcelFileDescriptor;

.field final c:Lbsy;

.field d:Lcom/google/android/gms/drive/database/data/EntrySpec;

.field final e:Lcom/google/android/gms/drive/auth/AppIdentity;

.field final f:Landroid/os/IBinder;

.field final g:J

.field final synthetic h:Lbrg;


# direct methods
.method constructor <init>(Lbrg;Landroid/os/ParcelFileDescriptor;Lbsy;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;Landroid/os/IBinder;)V
    .locals 2

    iput-object p1, p0, Lbrh;->h:Lbrg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lbrh;->c:Lbsy;

    iput-object p4, p0, Lbrh;->d:Lcom/google/android/gms/drive/database/data/EntrySpec;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    iput-object v0, p0, Lbrh;->b:Landroid/os/ParcelFileDescriptor;

    iget-object v0, p1, Lbrg;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lbrh;->a:I

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lbrh;->e:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {p6}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    iput-object v0, p0, Lbrh;->f:Landroid/os/IBinder;

    iget-object v0, p1, Lbrg;->b:Lcon;

    invoke-interface {v0}, Lcon;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lbrh;->g:J

    return-void
.end method


# virtual methods
.method final a()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lbrh;->f:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method final b()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lbrh;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lbrh;->c:Lbsy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrh;->c:Lbsy;

    invoke-virtual {v0}, Lbsy;->c()V

    :cond_0
    iget-object v0, p0, Lbrh;->f:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    return-void

    :catch_0
    move-exception v0

    const-string v0, "HashBasedOpenContentsStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to close file: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lbrh;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final binderDied()V
    .locals 5

    const-string v0, "HashBasedOpenContentsStore"

    const-string v1, "Client died with open content: %d."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbrh;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lbrh;->h:Lbrg;

    invoke-virtual {v0, p0}, Lbrg;->a(Lbrh;)V

    invoke-virtual {p0}, Lbrh;->b()V

    return-void
.end method
