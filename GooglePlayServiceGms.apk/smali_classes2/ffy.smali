.class final Lffy;
.super Lfgu;
.source "SourceFile"


# direct methods
.method constructor <init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lfgu;-><init>(Landroid/content/Context;Lfbz;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lffe;Lffa;)Landroid/util/Pair;
    .locals 6

    invoke-virtual {p2, p3}, Lffe;->a(Lffa;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "circles.firstTimeAdd.needConsent"

    invoke-static {v0, v2}, Lffu;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Z

    move-result v2

    const-string v3, "circles.firstTimeAdd.text"

    invoke-static {v0, v3}, Lffu;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0b01c1    # com.google.android.gms.R.string.people_first_time_add_title_text

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0b01c2    # com.google.android.gms.R.string.people_first_time_add_ok_text

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "circles.first_time_add_need_consent"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "circles.first_time_add_text"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circles.first_time_add_title_text"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circles.first_time_add_ok_text"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lffc;->c:Lffc;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
