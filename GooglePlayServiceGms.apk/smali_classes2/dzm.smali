.class public final Ldzm;
.super Ldwx;
.source "SourceFile"


# instance fields
.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Ldvn;Landroid/view/View$OnClickListener;)V
    .locals 1

    sget v0, Lxb;->f:I

    invoke-direct {p0, p1, v0}, Ldwx;-><init>(Landroid/content/Context;I)V

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Ldvn;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Ldzm;->g:Landroid/view/LayoutInflater;

    iput-object p2, p0, Ldzm;->h:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Ldzm;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Ldzm;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
    .locals 5

    check-cast p4, Ldfb;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzn;

    iget-object v1, v0, Ldzn;->e:Ldzm;

    iget-boolean v1, v1, Ldvj;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldzn;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p4}, Ldfb;->c()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->A:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    iget-object v1, v0, Ldzn;->c:Landroid/database/CharArrayBuffer;

    invoke-interface {p4, v1}, Ldfb;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, v0, Ldzn;->b:Landroid/widget/TextView;

    iget-object v2, v0, Ldzn;->c:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v3, 0x0

    iget-object v4, v0, Ldzn;->c:Landroid/database/CharArrayBuffer;

    iget v4, v4, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/TextView;->setText([CII)V

    iget-object v0, v0, Ldzn;->d:Landroid/view/View;

    invoke-virtual {v0, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_0
    iget-object v1, v0, Ldzn;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public final l()Landroid/view/View;
    .locals 4

    iget-object v0, p0, Ldzm;->g:Landroid/view/LayoutInflater;

    sget v1, Lxc;->s:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ldzn;

    invoke-direct {v1, p0, v0}, Ldzn;-><init>(Ldzm;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method
