.class public final Lbrz;
.super Lbro;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/drive/internal/ListParentsRequest;


# direct methods
.method public constructor <init>(Lbrc;Lcom/google/android/gms/drive/internal/ListParentsRequest;Lchq;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lbro;-><init>(Lbrc;Lchq;)V

    iput-object p2, p0, Lbrz;->c:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;)V
    .locals 4

    iget-object v0, p0, Lbrz;->c:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    const-string v1, "Invalid getParents request: request must be provided"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrz;->c:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/ListParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid getParents request: DriveId must be provided"

    invoke-static {v0, v1}, Lbqw;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lbrz;->a:Landroid/os/IInterface;

    check-cast v0, Lchq;

    new-instance v1, Lcom/google/android/gms/drive/internal/OnListParentsResponse;

    iget-object v2, p0, Lbrz;->b:Lbrc;

    iget-object v3, p0, Lbrz;->c:Lcom/google/android/gms/drive/internal/ListParentsRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/ListParentsRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v2, v3}, Lbrc;->d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/internal/OnListParentsResponse;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-interface {v0, v1}, Lchq;->a(Lcom/google/android/gms/drive/internal/OnListParentsResponse;)V

    return-void
.end method
