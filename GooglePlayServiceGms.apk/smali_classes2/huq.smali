.class public final Lhuq;
.super Lhug;
.source "SourceFile"


# static fields
.field public static final c:Lhuf;


# instance fields
.field public final a:I

.field public final b:Lhus;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhur;

    invoke-direct {v0}, Lhur;-><init>()V

    sput-object v0, Lhuq;->c:Lhuf;

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lhug;-><init>(IIII)V

    iput p5, p0, Lhuq;->a:I

    const/16 v0, 0x50

    if-lt p4, v0, :cond_0

    const/16 v0, 0x54

    if-gt p4, v0, :cond_0

    sget-object v0, Lhus;->b:Lhus;

    :goto_0
    iput-object v0, p0, Lhuq;->b:Lhus;

    return-void

    :cond_0
    const/16 v0, 0x55

    if-lt p4, v0, :cond_1

    const/16 v0, 0x59

    if-gt p4, v0, :cond_1

    sget-object v0, Lhus;->c:Lhus;

    goto :goto_0

    :cond_1
    const/16 v0, 0x5a

    if-lt p4, v0, :cond_2

    const/16 v0, 0x5e

    if-gt p4, v0, :cond_2

    sget-object v0, Lhus;->d:Lhus;

    goto :goto_0

    :cond_2
    sget-object v0, Lhus;->a:Lhus;

    goto :goto_0
.end method

.method static synthetic a(Lhuq;)I
    .locals 1

    iget v0, p0, Lhuq;->i:I

    return v0
.end method

.method static synthetic a(Lhuq;I)I
    .locals 0

    iput p1, p0, Lhuq;->i:I

    return p1
.end method

.method public static a(Ljava/io/PrintWriter;Lhuq;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0, p1}, Lhug;->a(Ljava/io/PrintWriter;Lhug;)V

    const-string v0, ", Uncert="

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p1, Lhuq;->a:I

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, "mm, "

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lhuq;->b:Lhus;

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Lhuq;)V
    .locals 1

    if-nez p1, :cond_0

    const-string v0, "null"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return-void

    :cond_0
    const-string v0, "["

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lhug;->a(Ljava/lang/StringBuilder;Lhug;)V

    const-string v0, ", Uncert="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p1, Lhuq;->a:I

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "mm, "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lhuq;->b:Lhus;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static final c()Lhuq;
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Lhuq;

    const/4 v3, -0x2

    const/16 v4, 0x55

    const v5, 0x9c40

    move v2, v1

    invoke-direct/range {v0 .. v5}, Lhuq;-><init>(IIIII)V

    const/4 v1, 0x1

    iput v1, v0, Lhuq;->i:I

    return-object v0
.end method


# virtual methods
.method public final d()V
    .locals 1

    iget v0, p0, Lhuq;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhuq;->i:I

    return-void
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lhuq;->i:I

    return v0
.end method

.method public final f()Z
    .locals 1

    iget v0, p0, Lhuq;->i:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WifiApPosition ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Lhug;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", horizontalUncertaintyMm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lhuq;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", positionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lhuq;->b:Lhus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
