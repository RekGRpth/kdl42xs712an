.class public final Lgtw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[C

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/util/Comparator;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lixo;

.field private final f:Ljava/lang/CharSequence;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x52

    aput-char v2, v0, v1

    sput-object v0, Lgtw;->a:[C

    const/4 v0, 0x0

    sput-object v0, Lgtw;->b:Ljava/lang/String;

    new-instance v0, Lgtx;

    invoke-direct {v0}, Lgtx;-><init>()V

    sput-object v0, Lgtw;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lixo;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p1, p0, Lgtw;->d:Ljava/lang/String;

    iput-object p2, p0, Lgtw;->e:Lixo;

    iput-object p3, p0, Lgtw;->f:Ljava/lang/CharSequence;

    iput-object p4, p0, Lgtw;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lgtw;->h:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    iput-object p1, p0, Lgtw;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lgtw;->e:Lixo;

    iput-object p2, p0, Lgtw;->f:Ljava/lang/CharSequence;

    iput-object p3, p0, Lgtw;->g:Ljava/lang/String;

    iput-object p4, p0, Lgtw;->h:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p1, p2, v0}, Lgtw;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgtw;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lixo;
    .locals 1

    iget-object v0, p0, Lgtw;->e:Lixo;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lgtw;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgtw;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgtw;->h:Ljava/lang/String;

    return-object v0
.end method
