.class final Leaw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Leav;

.field private final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/database/CharArrayBuffer;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;


# direct methods
.method public constructor <init>(Leav;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Leaw;->a:Leav;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Leaw;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->aC:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leaw;->c:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Leaw;->d:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->o:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leaw;->e:Landroid/widget/TextView;

    sget v0, Lxa;->az:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leaw;->f:Landroid/view/View;

    iget-object v0, p0, Leaw;->f:Landroid/view/View;

    invoke-static {p1}, Leav;->a(Leav;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->aA:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leaw;->g:Landroid/widget/TextView;

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Leaw;->h:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    const/16 v4, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Leaw;->a:Leav;

    invoke-virtual {v0}, Leav;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leaw;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v2, p0, Leaw;->a:Leav;

    invoke-static {v2}, Leav;->b(Leav;)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lwz;->f:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    iget-object v0, p0, Leaw;->a:Leav;

    invoke-static {v0}, Leav;->b(Leav;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    iget-object v2, p0, Leaw;->d:Landroid/database/CharArrayBuffer;

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/Player;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v0, p0, Leaw;->c:Landroid/widget/TextView;

    iget-object v2, p0, Leaw;->d:Landroid/database/CharArrayBuffer;

    iget-object v2, v2, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v3, p0, Leaw;->d:Landroid/database/CharArrayBuffer;

    iget v3, v3, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v2, v1, v3}, Landroid/widget/TextView;->setText([CII)V

    iget-object v0, p0, Leaw;->e:Landroid/widget/TextView;

    iget-object v2, p0, Leaw;->a:Leav;

    invoke-static {v2}, Leav;->c(Leav;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Leaw;->f:Landroid/view/View;

    iget-object v2, p0, Leaw;->a:Leav;

    invoke-static {v2}, Leav;->b(Leav;)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Leaw;->h:Landroid/view/View;

    iget-object v2, p0, Leaw;->a:Leav;

    invoke-static {v2}, Leav;->b(Leav;)Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Leaw;->a:Leav;

    invoke-static {v0}, Leav;->d(Leav;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Leaw;->h:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Leaw;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Leaw;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Leaw;->g:Landroid/widget/TextView;

    iget-object v1, p0, Leaw;->a:Leav;

    invoke-static {v1}, Leav;->d(Leav;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_0
    iget-object v0, p0, Leaw;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Leaw;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Leaw;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Leaw;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Leaw;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    sget v1, Lxd;->i:I

    invoke-virtual {v0, v1}, Lov;->a(I)V

    new-instance v1, Ledk;

    iget-object v2, p0, Leaw;->a:Leav;

    invoke-static {v2}, Leav;->e(Leav;)Ledl;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v1, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    return-void
.end method
