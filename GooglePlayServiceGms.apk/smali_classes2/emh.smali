.class public final Lemh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[I

.field private final c:Ljava/util/Map;

.field private final d:Landroid/content/res/Resources;

.field private e:Z

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lehh;Landroid/content/res/Resources;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lehh;->d:Ljava/lang/String;

    iput-object v0, p0, Lemh;->a:Ljava/lang/String;

    invoke-static {}, Laia;->a()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lemh;->b:[I

    iget-object v1, p1, Lehh;->l:[Leim;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lemh;->b:[I

    iget v5, v3, Leim;->a:I

    iget v3, v3, Leim;->c:I

    aput v3, v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lemh;->a(Lehh;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lemh;->c:Ljava/util/Map;

    iput-object p2, p0, Lemh;->d:Landroid/content/res/Resources;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;Landroid/content/res/Resources;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lemh;->a:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lemh;->b:[I

    :goto_0
    invoke-static {p2}, Lemh;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lemh;->c:Ljava/util/Map;

    iput-object p3, p0, Lemh;->d:Landroid/content/res/Resources;

    return-void

    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;->b:[I

    iput-object v0, p0, Lemh;->b:[I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a(Lehh;)Ljava/util/Map;
    .locals 5

    iget-object v0, p0, Lehh;->k:[Leii;

    array-length v1, v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lehh;->k:[Leii;

    aget-object v3, v3, v0

    iget-object v3, v3, Leii;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private a([Lemi;Ljava/util/List;)V
    .locals 6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_3

    :try_start_0
    aget-object v2, p1, v1

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-instance v0, Lemm;

    iget-object v3, v2, Lemi;->b:Ljava/lang/String;

    iget-object v4, p0, Lemh;->c:Ljava/util/Map;

    invoke-direct {v0, v3, v4}, Lemm;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    new-instance v3, Lein;

    invoke-direct {v3}, Lein;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    invoke-virtual {v0}, Lemm;->a()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lemm;->h()Leio;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lemk; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v2, Lemb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "In global search section spec for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Laia;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lemk;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    :try_start_1
    iget-object v0, v3, Lein;->a:[Leio;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Leio;

    iput-object v0, v3, Lein;->a:[Leio;

    new-instance v0, Leim;

    invoke-direct {v0}, Leim;-><init>()V

    iget v2, v2, Lemi;->a:I

    iput v2, v0, Leim;->c:I

    iput v1, v0, Leim;->a:I

    iput-object v3, v0, Leim;->b:Lein;
    :try_end_1
    .catch Lemk; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lemh;->b:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lemh;->d:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    new-instance v0, Leme;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get resources for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lemh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leme;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lemh;->b:[I

    array-length v0, v0

    new-array v1, v0, [Lemi;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lemh;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lemh;->b:[I

    aget v2, v2, v0

    if-eqz v2, :cond_1

    :try_start_0
    new-instance v2, Lemi;

    iget-object v3, p0, Lemh;->b:[I

    aget v3, v3, v0

    iget-object v4, p0, Lemh;->d:Landroid/content/res/Resources;

    iget-object v5, p0, Lemh;->b:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lemi;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Lemb;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid resource ID for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Laia;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lemh;->b:[I

    aget v0, v4, v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lemb;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :try_start_1
    invoke-direct {p0, v1, v0}, Lemh;->a([Lemi;Ljava/util/List;)V

    iput-object v0, p0, Lemh;->f:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-boolean v6, p0, Lemh;->e:Z

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Lemh;->e:Z

    throw v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lemh;->f:Ljava/util/List;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lemh;->e:Z

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lemh;->a()V
    :try_end_0
    .catch Lemc; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget-object v0, p0, Lemh;->f:Ljava/util/List;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
