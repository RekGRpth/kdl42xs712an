.class final Lazf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/cast/CastDevice;

.field final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object p2, p0, Lazf;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v2, p1, Lazf;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eq p1, p0, :cond_1

    check-cast p1, Lazf;

    iget-object v2, p0, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v3, p1, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v2, v3}, Laxy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lazf;->b:Ljava/util/Set;

    iget-object v3, p1, Lazf;->b:Ljava/util/Set;

    invoke-static {v2, v3}, Laxy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lazf;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lazf;->b:Ljava/util/Set;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
