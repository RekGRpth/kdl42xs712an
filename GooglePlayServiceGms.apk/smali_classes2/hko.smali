.class final Lhko;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lidu;

.field private final c:Lhlk;

.field private d:Z

.field private e:Lhqy;

.field private f:[D

.field private g:J

.field private h:J

.field private i:J

.field private j:Lhms;

.field private k:Lhkp;

.field private l:D


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lhko;->a:Ljava/util/Map;

    sget-object v1, Lhmz;->a:Lhmz;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhko;->a:Ljava/util/Map;

    sget-object v1, Lhmz;->b:Lhmz;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhko;->a:Ljava/util/Map;

    sget-object v1, Lhmz;->c:Lhmz;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhko;->a:Ljava/util/Map;

    sget-object v1, Lhmz;->d:Lhmz;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lhko;->a:Ljava/util/Map;

    sget-object v1, Lhmz;->f:Lhmz;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Lidu;Lhlk;)V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhko;->d:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhko;->f:[D

    iput-wide v1, p0, Lhko;->g:J

    iput-wide v1, p0, Lhko;->h:J

    const-wide v0, 0xbebc2000L

    iput-wide v0, p0, Lhko;->i:J

    new-instance v0, Lhmr;

    invoke-direct {v0}, Lhmr;-><init>()V

    iput-object v0, p0, Lhko;->j:Lhms;

    const-wide v0, 0x4041800000000000L    # 35.0

    iput-wide v0, p0, Lhko;->l:D

    iput-object p1, p0, Lhko;->b:Lidu;

    iput-object p2, p0, Lhko;->c:Lhlk;

    return-void
.end method

.method static synthetic a(Lhko;)Z
    .locals 1

    iget-boolean v0, p0, Lhko;->d:Z

    return v0
.end method

.method static synthetic b()Ljava/util/Map;
    .locals 1

    sget-object v0, Lhko;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lhko;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhko;->d:Z

    return v0
.end method

.method static synthetic c(Lhko;)Lidu;
    .locals 1

    iget-object v0, p0, Lhko;->b:Lidu;

    return-object v0
.end method

.method static synthetic d(Lhko;)Lhlk;
    .locals 1

    iget-object v0, p0, Lhko;->c:Lhlk;

    return-object v0
.end method

.method static synthetic e(Lhko;)Lhkp;
    .locals 1

    iget-object v0, p0, Lhko;->k:Lhkp;

    return-object v0
.end method

.method static synthetic f(Lhko;)Lhms;
    .locals 1

    iget-object v0, p0, Lhko;->j:Lhms;

    return-object v0
.end method

.method static synthetic g(Lhko;)J
    .locals 2

    iget-wide v0, p0, Lhko;->i:J

    return-wide v0
.end method


# virtual methods
.method final a(JJ[DLjava/util/List;)Lcom/google/android/gms/location/ActivityRecognitionResult;
    .locals 14

    const/4 v3, 0x0

    iget-object v2, p0, Lhko;->f:[D

    if-eqz v2, :cond_2

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhmy;

    iget-object v2, v2, Lhmy;->a:Lhmz;

    sget-object v4, Lhmz;->f:Lhmz;

    if-ne v2, v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_2

    iget-object v2, p0, Lhko;->f:[D

    new-instance v4, Lilw;

    move-object/from16 v0, p5

    invoke-direct {v4, v0}, Lilw;-><init>([D)V

    new-instance v5, Lilw;

    invoke-direct {v5, v2}, Lilw;-><init>([D)V

    iget-wide v6, v5, Lilw;->a:D

    iget-wide v8, v5, Lilw;->b:D

    iget-wide v10, v5, Lilw;->c:D

    iget-wide v12, v4, Lilw;->a:D

    mul-double/2addr v6, v12

    iget-wide v12, v4, Lilw;->b:D

    mul-double/2addr v8, v12

    add-double/2addr v6, v8

    iget-wide v8, v4, Lilw;->c:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-virtual {v4}, Lilw;->a()D

    move-result-wide v8

    invoke-virtual {v5}, Lilw;->a()D

    move-result-wide v4

    mul-double/2addr v4, v8

    div-double v4, v6, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "ActivityDetector"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Angle change since last classification is: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-wide v6, p0, Lhko;->l:D

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_2

    iget-wide v2, p0, Lhko;->g:J

    sub-long v2, p1, v2

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    sub-long v4, p1, v2

    iget-wide v2, p0, Lhko;->h:J

    sub-long v2, p3, v2

    const-wide/16 v6, 0x2

    div-long/2addr v2, v6

    sub-long v6, p3, v2

    new-instance v2, Lcom/google/android/gms/location/ActivityRecognitionResult;

    new-instance v3, Lcom/google/android/gms/location/DetectedActivity;

    const/4 v8, 0x5

    const/16 v9, 0x64

    invoke-direct {v3, v8, v9}, Lcom/google/android/gms/location/DetectedActivity;-><init>(II)V

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/location/ActivityRecognitionResult;-><init>(Lcom/google/android/gms/location/DetectedActivity;JJ)V

    :goto_1
    move-object/from16 v0, p5

    iput-object v0, p0, Lhko;->f:[D

    iput-wide p1, p0, Lhko;->g:J

    move-wide/from16 v0, p3

    iput-wide v0, p0, Lhko;->h:J

    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move-object v2, v3

    goto :goto_1
.end method

.method final a()V
    .locals 1

    iget-boolean v0, p0, Lhko;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhko;->d:Z

    iget-object v0, p0, Lhko;->e:Lhqy;

    invoke-interface {v0}, Lhqy;->b()V

    :cond_0
    return-void
.end method

.method final a(IJIDLhms;Lhkp;Lilx;)V
    .locals 12

    iget-boolean v2, p0, Lhko;->d:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "ActivityDetector"

    const-string v3, "Activity detection requested when already in progress. Ignoring."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p7

    iput-object v0, p0, Lhko;->j:Lhms;

    move-object/from16 v0, p8

    iput-object v0, p0, Lhko;->k:Lhkp;

    iput-wide p2, p0, Lhko;->i:J

    move-wide/from16 v0, p5

    iput-wide v0, p0, Lhko;->l:D

    const/4 v2, 0x1

    iput-boolean v2, p0, Lhko;->d:Z

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    sget-object v2, Lhrz;->d:Lhrz;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v8, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/location/collectionlib/SensorScannerConfig;

    const-wide/16 v4, 0x5dc

    const-wide/32 v6, 0xf4240

    div-long v6, p2, v6

    move/from16 v3, p4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/location/collectionlib/SensorScannerConfig;-><init>(IJJ)V

    iget-object v3, p0, Lhko;->b:Lidu;

    const/4 v4, 0x1

    new-array v4, v4, [Lhrz;

    const/4 v5, 0x0

    sget-object v6, Lhrz;->d:Lhrz;

    aput-object v6, v4, v5

    invoke-static {v4}, Lhrz;->a([Lhrz;)Ljava/util/Set;

    move-result-object v4

    const-wide/16 v6, 0x0

    new-instance v9, Lhkq;

    const/4 v5, 0x0

    invoke-direct {v9, p0, v5}, Lhkq;-><init>(Lhko;B)V

    const-string v10, "SignalCollector"

    move-object v5, v8

    move-object v8, v2

    move-object/from16 v11, p9

    invoke-interface/range {v3 .. v11}, Lidu;->a(Ljava/util/Set;Ljava/util/Map;JLcom/google/android/location/collectionlib/SensorScannerConfig;Lhqq;Ljava/lang/String;Lilx;)Lhqy;

    move-result-object v2

    iput-object v2, p0, Lhko;->e:Lhqy;

    iget-object v2, p0, Lhko;->e:Lhqy;

    invoke-interface {v2}, Lhqy;->a()V

    goto :goto_0
.end method
