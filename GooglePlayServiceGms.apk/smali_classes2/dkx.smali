.class final Ldkx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldkk;

.field final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Landroid/net/LocalServerSocket;

.field f:Landroid/net/LocalSocket;

.field final g:Ldky;


# direct methods
.method constructor <init>(Ljava/lang/String;Ldkk;Ljava/lang/String;Landroid/net/LocalServerSocket;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldkx;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Ldkx;->c:Ljava/lang/String;

    iput-object p2, p0, Ldkx;->a:Ldkk;

    iput-object p4, p0, Ldkx;->e:Landroid/net/LocalServerSocket;

    iput-object p3, p0, Ldkx;->d:Ljava/lang/String;

    new-instance v0, Ldky;

    invoke-direct {v0, p0, v1}, Ldky;-><init>(Ldkx;B)V

    iput-object v0, p0, Ldkx;->g:Ldky;

    iget-object v0, p0, Ldkx;->g:Ldky;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ldky;->setPriority(I)V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    iget-object v0, p0, Ldkx;->f:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkx;->f:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkx;->f:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    :cond_0
    return-void
.end method
