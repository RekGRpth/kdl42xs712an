.class public final Lgvy;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field final a:[C

.field b:Ljava/util/ArrayList;

.field c:Ljava/lang/CharSequence;

.field private final d:Landroid/content/Context;

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:C

.field private final i:[C

.field private final j:Ljava/util/List;

.field private final k:Landroid/view/LayoutInflater;

.field private l:Lgvz;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    if-eqz p5, :cond_0

    array-length v0, p5

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "remainingFields are required"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    if-eqz p7, :cond_1

    invoke-interface {p7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "addressSources are required"

    invoke-static {v1, v0}, Lbkm;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lgvy;->d:Landroid/content/Context;

    const v0, 0x7f04013c    # com.google.android.gms.R.layout.wallet_row_address_hint_spinner

    iput v0, p0, Lgvy;->e:I

    iput p2, p0, Lgvy;->f:I

    iput-object p3, p0, Lgvy;->g:Ljava/lang/String;

    iput-char p4, p0, Lgvy;->h:C

    invoke-static {p5}, Lgvy;->a([C)[C

    move-result-object v0

    iput-object v0, p0, Lgvy;->a:[C

    if-eqz p6, :cond_2

    invoke-virtual {p6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lgvy;->i:[C

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lgvy;->j:Ljava/util/List;

    iget-object v0, p0, Lgvy;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgvy;->k:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgvy;->b:Ljava/util/ArrayList;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lgvy;->k:Landroid/view/LayoutInflater;

    iget v1, p0, Lgvy;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1}, Lgvy;->a(I)Lgtw;

    move-result-object v1

    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lgtw;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method private a(I)Lgtw;
    .locals 1

    iget-object v0, p0, Lgvy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtw;

    return-object v0
.end method

.method static synthetic a(Lgvy;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lgvy;->j:Ljava/util/List;

    return-object v0
.end method

.method private static final a([C)[C
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-char v1, p0, v0

    invoke-static {v1}, Lhgp;->a(C)Z

    move-result v1

    if-nez v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    array-length v1, p0

    invoke-static {p0, v0, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fields must contain at least one valid field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic b(Lgvy;)C
    .locals 1

    iget-char v0, p0, Lgvy;->h:C

    return v0
.end method

.method static synthetic c(Lgvy;)I
    .locals 1

    iget v0, p0, Lgvy;->f:I

    return v0
.end method

.method static synthetic d(Lgvy;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgvy;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lgvy;)[C
    .locals 1

    iget-object v0, p0, Lgvy;->i:[C

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lgvy;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lgvy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lgvy;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lgvy;->l:Lgvz;

    if-nez v0, :cond_0

    new-instance v0, Lgvz;

    invoke-direct {v0, p0}, Lgvz;-><init>(Lgvy;)V

    iput-object v0, p0, Lgvy;->l:Lgvz;

    :cond_0
    iget-object v0, p0, Lgvy;->l:Lgvz;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lgvy;->a(I)Lgtw;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lgvy;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
