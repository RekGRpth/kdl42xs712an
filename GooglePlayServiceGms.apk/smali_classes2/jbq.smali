.class public final Ljbq;
.super Lizk;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljbu;

.field private c:Z

.field private d:Ljbu;

.field private e:Z

.field private f:Ljbu;

.field private g:Z

.field private h:Ljbu;

.field private i:Z

.field private j:Ljbu;

.field private k:Z

.field private l:I

.field private m:Ljava/util/List;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Ljava/lang/String;

.field private t:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    iput-object v0, p0, Ljbq;->b:Ljbu;

    iput-object v0, p0, Ljbq;->d:Ljbu;

    iput-object v0, p0, Ljbq;->f:Ljbu;

    iput-object v0, p0, Ljbq;->h:Ljbu;

    iput-object v0, p0, Ljbq;->j:Ljbu;

    iput v1, p0, Ljbq;->l:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljbq;->m:Ljava/util/List;

    iput-boolean v1, p0, Ljbq;->o:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljbq;->q:Z

    const-string v0, ""

    iput-object v0, p0, Ljbq;->s:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Ljbq;->t:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Ljbq;->t:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Ljbq;->b()I

    :cond_0
    iget v0, p0, Ljbq;->t:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 4

    const/4 v3, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v3, p0, Ljbq;->a:Z

    iput-object v0, p0, Ljbq;->b:Ljbu;

    goto :goto_0

    :sswitch_2
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v3, p0, Ljbq;->c:Z

    iput-object v0, p0, Ljbq;->d:Ljbu;

    goto :goto_0

    :sswitch_3
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v3, p0, Ljbq;->e:Z

    iput-object v0, p0, Ljbq;->f:Ljbu;

    goto :goto_0

    :sswitch_4
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v3, p0, Ljbq;->g:Z

    iput-object v0, p0, Ljbq;->h:Ljbu;

    goto :goto_0

    :sswitch_5
    new-instance v0, Ljbu;

    invoke-direct {v0}, Ljbu;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v3, p0, Ljbq;->i:Z

    iput-object v0, p0, Ljbq;->j:Ljbu;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->g()I

    move-result v0

    iput-boolean v3, p0, Ljbq;->k:Z

    iput v0, p0, Ljbq;->l:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iget-object v2, p0, Ljbq;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ljbq;->m:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Ljbq;->m:Ljava/util/List;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v3, p0, Ljbq;->n:Z

    iput-boolean v0, p0, Ljbq;->o:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    iput-boolean v3, p0, Ljbq;->p:Z

    iput-boolean v0, p0, Ljbq;->q:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v3, p0, Ljbq;->r:Z

    iput-object v0, p0, Ljbq;->s:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 4

    iget-boolean v0, p0, Ljbq;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Ljbq;->b:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Ljbq;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljbq;->d:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_1
    iget-boolean v0, p0, Ljbq;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljbq;->f:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-boolean v0, p0, Ljbq;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljbq;->h:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_3
    iget-boolean v0, p0, Ljbq;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljbq;->j:Ljbu;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_4
    iget-boolean v0, p0, Ljbq;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Ljbq;->l:I

    invoke-virtual {p1, v0, v1}, Lizh;->b(II)V

    :cond_5
    iget-object v0, p0, Ljbq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2, v3}, Lizh;->a(IJ)V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Ljbq;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Ljbq;->o:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Ljbq;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Ljbq;->q:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_8
    iget-boolean v0, p0, Ljbq;->r:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Ljbq;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_9
    return-void
.end method

.method public final b()I
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Ljbq;->a:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    iget-object v2, p0, Ljbq;->b:Ljbu;

    invoke-static {v0, v2}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Ljbq;->c:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Ljbq;->d:Ljbu;

    invoke-static {v2, v3}, Lizh;->b(ILizk;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Ljbq;->e:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Ljbq;->f:Ljbu;

    invoke-static {v2, v3}, Lizh;->b(ILizk;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Ljbq;->g:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Ljbq;->h:Ljbu;

    invoke-static {v2, v3}, Lizh;->b(ILizk;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-boolean v2, p0, Ljbq;->i:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Ljbq;->j:Ljbu;

    invoke-static {v2, v3}, Lizh;->b(ILizk;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Ljbq;->k:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x6

    iget v3, p0, Ljbq;->l:I

    invoke-static {v2, v3}, Lizh;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    :goto_1
    iget-object v0, p0, Ljbq;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lizh;->a(J)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_2

    :cond_4
    add-int v0, v2, v1

    iget-object v1, p0, Ljbq;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v1, p0, Ljbq;->n:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    iget-boolean v2, p0, Ljbq;->o:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljbq;->p:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x9

    iget-boolean v2, p0, Ljbq;->q:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Ljbq;->r:Z

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    iget-object v2, p0, Ljbq;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Ljbq;->t:I

    return v0

    :cond_8
    move v2, v0

    goto :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method
