.class Liby;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Libw;


# static fields
.field private static final a:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Liby;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Liby;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lhug;Lhug;)D
    .locals 12

    const-wide v8, 0x416312d000000000L    # 1.0E7

    iget v0, p0, Lhug;->d:I

    invoke-static {v0}, Liba;->b(I)D

    move-result-wide v0

    iget v2, p1, Lhug;->d:I

    invoke-static {v2}, Liba;->b(I)D

    move-result-wide v2

    iget v4, p1, Lhug;->e:I

    int-to-double v4, v4

    div-double/2addr v4, v8

    iget v6, p0, Lhug;->e:I

    int-to-double v6, v6

    div-double/2addr v6, v8

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private static a(IIID)Lhue;
    .locals 12

    int-to-double v0, p2

    const-wide v2, 0x4158554c00000000L    # 6378800.0

    div-double/2addr v0, v2

    invoke-static {p0}, Liba;->b(I)D

    move-result-wide v2

    invoke-static {p1}, Liba;->b(I)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static/range {p3 .. p4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    invoke-static/range {p3 .. p4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v2, v10

    sub-double/2addr v0, v2

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    add-double/2addr v0, v4

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    rem-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    sub-double/2addr v0, v2

    new-instance v2, Lhue;

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lhue;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method private static a(Lhue;Lhue;)Lhue;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    move-object/from16 v0, p1

    iget-object v1, v0, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, p1

    iget-object v1, v0, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    sub-double/2addr v8, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    mul-double/2addr v8, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v4, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    add-double/2addr v12, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    add-double/2addr v14, v10

    mul-double/2addr v12, v14

    mul-double v14, v8, v8

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    add-double/2addr v1, v10

    invoke-static {v8, v9, v1, v2}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    add-double/2addr v1, v6

    new-instance v3, Lhue;

    const-wide v6, -0x4006de04abbbd2e8L    # -1.5707963267948966

    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const-wide v5, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {v1, v2, v5, v6}, Ljava/lang/Math;->IEEEremainder(DD)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lhue;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3
.end method

.method private static a()Libx;
    .locals 4

    new-instance v0, Libx;

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v3, Libx;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2, v3}, Libx;-><init>(Lhug;ILjava/util/Set;)V

    return-object v0
.end method

.method private static a(Ljava/util/Set;Licb;)Ljava/util/List;
    .locals 3

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Licb;

    invoke-static {v1, p1}, Liby;->a(Licb;Licb;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lhug;Ljava/util/Map;)Ljava/util/Set;
    .locals 5

    sget-object v2, Libx;->a:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhug;

    invoke-static {v1, p0}, Liba;->a(Lhug;Lhug;)I

    move-result v1

    const/16 v4, 0xfa

    if-le v1, v4, :cond_2

    sget-object v1, Libx;->a:Ljava/util/Set;

    if-ne v2, v1, :cond_1

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object v2, v1

    goto :goto_0

    :cond_0
    return-object v2

    :cond_1
    move-object v1, v2

    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_2
.end method

.method private static a(Ljava/util/List;)V
    .locals 5

    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licb;

    iput v1, v0, Licb;->c:I

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licb;

    add-int/lit8 v1, v2, 0x1

    move v3, v1

    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Licb;

    invoke-static {v0, v1}, Liby;->a(Licb;Licb;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Licb;->a()V

    invoke-virtual {v1}, Licb;->a()V

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    return-void
.end method

.method private static a(Ljava/util/List;I)V
    .locals 2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licb;

    iget v0, v0, Licb;->c:I

    if-ne v0, p1, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static a(Licb;Licb;)Z
    .locals 3

    iget-object v0, p0, Licb;->a:Lhug;

    iget-object v1, p1, Licb;->a:Lhug;

    invoke-static {v0, v1}, Liba;->a(Lhug;Lhug;)I

    move-result v2

    iget v0, v0, Lhug;->f:I

    div-int/lit16 v0, v0, 0x3e8

    iget v1, v1, Lhug;->f:I

    div-int/lit16 v1, v1, 0x3e8

    add-int/2addr v0, v1

    if-gt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Lica;
    .locals 5

    const v1, 0x7fffffff

    const/high16 v0, -0x80000000

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licb;

    iget v4, v0, Licb;->c:I

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v0, v0, Licb;->c:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lica;

    invoke-direct {v0, v1, v2}, Lica;-><init>(II)V

    return-object v0
.end method

.method private c(Ljava/util/List;)Lhug;
    .locals 5

    const v4, 0x249f0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licb;

    invoke-static {v2, v0}, Liby;->a(Ljava/util/Set;Licb;)Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Liby;->e(Ljava/util/List;)Lhug;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhug;

    :goto_2
    iget v1, v0, Lhug;->f:I

    if-ge v1, v4, :cond_3

    new-instance v1, Lhui;

    invoke-direct {v1, v0}, Lhui;-><init>(Lhug;)V

    iput v4, v1, Lhui;->c:I

    invoke-virtual {v1}, Lhui;->a()Lhug;

    move-result-object v0

    :cond_3
    return-object v0

    :cond_4
    invoke-static {v1}, Liby;->d(Ljava/util/List;)Lhug;

    move-result-object v0

    goto :goto_2
.end method

.method private static d(Ljava/util/List;)Lhug;
    .locals 13

    const-wide v11, 0x3e7ad7f29abcaf48L    # 1.0E-7

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v2, v0

    move-object v3, v1

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhug;

    add-int/lit8 v1, v2, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhug;

    invoke-static {v0, v1}, Liby;->a(Lhug;Lhug;)D

    move-result-wide v4

    iget v6, v0, Lhug;->d:I

    iget v7, v0, Lhug;->e:I

    iget v0, v0, Lhug;->f:I

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v6, v7, v0, v4, v5}, Liby;->a(IIID)Lhue;

    move-result-object v0

    iget v6, v1, Lhug;->d:I

    iget v7, v1, Lhug;->e:I

    iget v1, v1, Lhug;->f:I

    div-int/lit16 v1, v1, 0x3e8

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v4, v8

    invoke-static {v6, v7, v1, v4, v5}, Liby;->a(IIID)Lhue;

    move-result-object v1

    invoke-static {v0, v1}, Liby;->a(Lhue;Lhue;)Lhue;

    move-result-object v0

    if-nez v3, :cond_0

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v3, v0

    goto :goto_0

    :cond_0
    invoke-static {v3, v0}, Liby;->a(Lhue;Lhue;)Lhue;

    move-result-object v0

    goto :goto_1

    :cond_1
    new-instance v1, Lhui;

    invoke-direct {v1}, Lhui;-><init>()V

    iget-object v0, v3, Lhue;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Liba;->b(D)I

    move-result v2

    iget-object v0, v3, Lhue;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Liba;->b(D)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lhui;->a(II)Lhui;

    move-result-object v9

    const v0, 0x7fffffff

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v8, v0

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhug;

    iget v0, v9, Lhui;->a:I

    int-to-double v0, v0

    mul-double/2addr v0, v11

    iget v2, v9, Lhui;->b:I

    int-to-double v2, v2

    mul-double/2addr v2, v11

    iget v4, v6, Lhug;->d:I

    int-to-double v4, v4

    mul-double/2addr v4, v11

    iget v6, v6, Lhug;->e:I

    int-to-double v6, v6

    mul-double/2addr v6, v11

    invoke-static/range {v0 .. v7}, Liba;->a(DDDD)D

    move-result-wide v0

    double-to-int v0, v0

    if-ge v0, v8, :cond_3

    :goto_3
    move v8, v0

    goto :goto_2

    :cond_2
    invoke-static {v8}, Liba;->a(I)I

    move-result v0

    iput v0, v9, Lhui;->c:I

    invoke-virtual {v9}, Lhui;->a()Lhug;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v8

    goto :goto_3
.end method

.method private e(Ljava/util/List;)Lhug;
    .locals 16

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Licb;

    iget-object v2, v1, Licb;->a:Lhug;

    const/4 v1, 0x1

    move v9, v1

    move-object v3, v2

    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v9, v1, :cond_3

    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Licb;

    iget-object v2, v1, Licb;->a:Lhug;

    invoke-static {v3, v2}, Liba;->a(Lhug;Lhug;)I

    move-result v10

    iget v1, v3, Lhug;->f:I

    div-int/lit16 v11, v1, 0x3e8

    iget v1, v2, Lhug;->f:I

    div-int/lit16 v1, v1, 0x3e8

    sub-int v4, v11, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v10, v4, :cond_1

    if-ge v11, v1, :cond_0

    move-object v2, v3

    :cond_0
    :goto_1
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    move-object v3, v2

    goto :goto_0

    :cond_1
    invoke-static {v3, v2}, Liby;->a(Lhug;Lhug;)D

    move-result-wide v4

    add-int v6, v10, v1

    invoke-static {v11, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget v7, v3, Lhug;->d:I

    iget v8, v2, Lhug;->d:I

    iget v12, v3, Lhug;->e:I

    iget v2, v2, Lhug;->e:I

    invoke-static {v7, v12, v6, v4, v5}, Liby;->a(IIID)Lhue;

    move-result-object v6

    const-wide v12, 0x400921fb54442d18L    # Math.PI

    add-double/2addr v4, v12

    add-int v7, v10, v11

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v8, v2, v7, v4, v5}, Liby;->a(IIID)Lhue;

    move-result-object v7

    invoke-static {v6, v7}, Liby;->a(Lhue;Lhue;)Lhue;

    move-result-object v12

    add-int v2, v11, v1

    if-le v10, v2, :cond_2

    iget-object v1, v6, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    iget-object v3, v6, Lhue;->b:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iget-object v5, v7, Lhue;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    iget-object v7, v7, Lhue;->b:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v8}, Liba;->b(DDDD)D

    move-result-wide v1

    move-wide v2, v1

    :goto_2
    new-instance v4, Lhug;

    iget-object v1, v12, Lhue;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-static {v5, v6}, Liba;->b(D)I

    move-result v5

    iget-object v1, v12, Lhue;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Liba;->b(D)I

    move-result v1

    double-to-int v2, v2

    invoke-static {v2}, Liba;->a(I)I

    move-result v2

    invoke-direct {v4, v5, v1, v2}, Lhug;-><init>(III)V

    move-object v2, v4

    goto :goto_1

    :cond_2
    mul-int v13, v10, v10

    mul-int v14, v11, v11

    mul-int v15, v1, v1

    invoke-static {v3}, Liba;->a(Lhug;)D

    move-result-wide v1

    invoke-static {v3}, Liba;->b(Lhug;)D

    move-result-wide v3

    iget-object v5, v12, Lhue;->a:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    iget-object v7, v12, Lhue;->b:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v7

    invoke-static/range {v1 .. v8}, Liba;->b(DDDD)D

    move-result-wide v1

    double-to-int v1, v1

    mul-int v2, v1, v1

    add-int v3, v13, v14

    sub-int/2addr v3, v15

    int-to-double v3, v3

    mul-int/lit8 v5, v11, 0x2

    mul-int/2addr v5, v10

    int-to-double v5, v5

    div-double/2addr v3, v5

    add-int/2addr v2, v14

    int-to-double v5, v2

    mul-int/lit8 v2, v11, 0x2

    mul-int/2addr v1, v2

    int-to-double v1, v1

    mul-double/2addr v1, v3

    sub-double v1, v5, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    move-wide v2, v1

    goto :goto_2

    :cond_3
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/util/Map;Ljava/util/Map;)Libx;
    .locals 9

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Liby;->a()Libx;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhuq;

    iget-object v2, v0, Lhuq;->b:Lhus;

    sget-object v5, Lhus;->c:Lhus;

    if-eq v2, v5, :cond_3

    iget-object v0, v0, Lhuq;->b:Lhus;

    sget-object v2, Lhus;->d:Lhus;

    if-ne v0, v2, :cond_2

    :cond_3
    move v2, v3

    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhuq;

    if-nez v2, :cond_5

    iget-object v7, v1, Lhuq;->b:Lhus;

    sget-object v8, Lhus;->c:Lhus;

    if-eq v7, v8, :cond_5

    iget-object v1, v1, Lhuq;->b:Lhus;

    sget-object v7, Lhus;->d:Lhus;

    if-ne v1, v7, :cond_4

    :cond_5
    new-instance v7, Licb;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhug;

    invoke-direct {v7, v1, v0}, Licb;-><init>(Ljava/lang/Long;Lhug;)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    new-instance v0, Libz;

    invoke-direct {v0, p0, v3}, Libz;-><init>(Liby;B)V

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_7
    invoke-static {v5}, Liby;->a(Ljava/util/List;)V

    invoke-static {v5}, Liby;->b(Ljava/util/List;)Lica;

    move-result-object v1

    iget v0, v1, Lica;->a:I

    iget v2, v1, Lica;->b:I

    if-eq v0, v2, :cond_9

    move v0, v4

    :goto_3
    if-eqz v0, :cond_8

    iget v1, v1, Lica;->b:I

    invoke-static {v5, v1}, Liby;->a(Ljava/util/List;I)V

    :cond_8
    if-nez v0, :cond_7

    invoke-direct {p0, v5}, Liby;->c(Ljava/util/List;)Lhug;

    move-result-object v1

    iget v0, v1, Lhug;->f:I

    const v2, 0x16e360

    if-le v0, v2, :cond_a

    sget-object v0, Liby;->a:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring computed location since accuracy too high: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, Lhug;->f:I

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-static {}, Liby;->a()Libx;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    invoke-static {v1, p1}, Liby;->a(Lhug;Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_b

    sget-object v0, Liby;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Not returning location for the following outliers: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_b
    new-instance v0, Libx;

    const/16 v3, 0x50

    invoke-direct {v0, v1, v3, v2}, Libx;-><init>(Lhug;ILjava/util/Set;)V

    goto/16 :goto_0

    :cond_c
    move v2, v4

    goto/16 :goto_1
.end method
