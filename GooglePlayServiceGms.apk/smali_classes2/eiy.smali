.class public final Leiy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/List;

.field private static final b:Ljava/util/Map;

.field private static final c:Landroid/util/SparseArray;


# instance fields
.field private final d:Landroid/content/SharedPreferences;

.field private final e:Lelt;

.field private final f:Ljava/util/Map;

.field private g:Z

.field private final h:Lems;

.field private i:Ljavax/crypto/Mac;

.field private j:Ljava/security/SecureRandom;

.field private final k:Ljava/lang/Object;

.field private final l:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "from_address"

    aput-object v1, v0, v3

    const-string v1, "to_addresses"

    aput-object v1, v0, v4

    const-string v1, "subject"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "body"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Leiy;->a:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v6}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Leiy;->b:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Leiy;->b:Ljava/util/Map;

    const-string v1, "plain"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Leiy;->b:Ljava/util/Map;

    const-string v1, "html"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Leiy;->b:Ljava/util/Map;

    const-string v1, "rfc822"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/util/SparseArray;

    sget-object v1, Leiy;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Leiy;->c:Landroid/util/SparseArray;

    sget-object v0, Leiy;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v3, Leiy;->c:Landroid/util/SparseArray;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Lelt;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leiy;->f:Ljava/util/Map;

    iput-boolean v1, p0, Leiy;->g:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leiy;->k:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Leiy;->l:Ljava/lang/Object;

    invoke-virtual {p2, p3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    iput-object p1, p0, Leiy;->e:Lelt;

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v1, "created"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    invoke-direct {p0}, Leiy;->u()Ljavax/crypto/Mac;

    move-result-object v0

    iput-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "corpuskey:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lehj;

    invoke-direct {v3}, Lehj;-><init>()V

    invoke-static {v0, v3}, Lell;->a(Ljava/lang/String;Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehj;

    invoke-direct {p0, v1, v0}, Leiy;->a(Ljava/lang/String;Lehj;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-corpus-counters-scratch.tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-class v1, Lehi;

    new-instance v2, Lems;

    invoke-direct {v2, v0, v1}, Lems;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    iput-object v2, p0, Leiy;->h:Lems;

    iget-object v0, p0, Leiy;->h:Lems;

    invoke-virtual {v0}, Lems;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Leiy;->a(Ljava/util/Collection;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 7

    iget-object v3, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update(B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update(B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/crypto/Mac;->update([B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v4

    const/4 v0, 0x7

    aget-byte v0, v4, v0

    and-int/lit8 v0, v0, 0x7f

    int-to-long v1, v0

    const/4 v0, 0x6

    :goto_0
    if-ltz v0, :cond_0

    const/16 v5, 0x8

    shl-long/2addr v1, v5

    aget-byte v5, v4, v0

    and-int/lit16 v5, v5, 0xff

    int-to-long v5, v5

    add-long/2addr v1, v5

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public static a(Lels;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lehh;
    .locals 13

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v5, Lehh;

    invoke-direct {v5}, Lehh;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    iput-object v0, v5, Lehh;->b:Ljava/lang/String;

    iget-object v0, p0, Lels;->a:Ljava/lang/String;

    iput-object v0, v5, Lehh;->d:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->c:Ljava/lang/String;

    iput-object v0, v5, Lehh;->c:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lehh;->f:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->g:Z

    iput-boolean v0, v5, Lehh;->h:Z

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;->c:[Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {v1, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(I[Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, v5, Lehh;->n:Z

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    new-array v0, v0, [Leii;

    iput-object v0, v5, Lehh;->k:[Leii;

    move v3, v2

    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    if-ge v3, v0, :cond_8

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    aget-object v6, v0, v3

    sget-object v0, Leiy;->b:Ljava/util/Map;

    iget-object v4, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v0, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    :goto_2
    new-instance v8, Leii;

    invoke-direct {v8}, Leii;-><init>()V

    iget-object v4, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    iput-object v4, v8, Leii;->a:Ljava/lang/String;

    iget-boolean v4, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    if-nez v4, :cond_6

    move v4, v1

    :goto_3
    iput-boolean v4, v8, Leii;->b:Z

    iput v7, v8, Leii;->c:I

    iget v4, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->e:I

    iput v4, v8, Leii;->d:I

    iget-boolean v4, v6, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->f:Z

    iput-boolean v4, v8, Leii;->e:Z

    iput-object v0, v8, Leii;->f:Ljava/lang/String;

    invoke-virtual {v6, v1}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    :goto_4
    iget-object v4, v5, Lehh;->d:Ljava/lang/String;

    const-string v7, "com.google.android.googlequicksearchbox"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v5, Lehh;->b:Ljava/lang/String;

    const-string v7, "contacts"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v5, Lehh;->c:Ljava/lang/String;

    const-string v7, "2"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v8, Leii;->a:Ljava/lang/String;

    const-string v7, "givennames"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    :cond_0
    if-eqz v0, :cond_1

    new-array v0, v1, [I

    aput v2, v0, v2

    iput-object v0, v8, Leii;->g:[I

    :cond_1
    invoke-static {v6}, Lait;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v9

    cmpg-double v0, v9, v11

    if-gez v0, :cond_2

    invoke-static {v9, v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(D)I

    move-result v0

    iput v0, v8, Leii;->h:I

    :cond_2
    invoke-static {v6}, Lait;->b(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v6

    cmpg-double v0, v6, v11

    if-gez v0, :cond_3

    invoke-static {v6, v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(D)I

    move-result v0

    iput v0, v8, Leii;->i:I

    :cond_3
    iget-object v0, v5, Lehh;->k:[Leii;

    aput-object v8, v0, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    const-string v0, ""

    goto :goto_2

    :cond_6
    move v4, v2

    goto :goto_3

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    return-object v5

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;ZZZ)Lehk;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    new-instance v0, Lehk;

    invoke-direct {v0}, Lehk;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lehj;->b:Lehk;

    if-nez v2, :cond_2

    if-eqz p3, :cond_3

    new-instance v1, Lehk;

    invoke-direct {v1}, Lehk;-><init>()V

    iput-object v1, v0, Lehj;->b:Lehk;

    :cond_2
    if-eqz p4, :cond_5

    iget-object v0, v0, Lehj;->b:Lehk;

    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehk;

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    new-instance v0, Lehk;

    invoke-direct {v0}, Lehk;-><init>()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, v0, Lehj;->b:Lehk;

    goto :goto_0
.end method

.method static synthetic a(Leiy;)Lelt;
    .locals 1

    iget-object v0, p0, Leiy;->e:Lelt;

    return-object v0
.end method

.method private static a(Lehj;)Ljava/lang/String;
    .locals 14

    const/16 v13, 0x20

    const/4 v12, 0x1

    const/16 v11, 0xa

    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lehj;->a:Lehh;

    iget-object v6, v0, Lehh;->k:[Leii;

    array-length v7, v6

    move v3, v1

    move v0, v1

    :goto_0
    if-ge v3, v7, :cond_4

    aget-object v2, v6, v3

    const-string v4, "    "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v8, v2, Leii;->a:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, v2, Leii;->b:Z

    if-nez v0, :cond_0

    const-string v0, "(noindex)"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget v0, v2, Leii;->c:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "(unknown)"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    :pswitch_0
    iget v0, v2, Leii;->d:I

    if-le v0, v12, :cond_1

    const-string v0, " w:"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v8, v2, Leii;->d:I

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v0, v2, Leii;->g:[I

    array-length v0, v0

    if-lez v0, :cond_3

    const-string v0, " (variants"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v2, Leii;->g:[I

    array-length v9, v8

    move v2, v1

    :goto_2
    if-ge v2, v9, :cond_2

    aget v0, v8, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const-string v0, "unknown"

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    packed-switch v10, :pswitch_data_1

    :goto_3
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :pswitch_1
    const-string v0, "(html)"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_2
    const-string v0, "(rfc822)"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_3
    const-string v0, "nick"

    goto :goto_3

    :cond_2
    const/16 v0, 0x29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->l:[Leim;

    array-length v3, v0

    const-string v0, "Global Search Sections: "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-lez v3, :cond_a

    move v2, v1

    :goto_4
    if-ge v2, v3, :cond_a

    iget-object v0, p0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->l:[Leim;

    aget-object v4, v0, v2

    const-string v0, "    "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, v4, Leim;->a:I

    invoke-static {v6}, Laia;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ": 0x"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v6, v4, Leim;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " parts: \""

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_5
    iget-object v6, v4, Leim;->b:Lein;

    iget-object v6, v6, Lein;->a:[Leio;

    array-length v6, v6

    if-ge v0, v6, :cond_9

    iget-object v6, v4, Leim;->b:Lein;

    iget-object v6, v6, Lein;->a:[Leio;

    aget-object v6, v6, v0

    iget-object v7, v6, Leio;->c:Leip;

    if-eqz v7, :cond_5

    const-string v6, "%$bestMatch"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v7, v6, Leio;->b:Leiq;

    if-eqz v7, :cond_6

    const-string v7, "$"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lehj;->a:Lehh;

    iget-object v8, v8, Lehh;->k:[Leii;

    iget-object v6, v6, Leio;->b:Leiq;

    iget v6, v6, Leiq;->a:I

    aget-object v6, v8, v6

    iget-object v6, v6, Leii;->a:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_6
    iget-object v7, v6, Leio;->a:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v6, v6, Leio;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_7
    iget-object v6, v6, Leio;->d:Leir;

    if-eqz v6, :cond_8

    const-string v6, "%$uri"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_8
    const-string v6, "[?unknown template part]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_9
    const-string v0, "\"\n"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lehj;->b:Lehk;

    iget-object v3, v0, Lehk;->c:[Lehl;

    array-length v4, v3

    move v0, v1

    :goto_7
    if-ge v0, v4, :cond_b

    aget-object v6, v3, v0

    const-string v7, "    "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v6, Lehl;->a:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ": "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, v6, Lehl;->b:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lehj;->b:Lehk;

    iget v0, v0, Lehk;->d:I

    packed-switch v0, :pswitch_data_2

    const-string v0, "UNKNOWN"

    :goto_8
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "  id:%d\n  name:%s\n  version:\"%s\"\n  package:%s\n  uri:%s\n  trimmable:%s\n  state:%s\n  last indexed/committed:%d/%d\n  source:%s\n  usageReportId:%d\n  sections:\n%s  counters:\n%s"

    const/16 v6, 0xd

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lehj;->a:Lehh;

    iget v7, v7, Lehh;->a:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    iget-object v1, p0, Lehj;->a:Lehh;

    iget-object v1, v1, Lehh;->b:Ljava/lang/String;

    aput-object v1, v6, v12

    const/4 v1, 0x2

    iget-object v7, p0, Lehj;->a:Lehh;

    iget-object v7, v7, Lehh;->c:Ljava/lang/String;

    aput-object v7, v6, v1

    const/4 v1, 0x3

    iget-object v7, p0, Lehj;->a:Lehh;

    iget-object v7, v7, Lehh;->d:Ljava/lang/String;

    aput-object v7, v6, v1

    const/4 v1, 0x4

    iget-object v7, p0, Lehj;->a:Lehh;

    iget-object v7, v7, Lehh;->f:Ljava/lang/String;

    aput-object v7, v6, v1

    const/4 v1, 0x5

    iget-object v7, p0, Lehj;->a:Lehh;

    iget-boolean v7, v7, Lehh;->h:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v1, 0x6

    aput-object v0, v6, v1

    const/4 v0, 0x7

    iget-object v1, p0, Lehj;->b:Lehk;

    iget-wide v7, v1, Lehk;->a:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0x8

    iget-object v1, p0, Lehj;->b:Lehk;

    iget-wide v7, v1, Lehk;->b:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0x9

    iget-object v1, p0, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->m:I

    invoke-static {v1}, Lema;->b(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    iget-object v0, p0, Lehj;->a:Lehh;

    iget-wide v0, v0, Lehh;->j:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v11

    const/16 v0, 0xb

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/16 v0, 0xc

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v3, v4, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_4
    const-string v0, "active"

    goto :goto_8

    :pswitch_5
    const-string v0, "LIMBO"

    goto/16 :goto_8

    :pswitch_6
    const-string v0, "DELETED"

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    iget-object v3, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v3, v0}, Ljavax/crypto/Mac;->update([B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    const/16 v3, 0x2d

    invoke-virtual {v0, v3}, Ljavax/crypto/Mac;->update(B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update([B)V

    iget-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v0

    invoke-static {v0}, Lell;->a([B)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static final a(Lehh;)Ljava/util/Map;
    .locals 6

    iget-object v0, p0, Lehh;->k:[Leii;

    array-length v1, v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lehh;->k:[Leii;

    aget-object v3, v3, v0

    iget-object v4, v3, Leii;->a:Ljava/lang/String;

    new-instance v5, Lejh;

    invoke-direct {v5, v0, v3}, Lejh;-><init>(ILeii;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private final a(Lejd;)Ljava/util/Set;
    .locals 5

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    invoke-interface {p1, v1}, Lejd;->a(Lehj;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method private a(Landroid/content/SharedPreferences$Editor;Lehr;)V
    .locals 6

    const/4 v5, 0x0

    if-nez p2, :cond_0

    const-string v0, "flushstatus"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Leiy;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lehk;

    invoke-direct {v2}, Lehk;-><init>()V

    invoke-direct {p0, v0, v2, p1, v5}, Leiy;->a(Ljava/lang/String;Lehk;Landroid/content/SharedPreferences$Editor;Z)V

    goto :goto_0

    :cond_0
    const-string v0, "flushstatus"

    invoke-static {p2}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->a:Lehh;

    invoke-static {v1}, Leiy;->d(Lehh;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget-wide v3, v1, Lehk;->a:J

    iput-wide v3, v1, Lehk;->b:J

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, v5}, Leiy;->a(Ljava/lang/String;Lehk;Landroid/content/SharedPreferences$Editor;Z)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Leiy;->v()V

    return-void
.end method

.method private a(Ljava/lang/String;Lehj;)V
    .locals 2

    iget-object v0, p2, Lehj;->a:Lehh;

    if-nez v0, :cond_0

    new-instance v0, Lehh;

    invoke-direct {v0}, Lehh;-><init>()V

    iput-object v0, p2, Lehj;->a:Lehh;

    :cond_0
    iget-object v0, p2, Lehj;->b:Lehk;

    if-nez v0, :cond_1

    new-instance v0, Lehk;

    invoke-direct {v0}, Lehk;-><init>()V

    iput-object v0, p2, Lehj;->b:Lehk;

    :cond_1
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Leiy;->g:Z

    iget-object v0, p2, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    iget-object v1, p0, Leiy;->e:Lelt;

    invoke-virtual {v1, v0}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v0

    iget-object v1, v0, Lels;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lels;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Lehk;Landroid/content/SharedPreferences$Editor;Z)V
    .locals 3

    const-string v0, "Setting status for %s"

    invoke-static {v0, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_0

    const-string v0, "Cannot set status for non-existent key: %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    iput-object p2, v0, Lehj;->b:Lehk;

    iget-boolean v1, p0, Leiy;->g:Z

    or-int/2addr v1, p4

    iput-boolean v1, p0, Leiy;->g:Z

    iget-object v1, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "corpuskey:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Leiy;->g:Z

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v2, Landroid/util/SparseArray;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/util/SparseArray;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehi;

    iget v3, v0, Lehi;->a:I

    iget-object v0, v0, Lehi;->b:[Lehl;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v1, v0, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->a:I

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lehl;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lehj;->b:Lehk;

    iput-object v1, v0, Lehk;->c:[Lehl;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_0

    const-string v0, "Can\'t find corpus with key %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v3, v0, Lehj;->a:Lehh;

    iget-object v3, v3, Lehh;->b:Ljava/lang/String;

    iget-object v4, v0, Lehj;->b:Lehk;

    iget v4, v4, Lehk;->d:I

    if-eq v4, p2, :cond_1

    const-string v4, "Can\'t remove corpus %s, not in expected state %s, actual state=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    const/4 v1, 0x2

    iget-object v0, v0, Lehj;->b:Lehk;

    iget v0, v0, Lehk;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lehe;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    goto :goto_0

    :cond_1
    const-string v4, "Removing inactive corpus %s"

    invoke-static {v4, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iput-boolean v1, p0, Leiy;->g:Z

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    iget-object v3, p0, Leiy;->e:Lelt;

    invoke-virtual {v3, v0}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v0

    iget-object v3, v0, Lels;->j:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, v0, Lels;->c:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lels;->b()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private b(I)Ljava/util/Map;
    .locals 4

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-ne v1, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static b(Lehh;)[Lcom/google/android/gms/appdatasearch/Feature;
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lehh;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {}, Lahy;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v1

    aput-object v1, v0, v2

    :goto_0
    return-object v0

    :cond_0
    new-array v0, v2, [Lcom/google/android/gms/appdatasearch/Feature;

    goto :goto_0
.end method

.method public static c(Lehh;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 10

    const/4 v1, 0x0

    iget-object v0, p0, Lehh;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->a(Ljava/lang/String;)Laih;

    move-result-object v3

    iget-object v0, p0, Lehh;->c:Ljava/lang/String;

    iput-object v0, v3, Laih;->a:Ljava/lang/String;

    iget-object v0, p0, Lehh;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v3, Laih;->b:Landroid/net/Uri;

    iget-object v2, p0, Lehh;->l:[Leim;

    invoke-static {p0}, Leiy;->b(Lehh;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v4

    new-instance v5, Lahw;

    invoke-direct {v5}, Lahw;-><init>()V

    array-length v6, v2

    move v0, v1

    :goto_1
    if-ge v0, v6, :cond_1

    aget-object v7, v2, v0

    iget v8, v7, Leim;->a:I

    invoke-static {v8}, Laia;->a(I)Ljava/lang/String;

    move-result-object v8

    iget v7, v7, Leim;->c:I

    invoke-static {v8}, Laia;->a(Ljava/lang/String;)I

    move-result v8

    iget-object v9, v5, Lahw;->a:[I

    aput v7, v9, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_2

    move v0, v1

    :goto_2
    array-length v2, v4

    if-ge v0, v2, :cond_2

    iget-object v2, v5, Lahw;->b:Ljava/util/ArrayList;

    aget-object v6, v4, v0

    invoke-static {v2, v6}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    new-instance v2, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v4, v5, Lahw;->a:[I

    iget-object v0, v5, Lahw;->b:Ljava/util/ArrayList;

    iget-object v5, v5, Lahw;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v2, v4, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    iput-object v2, v3, Laih;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v0, p0, Lehh;->h:Z

    iput-boolean v0, v3, Laih;->e:Z

    move v2, v1

    :goto_3
    iget-object v0, p0, Lehh;->k:[Leii;

    array-length v0, v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lehh;->k:[Leii;

    aget-object v4, v0, v2

    sget-object v0, Leiy;->c:Landroid/util/SparseArray;

    iget v5, v4, Leii;->c:I

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, "plain"

    :cond_3
    iget-object v5, v4, Leii;->a:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(Ljava/lang/String;)Laij;

    move-result-object v5

    iput-object v0, v5, Laij;->a:Ljava/lang/String;

    iget-boolean v0, v4, Leii;->b:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, v5, Laij;->b:Z

    iget v0, v4, Leii;->d:I

    iput v0, v5, Laij;->c:I

    iget-boolean v0, v4, Leii;->e:Z

    iput-boolean v0, v5, Laij;->d:Z

    iget-object v0, v4, Leii;->f:Ljava/lang/String;

    iput-object v0, v5, Laij;->e:Ljava/lang/String;

    iget-object v0, v4, Leii;->g:[I

    invoke-static {v0}, Lell;->a([I)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_4

    invoke-static {}, Lait;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-virtual {v5, v0}, Laij;->a(Lcom/google/android/gms/appdatasearch/Feature;)Laij;

    :cond_4
    iget-object v0, v3, Laih;->c:Ljava/util/List;

    invoke-virtual {v5}, Laij;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    invoke-virtual {v3}, Laih;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lehh;)Z
    .locals 1

    iget-object v0, p0, Lehh;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Ljava/security/SecureRandom;
    .locals 1

    iget-object v0, p0, Leiy;->j:Ljava/security/SecureRandom;

    if-nez v0, :cond_0

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Leiy;->j:Ljava/security/SecureRandom;

    :cond_0
    iget-object v0, p0, Leiy;->j:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private u()Ljavax/crypto/Mac;
    .locals 4

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v1, "hmackey"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v1, "hmackey"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    :try_start_0
    const-string v1, "HmacSHA1"

    invoke-static {v1}, Lell;->c(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot find algo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "CannotHappenException"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-direct {p0}, Leiy;->t()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hmackey"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA1"

    invoke-direct {v2, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Bad key type"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private v()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Leiy;->h:Lems;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leiy;->h:Lems;

    iget-object v0, v0, Lems;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Leiy;->h:Lems;

    iget-object v0, v2, Lems;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Deleting scratch file %s"

    iget-object v3, v2, Lems;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, v2, Lems;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v3, "Failed to delete scratch file %s"

    iget-object v2, v2, Lems;->a:Ljava/lang/String;

    invoke-static {v3, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    const-string v0, "Failed to delete stale counter scratch file."

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_1
    iput-boolean v1, p0, Leiy;->g:Z

    return-void

    :cond_2
    const-string v0, "Scratch file %s already deleted"

    iget-object v2, v2, Lems;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    goto :goto_0
.end method

.method private w()Z
    .locals 9

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v5, v0, Lehj;->a:Lehh;

    iget-wide v5, v5, Lehh;->j:J

    iget-object v5, v0, Lehj;->a:Lehh;

    iget-object v6, v0, Lehj;->a:Lehh;

    iget-object v6, v6, Lehh;->d:Ljava/lang/String;

    iget-object v7, v0, Lehj;->a:Lehh;

    iget-object v7, v7, Lehh;->b:Ljava/lang/String;

    iget-object v8, v0, Lehj;->a:Lehh;

    iget-object v8, v8, Lehh;->c:Ljava/lang/String;

    invoke-direct {p0, v6, v7, v8}, Leiy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v5, Lehh;->j:J

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "corpuskey:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Failed to write backfilled corpus configs to preferences."

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    const/4 v0, 0x0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lels;)I
    .locals 5

    const/4 v1, -0x1

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p2, p1}, Lels;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v2

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-eqz v0, :cond_1

    iget-object v1, v0, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->a:I

    iget-object v3, v0, Lehj;->b:Lehk;

    iget v3, v3, Lehk;->d:I

    if-nez v3, :cond_1

    iget-object v3, v0, Lehj;->b:Lehk;

    const/4 v4, 0x1

    iput v4, v3, Lehk;->d:I

    iget-object v3, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    iget-object v0, v0, Lehj;->b:Lehk;

    const/4 v4, 0x0

    invoke-direct {p0, p1, v0, v3, v4}, Leiy;->a(Ljava/lang/String;Lehk;Landroid/content/SharedPreferences$Editor;Z)V

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    move v0, v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/lang/String;
    .locals 2

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Leiy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lels;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p1, Lels;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Leiy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v0, Lehh;->i:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lels;)Ljava/util/List;
    .locals 5

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Lels;->a()Ljava/util/Set;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Lehy;)Ljava/util/Set;
    .locals 11

    const/4 v10, -0x1

    iget-object v3, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    new-instance v4, Landroid/util/SparseIntArray;

    invoke-direct {v4}, Landroid/util/SparseIntArray;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lehy;->a:[Lehz;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p1, Lehy;->a:[Lehz;

    aget-object v1, v1, v0

    iget v1, v1, Lehz;->a:I

    invoke-virtual {v4, v1, v0}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v7, v1, Lehj;->b:Lehk;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->a:I

    const/4 v2, -0x1

    invoke-virtual {v4, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v8

    iget-wide v1, v7, Lehk;->b:J

    if-eq v8, v10, :cond_1

    iget-object v1, p1, Lehy;->a:[Lehz;

    aget-object v1, v1, v8

    iget-wide v1, v1, Lehz;->b:J

    :cond_1
    iget-wide v8, v7, Lehk;->a:J

    cmp-long v8, v1, v8

    if-gez v8, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iput-wide v1, v7, Lehk;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v5
.end method

.method public final a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;
    .locals 6

    iget-boolean v0, p1, Lelv;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lejd;->b:Lejd;

    invoke-direct {p0, v0}, Leiy;->a(Lejd;)Ljava/util/Set;

    move-result-object v0

    move-object v1, v0

    :cond_0
    :goto_0
    if-eqz p2, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, p2, v0

    iget-object v5, p1, Lely;->e:Ljava/lang/String;

    invoke-direct {p0, v5, v4}, Leiy;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Leiy;->e:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Lelv;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {p0, v0}, Leiy;->a(Lels;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lely;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    new-instance v0, Lejb;

    invoke-direct {v0, p0}, Lejb;-><init>(Leiy;)V

    invoke-direct {p0, v0}, Leiy;->a(Lejd;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_3
    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    :cond_4
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Leiy;->v()V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-direct {p0}, Leiy;->u()Ljavax/crypto/Mac;

    move-result-object v0

    iput-object v0, p0, Leiy;->i:Ljavax/crypto/Mac;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lehg;Lehr;)V
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "compactstatus"

    invoke-static {p1}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-direct {p0, v0, p2}, Leiy;->a(Landroid/content/SharedPreferences$Editor;Lehr;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 7

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "No corprora\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lejc;

    invoke-direct {v2, p0}, Lejc;-><init>(Leiy;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v3, "%s:\n%s\n"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    invoke-static {v0}, Leiy;->a(Lehj;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;J[Lehl;I)V
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v2, v3}, Leiy;->a(Ljava/lang/String;ZZZ)Lehk;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Cannot set status for non-existent key: %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-ltz v2, :cond_1

    iput-wide p2, v0, Lehk;->a:J

    :cond_1
    if-eqz p4, :cond_2

    iget-object v2, v0, Lehk;->c:[Lehl;

    invoke-static {v2, p4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Leiy;->g:Z

    iput-object p4, v0, Lehk;->c:[Lehl;

    :cond_2
    iput p5, v0, Lehk;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lehk;)V
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {p2}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehk;

    const/4 v3, 0x1

    invoke-direct {p0, p1, v0, v2, v3}, Leiy;->a(Ljava/lang/String;Lehk;Landroid/content/SharedPreferences$Editor;Z)V

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lema;)V
    .locals 1

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Leiy;->a(Ljava/lang/String;Lema;Lejg;)V
    :try_end_0
    .catch Leme; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lema;Lejg;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    const-string v0, "Setting config for %s"

    invoke-static {v0, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_4

    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget v0, v0, Lehh;->a:I

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/util/BitSet;->length()I

    move-result v0

    invoke-virtual {v2}, Ljava/util/BitSet;->cardinality()I

    move-result v4

    if-ne v0, v4, :cond_2

    invoke-virtual {v2}, Ljava/util/BitSet;->length()I

    move-result v0

    :cond_1
    const v1, 0xfffe

    if-le v0, v1, :cond_3

    new-instance v0, Lemb;

    const-string v1, "Too many corpora"

    invoke-direct {v0, v1}, Lemb;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Ljava/util/BitSet;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    new-instance v2, Lehj;

    invoke-direct {v2}, Lehj;-><init>()V

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v4, v5}, Lema;->a(ILjava/lang/Object;J)Lema;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    :goto_2
    invoke-virtual {v2, p2}, Lema;->a(Lema;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lemb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CorpusConfig: cannot "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lema;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " when previously "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Lema;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lemb;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v1, v0, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->a:I

    iget-object v2, v0, Lehj;->a:Lehh;

    iget v2, v2, Lehh;->m:I

    iget-object v4, v0, Lehj;->a:Lehh;

    iget-wide v4, v4, Lehh;->o:J

    invoke-static {v2, v0, v4, v5}, Lema;->a(ILjava/lang/Object;J)Lema;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    :cond_5
    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {p2}, Lema;->b()I

    move-result v4

    iput v4, v0, Lehh;->m:I

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {p2}, Lema;->c()J

    move-result-wide v4

    iput-wide v4, v0, Lehh;->o:J

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iput v1, v0, Lehh;->a:I

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v0, v0, Lehh;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    new-instance v0, Lemb;

    const-string v1, "Need name and package name"

    invoke-direct {v0, v1}, Lemb;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    invoke-direct {p0}, Leiy;->t()Ljava/security/SecureRandom;

    move-result-object v1

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v4

    iput-wide v4, v0, Lehh;->i:J

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v4, v1, Lehh;->d:Ljava/lang/String;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v5, v1, Lehh;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v1, v1, Lehh;->c:Ljava/lang/String;

    invoke-direct {p0, v4, v5, v1}, Leiy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v0, Lehh;->j:J

    invoke-virtual {v2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lizs;

    invoke-static {v1}, Lell;->b(Lizs;)Lizs;

    move-result-object v1

    check-cast v1, Lehh;

    iput-object v1, v0, Lehj;->a:Lehh;

    if-eqz p3, :cond_8

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-interface {p3, v0}, Lejg;->a(Lehh;)V

    :cond_8
    invoke-virtual {v2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    invoke-direct {p0, p1, v0}, Leiy;->a(Ljava/lang/String;Lehj;)V

    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "corpuskey:"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-static {v0}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(I)Z
    .locals 3

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const/16 v2, 0x26

    if-ge p1, v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_1
    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return v0

    :pswitch_0
    invoke-direct {p0}, Leiy;->w()Z

    move-result v1

    goto :goto_1

    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lehr;)Z
    .locals 2

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Leiy;->a(Landroid/content/SharedPreferences$Editor;Lehr;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lelv;Ljava/lang/String;)Z
    .locals 4

    iget-object v0, p0, Leiy;->e:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Lelv;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {p0, v0}, Leiy;->a(Lels;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v0, p2}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lema;Lema;)Z
    .locals 11

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {p3}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v2

    check-cast v2, Lehh;

    invoke-virtual {p2}, Lema;->b()I

    move-result v3

    invoke-virtual {p3}, Lema;->b()I

    move-result v6

    if-eq v3, v6, :cond_c

    invoke-virtual {p3}, Lema;->b()I

    move-result v3

    iput v3, v2, Lehh;->m:I

    move v3, v4

    :goto_0
    iget-object v6, v0, Lehh;->b:Ljava/lang/String;

    iget-object v7, v1, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_1
    return v5

    :cond_1
    iget-object v6, v0, Lehh;->c:Ljava/lang/String;

    iget-object v7, v1, Lehh;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, v0, Lehh;->d:Ljava/lang/String;

    iget-object v7, v1, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v0, Lehh;->e:I

    iget v7, v1, Lehh;->e:I

    if-ne v6, v7, :cond_0

    invoke-static {v0}, Leiy;->d(Lehh;)Z

    move-result v6

    invoke-static {v1}, Leiy;->d(Lehh;)Z

    move-result v7

    if-ne v6, v7, :cond_0

    iget-object v6, v0, Lehh;->f:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v0, Lehh;->f:Ljava/lang/String;

    iget-object v7, v1, Lehh;->f:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v3, v1, Lehh;->f:Ljava/lang/String;

    iput-object v3, v2, Lehh;->f:Ljava/lang/String;

    move v3, v4

    :cond_2
    iget v6, v0, Lehh;->g:I

    iget v7, v1, Lehh;->g:I

    if-eq v6, v7, :cond_3

    iget v3, v1, Lehh;->g:I

    iput v3, v2, Lehh;->g:I

    move v3, v4

    :cond_3
    iget-boolean v6, v0, Lehh;->h:Z

    iget-boolean v7, v1, Lehh;->h:Z

    if-eq v6, v7, :cond_4

    iget-boolean v3, v1, Lehh;->h:Z

    iput-boolean v3, v2, Lehh;->h:Z

    move v3, v4

    :cond_4
    iget-object v6, v0, Lehh;->k:[Leii;

    array-length v6, v6

    iget-object v7, v1, Lehh;->k:[Leii;

    array-length v7, v7

    if-ne v6, v7, :cond_0

    move v6, v3

    move v3, v5

    :goto_2
    iget-object v7, v0, Lehh;->k:[Leii;

    array-length v7, v7

    if-ge v3, v7, :cond_7

    iget-object v7, v0, Lehh;->k:[Leii;

    aget-object v7, v7, v3

    iget-object v8, v1, Lehh;->k:[Leii;

    aget-object v8, v8, v3

    iget-object v9, v7, Leii;->a:Ljava/lang/String;

    iget-object v10, v8, Leii;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-boolean v9, v7, Leii;->b:Z

    iget-boolean v10, v8, Leii;->b:Z

    if-ne v9, v10, :cond_0

    iget v9, v7, Leii;->c:I

    iget v10, v8, Leii;->c:I

    if-ne v9, v10, :cond_0

    iget-boolean v9, v7, Leii;->e:Z

    iget-boolean v10, v8, Leii;->e:Z

    if-ne v9, v10, :cond_0

    iget-object v9, v7, Leii;->g:[I

    iget-object v10, v8, Leii;->g:[I

    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v9

    if-eqz v9, :cond_0

    iget v9, v7, Leii;->h:I

    iget v10, v8, Leii;->h:I

    if-ne v9, v10, :cond_0

    iget v9, v7, Leii;->i:I

    iget v10, v8, Leii;->i:I

    if-ne v9, v10, :cond_0

    iget-object v9, v7, Leii;->f:Ljava/lang/String;

    iget-object v10, v8, Leii;->f:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v6, v2, Lehh;->k:[Leii;

    aget-object v6, v6, v3

    iget-object v9, v8, Leii;->f:Ljava/lang/String;

    iput-object v9, v6, Leii;->f:Ljava/lang/String;

    move v6, v4

    :cond_5
    iget v7, v7, Leii;->d:I

    iget v9, v8, Leii;->d:I

    if-eq v7, v9, :cond_6

    iget-object v6, v2, Lehh;->k:[Leii;

    aget-object v6, v6, v3

    iget v7, v8, Leii;->d:I

    iput v7, v6, Leii;->d:I

    move v6, v4

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    iget-object v3, v0, Lehh;->l:[Leim;

    iget-object v5, v1, Lehh;->l:[Leim;

    invoke-static {v3, v5}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, v1, Lehh;->l:[Leim;

    iput-object v3, v2, Lehh;->l:[Leim;

    move v6, v4

    :cond_8
    iget-boolean v3, v0, Lehh;->n:Z

    iget-boolean v5, v1, Lehh;->n:Z

    if-eq v3, v5, :cond_9

    iget-boolean v3, v1, Lehh;->n:Z

    iput-boolean v3, v2, Lehh;->n:Z

    move v6, v4

    :cond_9
    iget-wide v7, v0, Lehh;->o:J

    iget-wide v9, v1, Lehh;->o:J

    cmp-long v0, v7, v9

    if-eqz v0, :cond_a

    iget-wide v0, v1, Lehh;->o:J

    iput-wide v0, v2, Lehh;->o:J

    move v6, v4

    :cond_a
    if-eqz v6, :cond_b

    invoke-virtual {p3, v2}, Lema;->a(Ljava/lang/Object;)Lema;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Leiy;->a(Ljava/lang/String;Lema;)V

    :cond_b
    move v5, v4

    goto/16 :goto_1

    :cond_c
    move v3, v5

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)Lehj;
    .locals 2

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lels;)Ljava/util/Map;
    .locals 5

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p1}, Lels;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-eqz v0, :cond_0

    iget-object v4, v0, Lehj;->a:Lehh;

    iget-object v4, v4, Lehh;->b:Ljava/lang/String;

    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-static {v0}, Leiy;->b(Lehh;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final b()V
    .locals 3

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lehg;

    invoke-direct {v0}, Lehg;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Leiy;->a(Lehg;Lehr;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;Lels;)Z
    .locals 6

    const/4 v4, 0x1

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p2, p1}, Lels;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Key "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " doesn\'t exist for pkg "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p2, Lels;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-eqz v0, :cond_1

    iget-object v3, v0, Lehj;->b:Lehk;

    iget v3, v3, Lehk;->d:I

    if-ne v3, v4, :cond_1

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Leiy;->a(Ljava/lang/String;I)Z

    iget-object v1, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "corpuskey:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v3, v0, Lehj;->b:Lehk;

    const/4 v4, 0x2

    iput v4, v3, Lehk;->d:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lehj;->a:Lehh;

    iget v4, v4, Lehh;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v0}, Leiy;->a(Ljava/lang/String;Lehj;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "corpuskey:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lell;->a(Lizs;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lema;
    .locals 4

    invoke-virtual {p0, p1}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lehj;->a:Lehh;

    iget v1, v1, Lehh;->m:I

    iget-object v2, v0, Lehj;->a:Lehh;

    iget-wide v2, v2, Lehh;->o:J

    invoke-static {v1, v0, v2, v3}, Lema;->a(ILjava/lang/Object;J)Lema;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v4, v0, Lehj;->b:Lehk;

    iget v4, v4, Lehk;->d:I

    if-nez v4, :cond_0

    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final d(Ljava/lang/String;)Lehh;
    .locals 2

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehh;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Ljava/util/Set;
    .locals 1

    sget-object v0, Lejd;->b:Lejd;

    invoke-direct {p0, v0}, Leiy;->a(Lejd;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lema;
    .locals 2

    invoke-virtual {p0, p1}, Leiy;->c(Ljava/lang/String;)Lema;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-virtual {v1, v0}, Lema;->a(Ljava/lang/Object;)Lema;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    new-instance v0, Leiz;

    invoke-direct {v0, p0}, Leiz;-><init>(Leiy;)V

    invoke-direct {p0, v0}, Leiy;->a(Lejd;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lehk;
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0, v2, v3}, Leiy;->a(Ljava/lang/String;ZZZ)Lehk;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Ljava/util/Set;
    .locals 1

    new-instance v0, Leja;

    invoke-direct {v0, p0}, Leja;-><init>(Leiy;)V

    invoke-direct {p0, v0}, Leiy;->a(Lejd;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()[J
    .locals 8

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    const/4 v1, 0x0

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget v0, v0, Lehh;->a:I

    if-le v0, v1, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    new-array v4, v0, [J

    new-instance v5, Ljava/util/zip/CRC32;

    invoke-direct {v5}, Ljava/util/zip/CRC32;-><init>()V

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-virtual {v5}, Ljava/util/zip/CRC32;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->a:Lehh;

    iget-object v1, v1, Lehh;->d:Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/zip/CRC32;->update([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget v0, v0, Lehh;->a:I

    invoke-virtual {v5}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    aput-wide v6, v4, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CannotHappenException"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v4

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final h()Landroid/util/SparseArray;
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    new-instance v2, Landroid/util/SparseArray;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/util/SparseArray;-><init>(I)V

    invoke-virtual {p0}, Leiy;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget v4, v0, Lehh;->a:I

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final h(Ljava/lang/String;)Z
    .locals 3

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lehj;->b:Lehk;

    iget v2, v2, Lehk;->d:I

    if-nez v2, :cond_0

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    const-string v2, "com.google.android.gm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()Ljava/util/Map;
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Leiy;->b(I)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    invoke-static {v0}, Lell;->b(Lizs;)Lizs;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Leiy;->k:Ljava/lang/Object;

    return-object v0
.end method

.method public final k()Z
    .locals 8

    const/4 v1, 0x0

    iget-object v3, p0, Leiy;->k:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    const/4 v2, 0x2

    :try_start_1
    invoke-direct {p0, v2}, Leiy;->b(I)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v6, 0x2

    invoke-direct {p0, v0, v6}, Leiy;->a(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-nez v2, :cond_1

    iget-object v2, p0, Leiy;->d:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "corpuskey:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_3
    iget-boolean v0, p0, Leiy;->g:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Leiy;->g:Z

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return v1
.end method

.method public final l()V
    .locals 7

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, p0, Leiy;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Leiy;->h:Lems;

    if-eqz v0, :cond_3

    iget-object v3, p0, Leiy;->h:Lems;

    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_0

    iget v6, v1, Lehk;->d:I

    if-nez v6, :cond_0

    new-instance v6, Lehi;

    invoke-direct {v6}, Lehi;-><init>()V

    iget-object v0, v0, Lehj;->a:Lehh;

    iget v0, v0, Lehh;->a:I

    iput v0, v6, Lehi;->a:I

    iget-object v0, v1, Lehk;->c:[Lehl;

    iput-object v0, v6, Lehi;->b:[Lehl;

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    iget-object v1, v0, Lehj;->b:Lehk;

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v4}, Lems;->a(Ljava/lang/Iterable;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Leiy;->g:Z

    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final m()Lehr;
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v2, "flushstatus"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lehr;

    invoke-direct {v2}, Lehr;-><init>()V

    invoke-static {v0, v2}, Lell;->a(Ljava/lang/String;Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehr;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final n()Lehg;
    .locals 4

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v2, "compactstatus"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lehg;

    invoke-direct {v2}, Lehg;-><init>()V

    invoke-static {v0, v2}, Lell;->a(Ljava/lang/String;Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Lehg;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final o()J
    .locals 5

    iget-object v1, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Leiy;->d:Landroid/content/SharedPreferences;

    const-string v2, "created"

    const-wide/16 v3, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final p()J
    .locals 6

    invoke-virtual {p0}, Leiy;->o()J

    move-result-wide v0

    invoke-virtual {p0}, Leiy;->n()Lehg;

    move-result-object v2

    iget-wide v2, v2, Lehg;->a:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()[I
    .locals 6

    const/4 v2, 0x0

    iget-object v3, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-boolean v0, v0, Lehh;->h:Z

    if-nez v0, :cond_3

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    new-array v5, v1, [I

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-boolean v2, v0, Lehh;->h:Z

    if-nez v2, :cond_2

    iget v0, v0, Lehh;->a:I

    aput v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    goto :goto_2

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v5

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v1

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final r()Landroid/util/SparseArray;
    .locals 12

    const/4 v2, 0x0

    iget-object v3, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    new-instance v4, Landroid/util/SparseArray;

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Landroid/util/SparseArray;-><init>(I)V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v6, v1, Lehj;->a:Lehh;

    iget-object v1, v6, Lehh;->d:Ljava/lang/String;

    const-string v7, "com.google.android.gm"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v7, Lehv;

    invoke-direct {v7}, Lehv;-><init>()V

    iget v1, v6, Lehh;->a:I

    iput v1, v7, Lehv;->a:I

    const-string v1, "^f"

    iput-object v1, v7, Lehv;->b:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, v7, Lehv;->d:Z

    iget-object v1, v6, Lehh;->k:[Leii;

    array-length v1, v1

    new-array v1, v1, [Lehw;

    iput-object v1, v7, Lehv;->c:[Lehw;

    move v1, v2

    :goto_1
    iget-object v8, v6, Lehh;->k:[Leii;

    array-length v8, v8

    if-ge v1, v8, :cond_3

    iget-object v8, v6, Lehh;->k:[Leii;

    aget-object v8, v8, v1

    sget-object v9, Leiy;->a:Ljava/util/List;

    iget-object v10, v8, Leii;->a:Ljava/lang/String;

    invoke-interface {v9, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    new-instance v9, Lehw;

    invoke-direct {v9}, Lehw;-><init>()V

    iput v1, v9, Lehw;->a:I

    iget-object v8, v8, Leii;->a:Ljava/lang/String;

    const-string v10, "body"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "GmailBodyStripQuoted"

    aput-object v11, v8, v10

    iput-object v8, v9, Lehw;->b:[Ljava/lang/String;

    :cond_1
    iget-object v8, v7, Lehv;->c:[Lehw;

    aput-object v9, v8, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget v1, v6, Lehh;->a:I

    new-instance v8, Lejf;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Leiy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v6, v6, Lehh;->d:Ljava/lang/String;

    invoke-direct {v8, v0, v6, v7}, Lejf;-><init>(Ljava/lang/String;Ljava/lang/String;Lehv;)V

    invoke-virtual {v4, v1, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v4
.end method

.method public final s()[Ljava/lang/String;
    .locals 6

    iget-object v2, p0, Leiy;->l:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Leiy;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->a:Lehh;

    iget-object v1, v1, Lehh;->d:Ljava/lang/String;

    const-string v5, "com.google.android.gm"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Leiy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
