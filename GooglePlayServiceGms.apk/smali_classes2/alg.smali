.class public final Lalg;
.super Lbmi;
.source "SourceFile"


# instance fields
.field private final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lbmi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-boolean p6, p0, Lalg;->g:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-super/range {p0 .. p5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Null response received for path "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    move-object v3, v0

    iget-object v0, v3, Lsp;->a:Lrz;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lsp;->a:Lrz;

    iget v0, v0, Lrz;->a:I

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_7

    throw v0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {v3, v4}, Lbng;->b(Lsp;Ljava/lang/String;)Lbne;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    :cond_2
    move-object v0, v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lbne;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lbne;->c()Ljava/lang/String;

    move-result-object v6

    const-string v0, "UnregisteredClientId"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "accessNotConfigured"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "userRateLimitExceeded"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_4
    invoke-static {}, Laln;->c()Lalo;

    move-result-object v0

    const/16 v5, 0x3e8

    iput v5, v0, Lalo;->a:I

    const/4 v5, 0x7

    iput v5, v0, Lalo;->b:I

    move-object v5, v0

    :goto_2
    if-nez v5, :cond_5

    move-object v0, v4

    goto :goto_1

    :cond_5
    iput-object v6, v5, Lalo;->c:Ljava/lang/String;

    iget v0, v5, Lalo;->b:I

    const/4 v4, -0x1

    if-eq v0, v4, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Lbkm;->a(Z)V

    new-instance v0, Laln;

    invoke-direct {v0, v5, v2}, Laln;-><init>(Lalo;B)V

    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    throw v3

    :cond_8
    move-object v5, v4

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lakl;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Lbmx;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lbmx;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    :try_start_0
    iget-object v2, p0, Lalg;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lbmz;->b(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const-string v2, "AppStateServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v2, "AppStateServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Auth related exception is being ignored: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lamq;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lalg;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lbmi;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "User-Agent"

    const-string v2, "AppState/1.0"

    invoke-static {p1, v2}, Lbmw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method protected final a(Lsc;Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lbml;

    iget-object v1, p0, Lalg;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lsc;->a()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/16 v3, 0x2710

    :goto_0
    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lbml;-><init>(Landroid/content/Context;Ljava/lang/String;IIF)V

    invoke-virtual {p1, v0}, Lsc;->a(Lsm;)Lsc;

    return-void

    :cond_0
    const/16 v3, 0x1388

    goto :goto_0
.end method

.method protected final b()Ljava/lang/String;
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lakl;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lalg;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lakl;->c:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lakl;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-super {p0, p1}, Lbmi;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lalg;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    throw v0
.end method
