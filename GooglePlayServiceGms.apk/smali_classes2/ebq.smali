.class public final Lebq;
.super Lebp;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lebp;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)Lebq;
    .locals 3

    new-instance v0, Lebq;

    invoke-direct {v0}, Lebq;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "game"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lebq;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method protected final J()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    sget v1, Lxf;->E:I

    invoke-virtual {v0, v1}, Lo;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final K()V
    .locals 3

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "game"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    instance-of v2, v1, Lebr;

    if-eqz v2, :cond_0

    check-cast v1, Lebr;

    :goto_0
    invoke-interface {v1, v0}, Lebr;->d(Lcom/google/android/gms/games/Game;)V

    return-void

    :cond_0
    instance-of v2, v1, Lebs;

    if-eqz v2, :cond_1

    check-cast v1, Lebs;

    invoke-interface {v1}, Lebs;->a()Lebr;

    move-result-object v1

    invoke-static {v1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lebr;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GameChangeAccountDialogFragment must be used with a parent Activity which implements GameAccountSwitcher or GameAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final L()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final M()V
    .locals 3

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "game"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Leee;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
