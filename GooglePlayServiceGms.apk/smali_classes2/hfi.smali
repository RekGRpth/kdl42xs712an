.class final Lhfi;
.super Lhcd;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

.field final synthetic f:Lhfd;


# direct methods
.method constructor <init>(Lhfd;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)V
    .locals 1

    iput-object p1, p0, Lhfi;->f:Lhfd;

    iput-object p4, p0, Lhfi;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lhfi;->e:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    const/16 v0, 0xb

    invoke-direct {p0, p2, v0, p3}, Lhcd;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;ILandroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lhgm;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 6

    const/16 v5, 0x18

    iget-object v0, p0, Lhfi;->f:Lhfd;

    invoke-static {v0}, Lhfd;->c(Lhfd;)Lhfo;

    move-result-object v0

    iget-object v1, p0, Lhfi;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lhfd;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhfi;->e:Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;->b()Ljaq;

    move-result-object v2

    iget-object v3, v0, Lhfo;->a:Landroid/content/Context;

    new-instance v4, Lhfv;

    invoke-direct {v4, v0, v1, p1, v2}, Lhfv;-><init>(Lhfo;Ljava/lang/String;Lhgm;Ljaq;)V

    const-string v0, "get_bin_derived_data"

    invoke-static {v3, v4, v0}, Lgsp;->a(Landroid/content/Context;Lbpj;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v1

    if-ne v1, v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lizs;

    move-result-object v0

    check-cast v0, Ljar;

    iget v1, v0, Ljar;->a:I

    invoke-static {v1}, Lgtk;->a(I)I

    move-result v1

    iput v1, v0, Ljar;->a:I

    new-instance v1, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    invoke-direct {v1, v5, v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILizs;)V

    move-object v0, v1

    :cond_0
    return-object v0
.end method
