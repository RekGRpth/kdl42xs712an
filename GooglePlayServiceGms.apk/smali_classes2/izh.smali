.class public final Lizh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field b:I

.field final c:Ljava/io/OutputStream;

.field private final d:[B


# direct methods
.method private constructor <init>(Ljava/io/OutputStream;[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lizh;->c:Ljava/io/OutputStream;

    iput-object p2, p0, Lizh;->d:[B

    const/4 v0, 0x0

    iput v0, p0, Lizh;->b:I

    array-length v0, p2

    iput v0, p0, Lizh;->a:I

    return-void
.end method

.method constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lizh;->c:Ljava/io/OutputStream;

    iput-object p1, p0, Lizh;->d:[B

    const/4 v0, 0x0

    iput v0, p0, Lizh;->b:I

    add-int/lit8 v0, p3, 0x0

    iput v0, p0, Lizh;->a:I

    return-void
.end method

.method public static a(I)I
    .locals 1

    if-ltz p0, :cond_0

    invoke-static {p0}, Lizh;->d(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(J)I
    .locals 4

    const-wide/16 v2, 0x0

    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    :cond_5
    const-wide/high16 v0, -0x2000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    :cond_6
    const-wide/high16 v0, -0x100000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    :cond_7
    const-wide/high16 v0, -0x8000000000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-static {v1}, Lizh;->d(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljava/io/OutputStream;)Lizh;
    .locals 2

    new-instance v0, Lizh;

    const/16 v1, 0x1000

    new-array v1, v1, [B

    invoke-direct {v0, p0, v1}, Lizh;-><init>(Ljava/io/OutputStream;[B)V

    return-object v0
.end method

.method private a([B)V
    .locals 5

    const/4 v4, 0x0

    array-length v0, p1

    iget v1, p0, Lizh;->a:I

    iget v2, p0, Lizh;->b:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Lizh;->d:[B

    iget v2, p0, Lizh;->b:I

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lizh;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lizh;->b:I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lizh;->a:I

    iget v2, p0, Lizh;->b:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lizh;->d:[B

    iget v3, p0, Lizh;->b:I

    invoke-static {p1, v4, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v1, 0x0

    sub-int/2addr v0, v1

    iget v1, p0, Lizh;->a:I

    iput v1, p0, Lizh;->b:I

    invoke-direct {p0}, Lizh;->b()V

    iget v1, p0, Lizh;->a:I

    if-gt v0, v1, :cond_1

    iget-object v1, p0, Lizh;->d:[B

    invoke-static {p1, v2, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v0, p0, Lizh;->b:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lizh;->c:Ljava/io/OutputStream;

    invoke-virtual {v1, p1, v2, v0}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lizl;->a(II)I

    move-result v0

    invoke-static {v0}, Lizh;->d(I)I

    move-result v0

    return v0
.end method

.method public static b(ILizf;)I
    .locals 3

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-virtual {p1}, Lizf;->a()I

    move-result v1

    invoke-static {v1}, Lizh;->d(I)I

    move-result v1

    invoke-virtual {p1}, Lizf;->a()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILizk;)I
    .locals 3

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-virtual {p1}, Lizk;->b()I

    move-result v1

    invoke-static {v1}, Lizh;->d(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .locals 2

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-static {p1}, Lizh;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private b()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lizh;->c:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    new-instance v0, Lizi;

    invoke-direct {v0}, Lizi;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lizh;->c:Ljava/io/OutputStream;

    iget-object v1, p0, Lizh;->d:[B

    iget v2, p0, Lizh;->b:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    iput v3, p0, Lizh;->b:I

    return-void
.end method

.method private b(J)V
    .locals 4

    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    long-to-int v0, p1

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    return-void

    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public static c(II)I
    .locals 2

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-static {p1}, Lizh;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static c(IJ)I
    .locals 2

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-static {p1, p2}, Lizh;->a(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private c(J)V
    .locals 2

    long-to-int v0, p1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x8

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x10

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x18

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x20

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x28

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x30

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    const/16 v0, 0x38

    shr-long v0, p1, v0

    long-to-int v0, v0

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    return-void
.end method

.method public static d(I)I
    .locals 1

    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static d(II)I
    .locals 2

    invoke-static {p0}, Lizh;->b(I)I

    move-result v0

    invoke-static {p1}, Lizh;->e(I)I

    move-result v1

    invoke-static {v1}, Lizh;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private static e(I)I
    .locals 2

    shl-int/lit8 v0, p0, 0x1

    shr-int/lit8 v1, p0, 0x1f

    xor-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lizh;->c:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lizh;->b()V

    :cond_0
    return-void
.end method

.method public final a(B)V
    .locals 3

    iget v0, p0, Lizh;->b:I

    iget v1, p0, Lizh;->a:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lizh;->b()V

    :cond_0
    iget-object v0, p0, Lizh;->d:[B

    iget v1, p0, Lizh;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lizh;->b:I

    aput-byte p1, v0, v1

    return-void
.end method

.method public final a(ID)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lizh;->c(J)V

    return-void
.end method

.method public final a(IF)V
    .locals 2

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lizh;->a(B)V

    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lizh;->a(B)V

    shr-int/lit8 v1, v0, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lizh;->a(B)V

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    return-void
.end method

.method public final a(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    if-ltz p2, :cond_0

    invoke-virtual {p0, p2}, Lizh;->c(I)V

    :goto_0
    return-void

    :cond_0
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lizh;->b(J)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-direct {p0, p2, p3}, Lizh;->b(J)V

    return-void
.end method

.method public final a(ILizf;)V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-virtual {p2}, Lizf;->b()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lizh;->c(I)V

    invoke-direct {p0, v0}, Lizh;->a([B)V

    return-void
.end method

.method public final a(ILizk;)V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-virtual {p2}, Lizk;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lizh;->c(I)V

    invoke-virtual {p2, p0}, Lizk;->a(Lizh;)V

    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lizh;->c(I)V

    invoke-direct {p0, v0}, Lizh;->a([B)V

    return-void
.end method

.method public final a(IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    return-void
.end method

.method public final b(II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-static {p2}, Lizh;->e(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lizh;->c(I)V

    return-void
.end method

.method public final b(IJ)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lizh;->e(II)V

    invoke-direct {p0, p2, p3}, Lizh;->c(J)V

    return-void
.end method

.method public final c(I)V
    .locals 1

    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    return-void

    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lizh;->a(B)V

    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public final e(II)V
    .locals 1

    invoke-static {p1, p2}, Lizl;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lizh;->c(I)V

    return-void
.end method
