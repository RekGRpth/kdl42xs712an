.class public final Lfzh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfuq;
.implements Lfze;


# static fields
.field private static final a:Z


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private final e:Lftz;

.field private f:Lftx;

.field private g:Lfzf;

.field private h:Landroid/app/Activity;

.field private i:Lbbo;

.field private j:Z

.field private k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

.field private m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lfsr;->O:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lfzh;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lftx;->a:Lftz;

    iput-object v0, p0, Lfzh;->e:Lftz;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->d()V

    return-object v0
.end method


# virtual methods
.method public final P_()V
    .locals 2

    sget-boolean v0, Lfzh;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfzh;->j:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_2
    return-void
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lfzh;->g:Lfzf;

    iput-object v0, p0, Lfzh;->h:Landroid/app/Activity;

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    instance-of v0, p1, Lfzf;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lfzf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lfzf;

    iput-object v0, p0, Lfzh;->g:Lfzf;

    iput-object p1, p0, Lfzh;->h:Landroid/app/Activity;

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 3

    sget-boolean v0, Lfzh;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnectionFailed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-object p1, p0, Lfzh;->i:Lbbo;

    iget-object v0, p0, Lfzh;->g:Lfzf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzh;->g:Lfzf;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lfzf;->b(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    :cond_1
    return-void
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 4

    const/4 v3, 0x0

    sget-boolean v0, Lfzh;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAccountUpgraded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lfzh;->g:Lfzf;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzh;->g:Lfzf;

    invoke-interface {v0, p1, p2}, Lfzf;->b(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    :cond_1
    iput-boolean v3, p0, Lfzh;->j:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-nez v0, :cond_4

    iput-object p2, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v0, p0, Lfzh;->g:Lfzf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzh;->g:Lfzf;

    iget-object v1, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p1, v1}, Lfzf;->a(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lfzh;->j:Z

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Lfzh;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lfzh;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object p2, p0, Lfzh;->m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v0, p0, Lfzh;->g:Lfzf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzh;->g:Lfzf;

    iget-object v1, p0, Lfzh;->m:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p1, v1}, Lfzf;->b(Lbbo;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    iget-boolean v0, p0, Lfzh;->j:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Upgrade account already in progress"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-boolean v0, Lfzh;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "upgradeAccount: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfzh;->j:Z

    iput-object p1, p0, Lfzh;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lfzh;->b_(Landroid/os/Bundle;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfzh;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_1
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzh;->b:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzh;->d:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfzh;->c:Ljava/lang/String;

    new-instance v1, Lfwa;

    iget-object v2, p0, Lfzh;->h:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lfwa;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lfzh;->b:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->a:Ljava/lang/String;

    iget-object v2, p0, Lfzh;->d:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->c:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v1, Lfwa;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sget-object v3, Lfmn;->d:Lbem;

    iget-object v3, v3, Lbem;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v1

    iput-object v0, v1, Lfwa;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v1, p0, Lfzh;->e:Lftz;

    iget-object v2, p0, Lfzh;->h:Landroid/app/Activity;

    invoke-interface {v1, v2, v0, p0, p0}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lfzh;->f:Lftx;

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    sget-boolean v0, Lfzh;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lfzh;->k:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-nez v0, :cond_2

    iget-object v0, p0, Lfzh;->f:Lftx;

    iget-object v1, p0, Lfzh;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, Lftx;->a(Lfuq;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lfzh;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzh;->f:Lftx;

    iget-object v1, p0, Lfzh;->c:Ljava/lang/String;

    iget-object v2, p0, Lfzh;->l:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-interface {v0, p0, v1, v2}, Lftx;->a(Lfuq;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lfzh;->f:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lfzh;->f:Lftx;

    return-void
.end method
