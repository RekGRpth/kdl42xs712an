.class final Lebl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ledl;


# instance fields
.field final a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final b:Landroid/widget/TextView;

.field final c:Landroid/database/CharArrayBuffer;

.field final d:Landroid/widget/TextView;

.field final e:Ljava/lang/StringBuilder;

.field final f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final g:Landroid/widget/TextView;

.field final h:Landroid/view/View;

.field final i:Landroid/view/View;

.field final synthetic j:Lebj;


# direct methods
.method public constructor <init>(Lebj;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lebl;->j:Lebj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lxa;->E:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebl;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v0, Lxa;->F:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebl;->b:Landroid/widget/TextView;

    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lebl;->c:Landroid/database/CharArrayBuffer;

    sget v0, Lxa;->aO:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebl;->d:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lebl;->e:Ljava/lang/StringBuilder;

    sget v0, Lxa;->aB:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lebl;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget-object v0, p0, Lebl;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d()V

    sget v0, Lxa;->aE:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebl;->g:Landroid/widget/TextView;

    sget v0, Lxa;->aq:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebl;->h:Landroid/view/View;

    iget-object v0, p0, Lebl;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->ap:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebl;->i:Landroid/view/View;

    iget-object v0, p0, Lebl;->i:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Lebl;->j:Lebj;

    invoke-virtual {v0}, Lebj;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Ldlb;

    if-eqz v2, :cond_2

    check-cast v0, Ldlb;

    invoke-interface {v0}, Ldlb;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lxa;->ah:I

    if-ne v2, v3, :cond_1

    iget-object v1, p0, Lebl;->j:Lebj;

    invoke-static {v1}, Lebj;->a(Lebj;)Lebk;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lebl;->j:Lebj;

    invoke-static {v1}, Lebj;->a(Lebj;)Lebk;

    move-result-object v1

    invoke-interface {v1, v0}, Lebk;->c(Lcom/google/android/gms/games/Game;)V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, v0, Ldlb;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v2, Lxa;->aq:I

    if-ne v1, v2, :cond_1

    check-cast v0, Ldlb;

    iget-object v1, p0, Lebl;->j:Lebj;

    invoke-static {v1}, Lebj;->a(Lebj;)Lebk;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lebl;->j:Lebj;

    invoke-static {v1}, Lebj;->a(Lebj;)Lebk;

    move-result-object v1

    iget-object v2, p0, Lebl;->j:Lebj;

    invoke-static {v2}, Lebj;->b(Lebj;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lebl;->j:Lebj;

    invoke-static {v3}, Lebj;->c(Lebj;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lebk;->a(Ldlb;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lxa;->ap:I

    if-ne v1, v2, :cond_2

    new-instance v0, Lov;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lov;-><init>(Landroid/content/Context;Landroid/view/View;)V

    sget v1, Lxd;->f:I

    invoke-virtual {v0, v1}, Lov;->a(I)V

    new-instance v1, Ledk;

    invoke-direct {v1, p0, p1}, Ledk;-><init>(Ledl;Landroid/view/View;)V

    iput-object v1, v0, Lov;->c:Lox;

    iget-object v0, v0, Lov;->b:Llz;

    invoke-virtual {v0}, Llz;->a()V

    goto :goto_0

    :cond_2
    const-string v1, "ReqSummListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
