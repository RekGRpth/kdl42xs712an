.class public final Lgru;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgru;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lgrw;)V
    .locals 7

    const/4 v4, 0x1

    new-instance v0, Lgsi;

    iget-object v1, p0, Lgru;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lgsi;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1, v4}, Lgsi;->a(Landroid/accounts/Account;I)Lizx;

    move-result-object v1

    const-string v2, "AddressRetreiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Is cached profile null?"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_0

    move v0, v4

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    invoke-static {v1, p2}, Lgty;->a(Lizx;Lcom/google/android/gms/identity/intents/UserAddressRequest;)[Lipv;

    move-result-object v0

    array-length v1, v0

    if-lez v1, :cond_1

    invoke-interface {p3, v0}, Lgrw;->a([Lipv;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lgsb;

    iget-object v1, p0, Lgru;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lsf;

    move-result-object v2

    new-instance v5, Lgrv;

    invoke-direct {v5, p0, p2, p3}, Lgrv;-><init>(Lgru;Lcom/google/android/gms/identity/intents/UserAddressRequest;Lgrw;)V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/app/GmsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lgsb;-><init>(Landroid/content/Context;Lsf;Landroid/accounts/Account;ILgsd;Landroid/os/Looper;)V

    invoke-virtual {v0}, Lgsb;->run()V

    goto :goto_1
.end method
