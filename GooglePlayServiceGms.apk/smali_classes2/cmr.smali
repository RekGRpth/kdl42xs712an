.class final enum Lcmr;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcmr;

.field public static final enum b:Lcmr;

.field public static final enum c:Lcmr;

.field public static final enum d:Lcmr;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum e:Lcmr;

.field public static final enum f:Lcmr;

.field public static final enum g:Lcmr;

.field public static final enum h:Lcmr;

.field public static final enum i:Lcmr;

.field public static final enum j:Lcmr;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final k:Ljava/util/Map;

.field private static final synthetic n:[Lcmr;


# instance fields
.field private final l:Ljava/lang/String;

.field private final m:Lcmm;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    new-instance v1, Lcmr;

    const-string v2, "NULL"

    const-string v3, "null"

    new-instance v4, Lcmk;

    invoke-direct {v4}, Lcmk;-><init>()V

    invoke-direct {v1, v2, v0, v3, v4}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->a:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "SET_APP_AUTH_STATE"

    const-string v3, "setAuthState"

    new-instance v4, Lcmv;

    invoke-direct {v4}, Lcmv;-><init>()V

    invoke-direct {v1, v2, v6, v3, v4}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->b:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "METADATA"

    const-string v3, "metadata"

    new-instance v4, Lcmi;

    invoke-direct {v4}, Lcmi;-><init>()V

    invoke-direct {v1, v2, v7, v3, v4}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->c:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "UPLOAD"

    const-string v3, "upload"

    new-instance v4, Lcnd;

    invoke-direct {v4}, Lcnd;-><init>()V

    invoke-direct {v1, v2, v8, v3, v4}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->d:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "CONTENT_AND_METADATA"

    const-string v3, "contentAndMetadata"

    new-instance v4, Lcma;

    invoke-direct {v4}, Lcma;-><init>()V

    invoke-direct {v1, v2, v9, v3, v4}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->e:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "UNDO_CONTENT_AND_METADATA"

    const/4 v3, 0x5

    const-string v4, "undoContentAndMetadata"

    new-instance v5, Lcmz;

    invoke-direct {v5}, Lcmz;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->f:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "CREATE_FILE"

    const/4 v3, 0x6

    const-string v4, "createFile"

    new-instance v5, Lcmc;

    invoke-direct {v5}, Lcmc;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->g:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "UNDO_CREATE_FILE"

    const/4 v3, 0x7

    const-string v4, "undoCreateFile"

    new-instance v5, Lcnb;

    invoke-direct {v5}, Lcnb;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->h:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "TRASH"

    const/16 v3, 0x8

    const-string v4, "trash"

    new-instance v5, Lcmx;

    invoke-direct {v5}, Lcmx;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->i:Lcmr;

    new-instance v1, Lcmr;

    const-string v2, "LEGACY_CREATE_FILE"

    const/16 v3, 0x9

    const-string v4, "legacyCreateFile"

    new-instance v5, Lcmg;

    invoke-direct {v5}, Lcmg;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcmr;-><init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V

    sput-object v1, Lcmr;->j:Lcmr;

    const/16 v1, 0xa

    new-array v1, v1, [Lcmr;

    sget-object v2, Lcmr;->a:Lcmr;

    aput-object v2, v1, v0

    sget-object v2, Lcmr;->b:Lcmr;

    aput-object v2, v1, v6

    sget-object v2, Lcmr;->c:Lcmr;

    aput-object v2, v1, v7

    sget-object v2, Lcmr;->d:Lcmr;

    aput-object v2, v1, v8

    sget-object v2, Lcmr;->e:Lcmr;

    aput-object v2, v1, v9

    const/4 v2, 0x5

    sget-object v3, Lcmr;->f:Lcmr;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcmr;->g:Lcmr;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcmr;->h:Lcmr;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcmr;->i:Lcmr;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcmr;->j:Lcmr;

    aput-object v3, v1, v2

    sput-object v1, Lcmr;->n:[Lcmr;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcmr;->k:Ljava/util/Map;

    invoke-static {}, Lcmr;->values()[Lcmr;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcmr;->k:Ljava/util/Map;

    iget-object v5, v3, Lcmr;->l:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcmm;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcmr;->l:Ljava/lang/String;

    iput-object p4, p0, Lcmr;->m:Lcmm;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcmr;
    .locals 1

    sget-object v0, Lcmr;->k:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmr;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcmr;
    .locals 1

    const-class v0, Lcmr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcmr;

    return-object v0
.end method

.method public static values()[Lcmr;
    .locals 1

    sget-object v0, Lcmr;->n:[Lcmr;

    invoke-virtual {v0}, [Lcmr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcmr;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcmr;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcmm;
    .locals 1

    iget-object v0, p0, Lcmr;->m:Lcmm;

    return-object v0
.end method
