.class public final Lhrd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liza;


# static fields
.field private static final a:Liya;


# instance fields
.field private final b:Lhsy;

.field private final c:Limb;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Liya;

    invoke-direct {v0}, Liya;-><init>()V

    sput-object v0, Lhrd;->a:Liya;

    const-string v1, "https://www.google.com/loc/m/api"

    iput-object v1, v0, Liya;->a:Ljava/lang/String;

    sget-object v0, Lhrd;->a:Liya;

    const-string v1, "location"

    iput-object v1, v0, Liya;->c:Ljava/lang/String;

    sget-object v0, Lhrd;->a:Liya;

    const-string v1, "1.0"

    iput-object v1, v0, Liya;->d:Ljava/lang/String;

    sget-object v0, Lhrd;->a:Liya;

    sget-object v1, Lhri;->a:Ljava/lang/String;

    iput-object v1, v0, Liya;->e:Ljava/lang/String;

    sget-object v0, Lhrd;->a:Liya;

    const-string v1, "android"

    iput-object v1, v0, Liya;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Limb;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lhrd;->a:Liya;

    invoke-static {p1, v0}, Lhsy;->a(Landroid/content/Context;Liya;)V

    invoke-static {}, Lhsy;->a()Lhsy;

    move-result-object v0

    iput-object v0, p0, Lhrd;->b:Lhsy;

    invoke-static {p2}, Lhsn;->a(Limb;)Limb;

    move-result-object v0

    iput-object v0, p0, Lhrd;->c:Limb;

    invoke-static {}, Lhsn;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhrd;->d:Ljava/util/Map;

    invoke-static {}, Lhsn;->e()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhrd;->e:Ljava/util/Map;

    return-void
.end method

.method private a(Ljava/lang/String;Livi;)Liyz;
    .locals 3

    :try_start_0
    invoke-virtual {p2}, Livi;->f()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Lhrg;

    invoke-direct {v1, p1, v0}, Lhrg;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v1}, Liyz;->h()V

    invoke-virtual {v1, p0}, Liyz;->a(Liza;)V

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Liyz;Livi;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lhrd;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhre;

    if-eqz v0, :cond_1

    invoke-static {p2, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iput-object v1, v0, Lhre;->b:Landroid/util/Pair;

    iget-object v0, v0, Lhre;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhrd;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lhqg;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez p2, :cond_2

    new-instance v0, Lhrw;

    const/4 v3, 0x0

    invoke-direct {v0, v3, v4, p3}, Lhrw;-><init>(ZLivi;Ljava/lang/String;)V

    :goto_1
    invoke-interface {v1, v2, v0}, Lhqg;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lhrw;

    const/4 v3, 0x1

    invoke-direct {v0, v3, p2, v4}, Lhrw;-><init>(ZLivi;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Liyz;Lizb;)V
    .locals 9

    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    :try_start_0
    iget v1, p2, Lizb;->d:I

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Server error, RC="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Lizb;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lhrd;->a(Liyz;Livi;Ljava/lang/String;)V

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p2}, Lizb;->Z_()Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v3, 0x400

    new-array v3, v3, [B

    :goto_1
    invoke-virtual {v1, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-ltz v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "Failed to read data from MASF: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhrd;->c:Limb;

    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    new-instance v1, Livi;

    sget-object v3, Lihj;->T:Livk;

    invoke-direct {v1, v3}, Livi;-><init>(Livk;)V

    invoke-virtual {v1, v2}, Livi;->b([B)Livi;

    invoke-virtual {v1}, Livi;->d()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v1, Ljava/io/IOException;

    const-string v2, "isValid returned after parsing reply"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "GLS error: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Livi;->c(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Licj;->b:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhrd;->c:Limb;

    invoke-virtual {v2, v1}, Limb;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :cond_4
    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0
.end method

.method public final a(Liyz;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "Failed to send data to MASF: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrd;->c:Limb;

    invoke-virtual {v1, v0}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lhrd;->a(Liyz;Livi;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Livi;Lhqg;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lhrd;->b:Lhsy;

    if-nez v2, :cond_1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhrd;->c:Limb;

    const-string v2, "Could not initialize MASF service."

    invoke-virtual {v1, v2}, Limb;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "g:loc/uil"

    invoke-direct {p0, v2, p1}, Lhrd;->a(Ljava/lang/String;Livi;)Liyz;

    move-result-object v2

    if-eqz p2, :cond_3

    iget-object v3, p0, Lhrd;->e:Ljava/util/Map;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lhrd;->e:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v0, v1

    :cond_2
    const-string v4, "Duplicated request."

    invoke-static {v0, v4}, Lhsn;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lhrd;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    iget-object v0, p0, Lhrd;->b:Lhsy;

    invoke-virtual {v0, v2}, Lhsy;->a(Liyz;)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method
