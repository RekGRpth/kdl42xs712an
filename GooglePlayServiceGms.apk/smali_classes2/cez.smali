.class public final enum Lcez;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcou;


# static fields
.field public static final enum a:Lcez;

.field public static final enum b:Lcez;

.field public static final enum c:Lcez;

.field public static final enum d:Lcez;

.field public static final enum e:Lcez;

.field public static final enum f:Lcez;

.field public static final enum g:Lcez;

.field public static final enum h:Lcez;

.field public static final enum i:Lcez;

.field public static final enum j:Lcez;

.field public static final enum k:Lcez;

.field public static final enum l:Lcez;

.field public static final enum m:Lcez;

.field private static final synthetic o:[Lcez;


# instance fields
.field private final n:Lcdp;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    new-instance v0, Lcez;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "entryId"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {}, Lcef;->a()Lcef;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->a:Lcez;

    new-instance v0, Lcez;

    const-string v1, "REQUEST_TIME"

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "requestTime"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->b:Lcez;

    new-instance v0, Lcez;

    const-string v1, "IS_COMPLETED"

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "isCompleted"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->c:Lcez;

    new-instance v0, Lcez;

    const-string v1, "IS_PAUSED_MANUALLY"

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "isPausedManually"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->d:Lcez;

    new-instance v0, Lcez;

    const-string v1, "IS_UPLOAD_REQUESTED_EVER"

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v2, Lcdq;

    invoke-direct {v2}, Lcdq;-><init>()V

    new-instance v3, Lcei;

    const-string v4, "isUploadRequestedEver"

    sget-object v5, Lcek;->a:Lcek;

    invoke-direct {v3, v4, v5}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v3, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->e:Lcez;

    new-instance v0, Lcez;

    const-string v1, "IS_IMPLICIT"

    const/4 v2, 0x5

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "isImplicit"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->f:Lcez;

    new-instance v0, Lcez;

    const-string v1, "BATCH_NUMBER"

    const/4 v2, 0x6

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "batchNumber"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v4}, Lcei;->a()Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->g:Lcez;

    new-instance v0, Lcez;

    const-string v1, "SYNC_DIRECTION_IN_BATCH"

    const/4 v2, 0x7

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "syncDirectionInBatch"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->h:Lcez;

    new-instance v0, Lcez;

    const-string v1, "BYTES_TRANSFERRED"

    const/16 v2, 0x8

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "bytesTransferred"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->i:Lcez;

    new-instance v0, Lcez;

    const-string v1, "ATTEMPT_COUNT"

    const/16 v2, 0x9

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "attemptCount"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    iput-boolean v7, v4, Lcei;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcei;->a(Ljava/lang/Object;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->j:Lcez;

    new-instance v0, Lcez;

    const-string v1, "UPLOAD_URI"

    const/16 v2, 0xa

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "uploadUri"

    sget-object v6, Lcek;->c:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->k:Lcez;

    new-instance v0, Lcez;

    const-string v1, "UPLOAD_SNAPSHOT_LAST_MODIFIED_TIME"

    const/16 v2, 0xb

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "uploadSnapshotLastModifiedTime"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->l:Lcez;

    new-instance v0, Lcez;

    const-string v1, "DOCUMENT_CONTENT_ID"

    const/16 v2, 0xc

    invoke-static {}, Lcey;->d()Lcey;

    new-instance v3, Lcdq;

    invoke-direct {v3}, Lcdq;-><init>()V

    new-instance v4, Lcei;

    const-string v5, "documentContentId"

    sget-object v6, Lcek;->a:Lcek;

    invoke-direct {v4, v5, v6}, Lcei;-><init>(Ljava/lang/String;Lcek;)V

    invoke-static {}, Lceb;->a()Lceb;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcei;->a(Lcdt;Lcdp;)Lcei;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Lcdq;->a(ILcei;)Lcdq;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcez;-><init>(Ljava/lang/String;ILcdq;)V

    sput-object v0, Lcez;->m:Lcez;

    const/16 v0, 0xd

    new-array v0, v0, [Lcez;

    sget-object v1, Lcez;->a:Lcez;

    aput-object v1, v0, v8

    sget-object v1, Lcez;->b:Lcez;

    aput-object v1, v0, v7

    sget-object v1, Lcez;->c:Lcez;

    aput-object v1, v0, v9

    sget-object v1, Lcez;->d:Lcez;

    aput-object v1, v0, v10

    sget-object v1, Lcez;->e:Lcez;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcez;->f:Lcez;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcez;->g:Lcez;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcez;->h:Lcez;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcez;->i:Lcez;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcez;->j:Lcez;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcez;->k:Lcez;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcez;->l:Lcez;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcez;->m:Lcez;

    aput-object v2, v0, v1

    sput-object v0, Lcez;->o:[Lcez;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcdq;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcdq;->a()Lcdp;

    move-result-object v0

    iput-object v0, p0, Lcez;->n:Lcdp;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcez;
    .locals 1

    const-class v0, Lcez;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcez;

    return-object v0
.end method

.method public static values()[Lcez;
    .locals 1

    sget-object v0, Lcez;->o:[Lcez;

    invoke-virtual {v0}, [Lcez;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcez;

    return-object v0
.end method


# virtual methods
.method public final a()Lcdp;
    .locals 1

    iget-object v0, p0, Lcez;->n:Lcdp;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcez;->n:Lcdp;

    return-object v0
.end method
