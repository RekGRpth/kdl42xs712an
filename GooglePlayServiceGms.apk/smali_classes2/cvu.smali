.class final Lcvu;
.super Lcve;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Ldov;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcvu;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcve;Lbmi;)V
    .locals 2

    const-string v0, "RevisionAgent"

    sget-object v1, Lcvu;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcve;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcve;)V

    new-instance v0, Ldov;

    invoke-direct {v0, p2}, Ldov;-><init>(Lbmi;)V

    iput-object v0, p0, Lcvu;->b:Ldov;

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;)Ldmw;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    iget-object v0, p0, Lcvu;->b:Ldov;

    const-string v1, "android:7"

    invoke-static {v1}, Ldov;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Ldov;->a:Lbmi;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Ldmw;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lbmi;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Ldmw;
    :try_end_0
    .catch Lsp; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    invoke-static {p0}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "lastPassedRevisionCheckMs"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "serverApiVersion3p"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "serverApiVersion1p"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 11

    const-wide/16 v9, -0x1

    const/4 v1, 0x1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    const-string v2, "Must use 1P context for revision check"

    invoke-static {v0, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcto;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v0, "lastPassedRevisionCheckMs"

    invoke-interface {v2, v0, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {}, Lbpg;->c()Lbpe;

    move-result-object v0

    invoke-interface {v0}, Lbpe;->a()J

    move-result-wide v5

    sub-long v7, v5, v3

    cmp-long v0, v3, v9

    if-eqz v0, :cond_1

    sget-object v0, Lcwh;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v7, v3

    if-gtz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    invoke-direct {p0, p2}, Lcvu;->a(Lcom/google/android/gms/common/server/ClientContext;)Ldmw;
    :try_end_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ldqq;->a()I

    move-result v3

    const/16 v4, 0x3eb

    if-eq v3, v4, :cond_2

    throw v0

    :cond_2
    const-string v0, "RevisionAgent"

    const-string v3, "Cannot use restricted APIs, resetting"

    invoke-static {v0, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcvu;->a(Landroid/content/Context;)V

    invoke-direct {p0, p2}, Lcvu;->a(Lcom/google/android/gms/common/server/ClientContext;)Ldmw;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ldmw;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ldmw;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ldmw;->c()Ljava/lang/String;

    move-result-object v7

    const-string v0, "INVALID"

    invoke-static {v3, v0}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "lastPassedRevisionCheckMs"

    invoke-interface {v1, v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    const-string v2, "serverApiVersion3p"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "serverApiVersion1p"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
