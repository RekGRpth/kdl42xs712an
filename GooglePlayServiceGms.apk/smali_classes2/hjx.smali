.class public final Lhjx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final O:Lhtg;


# instance fields
.field A:Z

.field B:Z

.field C:I

.field D:Z

.field E:Z

.field F:I

.field G:J

.field H:Lhuv;

.field I:Libt;

.field J:J

.field K:J

.field final L:Lhjf;

.field final M:Lhte;

.field final N:Lhji;

.field private final P:Ljava/util/Random;

.field private Q:Lidr;

.field private R:J

.field final a:Lidu;

.field final b:Lhof;

.field final c:Lhkl;

.field final d:Lhtu;

.field final e:Lhoj;

.field final f:Lhlr;

.field g:J

.field h:J

.field i:J

.field j:J

.field k:J

.field l:Lilx;

.field final m:Libc;

.field n:Lhud;

.field o:Z

.field p:Z

.field q:Livi;

.field r:J

.field s:J

.field t:Lhuv;

.field u:Z

.field v:J

.field w:J

.field x:Lhtg;

.field y:J

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lhtg;

    invoke-direct {v0}, Lhtg;-><init>()V

    sput-object v0, Lhjx;->O:Lhtg;

    return-void
.end method

.method public constructor <init>(Lidu;Lhof;Lhoj;Libt;Lhjf;Lhtu;Lhkl;Lhte;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lhjx;->P:Ljava/util/Random;

    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lhjx;->g:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhjx;->h:J

    const-wide v0, 0x1f3fffffc18L

    iput-wide v0, p0, Lhjx;->i:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhjx;->j:J

    const-wide/32 v0, 0x7fffffff

    iput-wide v0, p0, Lhjx;->k:J

    const/4 v0, 0x0

    iput-object v0, p0, Lhjx;->l:Lilx;

    new-instance v0, Lhud;

    invoke-direct {v0}, Lhud;-><init>()V

    iput-object v0, p0, Lhjx;->n:Lhud;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->o:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->p:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->R:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->r:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->s:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->u:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->v:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->w:J

    new-instance v0, Lhtg;

    const-wide/16 v1, 0x7530

    invoke-direct {v0, v1, v2}, Lhtg;-><init>(J)V

    iput-object v0, p0, Lhjx;->x:Lhtg;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->y:J

    const/4 v0, 0x0

    iput v0, p0, Lhjx;->z:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->A:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->B:Z

    const/4 v0, 0x0

    iput v0, p0, Lhjx;->C:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->D:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhjx;->E:Z

    const/4 v0, 0x0

    iput v0, p0, Lhjx;->F:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->G:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhjx;->J:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhjx;->K:J

    iput-object p1, p0, Lhjx;->a:Lidu;

    iput-object p2, p0, Lhjx;->b:Lhof;

    iput-object p3, p0, Lhjx;->e:Lhoj;

    iput-object p4, p0, Lhjx;->I:Libt;

    new-instance v0, Libc;

    iget-object v1, p2, Lhof;->d:Lhou;

    iget-object v2, p2, Lhof;->e:Lhou;

    invoke-virtual {p2}, Lhof;->e()Lhod;

    move-result-object v3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Libc;-><init>(Lhou;Lhou;Lhod;Libt;Lidu;)V

    iput-object v0, p0, Lhjx;->m:Libc;

    iput-object p6, p0, Lhjx;->d:Lhtu;

    iput-object p7, p0, Lhjx;->c:Lhkl;

    iput-object p5, p0, Lhjx;->L:Lhjf;

    iput-object p8, p0, Lhjx;->M:Lhte;

    new-instance v0, Lhlr;

    iget-object v1, p2, Lhof;->e:Lhou;

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lhlr;-><init>(Lhou;Licm;)V

    iput-object v0, p0, Lhjx;->f:Lhlr;

    new-instance v0, Lhji;

    invoke-direct {v0}, Lhji;-><init>()V

    iput-object v0, p0, Lhjx;->N:Lhji;

    invoke-interface {p1}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x4144997000000000L    # 2700000.0

    mul-double/2addr v2, v4

    double-to-long v2, v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lhjx;->J:J

    return-void
.end method

.method private static a(Lhtg;[Lhuv;J)Livi;
    .locals 15

    new-instance v9, Livi;

    sget-object v2, Lihj;->Q:Livk;

    invoke-direct {v9, v2}, Livi;-><init>(Livk;)V

    const-wide v6, 0x7fffffffffffffffL

    const-wide v2, 0x7fffffffffffffffL

    if-eqz p0, :cond_0

    move-object/from16 v0, p1

    array-length v10, v0

    const/4 v4, 0x0

    move v8, v4

    :goto_0
    if-ge v8, v10, :cond_0

    aget-object v11, p1, v8

    if-eqz v11, :cond_7

    iget-wide v4, v11, Lhuv;->a:J

    iget-object v12, p0, Lhtg;->a:Lhtf;

    invoke-virtual {v12}, Lhtf;->f()J

    move-result-wide v12

    sub-long/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    cmp-long v12, v4, v6

    if-gez v12, :cond_7

    const-wide/32 v12, 0xafc8

    cmp-long v12, v4, v12

    if-gez v12, :cond_7

    iget-wide v2, v11, Lhuv;->a:J

    :goto_1
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move-wide v6, v4

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p1

    array-length v8, v0

    const/4 v4, 0x0

    move v7, v4

    move-object v4, p0

    :goto_2
    if-ge v7, v8, :cond_2

    aget-object v10, p1, v7

    new-instance v11, Livi;

    sget-object v6, Lihj;->F:Livk;

    invoke-direct {v11, v6}, Livi;-><init>(Livk;)V

    const/4 v6, 0x1

    if-eqz v10, :cond_6

    const/4 v6, 0x1

    move-wide/from16 v0, p2

    invoke-virtual {v10, v0, v1, v6}, Lhuv;->a(JZ)Livi;

    move-result-object v6

    const/4 v12, 0x3

    const/4 v13, 0x2

    invoke-virtual {v6, v12, v13}, Livi;->e(II)Livi;

    const/4 v12, 0x2

    invoke-virtual {v11, v12, v6}, Livi;->b(ILivi;)Livi;

    iget-wide v12, v10, Lhuv;->a:J

    cmp-long v6, v12, v2

    if-nez v6, :cond_1

    if-eqz v4, :cond_1

    iget-object v6, v4, Lhtg;->a:Lhtf;

    if-eqz v6, :cond_1

    move-wide/from16 v0, p2

    invoke-virtual {v6, v11, v0, v1}, Lhtf;->a(Livi;J)V

    const/4 v4, 0x0

    :cond_1
    new-instance v6, Livi;

    sget-object v10, Lihj;->I:Livk;

    invoke-direct {v6, v10}, Livi;-><init>(Livk;)V

    const/4 v10, 0x1

    const/4 v12, 0x1

    invoke-virtual {v6, v10, v12}, Livi;->e(II)Livi;

    const/16 v10, 0xc

    invoke-virtual {v11, v10, v6}, Livi;->a(ILivi;)V

    const/4 v6, 0x0

    move v14, v6

    move-object v6, v4

    move v4, v14

    :goto_3
    if-nez v4, :cond_5

    const/16 v4, 0xa

    const/4 v10, 0x0

    invoke-virtual {v11, v4, v10}, Livi;->e(II)Livi;

    const/4 v4, 0x4

    invoke-virtual {v9, v4, v11}, Livi;->a(ILivi;)V

    add-int/lit8 v4, v5, 0x1

    :goto_4
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v4

    move-object v4, v6

    goto :goto_2

    :cond_2
    if-eqz v4, :cond_4

    if-nez v5, :cond_4

    new-instance v2, Livi;

    sget-object v3, Lihj;->F:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/16 v3, 0xa

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Livi;->e(II)Livi;

    iget-object v3, v4, Lhtg;->a:Lhtf;

    if-eqz v3, :cond_3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v2, v0, v1}, Lhtf;->a(Livi;J)V

    :cond_3
    const/4 v3, 0x4

    invoke-virtual {v9, v3, v2}, Livi;->a(ILivi;)V

    :cond_4
    return-object v9

    :cond_5
    move v4, v5

    goto :goto_4

    :cond_6
    move v14, v6

    move-object v6, v4

    move v4, v14

    goto :goto_3

    :cond_7
    move-wide v4, v6

    goto/16 :goto_1
.end method

.method private a(I)V
    .locals 3

    iget v0, p0, Lhjx;->C:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lhjx;->C:I

    iget-object v0, p0, Lhjx;->a:Lidu;

    iget v1, p0, Lhjx;->F:I

    iget v2, p0, Lhjx;->C:I

    invoke-interface {v0, v1, v2}, Lidu;->a(II)V

    :cond_0
    return-void
.end method

.method private a(JZZ)V
    .locals 7

    iget-boolean v2, p0, Lhjx;->A:Z

    iget-boolean v0, p0, Lhjx;->A:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhjx;->D:Z

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean p3, p0, Lhjx;->A:Z

    iput-boolean p4, p0, Lhjx;->D:Z

    iget-boolean v1, p0, Lhjx;->A:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lhjx;->D:Z

    if-eqz v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_1
    if-eq v0, v1, :cond_2

    if-eqz v1, :cond_6

    iget-wide v0, p0, Lhjx;->g:J

    const-wide v3, 0x1f3fffffc18L

    cmp-long v0, v0, v3

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    iput-wide v0, p0, Lhjx;->h:J

    :cond_2
    :goto_2
    iget-boolean v0, p0, Lhjx;->A:Z

    if-eq v2, v0, :cond_3

    iget-boolean v0, p0, Lhjx;->A:Z

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lhjx;->i:J

    const-wide v2, 0x1f3fffffc18L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    iput-wide v0, p0, Lhjx;->k:J

    :cond_3
    :goto_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, p1

    iput-wide v0, p0, Lhjx;->h:J

    goto :goto_2

    :cond_7
    const-wide/32 v0, 0x7fffffff

    add-long/2addr v0, p1

    iput-wide v0, p0, Lhjx;->k:J

    goto :goto_3
.end method

.method private a(J[Lhuv;Lhtj;ZZ)V
    .locals 36

    if-nez p3, :cond_44

    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iput-object v3, v0, Lhjx;->H:Lhuv;

    :cond_0
    if-eqz p5, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lhjx;->q:Livi;

    :cond_1
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->R:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0xdbba00

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->P:Ljava/util/Random;

    sget-object v4, Lhjv;->e:Lbfy;

    invoke-static {v3, v4}, Lhjv;->a(Ljava/util/Random;Lbfy;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    const-string v4, "nlp"

    const-string v5, "settings"

    const-string v6, "gps"

    move-object/from16 v0, p0

    iget-object v7, v0, Lhjx;->a:Lidu;

    invoke-interface {v7}, Lidu;->k()Z

    move-result v7

    if-eqz v7, :cond_45

    const-wide/16 v7, 0x1

    :goto_1
    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    const-string v4, "nlp"

    const-string v5, "settings"

    const-string v6, "cell"

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lhjx;->A:Z

    if-eqz v7, :cond_46

    const-wide/16 v7, 0x1

    :goto_2
    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    const-string v4, "nlp"

    const-string v5, "settings"

    const-string v6, "wifi"

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lhjx;->D:Z

    if-eqz v7, :cond_47

    const-wide/16 v7, 0x1

    :goto_3
    const/4 v9, 0x0

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    move-wide/from16 v0, p1

    invoke-interface {v3, v0, v1}, Lidu;->a(J)V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->R:J

    :cond_3
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->K:J

    sub-long v3, p1, v3

    const-wide/32 v5, 0x36ee80

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lhof;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->I:Libt;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->b()J

    move-result-wide v4

    iget-object v3, v3, Libt;->a:Lhoc;

    const-wide/32 v6, 0x240c8400

    sub-long v6, v4, v6

    const-wide/32 v8, 0xa4cb800

    sub-long/2addr v4, v8

    iget-object v8, v3, Lhoc;->a:Lhnz;

    invoke-virtual {v8, v4, v5, v4, v5}, Lhnz;->a(JJ)V

    iget-object v4, v3, Lhoc;->b:Lhnt;

    invoke-virtual {v4, v6, v7, v6, v7}, Lhnt;->a(JJ)V

    iget-object v3, v3, Lhoc;->c:Lhnt;

    invoke-virtual {v3, v6, v7, v6, v7}, Lhnt;->a(JJ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->e:Lhoj;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->e:Lhoj;

    invoke-virtual {v3}, Lhoj;->c()V

    :cond_4
    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->K:J

    :cond_5
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->v:J

    sub-long v32, p1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    if-nez v3, :cond_48

    const-wide/16 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->x:Lhtg;

    iget-object v0, v5, Lhtg;->a:Lhtf;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->h:J

    cmp-long v5, p1, v5

    if-ltz v5, :cond_49

    const/4 v5, 0x1

    move v10, v5

    :goto_5
    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->k:J

    cmp-long v5, p1, v5

    if-ltz v5, :cond_4a

    const/4 v5, 0x1

    move/from16 v31, v5

    :goto_6
    if-nez v10, :cond_6

    if-eqz v31, :cond_4b

    :cond_6
    const/4 v5, 0x1

    move/from16 v30, v5

    :goto_7
    if-eqz v34, :cond_4c

    invoke-virtual/range {v34 .. v34}, Lhtf;->i()Z

    move-result v5

    if-eqz v5, :cond_4c

    invoke-virtual/range {v34 .. v34}, Lhtf;->f()J

    move-result-wide v5

    sub-long v5, p1, v5

    const-wide/32 v7, 0xafc8

    cmp-long v5, v5, v7

    if-gez v5, :cond_4c

    const/4 v5, 0x1

    :goto_8
    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->H:Lhuv;

    if-eqz v6, :cond_4d

    const-wide/32 v6, 0xafc8

    cmp-long v6, v3, v6

    if-gez v6, :cond_4d

    const/4 v6, 0x1

    move/from16 v29, v6

    :goto_9
    if-eqz v5, :cond_4f

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->M:Lhte;

    invoke-virtual {v6}, Lhte;->a()Z

    move-result v6

    if-eqz v6, :cond_4f

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->n:Lhud;

    iget-object v7, v6, Lhud;->c:Lhtd;

    if-nez v7, :cond_4e

    const/4 v6, 0x0

    :goto_a
    move-object/from16 v0, v34

    if-eq v0, v6, :cond_4f

    const/4 v6, 0x1

    move/from16 v28, v6

    :goto_b
    if-eqz v29, :cond_51

    move-object/from16 v0, p0

    iget-object v7, v0, Lhjx;->H:Lhuv;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->n:Lhud;

    iget-object v8, v6, Lhud;->b:Lhuu;

    if-nez v8, :cond_50

    const/4 v6, 0x0

    :goto_c
    if-eq v7, v6, :cond_51

    const/4 v6, 0x1

    move/from16 v27, v6

    :goto_d
    if-eqz v30, :cond_52

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->A:Z

    if-eqz v6, :cond_52

    const/4 v6, 0x1

    move/from16 v26, v6

    :goto_e
    if-eqz v10, :cond_53

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->D:Z

    if-eqz v6, :cond_53

    if-eqz v27, :cond_7

    const-wide/32 v6, 0xafc8

    sub-long v3, v6, v3

    const-wide/16 v6, 0xc8

    cmp-long v3, v3, v6

    if-gez v3, :cond_53

    :cond_7
    const/4 v3, 0x1

    move/from16 v25, v3

    :goto_f
    if-nez v26, :cond_8

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->y:J

    const-wide/16 v6, -0x1

    cmp-long v3, v3, v6

    if-eqz v3, :cond_54

    if-eqz v34, :cond_8

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->y:J

    invoke-virtual/range {v34 .. v34}, Lhtf;->f()J

    move-result-wide v6

    cmp-long v3, v3, v6

    if-lez v3, :cond_54

    :cond_8
    const/4 v3, 0x1

    move/from16 v24, v3

    :goto_10
    if-nez v25, :cond_9

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->G:J

    const-wide/16 v6, -0x1

    cmp-long v3, v3, v6

    if-eqz v3, :cond_55

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhjx;->D:Z

    if-eqz v3, :cond_55

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->G:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->H:Lhuv;

    iget-wide v6, v6, Lhuv;->a:J

    cmp-long v3, v3, v6

    if-lez v3, :cond_55

    :cond_9
    const/4 v3, 0x1

    move/from16 v23, v3

    :goto_11
    if-eqz v24, :cond_56

    if-nez v26, :cond_a

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->y:J

    sub-long v3, p1, v3

    const-wide/16 v6, 0x1388

    cmp-long v3, v3, v6

    if-gez v3, :cond_56

    :cond_a
    const/4 v3, 0x1

    move/from16 v22, v3

    :goto_12
    if-eqz v23, :cond_57

    if-nez v25, :cond_b

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->G:J

    sub-long v3, p1, v3

    const-wide/16 v6, 0x1388

    cmp-long v3, v3, v6

    if-gez v3, :cond_57

    :cond_b
    const/4 v3, 0x1

    move/from16 v21, v3

    :goto_13
    if-nez v22, :cond_c

    if-eqz v21, :cond_58

    :cond_c
    const/4 v3, 0x1

    :goto_14
    if-nez v28, :cond_d

    if-eqz v27, :cond_e

    :cond_d
    if-eqz v3, :cond_f

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhjx;->D:Z

    if-nez v3, :cond_59

    if-eqz v5, :cond_59

    if-nez v22, :cond_59

    :cond_f
    const/4 v3, 0x1

    :goto_15
    if-nez p5, :cond_11

    if-nez p6, :cond_11

    if-nez v30, :cond_10

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lhjx;->o:Z

    if-eqz v4, :cond_5a

    :cond_10
    if-eqz v3, :cond_5a

    :cond_11
    const/4 v3, 0x1

    move/from16 v20, v3

    :goto_16
    const/4 v3, 0x0

    if-eqz v20, :cond_b1

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->Q:Lidr;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    invoke-interface {v3}, Lidu;->m()Lidr;

    move-result-object v3

    const/4 v5, 0x0

    if-eqz v4, :cond_b0

    invoke-interface {v4}, Lidr;->f()J

    move-result-wide v6

    sub-long v6, p1, v6

    const-wide/32 v8, 0x15f90

    cmp-long v6, v6, v8

    if-gez v6, :cond_b0

    :goto_17
    if-eqz v3, :cond_12

    invoke-interface {v3}, Lidr;->f()J

    move-result-wide v5

    sub-long v5, p1, v5

    const-wide/32 v7, 0x15f90

    cmp-long v5, v5, v7

    if-gez v5, :cond_12

    invoke-interface {v3}, Lidr;->a()F

    move-result v5

    if-eqz v4, :cond_5b

    invoke-interface {v4}, Lidr;->a()F

    move-result v6

    float-to-double v7, v5

    float-to-double v5, v6

    const-wide v11, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v5, v11

    cmpg-double v5, v7, v5

    if-gez v5, :cond_af

    :goto_18
    move-object v4, v3

    :cond_12
    :goto_19
    if-nez v4, :cond_5c

    const/4 v13, 0x0

    :goto_1a
    if-eqz v31, :cond_13

    if-eqz v30, :cond_14

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhjx;->p:Z

    if-eqz v3, :cond_5f

    :cond_14
    const/4 v3, 0x1

    move v11, v3

    :goto_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lhjx;->m:Libc;

    move-object/from16 v35, v0

    if-eqz v28, :cond_60

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->x:Lhtg;

    move-object v5, v3

    :goto_1c
    if-eqz v29, :cond_61

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    move-object v9, v3

    :goto_1d
    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    invoke-interface {v3}, Lidu;->j()Licm;

    move-result-object v3

    invoke-interface {v3}, Licm;->a()J

    move-result-wide v6

    move-object/from16 v0, v35

    iget-object v12, v0, Libc;->b:Liax;

    iget-object v3, v12, Liax;->a:Licm;

    invoke-interface {v3}, Licm;->b()J

    move-result-wide v14

    const/4 v8, 0x0

    const/4 v3, 0x0

    if-eqz v5, :cond_ae

    iget-object v8, v5, Lhtg;->a:Lhtf;

    iget-object v3, v5, Lhtg;->b:Ljava/util/List;

    move-object v4, v3

    :goto_1e
    if-eqz v8, :cond_15

    invoke-virtual {v8}, Lhtf;->i()Z

    move-result v3

    if-nez v3, :cond_62

    :cond_15
    new-instance v3, Lhtd;

    const/4 v4, 0x0

    sget-object v5, Lhty;->b:Lhty;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lhtd;-><init>(Lhug;Lhty;JLhtf;)V

    move-object v12, v3

    :goto_1f
    iget-object v3, v12, Lhtd;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_6e

    const/4 v3, 0x1

    move/from16 v19, v3

    :goto_20
    if-nez v13, :cond_ad

    iget-object v3, v12, Lhtd;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_ad

    iget-object v4, v12, Lhtd;->c:Lhug;

    new-instance v3, Lhug;

    iget v5, v4, Lhug;->d:I

    iget v6, v4, Lhug;->e:I

    iget v4, v4, Lhug;->f:I

    mul-int/lit8 v4, v4, 0x4

    invoke-direct {v3, v5, v6, v4}, Lhug;-><init>(III)V

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_16

    const-string v4, "LocatorManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Generating cell-based aperture: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_16
    move-object v4, v3

    :goto_21
    if-nez p4, :cond_6f

    const/4 v3, 0x0

    :goto_22
    move-object/from16 v0, v35

    iget-object v5, v0, Libc;->a:Licc;

    invoke-static {v9}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Licc;->a(Ljava/util/List;Lhug;)Lhuu;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v5, v0, Lhuu;->d:Lhty;

    sget-object v6, Lhty;->a:Lhty;

    if-eq v5, v6, :cond_17

    if-eq v9, v3, :cond_17

    if-eqz v3, :cond_17

    move-object/from16 v0, v35

    iget-object v5, v0, Libc;->a:Licc;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v5, v3, v4}, Licc;->a(Ljava/util/List;Lhug;)Lhuu;

    move-result-object v18

    :cond_17
    if-eqz v18, :cond_70

    move-object/from16 v0, v18

    iget-object v3, v0, Lhuu;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_70

    const/4 v3, 0x1

    move v6, v3

    :goto_23
    if-eqz v6, :cond_71

    move-object/from16 v0, v18

    iget-object v3, v0, Lhuu;->c:Lhug;

    if-eqz v3, :cond_71

    move-object/from16 v0, v18

    iget-object v3, v0, Lhuu;->c:Lhug;

    instance-of v3, v3, Lhtl;

    if-eqz v3, :cond_71

    move-object/from16 v0, v18

    iget-object v3, v0, Lhuu;->c:Lhug;

    check-cast v3, Lhtl;

    iget-object v3, v3, Lhtl;->b:Ljava/lang/String;

    if-eqz v3, :cond_71

    const/4 v3, 0x1

    move v13, v3

    :goto_24
    move-object/from16 v0, v18

    iget-object v3, v0, Lhtw;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_72

    const/4 v3, 0x1

    :goto_25
    iget-object v4, v12, Lhtw;->d:Lhty;

    sget-object v5, Lhty;->a:Lhty;

    if-ne v4, v5, :cond_73

    const/4 v4, 0x1

    :goto_26
    if-nez v3, :cond_74

    if-nez v4, :cond_74

    const/4 v3, 0x0

    :goto_27
    if-nez v3, :cond_ac

    if-eqz p4, :cond_ac

    move-object/from16 v0, p4

    iget-object v4, v0, Lhtj;->d:Lhty;

    sget-object v5, Lhty;->a:Lhty;

    if-ne v4, v5, :cond_ac

    move-object/from16 v14, p4

    :goto_28
    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->d:Ljava/util/Random;

    sget-object v4, Lhjv;->d:Lbfy;

    invoke-static {v3, v4}, Lhjv;->a(Ljava/util/Random;Lbfy;)Z

    move-result v3

    if-eqz v3, :cond_1b

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    if-eqz v14, :cond_ab

    if-ne v14, v12, :cond_7b

    const/4 v5, 0x1

    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    :goto_29
    if-eqz v6, :cond_18

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "has_wifi"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    :cond_18
    if-eqz v19, :cond_19

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "has_cell"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    :cond_19
    if-eqz v13, :cond_1a

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "has_indoor"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    :cond_1a
    if-eqz v17, :cond_7d

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "uses_cell"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    :cond_1b
    :goto_2a
    if-eqz v11, :cond_80

    if-ne v14, v12, :cond_80

    const/4 v8, 0x1

    :goto_2b
    new-instance v3, Lhud;

    move-object v4, v14

    move-object/from16 v5, v18

    move-object v6, v12

    move-object/from16 v7, p4

    invoke-direct/range {v3 .. v8}, Lhud;-><init>(Lhtw;Lhuu;Lhtd;Lhtj;Z)V

    move-object v11, v3

    :goto_2c
    if-eqz v11, :cond_81

    iget-object v3, v11, Lhud;->a:Lhtw;

    if-eqz v3, :cond_81

    const/4 v3, 0x1

    move v5, v3

    :goto_2d
    if-eqz v11, :cond_82

    iget-object v3, v11, Lhud;->c:Lhtd;

    iget-object v3, v3, Lhtd;->d:Lhty;

    sget-object v4, Lhty;->c:Lhty;

    if-ne v3, v4, :cond_82

    const/4 v3, 0x1

    move v4, v3

    :goto_2e
    if-eqz v11, :cond_83

    iget-object v3, v11, Lhud;->b:Lhuu;

    iget-object v3, v3, Lhuu;->d:Lhty;

    sget-object v6, Lhty;->c:Lhty;

    if-ne v3, v6, :cond_83

    const/4 v3, 0x1

    :goto_2f
    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->q:Livi;

    if-nez v6, :cond_84

    if-nez v30, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->u:Z

    if-eqz v6, :cond_84

    :cond_1c
    if-eqz v4, :cond_1d

    if-nez v28, :cond_1e

    :cond_1d
    if-eqz v3, :cond_84

    if-eqz v27, :cond_84

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    iget-object v3, v3, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_84

    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->N:Lhji;

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Lhji;->a(J)Z

    move-result v3

    if-eqz v3, :cond_84

    const/4 v3, 0x1

    move v9, v3

    :goto_30
    if-nez v9, :cond_1f

    if-eqz v5, :cond_85

    :cond_1f
    const/4 v3, 0x1

    move v8, v3

    :goto_31
    if-eqz v5, :cond_8d

    if-eqz v9, :cond_21

    iget-object v3, v11, Lhud;->a:Lhtw;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->m()Lidr;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->g:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lhjx;->i:J

    invoke-static {v5, v6, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    if-nez v4, :cond_88

    iget-object v3, v3, Lhtw;->c:Lhug;

    iget v3, v3, Lhug;->f:I

    const v4, 0x30d40

    if-ge v3, v4, :cond_86

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_20

    const-string v3, "NetworkLocator"

    const-string v4, "Reporting available location since we\'ve never reported one before."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_20
    const/4 v3, 0x1

    :goto_32
    if-eqz v3, :cond_8d

    :cond_21
    const/4 v3, 0x1

    :goto_33
    invoke-direct/range {p0 .. p0}, Lhjx;->b()Z

    move-result v6

    if-nez v6, :cond_8e

    if-eqz v30, :cond_8e

    const/4 v4, 0x1

    :goto_34
    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->q:Livi;

    if-eqz v5, :cond_22

    move-object/from16 v0, p0

    iget-wide v12, v0, Lhjx;->r:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lhjx;->v:J

    cmp-long v5, v12, v14

    if-gez v5, :cond_23

    :cond_22
    if-eqz v9, :cond_8f

    :cond_23
    const/4 v5, 0x1

    :goto_35
    if-nez v24, :cond_24

    if-nez v23, :cond_24

    if-eqz v5, :cond_90

    :cond_24
    const/4 v5, 0x1

    :goto_36
    if-eqz v6, :cond_91

    if-nez v30, :cond_91

    const-wide/16 v12, 0x1388

    cmp-long v7, v32, v12

    if-gez v7, :cond_25

    if-nez v5, :cond_91

    :cond_25
    const/4 v5, 0x1

    move v7, v5

    :goto_37
    if-eqz v6, :cond_92

    if-eqz v30, :cond_92

    if-nez v7, :cond_92

    const/4 v5, 0x1

    :goto_38
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->o:Z

    if-nez v6, :cond_26

    if-eqz v30, :cond_93

    :cond_26
    if-nez v20, :cond_93

    const/4 v6, 0x1

    :goto_39
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lhjx;->o:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->p:Z

    if-nez v6, :cond_27

    if-eqz v31, :cond_94

    :cond_27
    if-nez v20, :cond_94

    const/4 v6, 0x1

    :goto_3a
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lhjx;->p:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lhjx;->u:Z

    if-nez v6, :cond_28

    if-eqz v30, :cond_95

    :cond_28
    if-nez v8, :cond_95

    const/4 v6, 0x1

    :goto_3b
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lhjx;->u:Z

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjx;->l:Lilx;

    invoke-interface {v4, v6, v8}, Lidu;->a(ILilx;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->a()J

    move-result-wide p1

    const-wide/16 v12, -0x1

    move-object/from16 v0, p0

    iput-wide v12, v0, Lhjx;->w:J

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->v:J

    :cond_29
    if-eqz v5, :cond_2a

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->v:J

    :cond_2a
    if-eqz v10, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->d:Lhtu;

    sget-object v5, Lhtv;->a:Lhtv;

    invoke-virtual {v4, v5}, Lhtu;->a(Lhtv;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lhjx;->g:J

    add-long v4, v4, p1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lhjx;->h:J

    :cond_2b
    if-eqz v31, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->d:Lhtu;

    sget-object v5, Lhtv;->a:Lhtv;

    invoke-virtual {v4, v5}, Lhtu;->a(Lhtv;)V

    move-object/from16 v0, p0

    iget-wide v4, v0, Lhjx;->i:J

    add-long v4, v4, p1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lhjx;->k:J

    :cond_2c
    if-eqz v26, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->f()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->y:J

    :cond_2d
    if-eqz v25, :cond_2e

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->l:Lilx;

    invoke-interface {v4, v5}, Lidu;->a(Lilx;)V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->G:J

    :cond_2e
    if-eqz v9, :cond_2f

    if-eqz v28, :cond_96

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->x:Lhtg;

    move-object v5, v4

    :goto_3c
    if-eqz v27, :cond_97

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->H:Lhuv;

    iget-object v4, v4, Lhuv;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_97

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->H:Lhuv;

    :goto_3d
    const/4 v6, 0x1

    new-array v6, v6, [Lhuv;

    const/4 v8, 0x0

    aput-object v4, v6, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjx;->a:Lidu;

    invoke-interface {v8}, Lidu;->j()Licm;

    move-result-object v8

    invoke-interface {v8}, Licm;->c()J

    move-result-wide v8

    invoke-static {v5, v6, v8, v9}, Lhjx;->a(Lhtg;[Lhuv;J)Livi;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lhjx;->q:Livi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->c:Lhkl;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->a:Lidu;

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjx;->q:Livi;

    invoke-virtual {v5, v6, v8}, Lhkl;->a(Lidu;Livi;)V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->r:J

    move-object/from16 v0, p0

    iput-object v4, v0, Lhjx;->t:Lhuv;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->q:Livi;

    invoke-interface {v4, v5}, Lidu;->a(Livi;)V

    :cond_2f
    if-eqz v20, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->I:Libt;

    invoke-virtual {v4}, Libt;->a()V

    :cond_30
    if-eqz v3, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->f:Lhlr;

    if-eqz v29, :cond_98

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    :goto_3e
    invoke-virtual {v4, v11, v3}, Lhlr;->a(Lhud;Lhuv;)Lhlt;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    invoke-interface {v3, v5}, Lidu;->a(Lhlt;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    invoke-interface {v3}, Lidu;->m()Lidr;

    move-result-object v3

    if-eqz v3, :cond_31

    invoke-interface {v3}, Lidr;->f()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-wide v8, v0, Lhjx;->s:J

    cmp-long v6, v8, v3

    if-lez v6, :cond_99

    sget-boolean v6, Licj;->b:Z

    if-eqz v6, :cond_31

    const-string v6, "NetworkLocator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "This location used GLS-fetched data: lastLocTime "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", glsQueryTime "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v8, v0, Lhjx;->r:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_31
    :goto_3f
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->n:Lhud;

    iget-object v3, v3, Lhud;->a:Lhtw;

    if-eqz v3, :cond_35

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->n:Lhud;

    iget-object v3, v3, Lhud;->a:Lhtw;

    iget-object v4, v11, Lhud;->a:Lhtw;

    iget-object v6, v3, Lhtw;->c:Lhug;

    iget-object v8, v4, Lhtw;->c:Lhug;

    invoke-static {v6, v8}, Liba;->b(Lhug;Lhug;)D

    move-result-wide v9

    const-wide v12, 0x408f400000000000L    # 1000.0

    mul-double/2addr v9, v12

    iget v6, v6, Lhug;->f:I

    int-to-double v12, v6

    sub-double/2addr v9, v12

    iget v6, v8, Lhug;->f:I

    int-to-double v12, v6

    sub-double v8, v9, v12

    iget-wide v12, v3, Lhtw;->e:J

    iget-wide v3, v4, Lhtw;->e:J

    sub-long v3, v12, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->abs(J)J

    move-result-wide v3

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpg-double v6, v8, v12

    if-ltz v6, :cond_32

    const-wide/16 v12, 0x0

    cmp-long v6, v3, v12

    if-nez v6, :cond_9a

    :cond_32
    const-wide/16 v3, 0x0

    :goto_40
    sget-boolean v6, Licj;->b:Z

    if-eqz v6, :cond_33

    const-string v6, "NetworkLocator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Speed: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_33
    const-wide v8, 0x407544a3d70a3d71L    # 340.29

    cmpl-double v6, v3, v8

    if-lez v6, :cond_35

    sget-boolean v6, Licj;->b:Z

    if-eqz v6, :cond_34

    const-string v6, "NetworkLocator"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Speed > Mach1: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_34
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->d:Lhtu;

    sget-object v4, Lhtv;->d:Lhtv;

    invoke-virtual {v3, v4}, Lhtu;->a(Lhtv;)V

    :cond_35
    move-object/from16 v0, p0

    iput-object v11, v0, Lhjx;->n:Lhud;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->b:Lhof;

    iget-object v4, v11, Lhud;->a:Lhtw;

    iput-object v4, v3, Lhof;->b:Lhtw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->d:Lhtu;

    sget-object v4, Lhtv;->b:Lhtv;

    invoke-virtual {v3, v4}, Lhtu;->a(Lhtv;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->n:Lhud;

    if-nez v5, :cond_9b

    const/4 v3, 0x0

    :goto_41
    invoke-interface {v4, v6, v3}, Lidu;->a(Lhud;Lhup;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    move-wide/from16 v0, p1

    invoke-interface {v3, v0, v1}, Lidu;->a(J)V

    :cond_36
    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->h:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->k:J

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-direct/range {p0 .. p0}, Lhjx;->b()Z

    move-result v5

    if-eqz v5, :cond_37

    if-nez v7, :cond_37

    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->v:J

    const-wide/16 v8, 0x1388

    add-long/2addr v5, v8

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    :cond_37
    const-wide v5, 0x7fffffffffffffffL

    cmp-long v5, v3, v5

    if-gez v5, :cond_38

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->a:Lidu;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lhjx;->l:Lilx;

    invoke-interface {v5, v6, v3, v4, v8}, Lidu;->a(IJLilx;)V

    :cond_38
    move-object/from16 v0, p0

    iget-wide v5, v0, Lhjx;->J:J

    sub-long v5, p1, v5

    const-wide/32 v8, 0x5265c0

    cmp-long v8, v5, v8

    if-gtz v8, :cond_39

    const-wide/32 v8, 0x2932e0

    cmp-long v5, v5, v8

    if-lez v5, :cond_3c

    sub-long v3, v3, p1

    const-wide/16 v5, 0x2710

    cmp-long v3, v3, v5

    if-lez v3, :cond_3c

    if-nez v24, :cond_3c

    if-nez v23, :cond_3c

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->q:Livi;

    if-nez v3, :cond_3c

    invoke-direct/range {p0 .. p0}, Lhjx;->b()Z

    move-result v3

    if-eqz v3, :cond_3c

    :cond_39
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_3a

    const-string v3, "NetworkLocator"

    const-string v4, "NlpState checkpointing..."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3a
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->b:Lhof;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-virtual {v3, v4}, Lhof;->b(Lidu;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->I:Libt;

    iget-object v3, v3, Libt;->a:Lhoc;

    invoke-virtual {v3}, Lhoc;->a()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->e:Lhoj;

    if-eqz v3, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->e:Lhoj;

    invoke-virtual {v3}, Lhoj;->c()V

    :try_start_0
    iget-object v4, v3, Lhoj;->g:Lidx;

    invoke-virtual {v3}, Lhoj;->a()Livi;

    move-result-object v3

    invoke-virtual {v4, v3}, Lidx;->b(Livi;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3b
    :goto_42
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->L:Lhjf;

    invoke-virtual {v3}, Lhjf;->a()V

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->J:J

    :cond_3c
    if-eqz v7, :cond_3d

    move-wide/from16 v0, p1

    move-object/from16 v2, p0

    iput-wide v0, v2, Lhjx;->w:J

    move-object/from16 v0, p0

    iget-wide v3, v0, Lhjx;->v:J

    const-wide/16 v5, 0x1

    sub-long v5, p1, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lhjx;->v:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->a:Lidu;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lidu;->b(I)V

    :cond_3d
    if-eqz v11, :cond_9c

    iget-object v3, v11, Lhud;->c:Lhtd;

    if-eqz v3, :cond_9c

    const/4 v3, 0x1

    move v4, v3

    :goto_43
    const/4 v3, 0x1

    if-eqz v24, :cond_9d

    if-nez v22, :cond_9d

    const/4 v3, 0x2

    :cond_3e
    :goto_44
    const/4 v4, 0x1

    if-eq v3, v4, :cond_3f

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lhjx;->a(I)V

    :cond_3f
    if-eqz v11, :cond_a4

    iget-object v3, v11, Lhud;->b:Lhuu;

    if-eqz v3, :cond_a4

    const/4 v3, 0x1

    move v4, v3

    :goto_45
    const/4 v3, 0x1

    if-eqz v23, :cond_a5

    if-nez v21, :cond_a5

    const/4 v3, 0x2

    :cond_40
    :goto_46
    const/4 v4, 0x1

    if-eq v3, v4, :cond_41

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lhjx;->b(I)V

    :cond_41
    if-eqz v11, :cond_43

    iget-object v3, v11, Lhud;->b:Lhuu;

    if-eqz v3, :cond_42

    iget-object v3, v11, Lhud;->b:Lhuu;

    iget-object v3, v3, Lhuu;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_42

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhjx;->D:Z

    if-eqz v3, :cond_42

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lhjx;->b(I)V

    :cond_42
    iget-object v3, v11, Lhud;->c:Lhtd;

    if-eqz v3, :cond_43

    iget-object v3, v11, Lhud;->c:Lhtd;

    iget-object v3, v3, Lhtd;->d:Lhty;

    sget-object v4, Lhty;->a:Lhty;

    if-ne v3, v4, :cond_43

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lhjx;->A:Z

    if-eqz v3, :cond_43

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lhjx;->a(I)V

    :cond_43
    return-void

    :cond_44
    const/4 v3, 0x0

    aget-object v3, p3, v3

    goto/16 :goto_0

    :cond_45
    const-wide/16 v7, 0x0

    goto/16 :goto_1

    :cond_46
    const-wide/16 v7, 0x0

    goto/16 :goto_2

    :cond_47
    const-wide/16 v7, 0x0

    goto/16 :goto_3

    :cond_48
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->H:Lhuv;

    iget-wide v3, v3, Lhuv;->a:J

    sub-long v3, p1, v3

    goto/16 :goto_4

    :cond_49
    const/4 v5, 0x0

    move v10, v5

    goto/16 :goto_5

    :cond_4a
    const/4 v5, 0x0

    move/from16 v31, v5

    goto/16 :goto_6

    :cond_4b
    const/4 v5, 0x0

    move/from16 v30, v5

    goto/16 :goto_7

    :cond_4c
    const/4 v5, 0x0

    goto/16 :goto_8

    :cond_4d
    const/4 v6, 0x0

    move/from16 v29, v6

    goto/16 :goto_9

    :cond_4e
    iget-object v6, v6, Lhud;->c:Lhtd;

    iget-object v6, v6, Lhtd;->a:Lhtf;

    goto/16 :goto_a

    :cond_4f
    const/4 v6, 0x0

    move/from16 v28, v6

    goto/16 :goto_b

    :cond_50
    iget-object v6, v6, Lhud;->b:Lhuu;

    iget-object v6, v6, Lhuu;->a:Lhuv;

    goto/16 :goto_c

    :cond_51
    const/4 v6, 0x0

    move/from16 v27, v6

    goto/16 :goto_d

    :cond_52
    const/4 v6, 0x0

    move/from16 v26, v6

    goto/16 :goto_e

    :cond_53
    const/4 v3, 0x0

    move/from16 v25, v3

    goto/16 :goto_f

    :cond_54
    const/4 v3, 0x0

    move/from16 v24, v3

    goto/16 :goto_10

    :cond_55
    const/4 v3, 0x0

    move/from16 v23, v3

    goto/16 :goto_11

    :cond_56
    const/4 v3, 0x0

    move/from16 v22, v3

    goto/16 :goto_12

    :cond_57
    const/4 v3, 0x0

    move/from16 v21, v3

    goto/16 :goto_13

    :cond_58
    const/4 v3, 0x0

    goto/16 :goto_14

    :cond_59
    const/4 v3, 0x0

    goto/16 :goto_15

    :cond_5a
    const/4 v3, 0x0

    move/from16 v20, v3

    goto/16 :goto_16

    :cond_5b
    move-object v4, v3

    goto/16 :goto_19

    :cond_5c
    invoke-interface {v4}, Lidr;->a()F

    move-result v3

    float-to-double v5, v3

    const-wide v7, 0x408f400000000000L    # 1000.0

    cmpg-double v5, v5, v7

    if-gez v5, :cond_5d

    const/high16 v3, 0x457a0000    # 4000.0f

    :goto_47
    new-instance v13, Lhug;

    invoke-interface {v4}, Lidr;->b()D

    move-result-wide v5

    const-wide v7, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-interface {v4}, Lidr;->c()D

    move-result-wide v6

    const-wide v8, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v6, v8

    double-to-int v4, v6

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v3, v6

    float-to-int v3, v3

    invoke-direct {v13, v5, v4, v3}, Lhug;-><init>(III)V

    goto/16 :goto_1a

    :cond_5d
    float-to-double v5, v3

    const-wide v7, 0x40c3880000000000L    # 10000.0

    cmpg-double v3, v5, v7

    if-gez v3, :cond_5e

    const v3, 0x47435000    # 50000.0f

    goto :goto_47

    :cond_5e
    const v3, 0x47c35000    # 100000.0f

    goto :goto_47

    :cond_5f
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_1b

    :cond_60
    sget-object v3, Lhjx;->O:Lhtg;

    move-object v5, v3

    goto/16 :goto_1c

    :cond_61
    const/4 v3, 0x0

    move-object v9, v3

    goto/16 :goto_1d

    :cond_62
    const/4 v3, 0x0

    invoke-virtual {v12, v8, v3, v14, v15}, Liax;->a(Lhtf;Ljava/util/Map;J)Lhno;

    move-result-object v3

    if-nez v3, :cond_64

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_63

    const-string v3, "CellLocator"

    const-string v4, "Primary cell miss in cache. Need server request."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_63
    new-instance v3, Lhtd;

    const/4 v4, 0x0

    sget-object v5, Lhty;->c:Lhty;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lhtd;-><init>(Lhug;Lhty;JLhtf;)V

    move-object v12, v3

    goto/16 :goto_1f

    :cond_64
    iget-object v3, v3, Lhno;->d:Ljava/lang/Object;

    check-cast v3, Lhug;

    invoke-virtual {v3}, Lhug;->b()Z

    move-result v3

    if-nez v3, :cond_66

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_65

    const-string v3, "CellLocator"

    const-string v4, "Primary cell is in cache with no location."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_65
    new-instance v3, Lhtd;

    const/4 v4, 0x0

    sget-object v5, Lhty;->b:Lhty;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lhtd;-><init>(Lhug;Lhty;JLhtf;)V

    move-object v12, v3

    goto/16 :goto_1f

    :cond_66
    if-nez v4, :cond_69

    const/4 v3, 0x0

    :goto_48
    new-instance v16, Libb;

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Libb;-><init>(I)V

    new-instance v4, Liay;

    move-object/from16 v0, v16

    invoke-direct {v4, v12, v14, v15, v0}, Liay;-><init>(Liax;JLibb;)V

    const-wide/16 v14, 0x7530

    sub-long v14, v6, v14

    iget-object v3, v5, Lhtg;->a:Lhtf;

    if-eqz v3, :cond_67

    iget-object v3, v5, Lhtg;->a:Lhtf;

    invoke-virtual {v4, v3}, Lhth;->a(Lhtf;)V

    :cond_67
    iget-object v3, v5, Lhtg;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_68
    :goto_49
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhtf;

    invoke-virtual {v3}, Lhtf;->f()J

    move-result-wide v17

    cmp-long v12, v17, v14

    if-lez v12, :cond_68

    invoke-virtual {v4, v3}, Lhth;->a(Lhtf;)V

    goto :goto_49

    :cond_69
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    goto :goto_48

    :cond_6a
    new-instance v4, Lhug;

    invoke-virtual/range {v16 .. v16}, Libb;->a()D

    move-result-wide v14

    const-wide v17, 0x416312d000000000L    # 1.0E7

    mul-double v14, v14, v17

    double-to-int v3, v14

    invoke-virtual/range {v16 .. v16}, Libb;->b()D

    move-result-wide v14

    const-wide v17, 0x416312d000000000L    # 1.0E7

    mul-double v14, v14, v17

    double-to-int v5, v14

    invoke-virtual/range {v16 .. v16}, Libb;->c()I

    move-result v12

    invoke-static {v12}, Liba;->a(I)I

    move-result v12

    move-object/from16 v0, v16

    iget v14, v0, Libb;->d:I

    invoke-direct {v4, v3, v5, v12, v14}, Lhug;-><init>(IIII)V

    invoke-static {v4}, Liba;->c(Lhug;)Z

    move-result v3

    if-eqz v3, :cond_6c

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_6b

    const-string v3, "CellLocator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v12, "Found cell location: "

    invoke-direct {v5, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6b
    new-instance v3, Lhtd;

    sget-object v5, Lhty;->a:Lhty;

    invoke-direct/range {v3 .. v8}, Lhtd;-><init>(Lhug;Lhty;JLhtf;)V

    move-object v12, v3

    goto/16 :goto_1f

    :cond_6c
    sget-boolean v3, Licj;->d:Z

    if-eqz v3, :cond_6d

    const-string v3, "CellLocator"

    const-string v4, "Cell location had non-sane values"

    invoke-static {v3, v4}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6d
    new-instance v3, Lhtd;

    const/4 v4, 0x0

    sget-object v5, Lhty;->b:Lhty;

    const-wide/16 v6, 0x0

    invoke-direct/range {v3 .. v8}, Lhtd;-><init>(Lhug;Lhty;JLhtf;)V

    move-object v12, v3

    goto/16 :goto_1f

    :cond_6e
    const/4 v3, 0x0

    move/from16 v19, v3

    goto/16 :goto_20

    :cond_6f
    move-object/from16 v0, p4

    iget-object v3, v0, Lhtj;->a:Lhuv;

    goto/16 :goto_22

    :cond_70
    const/4 v3, 0x0

    move v6, v3

    goto/16 :goto_23

    :cond_71
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_24

    :cond_72
    const/4 v3, 0x0

    goto/16 :goto_25

    :cond_73
    const/4 v4, 0x0

    goto/16 :goto_26

    :cond_74
    if-nez v3, :cond_75

    move-object v3, v12

    goto/16 :goto_27

    :cond_75
    if-eqz v4, :cond_7a

    move-object/from16 v0, v18

    iget-object v4, v0, Lhtw;->c:Lhug;

    iget-object v5, v12, Lhtw;->c:Lhug;

    invoke-static {v4, v5}, Liba;->a(Lhug;Lhug;)I

    move-result v3

    iget v7, v4, Lhug;->f:I

    iget v8, v5, Lhug;->f:I

    add-int/2addr v7, v8

    const v8, 0x3567e0

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    div-int/lit16 v7, v7, 0x3e8

    if-gt v3, v7, :cond_76

    const/4 v3, 0x1

    :goto_4a
    if-eqz v3, :cond_78

    iget v3, v4, Lhug;->f:I

    iget v4, v5, Lhug;->f:I

    if-le v3, v4, :cond_77

    const/4 v3, 0x1

    :goto_4b
    if-eqz v3, :cond_7a

    move-object v3, v12

    goto/16 :goto_27

    :cond_76
    const/4 v3, 0x0

    goto :goto_4a

    :cond_77
    const/4 v3, 0x0

    goto :goto_4b

    :cond_78
    iget v3, v4, Lhug;->g:I

    iget v4, v5, Lhug;->g:I

    if-ge v3, v4, :cond_79

    const/4 v3, 0x1

    goto :goto_4b

    :cond_79
    const/4 v3, 0x0

    goto :goto_4b

    :cond_7a
    move-object/from16 v3, v18

    goto/16 :goto_27

    :cond_7b
    if-nez v13, :cond_7c

    const/4 v3, 0x1

    :goto_4c
    move v15, v13

    move/from16 v16, v3

    move/from16 v17, v5

    goto/16 :goto_29

    :cond_7c
    const/4 v3, 0x0

    goto :goto_4c

    :cond_7d
    if-eqz v16, :cond_7e

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "uses_wifi"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_2a

    :cond_7e
    if-eqz v15, :cond_7f

    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "uses_indoor"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_2a

    :cond_7f
    move-object/from16 v0, v35

    iget-object v3, v0, Libc;->c:Lidu;

    const-string v4, "nlp"

    const-string v5, "locType"

    const-string v6, "no_location"

    const-wide/16 v7, 0x1

    const/4 v9, 0x1

    invoke-interface/range {v3 .. v9}, Lidu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZ)V

    goto/16 :goto_2a

    :cond_80
    const/4 v8, 0x0

    goto/16 :goto_2b

    :cond_81
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_2d

    :cond_82
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_2e

    :cond_83
    const/4 v3, 0x0

    goto/16 :goto_2f

    :cond_84
    const/4 v3, 0x0

    move v9, v3

    goto/16 :goto_30

    :cond_85
    const/4 v3, 0x0

    move v8, v3

    goto/16 :goto_31

    :cond_86
    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_87

    const-string v3, "NetworkLocator"

    const-string v4, "Not reporting first fix as it may be significantly improved by going to the server."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_87
    const/4 v3, 0x0

    goto/16 :goto_32

    :cond_88
    iget-wide v12, v3, Lhtw;->e:J

    invoke-interface {v4}, Lidr;->f()J

    move-result-wide v14

    sub-long/2addr v12, v14

    const-wide/32 v14, 0xdbba0

    add-long/2addr v5, v14

    cmp-long v5, v12, v5

    if-ltz v5, :cond_8a

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_89

    const-string v3, "NetworkLocator"

    const-string v4, "New location significantly newer than previous. Reporting it."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_89
    const/4 v3, 0x1

    goto/16 :goto_32

    :cond_8a
    iget-object v3, v3, Lhtw;->c:Lhug;

    iget v3, v3, Lhug;->f:I

    int-to-float v3, v3

    invoke-interface {v4}, Lidr;->a()F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v4, v5

    const v5, 0x47c35000    # 100000.0f

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_8c

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_8b

    const-string v3, "NetworkLocator"

    const-string v4, "Not reporting location since the new location has worse accuracy than the previous one."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8b
    const/4 v3, 0x0

    goto/16 :goto_32

    :cond_8c
    const/4 v3, 0x1

    goto/16 :goto_32

    :cond_8d
    const/4 v3, 0x0

    goto/16 :goto_33

    :cond_8e
    const/4 v4, 0x0

    goto/16 :goto_34

    :cond_8f
    const/4 v5, 0x0

    goto/16 :goto_35

    :cond_90
    const/4 v5, 0x0

    goto/16 :goto_36

    :cond_91
    const/4 v5, 0x0

    move v7, v5

    goto/16 :goto_37

    :cond_92
    const/4 v5, 0x0

    goto/16 :goto_38

    :cond_93
    const/4 v6, 0x0

    goto/16 :goto_39

    :cond_94
    const/4 v6, 0x0

    goto/16 :goto_3a

    :cond_95
    const/4 v6, 0x0

    goto/16 :goto_3b

    :cond_96
    const/4 v4, 0x0

    move-object v5, v4

    goto/16 :goto_3c

    :cond_97
    const/4 v4, 0x0

    goto/16 :goto_3d

    :cond_98
    const/4 v3, 0x0

    goto/16 :goto_3e

    :cond_99
    move-object/from16 v0, p0

    iget-object v3, v0, Lhjx;->d:Lhtu;

    sget-object v4, Lhtv;->c:Lhtv;

    invoke-virtual {v3, v4}, Lhtu;->a(Lhtv;)V

    goto/16 :goto_3f

    :cond_9a
    long-to-double v3, v3

    div-double v3, v8, v3

    goto/16 :goto_40

    :cond_9b
    iget-object v3, v5, Lhlt;->a:Lhup;

    goto/16 :goto_41

    :catch_0
    move-exception v3

    sget-boolean v3, Licj;->b:Z

    if-eqz v3, :cond_3b

    const-string v3, "SeenDevicesCache"

    const-string v4, "Unable to save data to disk."

    invoke-static {v3, v4}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_42

    :cond_9c
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_43

    :cond_9d
    if-eqz v20, :cond_9e

    if-eqz v28, :cond_9e

    if-eqz v4, :cond_9f

    iget-object v5, v11, Lhud;->c:Lhtd;

    iget-object v5, v5, Lhtd;->d:Lhty;

    sget-object v6, Lhty;->b:Lhty;

    if-eq v5, v6, :cond_9f

    :cond_9e
    if-eqz v34, :cond_a0

    invoke-virtual/range {v34 .. v34}, Lhtf;->f()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-wide v7, v0, Lhjx;->y:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_a0

    move-object/from16 v0, p0

    iget v5, v0, Lhjx;->z:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_a0

    :cond_9f
    const/4 v3, 0x4

    goto/16 :goto_44

    :cond_a0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lhjx;->A:Z

    if-nez v5, :cond_a1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lhjx;->B:Z

    if-eqz v5, :cond_a1

    const/4 v3, 0x6

    goto/16 :goto_44

    :cond_a1
    if-eqz v20, :cond_a3

    if-eqz p5, :cond_a3

    if-eqz v4, :cond_a2

    iget-object v5, v11, Lhud;->c:Lhtd;

    iget-object v5, v5, Lhtd;->d:Lhty;

    sget-object v6, Lhty;->a:Lhty;

    if-eq v5, v6, :cond_a3

    :cond_a2
    const/4 v3, 0x3

    goto/16 :goto_44

    :cond_a3
    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->N:Lhji;

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Lhji;->a(J)Z

    move-result v5

    if-nez v5, :cond_3e

    if-eqz v4, :cond_3e

    iget-object v4, v11, Lhud;->c:Lhtd;

    iget-object v4, v4, Lhtd;->d:Lhty;

    sget-object v5, Lhty;->c:Lhty;

    if-ne v4, v5, :cond_3e

    const/4 v3, 0x5

    goto/16 :goto_44

    :cond_a4
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_45

    :cond_a5
    if-eqz v20, :cond_a7

    if-eqz v27, :cond_a7

    if-eqz v4, :cond_a6

    iget-object v5, v11, Lhud;->b:Lhuu;

    iget-object v5, v5, Lhuu;->d:Lhty;

    sget-object v6, Lhty;->b:Lhty;

    if-ne v5, v6, :cond_a7

    :cond_a6
    const/4 v3, 0x4

    goto/16 :goto_46

    :cond_a7
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lhjx;->D:Z

    if-nez v5, :cond_a8

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lhjx;->E:Z

    if-eqz v5, :cond_a8

    const/4 v3, 0x6

    goto/16 :goto_46

    :cond_a8
    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->N:Lhji;

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Lhji;->a(J)Z

    move-result v5

    if-nez v5, :cond_a9

    if-eqz v4, :cond_a9

    iget-object v5, v11, Lhud;->b:Lhuu;

    iget-object v5, v5, Lhuu;->d:Lhty;

    sget-object v6, Lhty;->c:Lhty;

    if-ne v5, v6, :cond_a9

    const/4 v3, 0x5

    goto/16 :goto_46

    :cond_a9
    if-eqz v20, :cond_40

    if-eqz p5, :cond_40

    move-object/from16 v0, p0

    iget-object v5, v0, Lhjx;->t:Lhuv;

    if-eqz v5, :cond_40

    if-eqz v4, :cond_aa

    iget-object v4, v11, Lhud;->b:Lhuu;

    iget-object v4, v4, Lhuu;->d:Lhty;

    sget-object v5, Lhty;->a:Lhty;

    if-eq v4, v5, :cond_40

    :cond_aa
    const/4 v3, 0x3

    goto/16 :goto_46

    :cond_ab
    move v15, v3

    move/from16 v16, v4

    move/from16 v17, v5

    goto/16 :goto_29

    :cond_ac
    move-object v14, v3

    goto/16 :goto_28

    :cond_ad
    move-object v4, v13

    goto/16 :goto_21

    :cond_ae
    move-object v4, v3

    goto/16 :goto_1e

    :cond_af
    move-object v3, v4

    goto/16 :goto_18

    :cond_b0
    move-object v4, v5

    goto/16 :goto_17

    :cond_b1
    move-object v11, v3

    goto/16 :goto_2c
.end method

.method private b(I)V
    .locals 3

    iget v0, p0, Lhjx;->F:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lhjx;->F:I

    iget-object v0, p0, Lhjx;->a:Lidu;

    iget v1, p0, Lhjx;->F:I

    iget v2, p0, Lhjx;->C:I

    invoke-interface {v0, v1, v2}, Lidu;->a(II)V

    :cond_0
    return-void
.end method

.method private b()Z
    .locals 4

    iget-wide v0, p0, Lhjx;->v:J

    iget-wide v2, p0, Lhjx;->w:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 7

    const/4 v3, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    return-void
.end method

.method public final a(IIIZLilx;)V
    .locals 8

    int-to-long v0, p1

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lhjx;->g:J

    int-to-long v0, p2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lhjx;->i:J

    int-to-long v0, p3

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lhjx;->j:J

    iput-object p5, p0, Lhjx;->l:Lilx;

    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    iget-boolean v0, p0, Lhjx;->A:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhjx;->D:Z

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    const v0, 0x7fffffff

    if-ge p1, v0, :cond_1

    if-eqz p4, :cond_4

    const-wide/16 v3, 0x0

    :goto_1
    iget-wide v5, p0, Lhjx;->h:J

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lhjx;->h:J

    :cond_1
    iget-boolean v0, p0, Lhjx;->A:Z

    if-eqz v0, :cond_2

    const v0, 0x7fffffff

    if-ge p2, v0, :cond_2

    if-eqz p4, :cond_5

    const-wide/16 v3, 0x0

    :goto_2
    iget-wide v5, p0, Lhjx;->k:J

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lhjx;->k:J

    :cond_2
    iget-object v3, p0, Lhjx;->I:Libt;

    iget-wide v4, p0, Lhjx;->g:J

    const-wide/16 v6, 0x4e20

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    const/4 v0, 0x1

    :goto_3
    iput-boolean v0, v3, Libt;->b:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-wide v3, p0, Lhjx;->g:J

    add-long/2addr v3, v1

    goto :goto_1

    :cond_5
    iget-wide v3, p0, Lhjx;->i:J

    add-long/2addr v3, v1

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(Lhtf;)V
    .locals 10

    const/4 v3, 0x0

    const/4 v5, 0x0

    if-nez p1, :cond_1

    sget-boolean v0, Licj;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "NetworkLocator"

    const-string v1, "null cell state delivered"

    invoke-static {v0, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lhjx;->y:J

    :goto_0
    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    move-object v0, p0

    move-object v4, v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    return-void

    :cond_1
    iget-object v1, p0, Lhjx;->x:Lhtg;

    if-eqz p1, :cond_6

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Lhtf;->f()J

    move-result-wide v6

    iget-wide v8, v1, Lhtg;->c:J

    sub-long/2addr v6, v8

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtf;

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lhtf;->f()J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-gez v0, :cond_3

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtf;

    goto :goto_1

    :cond_2
    move-object v0, v3

    goto :goto_1

    :cond_3
    iget-object v0, v1, Lhtg;->a:Lhtf;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lhtg;->a:Lhtf;

    invoke-virtual {v0}, Lhtf;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lhtg;->a:Lhtf;

    invoke-virtual {v0, p1}, Lhtf;->b(Lhtf;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x4

    if-lt v0, v2, :cond_4

    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    iget-object v0, v1, Lhtg;->b:Ljava/util/List;

    iget-object v2, v1, Lhtg;->a:Lhtf;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iput-object p1, v1, Lhtg;->a:Lhtf;

    :cond_6
    invoke-virtual {p1}, Lhtf;->i()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lhjx;->z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhjx;->z:I

    goto/16 :goto_0

    :cond_7
    iput v5, p0, Lhjx;->z:I

    goto/16 :goto_0
.end method

.method public final a(Lidr;)V
    .locals 0

    iput-object p1, p0, Lhjx;->Q:Lidr;

    return-void
.end method

.method public final a(Livi;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lhjx;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lhjx;->t:Lhuv;

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2, v3}, Lhtj;->a(Livi;Lhuv;J)Lhtj;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lhjx;->b:Lhof;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lhjx;->a:Lidu;

    invoke-interface {v6}, Lidu;->j()Licm;

    move-result-object v6

    invoke-interface {v6}, Licm;->b()J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v4, v6, v7}, Lhof;->a(Livi;ZJ)V

    move-object/from16 v0, p0

    iput-wide v2, v0, Lhjx;->s:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lhjx;->I:Libt;

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->a:Lidu;

    invoke-interface {v4}, Lidu;->j()Licm;

    move-result-object v4

    invoke-interface {v4}, Licm;->b()J

    move-result-wide v7

    sget-boolean v4, Licj;->c:Z

    if-eqz v4, :cond_0

    const-string v4, "ModelStateManager"

    const-string v6, "Received GLS response (maybe update mac to cluster mappings)..."

    invoke-static {v4, v6}, Lilz;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v9, v1, Libt;->a:Lhoc;

    invoke-static/range {p1 .. p1}, Lhoc;->a(Livi;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Livi;->k(I)I

    move-result v10

    const/4 v1, 0x0

    move v6, v1

    :goto_0
    if-ge v6, v10, :cond_4

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, Livi;->c(II)Livi;

    move-result-object v1

    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Livi;->f(I)Livi;

    move-result-object v11

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_1
    const/4 v4, 0x1

    invoke-virtual {v11, v4}, Livi;->k(I)I

    move-result v4

    if-ge v1, v4, :cond_2

    const/4 v4, 0x1

    invoke-virtual {v11, v4, v1}, Livi;->c(II)Livi;

    move-result-object v13

    const/4 v4, 0x1

    invoke-virtual {v13, v4}, Livi;->g(I)Ljava/lang/String;

    move-result-object v14

    const/4 v4, 0x0

    :goto_2
    const/4 v15, 0x2

    invoke-virtual {v13, v15}, Livi;->k(I)I

    move-result v15

    if-ge v4, v15, :cond_1

    const/4 v15, 0x2

    invoke-virtual {v13, v15, v4}, Livi;->b(II)J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v12, v15, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    iget-object v11, v9, Lhoc;->a:Lhnz;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v11, v12, v1, v7, v8}, Lhnz;->a(Ljava/lang/Object;Ljava/lang/Object;J)V

    goto :goto_3

    :cond_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lhjx;->N:Lhji;

    if-eqz p1, :cond_6

    const/4 v1, 0x1

    :goto_4
    if-eqz v1, :cond_7

    invoke-virtual {v4}, Lhji;->a()V

    const-wide/16 v1, 0x0

    iput-wide v1, v4, Lhji;->b:J

    :cond_5
    :goto_5
    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_4

    :cond_7
    iput-wide v2, v4, Lhji;->b:J

    iget-boolean v1, v4, Lhji;->e:Z

    if-eqz v1, :cond_a

    iget-boolean v1, v4, Lhji;->c:Z

    if-nez v1, :cond_5

    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v5, 0x3

    if-le v1, v5, :cond_8

    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_8
    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v5, 0x3

    if-lt v1, v5, :cond_5

    iget-object v1, v4, Lhji;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long v1, v2, v5

    const-wide/32 v5, 0x927c0

    cmp-long v1, v1, v5

    if-gez v1, :cond_5

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_9

    const-string v1, "GlsFailureTracker"

    const-string v2, "Entering back-off mode."

    invoke-static {v1, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    const/4 v1, 0x1

    iput-boolean v1, v4, Lhji;->c:Z

    goto :goto_5

    :cond_a
    invoke-virtual {v4}, Lhji;->a()V

    goto :goto_5
.end method

.method public final a(Z)V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjx;->B:Z

    iget-object v1, p0, Lhjx;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->a()J

    move-result-wide v1

    if-nez p1, :cond_0

    :goto_0
    iget-boolean v3, p0, Lhjx;->D:Z

    invoke-direct {p0, v1, v2, v0, v3}, Lhjx;->a(JZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ZZI)V
    .locals 2

    iget-object v0, p0, Lhjx;->N:Lhji;

    iget-boolean v1, v0, Lhji;->e:Z

    if-eqz v1, :cond_0

    iget v1, v0, Lhji;->f:I

    if-eq v1, p3, :cond_1

    :cond_0
    invoke-virtual {v0}, Lhji;->a()V

    :cond_1
    iput-boolean p1, v0, Lhji;->e:Z

    iput-boolean p2, v0, Lhji;->d:Z

    if-eqz p1, :cond_2

    iput p3, v0, Lhji;->f:I

    :cond_2
    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_3

    const-string v1, "GlsFailureTracker"

    invoke-virtual {v0}, Lhji;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final a([Lhuv;)V
    .locals 7

    const/4 v5, 0x0

    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    aget-object v0, p1, v5

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    goto :goto_0
.end method

.method public final b(Livi;)V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lhjx;->I:Libt;

    iget-object v1, p0, Lhjx;->a:Lidu;

    invoke-interface {v1}, Lidu;->j()Licm;

    move-result-object v1

    invoke-interface {v1}, Licm;->b()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Libt;->a(Livi;J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v1

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lhjx;->a(J[Lhuv;Lhtj;ZZ)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhjx;->E:Z

    iget-object v0, p0, Lhjx;->a:Lidu;

    invoke-interface {v0}, Lidu;->j()Licm;

    move-result-object v0

    invoke-interface {v0}, Licm;->a()J

    move-result-wide v0

    iget-boolean v2, p0, Lhjx;->A:Z

    invoke-direct {p0, v0, v1, v2, p1}, Lhjx;->a(JZZ)V

    return-void
.end method
