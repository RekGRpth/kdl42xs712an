.class public final Lbve;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcfz;

.field private final b:Lcby;

.field private final c:Lcmn;

.field private final d:Lbst;

.field private final e:Lcdu;

.field private final f:Lcon;


# direct methods
.method public constructor <init>(Lcfz;Lcby;Lcmn;Lbst;Lcdu;Lcon;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfz;

    iput-object v0, p0, Lbve;->a:Lcfz;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcby;

    iput-object v0, p0, Lbve;->b:Lcby;

    iput-object p3, p0, Lbve;->c:Lcmn;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbst;

    iput-object v0, p0, Lbve;->d:Lbst;

    iput-object p5, p0, Lbve;->e:Lcdu;

    iput-object p6, p0, Lbve;->f:Lcon;

    return-void
.end method

.method private static a(Lcfn;)Ljava/io/InputStream;
    .locals 4

    iget-object v0, p0, Lcfn;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfn;->a:Ljava/io/File;

    :goto_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v0, p0, Lcfn;->c:Ljavax/crypto/SecretKey;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcfn;->b:Ljava/io/File;

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-interface {v0}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    new-instance v0, Ljavax/crypto/CipherInputStream;

    invoke-direct {v0, v1, v2}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lbud;

    invoke-direct {v1, v0}, Lbud;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lbud;

    invoke-direct {v1, v0}, Lbud;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lbud;

    invoke-direct {v1, v0}, Lbud;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/io/File;)V
    .locals 6

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v3}, Lbve;->a(Ljava/io/File;)V

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbve;->a:Lcfz;

    invoke-interface {v5, v4}, Lcfz;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private b()V
    .locals 16

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->b()Lcgs;

    move-result-object v13

    :try_start_0
    invoke-interface {v13}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcfn;

    move-object v9, v0

    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->a:Lcfz;

    invoke-interface {v1, v9}, Lcfz;->b(Lcfn;)Lcfp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    if-eqz v7, :cond_8

    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->d:Lbst;

    invoke-virtual {v1}, Lbst;->a()Lbsy;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lbud; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    :try_start_2
    invoke-static {v9}, Lbve;->a(Lcfn;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lbud; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v12

    :try_start_3
    invoke-virtual {v10}, Lbsy;->b()Lbsx;

    move-result-object v1

    invoke-static {v12, v1}, Lbpp;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    new-instance v1, Lbsz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbve;->e:Lcdu;

    iget-object v3, v9, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbve;->f:Lcon;

    invoke-interface {v4}, Lcon;->a()J

    move-result-wide v4

    invoke-virtual {v7}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lbsz;-><init>(Lcdu;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v10, v1}, Lbsy;->a(Lbsu;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcge;

    move-object v5, v0

    invoke-virtual {v7}, Lcfp;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v7}, Lcjc;->a(Lcfp;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lbve;->c:Lcmn;

    new-instance v1, Lcmf;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbve;->a:Lcfz;

    invoke-virtual {v7}, Lcfp;->c()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcfz;->a(J)Lcfc;

    move-result-object v2

    iget-object v3, v9, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v4, v5, Lcfl;->f:J

    invoke-virtual {v7}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcmf;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;JLcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/database/data/EntrySpec;)V

    invoke-virtual {v8, v1}, Lcmn;->a(Lcml;)Z

    :goto_1
    const-string v1, "LegacyContentMigrator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Migrated legacy content to new storage system: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lbud; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    if-eqz v10, :cond_1

    :try_start_4
    invoke-virtual {v10}, Lbsy;->c()V

    :cond_1
    if-eqz v12, :cond_2

    invoke-static {v12}, Lbpm;->a(Ljava/io/Closeable;)V

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->a:Lcfz;

    invoke-interface {v1, v9}, Lcfz;->a(Lcfn;)V

    invoke-virtual {v9}, Lcfn;->l()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lbve;->a:Lcfz;

    invoke-interface {v1, v9}, Lcfz;->c(Lcfn;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v13}, Lcgs;->close()V

    throw v1

    :cond_3
    :try_start_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lbve;->c:Lcmn;

    new-instance v1, Lclz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbve;->a:Lcfz;

    invoke-virtual {v7}, Lcfp;->c()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcfz;->a(J)Lcfc;

    move-result-object v2

    iget-object v3, v9, Lcfn;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v7}, Lcfp;->a()Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    iget-object v5, v5, Lcge;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    invoke-direct/range {v1 .. v8}, Lclz;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V

    invoke-virtual {v15, v1}, Lcmn;->a(Lcml;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lbud; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v2, v10

    move-object v3, v12

    :goto_3
    :try_start_6
    const-string v4, "LegacyContentMigrator"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unable to migrate legacy content: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v1, v5}, Lcbv;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    if-eqz v2, :cond_4

    :try_start_7
    invoke-virtual {v2}, Lbsy;->c()V

    :cond_4
    if-eqz v3, :cond_2

    invoke-static {v3}, Lbpm;->a(Ljava/io/Closeable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v1

    move-object v10, v11

    move-object v12, v11

    :goto_4
    :try_start_8
    const-string v2, "LegacyContentMigrator"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to migrate legacy content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcbv;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    if-eqz v10, :cond_5

    :try_start_9
    invoke-virtual {v10}, Lbsy;->c()V

    :cond_5
    if-eqz v12, :cond_2

    invoke-static {v12}, Lbpm;->a(Ljava/io/Closeable;)V

    goto/16 :goto_2

    :catchall_1
    move-exception v1

    move-object v10, v11

    move-object v12, v11

    :goto_5
    if-eqz v10, :cond_6

    invoke-virtual {v10}, Lbsy;->c()V

    :cond_6
    if-eqz v12, :cond_7

    invoke-static {v12}, Lbpm;->a(Ljava/io/Closeable;)V

    :cond_7
    throw v1

    :cond_8
    const-string v1, "LegacyContentMigrator"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Discarding legacy pending upload with no reference: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2

    :cond_9
    invoke-interface {v13}, Lcgs;->close()V

    return-void

    :catchall_2
    move-exception v1

    move-object v12, v11

    goto :goto_5

    :catchall_3
    move-exception v1

    goto :goto_5

    :catchall_4
    move-exception v1

    move-object v10, v2

    move-object v12, v3

    goto :goto_5

    :catch_2
    move-exception v1

    move-object v12, v11

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    move-object v2, v11

    move-object v3, v11

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v2, v10

    move-object v3, v11

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    monitor-enter p0

    :try_start_0
    const-string v0, "LegacyContentMigrator"

    const-string v1, "Migrating any legacy content..."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbve;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->b()Lcgs;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    :try_start_1
    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfn;

    iget-object v3, p0, Lbve;->a:Lcfz;

    invoke-interface {v3}, Lcfz;->r()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lbve;->a:Lcfz;

    invoke-interface {v3, v0}, Lcfz;->a(Lcfn;)V

    invoke-virtual {v0}, Lcfn;->l()V

    const-string v3, "LegacyContentMigrator"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Discarding legacy cached content: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Lcgs;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "LegacyContentMigrator"

    const-string v2, "Content migration failed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    :try_start_4
    invoke-interface {v1}, Lcgs;->close()V

    invoke-direct {p0}, Lbve;->b()V

    iget-object v0, p0, Lbve;->b:Lcby;

    invoke-interface {v0}, Lcby;->a()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lbve;->a(Ljava/io/File;)V

    iget-object v0, p0, Lbve;->b:Lcby;

    invoke-interface {v0}, Lcby;->b()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lbve;->a(Ljava/io/File;)V

    const-string v0, "LegacyContentMigrator"

    const-string v1, "Content migration complete."

    invoke-static {v0, v1}, Lcbv;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
