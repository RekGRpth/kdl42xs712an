.class public final Lhvw;
.super Lilf;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Received Gps Location"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Received Wifi Location"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Received Cell Location"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Received Fused Location"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Received Unknown Location"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Gps Started 0 <= interval < 1min"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Gps Started 1min <= interval < 5min"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Gps Started interval >= 5min"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Gps Stopped"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Nlp Started 0 <= interval < 1min"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Nlp Started 1min <= interval < 5min"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "Nlp Started interval >= 5min"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Nlp Stopped"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Low Power Nlp Started 0 <= interval < 1min"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Low Power Nlp Started 1min <= interval < 5min"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Low Power Nlp Started interval >= 5min"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Low Power Nlp Stopped"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Network Location Setting Enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Network Location Setting Disabled"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Gps Setting Enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "Gps Setting Disabled"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "State Changed To Default"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "State Changed To Wifi"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "State Changed To Gps"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "State Changed To Gps In Vehicle"

    aput-object v2, v0, v1

    sput-object v0, Lhvw;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x190

    invoke-direct {p0, v0}, Lilf;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0, p1}, Lhvw;->c(I)B

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lhvw;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lhvw;->e(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lhvw;->e(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lhvw;->a:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final a(J)V
    .locals 3

    const/4 v0, 0x7

    const-wide/32 v1, 0xea60

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    const/4 v0, 0x5

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :cond_1
    const-wide/32 v1, 0x493e0

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const/16 v0, 0x11

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :cond_0
    const/16 v0, 0x12

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    const/4 v0, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final b(I)V
    .locals 3

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :pswitch_0
    const/16 v0, 0x15

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x16

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x17

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x18

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(J)V
    .locals 3

    const/16 v0, 0xb

    const-wide/32 v1, 0xea60

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    const/16 v0, 0x9

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :cond_1
    const-wide/32 v1, 0x493e0

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    const/16 v0, 0xa

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    if-eqz p1, :cond_0

    const/16 v0, 0x13

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :cond_0
    const/16 v0, 0x14

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    const/4 v0, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final c(J)V
    .locals 3

    const/16 v0, 0xf

    const-wide/32 v1, 0xea60

    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    const/16 v0, 0xd

    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void

    :cond_1
    const-wide/32 v1, 0x493e0

    cmp-long v1, p1, v1

    if-gez v1, :cond_0

    const/16 v0, 0xe

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    const/4 v0, 0x4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final e()V
    .locals 3

    const/16 v0, 0x8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final f()V
    .locals 3

    const/16 v0, 0xc

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method

.method public final g()V
    .locals 3

    const/16 v0, 0x10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lhvw;->a(BJ)V

    return-void
.end method
