.class public final Lgwf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 0

    iput-object p1, p0, Lgwf;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    iget-object v0, p0, Lgwf;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lgwf;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgth;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method
