.class final Lifx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liza;


# instance fields
.field public a:Ljava/util/List;

.field public b:Ljava/util/List;

.field public c:Ljava/lang/Exception;

.field public d:Lizb;

.field final synthetic e:Lifw;


# direct methods
.method constructor <init>(Lifw;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 2

    const/4 v1, 0x1

    iput-object p1, p0, Lifx;->e:Lifw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lifx;->a:Ljava/util/List;

    iget-object v0, p0, Lifx;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lifx;->b:Ljava/util/List;

    iget-object v0, p0, Lifx;->b:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final a(Liyz;Lizb;)V
    .locals 2

    iput-object p2, p0, Lifx;->d:Lizb;

    iget-object v0, p0, Lifx;->e:Lifw;

    invoke-static {v0}, Lifw;->a(Lifw;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final a(Liyz;Ljava/lang/Exception;)V
    .locals 2

    iput-object p2, p0, Lifx;->c:Ljava/lang/Exception;

    invoke-static {}, Lifw;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lifw;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "masf request failed"

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    iget-object v0, p0, Lifx;->e:Lifw;

    invoke-static {v0}, Lifw;->a(Lifw;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
