.class final Lhyz;
.super Lhyy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lhyt;


# direct methods
.method public constructor <init>(Lhyt;Lhys;)V
    .locals 0

    iput-object p1, p0, Lhyz;->a:Lhyt;

    invoke-direct {p0, p1, p2}, Lhyy;-><init>(Lhyt;Lhys;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "DisabledState"

    return-object v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 6

    const/16 v5, 0x3e8

    const/4 v1, 0x1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofencerStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "processMessage, current state=DisabledState msg="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lhyz;->a:Lhyt;

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Lhyt;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lhyb;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, p1}, Lhyz;->b(Landroid/os/Message;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lhyz;->a:Lhyt;

    invoke-static {v0}, Lhyt;->j(Lhyt;)Lhzn;

    move-result-object v0

    invoke-virtual {v0}, Lhzn;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GeofencerStateMachine"

    const-string v2, "Network location enabled."

    invoke-static {v0, v2}, Lhyb;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhyz;->a:Lhyt;

    iget-object v2, p0, Lhyz;->a:Lhyt;

    invoke-static {v2}, Lhyt;->m(Lhyt;)Lhzc;

    move-result-object v2

    invoke-static {v0, v2}, Lhyt;->d(Lhyt;Lbqg;)V

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    const-string v0, "GeofencerStateMachine"

    const-string v2, "Ignoring addGeofence because network location is disabled."

    invoke-static {v0, v2}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhyk;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhyk;->a(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :pswitch_3
    const-string v0, "GeofencerStateMachine"

    const-string v2, "Ignoring removeGeofence because network location is disabled."

    invoke-static {v0, v2}, Lhyb;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhzv;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhzv;->a(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    invoke-super {p0}, Lhyy;->b()V

    iget-object v0, p0, Lhyz;->c:Lhys;

    iget-object v1, v0, Lhys;->g:Lhzk;

    invoke-virtual {v1}, Lhzk;->a()V

    iget-object v1, v0, Lhys;->f:Lhyi;

    invoke-virtual {v1}, Lhyi;->a()V

    iget-object v0, v0, Lhys;->h:Lhxq;

    invoke-virtual {v0}, Lhxq;->b()V

    return-void
.end method
