.class public final Lgzz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgzx;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/ia/CartDetailsView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "price"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "start_time"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lgzz;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lgzz;->b:Landroid/content/Context;

    iput-object p1, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    return-void
.end method

.method private static a(I)I
    .locals 3

    packed-switch p0, :pswitch_data_0

    const-string v0, "DefaultCartDetailsRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown recurrence frequency units: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f0b0141    # com.google.android.gms.R.string.wallet_subscription

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0b013e    # com.google.android.gms.R.string.wallet_daily_subscription

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b013f    # com.google.android.gms.R.string.wallet_monthly_subscription

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0140    # com.google.android.gms.R.string.wallet_yearly_subscription

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    const v1, 0x7f040145    # com.google.android.gms.R.layout.wallet_row_line_item_default_long

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a034c    # com.google.android.gms.R.id.item_details

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a034e    # com.google.android.gms.R.id.item_price

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f0a034d    # com.google.android.gms.R.id.item_quantity

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 8

    new-instance v1, Ljava/util/HashMap;

    sget-object v0, Lgzz;->a:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    sget-object v2, Lgzz;->a:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    iget-object v6, p0, Lgzz;->b:Landroid/content/Context;

    const v7, 0x7f100036    # com.google.android.gms.R.style.WalletImportantPurchaseDetailText

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p1, v1}, Lgtp;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Liob;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c()V

    move-object/from16 v0, p1

    iget-object v1, v0, Liob;->b:[Liod;

    array-length v1, v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget-object v1, v0, Liob;->c:Lioh;

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a()V

    :cond_2
    move-object/from16 v0, p1

    iget-object v6, v0, Liob;->b:[Liod;

    array-length v7, v6

    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, v7, :cond_1d

    aget-object v8, v6, v4

    iget-object v1, v8, Liod;->f:Liop;

    if-eqz v1, :cond_19

    if-eqz v5, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v8, Liod;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lgzz;->b:Landroid/content/Context;

    iget-object v3, v8, Liod;->f:Liop;

    iget v3, v3, Liop;->b:I

    invoke-static {v3}, Lgzz;->a(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lgzz;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v1, v8, Liod;->a:Ljava/lang/String;

    iget-object v2, v8, Liod;->g:Lioh;

    invoke-static {v2}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lgzz;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    const v2, 0x7f040147    # com.google.android.gms.R.layout.wallet_row_line_item_subscription_long

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a034f    # com.google.android.gms.R.id.subscription_item_details

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v2, v8, Liod;->f:Liop;

    iget v3, v2, Liop;->b:I

    iget v2, v2, Liop;->a:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lgzz;->b:Landroid/content/Context;

    const/4 v11, 0x1

    if-ne v2, v11, :cond_b

    invoke-static {v3}, Lgzz;->a(I)I

    move-result v2

    :goto_3
    invoke-virtual {v10, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgzz;->b:Landroid/content/Context;

    const v11, 0x7f100036    # com.google.android.gms.R.style.WalletImportantPurchaseDetailText

    invoke-direct {v3, v10, v11}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v10, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v11, 0x21

    invoke-virtual {v9, v3, v10, v2, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget v2, v8, Liod;->h:I

    if-eqz v2, :cond_4

    iget v2, v8, Liod;->h:I

    packed-switch v2, :pswitch_data_0

    const-string v3, "DefaultCartDetailsRenderer"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unknown initial payment type: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_4
    if-lez v2, :cond_4

    const-string v3, " - "

    invoke-virtual {v9, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v10, v0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v10, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    const-string v2, "\n"

    invoke-virtual {v9, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v10, v8, Liod;->f:Liop;

    iget v11, v10, Liop;->a:I

    iget-object v2, v8, Liod;->c:Lioh;

    invoke-static {v2}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v12

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    iget-wide v13, v10, Liop;->c:J

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    iget v2, v10, Liop;->d:I

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_5
    iget-object v3, v8, Liod;->f:Liop;

    iget v14, v3, Liop;->b:I

    iget-boolean v8, v8, Liod;->i:Z

    iget v3, v3, Liop;->d:I

    if-eqz v3, :cond_d

    const/4 v3, 0x1

    :goto_6
    packed-switch v14, :pswitch_data_1

    const-string v3, "DefaultCartDetailsRenderer"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v15, "Unknown subscription frequency units: "

    invoke-direct {v8, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_7
    if-lez v3, :cond_18

    if-eqz v2, :cond_17

    iget v2, v10, Liop;->d:I

    mul-int/2addr v2, v11

    move-object/from16 v0, p0

    iget-object v8, v0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v12, v10, v14

    const/4 v12, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v10, v12

    const/4 v12, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v12

    const/4 v2, 0x3

    aput-object v13, v10, v2

    invoke-virtual {v8, v3, v11, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lgzz;->b(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_9
    invoke-virtual {v9, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_a
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_2

    :cond_5
    move-object/from16 v0, p1

    iget-object v4, v0, Liob;->b:[Liod;

    array-length v5, v4

    const/4 v1, 0x0

    move v3, v1

    :goto_b
    if-ge v3, v5, :cond_a

    aget-object v1, v4, v3

    iget-object v2, v1, Liod;->f:Liop;

    if-eqz v2, :cond_9

    iget-object v2, v1, Liod;->f:Liop;

    iget v1, v1, Liod;->h:I

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_c
    iget v2, v2, Liop;->a:I

    const/4 v6, 0x1

    if-eq v2, v6, :cond_8

    const/4 v2, 0x1

    :goto_d
    if-nez v1, :cond_6

    if-eqz v2, :cond_9

    :cond_6
    const/4 v1, 0x0

    move v5, v1

    goto/16 :goto_1

    :cond_7
    const/4 v1, 0x0

    goto :goto_c

    :cond_8
    const/4 v2, 0x0

    goto :goto_d

    :cond_9
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_b

    :cond_a
    const/4 v1, 0x1

    move v5, v1

    goto/16 :goto_1

    :cond_b
    const v2, 0x7f0b0141    # com.google.android.gms.R.string.wallet_subscription

    goto/16 :goto_3

    :pswitch_0
    const v2, 0x7f0b013c    # com.google.android.gms.R.string.wallet_free_trial

    goto/16 :goto_4

    :pswitch_1
    const v2, 0x7f0b013d    # com.google.android.gms.R.string.wallet_prorated

    goto/16 :goto_4

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_6

    :pswitch_2
    if-eqz v8, :cond_f

    if-eqz v3, :cond_e

    const v3, 0x7f0f000e    # com.google.android.gms.R.plurals.wallet_recurrence_info_day_tax_limited

    goto/16 :goto_7

    :cond_e
    const v3, 0x7f0f000c    # com.google.android.gms.R.plurals.wallet_recurrence_info_day_tax

    goto/16 :goto_7

    :cond_f
    if-eqz v3, :cond_10

    const v3, 0x7f0f000d    # com.google.android.gms.R.plurals.wallet_recurrence_info_day_limited

    goto/16 :goto_7

    :cond_10
    const v3, 0x7f0f000b    # com.google.android.gms.R.plurals.wallet_recurrence_info_day

    goto/16 :goto_7

    :pswitch_3
    if-eqz v8, :cond_12

    if-eqz v3, :cond_11

    const v3, 0x7f0f000a    # com.google.android.gms.R.plurals.wallet_recurrence_info_month_tax_limited

    goto/16 :goto_7

    :cond_11
    const v3, 0x7f0f0008    # com.google.android.gms.R.plurals.wallet_recurrence_info_month_tax

    goto/16 :goto_7

    :cond_12
    if-eqz v3, :cond_13

    const v3, 0x7f0f0009    # com.google.android.gms.R.plurals.wallet_recurrence_info_month_limited

    goto/16 :goto_7

    :cond_13
    const v3, 0x7f0f0007    # com.google.android.gms.R.plurals.wallet_recurrence_info_month

    goto/16 :goto_7

    :pswitch_4
    if-eqz v8, :cond_15

    if-eqz v3, :cond_14

    const v3, 0x7f0f0006    # com.google.android.gms.R.plurals.wallet_recurrence_info_year_tax_limited

    goto/16 :goto_7

    :cond_14
    const v3, 0x7f0f0004    # com.google.android.gms.R.plurals.wallet_recurrence_info_year_tax

    goto/16 :goto_7

    :cond_15
    if-eqz v3, :cond_16

    const v3, 0x7f0f0005    # com.google.android.gms.R.plurals.wallet_recurrence_info_year_limited

    goto/16 :goto_7

    :cond_16
    const v3, 0x7f0f0003    # com.google.android.gms.R.plurals.wallet_recurrence_info_year

    goto/16 :goto_7

    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v12, v8, v10

    const/4 v10, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v8, v10

    const/4 v10, 0x2

    aput-object v13, v8, v10

    invoke-virtual {v2, v3, v11, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_8

    :cond_18
    const-string v2, ""

    goto/16 :goto_9

    :cond_19
    iget-object v1, v8, Liod;->a:Ljava/lang/String;

    iget-object v2, v8, Liod;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v8, Liod;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1a
    iget v2, v8, Liod;->e:I

    const/4 v3, 0x0

    const/4 v9, 0x1

    if-le v2, v9, :cond_1c

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v2, v8, Liod;->d:Lioh;

    if-eqz v2, :cond_1b

    iget-object v2, v8, Liod;->d:Lioh;

    invoke-static {v2}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v2

    :goto_e
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lgzz;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lgzz;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1b
    const-string v2, ""

    goto :goto_e

    :cond_1c
    iget-object v2, v8, Liod;->c:Lioh;

    invoke-static {v2}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v2

    goto :goto_e

    :cond_1d
    move-object/from16 v0, p1

    iget-object v1, v0, Liob;->c:Lioh;

    if-eqz v1, :cond_0

    move-object/from16 v0, p1

    iget-object v1, v0, Liob;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Lgzz;->b:Landroid/content/Context;

    const v2, 0x7f0b011f    # com.google.android.gms.R.string.wallet_discount_label

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    move-object/from16 v0, p1

    iget-object v3, v0, Liob;->c:Lioh;

    invoke-static {v3}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Liob;)V
    .locals 7

    const v3, 0x7f0b0122    # com.google.android.gms.R.string.wallet_inclusive_tax_vat

    const v2, 0x7f0b0121    # com.google.android.gms.R.string.wallet_inclusive_tax_gst

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p1, Liob;->e:Lioh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Liob;->e:Lioh;

    invoke-static {v0}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p1, Liob;->f:Lipt;

    if-eqz v1, :cond_1

    iget-object v0, v1, Lipt;->a:Lioh;

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Lipt;->b:Z

    if-eqz v0, :cond_3

    iget v0, v1, Lipt;->c:I

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    const v2, 0x7f0b0123    # com.google.android.gms.R.string.wallet_inclusive_tax_tax

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lgzz;->b:Landroid/content/Context;

    const v3, 0x7f0b0120    # com.google.android.gms.R.string.wallet_includes_tax_fmt

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    iget-object v0, v1, Lipt;->a:Lioh;

    invoke-static {v0}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    iget-object v1, p1, Liob;->g:Lipr;

    if-eqz v1, :cond_2

    iget-object v0, v1, Lipr;->a:Lioh;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lipr;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    const v2, 0x7f0b0124    # com.google.android.gms.R.string.wallet_shipping_with_type_label

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, v1, Lipr;->b:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v2, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v1, v1, Lipr;->a:Lioh;

    invoke-static {v1}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :pswitch_0
    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget v0, v1, Lipt;->c:I

    packed-switch v0, :pswitch_data_1

    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    const v2, 0x7f0b011e    # com.google.android.gms.R.string.wallet_tax_label

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v2, p0, Lgzz;->c:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v1, v1, Lipt;->a:Lioh;

    invoke-static {v1}, Lgth;->a(Lioh;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :pswitch_3
    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lgzz;->b:Landroid/content/Context;

    const v2, 0x7f0b0174    # com.google.android.gms.R.string.wallet_shipping_label

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
