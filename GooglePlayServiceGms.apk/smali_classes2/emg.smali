.class public final Lemg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:[Lemf;

.field public final b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lemf;

    iput-object v0, p0, Lemg;->a:[Lemf;

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v4, p0, Lemg;->a:[Lemf;

    add-int/lit8 v2, v1, 0x1

    new-instance v5, Lemf;

    invoke-direct {v5, v0}, Lemf;-><init>(Lehh;)V

    aput-object v5, v4, v1

    move v1, v2

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    return-void
.end method


# virtual methods
.method public final a(Lekz;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 10

    const/4 v4, 0x0

    iget-object v0, p0, Lemg;->a:[Lemf;

    array-length v0, v0

    new-array v3, v0, [Ljava/lang/String;

    iget-object v0, p0, Lemg;->a:[Lemf;

    array-length v0, v0

    new-array v7, v0, [Landroid/os/Bundle;

    iget-object v0, p0, Lemg;->a:[Lemf;

    array-length v0, v0

    new-array v8, v0, [Landroid/os/Bundle;

    new-instance v5, Landroid/util/SparseIntArray;

    iget-object v0, p0, Lemg;->a:[Lemf;

    array-length v0, v0

    invoke-direct {v5, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    move v1, v4

    :goto_0
    iget-object v0, p0, Lemg;->a:[Lemf;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lemg;->a:[Lemf;

    aget-object v2, v0, v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v2, Lemf;->a:Lehh;

    iget-object v6, v6, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "-"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v6, v2, Lemf;->a:Lehh;

    iget-object v6, v6, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    iget-object v0, p1, Lekz;->f:[Lela;

    aget-object v0, v0, v1

    iget v6, p1, Lekz;->a:I

    invoke-virtual {v2, v0}, Lemf;->a(Lela;)Landroid/util/Pair;

    move-result-object v6

    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    aput-object v0, v7, v1

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    aput-object v0, v8, v1

    iget-object v0, v2, Lemf;->a:Lehh;

    iget v0, v0, Lehh;->a:I

    invoke-virtual {v5, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v2, p1, Lekz;->c:[I

    :goto_1
    iget v0, p1, Lekz;->a:I

    if-ge v4, v0, :cond_1

    aget v0, v2, v4

    invoke-virtual {v5, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    aput v0, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v4, p1, Lekz;->d:[I

    iget-object v5, p1, Lekz;->e:[B

    iget v1, p1, Lekz;->a:I

    iget-object v9, p1, Lekz;->g:[B

    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;[B)V

    return-object v0
.end method
