.class final Lbwe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lbwd;


# direct methods
.method constructor <init>(Lbwd;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lbwe;->b:Lbwd;

    iput-object p2, p0, Lbwe;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    check-cast p2, Lbvw;

    new-instance v0, Lbwf;

    invoke-direct {v0, p0, p2}, Lbwf;-><init>(Lbwe;Lbvw;)V

    new-instance v1, Lbwg;

    iget-object v2, p0, Lbwe;->b:Lbwd;

    const-string v3, "SyncSchedulerImpl"

    invoke-direct {v1, v2, v3, v0}, Lbwg;-><init>(Lbwd;Ljava/lang/String;Ljava/lang/Runnable;)V

    :try_start_0
    iget-object v0, p0, Lbwe;->b:Lbwd;

    invoke-static {v0}, Lbwd;->c(Lbwd;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lbwe;->b:Lbwd;

    iget-object v1, p0, Lbwe;->a:Ljava/lang/String;

    sget-object v2, Lccw;->a:Lccw;

    sget-object v3, Lccw;->c:Lccw;

    invoke-virtual {v0, v1, v2, v3}, Lbwd;->a(Ljava/lang/String;Lccw;Lccw;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    iget-object v0, p0, Lbwe;->b:Lbwd;

    iget-object v1, p0, Lbwe;->a:Ljava/lang/String;

    sget-object v2, Lccw;->c:Lccw;

    invoke-static {v0, v1, v2}, Lbwd;->a(Lbwd;Ljava/lang/String;Lccw;)V

    return-void
.end method
