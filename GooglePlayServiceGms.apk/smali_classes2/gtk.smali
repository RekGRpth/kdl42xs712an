.class public final Lgtk;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljbi;)Lioj;
    .locals 13

    const/4 v12, 0x5

    const/4 v2, 0x2

    const/4 v1, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v7, Lioj;

    invoke-direct {v7}, Lioj;-><init>()V

    iget-object v0, p0, Ljbi;->h:Ljab;

    if-eqz v0, :cond_1

    iget-object v8, p0, Ljbi;->h:Ljab;

    if-nez v8, :cond_8

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v7, Lioj;->e:Lipv;

    :cond_1
    iget-object v0, p0, Ljbi;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ljbi;->a:Ljava/lang/String;

    iput-object v0, v7, Lioj;->b:Ljava/lang/String;

    :cond_2
    iget v0, p0, Ljbi;->e:I

    if-eqz v0, :cond_3

    iget v0, p0, Ljbi;->e:I

    iput v0, v7, Lioj;->j:I

    :cond_3
    iget v0, p0, Ljbi;->f:I

    if-eqz v0, :cond_4

    iget v0, p0, Ljbi;->f:I

    iput v0, v7, Lioj;->k:I

    :cond_4
    iget-object v0, p0, Ljbi;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Ljbi;->d:Ljava/lang/String;

    iput-object v0, v7, Lioj;->m:Ljava/lang/String;

    :cond_5
    iget-object v0, p0, Ljbi;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Ljbi;->j:Ljava/lang/String;

    iput-object v0, v7, Lioj;->a:Ljava/lang/String;

    :cond_6
    iget-object v0, p0, Ljbi;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Ljbi;->k:Ljava/lang/String;

    iput-object v0, v7, Lioj;->i:Ljava/lang/String;

    :cond_7
    iget v0, p0, Ljbi;->i:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    invoke-static {v0, v12}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    :goto_2
    iget v0, p0, Ljbi;->b:I

    packed-switch v0, :pswitch_data_1

    :goto_3
    iput v4, v7, Lioj;->c:I

    :goto_4
    iget v0, p0, Ljbi;->l:I

    invoke-static {v0}, Lgtk;->a(I)I

    move-result v0

    iput v0, v7, Lioj;->l:I

    move-object v0, v7

    goto :goto_0

    :cond_8
    new-instance v6, Lipv;

    invoke-direct {v6}, Lipv;-><init>()V

    iput-boolean v3, v6, Lipv;->g:Z

    if-nez v8, :cond_b

    const/4 v0, 0x0

    :goto_5
    iput-object v0, v6, Lipv;->a:Lixo;

    iget v0, v8, Ljab;->m:I

    if-eq v0, v3, :cond_19

    move v0, v3

    :goto_6
    iput-boolean v0, v6, Lipv;->f:Z

    iget-object v0, v8, Ljab;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, v8, Ljab;->a:Ljava/lang/String;

    iput-object v0, v6, Lipv;->c:Ljava/lang/String;

    :cond_9
    iget-object v0, v8, Ljab;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, v8, Ljab;->j:Ljava/lang/String;

    iput-object v0, v6, Lipv;->d:Ljava/lang/String;

    :cond_a
    iget-boolean v0, v8, Ljab;->k:Z

    iput-boolean v0, v6, Lipv;->e:Z

    move-object v0, v6

    goto/16 :goto_1

    :cond_b
    new-instance v5, Lixo;

    invoke-direct {v5}, Lixo;-><init>()V

    iget-object v0, v8, Ljab;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    new-array v9, v0, [Ljava/lang/String;

    iput-object v9, v5, Lixo;->q:[Ljava/lang/String;

    :goto_8
    if-lez v0, :cond_f

    iget-object v9, v5, Lixo;->q:[Ljava/lang/String;

    add-int/lit8 v10, v0, -0x1

    const-string v11, ""

    aput-object v11, v9, v10

    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    :cond_c
    iget-object v0, v8, Ljab;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v2

    goto :goto_7

    :cond_d
    iget-object v0, v8, Ljab;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v3

    goto :goto_7

    :cond_e
    move v0, v4

    goto :goto_7

    :cond_f
    iget-object v0, v8, Ljab;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, v5, Lixo;->q:[Ljava/lang/String;

    iget-object v9, v8, Ljab;->c:Ljava/lang/String;

    aput-object v9, v0, v4

    :cond_10
    iget-object v0, v8, Ljab;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, v5, Lixo;->q:[Ljava/lang/String;

    iget-object v9, v8, Ljab;->d:Ljava/lang/String;

    aput-object v9, v0, v3

    :cond_11
    iget-object v0, v8, Ljab;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, v5, Lixo;->q:[Ljava/lang/String;

    iget-object v9, v8, Ljab;->e:Ljava/lang/String;

    aput-object v9, v0, v2

    :cond_12
    iget-object v0, v8, Ljab;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, v8, Ljab;->g:Ljava/lang/String;

    iput-object v0, v5, Lixo;->f:Ljava/lang/String;

    :cond_13
    iget-object v0, v8, Ljab;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, v8, Ljab;->f:Ljava/lang/String;

    iput-object v0, v5, Lixo;->a:Ljava/lang/String;

    :cond_14
    iget-object v0, v8, Ljab;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, v8, Ljab;->b:Ljava/lang/String;

    iput-object v0, v5, Lixo;->s:Ljava/lang/String;

    :cond_15
    iget-object v0, v8, Ljab;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, v8, Ljab;->i:Ljava/lang/String;

    iput-object v0, v5, Lixo;->k:Ljava/lang/String;

    :cond_16
    iget-object v0, v8, Ljab;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    iget-object v0, v8, Ljab;->h:Ljava/lang/String;

    iput-object v0, v5, Lixo;->d:Ljava/lang/String;

    :cond_17
    iget-object v0, v8, Ljab;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, v8, Ljab;->l:Ljava/lang/String;

    iput-object v0, v5, Lixo;->r:Ljava/lang/String;

    :cond_18
    move-object v0, v5

    goto/16 :goto_5

    :cond_19
    move v0, v4

    goto/16 :goto_6

    :pswitch_1
    iput v3, v7, Lioj;->h:I

    goto/16 :goto_2

    :pswitch_2
    iput v3, v7, Lioj;->h:I

    goto/16 :goto_2

    :pswitch_3
    iput v2, v7, Lioj;->h:I

    goto/16 :goto_2

    :pswitch_4
    iput v3, v7, Lioj;->h:I

    goto/16 :goto_2

    :pswitch_5
    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    invoke-static {v0, v2}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    goto/16 :goto_2

    :pswitch_6
    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    const/4 v5, 0x6

    invoke-static {v0, v5}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    goto/16 :goto_2

    :pswitch_7
    iget v0, p0, Ljbi;->f:I

    if-eqz v0, :cond_1a

    iget v0, p0, Ljbi;->e:I

    if-eqz v0, :cond_1a

    iget v0, p0, Ljbi;->e:I

    iget v5, p0, Ljbi;->f:I

    const/4 v6, -0x1

    invoke-static {v0, v5, v6}, Lgth;->a(III)I

    move-result v0

    packed-switch v0, :pswitch_data_2

    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    invoke-static {v0, v12}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    goto/16 :goto_2

    :pswitch_8
    iput v3, v7, Lioj;->h:I

    goto/16 :goto_2

    :pswitch_9
    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    invoke-static {v0, v2}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    goto/16 :goto_2

    :cond_1a
    iput v1, v7, Lioj;->h:I

    iget-object v0, v7, Lioj;->g:[I

    invoke-static {v0, v12}, Lboz;->b([II)[I

    move-result-object v0

    iput-object v0, v7, Lioj;->g:[I

    goto/16 :goto_2

    :pswitch_a
    iput v4, v7, Lioj;->d:I

    iput v1, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_b
    iput v4, v7, Lioj;->d:I

    const/4 v0, 0x4

    iput v0, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_c
    iput v4, v7, Lioj;->d:I

    const/4 v0, 0x6

    iput v0, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_d
    iput v4, v7, Lioj;->d:I

    iput v2, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_e
    iput v4, v7, Lioj;->d:I

    iput v12, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_f
    iput v4, v7, Lioj;->d:I

    const/4 v0, 0x7

    iput v0, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_10
    iput v4, v7, Lioj;->d:I

    goto/16 :goto_3

    :pswitch_11
    iput v4, v7, Lioj;->d:I

    iput v3, v7, Lioj;->c:I

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_f
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method
