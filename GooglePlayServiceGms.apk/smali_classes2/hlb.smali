.class public final Lhlb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhlb;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private a(I)I
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lhlb;->a:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    move v2, v1

    move v3, v1

    move v4, v1

    :goto_0
    iget-object v1, p0, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/32 v7, 0xdbba0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {v1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :pswitch_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :pswitch_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    if-nez v2, :cond_3

    if-nez v4, :cond_3

    if-gtz v3, :cond_1

    iget-object v0, p0, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_3

    :cond_1
    iget-object v0, p0, Lhlb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_2

    add-int/lit8 p1, p1, -0x1

    :cond_2
    :goto_2
    return p1

    :cond_3
    const/4 p1, -0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/location/ActivityRecognitionResult;JLcom/google/android/gms/location/ActivityRecognitionResult;J)Lhlc;
    .locals 6

    new-instance v0, Lhlc;

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v1

    sub-long/2addr v1, p1

    invoke-virtual {p3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v3

    add-long/2addr v3, p4

    invoke-virtual {p0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lhlc;-><init>(JJI)V

    return-object v0
.end method

.method private a(IJJJ)Ljava/lang/Boolean;
    .locals 17

    cmp-long v2, p2, p4

    if-lez v2, :cond_6

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhlb;->b:Ljava/util/List;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_0
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lhlb;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v13, v2

    move-wide v15, v4

    move-wide v5, v15

    move-wide v3, v13

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhlc;

    iget-wide v8, v2, Lhlc;->a:J

    move-wide/from16 v0, p4

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iget-wide v10, v2, Lhlc;->b:J

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    cmp-long v12, v10, v8

    if-lez v12, :cond_5

    sub-long v8, v10, v8

    iget v2, v2, Lhlc;->c:I

    move/from16 v0, p1

    if-ne v2, v0, :cond_1

    add-long/2addr v5, v8

    :cond_1
    add-long v2, v3, v8

    move-wide v4, v5

    :goto_3
    move-wide v13, v2

    move-wide v15, v4

    move-wide v5, v15

    move-wide v3, v13

    goto :goto_2

    :cond_2
    sub-long v7, p2, p4

    sub-long v2, v7, v3

    sub-long v7, p2, p4

    const-wide/16 v9, 0x1

    sub-long/2addr v7, v9

    const-wide/16 v9, 0x0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    move-wide/from16 v0, p6

    invoke-static {v7, v8, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    cmp-long v4, v5, v7

    if-lez v4, :cond_3

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gtz v2, :cond_4

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    move-wide v13, v3

    move-wide v2, v13

    move-wide v15, v5

    move-wide v4, v15

    goto :goto_3

    :cond_6
    move-wide/from16 v13, p4

    move-wide/from16 p4, p2

    move-wide/from16 p2, v13

    goto :goto_0
.end method


# virtual methods
.method public final a(JJJ)Ljava/lang/Boolean;
    .locals 8

    const/4 v1, 0x0

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lhlb;->a(IJJJ)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/util/List;)Ljava/util/List;
    .locals 13

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/ActivityRecognitionResult;

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    move v7, v1

    move-wide v11, v2

    move-wide v1, v11

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v7, v3, :cond_2

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Lcom/google/android/gms/location/ActivityRecognitionResult;

    add-int/lit8 v3, v7, -0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/location/ActivityRecognitionResult;

    invoke-virtual {v6}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-virtual {v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v8

    sub-long/2addr v4, v8

    const-wide/32 v8, 0x668a0

    cmp-long v4, v4, v8

    if-lez v4, :cond_0

    const-wide/32 v4, 0x33450

    invoke-static/range {v0 .. v5}, Lhlb;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;JLcom/google/android/gms/location/ActivityRecognitionResult;J)Lhlc;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/32 v1, 0x33450

    move-object v3, v6

    :goto_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move-object v0, v3

    goto :goto_0

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v5

    if-eq v4, v5, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a()Lcom/google/android/gms/location/DetectedActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/location/DetectedActivity;->a()I

    move-result v4

    if-nez v4, :cond_1

    invoke-direct {p0, v7}, Lhlb;->a(I)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    move v7, v4

    move-object v3, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v6}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v4

    invoke-virtual {v3}, Lcom/google/android/gms/location/ActivityRecognitionResult;->d()J

    move-result-wide v8

    sub-long/2addr v4, v8

    const-wide/16 v8, 0x2

    div-long v8, v4, v8

    sub-long/2addr v4, v8

    invoke-static/range {v0 .. v5}, Lhlb;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;JLcom/google/android/gms/location/ActivityRecognitionResult;J)Lhlc;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v1, v8

    move-object v3, v6

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/location/ActivityRecognitionResult;

    const-wide/32 v4, 0x33450

    invoke-static/range {v0 .. v5}, Lhlb;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;JLcom/google/android/gms/location/ActivityRecognitionResult;J)Lhlc;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v10}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v3, v0

    goto :goto_1
.end method

.method public final b(JJJ)Ljava/lang/Boolean;
    .locals 8

    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lhlb;->a(IJJJ)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c(JJJ)Ljava/lang/Boolean;
    .locals 8

    const/4 v1, 0x2

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lhlb;->a(IJJJ)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
