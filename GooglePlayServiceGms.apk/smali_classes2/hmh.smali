.class final Lhmh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhmq;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lhmx;)Lhmy;
    .locals 10

    const/16 v9, 0xc

    const/4 v8, 0x5

    const-wide v6, 0x3fc3333333333333L    # 0.15

    const/4 v5, 0x0

    const/16 v4, 0x64

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe7a3f4666ec9e2L    # 0.738764

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_121

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3f92d2bb23571d1dL    # 0.018382

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_27

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd16e6106ab14ecL    # 0.272362

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_22

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f55c63ae2541d8fL    # 0.001329

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1e

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40247eff5c6c11a1L    # 10.248042

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fe11efb6dca07f6L    # 0.535032

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f17dae81882adc5L    # 9.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f52ba16e7a311e8L    # 0.001143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f229cbab649d389L    # 1.42E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43c254a3c64346L    # 6.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f13660e51d25aabL    # 7.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_2
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f2b003686a4ca4fL    # 2.06E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto :goto_0

    :cond_5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4024846de76427c8L    # 10.258651

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402480dbad3a604eL    # 10.251676

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f157eed45e9185dL    # 8.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1d

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5bfbdf090f733bL    # 0.001708

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4cef240fa9c12fL    # 8.83E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdf0196d8f4f93cL    # 0.484472

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402522c1b10fd7e4L    # 10.567884

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe117d8cf398e97L    # 0.534161

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6346994185058eL    # 0.002353

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f142f61ed5ae1ceL    # 7.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe5a8954a7f8013L    # 0.676829

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ef3ec460ed80a18L    # 1.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f201f31f46ed246L    # 1.23E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f0abd1aa821f299L    # 5.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f702363b256ffc1L    # 0.00394

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f6cacd184c272L    # 9.59E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdce73a365cb35fL    # 0.451613

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f21f4f50a02b841L    # 1.37E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f03660e51d25aabL    # 3.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe45d18090b417dL    # 0.636364

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3efc4fc1df3300deL    # 2.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1e
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f227b2cc70867aeL    # 1.41E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_21

    iget-wide v0, p1, Lhmx;->c:D

    const-wide/high16 v2, 0x3fd9000000000000L    # 0.390625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1f
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2733226c3b927dL    # 1.77E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_20

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_20
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_22
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f15c209246bf013L    # 8.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_26

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd776188b11409aL    # 0.366583

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_25

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_24

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f53660e51d25aabL    # 0.001184

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_23

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_23
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_24
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_25
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_26
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_27
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40001ef49cf56eadL    # 2.015115

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b6

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa4da09cc319c5aL    # 0.040726

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_48

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe11b5e95b78ccaL    # 0.534591

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_39

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff97229e90795f6L    # 1.590372

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_32

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f17dae81882adc5L    # 9.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_31

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_28

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_28
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f893ab430f49492L    # 0.012319

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_30

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f142f61ed5ae1ceL    # 7.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2f

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022ceaceaaf35e3L    # 9.403663

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2a

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6e96c3fc43b2ddL    # 0.003734

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_29

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_29
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2a
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f62f6e82949a565L    # 0.002315

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2c

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd37168f8e7ddcaL    # 0.303797

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2c
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f1f14983d790L    # 0.155821

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2e

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f750870110a137fL    # 0.005135

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_2d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_2f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_30
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_31
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_32
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe059e1f3a57eaaL    # 0.510972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_35

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1040bfe3b03e21L    # 6.2E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_34

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f14b599aa60913aL    # 7.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_33

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_33
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_34
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_35
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe1c638c9752978L    # 0.555447

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_37

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f316ebd4cfd08d5L    # 2.66E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_36

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_36
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_37
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f9378ee28672L    # 0.156043

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_38

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_38
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_39
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f382ecaeea63b69L    # 3.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_45

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8a6efc371da37fL    # 0.012907

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3b

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcb7d6b65a9a805L    # 0.214765

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3b
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f28c5c9a34ca0c3L    # 1.89E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_44

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f396d8f4f93bc0aL    # 3.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_43

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcfe679463cfb33L    # 0.249221

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3e

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f15c209246bf013L    # 8.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3d

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f26f0068db8bac7L    # 1.75E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3e
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc5caafbc1acde2L    # 0.170248

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_42

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5148fd9fd36f7eL    # 0.001055

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_3f
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f69e30014f8b589L    # 0.00316

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_40

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_40
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fdcd6e8af81626bL    # 0.450617

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_41

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_41
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_42
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_43
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_44
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_45
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1d19157abb8801L    # 1.11E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_47

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8801b43526527aL    # 0.011722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_46

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_46
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_47
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_48
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf309c7ffde721L    # 0.487342

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a6

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa7af42784a9479L    # 0.046259

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7c

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb320a73f748a16L    # 0.074717

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_57

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f9215b9a5a89b95L    # 0.017661

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_52

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd6f96e158750c2L    # 0.358974

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_49

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_49
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f507faa044ae85cL    # 0.001007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_51

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f21b1d92b7fe08bL    # 1.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4d

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f24b599aa60913aL    # 1.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4c

    iget-wide v0, p1, Lhmx;->c:D

    const-wide/high16 v2, 0x3fca000000000000L    # 0.203125

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4b

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022c1c90c4dec1cL    # 9.378487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff6459ea57214f0L    # 1.391997

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4e
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4c36976bc1effaL    # 8.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_50

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffda10385c67dfeL    # 1.85181

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_4f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_50
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_51
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_52
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd76cc1ca3a4b55L    # 0.366013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_56

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f8c68ec52a411c3L    # 0.013872

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_55

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f3b43526527a205L    # 4.16E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_54

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe7d1c5c5718eb9L    # 0.744357

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_53

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_53
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_54
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_55
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_56
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_57
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff206f7a0b5ed8dL    # 1.126701

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_59

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022c58c8eef1bacL    # 9.385838

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_58

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_58
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_59
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401773621fafc8b0L    # 5.862679

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_78

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f676145953586cbL    # 0.002854

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_76

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40218884e831ad21L    # 8.766639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5e

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9eecbfb15b573fL    # 0.0302

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5a
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff80b51372a38b9L    # 1.502763

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5c

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f363779e9d0e992L    # 3.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5c
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x401d6b2bfdb4cc25L    # 7.35466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5e
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd7a4b124d099e1L    # 0.369427

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6b

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9739b024f6598eL    # 0.022681

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_64

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4022af00474d9c6bL    # 9.341799

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_5f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_5f
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc4d1bf7ad4b274L    # 0.162651

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_60

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_60
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a9b8cb8e086beL    # 8.12E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_61

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_61
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f223810e8858ff7L    # 1.39E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_62
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4028a2ce24bba12bL    # 12.317979

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_63

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_63
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_64
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f815f02c4d65e46L    # 0.008482

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6a

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40267c0d0aaa7dedL    # 11.242287

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_65

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_65
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd2c6a7ef9db22dL    # 0.293375

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_67

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f02599ed7c6fbd2L    # 3.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_66

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_66
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_67
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f61c2a023209678L    # 0.002168

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_68

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_68
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40224ed17c5ef630L    # 9.153942

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_69

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_69
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6b
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f72d44dca8e2e2cL    # 0.004597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f814db59578a2a9L    # 0.008449

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6d
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2797cc39ffd60fL    # 1.8E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_73

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f44cec41dd1a21fL    # 6.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_71

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fce7064ece9a2c6L    # 0.237805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6e
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f408c3f3e0370ceL    # 5.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_70

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcec8e68e3ef284L    # 0.240506

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_6f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_70
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_71
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4711947cfa26a2L    # 7.04E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_72

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_72
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_73
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f46df3f961804daL    # 6.98E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_75

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3eed5c31593e5fb7L    # 1.4E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_74

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_74
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_75
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_76
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f68a222d5171e2aL    # 0.003007

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_77

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_77
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_78
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fbd99999999999aL    # 0.115625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7a

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc16a37ac3eb7ccL    # 0.136054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_79

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_79
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7a
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f738ac18f81e8a3L    # 0.004771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7c
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd6aba387592535L    # 0.354226

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8c

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd6cccccccccccdL    # 0.35625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8b

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401699bf9c62a1b6L    # 5.650145

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_89

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd5dd052934acb0L    # 0.341615

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_88

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9c122749f0e4daL    # 0.027413

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7f

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f02dfd694ccab3fL    # 3.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7e

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2083dbc23315d7L    # 1.26E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_7d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_7f
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb366b6177ea1c7L    # 0.075786

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_82

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd6333333333333L    # 0.346875

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_80

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_80
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4ca3a4b5568e82L    # 8.74E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_81

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_81
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_82
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f323810e8858ff7L    # 2.78E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_84

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f0797cc39ffd60fL    # 4.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_83

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_83
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_84
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f761b2a27f1b691L    # 0.005397

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_87

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fde19008205ff1eL    # 0.470276

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_85

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_85
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa367c286f8ad25L    # 0.037901

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_86

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_86
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_87
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_88
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_89
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcd6a8b8f14db59L    # 0.229814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8c
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fba5df2239e6abaL    # 0.102996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_90

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3faea50d6b228dcaL    # 0.059853

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8f

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40213ff2c3009b30L    # 8.624899

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8d
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5f4b1ee2435697L    # 0.00191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_8f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_90
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6148fd9fd36f7eL    # 0.00211

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9b

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6be8ff327aa68fL    # 0.003407

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_97

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f64cec41dd1a21fL    # 0.00254

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_96

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f068b5cbff47736L    # 4.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_92

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcf693c03bc4d23L    # 0.245399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_91

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_91
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_92
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc4008e9b38d60aL    # 0.156267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_95

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b9b66f9335d25L    # 0.00337

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_93

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_93
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2eabbcb1cc9646L    # 2.34E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_94

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_94
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_95
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_96
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_97
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f49b0ab2e1693c0L    # 7.84E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_99

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc0f6be37de939fL    # 0.13253

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_98

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_98
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_99
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efa36e2eb1c432dL    # 2.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9b
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f129cbab649d389L    # 7.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9d

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402a3ed0b30b5aa7L    # 13.122686

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9d
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fff18c9fb6134ceL    # 1.943552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a5

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddffe6d58c8eefL    # 0.468744

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9e
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3fef39085f4a1L    # 0.156218

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a2

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7ccb7d41743e96L    # 0.00703

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a1

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5101b003686a4dL    # 0.001038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a0

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3908e581cf7879L    # 3.82E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_9f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_9f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a2
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4d4b6a619da9caL    # 8.94E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a3
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021388f21709310L    # 8.610467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a6
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff272df505d0fa6L    # 1.153045

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a8

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f846bacf7446f9cL    # 0.009971

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a8
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f663dc486ad2dcbL    # 0.002715

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b0

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40713f077ccc0L    # 0.156466

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_a9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_a9
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc999999999999aL    # 0.2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_aa
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe10bf5d78811b2L    # 0.53271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_af

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ac

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1754b05b7cfe58L    # 8.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ac
    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f31e42e12620254L    # 2.73E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ad

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ad
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402aa5475a31a4beL    # 13.32281

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ae

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ae
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_af
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b0
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f83882278d0cc36L    # 0.009537

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b1
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f95db3397dd00f7L    # 0.021344

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b3

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b3
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f38e757928e0c9eL    # 3.8E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b4
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd26e2eb1c432caL    # 0.287975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b6
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fcbed634549b62cL    # 0.218182

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_110

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe8ff10ecb74de0L    # 0.781136

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f8

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fb9422ccb3a2596L    # 0.098666

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e3

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f71819d2391d580L    # 0.004274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d1

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc808d08919ef95L    # 0.187769

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bf

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdc87b56b873797L    # 0.445783

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_be

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3040bfe3b03e21L    # 2.48E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bd

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5947cfa26a22b4L    # 0.001543

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bc

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f7ee2435696e58aL    # 0.00754

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_bb

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f2626b2f23033a4L    # 1.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ba

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f03ec460ed80a18L    # 3.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b7
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc66afcce1c5825L    # 0.17514

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b8
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40255a9da597d49dL    # 10.676984

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_b9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_b9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_bf
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7cc100e6afcce2L    # 0.00702

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ce

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3b64e054690de1L    # 4.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cc

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f60abb44e50c5ebL    # 0.002035

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c9

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f22599ed7c6fbd2L    # 1.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c8

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4b3aeee957470fL    # 8.31E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c2

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f41aef6f8f04L    # 0.155887

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c1

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd8d5347a5b0ff1L    # 0.388013

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f33b9f127f5e84fL    # 3.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c7

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fe41b003686a4caL    # 0.628296

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c3
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f21904b3c3e74b0L    # 1.34E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c4
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1b003686a4ca4fL    # 1.03E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c6

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f467a95c853c148L    # 6.86E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_c5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_c9
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f73005814940bbbL    # 0.004639

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cb

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1eeed8904f6dfcL    # 1.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ca

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ca
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cc
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f44f8b588e368f1L    # 6.4E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ce
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fdb6db50f40e5a3L    # 0.428571

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_cf

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_cf
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f47d2849cb252ceL    # 7.27E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d1
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd70a3d70a3d70aL    # 0.36

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e1

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4000f31a8ef77f28L    # 2.118703

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dd

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdf38327674d163L    # 0.487805

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d7

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa2a0a958537e2cL    # 0.036382

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d6

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f58a86d71f36263L    # 0.001505

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d5

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f429cbab649d389L    # 5.68E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d4

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40171e67d77fae36L    # 5.779693

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d3

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc7bf9c62a1b5c8L    # 0.185535

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d7
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f617507e9d94d0eL    # 0.002131

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_dc

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f27763e4abe6a33L    # 1.79E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d8

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d8
    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f240dd3fe1975f3L    # 1.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_db

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fb6a22b3892ee85L    # 0.088412

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_da

    const/16 v0, 0x22

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_d9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_d9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_da
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_db
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dc
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_dd
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f13a92a30553261L    # 7.5E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_df

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402cf28198f1d3edL    # 14.473645

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_de

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_de
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_df
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4006ded5ae1cde5dL    # 2.858806

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e1
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4a8262456f75daL    # 8.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e3
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe17d9dba908a26L    # 0.546584

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f4

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9382e44b6e935cL    # 0.019054

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f3

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4002c5fe974a3401L    # 2.346677

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ec

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3efe68a0d349be90L    # 2.9E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e4

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e4
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f28a43bb40b34e7L    # 1.88E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e8

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd8ecd07852f7f5L    # 0.389454

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e7

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb77292c4934268L    # 0.091592

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e6

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37225b749adc90L    # 3.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e8
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ff38c61d8622c45L    # 1.221773

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_e9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_e9
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f142f61ed5ae1ceL    # 7.7E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ea

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ea
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fb6d3f9e7b80a9eL    # 0.089172

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_eb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_eb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ec
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x4006a5425f202108L    # 2.830693

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f2

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4023bc9adc8fb86fL    # 9.868369

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f1

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fb9c9f72f76e610L    # 0.100738

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ed

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ed
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd7792d1287c201L    # 0.366771

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f0

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7a9da597d49d7cL    # 0.006498

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ee

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ee
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4d323fee2c98e5L    # 8.91E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ef

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ef
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f4
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fbaad4f5903a754L    # 0.104207

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f6

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40bd44998d046L    # 0.156611

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f5

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f6
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f522b7baecd0785L    # 0.001109

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f8
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fb9187e7c06e19cL    # 0.09803

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fe

    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f1b866e43aa79bcL    # 1.05E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fc

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f1c92ddbdb5d895L    # 1.09E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fa

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f70fb65668c2614L    # 0.004146

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_f9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_f9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fa
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4021572a172abef0L    # 8.670243

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fb

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fb
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fc
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f2c2e33eff19503L    # 2.15E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_fd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_fe
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe1dcfede97d06cL    # 0.558227

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10b

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f24b599aa60913aL    # 1.58E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_100

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4030c10fc71d6063L    # 16.754147

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_ff

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_ff
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_100
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fcdcd2d44dca8e3L    # 0.232824

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10a

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b738e6d15ad10L    # 0.003351

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_104

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f967b5f1bef49cfL    # 0.021955

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_103

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40225644d87724fbL    # 9.168494

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_102

    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc00d2c386d2ed8L    # 0.125402

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_101

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_101
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_102
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_103
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_104
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4bd1ed9dfdac69L    # 8.49E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_105

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_105
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f3ebc83a96d4c34L    # 4.69E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_108

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc53c68661ae70cL    # 0.165906

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_107

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x400ba7ffde7210bfL    # 3.457031

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_106

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_106
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_107
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_108
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f98e368f08461faL    # 0.024305

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_109

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_109
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10b
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fdcd6e8af81626bL    # 0.450617

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10f

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f90f733a8a3f898L    # 0.016568

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10d

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f80db6a1e81cb47L    # 0.008231

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10d
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc4ea4228997c3dL    # 0.163399

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_10e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_10f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_110
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe0f9c1f85d744fL    # 0.530488

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_118

    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f69a1fd1569f490L    # 0.003129

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_113

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f4869835158b828L    # 7.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_112

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f6044f1a1986b9cL    # 0.001986

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_111

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_111
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_112
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_113
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fe3b2acfb762d84L    # 0.615561

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_117

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f37a89331a08bfcL    # 3.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_116

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40004f709b738e6dL    # 2.038789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_114

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_114
    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f43b18dac258d58L    # 6.01E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_115

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_115
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_116
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_117
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_118
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f564c729f59ccfbL    # 0.001361

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fe544ae85b9e8c4L    # 0.664634

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11b

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f14727dcbddb984L    # 7.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_119

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_119
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd0d39da16616b5L    # 0.262916

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->d:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11c
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f77a89331a08bfcL    # 0.005776

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_120

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f87457c0b135979L    # 0.011363

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11f

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f23cab81f969e3dL    # 1.51E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11d
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f70c804102ff8ecL    # 0.004097

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_11e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_11f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_120
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_121
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fd20d1fa333764fL    # 0.282051

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b6

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f84dad31fcd24e1L    # 0.010183

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15c

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fc86bc621b7e0acL    # 0.190789

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_125

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x401180aa64c2f838L    # 4.37565

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_124

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fdb8c75c4a83bL    # 7.964403

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_123

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f79ee88df37329cL    # 0.006331

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_122

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_122
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_123
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_124
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_125
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff2589374bc6a7fL    # 1.146625

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_157

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9ec204f2ae07e6L    # 0.030037

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_153

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ffb9dfc9ff92f2bL    # 1.726071

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13e

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fa24f6598e10cf6L    # 0.035762

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_126

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_126
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fcdb15b573eab36L    # 0.231975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12b

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa58e432441fea8L    # 0.042101

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12a

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40155ec2480e8c8bL    # 5.342538

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_129

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f62923e5b8561d4L    # 0.002267

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_128

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f72f76e6106bL    # 0.155981

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_127

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_127
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_128
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_129
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->g:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12b
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fbed7c6fbd273d6L    # 0.12048

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12d

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc40d23d4f15e7dL    # 0.156651

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12d
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402284bf8fcd67fdL    # 9.259274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13d

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa70f3882278d0dL    # 0.045038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_130

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f65faa8a82a5615L    # 0.002683

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12e
    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa33e35c5b4aa97L    # 0.037584

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_12f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_12f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_130
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7af8df7a4e7ab7L    # 0.006585

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_139

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3e90795f676eaL    # 0.155549

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_133

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9126634117f844L    # 0.016748

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_131

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_131
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4032bf474107314dL    # 18.747181

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_132

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_132
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_133
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f40f94c87980f56L    # 5.18E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_135

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f4cb1897a67aL    # 0.155908

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_134

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_134
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_135
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fc39c7bcc2938deL    # 0.153213

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_138

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f068b5cbff47736L    # 4.3E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_136

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_136
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f644241c3efae79L    # 0.002473

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_137

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_137
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_138
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_139
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402cddc5718eb895L    # 14.433147

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13a
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3ff920ec74320104L    # 1.570538

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13b
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7acde19fc2a887L    # 0.006544

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13e
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc0383ad9f0a1beL    # 0.126716

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_141

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f14727dcbddb984L    # 7.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_13f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_13f
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f72e62131a8ef78L    # 0.004614

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_140

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_140
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_141
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401f8b1bbcf4e875L    # 7.885848

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_145

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c15097c80841fL    # 0.001714

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_142

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_142
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7e58e64b231401L    # 0.007409

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_143

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_143
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc970e6f2e8c048L    # 0.198758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_144

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_144
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_145
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x403095f7c6759ab7L    # 16.585812

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_147

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f71f70de8f6ceffL    # 0.004386

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_146

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_146
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_147
    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f85350092ccf6beL    # 0.010355

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_151

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6e8a2ec28b2a6bL    # 0.003728

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14c

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ef284167a95dL    # 0.155736

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_148

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_148
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x402028ddc6195465L    # 8.079817

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_149

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_149
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f64db59578a2a91L    # 0.002546

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14a
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40210ac7b890d5a6L    # 8.521055

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14c
    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd4100e6afcce1cL    # 0.31348

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14f

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff67c5154866a12L    # 1.405351

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14d
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7d66adb402d16cL    # 0.007178

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_14e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_14f
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fecb57a355043e5L    # 0.897153

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_150

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_150
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_151
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x3ffb2cbd987c6328L    # 1.698423

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_152

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_152
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_153
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9a98676a7264a1L    # 0.025972

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_155

    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f84e54f7a919696L    # 0.010203

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_154

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_154
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_155
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f896744b2b777d1L    # 0.012404

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_156

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_156
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_157
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff6a72c94b380cbL    # 1.415814

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_159

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403918ffa7eb6bf4L    # 25.097651

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_158

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_158
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_159
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3ffbce4217d28L    # 0.156242

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15b

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4009918e325d4a5eL    # 3.196072

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15c
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3ff69fb506dd69d3L    # 1.413991

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_179

    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fe1cb7b28954a80L    # 0.556089

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_178

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e8a86d71f3626L    # 7.63528

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_165

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f962413db7f1737L    # 0.021622

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_163

    const/16 v0, 0x3e

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f90880d801b4352L    # 0.016144

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_162

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5c3265add9c27fL    # 0.001721

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15d
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4355475a31a4beL    # 5.9E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15f

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4057e6af9ebe9c88L    # 95.604469

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_15e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_15f
    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x40012a45d41fa765L    # 2.145641

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_161

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb901d19157abb9L    # 0.097684

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_160

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_160
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_161
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_162
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_163
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f7246bf01322f27L    # 0.004462

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_164

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_164
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_165
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fd0bb6672fba01fL    # 0.261438

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_177

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3797cc39ffd60fL    # 3.6E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_168

    iget-wide v0, p1, Lhmx;->f:D

    const-wide v2, 0x3fd6db6e503fb374L    # 0.357143

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_166

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_166
    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5a2a4db163babaL    # 0.001597

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_167

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_167
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_168
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f5cb89d6adf71ebL    # 0.001753

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16c

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9f25e56cd6c2f0L    # 0.030418

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16a

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fa1b499d0203e64L    # 0.034581

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_169

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_169
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16a
    const-wide v0, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f5040bfe3b03e21L    # 9.92E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16c
    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401e8de58e64b231L    # 7.638571

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16d

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16d
    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa355ef1fddebd9L    # 0.037765

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_176

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f153bd1676640a7L    # 8.1E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16e
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f6018e757928e0dL    # 0.001965

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_16f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_16f
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f3094a2b9d3cbc5L    # 2.53E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_170

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_170
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb235935fc3b4f6L    # 0.07113

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_175

    invoke-virtual {p1, v6, v7}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fcae7ab7564302bL    # 0.210195

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_174

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff5fad5bee3d5feL    # 1.373739

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_171

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_171
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f978897e996312fL    # 0.022982

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_173

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f722962cfd8f0c7L    # 0.004434

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_172

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_172
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_173
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_174
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_175
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_176
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_177
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_178
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_179
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fbc2b734b51372aL    # 0.110038

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a8

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb8c25072085b18L    # 0.096715

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a6

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcf984a0e410b63L    # 0.246835

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a2

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40018280ae104923L    # 2.188722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_192

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9238da3c21187eL    # 0.017795

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_184

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa4faacd9e83e42L    # 0.040975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_183

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fd8789613d31b9bL    # 0.38236

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_182

    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8e0b4e11dbca97L    # 0.01467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_180

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f8b03e20ccff21bL    # 0.013191

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17e

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3ff642a8869c66d3L    # 1.391274

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17a
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400707bfe7e1fc09L    # 2.878784

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17d

    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fc40ea5b530ced5L    # 0.156697

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17b
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa339e279dd3bb0L    # 0.037551

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17e
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc11efb6dca07f6L    # 0.133758

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_17f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_17f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_180
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc5aaef2c732592L    # 0.169279

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_181

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_181
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_182
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_183
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_184
    iget-wide v0, p1, Lhmx;->a:D

    const-wide v2, 0x3fd3da1ec4e7253eL    # 0.310188

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18f

    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9c226809d49518L    # 0.027475

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_188

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6704ff43419e30L    # 0.00281

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_186

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f969e3c968944L    # 9.64E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_185

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_185
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_186
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f85f56a7ac81d3bL    # 0.010722

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_187

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_187
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_188
    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fcab30b5aa71583L    # 0.208589

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18e

    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x4006c68d32830a0bL    # 2.846949

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18d

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fa27e0ef99806f2L    # 0.036118

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18c

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4000599a1fd1569fL    # 2.043751

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18b

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4020e4662bae03b4L    # 8.446092

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_189

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_189
    invoke-virtual {p1, v8}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8695d91ab8e8eaL    # 0.011028

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_18a

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18a
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18b
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18d
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18e
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_18f
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40337905e1c15098L    # 19.472746

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_191

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f936c58eeae9ee4L    # 0.018968

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_190

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_190
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_191
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_192
    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fe5f7b9e060fe48L    # 0.68649

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19a

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4815a07b352a84L    # 7.35E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_193

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_193
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc0d3e920c069e8L    # 0.131467

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_199

    iget-wide v0, p1, Lhmx;->c:D

    const-wide v2, 0x3fc70e4da09cc31aL    # 0.180124

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_197

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f52be48a58b3f64L    # 0.001144

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_194

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_194
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f4f7d73c9257860L    # 9.61E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_196

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8faebc408d8ec9L    # 0.01547

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_195

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_195
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_196
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_197
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x40092bea4ebdd335L    # 3.146443

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_198

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_198
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_199
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19a
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc0bf65dbfceb79L    # 0.130841

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19d

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x4002dac57e23f24eL    # 2.356822

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19b

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19b
    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3fae4a9cdc443915L    # 0.059163

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19c

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19c
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19d
    const/16 v0, 0x34

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f114d2f5dbb9cfaL    # 6.6E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19e

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19e
    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc24adff822bbedL    # 0.14291

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a1

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40543c304039abf3L    # 80.940445

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a0

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x401fad55a3a08399L    # 7.919272

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_19f

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_19f
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a0
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a1
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a2
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f88c9752977c88eL    # 0.012103

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a5

    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4039ad7d7c2ca149L    # 25.677696

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a4

    invoke-virtual {p1, v6, v7}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x40206d84b1ab0857L    # 8.213903

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a3

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a6
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x405eed36deb95e5bL    # 123.706474

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a8
    iget-wide v0, p1, Lhmx;->e:D

    const-wide v2, 0x400bde68a0d349bfL    # 3.483598

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ae

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3ff82f30a4e379b7L    # 1.511521

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ad

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f8f83382e44b6e9L    # 0.015387

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1a9

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1a9
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fc41adea897635eL    # 0.15707

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ac

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4015cb51372a38b9L    # 5.448552

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1aa

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1aa
    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc405ff1d81f106L    # 0.156433

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ab

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ab
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ac
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ad
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ae
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fd196fa82e87d2cL    # 0.27484

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b5

    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x4041c1465e892253L    # 35.50996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1af

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1af
    const-wide v0, 0x3fefae147ae147aeL    # 0.99

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x404d1b84c271fff8L    # 58.214989

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b4

    const-wide v0, 0x3fa999999999999aL    # 0.05

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x3f9eb1c432ca57a8L    # 0.029975

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b0
    const-wide v0, 0x3fc999999999999aL    # 0.2

    invoke-virtual {p1, v0, v1}, Lhmx;->a(D)D

    move-result-wide v0

    const-wide v2, 0x4018197785729b28L    # 6.02487

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b3

    iget-wide v0, p1, Lhmx;->b:D

    const-wide v2, 0x3fe9e310dbf0563fL    # 0.808968

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b1
    iget-wide v0, p1, Lhmx;->d:D

    const-wide v2, 0x3fc759ee88df3733L    # 0.182432

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b5
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b6
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3fb0f25a250f8402L    # 0.066198

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c9

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x402e7dae81882adcL    # 15.245472

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c8

    invoke-virtual {p1, v5}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3fc10b417ca2120eL    # 0.133156

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b9

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fddf7d73c925786L    # 0.468252

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b8

    invoke-virtual {p1, v5}, Lhmx;->c(I)D

    move-result-wide v0

    const-wide v2, 0x3fc3f72f76e6106bL    # 0.155981

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1b7

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1b9
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x403802d0a2446306L    # 24.010996

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c5

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f092a737110e454L    # 4.8E-5

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bb

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x40321bb99d451fc5L    # 18.108301

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1ba

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1ba
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->h:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bb
    invoke-virtual {p1, v9}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f9ed17c5ef62f9dL    # 0.030096

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c4

    invoke-virtual {p1, v9}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f6b1b79d909f1f1L    # 0.003309

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bf

    invoke-virtual {p1, v8}, Lhmx;->b(I)D

    move-result-wide v0

    const-wide v2, 0x3f91c9b413986339L    # 0.017371

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1be

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7098d477bbf940L    # 0.004052

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bc

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bc
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f72253111f0c34cL    # 0.00443

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1bd

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bd
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1be
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1bf
    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x402bf2d1f1cfbb95L    # 13.974258

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c0

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c0
    const-wide v0, 0x3fee666666666666L    # 0.95

    invoke-virtual {p1, v0, v1}, Lhmx;->b(D)D

    move-result-wide v0

    const-wide v2, 0x40309032873bc904L    # 16.563271

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c1

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c1
    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f7950331e3a7daaL    # 0.00618

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c3

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f72b3cc4ac6cdafL    # 0.004566

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c2

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c2
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c3
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c4
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c5
    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lhmx;->a(I)D

    move-result-wide v0

    const-wide v2, 0x3f300e6afcce1c58L    # 2.45E-4

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c7

    invoke-virtual {p1}, Lhmx;->a()D

    move-result-wide v0

    const-wide v2, 0x4034fe2d1f1cfbb9L    # 20.992876

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1c6

    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->a:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c6
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c7
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->b:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c8
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0

    :cond_1c9
    new-instance v0, Lhmy;

    sget-object v1, Lhmz;->c:Lhmz;

    invoke-direct {v0, v1, v4}, Lhmy;-><init>(Lhmz;I)V

    goto/16 :goto_0
.end method
