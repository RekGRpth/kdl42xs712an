.class public final Leim;
.super Lizs;
.source "SourceFile"


# static fields
.field private static volatile d:[Leim;


# instance fields
.field public a:I

.field public b:Lein;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v1, p0, Leim;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Leim;->b:Lein;

    iput v1, p0, Leim;->c:I

    const/4 v0, -0x1

    iput v0, p0, Leim;->C:I

    return-void
.end method

.method public static c()[Leim;
    .locals 2

    sget-object v0, Leim;->d:[Leim;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Leim;->d:[Leim;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Leim;

    sput-object v0, Leim;->d:[Leim;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Leim;->d:[Leim;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Leim;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Leim;->a:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Leim;->b:Lein;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Leim;->b:Lein;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Leim;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Leim;->c:I

    invoke-static {v1, v2}, Lizn;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Leim;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leim;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Leim;->b:Lein;

    if-nez v0, :cond_1

    new-instance v0, Lein;

    invoke-direct {v0}, Lein;-><init>()V

    iput-object v0, p0, Leim;->b:Lein;

    :cond_1
    iget-object v0, p0, Leim;->b:Lein;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Leim;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Leim;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Leim;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_0
    iget-object v0, p0, Leim;->b:Lein;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Leim;->b:Lein;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget v0, p0, Leim;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Leim;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->c(II)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Leim;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Leim;

    iget v2, p0, Leim;->a:I

    iget v3, p1, Leim;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Leim;->b:Lein;

    if-nez v2, :cond_4

    iget-object v2, p1, Leim;->b:Lein;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Leim;->b:Lein;

    iget-object v3, p1, Leim;->b:Lein;

    invoke-virtual {v2, v3}, Lein;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Leim;->c:I

    iget v3, p1, Leim;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Leim;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Leim;->b:Lein;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Leim;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Leim;->b:Lein;

    invoke-virtual {v0}, Lein;->hashCode()I

    move-result v0

    goto :goto_0
.end method
