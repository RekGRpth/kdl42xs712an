.class public final Lafu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lafv;

.field public volatile b:Laes;

.field private final c:Lafw;

.field private volatile d:Z

.field private volatile e:Z

.field private f:J

.field private g:J

.field private h:Z


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lafu;->d:Z

    iput-boolean v0, p0, Lafu;->e:Z

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lafu;->f:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lafu;->h:Z

    iput-object v2, p0, Lafu;->c:Lafw;

    iput-object v2, p0, Lafu;->a:Lafv;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lafw;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lafu;->d:Z

    iput-boolean v2, p0, Lafu;->e:Z

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lafu;->f:J

    iput-boolean v3, p0, Lafu;->h:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "trackingId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lafu;->c:Lafw;

    new-instance v0, Lafv;

    invoke-direct {v0, v2}, Lafv;-><init>(B)V

    iput-object v0, p0, Lafu;->a:Lafv;

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "trackingId"

    invoke-virtual {v0, v1, p1}, Lafv;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "sampleRate"

    const-string v2, "100"

    invoke-virtual {v0, v1, v2}, Lafv;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "sessionControl"

    const-string v2, "start"

    invoke-virtual {v0, v1, v2}, Lafv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "useSecure"

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lafv;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized b()Z
    .locals 11

    const-wide/32 v5, 0x1d4c0

    const-wide/16 v9, 0x7d0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lafu;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lafu;->f:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    iget-wide v3, p0, Lafu;->g:J

    sub-long v3, v1, v3

    const-wide/16 v5, 0x0

    cmp-long v5, v3, v5

    if-lez v5, :cond_1

    const-wide/32 v5, 0x1d4c0

    iget-wide v7, p0, Lafu;->f:J

    add-long/2addr v3, v7

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lafu;->f:J

    :cond_1
    iput-wide v1, p0, Lafu;->g:J

    iget-wide v1, p0, Lafu;->f:J

    cmp-long v1, v1, v9

    if-ltz v1, :cond_2

    iget-wide v1, p0, Lafu;->f:J

    sub-long/2addr v1, v9

    iput-wide v1, p0, Lafu;->f:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    const-string v0, "Excessive tracking detected.  Tracking call ignored."

    invoke-static {v0}, Lafl;->f(Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-boolean v0, p0, Lafu;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tracker closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    sget-object v1, Lafg;->A:Lafg;

    invoke-virtual {v0, v1}, Laff;->a(Lafg;)V

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "referrer"

    invoke-virtual {v0, v1, p1}, Lafv;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 5

    invoke-virtual {p0}, Lafu;->a()V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    sget-object v1, Lafg;->c:Lafg;

    invoke-virtual {v0, v1}, Laff;->a(Lafg;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laff;->a(Z)V

    const-string v0, "event"

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "eventCategory"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "eventAction"

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "eventLabel"

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_0

    const-string v2, "eventValue"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {}, Laff;->a()Laff;

    move-result-object v2

    sget-object v3, Lafg;->Y:Lafg;

    invoke-virtual {v2, v3}, Laff;->a(Lafg;)V

    invoke-virtual {p0, v0, v1}, Lafu;->a(Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laff;->a(Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lafu;->e:Z

    if-nez p2, :cond_0

    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    :cond_0
    const-string v0, "hitType"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lafu;->a:Lafv;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lafv;->a(Ljava/util/Map;Ljava/lang/Boolean;)V

    invoke-direct {p0}, Lafu;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Too many hits sent too quickly, throttling invoked."

    invoke-static {v0}, Lafl;->f(Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lafu;->a:Lafv;

    invoke-virtual {v0}, Lafv;->a()V

    return-void

    :cond_1
    iget-object v0, p0, Lafu;->c:Lafw;

    iget-object v1, p0, Lafu;->a:Lafv;

    invoke-virtual {v1}, Lafv;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Lafw;->a(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Laff;->a()Laff;

    move-result-object v0

    sget-object v1, Lafg;->B:Lafg;

    invoke-virtual {v0, v1}, Laff;->a(Lafg;)V

    iget-object v0, p0, Lafu;->a:Lafv;

    const-string v1, "campaign"

    invoke-virtual {v0, v1, p1}, Lafv;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
