.class final Lll;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Llk;

.field private b:I


# direct methods
.method public constructor <init>(Llk;)V
    .locals 1

    iput-object p1, p0, Lll;->a:Llk;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lll;->b:I

    invoke-direct {p0}, Lll;->a()V

    return-void
.end method

.method private a()V
    .locals 5

    iget-object v0, p0, Lll;->a:Llk;

    iget-object v0, v0, Llk;->c:Llm;

    invoke-virtual {v0}, Llm;->t()Llq;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lll;->a:Llk;

    iget-object v0, v0, Llk;->c:Llm;

    invoke-virtual {v0}, Llm;->n()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llq;

    if-ne v0, v2, :cond_0

    iput v1, p0, Lll;->b:I

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lll;->b:I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Llq;
    .locals 3

    iget-object v0, p0, Lll;->a:Llk;

    iget-object v0, v0, Llk;->c:Llm;

    invoke-virtual {v0}, Llm;->n()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lll;->a:Llk;

    invoke-static {v0}, Llk;->a(Llk;)I

    move-result v0

    add-int/2addr v0, p1

    iget v2, p0, Lll;->b:I

    if-ltz v2, :cond_0

    iget v2, p0, Lll;->b:I

    if-lt v0, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llq;

    return-object v0
.end method

.method public final getCount()I
    .locals 2

    iget-object v0, p0, Lll;->a:Llk;

    iget-object v0, v0, Llk;->c:Llm;

    invoke-virtual {v0}, Llm;->n()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lll;->a:Llk;

    invoke-static {v1}, Llk;->a(Llk;)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lll;->b:I

    if-gez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lll;->a(I)Llq;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lll;->a:Llk;

    iget-object v0, v0, Llk;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lll;->a:Llk;

    iget v1, v1, Llk;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    check-cast v0, Lme;

    invoke-virtual {p0, p1}, Lll;->a(I)Llq;

    move-result-object v2

    invoke-interface {v0, v2}, Lme;->a(Llq;)V

    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    invoke-direct {p0}, Lll;->a()V

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
