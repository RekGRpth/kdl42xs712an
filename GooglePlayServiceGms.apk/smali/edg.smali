.class public final Ledg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgl;


# instance fields
.field private final a:Landroid/support/v4/view/ViewPager;

.field private final b:Ledf;

.field private final c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;Ledf;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ledg;->a:Landroid/support/v4/view/ViewPager;

    iput-object p2, p0, Ledg;->b:Ledf;

    iput-object p3, p0, Ledg;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    iget-object v0, p0, Ledg;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    return-void
.end method

.method public final a(IFI)V
    .locals 3

    iget-object v0, p0, Ledg;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(IFI)V

    iget-object v0, p0, Ledg;->b:Ledf;

    invoke-virtual {v0}, Ledf;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    instance-of v2, v0, Ledh;

    if-eqz v2, :cond_0

    check-cast v0, Ledh;

    invoke-interface {v0, p1, p2}, Ledh;->a(IF)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ledg;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->b(I)V

    packed-switch p1, :pswitch_data_0

    const-string v0, "GFragmentPageChangeListener"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPageScrollStateChanged(): unexpected state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->f(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Ledg;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v3

    iget-object v4, p0, Ledg;->b:Ledf;

    iget-object v0, v4, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    instance-of v1, v0, Ledh;

    if-eqz v1, :cond_0

    check-cast v0, Ledh;

    invoke-interface {v0}, Ledh;->Q()V

    goto :goto_0

    :cond_1
    if-ltz v3, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, v4, Ledf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    :goto_3
    invoke-static {v1}, Lbiq;->a(Z)V

    iget-object v0, v4, Ledf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
