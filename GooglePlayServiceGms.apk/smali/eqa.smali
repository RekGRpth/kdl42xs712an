.class public final Leqa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:J

.field public d:I

.field public e:I

.field private f:S

.field private g:D

.field private h:D

.field private i:F


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Leqa;->a:Ljava/lang/String;

    iput v3, p0, Leqa;->b:I

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Leqa;->c:J

    iput-short v2, p0, Leqa;->f:S

    iput v3, p0, Leqa;->d:I

    iput v2, p0, Leqa;->e:I

    return-void
.end method


# virtual methods
.method public final a()Lepz;
    .locals 12

    iget-object v0, p0, Leqa;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request ID not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Leqa;->b:I

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transitions types not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Leqa;->b:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    iget v0, p0, Leqa;->e:I

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Non-negative loitering delay needs to be set when transition types include GEOFENCE_TRANSITION_DWELLING."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-wide v0, p0, Leqa;->c:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expiration not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-short v0, p0, Leqa;->f:S

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Geofence region not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget v0, p0, Leqa;->d:I

    if-gez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Notification responsiveness should be nonnegative."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    iget-object v1, p0, Leqa;->a:Ljava/lang/String;

    iget v2, p0, Leqa;->b:I

    iget-wide v3, p0, Leqa;->g:D

    iget-wide v5, p0, Leqa;->h:D

    iget v7, p0, Leqa;->i:F

    iget-wide v8, p0, Leqa;->c:J

    iget v10, p0, Leqa;->d:I

    iget v11, p0, Leqa;->e:I

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/location/internal/ParcelableGeofence;-><init>(Ljava/lang/String;IDDFJII)V

    return-object v0
.end method

.method public final a(DDF)Leqa;
    .locals 1

    const/4 v0, 0x1

    iput-short v0, p0, Leqa;->f:S

    iput-wide p1, p0, Leqa;->g:D

    iput-wide p3, p0, Leqa;->h:D

    iput p5, p0, Leqa;->i:F

    return-object p0
.end method
