.class public final Lccg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcce;


# static fields
.field public static final a:Lccf;


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private final e:I

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/concurrent/Executor;

.field private i:J

.field private j:Z

.field private k:Lccl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcch;

    invoke-direct {v0}, Lcch;-><init>()V

    sput-object v0, Lccg;->a:Lccf;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lcci;

    invoke-direct {v2, p0}, Lcci;-><init>(Lccg;)V

    iput-object v2, p0, Lccg;->b:Ljava/lang/Runnable;

    new-instance v2, Lccj;

    invoke-direct {v2, p0}, Lccj;-><init>(Lccg;)V

    iput-object v2, p0, Lccg;->c:Ljava/lang/Runnable;

    invoke-static {v0}, Lcbp;->a(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lccg;->f:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lccg;->i:J

    iput-boolean v1, p0, Lccg;->j:Z

    sget-object v2, Lccl;->a:Lccl;

    iput-object v2, p0, Lccg;->k:Lccl;

    if-lez p2, :cond_0

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lccg;->d:Ljava/lang/Runnable;

    iput p2, p0, Lccg;->e:I

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lccg;->g:Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lccg;->h:Ljava/util/concurrent/Executor;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lccg;J)J
    .locals 0

    iput-wide p1, p0, Lccg;->i:J

    return-wide p1
.end method

.method static synthetic a(Lccg;Lccl;)Lccl;
    .locals 0

    iput-object p1, p0, Lccg;->k:Lccl;

    return-object p1
.end method

.method static synthetic a(Lccg;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lccg;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Lccg;)Lccl;
    .locals 1

    iget-object v0, p0, Lccg;->k:Lccl;

    return-object v0
.end method

.method static synthetic c(Lccg;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lccg;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lccg;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lccg;->j:Z

    return v0
.end method

.method static synthetic e(Lccg;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lccg;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lccg;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lccg;->h:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic g(Lccg;)I
    .locals 1

    iget v0, p0, Lccg;->e:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lccg;->j:Z

    if-nez v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lccg;->i:J

    sub-long v2, v0, v2

    iget v4, p0, Lccg;->e:I

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lccg;->j:Z

    iget-object v0, p0, Lccg;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcck;

    invoke-direct {v1, p0, v2, v3}, Lcck;-><init>(Lccg;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lccg;->c:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    iput-wide v0, p0, Lccg;->i:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    const-string v0, "RateLimitedExecutor"

    const-string v1, "Dropping request, as an updater is already scheduled."

    invoke-static {v0, v1}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lccg;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    const-string v0, "RateLimitedExecutor[owner=%s, scheduled=%s, lastUpdated=%s, lapseSinceLastUpdate=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lccg;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lccg;->j:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lccg;->i:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lccg;->i:J

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
