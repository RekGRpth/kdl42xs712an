.class public abstract Lfpc;
.super Lfpp;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfpp;-><init>()V

    return-void
.end method


# virtual methods
.method abstract L()Ljava/lang/CharSequence;
.end method

.method abstract M()Ljava/lang/CharSequence;
.end method

.method abstract N()Landroid/content/Intent;
.end method

.method protected final O()Lfpa;
    .locals 1

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfpa;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {p0, v0}, Lfpc;->a(Landroid/content/Context;)Lfpa;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfpc;->a(Landroid/widget/ListAdapter;)V

    :cond_0
    return-object v0
.end method

.method abstract a(Landroid/content/Context;)Lfpa;
.end method

.method public final a(Landroid/view/Menu;)V
    .locals 3

    const/16 v2, 0x64

    invoke-super {p0, p1}, Lfpp;->a(Landroid/view/Menu;)V

    const/4 v0, 0x0

    const v1, 0x7f0b0473    # com.google.android.gms.R.string.common_list_apps_menu_help

    invoke-interface {p1, v0, v2, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method public final a_(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lfpp;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lfpc;->N()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1, v0}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-class v2, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfpc;->a(Landroid/content/Intent;I)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lfpc;->a(Landroid/content/Intent;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfpa;

    return-object v0
.end method

.method public final c()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lfpc;->O()Lfpa;

    move-result-object v2

    iget-object v3, p0, Lfpc;->i:Lfob;

    iget v3, v3, Lfob;->b:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-super {p0, v1, v1}, Lah;->a(ZZ)V

    invoke-virtual {p0}, Lfpc;->L()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lfpc;->a(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lfpc;->i:Lfob;

    iget-object v1, v1, Lfob;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, v0}, Lfpa;->a(Ljava/util/Collection;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lfpc;->i:Lfob;

    iget-object v3, v3, Lfob;->c:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    invoke-super {p0, v0, v1}, Lah;->a(ZZ)V

    iget-object v0, p0, Lfpc;->i:Lfob;

    iget-object v0, v0, Lfob;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0, v1}, Lfpa;->a(Ljava/util/Collection;Z)V

    goto :goto_0

    :pswitch_2
    invoke-super {p0, v1, v1}, Lah;->a(ZZ)V

    const v0, 0x7f0b0385    # com.google.android.gms.R.string.plus_list_apps_no_accounts

    invoke-virtual {p0, v0}, Lfpc;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfpc;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lfpa;->b()V

    goto :goto_0

    :pswitch_3
    invoke-super {p0, v1, v1}, Lah;->a(ZZ)V

    invoke-virtual {p0}, Lfpc;->M()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfpc;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lfpa;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lfpp;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfpc;->P()V

    invoke-virtual {p0}, Lfpc;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfpa;

    invoke-virtual {v0, p3}, Lfpa;->a(I)Lfxh;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lfxh;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lfpc;->i:Lfob;

    iget-object v2, v2, Lfob;->a:Landroid/accounts/Account;

    invoke-interface {v1}, Lfxh;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lfxh;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lfxh;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lfxh;->i()Z

    move-result v6

    invoke-interface {v1}, Lfxh;->j()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1}, Lfxh;->k()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroid/content/Intent;

    const-string v10, "com.google.android.gms.plus.action.MANAGE_APP"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v9, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v9, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v9, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v9, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v9, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_IS_ASPEN"

    invoke-virtual {v9, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_SCOPES"

    invoke-virtual {v9, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_REVOKE_HANDLE"

    invoke-virtual {v9, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v2, 0x1

    invoke-virtual {v0, v9, v2}, Lo;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v2, p0, Lfpc;->i:Lfob;

    invoke-virtual {p0}, Lfpc;->K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-interface {v1}, Lfxh;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lbck;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-virtual {v2, v3, v0}, Lfob;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    sget-object v0, Lbck;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method
