.class public final Lhad;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgwv;


# instance fields
.field Y:Landroid/widget/TextView;

.field Z:Landroid/view/View;

.field a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

.field private aA:Ljava/lang/String;

.field private aB:Z

.field private aC:Lhah;

.field private aD:Ljava/lang/String;

.field private aE:I

.field private aF:Lhac;

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

.field private aK:Z

.field private aL:Luu;

.field private aM:Lut;

.field private aN:Luu;

.field private aO:Lut;

.field private final aP:Lhcb;

.field aa:Lgwr;

.field ab:Lgwr;

.field ac:Lion;

.field ad:Lhca;

.field ae:[Liof;

.field af:Lcom/google/android/gms/wallet/common/PaymentModel;

.field ag:Z

.field ah:Z

.field ai:Ljava/lang/String;

.field aj:Ljava/lang/String;

.field ak:Z

.field al:I

.field am:Lgzx;

.field an:Lipl;

.field ao:Lgxh;

.field ap:Lgvu;

.field private aq:Landroid/view/View;

.field private ar:Landroid/view/View;

.field private as:Landroid/view/View;

.field private at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private au:Landroid/accounts/Account;

.field private av:Ljava/lang/String;

.field private aw:Lioq;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/util/ArrayList;

.field private az:Ljava/util/ArrayList;

.field b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

.field c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field d:Lgxg;

.field e:Lgvt;

.field f:Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-boolean v0, p0, Lhad;->ag:Z

    iput-boolean v0, p0, Lhad;->ah:Z

    iput-object v1, p0, Lhad;->ai:Ljava/lang/String;

    iput-object v1, p0, Lhad;->aj:Ljava/lang/String;

    iput-boolean v0, p0, Lhad;->aH:Z

    iput-boolean v0, p0, Lhad;->aI:Z

    iput-boolean v0, p0, Lhad;->aK:Z

    const/4 v0, -0x1

    iput v0, p0, Lhad;->al:I

    new-instance v0, Lhae;

    invoke-direct {v0, p0}, Lhae;-><init>(Lhad;)V

    iput-object v0, p0, Lhad;->ao:Lgxh;

    new-instance v0, Lhaf;

    invoke-direct {v0, p0}, Lhaf;-><init>(Lhad;)V

    iput-object v0, p0, Lhad;->ap:Lgvu;

    new-instance v0, Lhag;

    invoke-direct {v0, p0}, Lhag;-><init>(Lhad;)V

    iput-object v0, p0, Lhad;->aP:Lhcb;

    return-void
.end method

.method private J()Z
    .locals 1

    iget-object v0, p0, Lhad;->aw:Lioq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhad;->ak:Z

    iput v0, p0, Lhad;->aE:I

    invoke-direct {p0, v0}, Lhad;->c(I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "inapp.ReviewPurchaseFragment.PurchaseOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhad;->aa:Lgwr;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "inapp.ReviewPurchaseFragment.PurchaseRequestNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhad;->ab:Lgwr;

    iget-object v0, p0, Lhad;->ad:Lhca;

    iget-object v1, p0, Lhad;->aP:Lhcb;

    invoke-interface {v0, v1}, Lhca;->a(Lhcb;)V

    iget-object v0, p0, Lhad;->aa:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhad;->aa:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :goto_0
    iget-object v0, p0, Lhad;->ab:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->ab:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lhad;->ad:Lhca;

    iget-object v1, p0, Lhad;->aP:Lhcb;

    iget v2, p0, Lhad;->al:I

    invoke-interface {v0, v1, v2}, Lhca;->a(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lhad;->al:I

    return-void

    :cond_1
    invoke-direct {p0}, Lhad;->P()V

    goto :goto_0
.end method

.method private L()V
    .locals 2

    iget v0, p0, Lhad;->al:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lhad;->ad:Lhca;

    iget-object v1, p0, Lhad;->aP:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lhad;->al:I

    :cond_0
    return-void
.end method

.method private M()Z
    .locals 1

    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lhad;->M()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhad;->aG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lhad;->c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method private O()V
    .locals 3

    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-boolean v1, p0, Lhad;->aG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhad;->aF:Lhac;

    iget-object v2, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-virtual {v1, v2, v0}, Lhac;->a(Lipv;Lioj;)Lion;

    move-result-object v0

    :goto_0
    invoke-direct {p0}, Lhad;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lion;->c:Liob;

    invoke-direct {p0, v1}, Lhad;->b(Liob;)V

    :cond_0
    iput-object v0, p0, Lhad;->ac:Lion;

    iget-object v1, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a()I

    move-result v1

    invoke-direct {p0, v1}, Lhad;->c(I)V

    invoke-direct {p0}, Lhad;->N()V

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhad;->a(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lhad;->aF:Lhac;

    invoke-virtual {v1, v0}, Lhac;->a(Lioj;)Lion;

    move-result-object v0

    goto :goto_0
.end method

.method private P()V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lhad;->an:Lipl;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lhad;->aP:Lhcb;

    iget-object v1, p0, Lhad;->an:Lipl;

    invoke-virtual {v0, v1}, Lhcb;->a(Lipl;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lhad;->aH:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v0, p0, Lhad;->aI:Z

    new-instance v1, Lipk;

    invoke-direct {v1}, Lipk;-><init>()V

    invoke-direct {p0}, Lhad;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhad;->av:Ljava/lang/String;

    iput-object v2, v1, Lipk;->b:Ljava/lang/String;

    :cond_2
    invoke-direct {p0}, Lhad;->J()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhad;->aw:Lioq;

    iput-object v2, v1, Lipk;->a:Lioq;

    :cond_3
    iget-object v2, p0, Lhad;->ad:Lhca;

    invoke-interface {v2, v1}, Lhca;->a(Lipk;)V

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v3, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    iget-object v1, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    if-nez v1, :cond_4

    const v0, 0x7f0b0118    # com.google.android.gms.R.string.wallet_retrieving_wallet_information

    :cond_4
    invoke-direct {p0, v3, v0}, Lhad;->a(ZI)V

    goto :goto_0
.end method

.method private Q()V
    .locals 1

    iget-object v0, p0, Lhad;->aC:Lhah;

    invoke-interface {v0}, Lhah;->g()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhad;->aK:Z

    return-void
.end method

.method static synthetic a(Lhad;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    iget-object v0, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lhad;
    .locals 3

    const-string v0, "buyFlowConfig must not be null or empty"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account must not be null or empty"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lhad;

    invoke-direct {v0}, Lhad;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "extraJwt"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhad;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lhad;
    .locals 3

    const-string v0, "buyFlowConfig must not be null or empty"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account must not be null or empty"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lhad;

    invoke-direct {v0}, Lhad;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "extraPcid"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "extraInfoText"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhad;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method static synthetic a(Lhad;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lhad;->az:Ljava/util/ArrayList;

    return-object p1
.end method

.method static a(Lipl;)Ljava/util/ArrayList;
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_2

    iget-object v0, p0, Lipl;->b:[Lion;

    array-length v0, v0

    if-lez v0, :cond_2

    iget-object v2, p0, Lipl;->b:[Lion;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    iget-object v5, v4, Lion;->b:Lipv;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lion;->b:Lipv;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v5, v4, Lion;->a:Lioj;

    if-eqz v5, :cond_1

    iget-object v5, v4, Lion;->a:Lioj;

    iget-object v5, v5, Lioj;->e:Lipv;

    if-eqz v5, :cond_1

    iget-object v4, v4, Lion;->a:Lioj;

    iget-object v4, v4, Lioj;->e:Lipv;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private a(Landroid/text/Spanned;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lhad;Liob;)V
    .locals 0

    invoke-direct {p0, p1}, Lhad;->a(Liob;)V

    return-void
.end method

.method static synthetic a(Lhad;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lhad;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhad;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lhad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhad;[Liof;Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lhad;->ae:[Liof;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->a(Landroid/text/Spanned;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0b011a    # com.google.android.gms.R.string.wallet_before_terms_of_service

    invoke-virtual {p0, v1}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0b011b    # com.google.android.gms.R.string.wallet_terms_of_service

    invoke-virtual {p0, v1}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lgth;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, Lhad;->a(Landroid/text/Spanned;)V

    goto :goto_0
.end method

.method static synthetic a(Lhad;[Lion;)V
    .locals 10

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lhad;->aF:Lhac;

    iget-boolean v1, p0, Lhad;->aG:Z

    invoke-virtual {v0, p1, v1}, Lhac;->a([Lion;Z)V

    iget-boolean v0, p0, Lhad;->aG:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    iget-object v1, p0, Lhad;->aF:Lhac;

    invoke-virtual {v1}, Lhac;->a()[Lipv;

    move-result-object v4

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lhad;->ah:Z

    if-nez v1, :cond_0

    invoke-static {v4}, Lgth;->a([Lipv;)Lipv;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lhad;->aF:Lhac;

    invoke-virtual {v1, v0}, Lhac;->a(Lipv;)[Lioj;

    move-result-object v1

    iget-object v5, p0, Lhad;->e:Lgvt;

    invoke-interface {v5, v4}, Lgvt;->a([Lipv;)V

    iget-object v4, p0, Lhad;->e:Lgvt;

    invoke-interface {v4, v0}, Lgvt;->a(Lipv;)V

    iget-object v4, p0, Lhad;->e:Lgvt;

    invoke-interface {v4, v3}, Lgvt;->setVisibility(I)V

    iget-object v4, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v4, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    if-eqz v1, :cond_1

    array-length v6, v0

    move v5, v3

    :goto_1
    if-ge v5, v6, :cond_8

    aget-object v4, v0, v5

    iget-object v7, v4, Lioj;->a:Ljava/lang/String;

    iget-object v8, v1, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v4, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    const/4 v1, 0x1

    move v9, v1

    move-object v1, v4

    move v4, v9

    :goto_2
    if-nez v4, :cond_1

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    move-object v1, v2

    :cond_1
    if-nez v1, :cond_2

    iget-boolean v4, p0, Lhad;->ag:Z

    if-nez v4, :cond_2

    invoke-static {v0}, Lgth;->a([Lioj;)Lioj;

    move-result-object v1

    :cond_2
    invoke-static {v1}, Lhad;->b(Lioj;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    move-object v1, v2

    :cond_3
    array-length v4, v0

    move v2, v3

    :goto_3
    if-ge v2, v4, :cond_6

    aget-object v5, v0, v2

    invoke-static {v5}, Lhad;->b(Lioj;)Z

    move-result v5

    if-nez v5, :cond_6

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhad;->aF:Lhac;

    invoke-virtual {v0}, Lhac;->b()[Lioj;

    move-result-object v0

    goto :goto_0

    :cond_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lhad;->d:Lgxg;

    invoke-interface {v2, v0}, Lgxg;->a([Lioj;)V

    iget-object v0, p0, Lhad;->d:Lgxg;

    invoke-interface {v0, v1}, Lgxg;->a(Lioj;)V

    iget-object v0, p0, Lhad;->d:Lgxg;

    invoke-interface {v0, v3}, Lgxg;->setVisibility(I)V

    iget-object v0, p0, Lhad;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhad;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_7
    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-direct {p0}, Lhad;->O()V

    iget-object v0, p0, Lhad;->ar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_8
    move v4, v3

    goto :goto_2
.end method

.method private a(Liob;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhad;->am:Lgzx;

    invoke-interface {v0, p1}, Lgzx;->a(Liob;)V

    iget-object v0, p0, Lhad;->Z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setVisibility(I)V

    iget-object v0, p0, Lhad;->as:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lhad;->aC:Lhah;

    invoke-interface {v0, p1, p2}, Lhah;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    iput-boolean v3, p0, Lhad;->aK:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "purchase_failed"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhad;->aL:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aM:Lut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aL:Luu;

    iget-object v1, p0, Lhad;->aM:Lut;

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->aL:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lhad;->aL:Luu;

    iput-object v5, p0, Lhad;->aM:Lut;

    :cond_0
    return-void
.end method

.method private a(ZI)V
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lhad;->aE:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lhad;->aE:I

    invoke-direct {p0, p2}, Lhad;->c(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lhad;Z)Z
    .locals 0

    iput-boolean p1, p0, Lhad;->aG:Z

    return p1
.end method

.method static synthetic a(Lioj;)Z
    .locals 1

    invoke-static {p0}, Lhad;->b(Lioj;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lhad;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lhad;->au:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic b(Lhad;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lhad;->aD:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lhad;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lhad;->ay:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b(Liob;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhad;->am:Lgzx;

    invoke-interface {v0, p1}, Lgzx;->b(Liob;)V

    iget-object v0, p0, Lhad;->Z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setVisibility(I)V

    iget-object v0, p0, Lhad;->as:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lhad;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lhad;->av:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lhad;Z)Z
    .locals 0

    iput-boolean p1, p0, Lhad;->aB:Z

    return p1
.end method

.method private static b(Lioj;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {p0}, Lgth;->e(Lioj;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lhad;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lhad;->aA:Ljava/lang/String;

    return-object p1
.end method

.method private c(I)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lhad;->aE:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lhad;->M()Z

    move-result v3

    if-eq v0, v3, :cond_1

    if-eqz v0, :cond_4

    iget-object v1, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v1, p0, Lhad;->d:Lgxg;

    invoke-interface {v1, v2}, Lgxg;->setEnabled(Z)V

    iget-boolean v1, p0, Lhad;->aG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhad;->e:Lgvt;

    invoke-interface {v1, v2}, Lgvt;->setEnabled(Z)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lhad;->N()V

    :cond_1
    if-eqz v0, :cond_2

    if-nez p1, :cond_5

    :cond_2
    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->b()V

    :goto_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->setVisibility(I)V

    iget-object v2, p0, Lhad;->d:Lgxg;

    invoke-interface {v2, v1}, Lgxg;->setEnabled(Z)V

    iget-boolean v2, p0, Lhad;->aG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhad;->e:Lgvt;

    invoke-interface {v2, v1}, Lgvt;->setEnabled(Z)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;->a(I)V

    goto :goto_2
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lhad;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iput-object p1, p0, Lhad;->aj:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lhad;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhad;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lhad;)Z
    .locals 1

    iget-boolean v0, p0, Lhad;->aB:Z

    return v0
.end method

.method static synthetic d(Lhad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhad;->aD:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lhad;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lhad;->e(Ljava/lang/String;)V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    const v0, 0x7f0b012f    # com.google.android.gms.R.string.wallet_payment_failed_error_message

    invoke-virtual {p0, v0}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lhad;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lhad;)Lioq;
    .locals 1

    iget-object v0, p0, Lhad;->aw:Lioq;

    return-object v0
.end method

.method static synthetic e(Lhad;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lhad;->d(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lhad;->aC:Lhah;

    invoke-interface {v0, p1}, Lhah;->b(Ljava/lang/String;)V

    iput-boolean v3, p0, Lhad;->aK:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "purchase_success"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lhad;->aL:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aM:Lut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aL:Luu;

    iget-object v1, p0, Lhad;->aM:Lut;

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "click_to_activity_result"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->aL:Luu;

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iput-object v5, p0, Lhad;->aL:Luu;

    iput-object v5, p0, Lhad;->aM:Lut;

    :cond_0
    return-void
.end method

.method static synthetic f(Lhad;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lhad;->az:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lhad;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lhad;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lhad;)V
    .locals 0

    invoke-direct {p0}, Lhad;->O()V

    return-void
.end method

.method static synthetic h(Lhad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhad;->aA:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lhad;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lhad;->ay:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic j(Lhad;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhad;->aI:Z

    return v0
.end method

.method static synthetic k(Lhad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhad;->ax:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lhad;)Z
    .locals 1

    invoke-direct {p0}, Lhad;->b()Z

    move-result v0

    return v0
.end method

.method static synthetic m(Lhad;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lhad;->a(ZI)V

    return-void
.end method

.method static synthetic n(Lhad;)Luu;
    .locals 1

    iget-object v0, p0, Lhad;->aN:Luu;

    return-object v0
.end method

.method static synthetic o(Lhad;)Lut;
    .locals 1

    iget-object v0, p0, Lhad;->aO:Lut;

    return-object v0
.end method

.method static synthetic p(Lhad;)Luu;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhad;->aN:Luu;

    return-object v0
.end method

.method static synthetic q(Lhad;)Lut;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lhad;->aO:Lut;

    return-object v0
.end method

.method static synthetic r(Lhad;)V
    .locals 1

    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic s(Lhad;)V
    .locals 0

    invoke-direct {p0}, Lhad;->P()V

    return-void
.end method

.method static synthetic t(Lhad;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhad;->aH:Z

    return v0
.end method

.method static synthetic u(Lhad;)Z
    .locals 1

    iget-boolean v0, p0, Lhad;->aI:Z

    return v0
.end method

.method static synthetic v(Lhad;)V
    .locals 3

    iget-object v0, p0, Lhad;->ab:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhad;->ab:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhad;->ab:Lgwr;

    iget-object v0, p0, Lhad;->ab:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhad;->ab:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "inapp.ReviewPurchaseFragment.PurchaseRequestNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic w(Lhad;)V
    .locals 3

    iget-object v0, p0, Lhad;->aa:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhad;->aa:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhad;->aa:Lgwr;

    iget-object v0, p0, Lhad;->aa:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhad;->aa:Lgwr;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v2, "inapp.ReviewPurchaseFragment.PurchaseOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const/4 v1, 0x0

    const v5, 0x7f0b012c    # com.google.android.gms.R.string.wallet_accept_label

    const/4 v2, 0x0

    const v0, 0x7f040136    # com.google.android.gms.R.layout.wallet_fragment_review_purchase

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhad;->aq:Landroid/view/View;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033b    # com.google.android.gms.R.id.butter_bar_text

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhad;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02fe    # com.google.android.gms.R.id.prog_bar_view

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iput-object v0, p0, Lhad;->b:Lcom/google/android/gms/wallet/common/ui/ProgressBarView;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02fb    # com.google.android.gms.R.id.instrument_selector

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgxg;

    iput-object v0, p0, Lhad;->d:Lgxg;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033a    # com.google.android.gms.R.id.pay_to_name

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhad;->Z:Landroid/view/View;

    iget-object v0, p0, Lhad;->d:Lgxg;

    iget-object v3, p0, Lhad;->ao:Lgxh;

    invoke-interface {v0, v3}, Lgxg;->a(Lgxh;)V

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a0322    # com.google.android.gms.R.id.address_selector

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lgvt;

    iput-object v0, p0, Lhad;->e:Lgvt;

    iget-object v0, p0, Lhad;->e:Lgvt;

    iget-object v3, p0, Lhad;->ap:Lgvu;

    invoke-interface {v0, v3}, Lgvt;->a(Lgvu;)V

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02f8    # com.google.android.gms.R.id.enrollment_text

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhad;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033c    # com.google.android.gms.R.id.merchant_name

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhad;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lhad;->c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033d    # com.google.android.gms.R.id.cart_details

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iput-object v0, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    iget-object v0, p0, Lhad;->ax:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02f2    # com.google.android.gms.R.id.info_text

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhad;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lhad;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lhad;->ax:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a02fa    # com.google.android.gms.R.id.instrument_error_text

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhad;->Y:Landroid/widget/TextView;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033f    # com.google.android.gms.R.id.spacer

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhad;->ar:Landroid/view/View;

    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    const v3, 0x7f0a033e    # com.google.android.gms.R.id.details_separator

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhad;->as:Landroid/view/View;

    iget-object v0, p0, Lhad;->c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lhad;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v1, "com.google.android.libraries.inapp.EXTRA_UI_TEMPLATE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v1, "com.google.android.libraries.inapp.EXTRA_USAGE_UNIT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v1, "com.google.android.gms.wallet.freeUsageAmount"

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "com.google.android.gms.wallet.usageDiscountMessage"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    packed-switch v3, :pswitch_data_0

    new-instance v0, Lgzz;

    iget-object v1, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v0, v1}, Lgzz;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V

    iput-object v0, p0, Lhad;->am:Lgzx;

    :cond_1
    :goto_1
    iget-object v0, p0, Lhad;->aq:Landroid/view/View;

    return-object v0

    :pswitch_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v1, Lhai;

    iget-object v3, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v1, v3, v2, v0}, Lhai;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;ILjava/lang/String;)V

    iput-object v1, p0, Lhad;->am:Lgzx;

    :goto_2
    iget-object v0, p0, Lhad;->c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {p0, v5}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lhai;

    iget-object v3, p0, Lhad;->a:Lcom/google/android/gms/wallet/ia/CartDetailsView;

    invoke-direct {v0, v3, v2, v1}, Lhai;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;II)V

    iput-object v0, p0, Lhad;->am:Lgzx;

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lhad;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhad;->c:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {p0, v5}, Lhad;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    move-object v0, v1

    move v3, v2

    move v1, v2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final a()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lhad;->aL:Luu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aM:Lut;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Luu;

    const-string v1, "purchase"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhad;->aL:Luu;

    iget-object v0, p0, Lhad;->aL:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhad;->aM:Lut;

    :cond_1
    new-instance v1, Lipm;

    invoke-direct {v1}, Lipm;-><init>()V

    invoke-direct {p0}, Lhad;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhad;->ac:Lion;

    iget-object v0, v0, Lion;->c:Liob;

    iget-object v0, v0, Liob;->a:Ljava/lang/String;

    iput-object v0, v1, Lipm;->b:Ljava/lang/String;

    iget-object v0, p0, Lhad;->av:Ljava/lang/String;

    iput-object v0, v1, Lipm;->e:Ljava/lang/String;

    :cond_2
    :goto_0
    iget-object v0, p0, Lhad;->ae:[Liof;

    iget-object v2, p0, Lhad;->ae:[Liof;

    array-length v2, v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liof;

    iput-object v0, v1, Lipm;->c:[Liof;

    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lhad;->a(ZI)V

    iget-object v0, p0, Lhad;->ad:Lhca;

    invoke-interface {v0, v1}, Lhca;->a(Lipm;)V

    iget-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v3, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    return-void

    :cond_3
    invoke-direct {p0}, Lhad;->J()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhad;->aw:Lioq;

    iput-object v0, v1, Lipm;->a:Lioq;

    new-instance v0, Lior;

    invoke-direct {v0}, Lior;-><init>()V

    iput-object v0, v1, Lipm;->d:Lior;

    iget-object v0, v1, Lipm;->d:Lior;

    iget-object v2, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    iget-object v2, v2, Lioj;->a:Ljava/lang/String;

    iput-object v2, v0, Lior;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-direct {p0, v1, v1}, Lhad;->a(ZI)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lhad;->P()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lhad;->Q()V

    goto :goto_0

    :cond_0
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    iput-boolean v3, p0, Lhad;->aH:Z

    packed-switch p1, :pswitch_data_0

    :goto_0
    iget-boolean v0, p0, Lhad;->aK:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "review_purchase"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lhad;->ak:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhad;->K()V

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    if-ne p2, v0, :cond_1

    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully added an instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v2, Lioj;

    invoke-static {p3, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v1, p0, Lhad;->an:Lipl;

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled adding an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    packed-switch p2, :pswitch_data_1

    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully updated an instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lioj;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v1, p0, Lhad;->an:Lipl;

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled updating an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_4
    if-ne p2, v0, :cond_4

    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully upgraded instrument"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lioj;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v1, p0, Lhad;->an:Lipl;

    invoke-static {v0}, Lgth;->d(Lioj;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_2
    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    goto :goto_2

    :cond_4
    if-nez p2, :cond_5

    invoke-direct {p0}, Lhad;->Q()V

    goto/16 :goto_0

    :cond_5
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    if-ne p2, v0, :cond_6

    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully signed up for Wallet"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lhad;->an:Lipl;

    invoke-direct {p0}, Lhad;->J()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lhad;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    if-nez p2, :cond_8

    invoke-direct {p0}, Lhad;->Q()V

    :cond_7
    :goto_3
    invoke-direct {p0, v3, v3}, Lhad;->a(ZI)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_6
    packed-switch p2, :pswitch_data_2

    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an address resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully added an address"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lipv;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v1, p0, Lhad;->an:Lipl;

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    goto/16 :goto_0

    :pswitch_8
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled adding an address"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_9
    packed-switch p2, :pswitch_data_3

    const-string v0, "ReviewPurchaseFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an address resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_a
    const-string v0, "ReviewPurchaseFragment"

    const-string v2, "Successfully updated an address"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.gms.wallet.address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const-class v2, Lipv;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    iput-object v1, p0, Lhad;->an:Lipl;

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    goto/16 :goto_0

    :pswitch_b
    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "User canceled updating an address"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_9
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    :try_start_0
    check-cast p1, Lhah;

    iput-object p1, p0, Lhad;->aC:Lhah;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lhad;->au:Landroid/accounts/Account;

    const-string v0, "extraJwt"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhad;->av:Ljava/lang/String;

    const-string v0, "extraPcid"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lioq;

    invoke-direct {v2}, Lioq;-><init>()V

    iput-object v2, p0, Lhad;->aw:Lioq;

    iget-object v2, p0, Lhad;->aw:Lioq;

    iput-object v0, v2, Lioq;->a:Ljava/lang/String;

    :cond_0
    const-string v2, "extraInfoText"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhad;->ax:Ljava/lang/String;

    iget-object v1, p0, Lhad;->av:Ljava/lang/String;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "Cannot specify both PCID and JWT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lhad;->ad:Lhca;

    if-nez v0, :cond_2

    new-instance v0, Lhbu;

    const/4 v1, 0x1

    iget-object v2, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lhad;->au:Landroid/accounts/Account;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v4}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lhbu;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v0, p0, Lhad;->ad:Lhca;

    iget-object v0, p0, Lhad;->ad:Lhca;

    invoke-interface {v0}, Lhca;->a()V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string v1, "ReviewPurchaseFragment"

    const-string v2, "Activity must implement OnPurchaseEndedListener interface!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0

    :cond_3
    iget-object v1, p0, Lhad;->av:Ljava/lang/String;

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    const-string v0, "ReviewPurchaseFragment"

    const-string v1, "Cannot leave out both PCID and JWT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_INVALID_PARAMETER"

    invoke-direct {p0, v0}, Lhad;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhad;->aH:Z

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->a(Landroid/content/Intent;I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v1, 0x7f050011    # com.google.android.gms.R.anim.wallet_push_up_in

    const v2, 0x7f05000f    # com.google.android.gms.R.anim.wallet_hold

    invoke-virtual {v0, v1, v2}, Lo;->overridePendingTransition(II)V

    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->Y:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lhad;->Y:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iput-object p1, p0, Lhad;->ai:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lhad;->Y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhad;->Y:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lhad;->q()V

    if-eqz p1, :cond_2

    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lhad;->aH:Z

    const-string v0, "purchaseOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "purchaseOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lipl;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipl;

    iput-object v0, p0, Lhad;->an:Lipl;

    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhad;->al:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Luu;

    const-string v1, "get_purchase_options"

    invoke-direct {v0, v1}, Luu;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lhad;->aN:Luu;

    iget-object v0, p0, Lhad;->aN:Luu;

    invoke-virtual {v0}, Luu;->a()Lut;

    move-result-object v0

    iput-object v0, p0, Lhad;->aO:Lut;

    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhad;->aH:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "review_purchase"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    new-instance v0, Lhac;

    invoke-direct {v0}, Lhac;-><init>()V

    iput-object v0, p0, Lhad;->aF:Lhac;

    iget-object v0, p0, Lhad;->aj:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhad;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lhad;->ai:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lhad;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lhad;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->av:Ljava/lang/String;

    iget-object v1, p0, Lhad;->at:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v0, v1}, Lgsw;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    iput-object v0, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    iget-object v0, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->b()Liob;

    move-result-object v0

    invoke-direct {p0, v0}, Lhad;->a(Liob;)V

    iget-object v0, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->b()Liob;

    move-result-object v0

    invoke-direct {p0, v0}, Lhad;->b(Liob;)V

    iget-object v0, p0, Lhad;->aJ:Lcom/google/android/gms/wallet/common/DisplayHints;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/DisplayHints;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lhad;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lhad;->L()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lhad;->al:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "model"

    iget-object v1, p0, Lhad;->af:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lhad;->aH:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lhad;->an:Lipl;

    if-eqz v0, :cond_0

    const-string v0, "purchaseOptionsPostResponse"

    iget-object v1, p0, Lhad;->an:Lipl;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lhad;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lhad;->a()V

    return-void
.end method

.method public final w()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-boolean v0, p0, Lhad;->aH:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhad;->ak:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lhad;->K()V

    goto :goto_0
.end method

.method public final x()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lhad;->L()V

    return-void
.end method
