.class public final Lgwr;
.super Lboc;
.source "SourceFile"


# instance fields
.field private Y:Lgwv;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lboc;-><init>()V

    return-void
.end method

.method public static J()Lgwr;
    .locals 4

    const/4 v0, 0x1

    const v1, 0x7f0b0114    # com.google.android.gms.R.string.wallet_title_possibly_recoverable_error_dialog

    const v2, 0x7f0b0115    # com.google.android.gms.R.string.wallet_body_instrument_possibly_recoverable_error_dialog

    const/16 v3, 0x3e9

    invoke-static {v0, v1, v2, v3}, Lgwr;->a(IIII)Lgwr;

    move-result-object v0

    return-object v0
.end method

.method public static K()Lgwr;
    .locals 4

    const/4 v0, 0x1

    const v1, 0x7f0b0114    # com.google.android.gms.R.string.wallet_title_possibly_recoverable_error_dialog

    const v2, 0x7f0b0116    # com.google.android.gms.R.string.wallet_body_address_possibly_recoverable_error_dialog

    const/16 v3, 0x3e9

    invoke-static {v0, v1, v2, v3}, Lgwr;->a(IIII)Lgwr;

    move-result-object v0

    return-object v0
.end method

.method private static a(IIII)Lgwr;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "buttonMode"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "titleId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "messageId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lgwr;

    invoke-direct {v1}, Lgwr;-><init>()V

    invoke-virtual {v1, v0}, Lgwr;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)Lgwr;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "buttonMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "errorCode"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lgwr;

    invoke-direct {v1}, Lgwr;-><init>()V

    invoke-virtual {v1, v0}, Lgwr;->g(Landroid/os/Bundle;)V

    return-object v1
.end method

.method static synthetic a(Lgwr;)Lgwv;
    .locals 1

    iget-object v0, p0, Lgwr;->Y:Lgwv;

    return-object v0
.end method

.method public static c(I)Lgwr;
    .locals 3

    const v0, 0x7f0b0179    # com.google.android.gms.R.string.wallet_network_error_title

    const v1, 0x7f0b017a    # com.google.android.gms.R.string.wallet_network_error_message

    const/16 v2, 0x3e8

    invoke-static {p0, v0, v1, v2}, Lgwr;->a(IIII)Lgwr;

    move-result-object v0

    return-object v0
.end method

.method public static d(I)Lgwr;
    .locals 3

    const/4 v0, 0x2

    const v1, 0x7f0b0179    # com.google.android.gms.R.string.wallet_network_error_title

    const v2, 0x7f0b017a    # com.google.android.gms.R.string.wallet_network_error_message

    invoke-static {v0, v1, v2, p0}, Lgwr;->a(IIII)Lgwr;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lgwv;)V
    .locals 0

    iput-object p1, p0, Lgwr;->Y:Lgwv;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    const/4 v6, 0x1

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v0, "titleId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "titleId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    :goto_0
    const-string v0, "messageId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "messageId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    :goto_1
    const-string v0, "buttonMode"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v6, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgwr;->b(Z)V

    const v0, 0x7f0b017b    # com.google.android.gms.R.string.wallet_ok

    new-instance v3, Lgws;

    invoke-direct {v3, p0, v1}, Lgws;-><init>(Lgwr;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_0
    :goto_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_1
    const-string v0, "title"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    :cond_2
    new-instance v3, Landroid/text/SpannableString;

    const-string v0, "message"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f040156    # com.google.android.gms.R.layout.wallet_view_dialog_body_text

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0xf

    invoke-static {v0, v3}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    :cond_3
    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    invoke-virtual {p0, v6}, Lgwr;->b(Z)V

    const v0, 0x7f0b017c    # com.google.android.gms.R.string.wallet_retry

    new-instance v3, Lgwt;

    invoke-direct {v3, p0, v1}, Lgwt;-><init>(Lgwr;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b014c    # com.google.android.gms.R.string.wallet_cancel

    new-instance v3, Lgwu;

    invoke-direct {v3, p0, v1}, Lgwu;-><init>(Lgwr;Landroid/os/Bundle;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 4

    invoke-super {p0, p1}, Lboc;->onCancel(Landroid/content/DialogInterface;)V

    iget-object v0, p0, Lgwr;->Y:Lgwv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgwr;->Y:Lgwv;

    const/4 v1, 0x2

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "errorCode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lgwv;->a(II)V

    :cond_0
    return-void
.end method
