.class public final Lfyb;
.super Landroid/text/style/ClickableSpan;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lfyc;

.field private final c:Lfyd;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfyc;Lfyd;)V
    .locals 0

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    iput-object p1, p0, Lfyb;->a:Landroid/app/Activity;

    iput-object p2, p0, Lfyb;->b:Lfyc;

    iput-object p3, p0, Lfyb;->c:Lfyd;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lfyb;->c:Lfyd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyb;->c:Lfyd;

    iget-object v1, v0, Lfyd;->a:Landroid/content/Context;

    iget-object v2, v0, Lfyd;->b:Ljava/lang/String;

    iget-object v3, v0, Lfyd;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, v0, Lfyd;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, v0, Lfyd;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v0}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfyb;->a:Landroid/app/Activity;

    iget-object v1, p0, Lfyb;->b:Lfyc;

    iget-object v1, v1, Lfyc;->a:Landroid/content/Intent;

    iget-object v2, p0, Lfyb;->b:Lfyc;

    iget v2, v2, Lfyc;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    iget-object v0, p0, Lfyb;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110014    # com.google.android.gms.R.bool.plus_links_underlined

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    const v1, 0x7f0c011c    # com.google.android.gms.R.color.plus_oob_link_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    return-void
.end method
