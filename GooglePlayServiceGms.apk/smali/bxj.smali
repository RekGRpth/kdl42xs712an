.class public final Lbxj;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V
    .locals 0

    iput-object p1, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lbxj;-><init>(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    new-instance v0, Lcbm;

    iget-object v1, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbm;-><init>(Lcoy;)V

    invoke-virtual {v0}, Lcbm;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->d(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcco;

    iget-object v1, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcco;-><init>(Lcoy;)V

    iget-object v1, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    const v2, 0x7f0b0075    # com.google.android.gms.R.string.drive_storage_management_clear_data_failed

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcco;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b006a    # com.google.android.gms.R.string.drive_storage_management_reclaim_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    iget-object v0, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lbxj;->a:Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;->c(Lcom/google/android/gms/drive/data/ui/DriveManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b0074    # com.google.android.gms.R.string.drive_storage_management_reclaiming

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method
