.class public abstract Ldwx;
.super Ldve;
.source "SourceFile"


# instance fields
.field public final e:I

.field public final f:I

.field private g:Ljava/util/Stack;

.field private h:Ljava/util/Stack;

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Ldwx;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 6

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Ldve;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Ldwx;->g:Ljava/util/Stack;

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Ldwx;->h:Ljava/util/Stack;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    if-gtz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numColumns must be at least 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v1

    const-string v3, "MultiColDBufferAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to find resource: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v0

    goto :goto_0

    :cond_0
    iput v1, p0, Ldwx;->e:I

    if-lez p3, :cond_1

    :try_start_1
    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_1
    if-gtz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxNumRows must be at least 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v1

    const-string v3, "MultiColDBufferAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to find resource: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    iput v0, p0, Ldwx;->f:I

    sget v0, Lwy;->l:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Ldwx;->j:I

    iput v0, p0, Ldwx;->i:I

    return-void
.end method


# virtual methods
.method protected final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    const/4 v4, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, -0x1

    const/4 v2, 0x0

    if-nez p2, :cond_3

    new-instance p2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Ldwx;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v9, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget v0, p0, Ldwx;->i:I

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v1

    iget v3, p0, Ldwx;->j:I

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v5

    invoke-virtual {p2, v0, v1, v3, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_0
    iget v0, p0, Ldwx;->e:I

    mul-int v5, p1, v0

    iget-object v0, p0, Ldwx;->c:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v6

    move v1, v2

    :goto_1
    iget v0, p0, Ldwx;->e:I

    if-ge v1, v0, :cond_a

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    add-int v7, v5, v1

    if-ge v7, v6, :cond_0

    if-gez v7, :cond_4

    :cond_0
    move v3, v4

    :goto_2
    if-eqz v3, :cond_6

    instance-of v3, v0, Ldwy;

    if-nez v3, :cond_2

    if-eqz v0, :cond_1

    iget-object v3, p0, Ldwx;->g:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    :cond_1
    iget-object v0, p0, Ldwx;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Ldwx;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwy;

    :goto_3
    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_2
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    check-cast p2, Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    new-instance v0, Ldwy;

    iget-object v3, p0, Ldwx;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Ldwy;-><init>(Landroid/content/Context;)V

    goto :goto_3

    :cond_6
    iget-object v3, p0, Ldwx;->c:Lbgo;

    invoke-virtual {v3, v7}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v8, v0, Ldwy;

    if-eqz v8, :cond_9

    iget-object v8, p0, Ldwx;->h:Ljava/util/Stack;

    check-cast v0, Ldwy;

    invoke-virtual {v8, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    iget-object v0, p0, Ldwx;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Ldwx;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_5
    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_7
    :goto_6
    iget-object v8, p0, Ldwx;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v8, v7, v3}, Ldwx;->a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_4

    :cond_8
    iget-object v0, p0, Ldwx;->a:Landroid/content/Context;

    invoke-virtual {p0}, Ldwx;->l()Landroid/view/View;

    move-result-object v0

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v2, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_5

    :cond_9
    if-nez v0, :cond_7

    iget-object v0, p0, Ldwx;->a:Landroid/content/Context;

    invoke-virtual {p0}, Ldwx;->l()Landroid/view/View;

    move-result-object v0

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v2, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto :goto_6

    :cond_a
    return-object p2
.end method

.method public final a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Landroid/view/View;Landroid/content/Context;ILjava/lang/Object;)V
.end method

.method public final a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Ldwx;->c:Lbgo;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Ldve;->c()I

    move-result v0

    iget v1, p0, Ldwx;->e:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Ldwx;->e:I

    div-int/2addr v0, v1

    iget v1, p0, Ldwx;->f:I

    if-lez v1, :cond_0

    iget v1, p0, Ldwx;->f:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract l()Landroid/view/View;
.end method
