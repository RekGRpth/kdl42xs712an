.class public Lfqq;
.super Lfqy;
.source "SourceFile"


# instance fields
.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:I

.field private ak:I

.field private al:I

.field private am:Ljava/util/List;

.field i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfqy;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqq;->ai:Z

    return-void
.end method

.method static synthetic a(Lfqq;)I
    .locals 1

    iget v0, p0, Lfqq;->al:I

    return v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;
    .locals 4

    invoke-static {p0, p1, p8, p9}, Lfqy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "loadSuggested"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "loadGroups"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "loadCircles"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "loadPeople"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "requestCircleVisibility"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "description"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "headerVisible"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "includeSuggestions"

    invoke-virtual {v1, v2, p11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "maxSuggestedImages"

    move/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "maxSuggestedListItems"

    move/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "maxSuggestedDevice"

    move/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p15, :cond_0

    const-string v2, "excludedSuggestions"

    new-instance v3, Ljava/util/ArrayList;

    move-object/from16 v0, p15

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfqq;
    .locals 18

    new-instance v17, Lfqq;

    invoke-direct/range {v17 .. v17}, Lfqq;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    invoke-static/range {v1 .. v16}, Lfqq;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lfqq;->g(Landroid/os/Bundle;)V

    return-object v17
.end method

.method static synthetic b(Lfqq;)I
    .locals 1

    iget v0, p0, Lfqq;->aj:I

    return v0
.end method

.method static synthetic c(Lfqq;)I
    .locals 1

    iget v0, p0, Lfqq;->ak:I

    return v0
.end method

.method static synthetic d(Lfqq;)Z
    .locals 1

    iget-boolean v0, p0, Lfqq;->ag:Z

    return v0
.end method


# virtual methods
.method public E_()V
    .locals 1

    invoke-super {p0}, Lfqy;->E_()V

    invoke-virtual {p0}, Lfqq;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfqe;

    invoke-virtual {v0}, Lfqe;->j()V

    return-void
.end method

.method protected final L()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lfqq;->ac:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lfqq;->aj:I

    if-gtz v0, :cond_0

    iget v0, p0, Lfqq;->ak:I

    if-lez v0, :cond_2

    :cond_0
    iget v0, p0, Lfqq;->al:I

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    const/4 v1, 0x4

    new-instance v2, Lfqt;

    invoke-direct {v2, p0, v4}, Lfqt;-><init>(Lfqq;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    :cond_1
    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Lfqx;

    invoke-direct {v2, p0, v4}, Lfqx;-><init>(Lfqq;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    :cond_2
    iget-boolean v0, p0, Lfqq;->ad:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    new-instance v1, Lfqu;

    invoke-direct {v1, p0, v4}, Lfqu;-><init>(Lfqq;B)V

    invoke-virtual {v0, v4, v1}, Lak;->a(ILal;)Lcb;

    :cond_3
    iget-boolean v0, p0, Lfqq;->ae:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lfqs;

    invoke-direct {v2, p0, v4}, Lfqs;-><init>(Lfqq;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    :cond_4
    iget-boolean v0, p0, Lfqq;->af:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lfqq;->ai:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    new-instance v3, Lfqw;

    invoke-direct {v3, p0, v4}, Lfqw;-><init>(Lfqq;B)V

    invoke-virtual {v0, v1, v2, v3}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    :cond_5
    :goto_0
    return-void

    :cond_6
    invoke-virtual {p0}, Lfqq;->s()Lak;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v2, Lfqv;

    invoke-direct {v2, p0, v4}, Lfqv;-><init>(Lfqq;B)V

    invoke-virtual {v0, v1, v2}, Lak;->a(ILal;)Lcb;

    goto :goto_0
.end method

.method protected synthetic M()Landroid/widget/BaseAdapter;
    .locals 1

    invoke-virtual {p0}, Lfqq;->O()Lfqe;

    move-result-object v0

    return-object v0
.end method

.method protected N()Lfqr;
    .locals 1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Lfqr;

    return-object v0
.end method

.method protected O()Lfqe;
    .locals 9

    new-instance v0, Lfqe;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {p0}, Lfqq;->P()Lfrb;

    move-result-object v2

    iget-object v3, p0, Lfqy;->Z:Ljava/lang/String;

    iget-object v4, p0, Lfqy;->aa:Ljava/lang/String;

    iget-boolean v5, p0, Lfqq;->ah:Z

    iget v6, p0, Lfqq;->aj:I

    iget v7, p0, Lfqq;->ak:I

    iget-object v8, p0, Lfqq;->am:Ljava/util/List;

    invoke-direct/range {v0 .. v8}, Lfqe;-><init>(Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;)V

    iget-boolean v1, p0, Lfqy;->ab:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lfqe;->j()V

    :cond_0
    return-object v0
.end method

.method protected final P()Lfrb;
    .locals 1

    invoke-virtual {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lfqy;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfqr;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement AudienceSelectionFragmentHostActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfqy;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "loadSuggested"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ac:Z

    const-string v1, "loadGroups"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ad:Z

    const-string v1, "loadCircles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ae:Z

    const-string v1, "loadPeople"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->af:Z

    const-string v1, "requestCircleVisibility"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ag:Z

    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfqq;->i:Ljava/lang/String;

    const-string v1, "headerVisible"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ah:Z

    const-string v1, "includeSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lfqq;->ai:Z

    const-string v1, "maxSuggestedImages"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lfqq;->aj:I

    const-string v1, "maxSuggestedListItems"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lfqq;->ak:I

    const-string v1, "maxSuggestedDevice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lfqq;->al:I

    const-string v1, "excludedSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "excludedSuggestions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfqq;->am:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfqy;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfqq;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    return-void
.end method

.method public g_()V
    .locals 1

    invoke-virtual {p0}, Lfqq;->U()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lfqe;

    invoke-virtual {v0}, Lfqe;->k()V

    invoke-super {p0}, Lfqy;->g_()V

    return-void
.end method
