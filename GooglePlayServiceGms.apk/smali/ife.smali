.class public final Life;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static e:Ljava/io/File;


# instance fields
.field private d:Z

.field private final f:Landroid/content/Context;

.field private final g:Liev;

.field private final h:Lids;

.field private final i:Lifg;

.field private final j:Lifg;

.field private final k:Lifg;

.field private final l:Lifg;

.field private final m:Liha;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Life;->a:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Life;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lids;Liha;Liev;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Life;->d:Z

    new-instance v0, Lifg;

    sget-object v1, Lifh;->a:Lifh;

    invoke-direct {v0, p0, v1}, Lifg;-><init>(Life;Lifh;)V

    iput-object v0, p0, Life;->i:Lifg;

    new-instance v0, Lifg;

    sget-object v1, Lifh;->c:Lifh;

    invoke-direct {v0, p0, v1}, Lifg;-><init>(Life;Lifh;)V

    iput-object v0, p0, Life;->j:Lifg;

    new-instance v0, Lifg;

    sget-object v1, Lifh;->b:Lifh;

    invoke-direct {v0, p0, v1}, Lifg;-><init>(Life;Lifh;)V

    iput-object v0, p0, Life;->k:Lifg;

    new-instance v0, Lifg;

    sget-object v1, Lifh;->d:Lifh;

    invoke-direct {v0, p0, v1}, Lifg;-><init>(Life;Lifh;)V

    iput-object v0, p0, Life;->l:Lifg;

    iput-object p1, p0, Life;->f:Landroid/content/Context;

    iput-object p2, p0, Life;->h:Lids;

    iput-object p4, p0, Life;->g:Liev;

    iput-object p3, p0, Life;->m:Liha;

    invoke-static {p1}, Life;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Life;)Lids;
    .locals 1

    iget-object v0, p0, Life;->h:Lids;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Livi;
    .locals 3

    new-instance v0, Livi;

    sget-object v1, Lihj;->y:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    const/4 v1, 0x1

    sget-object v2, Life;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v1, 0x2

    sget-object v2, Life;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Livi;->b(ILjava/lang/String;)Livi;

    if-eqz p0, :cond_0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p0}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_0
    invoke-static {}, Life;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Livi;->b(ILjava/lang/String;)Livi;

    :cond_1
    return-object v0
.end method

.method public static a(Ljava/util/Locale;)Livi;
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Life;->a(Ljava/lang/String;)Livi;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized a()V
    .locals 6

    const-class v1, Life;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Life;->e:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :catch_0
    move-exception v2

    :try_start_2
    sget-boolean v3, Licj;->e:Z

    if-eqz v3, :cond_0

    const-string v3, "GlsClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to delete "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 5

    const-class v2, Life;

    monitor-enter v2

    :try_start_0
    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_1

    sget-object v0, Liae;->e:Liae;

    :goto_0
    invoke-static {v0, p0}, Liad;->a(Liae;Landroid/content/Context;)Liad;

    move-result-object v0

    iget v0, v0, Liad;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Life;->a:Ljava/lang/String;

    invoke-static {p0}, Lbfy;->a(Landroid/content/Context;)V

    invoke-static {p0}, Liun;->a(Landroid/content/Context;)Liun;

    const-string v1, "https://www.google.com/loc/m/api"

    sget-object v0, Lhjv;->f:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v3, " "

    const/4 v4, 0x4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    array-length v3, v0

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    aget-object v3, v0, v3

    const-string v4, "rewrite"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v1, 0x2

    aget-object v0, v0, v1

    sget-boolean v1, Licj;->b:Z

    if-eqz v1, :cond_0

    const-string v1, "GlsClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "using "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    new-instance v1, Liya;

    invoke-direct {v1}, Liya;-><init>()V

    iput-object v0, v1, Liya;->a:Ljava/lang/String;

    const-string v0, "location"

    iput-object v0, v1, Liya;->c:Ljava/lang/String;

    sget-object v0, Life;->a:Ljava/lang/String;

    iput-object v0, v1, Liya;->d:Ljava/lang/String;

    const-string v0, "android"

    iput-object v0, v1, Liya;->e:Ljava/lang/String;

    sget-boolean v0, Licj;->f:Z

    if-eqz v0, :cond_2

    const-string v0, "gms"

    :goto_2
    iput-object v0, v1, Liya;->f:Ljava/lang/String;

    invoke-static {p0, v1}, Lhsy;->a(Landroid/content/Context;Liya;)V

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Life;->b:Ljava/lang/String;

    :goto_3
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    sput-object v0, Life;->e:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-void

    :cond_1
    :try_start_1
    sget-object v0, Liae;->c:Liae;

    goto/16 :goto_0

    :cond_2
    const-string v0, "gmm"

    goto :goto_2

    :cond_3
    const-string v0, "android"

    sput-object v0, Life;->b:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Life;Livi;)V
    .locals 7

    const/4 v1, 0x0

    iget-boolean v0, p0, Life;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Livi;->a(IZ)Livi;

    iput-boolean v1, p0, Life;->d:Z

    :cond_0
    sget-object v0, Lhjv;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_2

    const/4 v0, 0x6

    invoke-virtual {p1, v0, v1}, Livi;->a(IZ)Livi;

    iget-object v0, p0, Life;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v1, v2, v0

    const/4 v4, 0x5

    iget-object v5, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Livi;->a(ILjava/lang/String;)V

    sget-boolean v4, Licj;->b:Z

    if-eqz v4, :cond_1

    const-string v4, "GlsClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "adding account "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lilz;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static a(Livi;Ljava/util/Locale;Ljava/util/List;)V
    .locals 12

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Livi;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v1

    int-to-double v1, v1

    const-wide v3, 0x416312d000000000L    # 1.0E7

    div-double v2, v1, v3

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v0

    int-to-double v0, v0

    const-wide v4, 0x416312d000000000L    # 1.0E7

    div-double/2addr v0, v4

    :cond_0
    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x5

    invoke-virtual {p0, v5}, Livi;->k(I)I

    move-result v5

    if-ge v4, v5, :cond_5

    new-instance v6, Landroid/location/Address;

    invoke-direct {v6, p1}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    const/4 v5, 0x5

    invoke-virtual {p0, v5, v4}, Livi;->c(II)Livi;

    move-result-object v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Livi;->g(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/location/Address;->setFeatureName(Ljava/lang/String;)V

    const/4 v7, 0x5

    invoke-virtual {v5, v7}, Livi;->i(I)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x5

    invoke-virtual {v5, v7}, Livi;->f(I)Livi;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Livi;->c(I)I

    move-result v8

    int-to-double v8, v8

    const-wide v10, 0x416312d000000000L    # 1.0E7

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Landroid/location/Address;->setLatitude(D)V

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Livi;->c(I)I

    move-result v7

    int-to-double v7, v7

    const-wide v9, 0x416312d000000000L    # 1.0E7

    div-double/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Landroid/location/Address;->setLongitude(D)V

    :cond_1
    :goto_1
    const/4 v7, 0x3

    invoke-virtual {v5, v7}, Livi;->f(I)Livi;

    move-result-object v7

    const/4 v5, 0x0

    :goto_2
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Livi;->k(I)I

    move-result v8

    if-ge v5, v8, :cond_3

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v5}, Livi;->d(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v5, v8}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, Livi;->i(I)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v2, v3}, Landroid/location/Address;->setLatitude(D)V

    invoke-virtual {v6, v0, v1}, Landroid/location/Address;->setLongitude(D)V

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :goto_3
    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Livi;->k(I)I

    move-result v8

    if-ge v5, v8, :cond_4

    const/4 v8, 0x2

    invoke-virtual {v7, v8, v5}, Livi;->c(II)Livi;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Livi;->c(I)I

    move-result v9

    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Livi;->g(I)Ljava/lang/String;

    move-result-object v8

    packed-switch v9, :pswitch_data_0

    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :pswitch_0
    invoke-virtual {v6, v8}, Landroid/location/Address;->setAdminArea(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_1
    invoke-virtual {v6, v8}, Landroid/location/Address;->setSubAdminArea(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_2
    invoke-virtual {v6, v8}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_3
    invoke-virtual {v6, v8}, Landroid/location/Address;->setSubLocality(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_4
    invoke-virtual {v6, v8}, Landroid/location/Address;->setThoroughfare(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_5
    invoke-virtual {v6, v8}, Landroid/location/Address;->setSubThoroughfare(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_6
    invoke-virtual {v6, v8}, Landroid/location/Address;->setPremises(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_7
    invoke-virtual {v6, v8}, Landroid/location/Address;->setPostalCode(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_8
    invoke-virtual {v6, v8}, Landroid/location/Address;->setCountryName(Ljava/lang/String;)V

    goto :goto_4

    :pswitch_9
    invoke-virtual {v6, v8}, Landroid/location/Address;->setCountryCode(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_5
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public static a(Ljava/util/Locale;Ljava/lang/String;DDILjava/util/List;)V
    .locals 5

    new-instance v0, Livi;

    sget-object v1, Lihj;->F:Livk;

    invoke-direct {v0, v1}, Livi;-><init>(Livk;)V

    new-instance v1, Livi;

    sget-object v2, Lihj;->i:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    const-wide v3, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v3, p2

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    const/4 v2, 0x2

    const-wide v3, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v3, p4

    double-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Livi;->e(II)Livi;

    new-instance v2, Livi;

    sget-object v3, Lihj;->r:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    const/4 v1, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v1, v3, v4}, Livi;->a(IJ)Livi;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Livi;->b(ILivi;)Livi;

    new-instance v1, Livi;

    sget-object v2, Lihj;->O:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p6}, Livi;->e(II)Livi;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Livi;->b(ILivi;)Livi;

    new-instance v1, Livi;

    sget-object v2, Lihj;->Q:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Livi;->a(ILivi;)V

    invoke-static {p0}, Life;->a(Ljava/util/Locale;)Livi;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;

    if-eqz p1, :cond_0

    new-instance v0, Livi;

    sget-object v2, Lihj;->z:Livk;

    invoke-direct {v0, v2}, Livi;-><init>(Livk;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p1}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Livi;->b(ILivi;)Livi;

    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {v1, v0}, Livi;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Livi;

    sget-object v2, Lihj;->T:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    new-instance v2, Liyw;

    const-string v3, "g:loc/ql"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {v2, v3, v0}, Liyw;-><init>(Ljava/lang/String;[B)V

    new-instance v3, Lifk;

    invoke-direct {v3, v1}, Lifk;-><init>(Livi;)V

    invoke-virtual {v2, v3}, Liyz;->a(Liza;)V

    invoke-static {}, Lhsy;->a()Lhsy;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhsy;->a(Liyz;)V

    :try_start_1
    iget-object v0, v3, Lifk;->a:Lizd;

    invoke-virtual {v0}, Lizd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livi;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_4

    iget-boolean v0, v3, Lifk;->b:Z

    if-eqz v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid response from server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_1

    const-string v1, "GlsClient"

    const-string v2, "reverseGeocode(): unable to write request to payload"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    throw v0

    :catch_1
    move-exception v0

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "GlsClient"

    const-string v1, "reverseGeocode(): response timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "response time-out"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timed out waiting for response from server"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v1

    if-eqz v1, :cond_6

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_5

    const-string v0, "GlsClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reverseGeocode(): RPC failed with status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RPC failed with status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Livi;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {v1}, Life;->b(Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-nez v1, :cond_9

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_8

    const-string v0, "GlsClient"

    const-string v1, "reverseGeocode(): no ReplyElement"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_0
    return-void

    :cond_9
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Livi;->c(I)I

    move-result v1

    if-eqz v1, :cond_a

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_8

    const-string v0, "GlsClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reverseGeocode(): GLS failed with status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_a
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-nez v1, :cond_b

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_8

    const-string v0, "GlsClient"

    const-string v1, "reverseGeocode(): no location in ReplyElement"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_b
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Livi;->f(I)Livi;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Livi;->i(I)Z

    move-result v1

    if-nez v1, :cond_c

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_8

    const-string v0, "GlsClient"

    const-string v1, "reverseGeocode(): no feature in GLocation"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_c
    invoke-static {v0, p0, p7}, Life;->a(Livi;Ljava/util/Locale;Ljava/util/List;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;DDDDILjava/util/List;)V
    .locals 8

    new-instance v1, Livi;

    sget-object v2, Lihj;->F:Livk;

    invoke-direct {v1, v2}, Livi;-><init>(Livk;)V

    new-instance v2, Livi;

    sget-object v3, Lihj;->r:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Livi;->a(IJ)Livi;

    const/16 v3, 0xf

    invoke-virtual {v2, v3, p2}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Livi;->b(ILivi;)Livi;

    new-instance v2, Livi;

    sget-object v3, Lihj;->O:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x1

    move/from16 v0, p11

    invoke-virtual {v2, v3, v0}, Livi;->e(II)Livi;

    const-wide/16 v3, 0x0

    cmpl-double v3, p3, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x0

    cmpl-double v3, p5, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x0

    cmpl-double v3, p7, v3

    if-eqz v3, :cond_0

    const-wide/16 v3, 0x0

    cmpl-double v3, p9, v3

    if-eqz v3, :cond_0

    new-instance v3, Livi;

    sget-object v4, Lihj;->i:Livk;

    invoke-direct {v3, v4}, Livi;-><init>(Livk;)V

    const/4 v4, 0x1

    const-wide v5, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v5, p3

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    const/4 v4, 0x2

    const-wide v5, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v5, p5

    double-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Livi;->e(II)Livi;

    new-instance v4, Livi;

    sget-object v5, Lihj;->i:Livk;

    invoke-direct {v4, v5}, Livi;-><init>(Livk;)V

    const/4 v5, 0x1

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double/2addr v6, p7

    double-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Livi;->e(II)Livi;

    const/4 v5, 0x2

    const-wide v6, 0x416312d000000000L    # 1.0E7

    mul-double v6, v6, p9

    double-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Livi;->e(II)Livi;

    new-instance v5, Livi;

    sget-object v6, Lihj;->l:Livk;

    invoke-direct {v5, v6}, Livi;-><init>(Livk;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v3}, Livi;->b(ILivi;)Livi;

    const/4 v3, 0x2

    invoke-virtual {v5, v3, v4}, Livi;->b(ILivi;)Livi;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, Livi;->b(ILivi;)Livi;

    :cond_0
    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Livi;->b(ILivi;)Livi;

    new-instance v2, Livi;

    sget-object v3, Lihj;->Q:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v1}, Livi;->a(ILivi;)V

    invoke-static {p0}, Life;->a(Ljava/util/Locale;)Livi;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    if-eqz p1, :cond_1

    new-instance v1, Livi;

    sget-object v3, Lihj;->z:Livk;

    invoke-direct {v1, v3}, Livi;-><init>(Livk;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3, p1}, Livi;->b(ILjava/lang/String;)Livi;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1}, Livi;->b(ILivi;)Livi;

    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {v2, v1}, Livi;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v2, Livi;

    sget-object v3, Lihj;->T:Livk;

    invoke-direct {v2, v3}, Livi;-><init>(Livk;)V

    new-instance v3, Liyw;

    const-string v4, "g:loc/ql"

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v3, v4, v1}, Liyw;-><init>(Ljava/lang/String;[B)V

    new-instance v4, Lifk;

    invoke-direct {v4, v2}, Lifk;-><init>(Livi;)V

    invoke-virtual {v3, v4}, Liyz;->a(Liza;)V

    invoke-static {}, Lhsy;->a()Lhsy;

    move-result-object v1

    invoke-virtual {v1, v3}, Lhsy;->a(Liyz;)V

    :try_start_1
    iget-object v1, v4, Lifk;->a:Lizd;

    invoke-virtual {v1}, Lizd;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Livi;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_5

    iget-boolean v1, v4, Lifk;->b:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Invalid response from server"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v1

    sget-boolean v2, Licj;->d:Z

    if-eqz v2, :cond_2

    const-string v2, "GlsClient"

    const-string v3, "forwardGeocode(): unable to write request to payload"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    throw v1

    :catch_1
    move-exception v1

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_3

    const-string v1, "GlsClient"

    const-string v2, "forwardGeocode(): response timeout"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "response time-out"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Timed out waiting for response from server"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v2

    if-eqz v2, :cond_7

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_6

    const-string v1, "GlsClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "forwardGeocode(): RPC failed with status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RPC failed with status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Livi;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v2}, Life;->b(Ljava/lang/String;)V

    :cond_8
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_a

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_9

    const-string v1, "GlsClient"

    const-string v2, "forwardGeocode(): no ReplyElement"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_0
    return-void

    :cond_a
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Livi;->f(I)Livi;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Livi;->c(I)I

    move-result v2

    if-eqz v2, :cond_b

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_9

    const-string v1, "GlsClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "forwardGeocode(): GLS failed with status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_b
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_c

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_9

    const-string v1, "GlsClient"

    const-string v2, "forwardGeocode(): no location in ReplyElement"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_c
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Livi;->f(I)Livi;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Livi;->i(I)Z

    move-result v2

    if-nez v2, :cond_d

    sget-boolean v1, Licj;->d:Z

    if-eqz v1, :cond_9

    const-string v1, "GlsClient"

    const-string v2, "forwardGeocode(): no feature in GLocation"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_d
    move-object/from16 v0, p12

    invoke-static {v1, p0, v0}, Life;->a(Livi;Ljava/util/Locale;Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic b(Life;)Liha;
    .locals 1

    iget-object v0, p0, Life;->m:Liha;

    return-object v0
.end method

.method public static declared-synchronized b()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    const-class v2, Life;

    monitor-enter v2

    :try_start_0
    sget-object v0, Life;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Life;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v2

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/File;

    sget-object v3, Life;->e:Ljava/io/File;

    const-string v4, "nlp_GlsPlatformKey"

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v4, Ljava/io/DataInputStream;

    invoke-direct {v4, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/io/DataInputStream;->close()V

    sput-object v0, Life;->c:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static declared-synchronized b(Ljava/lang/String;)V
    .locals 4

    const-class v1, Life;

    monitor-enter v1

    :try_start_0
    sget-object v0, Life;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Life;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GlsClient"

    const-string v2, "setPlatformKey(): couldn\'t create directory"

    invoke-static {v0, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/File;

    sget-object v2, Life;->e:Ljava/io/File;

    const-string v3, "nlp_GlsPlatformKey"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    sput-object p0, Life;->c:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GlsClient"

    const-string v2, "setPlatformKey(): unable to create platform key file"

    invoke-static {v0, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "GlsClient"

    const-string v2, "setPlatformKey(): unable to write to platform key"

    invoke-static {v0, v2}, Lilz;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method static synthetic c(Life;)Liev;
    .locals 1

    iget-object v0, p0, Life;->g:Liev;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 2

    sget-object v0, Lhjv;->g:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "verbose"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "on"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Livi;)V
    .locals 2

    const/4 v1, 0x3

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Livi;->c(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Livi;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Livi;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Life;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Livi;)V
    .locals 1

    iget-object v0, p0, Life;->i:Lifg;

    invoke-virtual {v0, p1}, Lifg;->a(Livi;)V

    return-void
.end method

.method public final b(Livi;)V
    .locals 1

    iget-object v0, p0, Life;->k:Lifg;

    invoke-virtual {v0, p1}, Lifg;->a(Livi;)V

    return-void
.end method

.method public final c(Livi;)V
    .locals 1

    iget-object v0, p0, Life;->j:Lifg;

    invoke-virtual {v0, p1}, Lifg;->a(Livi;)V

    return-void
.end method

.method public final d(Livi;)V
    .locals 1

    iget-object v0, p0, Life;->l:Lifg;

    invoke-virtual {v0, p1}, Lifg;->a(Livi;)V

    return-void
.end method
