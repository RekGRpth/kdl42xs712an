.class public final Lgpy;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private Y:Landroid/view/View;

.field private Z:I

.field protected a:[Ljava/lang/String;

.field protected b:I

.field private c:Lgpz;

.field private d:Lbby;

.field private e:Landroid/widget/Spinner;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lgpy;->Z:I

    return-void
.end method

.method private a()V
    .locals 13

    const/4 v8, 0x0

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->j()Lgpx;

    move-result-object v9

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, v9, Lgpx;->f:Ljava/lang/String;

    invoke-static {v0, v3}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v10

    iget v0, p0, Lgpy;->Z:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, v9, Lgpx;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, v9, Lgpx;->e:Ljava/lang/String;

    :goto_1
    const v3, 0x7f020218    # com.google.android.gms.R.drawable.plus_iconic_ic_send_darkgrey_32

    move v4, v3

    move v5, v1

    move-object v7, v0

    move v3, v1

    move v0, v1

    :goto_2
    iget-object v11, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v11, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v5, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lbpr;->a()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v5, v1, v1, v4, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lgpy;->h:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lgpy;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    if-eqz v3, :cond_6

    if-eqz v10, :cond_6

    const v0, 0x7f0b03d5    # com.google.android.gms.R.string.plus_sharebox_audience_selection_activity_title

    invoke-virtual {p0, v0}, Lgpy;->b(I)Ljava/lang/String;

    move-result-object v0

    move v3, v1

    :goto_5
    iget-object v5, p0, Lgpy;->e:Landroid/widget/Spinner;

    if-eqz v3, :cond_a

    move v4, v1

    :goto_6
    invoke-virtual {v5, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v4, p0, Lgpy;->g:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lgpy;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpy;->a:[Ljava/lang/String;

    array-length v0, v0

    if-eq v0, v2, :cond_0

    if-nez v3, :cond_b

    :cond_0
    iget-object v0, p0, Lgpy;->Y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lgpy;->Y:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_7
    return-void

    :pswitch_1
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v6

    move-object v7, v8

    goto :goto_2

    :pswitch_2
    const v0, 0x7f0b03c4    # com.google.android.gms.R.string.plus_sharebox_next

    invoke-virtual {p0, v0}, Lgpy;->a(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {}, Lbpr;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f020215    # com.google.android.gms.R.drawable.plus_iconic_ic_previous_grey_32

    :goto_8
    move v4, v0

    move v5, v1

    move-object v7, v3

    move v3, v2

    move v0, v1

    goto :goto_2

    :cond_1
    const v0, 0x7f020213    # com.google.android.gms.R.drawable.plus_iconic_ic_next_grey_32

    goto :goto_8

    :pswitch_3
    move v0, v2

    move v3, v2

    move v4, v1

    move v5, v6

    move-object v7, v8

    goto :goto_2

    :cond_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    const v0, 0x7f0b03c3    # com.google.android.gms.R.string.plus_sharebox_share

    invoke-virtual {p0, v0}, Lgpy;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    iget-object v5, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v5, v4, v1, v1, v1}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lgpy;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lgpy;->i:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_6
    invoke-virtual {v9}, Lgpx;->c()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_9
    iget-object v3, v9, Lgpx;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    move v3, v2

    :goto_a
    if-eqz v3, :cond_9

    iget-object v3, v9, Lgpx;->c:Ljava/lang/String;

    move-object v12, v3

    move v3, v0

    move-object v0, v12

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_9

    :cond_8
    move v3, v1

    goto :goto_a

    :cond_9
    const v3, 0x7f0b0336    # com.google.android.gms.R.string.plus_app_label

    invoke-virtual {p0, v3}, Lgpy;->b(I)Ljava/lang/String;

    move-result-object v3

    move-object v12, v3

    move v3, v0

    move-object v0, v12

    goto/16 :goto_5

    :cond_a
    const/16 v4, 0x8

    goto/16 :goto_6

    :cond_b
    iget-object v0, p0, Lgpy;->Y:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lgpy;->Y:Landroid/view/View;

    const v1, 0x7f0200a2    # com.google.android.gms.R.drawable.common_settings_spinner_bg

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final E_()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lgpy;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v0, p0, Lgpy;->b:I

    if-ltz v0, :cond_1

    iget-object v0, p0, Lgpy;->a:[Ljava/lang/String;

    iget v1, p0, Lgpy;->b:I

    invoke-virtual {p0, v0, v1}, Lgpy;->a([Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->K()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgpy;->c:Lgpz;

    invoke-interface {v1}, Lgpz;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->L()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lgpy;->a([Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    const v0, 0x7f040110    # com.google.android.gms.R.layout.plus_sharebox_title_fragment

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a00c7    # com.google.android.gms.R.id.account_spinner

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lgpy;->e:Landroid/widget/Spinner;

    const v0, 0x7f0a01c4    # com.google.android.gms.R.id.title_text

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgpy;->g:Landroid/widget/TextView;

    const v0, 0x7f0a02c2    # com.google.android.gms.R.id.title_container

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpy;->Y:Landroid/view/View;

    const v0, 0x7f0a00d5    # com.google.android.gms.R.id.up_button

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpy;->h:Landroid/view/View;

    const v0, 0x7f0a00d6    # com.google.android.gms.R.id.up_button_icon

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpy;->i:Landroid/view/View;

    const v0, 0x7f0a02d6    # com.google.android.gms.R.id.share_button

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lgpy;->f:Landroid/widget/Button;

    iget-object v0, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lgpy;->f:Landroid/widget/Button;

    if-eqz p3, :cond_2

    const-string v0, "button_enabled"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->j()Lgpx;

    move-result-object v0

    iget v0, v0, Lgpx;->d:I

    if-lez v0, :cond_3

    :goto_1
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lgpy;->c:Lgpz;

    invoke-interface {v1}, Lgpz;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lgpy;->c:Lgpz;

    invoke-interface {v1}, Lgpz;->j()Lgpx;

    move-result-object v1

    iget v1, v1, Lgpx;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v0, 0x7f0a02d4    # com.google.android.gms.R.id.titlebar_icon_layout

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_2
    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->j()Lgpx;

    move-result-object v0

    iget-object v1, v0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a02d5    # com.google.android.gms.R.id.plus_page_name

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lgpy;->a()V

    return-object v3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "ShareBox"

    const-string v4, "Unable to override the app icon."

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "ShareBox"

    const-string v4, "Unable to override the app icon."

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpz;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpz;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lgpz;

    iput-object p1, p0, Lgpy;->c:Lgpz;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v1, p0, Lgpy;->f:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 3

    iput-object p1, p0, Lgpy;->a:[Ljava/lang/String;

    iput p2, p0, Lgpy;->b:I

    new-instance v0, Lbby;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Lgpy;->a:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lbby;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v0, p0, Lgpy;->d:Lbby;

    iget-object v0, p0, Lgpy;->e:Landroid/widget/Spinner;

    iget-object v1, p0, Lgpy;->d:Lbby;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpy;->e:Landroid/widget/Spinner;

    iget v1, p0, Lgpy;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lgpy;->e:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_0
    invoke-direct {p0}, Lgpy;->a()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "account_names"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpy;->a:[Ljava/lang/String;

    const-string v0, "selected_position"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgpy;->b:I

    const-string v0, "button_action"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lgpy;->Z:I

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    iput p1, p0, Lgpy;->Z:I

    invoke-direct {p0}, Lgpy;->a()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "account_names"

    iget-object v1, p0, Lgpy;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "selected_position"

    iget v1, p0, Lgpy;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "button_action"

    iget v1, p0, Lgpy;->Z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "button_enabled"

    iget-object v1, p0, Lgpy;->f:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a02d6    # com.google.android.gms.R.id.share_button

    if-ne v0, v1, :cond_2

    iget v0, p0, Lgpy;->Z:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgpy;->a(Z)V

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->q()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a02c2    # com.google.android.gms.R.id.title_container

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lgpy;->e:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->performClick()Z

    :cond_1
    return-void

    :pswitch_0
    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->r()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->s()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a00d5    # com.google.android.gms.R.id.up_button

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgpy;->c:Lgpz;

    invoke-interface {v0}, Lgpz;->s()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget v0, p0, Lgpy;->b:I

    if-eq v0, p3, :cond_0

    iput p3, p0, Lgpy;->b:I

    iget-object v1, p0, Lgpy;->c:Lgpz;

    iget-object v0, p0, Lgpy;->d:Lbby;

    iget v2, p0, Lgpy;->b:I

    invoke-virtual {v0, v2}, Lbby;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Lgpz;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
