.class public final Lang;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/Lock;


# instance fields
.field private final b:Laoc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lang;->a:Ljava/util/concurrent/locks/Lock;

    return-void
.end method

.method public constructor <init>(Laoc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoc;

    iput-object v0, p0, Lang;->b:Laoc;

    return-void
.end method

.method private static a()J
    .locals 6

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    const-wide/32 v2, 0x3dcc500

    const-wide v4, 0x4184997000000000L    # 4.32E7

    mul-double/2addr v0, v4

    double-to-long v0, v0

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(Landroid/content/Context;J)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long v1, v0, p1

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.auth.authzen.CHECK_REGISTRATION"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    sget-object v0, Lang;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "authzen received event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "P"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const-string v0, "P"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "P"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "AuthZen"

    const-string v1, "Received an empty gcm message"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-static {}, Lang;->a()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lang;->a(Landroid/content/Context;J)V

    sget-object v0, Lang;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_0
    :try_start_1
    invoke-static {v0}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    if-nez v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid base64 encoding"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lizj; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lanp; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "AuthZen"

    const-string v2, "Couldn\'t parse proto"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lang;->a()J

    move-result-wide v1

    invoke-static {p1, v1, v2}, Lang;->a(Landroid/content/Context;J)V

    sget-object v1, Lang;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    if-nez v0, :cond_3

    :try_start_3
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_3
    .catch Lizj; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Lanp; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "AuthZen"

    const-string v2, "Crypto key problem"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_3
    :try_start_5
    invoke-static {v0}, Ljde;->a([B)Ljde;

    move-result-object v1

    iget-object v1, v1, Ljde;->a:Lizf;

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    invoke-static {v1}, Ljdd;->a([B)Ljdd;

    move-result-object v1

    iget-object v1, v1, Ljdd;->a:Ljdc;

    iget-object v1, v1, Ljdc;->d:Lizf;

    invoke-virtual {v1}, Lizf;->b()[B

    move-result-object v1

    if-eqz v1, :cond_4

    array-length v2, v1

    if-nez v2, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing key handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lizj; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Lanp; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v0

    :try_start_6
    const-string v1, "AuthZen"

    const-string v2, "Crypto signature problem"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :cond_5
    :try_start_7
    new-instance v2, Lano;

    invoke-direct {v2, p1}, Lano;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Lano;->a([B)Lanj;

    move-result-object v3

    if-nez v3, :cond_6

    const-string v0, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No encryption key found for given handle: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Lizj; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Lanp; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_8
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_6
    :try_start_9
    iget-object v1, v3, Lanj;->a:Lank;

    iget-object v1, v1, Lank;->a:Ljava/lang/String;

    invoke-static {p1, v1}, Lang;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account does not exist: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v3, Lanj;->a:Lank;

    iget-object v2, v2, Lank;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Lizj; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Lanp; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_4
    move-exception v0

    :try_start_a
    const-string v1, "AuthZen"

    const-string v2, "Error during decoding"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    :cond_7
    :try_start_b
    iget-object v1, v3, Lanj;->a:Lank;

    iget-object v1, v1, Lank;->b:Ljavax/crypto/SecretKey;

    invoke-static {v0, v1}, Ljcm;->a([BLjavax/crypto/SecretKey;)Ljcn;

    move-result-object v0

    const-string v1, "device_key"

    invoke-virtual {v2, v1}, Lano;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v1

    sget-object v2, Ljco;->c:Ljco;

    iget-object v4, v0, Ljcn;->a:Ljco;

    if-ne v2, v4, :cond_19

    iget-object v2, p0, Lang;->b:Laoc;

    iget-object v2, v3, Lanj;->a:Lank;

    iget-object v2, v2, Lank;->a:Ljava/lang/String;
    :try_end_b
    .catch Lizj; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catch Lanp; {:try_start_b .. :try_end_b} :catch_8
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :try_start_c
    iget-object v0, v0, Ljcn;->b:[B

    invoke-static {v0}, Ljby;->a([B)Ljby;
    :try_end_c
    .catch Lizj; {:try_start_c .. :try_end_c} :catch_7
    .catch Ljava/security/InvalidKeyException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6
    .catch Lanp; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v4

    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v0, v4, Ljby;->c:Ljbx;

    iget-wide v7, v0, Ljbx;->a:J

    cmp-long v0, v5, v7

    if-lez v0, :cond_8

    const-string v0, "AuthZen"

    const-string v9, "Authzen prompt was supposed to be expired based on device clock"

    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "AuthZen"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "expiry="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", now="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Lizj; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catch Lanp; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_8
    :try_start_e
    iget-object v0, v4, Ljby;->c:Ljbx;

    iget v0, v0, Ljbx;->b:I

    const/4 v5, 0x2

    if-ne v0, v5, :cond_a

    invoke-static {v4}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Ljby;)Z

    move-result v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for ACCOUNT_RECOVERY was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_e
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_e .. :try_end_e} :catch_5
    .catch Lizj; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6
    .catch Lanp; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_5
    move-exception v0

    :try_start_f
    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v5, "AuthZen"

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Laob;

    iget-object v3, v3, Lanj;->a:Lank;

    iget-object v3, v3, Lank;->b:Ljavax/crypto/SecretKey;

    invoke-direct {v0, p1, v2, v1, v3}, Laob;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)V

    new-instance v1, Ljbz;

    invoke-direct {v1}, Ljbz;-><init>()V

    const/16 v2, 0x63

    invoke-virtual {v1, v2}, Ljbz;->a(I)Ljbz;

    move-result-object v1

    new-instance v2, Ljbw;

    invoke-direct {v2}, Ljbw;-><init>()V

    invoke-virtual {v2, v4}, Ljbw;->a(Ljby;)Ljbw;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljbw;->a(Ljbz;)Ljbw;

    move-result-object v1

    new-instance v2, Ljcn;

    sget-object v3, Ljco;->d:Ljco;

    invoke-virtual {v1}, Ljbw;->d()[B

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljcn;-><init>(Ljco;[B)V

    invoke-virtual {v0, v2}, Laob;->a(Ljcn;)V
    :try_end_f
    .catch Lizj; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_f .. :try_end_f} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_4
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6
    .catch Lanp; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_0

    :catch_6
    move-exception v0

    :try_start_10
    const-string v1, "AuthZen"

    const-string v2, "Error while fetching key."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    :catch_7
    move-exception v0

    :try_start_11
    const-string v0, "AuthZen"

    const-string v1, "Received a malformed TxRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catch Lizj; {:try_start_11 .. :try_end_11} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_11 .. :try_end_11} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_11} :catch_4
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Lanp; {:try_start_11 .. :try_end_11} :catch_8
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_0

    :catch_8
    move-exception v0

    :try_start_12
    const-string v1, "AuthZen"

    const-string v2, "Error while creating key."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto/16 :goto_0

    :cond_9
    :try_start_13
    invoke-virtual {v3}, Lanj;->a()[B

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const/4 v5, 0x1

    if-ne v0, v5, :cond_18

    iget-object v0, v4, Ljby;->c:Ljbx;

    iget-boolean v0, v0, Ljbx;->c:Z

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for GENERIC_APPROVAL was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, v4, Ljby;->c:Ljbx;

    iget-object v0, v0, Ljbx;->d:Ljbn;

    iget v5, v0, Ljbn;->a:I

    iget-object v0, v4, Ljby;->c:Ljbx;

    iget-object v0, v0, Ljbx;->d:Ljbn;

    iget v6, v0, Ljbn;->c:I

    const/4 v0, 0x2

    if-ne v6, v0, :cond_c

    sget-object v0, Laqc;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    if-nez v0, :cond_10

    const-string v0, "AuthZen"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "The usecase specified by the incoming request has been disabled by gservices flags: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x3

    if-ne v6, v0, :cond_d

    sget-object v0, Laqc;->i:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_d
    const/4 v0, 0x4

    if-ne v6, v0, :cond_e

    sget-object v0, Laqc;->j:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_e
    const/4 v0, 0x5

    if-ne v6, v0, :cond_f

    sget-object v0, Laqc;->k:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_f
    const/4 v0, 0x0

    goto :goto_1

    :cond_10
    const/4 v0, 0x1

    if-ne v5, v0, :cond_12

    invoke-static {v4}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->a(Ljby;)Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for BASIC_CONFIRMATION was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    invoke-virtual {v3}, Lanj;->a()[B

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/BasicConfirmationWorkflow;->b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x2

    if-ne v5, v0, :cond_14

    invoke-static {v4}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->a(Ljby;)Z

    move-result v0

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for DOUBLE_CONFIRMATION was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    invoke-virtual {v3}, Lanj;->a()[B

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/DoubleConfirmationWorkflow;->b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_14
    const/4 v0, 0x3

    if-ne v5, v0, :cond_16

    invoke-static {v4}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Ljby;)Z

    move-result v0

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for PIN_CONFIRMATION was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    invoke-virtual {v3}, Lanj;->a()[B

    move-result-object v0

    invoke-static {v4, v2, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_16
    if-nez v5, :cond_17

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v5, "The request proto for NO_WORKFLOW_HINT was malformed"

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown workflow. Transaction not handled for workflow: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_18
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unknown workflow. Transaction not handled for prompt type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_13
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_13 .. :try_end_13} :catch_5
    .catch Lizj; {:try_start_13 .. :try_end_13} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_13 .. :try_end_13} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13 .. :try_end_13} :catch_4
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_6
    .catch Lanp; {:try_start_13 .. :try_end_13} :catch_8
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_19
    :try_start_14
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected payload type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Ljcn;->a:Ljco;

    invoke-virtual {v0}, Ljco;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Lizj; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_14 .. :try_end_14} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_14 .. :try_end_14} :catch_4
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_6
    .catch Lanp; {:try_start_14 .. :try_end_14} :catch_8
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_0

    :cond_1a
    :try_start_15
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, "com.google.android.gms.auth.authzen.CHECK_REGISTRATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    :cond_1b
    invoke-static {}, Lanx;->a()Lanx;

    move-result-object v0

    sget-object v1, Lany;->a:Lany;

    invoke-virtual {v0, v1}, Lanx;->a(Lany;)V

    goto/16 :goto_0

    :cond_1c
    const-string v1, "com.google.android.gms.auth.authzen.REGISTER_NOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-static {}, Lanx;->a()Lanx;

    move-result-object v0

    sget-object v1, Lany;->b:Lany;

    invoke-virtual {v0, v1}, Lanx;->a(Lany;)V

    goto/16 :goto_0

    :cond_1d
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_0
.end method
