.class public abstract Lfyv;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lfyf;


# instance fields
.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/String;

.field public p:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    new-instance v0, Lfzb;

    invoke-direct {v0, p0, p1, p2, p3}, Lfzb;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;
    .locals 7

    iget v0, p0, Lfyv;->p:I

    invoke-static {p0, p3, p2, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    iget-object v2, p0, Lfyv;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    iget-object v2, p0, Lfyv;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v6, Lfyc;

    invoke-direct {v6, v0, p1}, Lfyc;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    new-instance v0, Lfyd;

    iget-object v2, p0, Lfyv;->n:Ljava/lang/String;

    invoke-virtual {p0}, Lfyv;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    iget-object v5, p0, Lfyv;->o:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lfyd;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    new-instance v1, Lfyb;

    invoke-direct {v1, p0, v6, v0}, Lfyb;-><init>(Landroid/app/Activity;Lfyc;Lfyd;)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lgfp;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lgfp;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lgfp;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Lgfp;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v2, Lbcr;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v1, v0, v2}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lgfp;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lgfp;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lgfr;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lgfr;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lgfr;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Lgfr;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v1, "picasa"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lbcr;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-direct {p0, p1, v2, v0, v1}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v2, v0

    goto :goto_0

    :cond_2
    sget-object v1, Lbcr;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lgfr;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lgfr;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;
    .locals 3

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-interface {p2}, Lglo;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lglo;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lglo;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Lglo;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v2, Lbcr;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v1, v0, v2}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lglo;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lglo;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lfyv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    invoke-virtual {p0}, Lfyv;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    iget-object v0, p0, Lfyv;->n:Ljava/lang/String;

    iget-object v1, p0, Lfyv;->o:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2, v1}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method public b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    invoke-virtual {p0}, Lfyv;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbcr;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbcr;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    sget-object v0, Lbcr;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, p1}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    iget-object v0, p0, Lfyv;->n:Ljava/lang/String;

    iget-object v1, p0, Lfyv;->o:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2, v1}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method protected final g()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    iget v0, p0, Lfyv;->p:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final h()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lfyv;->setResult(I)V

    invoke-virtual {p0}, Lfyv;->finish()V

    return-void
.end method

.method protected final i()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfyv;->setResult(I)V

    invoke-virtual {p0}, Lfyv;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Lfyv;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfyv;->p:I

    invoke-virtual {p0}, Lfyv;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x103006b    # android.R.style.Theme_Holo

    invoke-virtual {p0, v0}, Lfyv;->setTheme(I)V

    :goto_0
    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfyv;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lfyv;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfyv;->o:Ljava/lang/String;

    iget-object v1, p0, Lfyv;->o:Ljava/lang/String;

    if-nez v1, :cond_2

    const-string v0, "UpgradeAccount"

    const-string v1, "Required client calling package extra is unspecified"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lfyv;->i()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lfyv;->p:I

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f100188    # com.google.android.gms.R.style.common_Activity_Light_Dialog

    invoke-virtual {p0, v0}, Lfyv;->setTheme(I)V

    goto :goto_0

    :pswitch_0
    const v0, 0x7f100184    # com.google.android.gms.R.style.common_Activity_Light

    invoke-virtual {p0, v0}, Lfyv;->setTheme(I)V

    goto :goto_0

    :cond_2
    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyv;->n:Ljava/lang/String;

    iget-object v0, p0, Lfyv;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "Required account name extra is unspecified"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lfyv;->i()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
