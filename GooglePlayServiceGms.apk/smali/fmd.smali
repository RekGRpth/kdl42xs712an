.class public Lfmd;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static final b:Z

.field private static c:Lfmd;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lfme;

.field private final f:Lfmh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lfkv;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lfmd;->a:Z

    sget-object v0, Lfkv;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lfmd;->b:Z

    const/4 v0, 0x0

    sput-object v0, Lfmd;->c:Lfmd;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lfme;Lfmh;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lfmd;->d:Landroid/content/Context;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfme;

    iput-object v0, p0, Lfmd;->e:Lfme;

    iput-object p3, p0, Lfmd;->f:Lfmh;

    return-void
.end method

.method public static a()Lfmd;
    .locals 7

    const-class v2, Lfmd;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lfmd;->c:Lfmd;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    new-instance v3, Lfmd;

    new-instance v4, Lfme;

    invoke-direct {v4}, Lfme;-><init>()V

    sget-object v1, Lfkv;->e:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v4, Lfme;->a:Z

    sget-object v1, Lfkv;->f:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, v4, Lfme;->b:J

    sget-object v1, Lfkv;->g:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, v4, Lfme;->c:J

    sget-object v1, Lfkv;->h:Lbfy;

    invoke-virtual {v1}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iput-wide v5, v4, Lfme;->d:J

    new-instance v1, Lfmh;

    invoke-direct {v1, v0}, Lfmh;-><init>(Landroid/content/Context;)V

    invoke-direct {v3, v0, v4, v1}, Lfmd;-><init>(Landroid/content/Context;Lfme;Lfmh;)V

    sput-object v3, Lfmd;->c:Lfmd;

    :cond_0
    sget-object v0, Lfmd;->c:Lfmd;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private b(J)J
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lfmd;->f:Lfmh;

    invoke-virtual {v0}, Lfmh;->a()J

    move-result-wide v0

    iget-object v2, p0, Lfmd;->f:Lfmh;

    invoke-virtual {v2}, Lfmh;->b()J

    move-result-wide v2

    cmp-long v4, p1, v0

    if-gez v4, :cond_0

    move-wide p1, v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    cmp-long v0, p1, v2

    if-gez v0, :cond_3

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfmd;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/playlog/uploader/UploaderAlarmReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lfmd;->d:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v5, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v0, p0, Lfmd;->d:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v5, p1, p2, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    iget-object v0, p0, Lfmd;->f:Lfmh;

    iget-object v1, v0, Lfmh;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-eqz v1, :cond_2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lfmh;->c:Ljava/lang/Long;

    iget-object v0, v0, Lfmh;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "b"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_2
    :goto_0
    return-wide p1

    :cond_3
    move-wide p1, v2

    goto :goto_0
.end method

.method private g()V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {p0}, Lfmd;->h()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lfmd;->f:Lfmh;

    invoke-virtual {v2}, Lfmh;->a()J

    move-result-wide v2

    sub-long v4, v2, v0

    const-wide/32 v6, 0x19bfcc00

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    :goto_0
    iget-object v2, p0, Lfmd;->f:Lfmh;

    iget-object v3, v2, Lfmh;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v0

    if-eqz v3, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lfmh;->b:Ljava/lang/Long;

    iget-object v2, v2, Lfmh;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "a"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private h()J
    .locals 2

    iget-object v0, p0, Lfmd;->e:Lfme;

    iget-boolean v0, v0, Lfme;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmd;->e:Lfme;

    iget-wide v0, v0, Lfme;->c:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lfmd;->d:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x1

    if-eq v1, v0, :cond_2

    const/16 v1, 0x9

    if-ne v1, v0, :cond_4

    :cond_2
    iget-object v0, p0, Lfmd;->e:Lfme;

    iget-wide v0, v0, Lfme;->c:J

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lfmd;->e:Lfme;

    iget-wide v0, v0, Lfme;->d:J

    goto :goto_0
.end method


# virtual methods
.method final a(J)V
    .locals 2

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lfmd;->e:Lfme;

    iput-wide p1, v0, Lfme;->b:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 6

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lfmd;->f:Lfmh;

    invoke-virtual {v0}, Lfmh;->b()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfmd;->h()J

    invoke-direct {p0}, Lfmd;->g()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final d()V
    .locals 2

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0}, Lfmd;->g()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 8

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lfmd;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Scheduler"

    const-string v2, "requesting immediate upload"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lfmd;->b(J)J

    move-result-wide v4

    sget-boolean v0, Lfmd;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "Scheduler"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "--> actual with delay: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v2, v4, v2

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 8

    const-class v1, Lfmd;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lfmd;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "Scheduler"

    const-string v2, "requesting upload"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lfmd;->h()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lfmd;->e:Lfme;

    iget-wide v6, v0, Lfme;->b:J

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lfmd;->b(J)J

    move-result-wide v2

    sget-boolean v0, Lfmd;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "Scheduler"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "--> actual with delay: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v2, v5

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
