.class public Ldv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ldy;

.field private static final c:Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Ldz;

    invoke-direct {v0}, Ldz;-><init>()V

    sput-object v0, Ldv;->b:Ldy;

    :goto_0
    sget-object v0, Ldv;->b:Ldy;

    invoke-interface {v0}, Ldy;->a()Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Ldv;->c:Ljava/lang/Object;

    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    new-instance v0, Ldw;

    invoke-direct {v0}, Ldw;-><init>()V

    sput-object v0, Ldv;->b:Ldy;

    goto :goto_0

    :cond_1
    new-instance v0, Leb;

    invoke-direct {v0}, Leb;-><init>()V

    sput-object v0, Ldv;->b:Ldy;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ldv;->b:Ldy;

    invoke-interface {v0, p0}, Ldy;->a(Ldv;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldv;->a:Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/view/View;)Lgx;
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p0}, Ldy;->a(Ljava/lang/Object;Landroid/view/View;)Lgx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/view/View;I)V
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Ldy;->a(Ljava/lang/Object;Landroid/view/View;I)V

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Ldy;->d(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method

.method public static c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p0, p1}, Ldy;->c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Ldv;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Landroid/view/View;Lgq;)V
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Ldy;->a(Ljava/lang/Object;Landroid/view/View;Lgq;)V

    return-void
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Ldy;->a(Ljava/lang/Object;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Ldy;->a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Ldy;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    sget-object v0, Ldv;->b:Ldy;

    sget-object v1, Ldv;->c:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Ldy;->b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    return-void
.end method
