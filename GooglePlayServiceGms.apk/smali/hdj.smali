.class public final Lhdj;
.super Lhct;
.source "SourceFile"


# instance fields
.field private final a:Lhcv;

.field private final b:Lhce;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhcv;)V
    .locals 2

    invoke-direct {p0}, Lhct;-><init>()V

    iput-object p2, p0, Lhdj;->a:Lhcv;

    new-instance v0, Lhce;

    const-string v1, "NetworkIaService"

    invoke-direct {v0, p1, v1}, Lhce;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lhdj;->b:Lhce;

    return-void
.end method

.method static synthetic a(Lhdj;)Lhcv;
    .locals 1

    iget-object v0, p0, Lhdj;->a:Lhcv;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lhgi;->b(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdl;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdl;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdn;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdn;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdm;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdm;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdp;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdp;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdk;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhdk;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdt;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhdt;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdv;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdv;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdu;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdu;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdr;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdr;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhds;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhds;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdq;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhdq;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    iget-object v6, p0, Lhdj;->b:Lhce;

    new-instance v0, Lhdo;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lhdo;-><init>(Lhdj;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lhce;->a(Lhcc;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method
