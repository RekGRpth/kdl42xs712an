.class final Lcym;
.super Lcwk;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcwm;

.field private final b:Lbds;


# direct methods
.method public constructor <init>(Lcwm;Lbds;)V
    .locals 1

    iput-object p1, p0, Lcym;->a:Lcwm;

    invoke-direct {p0}, Lcwk;-><init>()V

    const-string v0, "Holder must not be null"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbds;

    iput-object v0, p0, Lcym;->b:Lbds;

    return-void
.end method


# virtual methods
.method public final b(ILandroid/os/Bundle;)V
    .locals 5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iget-object v1, p0, Lcym;->a:Lcwm;

    new-instance v2, Lcyn;

    iget-object v3, p0, Lcym;->a:Lcwm;

    iget-object v4, p0, Lcym;->b:Lbds;

    invoke-direct {v2, v3, v4, v0, p2}, Lcyn;-><init>(Lcwm;Lbds;Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Lcwm;->a(Lbjg;)V

    return-void
.end method
