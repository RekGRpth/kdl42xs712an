.class public final Ldrl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldri;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ldaj;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldrl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iput-object p2, p0, Ldrl;->b:Ldaj;

    iput-object p3, p0, Ldrl;->c:Ljava/lang/String;

    iput-object p4, p0, Ldrl;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 4

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Ldrl;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Ldrl;->d:Ljava/lang/String;

    iget-object v3, p0, Ldrl;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v1, v2, v3}, Lcun;->f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    :try_start_1
    iget-object v1, p0, Ldrl;->b:Ldaj;

    invoke-interface {v1, v0}, Ldaj;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SignInIntentService"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v1

    invoke-virtual {v0}, Ldqq;->a()I

    move-result v0

    const/16 v2, 0x3eb

    if-ne v0, v2, :cond_0

    invoke-virtual {p2, p1}, Lcun;->c(Landroid/content/Context;)V

    :cond_0
    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method
