.class public final Lgrc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/FullWallet;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/FullWallet;)V
    .locals 0

    iput-object p1, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/FullWallet;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgrc;-><init>(Lcom/google/android/gms/wallet/FullWallet;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/Address;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->e:Lcom/google/android/gms/wallet/Address;

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/wallet/ProxyCard;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->c:Lcom/google/android/gms/wallet/ProxyCard;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->g:[Ljava/lang/String;

    return-object p0
.end method

.method public final b(Lcom/google/android/gms/wallet/Address;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->f:Lcom/google/android/gms/wallet/Address;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lgrc;
    .locals 1

    iget-object v0, p0, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->d:Ljava/lang/String;

    return-object p0
.end method
