.class public final Leu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ley;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    new-instance v0, Lex;

    invoke-direct {v0}, Lex;-><init>()V

    sput-object v0, Leu;->a:Ley;

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Lew;

    invoke-direct {v0}, Lew;-><init>()V

    sput-object v0, Leu;->a:Ley;

    goto :goto_0

    :cond_1
    new-instance v0, Lev;

    invoke-direct {v0}, Lev;-><init>()V

    sput-object v0, Leu;->a:Ley;

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0, p1}, Lct;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0, p1}, Ley;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;Lei;)Landroid/view/MenuItem;
    .locals 2

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0, p1}, Lct;->a(Lei;)Lct;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    const-string v0, "MenuItemCompat"

    const-string v1, "setActionProvider: item does not implement SupportMenuItem; ignoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0}, Lct;->getActionView()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0}, Ley;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;I)V
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0, p1}, Lct;->setShowAsAction(I)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0, p1}, Ley;->a(Landroid/view/MenuItem;I)V

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0, p1}, Lct;->setActionView(I)Landroid/view/MenuItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0, p1}, Ley;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;)Z
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0}, Lct;->expandActionView()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0}, Ley;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Landroid/view/MenuItem;)Z
    .locals 1

    instance-of v0, p0, Lct;

    if-eqz v0, :cond_0

    check-cast p0, Lct;

    invoke-interface {p0}, Lct;->isActionViewExpanded()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Leu;->a:Ley;

    invoke-interface {v0, p0}, Ley;->c(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
