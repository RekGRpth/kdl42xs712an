.class public final Ldeb;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 3

    packed-switch p0, :pswitch_data_0

    const-string v0, "MatchParticipantStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown participant status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNKNOWN_STATUS"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "PARTICIPANT_NOT_INVITED_YET"

    goto :goto_0

    :pswitch_1
    const-string v0, "PARTICIPANT_INVITED"

    goto :goto_0

    :pswitch_2
    const-string v0, "PARTICIPANT_JOINED"

    goto :goto_0

    :pswitch_3
    const-string v0, "PARTICIPANT_DECLINED"

    goto :goto_0

    :pswitch_4
    const-string v0, "PARTICIPANT_LEFT"

    goto :goto_0

    :pswitch_5
    const-string v0, "PARTICIPANT_FINISHED"

    goto :goto_0

    :pswitch_6
    const-string v0, "PARTICIPANT_UNRESPONSIVE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
