.class public Lapp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;


# instance fields
.field private final c:Laoy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lapp;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lapp;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Laso;->b:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->l:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->p:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->r:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->c:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->F:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Laso;->n:Laso;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lapp;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    new-instance v1, Laoy;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Laoy;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v1}, Lapp;-><init>(Laoy;)V

    return-void
.end method

.method public constructor <init>(Laoy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoy;

    iput-object v0, p0, Lapp;->c:Laoy;

    return-void
.end method

.method public static a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 1

    new-instance v0, Lapv;

    invoke-direct {v0}, Lapv;-><init>()V

    invoke-static {}, Lapv;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 3

    new-instance v0, Lapv;

    invoke-direct {v0}, Lapv;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;->b:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;->c:Z

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;->d:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lapv;->a(Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;
    .locals 5

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lapz;->a(Ljava/lang/String;)Laqa;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;->b:Ljava/lang/String;

    iget-boolean v3, v0, Laqa;->b:Z

    iget-boolean v4, v0, Laqa;->c:Z

    iget-boolean v0, v0, Laqa;->d:Z

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;-><init>(Ljava/lang/String;ZZZ)V

    return-object v1
.end method

.method public static a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;
    .locals 7

    new-instance v0, Lapv;

    invoke-direct {v0}, Lapv;-><init>()V

    invoke-static {}, Lapr;->a()Lapr;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->f:Z

    iget-object v6, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->g:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v6}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lapr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lapz;->a()Lapz;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lapz;->a(Ljava/lang/String;)Laqa;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Laqa;->b:Z

    :cond_0
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;
    .locals 2

    invoke-static {}, Lapj;->a()Lapj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lapj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Laso;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;-><init>(Laso;)V

    return-object v1
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 3

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    invoke-static {v0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lapp;->b:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Laso;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Laso;->o:Laso;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->d(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_0
    return-object p1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;
    .locals 3

    const-string v0, "Calling app description cannot be null!"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "AccountNameCheckRequest cannot be null!"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    new-instance v0, Lapf;

    iget-object v1, p0, Lapp;->c:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lapf;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2, p3, v1}, Lapf;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/util/concurrent/atomic/AtomicBoolean;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;
    .locals 7

    const-string v0, "accountRemovalRequest cannot be null!"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v0, "com.google"

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, v4, v0, v2}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    sget-object v1, Laso;->a:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;-><init>(Laso;)V

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    sget-object v1, Laso;->n:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;-><init>(Laso;)V

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    sget-object v1, Laso;->g:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;-><init>(Laso;)V

    goto :goto_1

    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    sget-object v1, Laso;->g:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;-><init>(Laso;)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    sget-object v1, Laso;->v:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;-><init>(Laso;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;
    .locals 3

    const-string v0, "clearTokenRequest cannot be null!"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;

    sget-object v1, Laso;->a:Laso;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;-><init>(Laso;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;
    .locals 5

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;

    iget-object v0, v1, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v3, v1, Laoz;->a:Landroid/accounts/Account;

    const-string v4, "oauthAccessToken"

    invoke-virtual {v0, v3, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v3, v1, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v1, v1, Laoz;->a:Landroid/accounts/Account;

    const-string v4, "services"

    invoke-virtual {v3, v1, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :goto_2
    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;-><init>(Ljava/lang/String;ZLjava/util/List;)V

    move-object v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;
    .locals 16

    new-instance v1, Lapk;

    move-object/from16 v0, p0

    iget-object v2, v0, Lapp;->c:Laoy;

    move-object/from16 v0, p1

    invoke-direct {v1, v2, v0}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    const-string v2, "LSID"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lapp;->c:Laoy;

    invoke-virtual {v3}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v10

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v3, p1

    move-object/from16 v12, p2

    invoke-virtual/range {v1 .. v15}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lapp;->c:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    invoke-static {v1}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v10

    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->k()Z

    move-result v6

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->m()Z

    move-result v7

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->i()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->j()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;
    .locals 6

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    new-instance v0, Lapf;

    iget-object v1, p0, Lapp;->c:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lapf;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lapf;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 12

    const/4 v11, 0x2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lapk;

    iget-object v2, p0, Lapp;->c:Laoy;

    invoke-direct {v0, v2, v1}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->a()Z

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->c()Z

    move-result v6

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->d()Z

    move-result v8

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->e()Z

    move-result v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    invoke-virtual/range {v0 .. v10}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZLandroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    sget-object v2, Laso;->a:Laso;

    if-eq v2, v1, :cond_1

    const-string v2, "GLSUser"

    invoke-static {v2, v11}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "%s createpPlusProfile wasn\'t successful: %s"

    new-array v3, v11, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lapp;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "GLSUser"

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Laso;->K:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_1
    invoke-direct {p0, v0}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 15

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lapk;

    iget-object v1, p0, Lapp;->c:Laoy;

    invoke-direct {v0, v1, v2}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapk;->b(Ljava/lang/String;)V

    const-string v1, "ac2dm"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v9, p0, Lapp;->c:Laoy;

    invoke-virtual {v9}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v11, p2

    invoke-virtual/range {v0 .. v14}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/AppDescription;Landroid/os/Bundle;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 8

    new-instance v0, Lapi;

    iget-object v1, p0, Lapp;->c:Laoy;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lapi;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "oauth1:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lapi;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->e()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->d()Ljava/lang/String;

    iget-object v3, p0, Lapp;->c:Laoy;

    iget-object v3, v3, Laoy;->a:Landroid/content/Context;

    const-string v4, "addAccount"

    invoke-static {v3, v4, v1}, Laph;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move v3, p2

    move v4, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v7}, Lapi;->a(Ljava/lang/String;ZZZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_1
    return-object v0

    :cond_2
    invoke-virtual {v0, v1}, Lapi;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 6

    const-string v0, "Calling AppDescription cannot be null!"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "TokenRequest cannot be null!"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    sget-object v0, Laqc;->o:Lbfy;

    invoke-virtual {v0}, Lbfy;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpg-double v0, v2, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    const-string v2, "getToken"

    invoke-static {v0, v2, v1}, Laph;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Lapk;

    iget-object v3, p0, Lapp;->c:Laoy;

    invoke-direct {v2, v3, v1}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2, p3, v0}, Lapk;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    invoke-direct {p0, v0}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 17

    const-string v1, "AppDescription cannot be null!"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "AccountCredentials cannot be null!"

    move-object/from16 v0, p2

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "GoogleAccountSetupRequest cannot be null!"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lapp;->c:Laoy;

    iget-object v2, v1, Laoy;->a:Landroid/content/Context;

    new-instance v1, Lapf;

    move-object/from16 v0, p0

    iget-object v3, v0, Lapp;->c:Laoy;

    iget-object v3, v3, Laoy;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, Lapf;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->e()Z

    move-result v5

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->g()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->h()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->b()Z

    move-result v11

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->c()Z

    move-result v12

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->f()Z

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->b()Ljava/lang/String;

    move-result-object v14

    const-string v15, "createAccount"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v2, v15, v0}, Laph;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v15, p4

    invoke-virtual/range {v1 .. v16}, Lapf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    return-object v1
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;
    .locals 4

    new-instance v0, Lapf;

    iget-object v1, p0, Lapp;->c:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lapf;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lapp;->c:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    const-string v2, "websetupurl"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Laph;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;->b:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lapf;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 1

    iget-object v0, p0, Lapp;->c:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lapk;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Lapk;

    iget-object v1, p0, Lapp;->c:Laoy;

    invoke-direct {v0, v1, p1}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual {v0}, Lapk;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lapk;

    iget-object v2, p0, Lapp;->c:Laoy;

    invoke-direct {v1, v2, v0}, Lapk;-><init>(Laoy;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_1

    invoke-virtual {v1, v2, p2}, Lapk;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    :cond_0
    return-object v0

    :cond_1
    if-nez v3, :cond_2

    if-eqz p2, :cond_3

    :cond_2
    invoke-virtual {v1, v0, v3, p2}, Lapk;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v0, "GLSUser"

    const-string v1, "Cannot update credentials when no credentials are supplied."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;-><init>()V

    sget-object v1, Laso;->u:Laso;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a(Laso;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    goto :goto_0
.end method
