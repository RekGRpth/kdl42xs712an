.class public final Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Lioj;

.field public b:Lipl;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    aget v3, v3, v1

    invoke-static {v3}, Lizn;->a(I)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-nez v0, :cond_1

    new-instance v0, Lioj;

    invoke-direct {v0}, Lioj;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-nez v0, :cond_2

    new-instance v0, Lipl;

    invoke-direct {v0}, Lipl;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_4

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lizm;->a()I

    :cond_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    array-length v3, v5

    if-ne v1, v3, :cond_6

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_7

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_8

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    if-eqz v0, :cond_c

    invoke-virtual {p1, v1}, Lizm;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    if-nez v1, :cond_a

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_a
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_b
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    :cond_c
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 3

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-nez v2, :cond_3

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {v2, v3}, Lioj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-nez v2, :cond_5

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-virtual {v2, v3}, Lipl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->c:[I

    invoke-static {v1}, Lizq;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->a:Lioj;

    invoke-virtual {v0}, Lioj;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateInstrumentPostResponse;->b:Lipl;

    invoke-virtual {v1}, Lipl;->hashCode()I

    move-result v1

    goto :goto_1
.end method
