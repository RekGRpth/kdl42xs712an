.class public Lcom/google/android/location/geofencer/service/GeofenceProviderService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/IBinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.android.location.service.GeofenceProvider"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lhyb;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GeofenceProviderService"

    const-string v1, "Got framework binding request."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/location/geofencer/service/GeofenceProviderService;->a:Landroid/os/IBinder;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const/16 v0, 0x12

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lhzj;->a()Lhzj;

    move-result-object v0

    invoke-virtual {v0}, Lhzj;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/GeofenceProviderService;->a:Landroid/os/IBinder;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "GeofenceProviderService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GeofenceProviderService"

    const-string v1, "Hardware geofence not available."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/geofencer/service/GeofenceProviderService;->a:Landroid/os/IBinder;

    goto :goto_0
.end method
