.class public Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Likx;


# instance fields
.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p1, p2}, Laur;->a(Ljava/lang/String;Ljava/lang/String;)Laur;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Laur;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    invoke-super {p0}, Lo;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    goto :goto_0

    :pswitch_1
    const-string v0, "activity_recognition_permission_whitelist"

    invoke-static {}, Lhzw;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, -0x1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a007b
        :pswitch_0    # com.google.android.gms.R.id.cancel_button
        :pswitch_1    # com.google.android.gms.R.id.accept_button
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    const v8, 0x7f0a007f    # com.google.android.gms.R.id.footnote

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v7}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->requestWindowFeature(I)Z

    const v0, 0x7f04001c    # com.google.android.gms.R.layout.activity_recognition_permission_dialog

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p0, v6}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a007c    # com.google.android.gms.R.id.accept_button

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v8}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    new-instance v0, Laoy;

    invoke-direct {v0, p0}, Laoy;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laoy;->d(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Laoy;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->n:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->n:Ljava/lang/String;

    if-nez v0, :cond_5

    :cond_3
    const-string v0, "ActivityRecogPermisAc"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ActivityRecogPermisAc"

    const-string v1, "Failed to get ApplicationInfo for package: %s"

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->o:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p0, v6}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->finish()V

    goto :goto_0

    :cond_5
    const v0, 0x7f0a0081    # com.google.android.gms.R.id.app_text

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b04cb    # com.google.android.gms.R.string.auth_app_permission_title

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->n:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0080    # com.google.android.gms.R.id.app_icon

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v8}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b04d0    # com.google.android.gms.R.string.auth_app_permission_footnote

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->n:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lbcg;

    const v1, 0x7f0b0451    # com.google.android.gms.R.string.activity_recognition_permission_label

    invoke-virtual {p0, v1}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b0452    # com.google.android.gms.R.string.activity_recognition_permission_description

    invoke-virtual {p0, v2}, Lcom/google/android/location/settings/ActivityRecognitionPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbcg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lbcg;->a()Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "activity_permission"

    invoke-virtual {v1, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const v2, 0x7f0a007e    # com.google.android.gms.R.id.scopes_layout

    invoke-static {v0}, Likv;->a(Lcom/google/android/gms/common/acl/ScopeData;)Likv;

    move-result-object v0

    const-string v3, "activity_permission"

    invoke-virtual {v1, v2, v0, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v1}, Lag;->c()I

    goto/16 :goto_0
.end method
