.class public Lcom/google/android/location/settings/LocationReportingSettingsActivity;
.super Liki;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Liki;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/location/reporting/service/AccountConfig;)I
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/AccountConfig;->n()I

    move-result v0

    return v0
.end method

.method protected final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->q:Liiw;

    iget-object v1, p0, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->s:Landroid/accounts/Account;

    invoke-interface {v0, v1, p1}, Liiw;->a(Landroid/accounts/Account;Z)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Liki;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0400a5    # com.google.android.gms.R.layout.location_reporting_settings

    invoke-virtual {p0, v0}, Lcom/google/android/location/settings/LocationReportingSettingsActivity;->setContentView(I)V

    :cond_0
    return-void
.end method
