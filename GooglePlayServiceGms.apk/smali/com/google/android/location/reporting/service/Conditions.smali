.class public Lcom/google/android/location/reporting/service/Conditions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Liiq;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Liiq;

    invoke-direct {v0}, Liiq;-><init>()V

    sput-object v0, Lcom/google/android/location/reporting/service/Conditions;->CREATOR:Liiq;

    return-void
.end method

.method public constructor <init>(IZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/reporting/service/Conditions;->a:I

    iput-boolean p2, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    iput-boolean p4, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    iput-boolean p5, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    iput-boolean p3, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    iput-boolean p6, p0, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    iput-boolean p7, p0, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    return-void
.end method

.method public constructor <init>(ZZZZZZ)V
    .locals 8

    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/location/reporting/service/Conditions;-><init>(IZZZZZZ)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/location/reporting/service/Conditions;->CREATOR:Liiq;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/reporting/service/Conditions;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/reporting/service/Conditions;

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/location/reporting/service/Conditions;->a:I

    iget v3, p1, Lcom/google/android/location/reporting/service/Conditions;->a:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/reporting/service/Conditions;->a:I

    return v0
.end method

.method public final h()Ljava/util/List;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x1

    const-string v3, "UnsupportedOs"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/16 v2, 0xb

    const-string v3, "RestrictedProfile"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x3

    const-string v3, "UnsupportedGeo"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x2

    const-string v3, "DeferringToGmm"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/location/reporting/service/Conditions;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/Conditions;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isIneligibleDueToGeoOnly()Z
    .locals 3
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/Conditions;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/location/reporting/InactiveReason;->a(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Conditions{supportedOs="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", restrictedProfile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", supportedGeo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deferToMaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", googleLocationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", locationEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/Conditions;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", versionCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/reporting/service/Conditions;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/location/reporting/service/Conditions;->CREATOR:Liiq;

    invoke-static {p0, p1}, Liiq;->a(Lcom/google/android/location/reporting/service/Conditions;Landroid/os/Parcel;)V

    return-void
.end method
