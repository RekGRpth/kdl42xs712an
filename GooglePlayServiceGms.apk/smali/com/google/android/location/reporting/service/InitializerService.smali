.class public Lcom/google/android/location/reporting/service/InitializerService;
.super Likg;
.source "SourceFile"


# instance fields
.field private a:Lijt;

.field private b:Liid;

.field private c:Liiv;

.field private d:Liky;

.field private e:Liin;

.field private f:Ljava/util/Map;

.field private g:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GCoreUlr-InitializerService"

    invoke-direct {p0, v0}, Likg;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lijt;Liid;Liiv;Liky;Liin;Landroid/content/Intent;)V
    .locals 1

    const-string v0, "InitializerServiceTest"

    invoke-direct {p0, v0}, Likg;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/location/reporting/service/InitializerService;->a:Lijt;

    iput-object p2, p0, Lcom/google/android/location/reporting/service/InitializerService;->b:Liid;

    iput-object p3, p0, Lcom/google/android/location/reporting/service/InitializerService;->c:Liiv;

    iput-object p4, p0, Lcom/google/android/location/reporting/service/InitializerService;->d:Liky;

    iput-object p5, p0, Lcom/google/android/location/reporting/service/InitializerService;->e:Liin;

    iput-object p6, p0, Lcom/google/android/location/reporting/service/InitializerService;->g:Landroid/content/Intent;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    return-void
.end method

.method private a()I
    .locals 14

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/InitializerService;->g:Landroid/content/Intent;

    if-nez v1, :cond_1

    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GCoreUlr"

    const-string v2, "Google Maps disabled, missing, or not integrated"

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v6, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/location/reporting/service/InitializerService;->d:Liky;

    iget-object v8, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/GmmSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/GmmSettings;->b()Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v9, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/reporting/GmmSettings;

    if-eqz v1, :cond_0

    const-string v4, "GCoreUlr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Maps reported both "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " and "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " for account "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/GmmSettings;->b()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lijy;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "GCoreUlr"

    const-string v2, "Google Maps integrated, binding so we can read its values..."

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/google/android/location/reporting/service/InitializerService;->c:Liiv;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/InitializerService;->g:Landroid/content/Intent;

    invoke-interface {v1, v2}, Liiv;->a(Landroid/content/Intent;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "GCoreUlr"

    const-string v3, "Failed to retrieve GMM settings, proceeding without them"

    invoke-static {v2, v3, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Likf;->a(Ljava/lang/Exception;)V

    :cond_3
    move-object v6, v0

    goto :goto_0

    :cond_4
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v11

    array-length v12, v11

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    if-ge v7, v12, :cond_6

    aget-object v13, v11, v7

    invoke-interface {v8, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihk;

    if-nez v0, :cond_8

    invoke-static {v13}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing server pref for account "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Recently added?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lijy;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lihk;

    const-wide/high16 v1, -0x8000000000000000L

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lihk;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;II)V

    move-object v1, v0

    :goto_3
    invoke-interface {v9, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/GmmSettings;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/location/reporting/GmmSettings;

    invoke-direct {v0, v13}, Lcom/google/android/gms/location/reporting/GmmSettings;-><init>(Landroid/accounts/Account;)V

    invoke-static {v13, v0, v1}, Lijf;->a(Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/GmmSettings;Lihk;)Liir;

    move-result-object v0

    :goto_4
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    :cond_5
    invoke-static {v13, v0, v1}, Lijf;->a(Landroid/accounts/Account;Lcom/google/android/gms/location/reporting/GmmSettings;Lihk;)Liir;

    move-result-object v0

    goto :goto_4

    :cond_6
    const-string v0, "GCoreUlr"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "computeStates(): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->a:Lijt;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "define("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Lijt;->a(Ljava/lang/String;Ljava/lang/Iterable;)Z

    const/4 v0, 0x1

    return v0

    :cond_8
    move-object v1, v0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/location/reporting/service/AccountConfig;)I
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/AccountConfig;->s()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/AccountConfig;->b()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;ZZ)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/location/reporting/service/ReportingConfig;)I
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/location/reporting/service/ReportingConfig;->a()Z

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/location/reporting/service/InitializerService;->a(Landroid/content/Context;ZZ)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;ZZ)I
    .locals 4

    invoke-static {p1, p2}, Lijt;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/location/reporting/service/InitializerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v0}, Likh;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    const/4 v0, 0x1

    :goto_1
    const-string v1, "GCoreUlr"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Skipping init, initialization="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (eligible="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", defined: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b()I
    .locals 12

    const/4 v3, 0x1

    const/4 v1, 0x3

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/location/reporting/service/InitializerService;->d:Liky;

    invoke-interface {v2}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v7

    array-length v8, v7

    move v6, v0

    move v4, v0

    move v2, v0

    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v9, v7, v6

    :try_start_0
    iget-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->b:Liid;

    invoke-interface {v0, v9}, Liid;->a(Landroid/accounts/Account;)Lihk;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    move v4, v2

    move v2, v3

    :goto_1
    iget-object v5, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    invoke-interface {v5, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v11, v2

    move v2, v4

    move v4, v11

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GCoreUlr"

    const-string v5, "InitializerService can\'t get server values"

    invoke-static {v2, v5, v0}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lihk;->a()Lihk;

    move-result-object v2

    invoke-static {p0}, Lbpu;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_2
    move-object v11, v2

    move v2, v4

    move v4, v0

    move-object v0, v11

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v5, v0

    const-string v0, "GCoreUlr"

    const-string v10, "InitializerService can\'t get server values"

    invoke-static {v0, v10, v5}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lihk;->a()Lihk;

    move-result-object v0

    instance-of v10, v5, Lane;

    if-nez v10, :cond_1

    instance-of v5, v5, Lanf;

    if-eqz v5, :cond_6

    :cond_1
    const/4 v2, 0x5

    move v11, v4

    move v4, v2

    move v2, v11

    goto :goto_1

    :cond_2
    if-nez v4, :cond_4

    if-eqz v2, :cond_4

    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GCoreUlr"

    const-string v1, "Failed to read server settings for all accounts, try again later"

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_3
    return v2

    :cond_4
    const-string v0, "GCoreUlr"

    invoke-static {v0, v1}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "GCoreUlr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Initialized server settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move v2, v3

    goto :goto_3

    :cond_6
    move v11, v4

    move v4, v2

    move v2, v11

    goto :goto_1
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 4

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->a:Lijt;

    invoke-virtual {v0}, Lijt;->a()Lcom/google/android/location/reporting/service/ReportingConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/ReportingConfig;->f()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "GCoreUlr"

    invoke-static {v1, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreUlr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleIntent(): should define is false; defined: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/ReportingConfig;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; eligible: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/ReportingConfig;->g()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/ReportingConfig;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "GCoreUlr"

    invoke-static {v0, v2}, Lijy;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GCoreUlr"

    const-string v1, "handleIntent() starting initialization"

    invoke-static {v0, v1}, Lijy;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->e:Liin;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Liin;->a(I)V

    invoke-direct {p0}, Lcom/google/android/location/reporting/service/InitializerService;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/location/reporting/service/InitializerService;->a()I

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Likg;->onCreate()V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    new-instance v0, Lijt;

    invoke-direct {v0, p0}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->a:Lijt;

    new-instance v0, Liib;

    invoke-direct {v0, p0}, Liib;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->b:Liid;

    new-instance v0, Liji;

    invoke-direct {v0, p0}, Liji;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->c:Liiv;

    new-instance v0, Likz;

    invoke-direct {v0, p0}, Likz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->d:Liky;

    new-instance v0, Liim;

    invoke-direct {v0, p0}, Liim;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->e:Liin;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/InitializerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lesr;->a(Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->g:Landroid/content/Intent;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/InitializerService;->f:Ljava/util/Map;

    return-void
.end method
