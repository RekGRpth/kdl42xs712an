.class public final Lcom/google/android/location/reporting/service/AccountConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Liij;


# instance fields
.field private final a:I

.field private final b:Landroid/accounts/Account;

.field private final c:Z

.field private final d:J

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z

.field private final i:J

.field private final j:I

.field private final k:Z

.field private final l:Z

.field private final m:Lcom/google/android/location/reporting/service/Conditions;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Liij;

    invoke-direct {v0}, Liij;-><init>()V

    sput-object v0, Lcom/google/android/location/reporting/service/AccountConfig;->CREATOR:Liij;

    return-void
.end method

.method public constructor <init>(ILandroid/accounts/Account;ZJZZZZJIZZLcom/google/android/location/reporting/service/Conditions;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->a:I

    iput-object p2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    iput-boolean p3, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    iput-wide p4, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    iput-boolean p6, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    iput-boolean p7, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    iput-boolean p8, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    iput-boolean p9, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    iput-wide p10, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    iput p12, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    iput-boolean p13, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    iput-boolean p14, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    iput-object p15, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    return-void
.end method

.method private constructor <init>(Liii;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->a:I

    iget-object v0, p1, Liii;->a:Landroid/accounts/Account;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    iget-object v0, p1, Liii;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    iget-object v0, p1, Liii;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    iget-object v0, p1, Liii;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    iget-object v0, p1, Liii;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    iget-object v0, p1, Liii;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    iget-object v0, p1, Liii;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    iget-object v0, p1, Liii;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    iget-object v0, p1, Liii;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    iget-object v0, p1, Liii;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    iget-object v0, p1, Liii;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    iget-object v0, p1, Liii;->l:Lcom/google/android/location/reporting/service/Conditions;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/reporting/service/Conditions;

    iput-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    return-void
.end method

.method public synthetic constructor <init>(Liii;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/location/reporting/service/AccountConfig;-><init>(Liii;)V

    return-void
.end method

.method private a(Z)I
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    if-nez v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    if-eqz v1, :cond_2

    if-nez p1, :cond_0

    const/4 v0, -0x2

    goto :goto_0

    :cond_2
    if-nez p1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/accounts/Account;)Liii;
    .locals 2

    new-instance v0, Liii;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Liii;-><init>(Landroid/accounts/Account;B)V

    return-object v0
.end method

.method private w()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->n()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->o()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->w()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->u()Z

    move-result v4

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->d()Z

    move-result v5

    new-instance v0, Lcom/google/android/gms/location/reporting/ReportingState;

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/location/reporting/ReportingState;-><init>(IIZZZI)V

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    return v0
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    return-wide v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/location/reporting/service/AccountConfig;->CREATOR:Liij;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/location/reporting/service/AccountConfig;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/location/reporting/service/AccountConfig;

    iget-object v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    iget-wide v4, p1, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    iget-wide v4, p1, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    iget v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    iget-boolean v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    iget-object v3, p1, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v2, v3}, Lcom/google/android/location/reporting/service/Conditions;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    return v0
.end method

.method public final h()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    return v0
.end method

.method public final l()Lcom/google/android/location/reporting/service/Conditions;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    return-object v0
.end method

.method public final m()I
    .locals 1

    iget v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->a:I

    return v0
.end method

.method public final n()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a(Z)I

    move-result v0

    return v0
.end method

.method public final o()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    invoke-direct {p0, v0}, Lcom/google/android/location/reporting/service/AccountConfig;->a(Z)I

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    iget v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->h()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/util/List;
    .locals 5

    const/4 v4, 0x7

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/Conditions;->f()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/16 v2, 0xc

    const-string v3, "LocationDisabled"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/Conditions;->e()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x4

    const-string v3, "GoogleLocationDisabled"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x5

    const-string v3, "NotGoogleAccountOnDevice"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    if-nez v1, :cond_4

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/16 v2, 0xa

    const-string v3, "AuthError"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    if-nez v1, :cond_5

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const/4 v2, 0x6

    const-string v3, "ReportingNotSelected"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "UnknownRestriction"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_6

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_1
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "AgeUnder13"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_2
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "AgeUnknown"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_3
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "DasherPolicy"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_4
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "LegalCountry"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_5
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "AccountDeleted"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    :pswitch_6
    new-instance v1, Lcom/google/android/gms/location/reporting/InactiveReason;

    const-string v2, "AccountPurged"

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/location/reporting/InactiveReason;-><init>(ILjava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final s()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->i()Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AccountConfig{mAccount="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    invoke-static {v1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReporting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->u()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDefined="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mValidAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAmbiguous="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReportingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHistoryEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mServerMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRestriction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->k:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDirty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mConditions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->m:Lcom/google/android/location/reporting/service/Conditions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->r()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/location/reporting/service/AccountConfig;->b:Landroid/accounts/Account;

    invoke-static {v1}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": ambiguous="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", restriction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", reporting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", history="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->o()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", serverMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/location/reporting/service/AccountConfig;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", reasons="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/AccountConfig;->r()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/location/reporting/service/AccountConfig;->CREATOR:Liij;

    invoke-static {p0, p1, p2}, Liij;->a(Lcom/google/android/location/reporting/service/AccountConfig;Landroid/os/Parcel;I)V

    return-void
.end method
