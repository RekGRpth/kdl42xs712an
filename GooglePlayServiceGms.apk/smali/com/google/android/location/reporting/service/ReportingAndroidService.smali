.class public Lcom/google/android/location/reporting/service/ReportingAndroidService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lijt;

.field private b:Lihy;

.field private c:Landroid/content/pm/PackageManager;

.field private d:Lcom/google/android/location/reporting/ApiMetadataStore;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/reporting/service/ReportingAndroidService;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->c:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/location/reporting/service/ReportingAndroidService;)Lijt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->a:Lijt;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/location/reporting/service/ReportingAndroidService;)Lihy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->b:Lihy;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/location/reporting/service/ReportingAndroidService;)Lcom/google/android/location/reporting/ApiMetadataStore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->d:Lcom/google/android/location/reporting/ApiMetadataStore;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "com.google.android.gms.location.reporting.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lijm;

    invoke-direct {v0, p0}, Lijm;-><init>(Lcom/google/android/location/reporting/service/ReportingAndroidService;)V

    invoke-virtual {v0}, Lijm;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {p0}, Likh;->a(Landroid/content/Context;)V

    new-instance v0, Lijt;

    invoke-direct {v0, p0}, Lijt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->a:Lijt;

    invoke-static {p0}, Lihy;->a(Landroid/content/Context;)Lihy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->b:Lihy;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingAndroidService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->c:Landroid/content/pm/PackageManager;

    new-instance v0, Lcom/google/android/location/reporting/ApiMetadataStore;

    invoke-virtual {p0}, Lcom/google/android/location/reporting/service/ReportingAndroidService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lihq;

    iget-object v3, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->b:Lihy;

    invoke-direct {v2, v3}, Lihq;-><init>(Lihy;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/reporting/ApiMetadataStore;-><init>(Landroid/content/Context;Lihq;)V

    iput-object v0, p0, Lcom/google/android/location/reporting/service/ReportingAndroidService;->d:Lcom/google/android/location/reporting/ApiMetadataStore;

    return-void
.end method
