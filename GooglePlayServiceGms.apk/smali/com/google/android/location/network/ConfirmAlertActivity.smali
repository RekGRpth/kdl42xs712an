.class public Lcom/google/android/location/network/ConfirmAlertActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private c:Z

.field private d:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->d:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic a(I)I
    .locals 0

    sput p0, Lcom/google/android/location/network/ConfirmAlertActivity;->a:I

    return p0
.end method

.method static synthetic b(I)I
    .locals 0

    sput p0, Lcom/google/android/location/network/ConfirmAlertActivity;->b:I

    return p0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    invoke-virtual {p0}, Lcom/google/android/location/network/ConfirmAlertActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "confirmLgaayl"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/location/network/ConfirmAlertActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/network/ConfirmAlertActivity;->finish()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0423    # com.google.android.gms.R.string.location_warning_title

    invoke-virtual {p0, v1}, Lcom/google/android/location/network/ConfirmAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0424    # com.google.android.gms.R.string.location_warning_message

    invoke-virtual {p0, v1}, Lcom/google/android/location/network/ConfirmAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0425    # com.google.android.gms.R.string.agree

    invoke-virtual {p0, v1}, Lcom/google/android/location/network/ConfirmAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0b0426    # com.google.android.gms.R.string.disagree

    invoke-virtual {p0, v1}, Lcom/google/android/location/network/ConfirmAlertActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Licg;

    invoke-direct {v1, p0}, Licg;-><init>(Lcom/google/android/location/network/ConfirmAlertActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->d:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->d:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-boolean v0, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/location/network/ConfirmAlertActivity;->a:I

    if-ne v0, v3, :cond_0

    sget v0, Lcom/google/android/location/network/ConfirmAlertActivity;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/location/network/ConfirmAlertActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    sput v2, Lcom/google/android/location/network/ConfirmAlertActivity;->a:I

    sput v2, Lcom/google/android/location/network/ConfirmAlertActivity;->b:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/location/network/ConfirmAlertActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/location/network/ConfirmAlertActivity;->c:Z

    invoke-static {v0, v1}, Lcom/google/android/location/network/NetworkLocationService;->a(Landroid/content/Context;Z)V

    return-void
.end method
