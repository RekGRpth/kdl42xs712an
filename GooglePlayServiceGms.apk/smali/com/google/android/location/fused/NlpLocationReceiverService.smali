.class public Lcom/google/android/location/fused/NlpLocationReceiverService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lhvp;

.field private b:Landroid/os/PowerManager$WakeLock;

.field private c:Lilp;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/location/fused/NlpLocationReceiverService;)Lhvp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->a:Lhvp;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    const/4 v4, 0x1

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {p0}, Lcom/google/android/location/fused/NlpLocationReceiverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhvx;->a(Landroid/content/Context;)Lhvx;

    move-result-object v1

    iget-object v2, v1, Lhvx;->a:Lhvp;

    invoke-virtual {v1}, Lhvx;->d()Landroid/os/Looper;

    move-result-object v1

    const-string v3, "GCoreFlp"

    invoke-virtual {v0, v4, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->b:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    iput-object v2, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->a:Lhvp;

    new-instance v0, Lhwq;

    invoke-direct {v0, p0, v1}, Lhwq;-><init>(Lcom/google/android/location/fused/NlpLocationReceiverService;Landroid/os/Looper;)V

    new-instance v1, Lilp;

    iget-object v2, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-direct {v1, v2, v0}, Lilp;-><init>(Landroid/os/PowerManager$WakeLock;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->c:Lilp;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    const/4 v2, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->a:Lhvp;

    iget-object v0, v0, Lhvp;->a:Landroid/content/Context;

    invoke-static {v0}, Lbqf;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->b:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->a:Lhvp;

    iget-object v1, v1, Lhvp;->n:Lhwb;

    invoke-virtual {v1}, Lhwb;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, Lile;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    :cond_2
    const-string v0, "com.google.android.location.internal.WIFI_LOCATION_STATUS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "com.google.android.location.internal.CELL_LOCATION_STATUS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    const-string v0, "GCoreFlp"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "Received status changed intent: %s"

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    const-string v0, "com.google.android.location.internal.WIFI_LOCATION_STATUS"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "com.google.android.location.internal.CELL_LOCATION_STATUS"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->c:Lilp;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Lilp;->a(IIILjava/lang/Object;)V

    goto :goto_0

    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "GCoreFlp"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Received activity result intent: %s"

    new-array v1, v5, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->c:Lilp;

    invoke-static {p1}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v4, v1}, Lilp;->a(IIILjava/lang/Object;)V

    goto :goto_0

    :cond_7
    invoke-static {p1}, Lhpg;->a(Landroid/content/Intent;)Lcom/google/android/location/clientlib/NlpLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/location/clientlib/NlpLocation;->a:Landroid/location/Location;

    if-eqz v1, :cond_0

    const-string v1, "GCoreFlp"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "Received NLP location: %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, v0, Lcom/google/android/location/clientlib/NlpLocation;->a:Landroid/location/Location;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lhwo;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_8
    iget-object v0, v0, Lcom/google/android/location/clientlib/NlpLocation;->a:Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/location/fused/NlpLocationReceiverService;->c:Lilp;

    invoke-virtual {v1, v5, v4, v4, v0}, Lilp;->a(IIILjava/lang/Object;)V

    goto/16 :goto_0
.end method
