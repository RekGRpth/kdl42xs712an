.class public final Lcom/google/android/gms/location/places/internal/OpeningHoursImpl;
.super Lere;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Leru;


# instance fields
.field public final a:I

.field public final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leru;

    invoke-direct {v0}, Leru;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl;->CREATOR:Leru;

    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 0

    invoke-direct {p0}, Lere;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/internal/OpeningHoursImpl;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Leru;->a(Lcom/google/android/gms/location/places/internal/OpeningHoursImpl;Landroid/os/Parcel;)V

    return-void
.end method
