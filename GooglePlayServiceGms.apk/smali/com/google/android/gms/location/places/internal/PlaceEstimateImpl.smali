.class public final Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;
.super Lerh;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Lerw;


# instance fields
.field public final a:I

.field private final b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

.field private final c:[F

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lerw;

    invoke-direct {v0}, Lerw;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->CREATOR:Lerw;

    return-void
.end method

.method public constructor <init>(I[Lcom/google/android/gms/location/places/internal/PlaceImpl;[FJ)V
    .locals 2

    invoke-direct {p0}, Lerh;-><init>()V

    array-length v0, p2

    array-length v1, p3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "mismatched places to probabilities arrays"

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->a:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    iput-object p3, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    iput-wide p4, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->d:J

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Lcom/google/android/gms/location/places/internal/PlaceImpl;[FJ)Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;
    .locals 6

    const/4 v1, 0x0

    array-length v0, p0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "mismatched places to probabilities arrays"

    invoke-static {v0, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;-><init>(I[Lcom/google/android/gms/location/places/internal/PlaceImpl;[FJ)V

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    aget v0, v0, p1

    return v0
.end method

.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    array-length v0, v0

    return v0
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->d:J

    return-wide v0
.end method

.method public final c()[Lcom/google/android/gms/location/places/internal/PlaceImpl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    return-object v0
.end method

.method public final d()[F
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->CREATOR:Lerw;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    iget-object v3, p1, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    iget-object v3, p1, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "PlaceEstimate{"

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    const-string v3, "(%f, %s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->c:[F

    aget v5, v5, v0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->b:[Lcom/google/android/gms/location/places/internal/PlaceImpl;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-eq v0, v3, :cond_0

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;->CREATOR:Lerw;

    invoke-static {p0, p1, p2}, Lerw;->a(Lcom/google/android/gms/location/places/internal/PlaceEstimateImpl;Landroid/os/Parcel;I)V

    return-void
.end method
