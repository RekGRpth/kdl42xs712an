.class public Lcom/google/android/gms/panorama/PanoramaViewActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static d:Leym;


# instance fields
.field protected a:Lezx;

.field private b:Lezs;

.field private c:Z

.field private e:Landroid/os/PowerManager$WakeLock;

.field private f:Lezf;

.field private g:Lezd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Leym;

    invoke-direct {v0}, Leym;-><init>()V

    sput-object v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->d:Leym;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :cond_0
    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "temp_pano.jpg"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v4, 0x1000

    new-array v4, v4, [B

    :goto_1
    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_2

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "PanoramaViewActivity"

    const-string v3, "Could not open file. "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "PanoramaViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrote stream to temporary file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lezx;->b(Z)V

    iput-boolean p1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    iget-object v3, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->b:Lezs;

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0200e4    # com.google.android.gms.R.drawable.ic_360pano_view

    :goto_1
    invoke-virtual {v3, v0}, Lezs;->setImageResource(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v0}, Lezx;->b()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-virtual {v0, p0}, Lezf;->a(Landroid/content/Context;)Lezf;

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v0, v1}, Lezx;->a(Z)V

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const v0, 0x7f0200fe    # com.google.android.gms.R.drawable.ic_compass

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-virtual {v0}, Lezf;->a()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v0, v2}, Lezx;->a(Z)V

    goto :goto_2
.end method

.method public static synthetic b(Lcom/google/android/gms/panorama/PanoramaViewActivity;)Lezs;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->b:Lezs;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->g:Lezd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->g:Lezd;

    invoke-virtual {v0}, Lezd;->a()V

    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    const/high16 v2, 0x200000

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a(Z)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Landroid/view/InputDevice;->getMotionRange(I)Landroid/view/InputDevice$MotionRange;

    move-result-object v1

    invoke-virtual {v2, v8}, Landroid/view/InputDevice;->getMotionRange(I)Landroid/view/InputDevice$MotionRange;

    move-result-object v0

    :goto_1
    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/view/InputDevice$MotionRange;->getMax()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getMax()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    mul-float v5, v2, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    mul-float v6, v1, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v7

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v1, v0}, Lezx;->a(Landroid/view/MotionEvent;)Z

    move v0, v8

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v0}, Lezx;->c()V

    invoke-direct {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a()V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-virtual {v0}, Lezf;->a()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    new-instance v0, Lezf;

    invoke-direct {v0}, Lezf;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v2, Lezd;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-direct {v2, v0, v3}, Lezd;-><init>(Landroid/view/Display;Lezf;)V

    iput-object v2, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->g:Lezd;

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->g:Lezd;

    invoke-virtual {v0}, Lezd;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-virtual {v0, p0}, Lezf;->a(Landroid/content/Context;)Lezf;

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a()V

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v3, 0x2000001a

    const-string v4, "PanoramaViewActivity"

    invoke-virtual {v0, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "PanoramaViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Attempting to show panorama : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lexz;

    invoke-direct {v0, p0}, Lexz;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    new-instance v3, Lezq;

    invoke-direct {v3, v2}, Lezq;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lezp;->a(Lezo;)Lezp;

    move-result-object v3

    if-nez v3, :cond_3

    const v1, 0x7f0b01cc    # com.google.android.gms.R.string.panorama_image_doesnt_contain_metadata

    invoke-static {v1, p0, v0}, Lezm;->a(ILandroid/content/Context;Lezl;)V

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    const-string v1, "PanoramaViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not load file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f0b01cd    # com.google.android.gms.R.string.panorama_image_file_could_not_be_read

    invoke-static {v1, p0, v0}, Lezm;->a(ILandroid/content/Context;Lezl;)V

    goto :goto_0

    :cond_4
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xa

    if-ge v0, v2, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    new-instance v0, Lezv;

    invoke-direct {v0, v4}, Lezv;-><init>(Ljava/io/File;)V

    :goto_2
    new-instance v1, Lezx;

    new-instance v2, Lezw;

    invoke-direct {v2, v0, v3}, Lezw;-><init>(Lfaf;Lezp;)V

    invoke-direct {v1, p0, v2}, Lezx;-><init>(Landroid/content/Context;Lezw;)V

    iput-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    new-instance v1, Leya;

    invoke-direct {v1, p0}, Leya;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-virtual {v0, v1}, Lezx;->b(Lezl;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    new-instance v1, Leyb;

    invoke-direct {v1, p0}, Leyb;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-virtual {v0, v1}, Lezx;->a(Lezl;)V

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lezs;

    invoke-direct {v1, p0}, Lezs;-><init>(Landroid/content/Context;)V

    new-instance v2, Leyc;

    invoke-direct {v2, p0}, Leyc;-><init>(Lcom/google/android/gms/panorama/PanoramaViewActivity;)V

    invoke-virtual {v1, v2}, Lezs;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->b:Lezs;

    iget-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->b:Lezs;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/panorama/PanoramaViewActivity;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->a:Lezx;

    iget-object v1, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->g:Lezd;

    iget-object v2, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->f:Lezf;

    invoke-virtual {v0, v1, v2}, Lezx;->a(Lezd;Lezf;)V

    iget-object v0, p0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->b:Lezs;

    invoke-virtual {v0}, Lezs;->a()V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    sget-object v0, Lcom/google/android/gms/panorama/PanoramaViewActivity;->d:Leym;

    invoke-static {v4, v0}, Lfag;->a(Ljava/io/File;Leym;)Lfaf;

    move-result-object v0

    goto :goto_2
.end method
