.class public Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lahs;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:[Lcom/google/android/gms/appdatasearch/Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lahs;

    invoke-direct {v0}, Lahs;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lahs;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->a:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->c:[Lcom/google/android/gms/appdatasearch/Feature;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lahs;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lahs;

    invoke-static {p0, p1, p2}, Lahs;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;Landroid/os/Parcel;I)V

    return-void
.end method
