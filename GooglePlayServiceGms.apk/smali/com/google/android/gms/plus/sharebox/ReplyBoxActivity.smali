.class public Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lgok;
.implements Lgpd;
.implements Lgph;


# instance fields
.field private n:Lgpb;

.field private o:Lgpe;

.field private p:Lcom/google/android/gms/common/people/data/Audience;

.field private q:Lfrb;

.field private r:Ljava/lang/String;

.field private s:Lgpx;

.field private final t:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    new-instance v0, Lgoz;

    invoke-direct {v0, p0}, Lgoz;-><init>(Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->t:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;)Lgoj;
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const v0, 0x7f0b03b0    # com.google.android.gms.R.string.plus_replybox_confirm_cancel_dialog_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgoj;->a(Ljava/lang/String;)Lgoj;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lgoj;

    goto :goto_0
.end method

.method private a(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b(I)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->l()V

    return-void
.end method

.method private b(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private k()V
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "post_error_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbof;

    if-nez v0, :cond_0

    const v0, 0x7f0b03b3    # com.google.android.gms.R.string.plus_replybox_post_error_message

    invoke-static {v0}, Lbof;->c(I)Lbof;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "post_error_dialog"

    invoke-virtual {v0, v1, v2}, Lbof;->a(Lu;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final a(Lbbo;)V
    .locals 3

    const v2, 0x7f0b03b2    # com.google.android.gms.R.string.plus_replybox_internal_error

    if-nez p1, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, p0, v0}, Lbbo;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "ReplyBox"

    const-string v1, "Failed to start connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    :cond_1
    const-string v0, "ReplyBox"

    const-string v1, "Failed connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    sget-object v1, Lbcz;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->l()V

    return-void
.end method

.method public final b(Lbbo;)V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgqe;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgqe;->a()V

    :cond_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    sget-object v1, Lbcz;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    const v0, 0x7f0b03b4    # com.google.android.gms.R.string.plus_replybox_post_success

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    sget-object v1, Lbcz;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->k()V

    goto :goto_0
.end method

.method public final e()Lgpx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    return-object v0
.end method

.method public final f()Lgpe;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    return-object v0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    invoke-virtual {v0}, Lgpb;->K()V

    return-void
.end method

.method public getCallingPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lfrb;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->q:Lfrb;

    return-object v0
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    invoke-virtual {v1}, Lgpb;->J()Z

    move-result v1

    invoke-virtual {v0, v1}, Lgpb;->a(Z)V

    return-void
.end method

.method public final j()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    invoke-virtual {v0}, Lgpb;->b()Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f0b03d1    # com.google.android.gms.R.string.plus_sharebox_post_pending

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lgqe;->a(Ljava/lang/CharSequence;)Lgqe;

    move-result-object v1

    iget-object v2, p0, Lo;->b:Lw;

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v1, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v2}, Lag;->d()I

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    invoke-virtual {v1, v0}, Lgpe;->a(Lcom/google/android/gms/plus/model/posts/Comment;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->k()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lo;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const-string v0, "ReplyBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to resolve connection/account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f0b03b2    # com.google.android.gms.R.string.plus_replybox_internal_error

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    invoke-virtual {v0}, Lgpb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->t:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    sget-object v1, Lbcz;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpe;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    invoke-super {p0}, Lo;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    const v4, 0x7f0b03b2    # com.google.android.gms.R.string.plus_replybox_internal_error

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->r:Ljava/lang/String;

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ReplyBox"

    const-string v1, "No accounts available to reply"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lbpu;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0b03b5    # com.google.android.gms.R.string.plus_replybox_no_network_connection

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->r:Ljava/lang/String;

    invoke-static {p0, v0}, Lgpq;->b(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ReplyBox"

    const-string v1, "Invalid reply action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->l()V

    goto :goto_0

    :cond_2
    new-instance v0, Lgpx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lgpx;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-direct {v3, v2, v5, v5}, Lcom/google/android/gms/plus/model/posts/PlusPage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, v0, Lgpx;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    :cond_3
    const-string v2, "com.google.android.gms.plus.intent.extra.INTERNAL_REPLY_ACTIVITY_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lgpx;->g:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.INTERNAL_REPLY_ADD_COMMENT_HINT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lgpx;->h:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lgpx;->f:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lgpx;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    invoke-virtual {v0}, Lgpx;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ReplyBox"

    const-string v1, "No account name provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    iget-object v0, v0, Lgpx;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_6

    const-string v0, "ReplyBox"

    const-string v1, "No activity ID provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const v0, 0x7f040101    # com.google.android.gms.R.layout.plus_replybox_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setContentView(I)V

    if-eqz p1, :cond_9

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->p:Lcom/google/android/gms/common/people/data/Audience;

    :goto_2
    new-instance v0, Lfrb;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->p:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v0, v1}, Lfrb;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->q:Lfrb;

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v2

    const-string v0, "reply_worker_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpe;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->s:Lgpx;

    iget-object v0, v0, Lgpx;->a:Ljava/lang/String;

    invoke-static {v0}, Lgpe;->a(Ljava/lang/String;)Lgpe;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->o:Lgpe;

    const-string v3, "reply_worker_fragment"

    invoke-virtual {v2, v0, v3}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_7
    const-string v0, "reply_fragment"

    invoke-virtual {v1, v0}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgpb;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    if-nez v0, :cond_8

    new-instance v0, Lgpb;

    invoke-direct {v0}, Lgpb;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    const v0, 0x7f0a02b5    # com.google.android.gms.R.id.post_container

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    const-string v3, "reply_fragment"

    invoke-virtual {v2, v0, v1, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->n:Lgpb;

    invoke-virtual {v2, v0}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v2}, Lag;->c()I

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lbkw;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->p:Lcom/google/android/gms/common/people/data/Audience;

    goto :goto_2
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lo;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgoj;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lgoj;->a(Lgok;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "audience"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->p:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const v0, 0x7f0a02b5    # com.google.android.gms.R.id.post_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lbow;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->onBackPressed()V

    invoke-static {p0, v0}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lo;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
