.class public Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;
.super Lfyv;
.source "SourceFile"

# interfaces
.implements Lfza;


# instance fields
.field private q:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lfyv;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "button_text"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2}, Lfyv;->a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lfyv;->a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a()V
    .locals 0

    invoke-super {p0}, Lfyv;->a()V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfyv;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    invoke-super {p0}, Lfyv;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lfyv;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final e()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lfyv;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400f8    # com.google.android.gms.R.layout.plus_oob_upgrade_account_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->setContentView(I)V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->q:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->q:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->g()Z

    move-result v2

    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "button_text"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v4, v1}, Lfyz;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfyz;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->q:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a02ae    # com.google.android.gms.R.id.content_layout

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->q:Landroid/support/v4/app/Fragment;

    const-string v3, "content_fragment"

    invoke-virtual {v0, v1, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    return-void
.end method
