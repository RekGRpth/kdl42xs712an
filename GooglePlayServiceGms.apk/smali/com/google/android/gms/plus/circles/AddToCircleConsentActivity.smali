.class public final Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfsq;


# static fields
.field private static final n:Lfav;


# instance fields
.field private o:Lfaj;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfsn;

    invoke-direct {v0}, Lfsn;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->n:Lfav;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final b(Z)V
    .locals 5

    const/4 v4, 0x1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    sget-object v1, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->n:Lfav;

    iget-object v2, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->q:Ljava/lang/String;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0, v1, v2, v3}, Lfch;->a(Lfav;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->finish()V

    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v5}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->finish()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v0, "com.google.android.gms.plus.circles.EXTRA_APPLICATION_ID"

    invoke-virtual {v6, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_0
    new-instance v0, Lfaj;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    const-string v0, "com.google.android.gms.plus.circles.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->p:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.circles.EXTRA_PAGE_ID"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->q:Ljava/lang/String;

    const-string v0, "com.google.android.gms.plus.circles.EXTRA_CONSENT_HTML"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.circles.EXTRA_TITLE_TEXT"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.circles.EXTRA_BUTTON_TEXT"

    invoke-virtual {v6, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lo;->b:Lw;

    const-string v4, "consentDialog"

    invoke-virtual {v3, v4}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-static {v0, v1, v2}, Lfsp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfsp;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "consentDialog"

    invoke-virtual {v0, v1, v2}, Lfsp;->a(Lu;Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    move-object v5, v0

    goto :goto_0
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/circles/AddToCircleConsentActivity;->o:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-super {p0}, Lo;->onStop()V

    return-void
.end method
