.class public Lcom/google/android/gms/plus/model/posts/Settings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lfya;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/gms/common/people/data/Audience;

.field private final c:Lcom/google/android/gms/common/people/data/Audience;

.field private final d:Z

.field private final e:Z

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfya;

    invoke-direct {v0}, Lfya;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/model/posts/Settings;->CREATOR:Lfya;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;ZZIII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->a:I

    iput-object p2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    iput-object p3, p0, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    iput-boolean p4, p0, Lcom/google/android/gms/plus/model/posts/Settings;->d:Z

    iput-boolean p5, p0, Lcom/google/android/gms/plus/model/posts/Settings;->e:Z

    iput p6, p0, Lcom/google/android/gms/plus/model/posts/Settings;->f:I

    iput p7, p0, Lcom/google/android/gms/plus/model/posts/Settings;->g:I

    iput p8, p0, Lcom/google/android/gms/plus/model/posts/Settings;->h:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;ZZIII)V
    .locals 9

    const/4 v1, 0x2

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/model/posts/Settings;-><init>(ILcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;ZZIII)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->d:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/gms/plus/model/posts/Settings;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/model/posts/Settings;

    iget v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v2}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->d:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->d:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->e:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->e:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->f:I

    iget v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->f:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->g:I

    iget v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->g:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/plus/model/posts/Settings;->h:I

    iget v2, p1, Lcom/google/android/gms/plus/model/posts/Settings;->h:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->e:Z

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->f:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->g:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->b:Lcom/google/android/gms/common/people/data/Audience;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->c:Lcom/google/android/gms/common/people/data/Audience;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Settings;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->h:I

    return v0
.end method

.method public final j()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Settings;->a:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lfya;->a(Lcom/google/android/gms/plus/model/posts/Settings;Landroid/os/Parcel;I)V

    return-void
.end method
