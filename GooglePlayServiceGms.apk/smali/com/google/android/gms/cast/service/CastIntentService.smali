.class public Lcom/google/android/gms/cast/service/CastIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/service/CastIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "CastIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;)V
    .locals 1

    new-instance v0, Lbaq;

    invoke-direct {v0, p1}, Lbaq;-><init>(Lbac;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;DDZ)V
    .locals 7

    new-instance v0, Lbbb;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lbbb;-><init>(Lbac;DDZ)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;I)V
    .locals 1

    new-instance v0, Lbbd;

    invoke-direct {v0, p1, p2}, Lbbd;-><init>(Lbac;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lbbg;

    invoke-direct {v0, p1, p2}, Lbbg;-><init>(Lbac;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lbas;

    invoke-direct {v0, p1, p2, p3}, Lbas;-><init>(Lbac;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    new-instance v0, Lbaz;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lbaz;-><init>(Lbac;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    new-instance v0, Lbat;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1, p4}, Lbat;-><init>(Lbac;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/lang/String;[BJLjava/lang/String;)V
    .locals 7

    new-instance v0, Lbay;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lbay;-><init>(Lbac;Ljava/lang/String;[BJLjava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;Ljava/nio/ByteBuffer;)V
    .locals 1

    new-instance v0, Lbav;

    invoke-direct {v0, p1, p2}, Lbav;-><init>(Lbac;Ljava/nio/ByteBuffer;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lbac;ZDZ)V
    .locals 6

    new-instance v0, Lbba;

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lbba;-><init>(Lbac;ZDZ)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lbak;)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    sget-object v0, Lcom/google/android/gms/cast/service/CastIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.cast.service.INTENT"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static b(Landroid/content/Context;Lbac;)V
    .locals 1

    new-instance v0, Lbar;

    invoke-direct {v0, p1}, Lbar;-><init>(Lbac;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lbac;I)V
    .locals 1

    new-instance v0, Lbbe;

    invoke-direct {v0, p1, p2}, Lbbe;-><init>(Lbac;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lbac;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lbaw;

    invoke-direct {v0, p1, p2}, Lbaw;-><init>(Lbac;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lbac;)V
    .locals 1

    new-instance v0, Lbau;

    invoke-direct {v0, p1}, Lbau;-><init>(Lbac;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static c(Landroid/content/Context;Lbac;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lbbi;

    invoke-direct {v0, p1, p2}, Lbbi;-><init>(Lbac;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static d(Landroid/content/Context;Lbac;)V
    .locals 1

    new-instance v0, Lbax;

    invoke-direct {v0, p1}, Lbax;-><init>(Lbac;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method

.method public static e(Landroid/content/Context;Lbac;)V
    .locals 1

    new-instance v0, Lbbc;

    invoke-direct {v0, p1}, Lbbc;-><init>(Lbac;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastIntentService;->a(Landroid/content/Context;Lbak;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/cast/service/CastIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbak;

    if-nez v0, :cond_0

    const-string v0, "CastIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lbak;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lbak;->b()V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v0}, Lbak;->b()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lbak;->b()V

    throw v1
.end method
