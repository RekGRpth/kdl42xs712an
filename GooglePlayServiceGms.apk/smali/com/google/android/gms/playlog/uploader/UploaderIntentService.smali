.class public Lcom/google/android/gms/playlog/uploader/UploaderIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lfkv;->a:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/playlog/uploader/UploaderIntentService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "UploaderIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    invoke-static {}, Lfmf;->a()Lfmf;

    move-result-object v0

    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v1

    invoke-virtual {v1}, Lfmd;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-boolean v1, Lcom/google/android/gms/playlog/uploader/UploaderIntentService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "UploaderIntentService"

    const-string v2, "--> uploading all staged data"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Lfmf;->b()V

    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v0

    invoke-virtual {v0}, Lfmd;->c()V

    invoke-static {}, Lfly;->a()Lfly;

    move-result-object v0

    invoke-virtual {v0}, Lfly;->d()Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lfmd;->a()Lfmd;

    move-result-object v0

    invoke-virtual {v0}, Lfmd;->e()V

    sget-boolean v0, Lcom/google/android/gms/playlog/uploader/UploaderIntentService;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "UploaderIntentService"

    const-string v1, "--> need to wait longer to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
