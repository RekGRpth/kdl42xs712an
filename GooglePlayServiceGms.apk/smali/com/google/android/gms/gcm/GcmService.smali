.class public Lcom/google/android/gms/gcm/GcmService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field static a:I

.field static b:Lorg/apache/http/client/HttpClient;

.field static c:Lcom/google/android/gms/gcm/GcmService;

.field static d:Ljava/util/concurrent/Semaphore;

.field static e:Ljava/lang/String;

.field static p:Ljava/util/LinkedList;

.field static q:Ljava/text/SimpleDateFormat;

.field private static final t:J

.field private static u:I


# instance fields
.field f:Ljava/lang/String;

.field g:Leeu;

.field h:Lefq;

.field public i:Lefs;

.field public j:Leer;

.field k:Lefw;

.field l:Z

.field m:Legb;

.field n:Lcom/google/android/gms/gcm/GcmProvisioning;

.field o:I

.field private r:Landroid/os/Handler;

.field private s:Landroid/os/PowerManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/gcm/GcmService;->a:I

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->d:Ljava/util/concurrent/Semaphore;

    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/gcm/GcmService;->t:J

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    const/16 v0, 0x7d0

    sput v0, Lcom/google/android/gms/gcm/GcmService;->u:I

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->q:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->r:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/GcmService;->l:Z

    return-void
.end method

.method static a(Landroid/content/Context;)Lcom/google/android/gms/gcm/GcmService;
    .locals 2

    sget v0, Lcom/google/android/gms/gcm/GcmService;->u:I

    int-to-long v0, v0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->a(Landroid/content/Context;J)Lcom/google/android/gms/gcm/GcmService;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;J)Lcom/google/android/gms/gcm/GcmService;
    .locals 2

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/gcm/GcmService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :try_start_0
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->d:Ljava/util/concurrent/Semaphore;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method private a()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lefn;

    invoke-direct {v1, p0}, Lefn;-><init>(Lcom/google/android/gms/gcm/GcmService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/gcm/GcmService;->o:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->k:Lefw;

    invoke-virtual {v0}, Lefw;->d()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    :cond_0
    return-void
.end method

.method static a(Ljava/io/PrintWriter;I)V
    .locals 3

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v0, p1, -0x1

    if-nez p1, :cond_0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    return-void

    :cond_0
    move p1, v0

    goto :goto_0

    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 7

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, -0x2

    :goto_0
    sget-object v2, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/gcm/GcmService;->q:Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0xc8

    if-le v0, v2, :cond_1

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "GCM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCM"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    :try_start_1
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, v0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0}, Lefs;->e()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;)Lorg/apache/http/client/HttpClient;
    .locals 3

    const/4 v2, 0x0

    const-string v0, "gcm_http_old"

    invoke-static {p0, v0, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    sget v1, Lcom/google/android/gms/gcm/GcmService;->a:I

    if-ne v0, v1, :cond_1

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->b:Lorg/apache/http/client/HttpClient;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->b:Lorg/apache/http/client/HttpClient;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sput v0, Lcom/google/android/gms/gcm/GcmService;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const-string v0, "GCM"

    const-string v1, "Using fallback http"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lwk;

    const-string v1, "Android-GCM/1.3"

    invoke-direct {v0, p0, v1, v2}, Lwk;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->b:Lorg/apache/http/client/HttpClient;

    goto :goto_0

    :cond_2
    const-string v0, "GCM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GCM"

    const-string v1, "Initializing new http client"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Legs;

    const-string v1, "Android-GCM/1.3"

    invoke-direct {v0, p0, v1}, Legs;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->b:Lorg/apache/http/client/HttpClient;

    const-string v1, "GCM"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCM"

    invoke-virtual {v0, v1}, Legs;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private declared-synchronized b()V
    .locals 7

    const/4 v6, -0x1

    monitor-enter p0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmService;->d(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmService;->b(Landroid/content/Context;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v1, v0}, Leeu;->a(Lorg/apache/http/client/HttpClient;)V

    invoke-static {p0}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lefr;->a(Lorg/apache/http/client/HttpClient;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    new-instance v1, Lbhv;

    invoke-direct {v1, p0}, Lbhv;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v0, "gcm_secure_port"

    const/4 v3, -0x1

    invoke-virtual {v1, v0, v3}, Lbhv;->a(Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    sget-object v4, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Leeu;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->b(Landroid/content/Context;)V

    const-string v3, "0"

    sget-object v4, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->d()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v0}, Leeu;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    if-ne v0, v6, :cond_3

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/GcmProvisioning;->b()I

    move-result v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    sget-object v4, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Leer;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/google/android/gms/gcm/PushMessagingRegistrarProxy;->a(Landroid/content/ContentResolver;)V

    const-string v2, "gcm_enable_registration2"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lbhv;->a(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/google/android/gms/gcm/PushMessagingRegistrarProxy;->a:I

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    const-string v3, "gtalk_rmq_ack_interval"

    const/16 v4, 0xa

    invoke-virtual {v1, v3, v4}, Lbhv;->a(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Leer;->a(I)V

    const-string v2, "gtalk_ssl_handshake_timeout_ms"

    const v3, 0xea60

    invoke-virtual {v1, v2, v3}, Lbhv;->a(Ljava/lang/String;I)I

    move-result v2

    new-instance v3, Landroid/net/SSLSessionCache;

    invoke-direct {v3, p0}, Landroid/net/SSLSessionCache;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v3}, Landroid/net/SSLCertificateSocketFactory;->getDefault(ILandroid/net/SSLSessionCache;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v3, v2}, Leer;->a(Ljavax/net/ssl/SSLSocketFactory;)V

    const-string v2, "gtalk_hostname"

    const-string v3, "mtalk.google.com"

    invoke-virtual {v1, v2, v3}, Lbhv;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    iput-object v1, v2, Leer;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    iput v0, v1, Leer;->d:I

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lefs;->a(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, "0"

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android_id"

    const-string v1, "0"

    invoke-static {p0, v0, v1}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    :cond_0
    const-string v0, "0"

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized d(Landroid/content/Context;)V
    .locals 3

    const-class v1, Lcom/google/android/gms/gcm/GcmService;

    monitor-enter v1

    :try_start_0
    const-string v0, "0"

    sget-object v2, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmService;->c(Landroid/content/Context;)Ljava/lang/String;

    invoke-static {p0}, Leet;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method final a(Ljava/io/PrintWriter;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DeviceID: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/gcm/GcmService;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Status: not provisioned"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    iget v0, v0, Leer;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string v0, "Disabled"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v0}, Leer;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v0}, Leer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    invoke-virtual {v0, p1}, Lefq;->a(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0, p1}, Lefs;->a(Ljava/io/PrintWriter;)V

    :goto_1
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v0}, Leer;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Connecting"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v0}, Leer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0, p1}, Lefs;->a(Ljava/io/PrintWriter;)V

    goto :goto_1

    :cond_3
    const-string v0, "Not connected"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0, p1}, Lefs;->a(Ljava/io/PrintWriter;)V

    goto :goto_1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p2}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/io/PrintWriter;)V

    const/16 v0, 0xc8

    invoke-static {p2, v0}, Lcom/google/android/gms/gcm/GcmService;->a(Ljava/io/PrintWriter;I)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 10

    const/4 v9, -0x1

    const/4 v8, 0x1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "gcm_service_enable"

    invoke-static {p0, v0, v8}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/gcm/GcmService;->o:I

    iget v0, p0, Lcom/google/android/gms/gcm/GcmService;->o:I

    if-ne v0, v9, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Leet;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/GcmProvisioning;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->s:Landroid/os/PowerManager;

    new-instance v0, Lefs;

    invoke-direct {v0, p0}, Lefs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    new-instance v0, Lefw;

    invoke-direct {v0, p0}, Lefw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->k:Lefw;

    new-instance v0, Lefq;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-direct {v0, p0, v1}, Lefq;-><init>(Landroid/content/Context;Lefs;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    invoke-static {}, Leet;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Legb;

    invoke-direct {v0}, Legb;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->m:Legb;

    :cond_1
    new-instance v0, Leeu;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->r:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->m:Legb;

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->k:Lefw;

    invoke-direct {v0, p0, v1, v2, v3}, Leeu;-><init>(Landroid/content/Context;Landroid/os/Handler;Legb;Lefw;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0, v1}, Leeu;->a(Lefs;)V

    new-instance v0, Leer;

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->k:Lefw;

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    iget-object v4, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    iget-object v5, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    iget-object v6, p0, Lcom/google/android/gms/gcm/GcmService;->m:Legb;

    iget-object v7, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Leer;-><init>(Landroid/content/Context;Lefw;Lefq;Lefs;Leeu;Legb;Lcom/google/android/gms/gcm/GcmProvisioning;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v0}, Lefs;->a()V

    sput-object p0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.google.android.intent.action.MCS_HEARTBEAT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.intent.action.GCM_RECONNECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-static {}, Leet;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "android.intent.action.USER_ADDED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_REMOVED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_STOPPED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_STOPPING"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.net.conn.DATA_ACTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_2
    const-string v0, "android.intent.action.DREAMING_STARTED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.DREAMING_STOPPED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const/4 v0, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "plugged"

    invoke-virtual {v0, v2, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    if-eq v0, v8, :cond_3

    const/4 v3, 0x2

    if-ne v0, v3, :cond_4

    :cond_3
    move v0, v8

    :goto_1
    invoke-virtual {v2, v0}, Leeu;->a(Z)V

    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/gcm/GcmService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmService;->b()V

    sget-object v0, Lcom/google/android/gms/gcm/GcmService;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/gcm/GcmService;->c:Lcom/google/android/gms/gcm/GcmService;

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/GcmService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmService;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->k:Lefw;

    invoke-virtual {v0}, Lefw;->a()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->h:Lefq;

    iget-object v0, v0, Lefq;->a:Leeq;

    invoke-virtual {v0}, Leeq;->c()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    iget-object v0, v0, Lefs;->a:Leeq;

    invoke-virtual {v0}, Leeq;->c()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v0}, Leeu;->a()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6

    const/4 v0, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    :goto_0
    const-string v3, "GCM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GcmService start "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "gcm_service_enable"

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/gcm/GcmService;->o:I

    :cond_0
    iget v3, p0, Lcom/google/android/gms/gcm/GcmService;->o:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/GcmProvisioning;->d()V

    invoke-virtual {p0}, Lcom/google/android/gms/gcm/GcmService;->stopSelf()V

    :cond_1
    :goto_1
    return v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const-string v3, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->b(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v3, p0}, Leeu;->a(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v3}, Lefs;->b()V

    invoke-static {p0}, Lefr;->a(Landroid/content/Context;)Lefr;

    move-result-object v3

    invoke-virtual {v3}, Lefr;->a()V

    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmService;->b()V

    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-static {p0}, Lcom/google/android/gms/gcm/GcmProvisioning;->c(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "com.google.android.gcm.intent.SEND"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v2, p1}, Leeu;->a(Landroid/content/Intent;)V

    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->a(I)V

    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->n:Lcom/google/android/gms/gcm/GcmProvisioning;

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/GcmProvisioning;->d()V

    iget-object v1, p0, Lcom/google/android/gms/gcm/GcmService;->j:Leer;

    invoke-virtual {v1}, Leer;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/gcm/GcmService;->a()V

    :cond_6
    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->a(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "GCM"

    const-string v3, "Failed to start GCM"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_7
    :try_start_1
    invoke-static {}, Leet;->d()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    goto :goto_1

    :cond_8
    if-nez p1, :cond_a

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lefs;->a(Z)V

    :cond_9
    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->a(I)V

    move v0, v1

    goto :goto_1

    :cond_a
    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    if-nez v3, :cond_b

    invoke-virtual {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->stopSelf(I)V

    goto :goto_1

    :cond_b
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    :cond_c
    iget-object v3, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    invoke-virtual {v3, p0, p1}, Lefs;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_d
    const-string v3, "com.google.android.gcm.intent.SEND"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->g:Leeu;

    invoke-virtual {v2, p1}, Leeu;->a(Landroid/content/Intent;)V

    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->a(I)V

    move v0, v1

    goto/16 :goto_1

    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/gms/gcm/GcmService;->i:Lefs;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lefs;->a(Z)V

    :cond_f
    invoke-direct {p0, p3}, Lcom/google/android/gms/gcm/GcmService;->a(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v1

    goto/16 :goto_1
.end method
