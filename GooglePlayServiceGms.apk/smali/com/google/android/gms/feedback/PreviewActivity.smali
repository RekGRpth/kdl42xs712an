.class public Lcom/google/android/gms/feedback/PreviewActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private a:Lcrz;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040119    # com.google.android.gms.R.layout.show_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewActivity;->setContentView(I)V

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->h()Lcom/google/android/gms/feedback/FeedbackSession;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/PreviewActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackSession;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    :try_start_0
    new-instance v1, Lcrz;

    invoke-direct {v1, p0, v0}, Lcrz;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    iput-object v1, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcrz;

    const v0, 0x7f0a02df    # com.google.android.gms.R.id.list

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcrz;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PreviewActivity"

    const-string v2, "failed to read in report fields"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcrz;

    invoke-virtual {v0, p3}, Lcrz;->a(I)V

    return-void
.end method
