.class public Lcom/google/android/gms/feedback/SendService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcsr;

.field private c:Z

.field private d:Z

.field private e:Lcsn;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->a:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/feedback/SendService;)Lcsn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->e:Lcsn;

    return-object v0
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcsr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    const-string v0, "GoogleFeedbackSendService"

    const-string v1, "starting report scan"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SendService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "reports"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcsr;

    invoke-direct {v1, p0, v0}, Lcsr;-><init>(Lcom/google/android/gms/feedback/SendService;Ljava/io/File;)V

    iput-object v1, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcsr;

    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcsr;

    invoke-virtual {v0}, Lcsr;->start()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/feedback/SendService;ZZ)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcsr;

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/gms/feedback/FeedbackConnectivityReceiver;->a(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SendService;->stopSelf()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic b(Lcom/google/android/gms/feedback/SendService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcsn;

    invoke-direct {v0, p0}, Lcsn;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->e:Lcsn;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    goto :goto_0
.end method
