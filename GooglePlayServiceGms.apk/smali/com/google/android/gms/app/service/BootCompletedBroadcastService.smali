.class public Lcom/google/android/gms/app/service/BootCompletedBroadcastService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "BootCompletedBroadcastService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "broadcastIntent"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "broadcastIntent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gms/app/settings/GoogleSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v1}, Lbox;->a(Landroid/content/Context;Landroid/content/ComponentName;)V

    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-static {p0, v1}, Lcom/google/android/gms/gcm/ServiceAutoStarter;->a(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/auth/be/recovery/AccountRecoveryBackgroundService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/auth/authzen/GcmReceiverService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lxv;->a()Lxv;

    move-result-object v1

    invoke-virtual {v1}, Lxv;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Ladi;->a()Ladi;

    move-result-object v2

    invoke-virtual {v2, v1}, Ladi;->a(Lxv;)V

    :cond_1
    invoke-static {}, Ladn;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ladn;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Ladn;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ladn;->a()V

    invoke-virtual {v1}, Ladn;->d()V

    invoke-virtual {v1}, Ladn;->b()V

    :cond_2
    invoke-static {}, Ladk;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ladk;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Ladk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ladk;->a()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ladk;->a(Z)V

    invoke-virtual {v1}, Ladk;->b()V

    :cond_3
    :goto_0
    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-static {p0, v1}, Lcom/google/android/gms/icing/service/SystemEventReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/location/internal/GoogleLocationManagerService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0, v0}, Liju;->a(Landroid/content/Context;Landroid/content/Intent;)V

    sget-object v0, Lexl;->e:Lbgm;

    invoke-virtual {v0}, Lbgm;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {p0, v0}, Lcom/google/android/gms/mdm/services/LockscreenMessageService;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/RetryAfterAlarmReceiver;->a(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.lockbox.LockboxAlarmReceiver"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {p0}, Lfkp;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadAlarmReceiver;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v0, Lcom/google/android/gms/security/snet/SnetBootCompletedReceiver;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/google/android/gms/security/snet/SnetBootCompletedReceiver;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/service/BootCompletedBroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_5
    return-void

    :cond_6
    invoke-static {}, Ladk;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ladk;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Ladk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ladk;->a()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ladk;->a(Z)V

    invoke-virtual {v1}, Ladk;->b()V

    goto :goto_0
.end method
