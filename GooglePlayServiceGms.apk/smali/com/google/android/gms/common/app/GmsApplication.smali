.class public final Lcom/google/android/gms/common/app/GmsApplication;
.super Landroid/app/Application;
.source "SourceFile"


# static fields
.field private static a:I

.field private static volatile b:Lcom/google/android/gms/common/app/GmsApplication;


# instance fields
.field private c:Lsf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const-class v1, Lcom/google/android/gms/common/app/GmsApplication;

    monitor-enter v1

    :try_start_0
    sput-object p0, Lcom/google/android/gms/common/app/GmsApplication;->b:Lcom/google/android/gms/common/app/GmsApplication;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "user"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "GMS Core PID not set yet! You must be calling this before onCreate..."

    invoke-static {v0, v3}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    sget v3, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    if-ne v0, v3, :cond_2

    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Current process ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") is not the GMS Core main process ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lbiq;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public static b()Lcom/google/android/gms/common/app/GmsApplication;
    .locals 1

    sget-object v0, Lcom/google/android/gms/common/app/GmsApplication;->b:Lcom/google/android/gms/common/app/GmsApplication;

    return-object v0
.end method


# virtual methods
.method protected final attachBaseContext(Landroid/content/Context;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, La;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    const-wide/16 v1, 0x5

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final declared-synchronized c()Lsf;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lsf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lsf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lst;

    new-instance v1, Lbmo;

    sget-object v2, Lbfx;->a:Lbfy;

    invoke-static {v2}, Lbhv;->a(Lbfy;)Z

    move-result v2

    invoke-direct {v1, p0, v2}, Lbmo;-><init>(Landroid/content/Context;Z)V

    invoke-direct {v0, v1}, Lst;-><init>(Ltc;)V

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "volley"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Lsf;

    new-instance v3, Lsw;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Lsw;-><init>(Ljava/io/File;B)V

    const/4 v1, 0x0

    invoke-direct {v2, v3, v0, v1}, Lsf;-><init>(Lro;Lrw;B)V

    iput-object v2, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lsf;

    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lsf;

    invoke-virtual {v0}, Lsf;->a()V

    iget-object v0, p0, Lcom/google/android/gms/common/app/GmsApplication;->c:Lsf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {p0}, Lhhw;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lbfy;->a(Landroid/content/Context;)V

    sput-object p0, Lbgh;->a:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.INITIALIZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/app/GmsApplication;->sendBroadcast(Landroid/content/Intent;)V

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/common/app/GmsApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->processName:Ljava/lang/String;

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/app/GmsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    sput v0, Lcom/google/android/gms/common/app/GmsApplication;->a:I

    :cond_1
    return-void
.end method
