.class public Lcom/google/android/gms/common/people/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

.field private final c:Landroid/widget/ImageView;

.field private final d:Ljava/util/LinkedHashMap;

.field private final e:Lftz;

.field private final f:Lfaj;

.field private g:Z

.field private final h:Ljava/util/HashMap;

.field private final i:Lbkt;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILftz;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILftz;)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    new-instance v0, Lbkt;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lbkt;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lbkt;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f04003c    # com.google.android.gms.R.layout.common_audience_view

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00cb    # com.google.android.gms.R.id.audience_view_list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    const v0, 0x7f0a00ca    # com.google.android.gms.R.id.audience_view_editable

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->c:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/google/android/gms/common/people/views/AudienceView;->e:Lftz;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->e:Lftz;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v3, p0

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lfaj;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->setSaveEnabled(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/common/people/views/AudienceView;)Lbkt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lbkt;

    return-object v0
.end method

.method public static synthetic a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/common/people/views/AudienceView;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lblr;)V
    .locals 1

    iget v0, p3, Lblr;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p3, Lblr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x66

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    iget v0, p3, Lblr;->c:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 3

    const/4 v2, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lbkt;

    invoke-virtual {v0, p1}, Lbkt;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-static {v0, p2}, Lcom/google/android/gms/common/people/views/AudienceView;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lfaj;

    new-instance v1, Lblp;

    invoke-direct {v1, p0, p1, p2}, Lblp;-><init>(Lcom/google/android/gms/common/people/views/AudienceView;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1, p1, v2, v2}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private static b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/common/people/views/AudienceView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    return v0
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->f()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    return-void
.end method

.method private f()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    const v2, 0x7f04003e    # com.google.android.gms.R.layout.common_audience_view_chip_empty

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(Lbbo;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 7

    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AudienceMember can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f04003d    # com.google.android.gms.R.layout.common_audience_view_chip

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v1, 0x7f0a00ce    # com.google.android.gms.R.id.chip_background

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v1, 0x7f0a00d0    # com.google.android.gms.R.id.chip_text

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00cf    # com.google.android.gms.R.id.chip_icon

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v6, 0x7f0a00d1    # com.google.android.gms.R.id.chip_close

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v0}, Lblq;->a(Landroid/content/Context;Lcom/google/android/gms/common/people/data/AudienceMember;)Lblr;

    move-result-object v6

    invoke-static {v5, v1, v2, v6}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lblr;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    goto/16 :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->f()V

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    :cond_6
    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public final c()V
    .locals 9

    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->d()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003d    # com.google.android.gms.R.layout.common_audience_view_chip

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a00ce    # com.google.android.gms.R.id.chip_background

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0a00d0    # com.google.android.gms.R.id.chip_text

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a00cf    # com.google.android.gms.R.id.chip_icon

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v4, 0x7f0a00d1    # com.google.android.gms.R.id.chip_close

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lblr;

    const v6, 0x7f020083    # com.google.android.gms.R.drawable.common_acl_chip_blue

    const v7, 0x7f0b0460    # com.google.android.gms.R.string.common_chips_label_only_you

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v7, 0x7f02008f    # com.google.android.gms.R.drawable.common_ic_acl_only_you

    const/4 v8, 0x0

    invoke-direct {v5, v6, v4, v7, v8}, Lblr;-><init>(ILjava/lang/String;IB)V

    invoke-static {v3, v0, v1, v5}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lblr;)V

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const/4 v1, 0x0

    check-cast p1, Landroid/os/Bundle;

    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "showOnlyYou"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    const-string v0, "showEmptyText"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->c()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->d()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "showOnlyYou"

    iget-boolean v2, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "showEmptyText"

    iget-boolean v2, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "audience"

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public setClickable(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    return-void
.end method
