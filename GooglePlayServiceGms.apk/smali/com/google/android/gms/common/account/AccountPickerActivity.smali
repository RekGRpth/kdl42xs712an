.class public Lcom/google/android/gms/common/account/AccountPickerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private a:Ljava/util/Set;

.field private b:Ljava/util/Set;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/util/ArrayList;

.field private i:I

.field private j:[Landroid/os/Parcelable;

.field private k:I

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    iput v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/common/account/AccountPickerActivity;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    return p1
.end method

.method private a(Ljava/lang/String;)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/common/account/AccountPickerActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 7

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    iget-object v6, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iput-object v2, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)Ljava/util/Set;
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x0

    const-string v2, "allowableAccountTypes"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    new-instance v0, Ljava/util/HashSet;

    array-length v2, v3

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    array-length v4, v2

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(I)V

    array-length v4, v2

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v2, v1

    iget-object v5, v5, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v0, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1, v0}, Lbjt;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "accountType"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    return-void
.end method

.method private b()V
    .locals 5

    const/4 v4, 0x1

    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.startChooseAccountTypeActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "allowableAccountTypes"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "allowableAccountTypes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "addAccountOptions"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "addAccountOptions"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "addAccountRequiredFeatures"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "addAccountRequiredFeatures"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "authTokenType"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "authTokenType"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/common/account/AccountPickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iput v4, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    const/4 v5, 0x0

    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AccountChooser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "runAddAccountForAuthenticator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addAccountOptions"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addAccountRequiredFeatures"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authTokenType"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    move-object v1, p1

    move-object v6, p0

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    if-nez p2, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4

    const/4 v0, 0x1

    if-ne p1, v0, :cond_6

    if-eqz p3, :cond_2

    const-string v0, "accountType"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: unable to find account type, pretending the request was canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: unable to find added account, pretending the request was canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v0, "AccountChooser"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    :cond_6
    if-ne p1, v9, :cond_3

    if-eqz p3, :cond_c

    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "accountType"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-eqz v2, :cond_7

    if-nez v1, :cond_b

    :cond_7
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v5

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    array-length v8, v7

    move v4, v3

    :goto_2
    if-ge v4, v8, :cond_8

    aget-object v0, v7, v4

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_8
    array-length v4, v5

    move v0, v3

    :goto_3
    if-ge v0, v4, :cond_b

    aget-object v7, v5, v0

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    iget-object v1, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    :goto_4
    if-nez v1, :cond_9

    if-eqz v0, :cond_3

    :cond_9
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_b
    move-object v0, v1

    move-object v1, v2

    goto :goto_4

    :cond_c
    move-object v2, v1

    goto :goto_1
.end method

.method public onCancelButtonClicked(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "overrideTheme"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "overrideTheme"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f10017e    # com.google.android.gms.R.style.DialogThemeOnV11

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setTheme(I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz p1, :cond_2

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    const-string v0, "existingAccounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    const-string v0, "selectedAccountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    const-string v0, "selectedAddAccount"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    :cond_1
    :goto_1
    const-string v0, "allowableAccounts"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_8

    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/HashSet;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :pswitch_0
    const v0, 0x7f10017f    # com.google.android.gms.R.style.LightDialogThemeOnV11

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setTheme(I)V

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    const-string v0, "selectedAccount"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    if-eqz v0, :cond_1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v0, v1

    :goto_3
    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    invoke-direct {p0, v4}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    const-string v0, "alwaysPromptForAccount"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    const-string v0, "descriptionTextOverride"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    const-string v0, "setGmsCoreAccount"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lbox;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_6

    const-string v4, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    const-string v0, "AccountChooser"

    const-string v1, "The calling package does not have the android.permission.GET_ACCOUNTS permission. Will display Chooser."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    :goto_4
    if-nez v0, :cond_7

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    const-string v0, "AccountChooser"

    const-string v1, "Could not get calling package."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_4

    :cond_7
    move v0, v3

    goto :goto_5

    :cond_8
    move-object v0, v1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onOkButtonClicked(Landroid/view/View;)V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    const/16 v8, 0x8

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lbov;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0b0474    # com.google.android.gms.R.string.common_restricted_no_accounts

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "AccountChooser"

    const-string v1, "User doesn\'t have the ability to add an Account. Nothing to choose."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {p0}, Lbov;->d(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v6, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    new-array v7, v0, [Ljava/lang/String;

    move v3, v2

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v7, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    if-eqz v6, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b045b    # com.google.android.gms.R.string.common_add_account_button_label

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    :cond_7
    :goto_3
    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    const v0, 0x7f040038    # com.google.android.gms.R.layout.common_account_picker

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "overrideCustomTheme"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "overrideCustomTheme"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_8
    :goto_4
    iget-object v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v5, 0x109000f    # android.R.layout.simple_list_item_single_choice

    invoke-direct {v3, p0, v5, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v3, Lbbz;

    invoke-direct {v3, p0}, Lbbz;-><init>(Lcom/google/android/gms/common/account/AccountPickerActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    if-eq v3, v4, :cond_9

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    const-string v0, "AccountChooser"

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "AccountChooser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "List item "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " should be selected"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const v0, 0x102001a    # android.R.id.button2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    if-eq v3, v4, :cond_e

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_a
    move v3, v2

    :goto_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_b
    move v3, v4

    goto/16 :goto_3

    :pswitch_0
    const-string v0, "android:id/title"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_c

    const v3, 0x7f020077    # com.google.android.gms.R.drawable.bg_play_games_banner_choose_account

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0d01e5    # com.google.android.gms.R.dimen.common_account_picker_header_padding

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_c
    const-string v0, "android:id/titleDivider"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_d
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_e
    move v1, v2

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "pendingRequest"

    iget v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "existingAccounts"

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    const-string v0, "selectedAddAccount"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "selectedAddAccount"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "selectedAccountName"

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/account/AccountPickerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    :catch_1
    move-exception v0

    :cond_0
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "errorMessage"

    const-string v2, "error communicating with server"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method
