.class public final Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;
.super Lbeo;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/concurrent/ConcurrentLinkedQueue;


# instance fields
.field private b:Lbhw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lbeo;-><init>(I)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;ILandroid/os/ResultReceiver;)V
    .locals 1

    new-instance v0, Lbie;

    invoke-direct {v0, p1, p3, p2}, Lbie;-><init>(Landroid/net/Uri;Landroid/os/ResultReceiver;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Lbid;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V
    .locals 2

    new-instance v0, Lbif;

    const/4 v1, 0x3

    invoke-direct {v0, p1, p2, v1}, Lbif;-><init>(Landroid/net/Uri;Ljava/util/ArrayList;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Lbid;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Lbid;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->c:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbid;

    if-nez v0, :cond_0

    const-string v0, "ImageMultiThreadedInten"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->b:Lbhw;

    invoke-interface {v0, p0, v1}, Lbid;->a(Landroid/content/Context;Lbhw;)V

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 1

    invoke-super {p0}, Lbeo;->onCreate()V

    invoke-static {p0}, Lbhw;->a(Landroid/content/Context;)Lbhw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->b:Lbhw;

    return-void
.end method
