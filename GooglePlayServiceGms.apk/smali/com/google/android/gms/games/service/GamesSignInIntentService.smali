.class public final Lcom/google/android/gms/games/service/GamesSignInIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "SignInIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V
    .locals 1

    new-instance v0, Ldre;

    invoke-direct {v0, p1, p2}, Ldre;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ldrg;

    invoke-direct {v0, p1, p2, p3}, Ldrg;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ldrf;

    invoke-direct {v0, p1, p2, p3, p4}, Ldrf;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ldrk;

    invoke-direct {v0, p1, p2}, Ldrk;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ldri;)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    sget-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.games.signin.service.INTENT"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V
    .locals 2

    new-instance v0, Ldrh;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1, p2}, Ldrh;-><init>(Lcom/google/android/gms/common/server/ClientContext;ZLdaj;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ldrj;

    invoke-direct {v0, p1, p2, p3}, Ldrj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ldrl;

    invoke-direct {v0, p1, p2, p3, p4}, Ldrl;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Ldri;)V

    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldri;

    if-nez v0, :cond_0

    const-string v0, "SignInIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Ldac;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcun;->a(Landroid/content/Context;)Lcun;

    move-result-object v1

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-interface {v0, p0, v1}, Ldri;->a(Landroid/content/Context;Lcun;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcun;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcun;->a()V

    throw v0
.end method
