.class public final Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Ldxc;
.implements Ldzo;
.implements Ledj;
.implements Ledo;
.implements Ledp;


# static fields
.field private static final v:Landroid/os/Bundle;

.field private static final w:Landroid/os/Bundle;

.field private static final x:[Ledi;


# instance fields
.field private A:Ledf;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:I

.field private E:Ledn;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/widget/ProgressBar;

.field private I:Z

.field private y:Landroid/support/v4/view/ViewPager;

.field private z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->v:Landroid/os/Bundle;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->w:Landroid/os/Bundle;

    sget-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->v:Landroid/os/Bundle;

    const-string v1, "leaderboard_collection_arg"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->v:Landroid/os/Bundle;

    const-string v1, "leaderboard_tab_index"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->w:Landroid/os/Bundle;

    const-string v1, "leaderboard_collection_arg"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->w:Landroid/os/Bundle;

    const-string v1, "leaderboard_tab_index"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v0, 0x2

    new-array v0, v0, [Ledi;

    new-instance v1, Ledi;

    const-class v2, Ldzu;

    const v3, 0x7f0b0279    # com.google.android.gms.R.string.games_leaderboard_social_tab

    sget-object v4, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->w:Landroid/os/Bundle;

    invoke-direct {v1, v2, v3, v4}, Ledi;-><init>(Ljava/lang/Class;ILandroid/os/Bundle;)V

    aput-object v1, v0, v5

    new-instance v1, Ledi;

    const-class v2, Ldzu;

    const v3, 0x7f0b0278    # com.google.android.gms.R.string.games_leaderboard_public_tab

    sget-object v4, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->v:Landroid/os/Bundle;

    invoke-direct {v1, v2, v3, v4}, Ledi;-><init>(Ljava/lang/Class;ILandroid/os/Bundle;)V

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->x:[Ledi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldxm;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    return-void
.end method

.method private a(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    return-void
.end method

.method private f(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    invoke-virtual {v0}, Ledf;->d()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzu;

    invoke-virtual {v0, p1}, Ldzu;->d(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    iget v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    if-ne p1, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iput p1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->F:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->G:Landroid/widget/TextView;

    invoke-static {p0, v0, v1, v2, v3}, Ledm;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/widget/TextView;Landroid/widget/TextView;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    invoke-virtual {v0}, Ledf;->d()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzu;

    iget v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    invoke-virtual {v0}, Ldzu;->P()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ldvp;)V
    .locals 2

    new-instance v0, Ldxp;

    invoke-direct {v0, p0, p1}, Ldxp;-><init>(Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;Ldvp;)V

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "This method should only be called from the main thread"

    invoke-static {v1}, Lbiq;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldxm;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-interface {v0, v1}, Ldxo;->a(Lcom/google/android/gms/games/Game;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->I:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ClientLeaderboardScores"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setLeaderboardName: displayName \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' doesn\'t match previous value \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    iget-object v1, p0, Ljp;->n:Ljq;

    invoke-virtual {v1}, Ljq;->b()Ljj;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "ClientLeaderboardScores"

    const-string v1, "enableActionBarSpinner: couldn\'t get actionBar, disabling spinner"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->F:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->G:Landroid/widget/TextView;

    invoke-static {p0, v0, v1, v2, v3}, Ledm;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/widget/TextView;Landroid/widget/TextView;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v0}, Ljj;->e(I)V

    new-instance v2, Ledr;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->E:Ledn;

    invoke-direct {v2, p0, v3}, Ledr;-><init>(Ledp;Ledn;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->E:Ledn;

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->E:Ledn;

    invoke-virtual {v1, v3, v2}, Ljj;->a(Landroid/widget/SpinnerAdapter;Ljm;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid timeSpan: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_2
    :pswitch_1
    invoke-virtual {v1, v0}, Ljj;->c(I)V

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final e(I)F
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090012    # com.google.android.gms.R.integer.games_leaderboard_score_view_pager_page_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->a(IZ)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04005c    # com.google.android.gms.R.layout.games_client_leaderboard_scores_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->setContentView(I)V

    const v0, 0x7f0a0138    # com.google.android.gms.R.id.pager

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    new-instance v0, Ledf;

    iget-object v1, p0, Lo;->b:Lw;

    sget-object v2, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->x:[Ledi;

    invoke-direct {v0, p0, v1, v2, p0}, Ledf;-><init>(Landroid/content/Context;Lu;[Ledi;Ledj;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lfe;)V

    const v0, 0x7f0a0136    # com.google.android.gms.R.id.play_tab_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00be    # com.google.android.gms.R.color.play_tab_selected_line_client

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->c(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(Landroid/support/v4/view/ViewPager;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    new-instance v1, Ledg;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->y:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->z:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-direct {v1, v2, v3, v4}, Ledg;-><init>(Landroid/support/v4/view/ViewPager;Ledf;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Lgl;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->B:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ClientLeaderboardScores"

    const-string v1, "EXTRA_LEADERBOARD_ID extra missing; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->finish()V

    :cond_0
    invoke-static {p0}, Ledn;->a(Landroid/content/Context;)Landroid/view/ViewGroup;

    move-result-object v1

    const v0, 0x7f0a002e    # com.google.android.gms.R.id.actionbar_compat_title

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->F:Landroid/widget/TextView;

    const v0, 0x7f0a002f    # com.google.android.gms.R.id.actionbar_compat_subtitle

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->G:Landroid/widget/TextView;

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0}, Ljj;->d()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ledn;

    invoke-direct {v2, v0, v1}, Ledn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->E:Ledn;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->C:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->F:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->G:Landroid/widget/TextView;

    invoke-static {p0, v0, v1, v2, v3}, Ledm;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/widget/TextView;Landroid/widget/TextView;)V

    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->s:Z

    const-string v0, "games.leaderboard_pref"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, v5, v5}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->a(IZ)V

    const-string v1, "time_span"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v2, 0x7f120003    # com.google.android.gms.R.menu.games_client_leaderboard_scores_menu

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0a037f    # com.google.android.gms.R.id.menu_leaderboard_reset_top_scores

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v0, 0x7f0a0380    # com.google.android.gms.R.id.menu_leaderboard_reset_player_centered_scores

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0a037e    # com.google.android.gms.R.id.menu_progress_bar

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-static {v2}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04001a    # com.google.android.gms.R.layout.actionbar_indeterminate_progress

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0a007a    # com.google.android.gms.R.id.progress_bar

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->I:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-static {v2, v3}, Leu;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Ldxm;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0, p1}, Ldxm;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->A:Ledf;

    invoke-virtual {v0}, Ledf;->d()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzu;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->b()V

    invoke-virtual {v0}, Ldzu;->H_()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->f(I)V

    move v0, v2

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->f(I)V

    move v0, v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a0032 -> :sswitch_0    # com.google.android.gms.R.id.menu_refresh
        0x7f0a037f -> :sswitch_1    # com.google.android.gms.R.id.menu_leaderboard_reset_top_scores
        0x7f0a0380 -> :sswitch_2    # com.google.android.gms.R.id.menu_leaderboard_reset_player_centered_scores
    .end sparse-switch
.end method

.method public final onPause()V
    .locals 3

    invoke-super {p0}, Ldxm;->onPause()V

    const-string v0, "games.leaderboard_pref"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "time_span"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public final onResume()V
    .locals 0

    invoke-super {p0}, Ldxm;->onResume()V

    return-void
.end method

.method protected final p()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method public final v()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->D:I

    return v0
.end method

.method public final x_()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->I:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/leaderboards/ClientLeaderboardScoreListActivity;->H:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method
