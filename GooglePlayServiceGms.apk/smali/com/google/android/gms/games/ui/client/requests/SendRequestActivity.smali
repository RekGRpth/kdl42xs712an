.class public final Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Ldyw;
.implements Ldzc;


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:I

.field private C:[B

.field private D:I

.field private v:Ljava/util/HashMap;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/ImageView;

.field private z:Lcom/google/android/gms/common/images/internal/LoadingImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final synthetic a(Lbek;)V
    .locals 3

    check-cast p1, Ldli;

    invoke-interface {p1}, Ldli;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogSendRequest"

    invoke-static {p0, v1}, Ledd;->a(Lo;Ljava/lang/String;)V

    invoke-static {v0}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f0b0249    # com.google.android.gms.R.string.games_network_error_send_wish

    :goto_0
    invoke-static {v0}, Lebn;->c(I)Lebn;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ui.dialog.networkErrorDialog"

    invoke-static {p0, v0, v1}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    :goto_1
    return-void

    :pswitch_1
    const v0, 0x7f0b024a    # com.google.android.gms.R.string.games_network_error_send_gift

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/util/HashMap;I)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:Landroid/widget/ImageView;

    const v1, 0x7f02016c    # com.google.android.gms.R.drawable.ic_send_holo_dark

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:Landroid/widget/ImageView;

    const v1, 0x7f02016b    # com.google.android.gms.R.drawable.ic_send_disabled_holo_dark

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Ldxm;->l(Landroid/os/Bundle;)V

    sget-object v0, Lcte;->o:Ldlf;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->j()Lbdu;

    move-result-object v1

    invoke-interface {v0, v1}, Ldlf;->b(Lbdu;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->C:[B

    array-length v1, v1

    if-le v1, v0, :cond_0

    const-string v1, "SendRequestActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Payload size cannot be greater than "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "SendRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SendRequestActivity"

    const-string v1, "onSend: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->v:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid request type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const v0, 0x7f0b0247    # com.google.android.gms.R.string.games_progress_dialog_send_wish

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.ui.dialog.progressDialogSendRequest"

    invoke-static {p0, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    sget-object v0, Lcte;->o:Ldlf;

    iget-object v2, p0, Ldxm;->r:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->C:[B

    iget v6, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->D:I

    invoke-interface/range {v0 .. v6}, Ldlf;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;I[BI)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0248    # com.google.android.gms.R.string.games_progress_dialog_send_gift

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f0a01a2
        :pswitch_0    # com.google.android.gms.R.id.item_container
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->s()V

    const v0, 0x7f040080    # com.google.android.gms.R.layout.games_send_request_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setContentView(I)V

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->s:Z

    const v0, 0x7f0a01a3    # com.google.android.gms.R.id.item_name

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:Landroid/widget/TextView;

    const v0, 0x7f0a01a2    # com.google.android.gms.R.id.item_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->x:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a01a4    # com.google.android.gms.R.id.send_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->y:Landroid/widget/ImageView;

    const v0, 0x7f0a01a6    # com.google.android.gms.R.id.item_icon

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "com.google.android.gms.games.REQUEST_TYPE"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "SendRequestActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->B:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    const-string v0, "SendRequestActivity"

    const-string v1, "Error parsing intent; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->finish()V

    :cond_0
    return-void

    :pswitch_0
    const v0, 0x7f0b0245    # com.google.android.gms.R.string.games_send_request_title_wish

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setTitle(I)V

    :goto_1
    const-string v0, "com.google.android.gms.games.PAYLOAD"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->C:[B

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->C:[B

    if-nez v0, :cond_1

    const-string v0, "SendRequestActivity"

    const-string v2, "Payload cannot be null!"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b0246    # com.google.android.gms.R.string.games_send_request_title_gift

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->setTitle(I)V

    goto :goto_1

    :cond_1
    const-string v0, "com.google.android.gms.games.REQUEST_LIFETIME"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->D:I

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->D:I

    if-eq v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->D:I

    if-gtz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.gms.games.REQUEST_ITEM_NAME"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v0, "SendRequestActivity"

    const-string v2, "The item name cannot be empty!"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "com.google.android.gms.games.REQUEST_ITEM_ICON"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    const-string v0, "SendRequestActivity"

    const-string v2, "The item icon cannot be null!"

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->z:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const-string v0, "players"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->A:Ljava/util/ArrayList;

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onSearchRequested()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->t()Z

    move-result v0

    return v0
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0xb

    return v0
.end method

.method public final u()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final w()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0072    # com.google.android.gms.R.dimen.games_send_request_item_icon_background_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0d0074    # com.google.android.gms.R.dimen.games_send_request_item_icon_background_bottom_padding

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public final x()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final y()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final z()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/SendRequestActivity;->A:Ljava/util/ArrayList;

    return-object v0
.end method
