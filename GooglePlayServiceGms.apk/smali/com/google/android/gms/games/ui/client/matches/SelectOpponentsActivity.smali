.class public final Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ldyw;
.implements Ldzc;


# instance fields
.field private A:Z

.field private B:Ljava/util/ArrayList;

.field private C:Landroid/view/View;

.field private D:Landroid/view/animation/Animation;

.field private E:Landroid/view/animation/Animation;

.field private F:Ljava/util/HashMap;

.field private G:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method

.method private B()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->F:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->G:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    if-le v0, v1, :cond_0

    const-string v1, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "hasMinPlayers: numSelected too large ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Landroid/view/animation/Animation;
    .locals 2

    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-object v0
.end method


# virtual methods
.method public final A()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Ljava/util/HashMap;I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->F:Ljava/util/HashMap;

    iput p2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->G:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    if-eqz v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    if-nez v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    return v0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->D:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->E:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "SelectOpponentsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SelectOpponentsActivity"

    const-string v1, "onPlay: \'Play\' action shouldn\'t have been enabled; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->F:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string v0, "players"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v0, "min_automatch_players"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->G:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "max_automatch_players"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->G:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0196
        :pswitch_0    # com.google.android.gms.R.id.select_players_play_button_container
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x7

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->s()V

    const v2, 0x7f04007e    # com.google.android.gms.R.layout.games_select_opponents_activity

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->setContentView(I)V

    const v2, 0x7f0b021e    # com.google.android.gms.R.string.games_select_opponents_title

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->setTitle(I)V

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->s:Z

    const v2, 0x7f0a018c    # com.google.android.gms.R.id.button_bar_container

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    const v2, 0x7f0a0196    # com.google.android.gms.R.id.select_players_play_button_container

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f05000a    # com.google.android.gms.R.anim.games_play_button_slidein

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->a(I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->D:Landroid/view/animation/Animation;

    const v2, 0x7f05000b    # com.google.android.gms.R.anim.games_play_button_slideout

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->a(I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->E:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->C:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->z:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    const-string v3, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    const-string v3, "com.google.android.gms.games.SHOW_AUTOMATCH"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Z

    const-string v3, "com.google.android.gms.games.MULTIPLAYER_TYPE"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:I

    const-string v3, "players"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:Ljava/util/ArrayList;

    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:I

    if-ne v2, v4, :cond_0

    const-string v2, "SelectOpponentsActivity"

    const-string v3, "parseIntent: missing intent extra \'EXTRA_MULTIPLAYER_TYPE\'... defaulting type to to RTMP"

    invoke-static {v2, v3}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:I

    :cond_0
    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    if-gtz v2, :cond_2

    const-string v1, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "minPlayers was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", but must be greater than or equal to 1."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "SelectOpponentsActivity"

    const-string v1, "Error parsing intent; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->finish()V

    :cond_1
    return-void

    :cond_2
    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    if-le v2, v5, :cond_3

    const-string v2, "SelectOpponentsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "maxPlayers was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", but is currently limited to 7 (for 8"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " players total, counting the current player)."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput v5, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    if-ge v2, v3, :cond_4

    const-string v1, "SelectOpponentsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Max must be greater than or equal to min. Max: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Min: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->v:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final onSearchRequested()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->t()Z

    move-result v0

    return v0
.end method

.method protected final p()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public final u()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->w:I

    return v0
.end method

.method public final v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->y:Z

    return v0
.end method

.method public final w()I
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->A:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0067    # com.google.android.gms.R.dimen.games_button_bar_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final y()Z
    .locals 1

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->x:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/SelectOpponentsActivity;->B:Ljava/util/ArrayList;

    return-object v0
.end method
