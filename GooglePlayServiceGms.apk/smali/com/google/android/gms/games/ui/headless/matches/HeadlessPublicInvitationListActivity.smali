.class public final Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;
.super Ldwr;
.source "SourceFile"

# interfaces
.implements Ldzy;
.implements Leam;
.implements Lebs;
.implements Lebv;


# instance fields
.field private q:Lecc;

.field private r:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const v0, 0x7f04007a    # com.google.android.gms.R.layout.games_public_invitation_list_activity

    const v1, 0x7f12000e    # com.google.android.gms.R.menu.games_headless_public_list_menu

    invoke-direct {p0, v0, v1}, Ldwr;-><init>(II)V

    return-void
.end method


# virtual methods
.method public final D_()Ldzx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->q:Lecc;

    return-object v0
.end method

.method public final a()Lebr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->q:Lecc;

    return-object v0
.end method

.method public final d()Lebu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->q:Lecc;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldwr;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->r:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->s:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->t:Ljava/lang/String;

    new-instance v0, Lecc;

    invoke-direct {v0, p0}, Lecc;-><init>(Ldwr;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->q:Lecc;

    const v0, 0x7f0b02bb    # com.google.android.gms.R.string.games_match_inbox_header_invitations

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->r:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final u()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->r:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationListActivity;->t:Ljava/lang/String;

    return-object v0
.end method
