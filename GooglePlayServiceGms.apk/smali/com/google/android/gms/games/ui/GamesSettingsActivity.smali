.class public final Lcom/google/android/gms/games/ui/GamesSettingsActivity;
.super Ldvn;
.source "SourceFile"

# interfaces
.implements Ljm;


# instance fields
.field private p:Ljava/lang/String;

.field private q:[Ljava/lang/String;

.field private r:Z

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Ljava/util/ArrayList;

.field private v:Z

.field private final w:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ldvn;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->r:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->s:Z

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w:Landroid/os/Bundle;

    return-void
.end method

.method private q()V
    .locals 4

    const v3, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->s:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1, v3}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v2, v0, Ldvv;

    if-eqz v2, :cond_1

    check-cast v0, Ldvv;

    invoke-virtual {v0}, Ldvv;->P()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-static {v2, v0}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "GamesSettings"

    const-string v1, "Not adding duplicate fragment"

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->t:Ljava/lang/String;

    invoke-static {v0, v2}, Ldvv;->a(Ljava/lang/String;Ljava/lang/String;)Ldvv;

    move-result-object v0

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v3, v0}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Ldvv;

    if-eqz v1, :cond_0

    check-cast v0, Ldvv;

    invoke-virtual {v0, p1}, Ldvv;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->u:Ljava/util/ArrayList;

    return-void
.end method

.method public final a(ZLandroid/os/Bundle;)V
    .locals 4

    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->v:Z

    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w:Landroid/os/Bundle;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->i()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->a()V

    goto :goto_0
.end method

.method protected final f()Lbdu;
    .locals 3

    iget-object v0, p0, Lo;->b:Lw;

    new-instance v1, Ldvr;

    invoke-direct {v1}, Ldvr;-><init>()V

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v2, 0x7f0a01a7    # com.google.android.gms.R.id.fragment_container

    invoke-virtual {v0, v2, v1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->b()V

    :cond_0
    invoke-static {}, Lcti;->a()Lctj;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lctj;->a:Z

    invoke-virtual {v0}, Lctj;->a()Lcti;

    move-result-object v0

    new-instance v1, Lbdw;

    invoke-direct {v1, p0, p0, p0}, Lbdw;-><init>(Landroid/content/Context;Lbdx;Lbdy;)V

    sget-object v2, Lcte;->e:Lbdm;

    invoke-virtual {v1, v2, v0}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    iput-object v1, v0, Lbdw;->a:Ljava/lang/String;

    const-string v1, "com.google.android.gms"

    iput-object v1, v0, Lbdw;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    return-object v0
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q()V

    return-void
.end method

.method public final l()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final m()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final n()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->v:Z

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.DEST_APP_VERSION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->t:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->r:Z

    const v0, 0x7f100074    # com.google.android.gms.R.style.Games_GamesSettingsLightTitleTheme

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->setTheme(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    if-nez v4, :cond_3

    move v1, v2

    :goto_1
    new-array v5, v1, [Ljava/lang/String;

    move v3, v2

    :goto_2
    if-ge v3, v1, :cond_4

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v5, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    if-eqz p1, :cond_2

    const-string v0, "selected_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "settingsDefaultAccount"

    const-string v1, "gcore.sharedPrefs"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_4
    iput-object v5, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_6

    :cond_5
    const-string v0, "GamesSettings"

    const-string v1, "No accounts found when creating settings activity. Bailing out."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->finish()V

    :goto_3
    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    :cond_8
    invoke-super {p0, p1}, Ldvn;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040082    # com.google.android.gms.R.layout.games_settings_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->setContentView(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljj;->a(Z)V

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->r:Z

    if-eqz v1, :cond_9

    const v1, 0x7f020135    # com.google.android.gms.R.drawable.ic_launcher_play_games

    invoke-virtual {v0, v1}, Ljj;->b(I)V

    new-instance v1, Ldvq;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q:[Ljava/lang/String;

    invoke-direct {v1, p0, v3}, Ldvq;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    const/16 v3, 0x8

    invoke-virtual {v0, v2, v3}, Ljj;->a(II)V

    invoke-virtual {v0, v6}, Ljj;->e(I)V

    invoke-virtual {v0, v1, p0}, Ljj;->a(Landroid/widget/SpinnerAdapter;Ljm;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ldvq;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljj;->c(I)V

    goto :goto_3

    :cond_9
    const v1, 0x7f0200e6    # com.google.android.gms.R.drawable.ic_ab_play_games

    invoke-virtual {v0, v1}, Ljj;->b(I)V

    const v1, 0x7f0b0214    # com.google.android.gms.R.string.games_settings_title

    invoke-virtual {v0, v1}, Ljj;->d(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005c    # com.google.android.gms.R.drawable.actionbar_dest_bg

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->b(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200e5    # com.google.android.gms.R.drawable.ic_ab_back_holo_dark

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->c(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Ldvn;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0    # android.R.id.home
    .end packed-switch
.end method

.method public final onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->s:Z

    invoke-super {p0}, Ldvn;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Ldvn;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->s:Z

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->q()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldvn;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_account"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onStop()V
    .locals 4

    invoke-super {p0}, Ldvn;->onStop()V

    const-string v0, "settingsDefaultAccount"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->p:Ljava/lang/String;

    const-string v2, "gcore.sharedPrefs"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lqy;->a(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method

.method public final p()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w:Landroid/os/Bundle;

    return-object v0
.end method
