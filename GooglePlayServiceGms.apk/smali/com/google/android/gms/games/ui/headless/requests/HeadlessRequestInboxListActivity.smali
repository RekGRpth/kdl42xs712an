.class public final Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;
.super Ldwr;
.source "SourceFile"

# interfaces
.implements Ldwt;
.implements Ldxc;
.implements Lebe;
.implements Lecb;


# instance fields
.field private q:I

.field private r:Lecj;

.field private s:Landroid/widget/ProgressBar;

.field private t:Z

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const v0, 0x7f040065    # com.google.android.gms.R.layout.games_inbox_list_activity

    const v1, 0x7f12000f    # com.google.android.gms.R.menu.games_inbox_menu

    invoke-direct {p0, v0, v1}, Ldwr;-><init>(II)V

    return-void
.end method

.method private b(Z)V
    .locals 5

    const v4, 0x7f0a0152    # com.google.android.gms.R.id.container

    const/4 v3, 0x1

    iget-object v1, p0, Lo;->b:Lw;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->u:Ljava/lang/String;

    invoke-static {v0}, Leck;->a(Ljava/lang/String;)Leck;

    move-result-object v0

    :goto_0
    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v1}, Lag;->c()I

    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->u:Ljava/lang/String;

    invoke-static {v0, v3}, Lecl;->a(Ljava/lang/String;I)Lecl;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->u:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lecl;->a(Ljava/lang/String;I)Lecl;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v4}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    const-string v1, "Failed to find fragment during resume!"

    invoke-static {v0, v1}, Lbiq;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :pswitch_3
    const v0, 0x7f0b02db    # com.google.android.gms.R.string.games_request_inbox_title

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->setTitle(I)V

    iget-boolean v0, p0, Ldwr;->p:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljj;->b(Z)V

    :cond_1
    return-void

    :pswitch_4
    const v0, 0x7f0b02dc    # com.google.android.gms.R.string.games_request_inbox_header_gifts

    goto :goto_2

    :pswitch_5
    const v0, 0x7f0b02dd    # com.google.android.gms.R.string.games_request_inbox_header_wishes

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final a()Leca;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->r:Lecj;

    return-object v0
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final d_(Ljava/lang/String;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, -0x1

    const-string v2, "inboxButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iget v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    if-eq v0, v2, :cond_1

    iput v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->b(Z)V

    :cond_1
    return-void

    :cond_2
    const-string v2, "giftsButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const-string v2, "wishesButton"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iput v2, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->b(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-super {p0}, Ldwr;->onBackPressed()V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Ldwr;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lecj;

    invoke-direct {v0, p0}, Lecj;-><init>(Ldwr;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->r:Lecj;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->u:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->u:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "HeadlessReqInboxAct"

    const-string v1, "Game ID not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljj;->b(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    iput v1, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->b(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    const-string v1, "savedStateCurrentFragmentIndex"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    goto :goto_2
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v6, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Ldwr;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v6

    :cond_0
    const v0, 0x7f0a037e    # com.google.android.gms.R.id.menu_progress_bar

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f04001a    # com.google.android.gms.R.layout.actionbar_indeterminate_progress

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f0a007a    # com.google.android.gms.R.id.progress_bar

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->t:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-static {v2, v3}, Leu;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldwr;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "savedStateCurrentFragmentIndex"

    iget v1, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final v()Lebd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->r:Lecj;

    return-object v0
.end method

.method public final x_()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/requests/HeadlessRequestInboxListActivity;->s:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method
