.class public final Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Ldzy;
.implements Leam;


# instance fields
.field private v:Ldxs;

.field private w:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method


# virtual methods
.method public final D_()Ldzx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->v:Ldxs;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04007a    # com.google.android.gms.R.layout.games_public_invitation_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->setContentView(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->s:Z

    new-instance v0, Ldxs;

    invoke-direct {v0, p0}, Ldxs;-><init>(Ldxm;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->v:Ldxs;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->w:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->x:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->y:Ljava/lang/String;

    const v0, 0x7f0b02bb    # com.google.android.gms.R.string.games_match_inbox_header_invitations

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->setTitle(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->w:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljj;->b(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final p()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public final u()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->w:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationListActivity;->y:Ljava/lang/String;

    return-object v0
.end method
