.class public final Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;
.super Ldxm;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxm;-><init>()V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x0

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a013e    # com.google.android.gms.R.id.setting_text_layout

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a0140    # com.google.android.gms.R.id.setting_description

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a0141    # com.google.android.gms.R.id.setting_value

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a0142    # com.google.android.gms.R.id.setting_progress_bar

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setEnabled(Z)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a013e    # com.google.android.gms.R.id.setting_text_layout

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a0140    # com.google.android.gms.R.id.setting_description

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(I)V

    const v2, 0x7f0a0141    # com.google.android.gms.R.id.setting_value

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a0142    # com.google.android.gms.R.id.setting_progress_bar

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v1}, Leeh;->a(Landroid/view/View;)V

    invoke-static {v0}, Leeh;->a(Landroid/view/View;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0b01fe    # com.google.android.gms.R.string.games_client_settings_signout_progress_dialog_msg

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.ui.dialog.progressDialog"

    invoke-static {p0, v1, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    invoke-static {v0}, Lcte;->e(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Ldxq;

    invoke-direct {v1, p0}, Ldxq;-><init>(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "ClientSettingsActivity"

    const-string v1, "Sign-out failed, GoogleApiClient not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private u()V
    .locals 1

    const v0, 0x7f0a013b    # com.google.android.gms.R.id.google_account_setting

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;)V

    return-void
.end method

.method private v()V
    .locals 1

    const v0, 0x7f0a013c    # com.google.android.gms.R.id.sharing_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final a(Lbbo;)V
    .locals 2

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Ldxm;->a(Lbbo;)V

    goto :goto_0
.end method

.method public final l(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ldxm;->l(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->v:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-static {v0}, Lcte;->c(Lbdu;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->v:Ljava/lang/String;

    :cond_0
    invoke-static {v0}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "ClientSettingsActivity"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0a013b    # com.google.android.gms.R.id.google_account_setting

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0b01fa    # com.google.android.gms.R.string.games_client_settings_google_account_desc

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;ILjava/lang/String;)V

    const v0, 0x7f0a013c    # com.google.android.gms.R.id.sharing_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0b0200    # com.google.android.gms.R.string.games_client_settings_sharing_description

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->a(Landroid/view/ViewGroup;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    const-string v0, "com.google.android.gms.games.ui.dialog.progressDialog"

    invoke-static {p0, v0}, Ledd;->a(Lo;Ljava/lang/String;)V

    const/16 v0, 0x2711

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->v()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->c()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Ldxm;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    move-result-object v1

    invoke-static {v1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "ClientSettingsActivity"

    const-string v1, "We don\'t have a current account name, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->finish()V

    goto :goto_0

    :cond_0
    const v2, 0x7f0b01fc    # com.google.android.gms.R.string.games_client_settings_signout_alert_dialog_title

    invoke-static {p0, v2}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0b01fd    # com.google.android.gms.R.string.games_client_settings_signout_alert_dialog_msg

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ldxr;

    invoke-direct {v1, p0, v6}, Ldxr;-><init>(Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;B)V

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a    # android.R.string.ok

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000    # android.R.string.cancel

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    invoke-static {p0, v0}, Leee;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->w:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->v:Ljava/lang/String;

    invoke-static {v1}, Lfmq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->p:Ljava/lang/String;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.plus.action.MANAGE_APP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.plus.APP_IS_ASPEN"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3, v5}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a013b
        :pswitch_0    # com.google.android.gms.R.id.google_account_setting
        :pswitch_1    # com.google.android.gms.R.id.sharing_settings
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/16 v3, 0x8

    const/4 v8, 0x1

    const v7, 0x7f0a013f    # com.google.android.gms.R.id.setting_name

    const/4 v6, 0x0

    invoke-super {p0, p1}, Ldxm;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04005e    # com.google.android.gms.R.layout.games_client_settings_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setContentView(I)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setResult(I)V

    const-string v0, "com.google.android.gms"

    invoke-static {p0, v0}, Ledw;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f0a013d    # com.google.android.gms.R.id.gms_build_version

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a0142    # com.google.android.gms.R.id.setting_progress_bar

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v3, 0x7f0b020a    # com.google.android.gms.R.string.games_gcore_build_version_title

    new-array v4, v8, [Ljava/lang/Object;

    const v5, 0x7f0b0458    # com.google.android.gms.R.string.common_app_name

    invoke-virtual {p0, v5}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0140    # com.google.android.gms.R.id.setting_description

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    const v0, 0x7f0b01f9    # com.google.android.gms.R.string.games_client_settings_activity_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->setTitle(I)V

    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->s:Z

    const v0, 0x7f0a013b    # com.google.android.gms.R.id.google_account_setting

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a013e    # com.google.android.gms.R.id.setting_text_layout

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b01fb    # com.google.android.gms.R.string.games_client_settings_google_account_signout_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0a013c    # com.google.android.gms.R.id.sharing_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b01ff    # com.google.android.gms.R.string.games_client_settings_sharing_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iput-boolean v8, p0, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->t:Z

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->u()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->v()V

    return-void

    :cond_0
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    invoke-super {p0}, Ldxm;->onResume()V

    return-void
.end method

.method public final onStart()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/main/ClientSettingsActivity;->j()Lbdu;

    invoke-super {p0}, Ldxm;->onStart()V

    return-void
.end method

.method protected final p()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method
