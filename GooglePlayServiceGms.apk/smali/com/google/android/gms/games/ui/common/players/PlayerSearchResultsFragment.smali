.class public final Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;
.super Ldvp;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Ldvf;
.implements Ledl;
.implements Ledt;
.implements Ledv;
.implements Leec;


# instance fields
.field private Z:Ljava/lang/String;

.field private aa:Leds;

.field private ab:Leap;

.field private ac:Landroid/view/View;

.field private ad:Z

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Z

.field private ah:Z

.field private ai:Lfaj;

.field private aj:Ledu;

.field private i:Ldvn;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ldvp;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ad:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ag:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ah:Z

    return-void
.end method


# virtual methods
.method public final R()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->af:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    sget v0, Lxc;->c:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lxa;->s:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ac:Landroid/view/View;

    new-instance v0, Leds;

    invoke-direct {v0, v1, p0}, Leds;-><init>(Landroid/view/View;Ledt;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    sget v0, Lxa;->r:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v2, Lxf;->aY:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Leds;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-object v1
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 5

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    const/4 v1, -0x1

    if-ne p2, v1, :cond_2

    invoke-static {p3}, Lbex;->a(Landroid/content/Intent;)Lbey;

    move-result-object v1

    invoke-interface {v1}, Lbey;->d()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Lbey;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    const-string v2, ""

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->Z:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-static {v1, v2, v3, v4, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Ldvp;->a(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public final a(Lbdu;)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ad:Z

    invoke-static {p1}, Lcte;->b(Lbdu;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->Z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->Z:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "PlayerSearchResFrag"

    const-string v1, "onGamesClientConnected: no current account! Bailing out..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ai:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ai:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ae:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "PlayerSearchResFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGoogleApiClientConnected: running pending query \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ae:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ae:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ae:Ljava/lang/String;

    goto :goto_0
.end method

.method public final synthetic a(Lbek;)V
    .locals 4

    check-cast p1, Lctx;

    invoke-interface {p1}, Lctx;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Lctx;->a()Lcts;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->L()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcts;->b()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v2, v0}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcts;->b()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ag:Z

    if-eqz v2, :cond_2

    const-string v0, "PlayerSearchResFrag"

    const-string v2, "onPlayersLoaded: discarding stray result"

    invoke-static {v0, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v1}, Lcts;->b()V

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-static {v0}, Leee;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    invoke-virtual {v2}, Leap;->h()V

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    invoke-virtual {v2, v1}, Leap;->a(Lbgo;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ah:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ah:Z

    invoke-virtual {v1}, Lcts;->a()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a()Landroid/widget/ListView;

    move-result-object v2

    invoke-static {v2}, Leee;->a(Landroid/widget/ListView;)V

    :cond_4
    invoke-virtual {v1}, Lcts;->a()I

    move-result v1

    if-lez v1, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ac:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcts;->b()V

    throw v0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    sget v2, Lxf;->aM:I

    sget v3, Lxf;->aY:I

    invoke-virtual {v1, v0, v2, v3}, Leds;->a(III)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ad:Z

    if-nez v0, :cond_1

    const-string v0, "PlayerSearchResFrag"

    const-string v1, "doSearch: not connected yet! Stashing away mPendingQuery..."

    invoke-static {v0, v1}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ae:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->J()Lbdu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v2}, Ldvn;->l()Z

    move-result v2

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    invoke-virtual {v0}, Leap;->a()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Leds;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ag:Z

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->af:Ljava/lang/String;

    sget-object v1, Lcte;->m:Lctw;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->af:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-static {v3}, Ledx;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lctw;->a(Lbdu;Ljava/lang/String;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aa:Leds;

    invoke-virtual {v0, v4}, Leds;->a(I)V

    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ag:Z

    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ah:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;Landroid/view/View;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    const-string v2, "Got onMenuItemClick from the dest app!"

    invoke-static {v0, v2}, Lbiq;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lxa;->ag:I

    if-ne v2, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aj:Ledu;

    if-eqz v1, :cond_0

    const-string v1, "PlayerSearchResFrag"

    const-string v2, "onManageCircles(): canceling the helper that was already running..."

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aj:Ledu;

    invoke-virtual {v1}, Ledu;->b()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ai:Lfaj;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->Z:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Ledu;->a(Lcom/google/android/gms/games/Player;Ledv;Landroid/support/v4/app/Fragment;Lfaj;Ljava/lang/String;I)Ledu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->aj:Ledu;

    :goto_0
    return v5

    :cond_1
    const-string v0, "PlayerSearchResFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onMenuItemClick: click from unexpected item: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v1

    goto :goto_0

    :cond_2
    const-string v0, "PlayerSearchResFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onMenuItemClick: click from unexpected view/item: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v1

    goto :goto_0
.end method

.method public final a_(I)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->J()Lbdu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v2}, Ldvn;->l()Z

    move-result v2

    invoke-static {v0, v1, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "PlayerSearchResFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->m:Lctw;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->af:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-static {v3}, Ledx;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lctw;->b(Lbdu;Ljava/lang/String;I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method

.method public final aa()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->L()Z

    move-result v0

    return v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Ldvp;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldvn;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    new-instance v2, Ldwp;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-direct {v2, v0}, Ldwp;-><init>(Landroid/content/Context;)V

    sget v0, Lxf;->aX:I

    invoke-virtual {v2, v0}, Ldwp;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    new-instance v3, Leap;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    invoke-direct {v3, v4, p0, p0, v0}, Leap;-><init>(Ldvn;Landroid/view/View$OnClickListener;Ledl;I)V

    iput-object v3, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    invoke-virtual {v0, p0}, Leap;->a(Ldvf;)V

    new-instance v0, Ldwu;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    aput-object v2, v3, v5

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    aput-object v2, v3, v1

    invoke-direct {v0, v3}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->a(Landroid/widget/ListAdapter;)V

    new-instance v2, Leau;

    invoke-direct {v2, p0, v5}, Leau;-><init>(Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;B)V

    new-instance v0, Lfaj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    const/16 v4, 0x76

    const/4 v5, 0x0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ai:Lfaj;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ab:Leap;

    invoke-virtual {v0}, Leap;->a()V

    invoke-super {p0}, Ldvp;->f()V

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-super {p0}, Ldvp;->g_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->ai:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    invoke-static {p1}, Leee;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/games/Player;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/games/Player;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    instance-of v1, v1, Leat;

    invoke-static {v1}, Lbiq;->a(Z)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    instance-of v1, v1, Leat;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerSearchResultsFragment;->i:Ldvn;

    check-cast v1, Leat;

    invoke-interface {v1, v0}, Leat;->a(Lcom/google/android/gms/games/Player;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "PlayerSearchResFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
