.class public Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Leaj;
.implements Leal;


# instance fields
.field private n:[Lcom/google/android/gms/games/multiplayer/Participant;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Landroid/net/Uri;

.field private s:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()[Lcom/google/android/gms/games/multiplayer/Participant;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->n:[Lcom/google/android/gms/games/multiplayer/Participant;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->r:Landroid/net/Uri;

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->s:Landroid/net/Uri;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->requestWindowFeature(I)Z

    const v0, 0x7f040074    # com.google.android.gms.R.layout.games_participant_list_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "com.google.android.gms.games.PARTICIPANTS"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v0, "ParticipListAct"

    const-string v3, "Required participants list is missing."

    invoke-static {v0, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->finish()V

    :cond_0
    const-string v0, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->o:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "ParticipListAct"

    const-string v3, "Required current account name is missing."

    invoke-static {v0, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->finish()V

    :cond_1
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->p:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->p:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, "ParticipListAct"

    const-string v3, "Required application id is missing."

    invoke-static {v0, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->finish()V

    :cond_2
    const-string v0, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->q:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->q:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, "ParticipListAct"

    const-string v3, "Required current player id is missing."

    invoke-static {v0, v3}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->finish()V

    :cond_3
    const-string v0, "com.google.android.gms.games.FEATURED_URI"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->r:Landroid/net/Uri;

    const-string v0, "com.google.android.gms.games.ICON_URI"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->s:Landroid/net/Uri;

    array-length v0, v2

    new-array v0, v0, [Lcom/google/android/gms/games/multiplayer/Participant;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->n:[Lcom/google/android/gms/games/multiplayer/Participant;

    const/4 v0, 0x0

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/restricted/matches/ParticipantListActivity;->n:[Lcom/google/android/gms/games/multiplayer/Participant;

    aget-object v0, v2, v1

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    return-void
.end method
