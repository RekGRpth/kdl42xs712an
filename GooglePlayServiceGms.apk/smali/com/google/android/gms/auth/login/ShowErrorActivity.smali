.class public final Lcom/google/android/gms/auth/login/ShowErrorActivity;
.super Latw;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field A:I

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Laso;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/Button;

.field private J:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Latw;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Laso;ZZ)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/ShowErrorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "isCreatingAccount"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "detail"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p2}, Laso;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isAddingAccount"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->B:Ljava/lang/String;

    const-string v0, "isCreatingAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->D:Z

    const-string v0, "isAddingAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->E:Z

    const-string v0, "detail"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    const-string v0, "status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lapc;->a(Ljava/lang/String;)Laso;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    if-eqz p1, :cond_0

    const v0, 0x7f0b04ba    # com.google.android.gms.R.string.auth_submission_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->G:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->G:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->G:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Latw;->onActivityResult(IILandroid/content/Intent;)V

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Latw;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    sget-object v1, Laso;->a:Laso;

    iput-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    sget-object v1, Laus;->a:[I

    invoke-virtual {v0}, Laso;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sparse-switch v1, :sswitch_data_0

    const-string v1, "GLSActivity"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GLSActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Unhandled status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Laso;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    :goto_1
    return-void

    :sswitch_0
    const-string v0, "com.google.android.apps.enterprise.dmagent"

    invoke-static {v0}, Lbjt;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GLSActivity"

    const-string v1, "Market not found for dmagent"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_1

    :sswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->E:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_1

    :cond_1
    iget v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_2
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v8, 0x2

    const v6, 0x7f0b048a    # com.google.android.gms.R.string.auth_back_button_label

    const/4 v7, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Latw;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Landroid/os/Bundle;)V

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setResult(I)V

    const v0, 0x7f040033    # com.google.android.gms.R.layout.auth_submission_error_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setContentView(I)V

    const v0, 0x7f0a00c0    # com.google.android.gms.R.id.submission_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->G:Landroid/widget/TextView;

    const v0, 0x7f0a00c1    # com.google.android.gms.R.id.explanation

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v0, 0x7f0a00a5    # com.google.android.gms.R.id.next_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->I:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->I:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00c3    # com.google.android.gms.R.id.skip_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "title"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const-string v3, "label"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->I:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    invoke-static {v0}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    const-string v3, "GLSActivity"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->x:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ShowError: %s %s %s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    aput-object v5, v4, v2

    iget v5, v0, Lapc;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    iget-object v5, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSActivity"

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v3, Laus;->a:[I

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    invoke-virtual {v4}, Laso;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_1
    iget v1, v0, Lapc;->f:I

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    iget v0, v0, Lapc;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :pswitch_0
    const v0, 0x7f0b055d    # com.google.android.gms.R.string.auth_gls_dmagent_activity_download_retry_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->I:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b055c    # com.google.android.gms.R.string.auth_gls_dmagent_activity_download_message

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->B:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f0b04e2    # com.google.android.gms.R.string.auth_error_needs_browser

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->finish()V

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v3, 0x7f0b04b6    # com.google.android.gms.R.string.auth_error_bad_password

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->B:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    goto/16 :goto_2

    :pswitch_4
    const v0, 0x7f0b04de    # com.google.android.gms.R.string.auth_show_error_activity_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->I:Landroid/widget/Button;

    const v3, 0x7f0b0490    # com.google.android.gms.R.string.auth_try_again_button_label

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->E:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f0b04b0    # com.google.android.gms.R.string.auth_error_login_failed

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    const v1, 0x7f0b048c    # com.google.android.gms.R.string.auth_skip_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :goto_3
    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    goto/16 :goto_2

    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b04c2    # com.google.android.gms.R.string.auth_login_activity_loginfail_text_pwonly

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v4, "https://www.google.com/accounts/recovery/?hl=%s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v5, Landroid/text/Annotation;

    invoke-virtual {v4, v2, v0, v5}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/Annotation;

    aget-object v0, v0, v2

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v4, v0}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/common/ux/URLSpanNoUnderline;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0, v5, v6, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->J:Landroid/widget/Button;

    const v1, 0x7f0b048f    # com.google.android.gms.R.string.auth_cancel_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_3

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v3, 0x7f0b04b9    # com.google.android.gms.R.string.auth_existing_account_error_text

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->B:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b04b8    # com.google.android.gms.R.string.auth_existing_account_error_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    iput v7, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    goto/16 :goto_2

    :pswitch_6
    sget-object v0, Laua;->z:Laub;

    iget-boolean v0, v0, Laub;->a:Z

    if-nez v0, :cond_6

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Laua;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_4
    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    iput v2, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    if-eqz v0, :cond_9

    const v0, 0x320ce

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    invoke-static {}, Laox;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f0b04af    # com.google.android.gms.R.string.auth_no_network_help_wifi_only

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b04ad    # com.google.android.gms.R.string.auth_no_network

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_5

    :cond_8
    const v0, 0x7f0b04ae    # com.google.android.gms.R.string.auth_no_network_help

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    sget-object v1, Laso;->m:Laso;

    if-ne v0, v1, :cond_b

    const v0, 0x320cc

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    invoke-static {}, Laox;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f0b0497    # com.google.android.gms.R.string.auth_network_unreliable_help_wifi_only

    :goto_7
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0b0495    # com.google.android.gms.R.string.auth_network_unreliable

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_a
    const v0, 0x7f0b0496    # com.google.android.gms.R.string.auth_network_unreliable_help

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f0b0494    # com.google.android.gms.R.string.auth_server_error_help

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0b0493    # com.google.android.gms.R.string.auth_server_error

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_7
    const v0, 0x7f0b049d    # com.google.android.gms.R.string.auth_plus_failure_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->D:Z

    if-eqz v0, :cond_c

    const v0, 0x7f0b049e    # com.google.android.gms.R.string.auth_plus_failure_new_account_text

    :goto_8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_c
    const v0, 0x7f0b049f    # com.google.android.gms.R.string.auth_plus_failure_text

    goto :goto_8

    :pswitch_8
    iput v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->A:I

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    const v1, 0x7f0b0494    # com.google.android.gms.R.string.auth_server_error_help

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0b0493    # com.google.android.gms.R.string.auth_server_error

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/ShowErrorActivity;->a(Ljava/lang/CharSequence;)V

    const-string v0, "GLSActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GAIA ERROR WITH NO RESOURCE STRING "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    invoke-virtual {v2}, Laso;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->H:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "isCreatingAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "isAddingAccount"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "detail"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "status"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/ShowErrorActivity;->F:Laso;

    invoke-virtual {v1}, Laso;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
