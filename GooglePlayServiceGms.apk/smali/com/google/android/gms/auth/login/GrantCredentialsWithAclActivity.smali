.class public Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Latr;
.implements Latu;
.implements Latv;


# static fields
.field private static final n:Ljava/lang/String;

.field private static final o:Ljava/util/regex/Pattern;

.field private static final p:Ljava/util/regex/Pattern;

.field private static final q:Ljava/lang/String;

.field private static final r:Ljava/lang/String;

.field private static final s:Ljava/lang/String;

.field private static final t:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/util/ArrayList;

.field private D:I

.field private E:Landroid/widget/Button;

.field private F:Landroid/widget/Button;

.field private G:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

.field private H:Latq;

.field private I:J

.field private J:J

.field private u:Landroid/widget/LinearLayout;

.field private v:Landroid/graphics/drawable/Drawable;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->n:Ljava/lang/String;

    const-string v0, "<placeholder\\s*id=[\'\"]app_name[\'\"]\\s*/?>(.*</placeholder>)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->o:Ljava/util/regex/Pattern;

    const-string v0, "<br\\s*/?>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->p:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->q:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "facl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->r:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pacl"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->s:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "consent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->t:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    return-void
.end method

.method private a(Laso;Laro;)Landroid/content/Intent;
    .locals 7

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {p1}, Lapc;->a(Laso;)Lapc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lapc;->b(Landroid/content/Intent;)V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->t:Ljava/lang/String;

    invoke-virtual {p2}, Laro;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->r:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->j()Z

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->g()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v3, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->s:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Landroid/content/Intent;
    .locals 4

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-class v2, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "callingPkg"

    invoke-virtual {v1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "callingUid"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "service"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "acctName"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;

    invoke-static {v0}, Lapb;->a(Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;)Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "scopeData"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object v1
.end method

.method public static a(Landroid/content/Intent;)Laug;
    .locals 5

    invoke-static {p0}, Lapc;->a(Landroid/content/Intent;)Laso;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->r:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    sget-object v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->s:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->t:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Laro;->a:Laro;

    :goto_0
    new-instance v4, Laug;

    invoke-direct {v4, v2, v0, v3, v1}, Laug;-><init>(Laso;Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;Laro;)V

    return-object v4

    :cond_0
    invoke-static {v1}, Laro;->valueOf(Ljava/lang/String;)Laro;

    move-result-object v1

    goto :goto_0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "scopeFragment"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "callingPkg"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    const-string v0, "callingUid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->z:I

    const-string v0, "service"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    const-string v1, "oauth2:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    const-string v3, "https://www.googleapis.com/auth/plus.me"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->x:Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "acctName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    const-string v0, "scopeData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    const-string v0, "lastScopeIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->D:I

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lbck;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
    .locals 1

    sget-object v0, Lbck;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 7

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p2, v0}, Lbcl;->a(Ljava/util/List;Z)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v6

    :goto_0
    move-object v0, p0

    move-object v3, p1

    move-object v4, p4

    invoke-static/range {v0 .. v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    return-void

    :cond_0
    move-object v6, v2

    goto :goto_0
.end method

.method private b(I)Latt;
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    invoke-static {p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Latt;

    return-object v0
.end method

.method private e()V
    .locals 10

    iget-object v2, p0, Lo;->b:Lw;

    invoke-virtual {v2}, Lu;->a()Lag;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-static {v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_0

    const v6, 0x7f0a007e    # com.google.android.gms.R.id.scopes_layout

    iget-object v7, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->w:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    invoke-static {v1, v7, v8, v9, v0}, Latt;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/acl/ScopeData;)Latt;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v5}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->d()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->x:Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Lag;->f()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v3}, Lag;->c()I

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->x:Z

    if-nez v0, :cond_5

    const v0, 0x7f0a00c5    # com.google.android.gms.R.id.title_logo

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201f1    # com.google.android.gms.R.drawable.plus_auth_google_logo

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const v0, 0x7f0a007f    # com.google.android.gms.R.id.footnote

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b04d2    # com.google.android.gms.R.string.auth_app_permission_ok_footnote

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a007c    # com.google.android.gms.R.id.accept_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0b0488    # com.google.android.gms.R.string.auth_ok_button_label

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    return-void
.end method

.method private f()V
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/acl/ScopeData;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/acl/ScopeData;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->b(I)Latt;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Latt;->K()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Latt;->L()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    sget-object v0, Lbcj;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_2
    sget-object v0, Laso;->o:Laso;

    sget-object v2, Laro;->c:Laro;

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Laso;Laro;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(ILandroid/content/Intent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->J:J

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-wide v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->J:J

    iget-wide v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->I:J

    sub-long/2addr v1, v3

    iput-wide v1, v0, Latq;->f:J

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    invoke-virtual {v0}, Latq;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->v:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final a(J)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-wide v1, v0, Latq;->g:J

    cmp-long v1, p1, v1

    if-lez v1, :cond_0

    iput-wide p1, v0, Latq;->g:J

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    const/4 v1, 0x1

    iput-boolean v1, v0, Latq;->h:Z

    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iput-boolean p1, v0, Latq;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iput-boolean p2, v0, Latq;->j:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->x:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    const/4 v1, 0x1

    iput v1, v0, Latq;->e:I

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->f()V

    invoke-super {p0}, Lo;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    const/4 v1, 0x3

    iput v1, v0, Latq;->e:I

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->f()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iput v9, v0, Latq;->e:I

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->G:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v4

    move v1, v4

    move v3, v4

    :goto_1
    if-ge v5, v6, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-direct {p0, v5}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->b(I)Latt;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Latt;->K()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7}, Latt;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/acl/ScopeData;->a(Ljava/lang/String;)V

    sget-object v0, Lbcj;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v7}, Latt;->M()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {p0, v0, v3, v7}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    move v0, v1

    move v1, v2

    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_0
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Latt;->L()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Latt;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->b(Ljava/lang/String;)V

    invoke-virtual {v7}, Latt;->J()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    sget-object v0, Lbcj;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v7}, Latt;->N()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v7}, Latt;->J()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {p0, v0, v1, v7}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    move v0, v2

    move v1, v3

    goto :goto_2

    :cond_1
    invoke-virtual {v0, v10}, Lcom/google/android/gms/common/acl/ScopeData;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Lcom/google/android/gms/common/acl/ScopeData;->b(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/acl/ScopeData;->a(Z)V

    move v0, v1

    move v1, v3

    goto :goto_2

    :cond_2
    if-nez v3, :cond_3

    if-eqz v1, :cond_4

    :cond_3
    sget-object v0, Lbcj;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_4
    invoke-static {p0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "GLSActivity"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    move v0, v4

    :goto_3
    if-ge v0, v2, :cond_7

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->b(I)Latt;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Latt;->K()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Latt;->L()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_5
    invoke-virtual {v3}, Latt;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    const-string v0, "GLSActivity"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v0, -0x1

    sget-object v1, Laso;->a:Laso;

    sget-object v2, Laro;->b:Laro;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Laso;Laro;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(ILandroid/content/Intent;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->J:J

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-wide v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->J:J

    iget-wide v3, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->I:J

    sub-long/2addr v1, v3

    iput-wide v1, v0, Latq;->f:J

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    invoke-virtual {v0}, Latq;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->G:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->pageScroll(I)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a007b
        :pswitch_0    # com.google.android.gms.R.id.cancel_button
        :pswitch_1    # com.google.android.gms.R.id.accept_button
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/4 v1, 0x0

    const/4 v10, 0x5

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Latq;

    invoke-direct {v0, p0}, Latq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->I:J

    invoke-virtual {p0, v8}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->requestWindowFeature(I)Z

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Landroid/os/Bundle;)V

    :goto_0
    invoke-static {p0}, Lbox;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GLSActivity"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v6, "\n"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->a(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const-string v0, "GLSActivity"

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    if-nez v0, :cond_6

    :cond_3
    const-string v0, "GLSActivity"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v2, "GLSActivity"

    const-string v4, "%s started with username=%s callingPackage=%s service=%s"

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_2
    aput-object v0, v5, v8

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    aput-object v0, v5, v9

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    :goto_3
    return-void

    :cond_5
    const-string v0, "<omitted>"

    goto :goto_2

    :cond_6
    const v0, 0x7f040032    # com.google.android.gms.R.layout.auth_request_access_to_google_service

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setContentView(I)V

    const v0, 0x7f0a007e    # com.google.android.gms.R.id.scopes_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->u:Landroid/widget/LinearLayout;

    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->E:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->E:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a007c    # com.google.android.gms.R.id.accept_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->F:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->F:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00be    # com.google.android.gms.R.id.scroll_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->G:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    new-instance v0, Laoy;

    invoke-direct {v0, p0}, Laoy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Laoy;->d(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Laoy;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->w:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->v:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->w:Ljava/lang/String;

    if-nez v2, :cond_9

    :cond_7
    const-string v0, "GLSActivity"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "GLSActivity"

    const-string v1, "Failed to get ApplicationInfo for package: %s"

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->finish()V

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    invoke-virtual {v2, v4}, Latq;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-virtual {v2, v4}, Latq;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->H:Latq;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v4}, Laoy;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Latq;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v2

    const-string v4, "headerFragment"

    invoke-virtual {v0, v4}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_a

    const v0, 0x7f0a00bf    # com.google.android.gms.R.id.header_fragment_layout

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->w:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    invoke-static {v4, v5}, Lats;->a(Ljava/lang/String;Ljava/lang/String;)Lats;

    move-result-object v4

    const-string v5, "headerFragment"

    invoke-virtual {v2, v0, v4, v5}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_a
    invoke-virtual {v2}, Lag;->f()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {v2}, Lag;->c()I

    :cond_b
    const v0, 0x7f0a007f    # com.google.android.gms.R.id.footnote

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b04d1    # com.google.android.gms.R.string.auth_app_permission_sign_in_footnote

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audience:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b04cd    # com.google.android.gms.R.string.auth_allow_external_site_access

    new-array v2, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "audience:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected auth token type to start with \'audience:\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lbbv;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_13

    const v1, 0x7f0d01d1    # com.google.android.gms.R.dimen.plus_auth_dialog_tablet_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    :goto_5
    float-to-double v0, v0

    invoke-static {p0, v0, v1}, Lbow;->a(Landroid/app/Activity;D)V

    goto/16 :goto_3

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_e
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-static {p0, v0}, Laoy;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    :cond_f
    const-string v2, "SID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    const-string v2, "LSID"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_10
    const v0, 0x7f0b0483    # com.google.android.gms.R.string.sid_lsid_grant_label

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b0484    # com.google.android.gms.R.string.sid_lsid_grant_detail

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_11
    new-instance v2, Lbcg;

    invoke-direct {v2, v0, v1}, Lbcg;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lbcg;->a()Lcom/google/android/gms/common/acl/ScopeData;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->e()V

    goto :goto_4

    :cond_13
    const v1, 0x7f0d01d2    # com.google.android.gms.R.dimen.plus_auth_dialog_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_5
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Lo;->onDestroy()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "callingPkg"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "callingUid"

    iget v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "service"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "acctName"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "scopeData"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "lastScopeIndex"

    iget v1, p0, Lcom/google/android/gms/auth/login/GrantCredentialsWithAclActivity;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
