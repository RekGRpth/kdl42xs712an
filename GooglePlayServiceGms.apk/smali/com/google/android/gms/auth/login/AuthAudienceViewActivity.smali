.class public Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbfo;


# instance fields
.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Lcom/google/android/gms/common/acl/ScopeData;

.field private r:Lcom/google/android/gms/common/people/data/Audience;

.field private s:Lcom/google/android/gms/common/people/data/Audience;

.field private t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

.field private u:Landroid/widget/RadioButton;

.field private v:Landroid/widget/LinearLayout;

.field private w:Landroid/widget/RadioButton;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/widget/TextView;

.field private z:Lfaj;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/acl/ScopeData;Lcom/google/android/gms/common/people/data/Audience;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "scope_description"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "calling_package"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "scope_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "pacl_audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->o:Ljava/lang/String;

    sget-object v1, Lbck;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->p:Ljava/lang/String;

    invoke-static {p0, v0, p1, v1, v2}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setChecked(Z)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->w:Landroid/widget/RadioButton;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Lcom/google/android/gms/common/people/data/Audience;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->q:Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {v0}, Lcom/google/android/gms/common/acl/ScopeData;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {v1}, Lbpd;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbky;->a([B)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Lbkw;

    invoke-direct {v2}, Lbkw;-><init>()V

    invoke-virtual {v2, v0}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v0

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "AuthAudienceViewActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to parse audience from roster: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "myCircles"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b045e    # com.google.android.gms.R.string.common_chips_label_your_circles

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lbkw;

    invoke-direct {v1}, Lbkw;-><init>()V

    invoke-virtual {v1, v0}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v0

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->b(Z)V

    invoke-static {}, Lbet;->a()Lbeu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->o:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbeu;->a(Ljava/lang/String;)Lbeu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    invoke-interface {v0, v1}, Lbeu;->a(Lcom/google/android/gms/common/people/data/Audience;)Lbeu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lbeu;->b(Ljava/util/List;)Lbeu;

    move-result-object v0

    sget-object v1, Lbch;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lbeu;->b(Ljava/lang/String;)Lbeu;

    move-result-object v0

    const v1, 0x7f0b0356    # com.google.android.gms.R.string.plus_audience_selection_description_acl

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbeu;->d(Ljava/lang/String;)Lbeu;

    move-result-object v0

    invoke-interface {v0}, Lbeu;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    sget-object v0, Lbcj;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->g()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_4

    invoke-static {p3}, Lbet;->a(Landroid/content/Intent;)Lbev;

    move-result-object v2

    new-instance v3, Lbkw;

    invoke-direct {v3}, Lbkw;-><init>()V

    invoke-interface {v2}, Lbev;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v3, v2}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v2

    invoke-virtual {v2}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v3, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v2}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->b(Z)V

    :cond_1
    sget-object v0, Lbcz;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lo;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->b(Z)V

    :cond_5
    sget-object v0, Lbcz;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "pacl_audience"

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->finish()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_2

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->g()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->w:Landroid/widget/RadioButton;

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->x:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->b(Z)V

    goto :goto_0

    :cond_4
    const v0, 0x7f0a025c    # com.google.android.gms.R.id.ok

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "pacl_audience"

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->finish()V

    goto :goto_0

    :cond_5
    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "pacl_audience"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    const-string v0, "scope_description"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->n:Ljava/lang/String;

    const-string v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->o:Ljava/lang/String;

    const-string v0, "scope_data"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/acl/ScopeData;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->q:Lcom/google/android/gms/common/acl/ScopeData;

    const-string v0, "calling_package"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->p:Ljava/lang/String;

    new-instance v0, Lfaj;

    const-class v1, Lbbr;

    invoke-static {v1}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbbr;

    const-class v1, Lbbs;

    invoke-static {v1}, Lbpw;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbbs;

    const/16 v4, 0x50

    iget-object v5, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->p:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->z:Lfaj;

    const v0, 0x7f0400cf    # com.google.android.gms.R.layout.plus_auth_audience_view_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->setContentView(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->r:Lcom/google/android/gms/common/people/data/Audience;

    const v0, 0x7f0a00b8    # com.google.android.gms.R.id.audience_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    new-instance v1, Lbfs;

    iget-object v2, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->z:Lfaj;

    invoke-direct {v1, v2}, Lbfs;-><init>(Lfaj;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lbfp;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lbfo;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Z)V

    const v0, 0x7f0a00b7    # com.google.android.gms.R.id.acl_radio_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    const v0, 0x7f0a0272    # com.google.android.gms.R.id.audience_view_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0273    # com.google.android.gms.R.id.private_pacl_radio_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->w:Landroid/widget/RadioButton;

    const v0, 0x7f0a00ba    # com.google.android.gms.R.id.private_pacl_layout

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->x:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->t:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->w:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->s:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lblc;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->b(Z)V

    const v0, 0x7f0a025c    # com.google.android.gms.R.id.ok

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    const v0, 0x7f0a0271    # com.google.android.gms.R.id.pacl_description

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->y:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->y:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lbbv;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0d01d1    # com.google.android.gms.R.dimen.plus_auth_dialog_tablet_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    :goto_1
    float-to-double v0, v0

    invoke-static {p0, v0, v1}, Lbow;->a(Landroid/app/Activity;D)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const v1, 0x7f0d01d2    # com.google.android.gms.R.dimen.plus_auth_dialog_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->z:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthAudienceViewActivity;->z:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-super {p0}, Lo;->onStop()V

    return-void
.end method
