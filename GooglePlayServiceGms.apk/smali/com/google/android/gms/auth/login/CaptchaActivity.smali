.class public Lcom/google/android/gms/auth/login/CaptchaActivity;
.super Latw;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Landroid/widget/EditText;

.field private B:Landroid/widget/Button;

.field private C:Landroid/widget/Button;

.field private D:Landroid/widget/ImageView;

.field private E:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Latw;-><init>()V

    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/CaptchaActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "bitmap"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    const-string v0, "answer"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "answer"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->finish()V

    return-void
.end method

.method public final h()V
    .locals 2

    invoke-super {p0}, Latw;->h()V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->B:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->B:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setFocusable(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->C:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Latw;->onCreate(Landroid/os/Bundle;)V

    sget-boolean v0, Lcom/google/android/gms/auth/login/CaptchaActivity;->w:Z

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "Doing captcha..."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "bitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->E:Landroid/graphics/Bitmap;

    :goto_0
    const v0, 0x7f04002c    # com.google.android.gms.R.layout.auth_captcha_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->setContentView(I)V

    const v0, 0x7f0a00a3    # com.google.android.gms.R.id.captcha_image_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->D:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->D:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->E:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f0a00a4    # com.google.android.gms.R.id.captcha_answer_edit

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->A:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->A:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0a00a5    # com.google.android.gms.R.id.next_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->B:Landroid/widget/Button;

    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->C:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->C:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->C:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->A:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->B:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/CaptchaActivity;->a(Landroid/view/View;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/CaptchaActivity;->h()V

    return-void

    :cond_2
    const-string v0, "bitmap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->E:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Latw;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "bitmap"

    iget-object v1, p0, Lcom/google/android/gms/auth/login/CaptchaActivity;->E:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
