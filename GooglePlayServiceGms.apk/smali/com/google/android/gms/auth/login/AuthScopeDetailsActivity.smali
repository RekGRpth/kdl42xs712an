.class public Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Landroid/content/Intent;

.field private q:Landroid/widget/ScrollView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/String;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "scope_details"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "show_plus_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "calling_package"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a025c    # com.google.android.gms.R.id.ok

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    const-string v1, "detail_end_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->q:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->q:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    const-string v1, "detail_screen_scrollable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->finish()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    const-string v1, "scroll_screen_end"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const v5, 0x7f0a0277    # com.google.android.gms.R.id.scope_details_text

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0400d0    # com.google.android.gms.R.layout.plus_auth_scope_details_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->setContentView(I)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    const-string v1, "detail_start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "scope_details"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->n:Ljava/lang/String;

    const-string v1, "show_plus_title"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->o:Z

    const v0, 0x7f0a0276    # com.google.android.gms.R.id.scrollview_details

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->q:Landroid/widget/ScrollView;

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->q:Landroid/widget/ScrollView;

    invoke-virtual {v0, p0}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->n:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->o:Z

    if-nez v0, :cond_0

    const v0, 0x7f0a00c5    # com.google.android.gms.R.id.title_logo

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0201f1    # com.google.android.gms.R.drawable.plus_auth_google_logo

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    const v0, 0x7f0a025c    # com.google.android.gms.R.id.ok

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lbbv;->a(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0d01d1    # com.google.android.gms.R.dimen.plus_auth_dialog_tablet_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    :goto_1
    float-to-double v0, v0

    invoke-static {p0, v0, v1}, Lbow;->a(Landroid/app/Activity;D)V

    return-void

    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0d01d2    # com.google.android.gms.R.dimen.plus_auth_dialog_resize_factor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0276    # com.google.android.gms.R.id.scrollview_details

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->q:Landroid/widget/ScrollView;

    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/login/AuthScopeDetailsActivity;->p:Landroid/content/Intent;

    const-string v1, "scroll_screen_end"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return v3
.end method
