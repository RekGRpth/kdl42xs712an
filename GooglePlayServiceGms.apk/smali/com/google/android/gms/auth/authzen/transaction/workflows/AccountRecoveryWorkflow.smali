.class public Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;
.super Lanz;
.source "SourceFile"


# instance fields
.field private t:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lanz;-><init>()V

    return-void
.end method

.method public static a(Ljby;)Z
    .locals 5

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-boolean v1, p0, Ljby;->b:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Ljby;->c:Ljbx;

    iget-boolean v2, p0, Ljby;->f:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Ljby;->g:Ljbm;

    iget-boolean v2, v2, Ljbm;->k:Z

    if-eqz v2, :cond_2

    iget-boolean v2, v1, Ljbx;->e:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Ljbx;->f:Ljbl;

    iget-boolean v2, v2, Ljbl;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Ljbx;->f:Ljbl;

    iget-object v2, v2, Ljbl;->b:Ljbs;

    invoke-virtual {v2}, Ljbs;->c()I

    move-result v2

    if-eq v2, v3, :cond_2

    :cond_1
    const-string v1, "AuthZen"

    const-string v2, "AccountRecoveryStrings contains pin text but pin data is malformed in AccountRecoveryDescriptor"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-boolean v2, v1, Ljbx;->e:Z

    if-eqz v2, :cond_3

    iget-object v2, v1, Ljbx;->f:Ljbl;

    iget-boolean v2, v2, Ljbl;->a:Z

    if-eqz v2, :cond_3

    iget-object v2, v1, Ljbx;->f:Ljbl;

    iget-object v2, v2, Ljbl;->b:Ljbs;

    invoke-virtual {v2}, Ljbs;->c()I

    move-result v2

    if-eq v2, v3, :cond_3

    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected number of pin options found: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Ljbx;->f:Ljbl;

    iget-object v1, v1, Ljbl;->b:Ljbs;

    invoke-virtual {v1}, Ljbs;->c()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method public final a(Laoj;I)V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, -0x1

    invoke-virtual {p1}, Laoj;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Laoh;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->g()Ljby;

    move-result-object v0

    iget-object v2, v0, Ljby;->c:Ljbx;

    iget-boolean v2, v2, Ljbx;->e:Z

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->g()Ljby;

    move-result-object v0

    iget-object v0, v0, Ljby;->c:Ljbx;

    iget-object v0, v0, Ljbx;->f:Ljbl;

    iget-object v0, v0, Ljbl;->b:Ljbs;

    iget-object v2, v0, Ljbs;->c:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->t:Landroid/os/Bundle;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v0, v2}, Laof;->a(Landroid/os/Bundle;ILjava/util/List;)Laof;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Laoj;Laoj;)V

    :goto_2
    return-void

    :cond_0
    iget-object v0, v0, Ljby;->c:Ljbx;

    iget-object v0, v0, Ljbx;->f:Ljbl;

    iget-boolean v0, v0, Ljbl;->a:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->t:Landroid/os/Bundle;

    invoke-static {v0}, Laog;->c(Landroid/os/Bundle;)Laog;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->t:Landroid/os/Bundle;

    invoke-static {v0}, Laoe;->c(Landroid/os/Bundle;)Laoe;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget-object v2, Laog;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-nez p2, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(I)V

    :goto_3
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->finish()V

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(I)V

    goto :goto_3

    :cond_5
    sget-object v2, Laof;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    if-nez p2, :cond_6

    const-string v0, "\u2713"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(I)V

    :goto_4
    invoke-static {v0}, Laoi;->a(Ljava/lang/String;)Laoi;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Laoj;Laoj;)V

    goto :goto_2

    :cond_6
    const-string v0, "X"

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(I)V

    goto :goto_4

    :cond_7
    sget-object v1, Laoe;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->finish()V

    goto :goto_2

    :cond_8
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment not supported in account recovery workflow: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lanz;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->g()Ljby;

    move-result-object v0

    invoke-static {v0}, Laos;->a(Ljby;)Laor;

    move-result-object v0

    invoke-interface {v0}, Laor;->b()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->t:Landroid/os/Bundle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->t:Landroid/os/Bundle;

    invoke-static {v1}, Laoh;->c(Landroid/os/Bundle;)Laoh;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Laoj;Laoj;)V

    :cond_0
    return-void
.end method
