.class public Lcom/google/android/gms/auth/be/GoogleAccountDataService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    new-instance v0, Laoy;

    invoke-direct {v0, p0}, Laoy;-><init>(Landroid/content/Context;)V

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Laoy;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "GoogleAccountDataService is only usable by first party apps."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lapo;

    new-instance v2, Lapp;

    invoke-direct {v2, v0}, Lapp;-><init>(Laoy;)V

    invoke-direct {v1, p0, v2}, Lapo;-><init>(Lcom/google/android/gms/auth/be/GoogleAccountDataService;Lapp;)V

    return-object v1
.end method
