.class public Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private o:Lbdu;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->p:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->q:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->r:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->f()V

    return-void
.end method

.method private f()V
    .locals 2

    const v0, 0x7f0b01ec    # com.google.android.gms.R.string.icing_storage_managment_connection_error

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a020a    # com.google.android.gms.R.id.icing_clear_usage_reports

    if-ne v0, v1, :cond_1

    invoke-static {}, Leod;->J()Leod;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Leod;->a(Lu;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0207    # com.google.android.gms.R.id.icing_usage_reports_enabled

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->toggle()V

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->r:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    sget-object v2, Lahh;->c:Laja;

    iget-object v3, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    invoke-interface {v2, v3, v1}, Laja;->a(Lbdu;Z)Lbeh;

    move-result-object v1

    new-instance v2, Leoc;

    invoke-direct {v2, p0, v0}, Leoc;-><init>(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;Z)V

    invoke-interface {v1, v2}, Lbeh;->a(Lbel;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->f()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lbdw;

    invoke-direct {v0, p0}, Lbdw;-><init>(Landroid/content/Context;)V

    sget-object v1, Lahh;->b:Lbdm;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbdw;->a(Lbdm;Lbdv;)Lbdw;

    move-result-object v0

    invoke-virtual {v0}, Lbdw;->a()Lbdu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    const v0, 0x7f04009f    # com.google.android.gms.R.layout.icing_privacy_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->setContentView(I)V

    const v0, 0x7f0a0206    # com.google.android.gms.R.id.icing_main_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->p:Landroid/view/View;

    const v0, 0x7f0a0205    # com.google.android.gms.R.id.icing_progress_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->q:Landroid/view/View;

    const v0, 0x7f0a0209    # com.google.android.gms.R.id.icing_usage_reports_enabled_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->s:Landroid/widget/CheckBox;

    const v0, 0x7f0a0207    # com.google.android.gms.R.id.icing_usage_reports_enabled

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->r:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a020a    # com.google.android.gms.R.id.icing_clear_usage_reports

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    invoke-interface {v0}, Lbdu;->b()V

    invoke-super {p0}, Ljp;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Ljp;->onResume()V

    iget-object v0, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    invoke-interface {v0}, Lbdu;->a()V

    sget-object v0, Lahh;->c:Laja;

    iget-object v1, p0, Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;->o:Lbdu;

    invoke-interface {v0, v1}, Laja;->b(Lbdu;)Lbeh;

    move-result-object v0

    new-instance v1, Leob;

    invoke-direct {v1, p0}, Leob;-><init>(Lcom/google/android/gms/icing/ui/IcingPrivacyActivity;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    return-void
.end method
