.class public Lcom/google/android/gms/icing/service/IndexWorkerService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lens;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "maintenance_frequency_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->b:Ljava/lang/String;

    const-string v0, "maintenance_time_of_day_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->c:Ljava/lang/String;

    const-string v0, "maintenance_time_fuzz_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private static a(JJ)J
    .locals 9

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const/16 v0, 0xb

    const-wide/32 v5, 0x36ee80

    div-long v5, p0, v5

    const-wide/16 v7, 0x18

    rem-long/2addr v5, v7

    long-to-int v1, v5

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    const-wide/32 v5, 0xea60

    div-long v5, p0, v5

    const-wide/16 v7, 0x3c

    rem-long/2addr v5, v7

    long-to-int v1, v5

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    const-wide/16 v5, 0x3e8

    div-long v5, p0, v5

    const-wide/16 v7, 0x3c

    rem-long/2addr v5, v7

    long-to-int v1, v5

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    const-wide/16 v5, 0x3e8

    rem-long v5, p0, v5

    long-to-int v1, v5

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    :goto_0
    add-long v5, v3, p2

    cmp-long v5, v0, v5

    if-gez v5, :cond_0

    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-string v3, "%s: First maintenance at %s"

    const-string v4, "main"

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    return-wide v0
.end method

.method public static synthetic a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;
    .locals 4

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "icing"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "index-corpora"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.icing.INDEX_CONTENT_PROVIDER"

    const-class v3, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v1, v2, v0, p0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "delay"

    invoke-virtual {v1, v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-static {p0, v0, v1, p1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v0, p1, v1, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lejm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lens;

    invoke-virtual {v0}, Lens;->c()Lejm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 4

    new-instance v1, Landroid/content/ComponentName;

    const-class v0, Lcom/google/android/gms/icing/service/PowerConnectedReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    const-string v4, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-direct {p0, v4}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public final b(Z)V
    .locals 15

    sget-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->b:Ljava/lang/String;

    const-wide/32 v1, 0x5265c00

    invoke-static {p0, v0, v1, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->c:Ljava/lang/String;

    const-wide/32 v1, 0x6ddd00

    invoke-static {p0, v0, v1, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v9

    sget-object v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->d:Ljava/lang/String;

    const-wide/32 v1, 0x36ee80

    invoke-static {p0, v0, v1, v2}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v11

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-eqz p1, :cond_0

    const-wide/32 v1, 0x927c0

    move-wide v2, v1

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "android_id"

    invoke-static {v6, v7}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    const-string v7, "MD5"

    invoke-static {v7}, Lell;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    move-object v8, v1

    :goto_1
    if-eqz v8, :cond_2

    array-length v1, v8

    const/16 v6, 0x8

    if-lt v1, v6, :cond_2

    const/4 v1, 0x7

    aget-byte v1, v8, v1

    and-int/lit8 v1, v1, 0x7f

    int-to-long v6, v1

    const/4 v1, 0x6

    :goto_2
    if-ltz v1, :cond_1

    const/16 v13, 0x8

    shl-long/2addr v6, v13

    aget-byte v13, v8, v1

    and-int/lit16 v13, v13, 0xff

    int-to-long v13, v13

    add-long/2addr v6, v13

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_0
    const-wide/16 v1, 0x0

    move-wide v2, v1

    goto :goto_0

    :cond_1
    rem-long/2addr v6, v11

    :goto_3
    add-long/2addr v6, v9

    const/4 v1, 0x0

    invoke-static {v6, v7, v2, v3}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(JJ)J

    move-result-wide v2

    const-string v6, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    invoke-direct {p0, v6}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    return-void

    :cond_2
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    long-to-int v6, v11

    invoke-virtual {v1, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-long v6, v1

    goto :goto_3

    :cond_3
    move-object v8, v1

    goto :goto_1
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lens;

    invoke-virtual {v0}, Lens;->c()Lejm;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lejm;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "%s: Binding with intent %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "%s: IndexWorkerService onCreate"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const-string v0, "main"

    invoke-static {v0, p0}, Lens;->a(Ljava/lang/String;Landroid/app/Service;)Lens;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lens;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "%s: IndexWorkerService onDestroy"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lens;

    invoke-virtual {v0}, Lens;->a()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    const-string v0, "%s: IndexWorkerService: onStartCommand with %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lens;

    invoke-virtual {v0}, Lens;->b()Lemz;

    move-result-object v0

    new-instance v1, Leni;

    invoke-direct {v1, p0, p1}, Leni;-><init>(Lcom/google/android/gms/icing/service/IndexWorkerService;Landroid/content/Intent;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    const-string v0, "%s: Unbind"

    const-string v1, "main"

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    return v0
.end method
