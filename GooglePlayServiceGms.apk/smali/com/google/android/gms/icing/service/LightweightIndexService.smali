.class public final Lcom/google/android/gms/icing/service/LightweightIndexService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lemu;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    new-instance v0, Lenq;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/LightweightIndexService;->a:Lemu;

    invoke-direct {v0, p0, v1}, Lenq;-><init>(Landroid/content/Context;Lemu;)V

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lemu;

    invoke-direct {v0, p0}, Lemu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/service/LightweightIndexService;->a:Lemu;

    return-void
.end method

.method public final onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/icing/service/LightweightIndexService;->a:Lemu;

    invoke-virtual {v0}, Lemu;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/LightweightIndexService;->a:Lemu;

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
