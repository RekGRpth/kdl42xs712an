.class public Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static final a:Lffd;

.field private static final b:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lffs;

    invoke-direct {v0}, Lffs;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->a:Lffd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "PeopleIRP"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    sget-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffb;

    if-nez v0, :cond_0

    const-string v0, "PeopleIRP"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Lfdk;->f(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lffb;->b()V

    goto :goto_0
.end method
