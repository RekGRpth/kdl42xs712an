.class public Lcom/google/android/gms/people/profile/AvatarPreviewActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private n:Landroid/net/Uri;

.field private o:Lcom/google/android/gms/people/profile/AvatarView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method private e()V
    .locals 2

    const v0, 0x7f0b01b9    # com.google.android.gms.R.string.people_avatar_error

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    goto :goto_0

    :pswitch_1
    const-string v0, "cropped-avatar.jpg"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lfei;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "People.Avatar"

    const-string v1, "Failed to get temp file to crop photo"

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->e()V

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->o:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->finish()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "People.Avatar"

    const-string v2, "Failed to write cropped photo"

    invoke-static {v1, v2, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->e()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a007b
        :pswitch_0    # com.google.android.gms.R.id.cancel_button
        :pswitch_1    # com.google.android.gms.R.id.accept_button
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->n:Landroid/net/Uri;

    :goto_0
    const v0, 0x7f040036    # com.google.android.gms.R.layout.avatar_preview_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->setContentView(I)V

    const v0, 0x7f0a00c6    # com.google.android.gms.R.id.avatar_view

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/AvatarView;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->o:Lcom/google/android/gms/people/profile/AvatarView;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->o:Lcom/google/android/gms/people/profile/AvatarView;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->n:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->b(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->o:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/AvatarView;->a()V

    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a007c    # com.google.android.gms.R.id.accept_button

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->n:Landroid/net/Uri;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "People.Avatar"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to initialize AvatarView: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->e()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;->n:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
