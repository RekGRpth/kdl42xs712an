.class public final Lcom/google/android/gms/people/profile/AvatarActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfap;
.implements Lfas;
.implements Lfaw;
.implements Lfek;


# instance fields
.field private final n:Lftz;

.field private o:Lk;

.field private p:Lfaj;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private v:Landroid/net/Uri;

.field private w:Landroid/net/Uri;

.field private x:Landroid/net/Uri;

.field private y:Landroid/net/Uri;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;-><init>(Lftz;)V

    return-void
.end method

.method constructor <init>(Lftz;)V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    iput-object p1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->n:Lftz;

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.profile.EXTRA_ACCOUNT"

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lbmt;

    invoke-direct {v0, p0}, Lbmt;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v0

    sget-object v1, Lbco;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    if-eqz p2, :cond_0

    new-instance v0, Lggl;

    invoke-direct {v0}, Lggl;-><init>()V

    iput-object p2, v0, Lggl;->b:Ljava/lang/String;

    iget-object v2, v0, Lggl;->c:Ljava/util/Set;

    const/16 v3, 0x3e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lggl;->a()Lggk;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v1, v0}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lbmt;

    :cond_0
    invoke-static {p0, v1}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    return-void
.end method

.method private i()V
    .locals 7

    const/4 v6, 0x1

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "People.Avatar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAvatar "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-eqz v0, :cond_1

    move v5, v6

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    iget-object v0, v0, Lfaj;->a:Lfch;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lfch;->a(Lfap;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V

    iput-boolean v6, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    invoke-static {}, Lfeg;->J()Lfeg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lk;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lk;

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    invoke-virtual {v0}, Lag;->d()I

    return-void

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    const v0, 0x7f0b01b9    # com.google.android.gms.R.string.people_avatar_error

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private k()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    return-void
.end method

.method private l()V
    .locals 2

    sget-object v0, Lbcv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->k()V

    return-void
.end method

.method public final a(Lbbo;Landroid/os/ParcelFileDescriptor;)V
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move-object v3, v2

    :goto_0
    if-nez v3, :cond_2

    const-string v0, "People.Avatar"

    const-string v1, "Failed to decode remote photo"

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-static {p2}, Lfba;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    const-string v0, "remote-avatar.jpg"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lfei;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v0, "People.Avatar"

    const-string v1, "Failed to get temp file for remote photo"

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    :cond_3
    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v0, Lfbd;->N:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v3, v2, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    iput-object v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_5
    const-string v2, "People.Avatar"

    const-string v3, "Failed to compress remove photo to temp file"

    invoke-static {v2, v3, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public final a(Lbbo;Lfed;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Lfed;->a()I

    move-result v2

    if-ne v2, v0, :cond_6

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->u:Z

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    invoke-static {}, Lfej;->J()Lfej;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "source_dialog"

    invoke-virtual {v0, v1, v2}, Lfej;->a(Lu;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-eqz v2, :cond_4

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    if-nez v2, :cond_4

    :goto_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->k()V

    goto :goto_1
.end method

.method public final a(Lbbo;Ljava/lang/String;)V
    .locals 3

    const-string v0, "PeopleService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "People.Avatar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAvatarSet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lk;

    invoke-virtual {v0}, Lk;->a()V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.people.profile.EXTRA_AVATAR_URL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lbcv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v2, "2"

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    sget-object v1, Lbct;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    return-void

    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->u:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lfaj;->a(Lfaw;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lfaj;->a(Lfas;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->i()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final f()V
    .locals 4

    const/4 v3, 0x2

    const-string v0, "camera-avatar.jpg"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lfei;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    if-nez v0, :cond_0

    const-string v0, "People.Avatar"

    const-string v1, "Failed to create temp file to take photo"

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "PeopleService"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "People.Avatar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onTakePhoto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "output"

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final h()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    return-void
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, -0x1

    const-string v2, "PeopleService"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "People.Avatar"

    const-string v3, "onActivityResult requestCode=%d resultCode=%d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v6, :cond_8

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v0, "People.Avatar"

    const-string v1, "Empty data returned from pick photo"

    invoke-static {v0, v1}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->j()V

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    goto :goto_0

    :cond_3
    const-string v2, "PeopleService"

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "People.Avatar"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pick photo returned "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    if-eqz v2, :cond_6

    const-string v3, "http"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "https"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_1
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lfaj;->a(Lfas;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    goto/16 :goto_0

    :pswitch_1
    if-ne p2, v6, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    goto/16 :goto_0

    :pswitch_2
    if-ne p2, v6, :cond_a

    const-string v0, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->i()V

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->l()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbov;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "People.Avatar"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {p0}, Lbox;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->q:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    const-string v0, "app_id"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    const-string v0, "page_gaia_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    const-string v0, "owner_loaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->u:Z

    const-string v0, "take_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    const-string v0, "pick_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    const-string v0, "remote_pick_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    const-string v0, "cropped_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    const-string v0, "result_pending"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "com.google.android.gms.people.profile.EXTRA_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    const-string v1, "com.google.android.gms.people.profile.EXTRA_PAGE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "People.Avatar"

    const-string v1, "Profile image account name is unspecified"

    invoke-static {v0, v1}, Lfdk;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v0, "People.Avatar"

    const-string v1, "Not allowed"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    if-ne v0, v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v1, "com.google.android.gms.people.profile.EXTRA_APP_ID"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    :cond_5
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->q:Ljava/lang/String;

    invoke-static {v0}, Lfjv;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    if-ne v0, v2, :cond_6

    const-string v0, "People.Avatar"

    const-string v1, "EXTRA_SOCIAL_CLIENT_APP_ID must be set"

    invoke-static {v0, v1}, Lfdk;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->n:Lftz;

    iget v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    iget-object v5, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->q:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    if-nez p1, :cond_0

    sget-object v0, Lbcv;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "app_id"

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "page_gaia_id"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "owner_loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "take_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->v:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "pick_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->w:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "remote_pick_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->x:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "cropped_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->y:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "result_pending"

    iget-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lo;->onStop()V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    :cond_1
    return-void
.end method
