.class public Lcom/google/android/gms/analytics/service/AnalyticsService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final g:Ljava/lang/Object;

.field private static final h:Ljava/util/List;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Z

.field private final c:Z

.field private d:Z

.field private volatile e:Z

.field private f:Z

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Ljava/lang/Runnable;

.field private k:Lahb;

.field private l:Landroid/os/Handler;

.field private m:Lafy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->g:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    sget-object v0, Lagi;->b:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    sget-object v0, Lagi;->y:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    iput-boolean v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->b:Z

    iput-boolean v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->e:Z

    iput-boolean v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->f:Z

    new-instance v0, Lagu;

    invoke-direct {v0, p0}, Lagu;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->i:Landroid/content/BroadcastReceiver;

    new-instance v0, Lagv;

    invoke-direct {v0, p0}, Lagv;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->j:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(Landroid/content/Intent;)V
    .locals 4

    sget-object v2, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    monitor-enter v2

    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0, p0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    monitor-exit v2

    :goto_1
    return-void

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/gms/analytics/service/AnalyticsService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/gms/analytics/service/AnalyticsService;)Lafy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->m:Lafy;

    return-object v0
.end method

.method public static synthetic b()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->g:Ljava/lang/Object;

    return-object v0
.end method

.method private static b(Landroid/content/Intent;)Z
    .locals 5

    const/4 v1, 0x0

    sget-object v3, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0, p0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    sget-object v0, Lcom/google/android/gms/analytics/service/AnalyticsService;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private c()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    const-string v1, "Stopping service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->stopSelf()V

    :cond_1
    return-void
.end method

.method public static synthetic c(Lcom/google/android/gms/analytics/service/AnalyticsService;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->c()V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/gms/analytics/service/AnalyticsService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->e:Z

    return v0
.end method

.method public static synthetic e(Lcom/google/android/gms/analytics/service/AnalyticsService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->f:Z

    return v0
.end method

.method public static synthetic f(Lcom/google/android/gms/analytics/service/AnalyticsService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lahb;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final declared-synchronized a(ZZ)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->f:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/analytics/service/AnalyticsService;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/service/AnalyticsService;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    sget-object v0, Lagi;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "PowerSaveMode "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_4

    if-nez p2, :cond_6

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iput-boolean p1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->f:Z

    iput-boolean p2, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_6
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    const-string v1, "onBind called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "com.google.android.gms.analytics.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/analytics/service/AnalyticsService;->a(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->d:Z

    new-instance v0, Lagy;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->getApplicationContext()Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    invoke-direct {v0, p0, v1}, Lagy;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;Lahb;)V

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 5

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    const-string v1, "The AnalyticsService was created."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;)V

    new-instance v1, Lagq;

    new-instance v2, Lahc;

    invoke-direct {v2, p0}, Lahc;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;)V

    invoke-direct {v1, v0, v2, p0}, Lagq;-><init>(Lafz;Lagf;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->m:Lafy;

    new-instance v0, Lahb;

    invoke-direct {v0, p0, p0}, Lahb;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    invoke-virtual {v0}, Lahb;->start()V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lagi;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/service/AnalyticsService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->e:Z

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lagx;

    invoke-direct {v2, p0}, Lagx;-><init>(Lcom/google/android/gms/analytics/service/AnalyticsService;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/service/AnalyticsService;->g:Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    sget-object v0, Lagi;->h:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v0, "com.google.android.gms.analytics.service.START"

    invoke-static {v0}, Lbox;->f(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    const-string v1, "The AnalyticsService was destroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    invoke-virtual {v0}, Lahb;->a()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->k:Lahb;

    invoke-virtual {v0}, Lahb;->interrupt()V

    sget-object v0, Lagi;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    const-string v1, "onRebind called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "com.google.android.gms.analytics.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/analytics/service/AnalyticsService;->a(Landroid/content/Intent;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->d:Z

    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->c:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "onUnbind called for intent "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "com.google.android.gms.analytics.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/analytics/service/AnalyticsService;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/google/android/gms/analytics/service/AnalyticsService;->d:Z

    invoke-direct {p0}, Lcom/google/android/gms/analytics/service/AnalyticsService;->c()V

    :cond_1
    return v3

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
