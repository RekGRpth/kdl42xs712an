.class public Lcom/google/android/gms/wallet/ow/RootActivity;
.super Lo;
.source "SourceFile"

# interfaces
.implements Lguw;
.implements Lgwv;
.implements Lgyf;
.implements Lhbp;


# instance fields
.field n:Lgwr;

.field o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private q:Landroid/accounts/Account;

.field private r:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/ow/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 3

    if-nez p2, :cond_0

    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    :cond_1
    const-string v0, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1, v0}, Lgrl;->a(Ljava/lang/String;)Lgrl;

    :cond_2
    const-string v0, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    iget-object v1, v1, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/RootActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->finish()V

    return-void
.end method

.method private b(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {v0, v1, p1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    return-void
.end method

.method private g()V
    .locals 9

    const/4 v7, -0x1

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v0, v1, Lhar;

    if-eqz v0, :cond_8

    move-object v0, v1

    check-cast v0, Lhar;

    invoke-virtual {v0}, Lhar;->J()I

    move-result v6

    invoke-virtual {v0}, Lhar;->K()I

    move-result v7

    :goto_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {p0}, Lhhh;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->r:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->r:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-static {v1}, Lhgi;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgyd;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lgyd;

    move-result-object v0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT"

    const-class v4, Lioj;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v2

    check-cast v2, Lioj;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.gms.wallet.FULL_WALLET_REQUEST"

    const-class v5, Ljau;

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v3

    check-cast v3, Ljau;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lhao;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lioj;Ljau;ZLjava/lang/String;)Lhao;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->h()V

    goto :goto_2

    :cond_5
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v8, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v5, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v7}, Lhar;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lhar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    :cond_6
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lhbb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.CREATE_WALLET_OBJECTS_REQUEST"

    const-class v2, Ljas;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljas;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.WOBS_ISSUER_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.wallet.WOBS_PROGRAM_NAME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-static {v3, v4, v0, v1, v2}, Lhbb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljas;Ljava/lang/String;Ljava/lang/String;)Lhbb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(Landroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    :cond_8
    move v6, v7

    goto/16 :goto_0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v1}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {v1, v0}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    goto :goto_0

    :cond_0
    const-string v0, "RootActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-static {p1, v0}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_INSTRUMENT_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.EXTRA_SELECTED_ADDRESS_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    goto :goto_0
.end method

.method public final a(Ljau;Lioj;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(Landroid/accounts/Account;)V
    .locals 0

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->h()V

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v1}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040126    # com.google.android.gms.R.layout.wallet_activity_simple_dialog

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->setContentView(I)V

    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/RootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->r:Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Lguw;)V

    :goto_0
    if-eqz p1, :cond_3

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->r:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    iget-object v0, p0, Lo;->b:Lw;

    const v1, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    invoke-virtual {v0, v1}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/RootActivity;->g()V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->o:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Lguw;)V

    goto :goto_0

    :cond_3
    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->p:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onlinewallet_root_activity"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lo;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->n:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->q:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/RootActivity;->r:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method
