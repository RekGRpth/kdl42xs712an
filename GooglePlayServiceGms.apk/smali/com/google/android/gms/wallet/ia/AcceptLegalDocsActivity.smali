.class public Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgwv;


# static fields
.field private static final s:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lgyi;

.field private D:I

.field private final E:Lhcb;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/ProgressBar;

.field p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field q:Lgwr;

.field r:Lgwr;

.field private t:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

.field private v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

.field private z:Lipg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "legalDocs"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->s:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lgxn;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->A:Z

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    new-instance v0, Lgzs;

    invoke-direct {v0, p0}, Lgzs;-><init>(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->E:Lhcb;

    return-void
.end method

.method private a(Lgwr;Ljava/lang/String;)Lgwr;
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v1, p0, Lo;->b:Lw;

    invoke-virtual {v0, v1, p2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lipg;)Lipg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V
    .locals 5

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b011b    # com.google.android.gms.R.string.wallet_terms_of_service

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgth;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b012d    # com.google.android.gms.R.string.wallet_privacy_policy_link

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b0139    # com.google.android.gms.R.string.wallet_privacy_policy_display_text

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lgth;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b013a    # com.google.android.gms.R.string.wallet_tos_and_privacy_format

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b(Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Landroid/widget/TextView;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lipg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    return-object v0
.end method

.method private g()V
    .locals 2

    new-instance v0, Lipf;

    invoke-direct {v0}, Lipf;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lhab;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lioy;

    move-result-object v1

    iput-object v1, v0, Lipf;->b:[Lioy;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Lipf;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b(Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->t:Ljava/lang/String;

    iput-object v1, v0, Lipf;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private h()V
    .locals 2

    new-instance v0, Lipc;

    invoke-direct {v0}, Lipc;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v1}, Lhab;->a([Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;)[Lioy;

    move-result-object v1

    iput-object v1, v0, Lipc;->c:[Lioy;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->c()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lipc;->b:[Ljava/lang/String;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    invoke-interface {v1, v0}, Lhca;->a(Lipc;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b(Z)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->t:Ljava/lang/String;

    iput-object v1, v0, Lipc;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public static synthetic h(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->r:Lgwr;

    const-string v1, "inapp.AcceptLegalDocsActivity.EnrollWithBrokerNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->r:Lgwr;

    return-void
.end method

.method private i()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->C:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->C:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->C:Lgyi;

    return-object v0
.end method

.method public static synthetic i(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lgwr;

    const-string v1, "inapp.AcceptLegalDocsActivity.LegalDocsNetworkErrorDialog"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lgwr;Ljava/lang/String;)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lgwr;

    return-void
.end method

.method private j()V
    .locals 2

    iget v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->E:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    :cond_0
    return-void
.end method

.method public static synthetic j(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(ILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_0
    const-string v3, "Activity requires brokerId extra!"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.brokerId"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->t:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.brokerAndRelationships"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v3, v0

    if-lez v3, :cond_1

    array-length v3, v0

    const-class v4, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;ILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->u:[Lcom/google/android/gms/wallet/shared/BrokerAndRelationships;

    :cond_1
    const-string v0, "legalDocsForCountry"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v2, Lgyr;->b:Lgyu;

    invoke-static {p0, v0, v2}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f04011c    # com.google.android.gms.R.layout.wallet_activity_accept_legal_docs

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->setContentView(I)V

    const v0, 0x7f0a02e2    # com.google.android.gms.R.id.legal_doc_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->n:Landroid/widget/TextView;

    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->o:Landroid/widget/ProgressBar;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-eqz p1, :cond_4

    const-string v0, "pendingEnrollRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->A:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    const-string v0, "legalDocumentsResponse"

    const-class v2, Lipg;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipg;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v0, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->C:Lgyi;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->C:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "accept_legal_docs"

    invoke-static {v0, v2, v3}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lgxn;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lgxn;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->E:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.AcceptLegalDocsActivity.LegalDocsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lgwr;

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "inapp.AcceptLegalDocsActivity.EnrollWithBrokerNetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->r:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lgwr;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->q:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->r:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->r:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->B:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->E:Lhcb;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    invoke-virtual {v0, v1}, Lhcb;->a(Lipg;)V

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->A:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->v:Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingEnrollRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    if-eqz v0, :cond_0

    const-string v0, "legalDocumentsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->z:Lipg;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    :cond_0
    return-void
.end method
