.class public Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    const-class v0, Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    const-string v0, "ConnectivityChangeReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setEnabled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    const-class v0, Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;

    invoke-static {p1, v0}, Lbox;->a(Landroid/content/Context;Ljava/lang/Class;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const-string v0, "ConnectivityChangeReceiver"

    const-string v1, "Receiver disabled, exiting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v0, "noConnectivity"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lhhh;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ConnectivityChangeReceiver"

    const-string v1, "Connected to network, updating cache..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, v2}, Lcom/google/android/gms/wallet/receiver/ConnectivityChangeBroadcastReceiver;->a(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_1
    const-string v0, "ConnectivityChangeReceiver"

    const-string v1, "Roaming or no connectivity"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
