.class public Lcom/google/android/gms/wallet/common/PaymentModel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Z

.field public b:Lioj;

.field public c:Lipv;

.field public d:Ljai;

.field public e:Z

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I

.field public i:Lioj;

.field public j:Lipv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgtg;

    invoke-direct {v0}, Lgtg;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/PaymentModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    iput v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lioj;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lipv;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lioj;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lipv;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/PaymentModel;->d:Ljai;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;Landroid/os/Parcel;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
