.class public Lcom/google/android/gms/wallet/common/ui/TosActivity;
.super Lo;
.source "SourceFile"


# instance fields
.field n:Landroid/view/ViewGroup;

.field private o:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lo;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "legalDocs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    const-class v2, Lcom/google/android/gms/wallet/common/ui/TosActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v3, "Activity requires buyFlowConfig extra!"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v3, "legalDocs"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "Activity requires legalDocs extra"

    invoke-static {v3, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v3, "legalDocs"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;

    sget-object v3, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v3}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040127    # com.google.android.gms.R.layout.wallet_activity_tos

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->setContentView(I)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->o:Landroid/view/LayoutInflater;

    const v0, 0x7f0a0313    # com.google.android.gms.R.id.tos_container

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/LegalDocsForCountry;->b()[Ljbe;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    if-eqz v3, :cond_1

    array-length v0, v3

    if-lez v0, :cond_1

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    iget-object v5, v0, Ljbe;->b:Ljava/lang/String;

    iget-object v6, v0, Ljbe;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->o:Landroid/view/LayoutInflater;

    const v7, 0x7f040144    # com.google.android.gms.R.layout.wallet_row_legal_doc_item

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v0, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/LinkButton;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/LinkButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/wallet/common/ui/LinkButton;->a(Ljava/lang/String;)V

    const v0, 0x7f0a034b    # com.google.android.gms.R.id.separator

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->n:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TosActivity;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    const v0, 0x7f0a0310    # com.google.android.gms.R.id.bar_icon_and_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c002e    # com.google.android.gms.R.color.wallet_top_bar_background

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundColor(I)V

    const v0, 0x7f0a0312    # com.google.android.gms.R.id.top_bar_separator

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TosActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method
