.class public Lcom/google/android/gms/wallet/common/ui/TopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/view/View;

.field c:Landroid/widget/TextView;

.field d:I

.field e:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

.field f:Lcom/google/android/gms/wallet/common/ui/AccountSelector;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setOrientation(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d001e    # com.google.android.gms.R.dimen.wallet_spacing_tight

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002e    # com.google.android.gms.R.color.wallet_top_bar_background

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setBackgroundColor(I)V

    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040164    # com.google.android.gms.R.layout.wallet_view_top_bar

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f0a0379    # com.google.android.gms.R.id.bar_title_and_email

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    const v0, 0x7f0a0377    # com.google.android.gms.R.id.bar_logo_and_email

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/view/View;

    const v0, 0x7f0a0311    # com.google.android.gms.R.id.bar_title

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:Landroid/widget/TextView;

    const v0, 0x7f0a0378    # com.google.android.gms.R.id.logo_account_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->e:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    const v0, 0x7f0a037a    # com.google.android.gms.R.id.title_account_selector

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->f:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setOrientation(I)V

    goto :goto_0
.end method

.method private b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->e:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->f:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->d:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final a(I)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->d:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    :cond_0
    return-void
.end method

.method public final a(Lguw;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    invoke-static {}, Lgry;->a()Lgry;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lgry;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lgth;->a([Landroid/accounts/Account;)[Lguu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lguu;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lguw;)V

    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->b()Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/os/Bundle;

    const-string v0, "instanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "instanceState"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "titleId"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/TopBarView;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method
