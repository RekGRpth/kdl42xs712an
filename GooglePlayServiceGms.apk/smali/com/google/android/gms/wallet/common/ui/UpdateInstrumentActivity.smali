.class public Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;
.super Lgxn;
.source "SourceFile"

# interfaces
.implements Lgwv;
.implements Lgyq;


# static fields
.field private static final v:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lioq;

.field private C:Z

.field private D:Lgyi;

.field private E:I

.field private F:I

.field private G:Lioj;

.field private final H:Lhcb;

.field n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field q:Landroid/widget/ProgressBar;

.field r:Lgwr;

.field public s:Landroid/widget/TextView;

.field public t:Lgxf;

.field u:Lgwr;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "updateInstrument"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgxn;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->F:I

    new-instance v0, Lgym;

    invoke-direct {v0, p0}, Lgym;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->H:Lhcb;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Z)V

    new-instance v0, Lipp;

    invoke-direct {v0}, Lipp;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    iget-object v1, v1, Lioj;->a:Ljava/lang/String;

    iput-object v1, v0, Lipp;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v1}, Lgxf;->a()Linv;

    move-result-object v1

    iput-object v1, v0, Lipp;->c:Linv;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->A:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->A:Ljava/lang/String;

    iput-object v1, v0, Lipp;->d:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->B:Lioq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->B:Lioq;

    iput-object v1, v0, Lipp;->a:Lioq;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j()Lgyi;

    move-result-object v1

    invoke-virtual {v1}, Lgyi;->a()Lhca;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    invoke-interface {v1, v0, v2}, Lhca;->a(Lipp;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Lioj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->s:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    if-nez p1, :cond_2

    move v0, v1

    :goto_2
    invoke-interface {v3, v0}, Lgxf;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    if-nez p1, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    :goto_4
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->C:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    if-nez p1, :cond_5

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method

.method public static synthetic c(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->A:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->k()V

    return-void
.end method

.method public static synthetic e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-static {}, Lgwr;->J()Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "UpdateInstrumentActivit.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic f(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Z)V

    return-void
.end method

.method public static synthetic g(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "UpdateInstrumentActivit.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method private j()Lgyi;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    return-object v0
.end method

.method private k()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->s:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->R_()Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->S_()Z

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    const-string v0, "UpdateInstrumentActivit"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public finish()V
    .locals 2

    invoke-super {p0}, Lgxn;->finish()V

    const/4 v0, 0x0

    const v1, 0x7f050010    # com.google.android.gms.R.anim.wallet_push_down_out

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const v7, 0x7f0a01af    # com.google.android.gms.R.id.fragment_holder

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lgxn;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Activity requires instrument extra!"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v4, Lioj;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    iget v0, v0, Lioj;->d:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    const-string v4, "Only credit card updates are currently supported"

    invoke-static {v0, v4}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->A:Ljava/lang/String;

    const-string v0, "com.google.android.gms.wallet.accountReference"

    const-class v4, Lioq;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioq;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->B:Lioq;

    const-string v0, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v4, Lgyr;->a:Lgyu;

    invoke-static {p0, v0, v4}, Lgyr;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lgyu;)V

    const v0, 0x7f040129    # com.google.android.gms.R.layout.wallet_activity_update_instrument

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    const-string v0, "errorMessageResourceId"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    :cond_0
    const v0, 0x7f0a02e6    # com.google.android.gms.R.id.top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const v4, 0x7f0b015e    # com.google.android.gms.R.string.wallet_update_card_title

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    :goto_1
    const v0, 0x7f0a033b    # com.google.android.gms.R.id.butter_bar_text

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->s:Landroid/widget/TextView;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(I)V

    :goto_2
    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Landroid/widget/ProgressBar;

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    new-instance v4, Lgyl;

    invoke-direct {v4, p0}, Lgyl;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0, v7}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgxf;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    if-nez v0, :cond_6

    const-string v0, "com.google.android.gms.wallet.addressHints"

    const-class v4, Lipv;

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v4

    const-string v0, "com.google.android.gms.wallet.baseAddress"

    const-class v5, Lipv;

    invoke-static {v3, v0, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lipv;

    const-string v5, "com.google.android.gms.wallet.requireAddressUpgrade"

    invoke-virtual {v3, v5, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    invoke-static {v6, v5, v2, v4, v0}, Lgyn;->a(Lioj;ZZLjava/util/Collection;Lipv;)Lgyn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    :goto_3
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v7, v0}, Lag;->a(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j()Lgyi;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->z:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v0, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    :goto_4
    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    const v0, 0x7f0a02e7    # com.google.android.gms.R.id.payment_form_top_bar

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const v4, 0x7f0b0147    # com.google.android.gms.R.string.wallet_local_update_card_title

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->k()V

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->G:Lioj;

    invoke-static {v0}, Lgxm;->a(Lioj;)Lgxm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    goto :goto_3

    :cond_7
    const-string v0, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->x:Landroid/accounts/Account;

    invoke-static {v1, v2, v0}, Lgyi;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->D:Lgyi;

    goto :goto_4
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lgxn;->onPause()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->b(Lhcb;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lgxn;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->C:Z

    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->F:I

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->C:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Z)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->w:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "update_instrument"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lgxn;->onResume()V

    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "UpdateInstrumentActivit.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->r:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    iget-object v0, p0, Lo;->b:Lw;

    const-string v1, "UpdateInstrumentActivit.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->u:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->H:Lhcb;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->F:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lgxn;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j()Lgyi;

    move-result-object v0

    invoke-virtual {v0}, Lgyi;->a()Lhca;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->H:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->F:I

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->F:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->C:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->E:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
