.class public Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;
.super Lbxe;
.source "SourceFile"


# instance fields
.field private p:Lcbo;

.field private q:Lcos;

.field private r:Lbuc;

.field private s:Lcfc;

.field private t:Lcom/google/android/gms/drive/DriveId;

.field private final u:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbxe;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->u:Landroid/os/Handler;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)Landroid/content/Intent;
    .locals 2

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.gms.drive.data.ui.open.CreateFolderActivity.CREATE_DOC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "driveId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcfc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->s:Lcfc;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->a(Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V
    .locals 3

    invoke-static {p1}, Lbya;->a(Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)Lbya;

    move-result-object v0

    invoke-virtual {v0}, Lbya;->q()V

    iget-object v1, p0, Lo;->b:Lw;

    const-string v2, "editTitleDialog"

    invoke-virtual {v0, v1, v2}, Lbya;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->t:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lbuc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->r:Lbuc;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcbo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->p:Lcbo;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcos;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->q:Lcos;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V
    .locals 9

    iget v0, p2, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lbxw;

    const-string v2, "Create new entry"

    move-object v1, p0

    move-object v5, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lbxw;-><init>(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;Ljava/lang/String;Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicReference;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->u:Landroid/os/Handler;

    new-instance v1, Lbxv;

    invoke-direct {v1, p0}, Lbxv;-><init>(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    invoke-super {p0, p1, p2, p3}, Lbxe;->onActivityResult(IILandroid/content/Intent;)V

    const-string v0, "CreateFolderActivity"

    const-string v1, "Unexpected activity request code: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lbxe;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcco;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->o:Lcoy;

    invoke-direct {v0, v1}, Lcco;-><init>(Lcoy;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->p:Lcbo;

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->o:Lcoy;

    invoke-virtual {v0}, Lcoy;->b()Lcos;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->q:Lcos;

    new-instance v0, Lbuc;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->o:Lcoy;

    invoke-direct {v0, v1}, Lbuc;-><init>(Lcoy;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->r:Lbuc;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "driveId"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->t:Lcom/google/android/gms/drive/DriveId;

    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v2, "CreateFolderActivity"

    const-string v3, "Account name is not specified in the intent."

    invoke-static {v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->finish()V

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->o:Lcoy;

    invoke-virtual {v2}, Lcoy;->f()Lcfz;

    move-result-object v2

    invoke-interface {v2, v0}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->s:Lcfc;

    const-string v0, "com.google.android.gms.drive.data.ui.open.CreateFolderActivity.CREATE_DOC"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->n:Lbtd;

    const-string v1, "/createNewDoc"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbtd;->a(Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v0, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    const-string v1, "application/vnd.google-apps.folder"

    const v2, 0x7f0b0023    # com.google.android.gms.R.string.drive_create_new_folder

    const v3, 0x7f0b0027    # com.google.android.gms.R.string.drive_default_new_folder_title

    const v4, 0x7f0b0052    # com.google.android.gms.R.string.drive_new_folder_title

    const v5, 0x7f0b0025    # com.google.android.gms.R.string.drive_creating_folder

    const v6, 0x7f0b0024    # com.google.android.gms.R.string.drive_create_new_folder_error

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;-><init>(Ljava/lang/String;IIIII)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->a(Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->finish()V

    goto :goto_0
.end method
