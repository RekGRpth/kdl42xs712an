.class public Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;
.super Ljp;
.source "SourceFile"


# instance fields
.field private o:Lcoy;

.field private p:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->p:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)Lcoy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->o:Lcoy;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->o:Lcoy;

    const v0, 0x7f040053    # com.google.android.gms.R.layout.drive_settings

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->setContentView(I)V

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    const v0, 0x7f0a0110    # com.google.android.gms.R.id.sync_preference

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0b0060    # com.google.android.gms.R.string.drive_prefs_sync_over_wifi_only_title

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f0a0082    # com.google.android.gms.R.id.summary

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0b0061    # com.google.android.gms.R.string.drive_prefs_sync_over_wifi_only_summary

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->p:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->p:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;->o:Lcoy;

    invoke-virtual {v2}, Lcoy;->i()Lcby;

    move-result-object v2

    invoke-interface {v2}, Lcby;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v1, Lbxr;

    invoke-direct {v1, p0}, Lbxr;-><init>(Lcom/google/android/gms/drive/data/ui/DriveSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
