.class public final Lfre;
.super Lfqq;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lfrc;


# instance fields
.field private ac:Landroid/widget/CheckBox;

.field private ad:Landroid/view/View;

.field private ae:Ljava/util/HashSet;

.field private af:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfqq;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfre;->ae:Ljava/util/HashSet;

    return-void
.end method

.method private V()V
    .locals 3

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v0

    new-instance v1, Lbkw;

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v2

    iget-object v2, v2, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v1, v2}, Lbkw;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iget-object v2, p0, Lfre;->ae:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v1

    invoke-virtual {v1}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private W()V
    .locals 2

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v0

    new-instance v1, Lbkw;

    invoke-direct {v1}, Lbkw;-><init>()V

    invoke-virtual {v1}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lfrb;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0}, Lfre;->X()V

    return-void
.end method

.method private X()V
    .locals 5

    const v4, 0x7f020203    # com.google.android.gms.R.drawable.plus_icn_arrow_up

    const/4 v1, 0x0

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->c(Z)V

    invoke-virtual {p0}, Lfre;->a()Landroid/widget/ListView;

    move-result-object v2

    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    const v3, 0x7f0b0393    # com.google.android.gms.R.string.plus_auth_hide_circles_label

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const/16 v0, 0x10

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v4, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    :goto_0
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lfre;->ad:Landroid/view/View;

    if-eq v3, v4, :cond_0

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v4, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private Y()V
    .locals 5

    const v4, 0x7f020202    # com.google.android.gms.R.drawable.plus_icn_arrow_down

    const/4 v1, 0x0

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->c(Z)V

    invoke-virtual {p0}, Lfre;->a()Landroid/widget/ListView;

    move-result-object v2

    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    const v3, 0x7f0b0392    # com.google.android.gms.R.string.plus_auth_see_circles_label

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const/16 v0, 0x10

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v4, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    :goto_0
    move v0, v1

    :goto_1
    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lfre;->ad:Landroid/view/View;

    if-eq v1, v3, :cond_0

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v4, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic a(Lfre;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic a(Lfre;Ljava/util/HashSet;)Ljava/util/HashSet;
    .locals 0

    iput-object p1, p0, Lfre;->ae:Ljava/util/HashSet;

    return-object p1
.end method

.method static synthetic b(Lfre;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lfre;->ae:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic c(Lfre;)V
    .locals 0

    invoke-direct {p0}, Lfre;->V()V

    return-void
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Lfqq;->E_()V

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    return-void
.end method

.method protected final J()Landroid/view/View;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lfre;->ad:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400c2    # com.google.android.gms.R.layout.plus_audience_selection_facl_header

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lfre;->ad:Landroid/view/View;

    iget-object v1, p0, Lfre;->ad:Landroid/view/View;

    const v2, 0x7f0a025f    # com.google.android.gms.R.id.audience_select_all_checkbox

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lfre;->ac:Landroid/widget/CheckBox;

    iget-object v1, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v1, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->n()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v1, p0, Lfre;->ad:Landroid/view/View;

    const v2, 0x7f0a0261    # com.google.android.gms.R.id.see_circles_label

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lfre;->af:Landroid/widget/TextView;

    iget-object v1, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lfre;->af:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setClickable(Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lfre;->Y()V

    :goto_0
    iget-object v1, p0, Lfre;->ad:Landroid/view/View;

    const v2, 0x7f0a0092    # com.google.android.gms.R.id.description

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v0, p0, Lfre;->ad:Landroid/view/View;

    const v1, 0x7f0a025d    # com.google.android.gms.R.id.audience_select_all

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lfre;->ad:Landroid/view/View;

    return-object v0

    :cond_1
    invoke-direct {p0}, Lfre;->X()V

    goto :goto_0
.end method

.method protected final K()Lcom/google/android/gms/plus/audience/FaclSelectionActivity;
    .locals 1

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    return-object v0
.end method

.method protected final synthetic M()Landroid/widget/BaseAdapter;
    .locals 1

    invoke-virtual {p0}, Lfre;->O()Lfqe;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic N()Lfqr;
    .locals 1

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    return-object v0
.end method

.method protected final O()Lfqe;
    .locals 6

    new-instance v0, Lfrf;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v3

    iget-object v4, p0, Lfqy;->Z:Ljava/lang/String;

    iget-object v5, p0, Lfqy;->aa:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lfrf;-><init>(Lfre;Landroid/content/Context;Lfrb;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lfqq;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "FaclSelectionFragment may only be used by FaclSelectionActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    if-eq p1, p0, :cond_0

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v0

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lfre;->ae:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public final g_()V
    .locals 1

    invoke-virtual {p0}, Lfre;->P()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->b(Lfrc;)V

    invoke-super {p0}, Lfqq;->g_()V

    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->b(Z)V

    invoke-direct {p0}, Lfre;->V()V

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfre;->X()V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a025d    # com.google.android.gms.R.id.audience_select_all

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lfre;->ac:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    invoke-direct {p0}, Lfre;->W()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a025f    # com.google.android.gms.R.id.audience_select_all_checkbox

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lfre;->W()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0261    # com.google.android.gms.R.id.see_circles_label

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lfqq;->N()Lfqr;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/FaclSelectionActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lfre;->X()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lfre;->Y()V

    goto :goto_0
.end method
