.class public abstract Lfrn;
.super Lbod;
.source "SourceFile"

# interfaces
.implements Lfar;


# static fields
.field protected static final c:Lbbo;

.field protected static final d:Lbbo;


# instance fields
.field private final b:Lftz;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:I

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lbbo;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    sput-object v0, Lfrn;->c:Lbbo;

    new-instance v0, Lbbo;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v2}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    sput-object v0, Lfrn;->d:Lbbo;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    sget-object v7, Lftx;->a:Lftz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lfrn;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILftz;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILftz;)V
    .locals 0

    invoke-direct {p0, p1}, Lbod;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lfrn;->e:Ljava/lang/String;

    iput-object p3, p0, Lfrn;->f:Ljava/lang/String;

    iput-object p4, p0, Lfrn;->h:Ljava/lang/String;

    iput-object p5, p0, Lfrn;->i:Ljava/lang/String;

    iput p6, p0, Lfrn;->j:I

    iput-object p7, p0, Lfrn;->b:Lftz;

    return-void
.end method

.method private d()V
    .locals 4

    iget-boolean v0, p0, Lfrn;->k:Z

    if-nez v0, :cond_0

    iget v0, p0, Lfrn;->j:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfrn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbod;->a:Lbbq;

    check-cast v0, Lfaj;

    iget-object v1, p0, Lfrn;->e:Ljava/lang/String;

    iget-object v2, p0, Lfrn;->f:Ljava/lang/String;

    iget v3, p0, Lfrn;->j:I

    invoke-virtual {v0, p0, v1, v2, v3}, Lfaj;->a(Lfar;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lfrn;->k:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    invoke-super {p0}, Lbod;->P_()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrn;->k:Z

    return-void
.end method

.method public final Q_()V
    .locals 0

    invoke-virtual {p0}, Lfrn;->k()V

    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Lbbr;Lbbs;)Lbbq;
    .locals 6

    iget-object v0, p0, Lfrn;->b:Lftz;

    iget-object v1, p0, Lfrn;->h:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, Lfrn;->i:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lfrn;
    .locals 0

    iput-object p1, p0, Lfrn;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lfrn;->k()V

    return-object p0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lbod;->b_(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lfrn;->d()V

    return-void
.end method

.method protected final f()V
    .locals 1

    invoke-virtual {p0}, Lfrn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfrn;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbod;->a:Lbbq;

    check-cast v0, Lfaj;

    invoke-virtual {v0, p0}, Lfaj;->a(Lfar;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrn;->k:Z

    invoke-super {p0}, Lbod;->f()V

    return-void
.end method

.method protected final m_()V
    .locals 0

    invoke-super {p0}, Lbod;->m_()V

    invoke-direct {p0}, Lfrn;->d()V

    return-void
.end method
