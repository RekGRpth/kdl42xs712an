.class public final Lfrm;
.super Lfrn;
.source "SourceFile"

# interfaces
.implements Lfaw;


# instance fields
.field b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lfrn;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrm;->b:Z

    invoke-super {p0}, Lfrn;->a()V

    return-void
.end method

.method public final a(Lbbo;Lfed;)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lfrm;->a(Lbbo;Lbgo;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lfed;->a()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_2

    sget-object v0, Lfrm;->d:Lbbo;

    invoke-virtual {p0, v0, v1}, Lfrm;->a(Lbbo;Lbgo;)V

    invoke-virtual {p2}, Lfed;->b()V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1, p2}, Lfrm;->a(Lbbo;Lbgo;)V

    goto :goto_0
.end method

.method protected final bridge synthetic a(Lbbq;)V
    .locals 5

    check-cast p1, Lfaj;

    iget-boolean v0, p0, Lfrm;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrm;->b:Z

    iget-object v0, p0, Lfrn;->e:Ljava/lang/String;

    iget-object v1, p0, Lfrn;->f:Ljava/lang/String;

    iget-object v2, p1, Lfaj;->a:Lfch;

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Lfch;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    :cond_0
    iget-object v0, p0, Lfrn;->e:Ljava/lang/String;

    iget-object v1, p0, Lfrn;->f:Ljava/lang/String;

    invoke-virtual {p1, p0, v0, v1}, Lfaj;->a(Lfaw;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
