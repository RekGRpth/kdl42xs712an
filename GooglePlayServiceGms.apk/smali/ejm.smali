.class public final Lejm;
.super Lajf;
.source "SourceFile"


# static fields
.field static final a:Ljava/lang/String;

.field private static o:D

.field private static p:D

.field private static q:D

.field private static final r:Ljava/lang/String;


# instance fields
.field final b:Landroid/content/Context;

.field final c:Lemz;

.field final d:Ljava/util/concurrent/Semaphore;

.field e:Lejk;

.field f:Lelj;

.field g:Leiy;

.field h:Lemv;

.field i:Lelt;

.field j:Lcom/google/android/gms/icing/impl/NativeIndex;

.field final k:Ljava/util/Set;

.field final l:Ljava/util/Set;

.field final m:Leji;

.field final n:Lelw;

.field private final s:Leiv;

.field private final t:Ljava/lang/String;

.field private final u:Lemp;

.field private v:Leky;

.field private final w:Lelx;

.field private final x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide v0, 0x3fa999999999999aL    # 0.05

    sput-wide v0, Lejm;->o:D

    sput-wide v0, Lejm;->p:D

    const-wide v0, 0x3fd3333333333333L    # 0.3

    sput-wide v0, Lejm;->q:D

    const-string v0, "compact_interval_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lejm;->r:Ljava/lang/String;

    const-string v0, "maintenance_force_interval_ms"

    invoke-static {v0}, Lell;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lejm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lemz;Ljava/lang/String;Lemp;Leji;Leiv;)V
    .locals 2

    invoke-direct {p0}, Lajf;-><init>()V

    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejm;->k:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lejm;->l:Ljava/util/Set;

    new-instance v0, Lejn;

    invoke-direct {v0, p0}, Lejn;-><init>(Lejm;)V

    iput-object v0, p0, Lejm;->n:Lelw;

    new-instance v0, Lejw;

    invoke-direct {v0, p0}, Lejw;-><init>(Lejm;)V

    iput-object v0, p0, Lejm;->w:Lelx;

    new-instance v0, Lekh;

    invoke-direct {v0, p0}, Lekh;-><init>(Lejm;)V

    iput-object v0, p0, Lejm;->x:Ljava/lang/Runnable;

    iput-object p1, p0, Lejm;->b:Landroid/content/Context;

    iput-object p2, p0, Lejm;->c:Lemz;

    iput-object p4, p0, Lejm;->u:Lemp;

    iput-object p6, p0, Lejm;->s:Leiv;

    iput-object p5, p0, Lejm;->m:Leji;

    iput-object p3, p0, Lejm;->t:Ljava/lang/String;

    return-void
.end method

.method private A()V
    .locals 2

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c()Lehr;

    move-result-object v0

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1, v0}, Leiy;->a(Lehr;)Z

    iget-object v0, p0, Lejm;->v:Leky;

    invoke-virtual {v0}, Leky;->a()V

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v1, "index_rebuilt"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    return-void
.end method

.method private B()I
    .locals 4

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v0

    sget-wide v2, Lejm;->o:D

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    const-string v0, "Design limits for indexing reached"

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lejm;->f:Lelj;

    invoke-virtual {v0}, Lelj;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lejm;->f:Lelj;

    invoke-virtual {v0}, Lelj;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Not enough disk space for indexing trimmable"

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "Not enough disk space for indexing"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    const/4 v0, 0x2

    goto :goto_0
.end method

.method static synthetic a(Lejm;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lejm;->x:Ljava/lang/Runnable;

    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;
    .locals 5

    if-eqz p1, :cond_1

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v1

    invoke-static {v1}, Leiy;->a(Lehh;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Corpus "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t contain section "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    const-string v0, "Not authorized to read requested corpora"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Found no matching corpora for package"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lelv;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/util/List;
    .locals 5

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->c()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v3, p0, Lejm;->i:Lelt;

    iget-object v4, v0, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lelt;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lehh;->l:[Leim;

    array-length v3, v3

    if-lez v3, :cond_0

    if-eqz p2, :cond_1

    iget-object v3, v0, Lehh;->d:Ljava/lang/String;

    iget-object v4, v0, Lehh;->b:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lelv;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Lelv;)Ljava/util/Set;

    move-result-object v0

    new-instance v3, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    iget-object v0, v0, Lels;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v4, v0, Lehh;->d:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto :goto_1
.end method

.method private a(Lehr;)V
    .locals 2

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, p1}, Leiy;->a(Lehr;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Error running post-flush cleanup"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Failed to commit flush status"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v1, "flush_failed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lejm;Ljava/lang/String;Lehh;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lejm;->a(Ljava/lang/String;Lehh;)V

    return-void
.end method

.method static synthetic a(Lejm;Ljava/util/Set;J)V
    .locals 6

    const-wide/16 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lejm;->a(Ljava/util/Set;JJ)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    .locals 5

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v2, Lejo;

    invoke-direct {v2, p0, v0, p2}, Lejo;-><init>(Lejm;Lely;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    check-cast v0, Lejo;

    invoke-virtual {v0}, Lejo;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    instance-of v1, v0, Lemb;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    instance-of v1, v0, Ljava/lang/SecurityException;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/SecurityException;

    throw v0

    :cond_1
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lehh;)V
    .locals 7

    invoke-virtual {p0}, Lejm;->o()V

    const-wide/16 v3, 0x3e8

    const-wide/32 v5, 0x493e0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lejm;->a(Ljava/lang/String;Lehh;JJ)V

    return-void
.end method

.method private a(Ljava/lang/String;Lehh;JJ)V
    .locals 9

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v4, p2, Lehh;->d:Ljava/lang/String;

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, v4}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lels;->h:Z

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "Not indexing corpus from package %s as it has never connected"

    invoke-static {v0, v4}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_1
    iget v2, p2, Lehh;->a:I

    iget-boolean v5, p2, Lehh;->h:Z

    iget-object v8, p0, Lejm;->c:Lemz;

    new-instance v0, Leke;

    move-object v1, p0

    move-object v3, p1

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Leke;-><init>(Lejm;ILjava/lang/String;Ljava/lang/String;ZJ)V

    invoke-virtual {v8, v0, p3, p4}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0
.end method

.method private a(Ljava/util/Set;JJ)V
    .locals 8

    invoke-virtual {p0}, Lejm;->o()V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v0

    const-string v2, "Indexing for %s"

    invoke-static {v2, v1}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v2, v0, Lehj;->a:Lehh;

    move-object v0, p0

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lejm;->a(Ljava/lang/String;Lehh;JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lehh;JLeix;Lekv;)Z
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lejm;->o()V

    :try_start_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lehh;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-virtual {v0, v13, v1, v2}, Leix;->a(Landroid/net/Uri;J)V

    invoke-static/range {p1 .. p1}, Leiy;->a(Lehh;)Ljava/util/Map;

    move-result-object v14

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v15

    const/4 v12, 0x0

    move-wide/from16 v10, p2

    :goto_0
    :try_start_1
    invoke-virtual/range {p4 .. p4}, Leix;->c()Z

    move-result v3

    if-eqz v3, :cond_18

    invoke-virtual/range {p4 .. p4}, Leix;->a()Leiw;

    move-result-object v3

    invoke-virtual {v3}, Leiw;->b()J

    move-result-wide v6

    invoke-virtual/range {p4 .. p4}, Leix;->b()Leiw;

    move-result-object v3

    invoke-virtual {v3}, Leiw;->b()J

    move-result-wide v3

    invoke-static {v6, v7, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    cmp-long v3, v6, v4

    if-nez v3, :cond_1

    const/4 v3, 0x1

    move v7, v3

    :goto_1
    cmp-long v3, v4, v10

    if-nez v3, :cond_3

    add-int/lit8 v3, v12, 0x1

    const-string v8, "Dup seqno for %s pkg %s %d last %d"

    const/4 v6, 0x4

    new-array v9, v6, [Ljava/lang/Object;

    const/4 v12, 0x0

    if-eqz v7, :cond_2

    const-string v6, "docs"

    :goto_2
    aput-object v6, v9, v12

    const/4 v6, 0x1

    move-object/from16 v0, p1

    iget-object v12, v0, Lehh;->d:Ljava/lang/String;

    aput-object v12, v9, v6

    const/4 v6, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v9, v6

    const/4 v6, 0x3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v9, v6

    invoke-static {v8, v9}, Lehe;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    move v12, v3

    :goto_3
    cmp-long v3, v4, v10

    if-ltz v3, :cond_0

    const/16 v3, 0xa

    if-le v12, v3, :cond_5

    :cond_0
    const-string v6, "Out of order seqno for %s pkg %s %d last %d"

    const/4 v3, 0x4

    new-array v8, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    if-eqz v7, :cond_4

    const-string v3, "docs"

    :goto_4
    aput-object v3, v8, v9

    const/4 v3, 0x1

    move-object/from16 v0, p1

    iget-object v7, v0, Lehh;->d:Ljava/lang/String;

    aput-object v7, v8, v3

    const/4 v3, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v3

    const/4 v3, 0x3

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-static {v6, v8}, Lehe;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v3, "out of order seqno"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v0, p5

    iput-wide v10, v0, Lekv;->c:J

    const/4 v3, 0x0

    :goto_5
    return v3

    :cond_1
    const/4 v3, 0x0

    move v7, v3

    goto :goto_1

    :cond_2
    :try_start_2
    const-string v6, "tags"

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    move v12, v3

    goto :goto_3

    :cond_4
    const-string v3, "tags"

    goto :goto_4

    :cond_5
    if-eqz v7, :cond_12

    cmp-long v3, v4, v10

    if-lez v3, :cond_6

    invoke-virtual/range {p4 .. p4}, Leix;->a()Leiw;

    move-result-object v8

    move-object/from16 v0, p1

    iget v7, v0, Lehh;->a:I

    invoke-virtual/range {p0 .. p0}, Lejm;->o()V

    const-string v3, "action"

    invoke-virtual {v8, v3}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "uri"

    invoke-virtual {v8, v6}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "Uri"

    const/16 v17, 0x100

    move/from16 v0, v17

    invoke-static {v9, v6, v0}, Lekx;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    const-string v3, "bad uri"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    :cond_6
    :goto_6
    invoke-virtual/range {p4 .. p4}, Leix;->a()Leiw;

    move-result-object v3

    invoke-virtual {v3}, Leiw;->c()Z

    move-result v3

    if-nez v3, :cond_7

    move-object/from16 v0, p4

    invoke-virtual {v0, v13, v4, v5}, Leix;->a(Landroid/net/Uri;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_7
    :goto_7
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-wide v6

    sub-long/2addr v6, v15

    const-wide/16 v8, 0x1388

    cmp-long v3, v6, v8

    if-ltz v3, :cond_16

    :goto_8
    move-object/from16 v0, p5

    iput-wide v4, v0, Lekv;->c:J

    const/4 v3, 0x1

    goto :goto_5

    :cond_8
    :try_start_4
    const-string v9, "add"

    invoke-static {v3, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_10

    new-instance v9, Lehm;

    invoke-direct {v9}, Lehm;-><init>()V

    iput-object v6, v9, Lehm;->b:Ljava/lang/String;

    const-string v3, "doc_score"

    const/4 v6, 0x0

    iget-boolean v0, v8, Leiw;->b:Z

    move/from16 v17, v0

    if-eqz v17, :cond_17

    iget-object v0, v8, Leiw;->c:Ljava/util/Map;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_17

    iget-object v6, v8, Leiw;->a:Lele;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v6, v3}, Lele;->b(I)I

    move-result v3

    :goto_9
    iput v3, v9, Lehm;->c:I

    iput v7, v9, Lehm;->a:I

    const-string v3, "created_timestamp"

    const-wide/16 v6, 0x0

    iget-boolean v0, v8, Leiw;->b:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    iget-object v0, v8, Leiw;->c:Ljava/util/Map;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_9

    iget-object v6, v8, Leiw;->a:Lele;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v6, v3}, Lele;->c(I)J

    move-result-wide v6

    :cond_9
    iput-wide v6, v9, Lehm;->f:J

    invoke-interface {v14}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Leho;

    iput-object v3, v9, Lehm;->d:[Leho;

    const/4 v3, 0x0

    invoke-interface {v14}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move v7, v3

    :cond_a
    :goto_a
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v6, "section_"

    move-object/from16 v0, v18

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lejh;

    invoke-virtual {v8, v6}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_a

    new-instance v18, Leho;

    invoke-direct/range {v18 .. v18}, Leho;-><init>()V

    iget v0, v3, Lejh;->a:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Leho;->a:I

    iget-object v0, v3, Lejh;->b:Leii;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Leho;->e:Leii;

    move-object/from16 v0, v18

    iput-object v6, v0, Leho;->c:Ljava/lang/String;

    iget-object v0, v3, Lejh;->b:Leii;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Leii;->c:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    iget-object v3, v3, Lejh;->b:Leii;

    iget-object v0, v3, Leii;->f:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-static {v6}, Leld;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Leho;->f:Ljava/lang/String;

    :cond_b
    :goto_b
    iget-object v6, v9, Lehm;->d:[Leho;

    add-int/lit8 v3, v7, 0x1

    aput-object v18, v6, v7

    move v7, v3

    goto :goto_a

    :cond_c
    invoke-static/range {v19 .. v19}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    :goto_c
    array-length v0, v6

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v3, v0, :cond_d

    aget-object v20, v6, v3

    invoke-static/range {v20 .. v20}, Leld;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    aput-object v20, v6, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_d
    move-object/from16 v0, v19

    invoke-static {v0, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    iput-object v3, v0, Leho;->f:Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_b

    :catchall_0
    move-exception v3

    :goto_d
    move-object/from16 v0, p5

    iput-wide v10, v0, Lekv;->c:J

    throw v3

    :cond_e
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3, v4, v5, v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JLehm;)Landroid/util/Pair;

    move-result-object v6

    iget-object v3, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v3, :cond_f

    iget-object v3, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lehr;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lejm;->a(Lehr;)V

    :cond_f
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, "index "

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_10
    const-string v8, "del"

    invoke-static {v3, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3, v4, v5, v7, v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILjava/lang/String;)I

    move-result v3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "delete "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_11
    const-string v3, "bad action"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_12
    cmp-long v3, v4, v10

    if-lez v3, :cond_13

    invoke-virtual/range {p4 .. p4}, Leix;->b()Leiw;

    move-result-object v3

    move-object/from16 v0, p1

    iget v6, v0, Lehh;->a:I

    invoke-virtual/range {p0 .. p0}, Lejm;->o()V

    const-string v7, "action"

    invoke-virtual {v3, v7}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v7, "uri"

    invoke-virtual {v3, v7}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "tag"

    invoke-virtual {v3, v8}, Leiw;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v8

    :try_start_6
    const-string v3, "Uri"

    const/16 v17, 0x100

    move/from16 v0, v17

    invoke-static {v3, v7, v0}, Lekx;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    const-string v3, "Tag"

    const/16 v17, 0x3e8

    move/from16 v0, v17

    invoke-static {v3, v8, v0}, Lekx;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    const-string v3, "add"

    invoke-static {v9, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    const/4 v9, 0x1

    :goto_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILjava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "tag "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    :cond_13
    :goto_f
    invoke-virtual/range {p4 .. p4}, Leix;->b()Leiw;

    move-result-object v3

    invoke-virtual {v3}, Leiw;->c()Z

    move-result v3

    if-nez v3, :cond_7

    move-object/from16 v0, p4

    invoke-virtual {v0, v13, v4, v5}, Leix;->a(Landroid/net/Uri;J)V

    goto/16 :goto_7

    :catch_0
    move-exception v3

    const-string v3, "bad tag args"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V

    goto :goto_f

    :cond_14
    const-string v3, "del"

    invoke-static {v9, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_15

    const/4 v9, 0x0

    goto :goto_e

    :cond_15
    const-string v3, "bad action"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lekv;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_f

    :catchall_1
    move-exception v3

    move-wide/from16 v10, p2

    goto/16 :goto_d

    :catchall_2
    move-exception v3

    move-wide v10, v4

    goto/16 :goto_d

    :cond_16
    move-wide v10, v4

    goto/16 :goto_0

    :cond_17
    move v3, v6

    goto/16 :goto_9

    :cond_18
    move-wide v4, v10

    goto/16 :goto_8
.end method

.method private b(Lehr;)V
    .locals 8

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    invoke-virtual {p0}, Lejm;->o()V

    if-nez p1, :cond_0

    const-string v0, "Compaction failed"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v1, "compaction_failed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->k()Lehg;

    move-result-object v1

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1, p1}, Leiy;->a(Lehg;Lehr;)V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->b()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lejm;->i:Lelt;

    invoke-virtual {v2}, Lelt;->d()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Collection;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {v0}, Lels;->g()V

    goto :goto_1

    :cond_1
    iget v0, v1, Lehg;->e:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v2, "compaction_with_errors"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    :cond_2
    const-string v0, "Done compaction min disk %.3f%% min index %.3f%% num docs %d old %d trimmed %d err %d"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lejm;->f:Lelj;

    invoke-virtual {v4}, Lelj;->f()D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget v4, v1, Lehg;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, v1, Lehg;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, v1, Lehg;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v1, v1, Lehg;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0
.end method

.method private b(Leko;)Z
    .locals 13

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v12, 0x26

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->e:Lejk;

    invoke-virtual {v0}, Lejk;->a()I

    move-result v4

    if-le v4, v12, :cond_0

    const-string v0, "Version going backward from %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    if-ne v4, v0, :cond_1

    const-string v0, "Version not set, assuming clear data."

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    move v0, v2

    goto :goto_0

    :cond_1
    if-ge v4, v12, :cond_a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_2

    iget-object v0, p0, Lejm;->f:Lelj;

    invoke-virtual {v0}, Lelj;->b()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v0

    iput-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    :cond_2
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v4

    :goto_1
    if-ge v3, v12, :cond_6

    sparse-switch v3, :sswitch_data_0

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :sswitch_0
    iput-boolean v1, p1, Leko;->b:Z

    goto :goto_2

    :sswitch_1
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->d()Ljava/util/Set;

    move-result-object v0

    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v9, p0, Lejm;->g:Leiy;

    invoke-virtual {v9, v0}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->k()Lehg;

    move-result-object v0

    iget-wide v8, v0, Lehg;->a:J

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->n()Lehg;

    move-result-object v0

    iget-wide v10, v0, Lehg;->a:J

    cmp-long v0, v8, v10

    if-eqz v0, :cond_5

    const-string v0, "Interrupted compact, skipping upgrade33"

    invoke-static {v0}, Lehe;->e(Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->m()Lehr;

    move-result-object v0

    iget-object v8, p0, Lejm;->f:Lelj;

    invoke-virtual {v8}, Lelj;->b()Ljava/io/File;

    move-result-object v8

    invoke-static {v7, v0, v8}, Leoi;->a(Ljava/util/List;Lehr;Ljava/io/File;)Z

    goto :goto_2

    :sswitch_2
    iput-boolean v1, p1, Leko;->c:Z

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v4}, Leiy;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "Couldn\'t upgrade corpus map from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_7
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v0, :cond_8

    move v0, v2

    :goto_4
    if-eqz v0, :cond_9

    const-string v1, "Successfully upgraded native from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v1, p0, Lejm;->e:Lejk;

    invoke-virtual {v1}, Lejk;->b()V

    :goto_5
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v5, v6}, Lejm;->a(IJ)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(I)Z

    move-result v0

    goto :goto_4

    :cond_9
    const-string v1, "Couldn\'t upgrade native from version %d to %d"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_5

    :cond_a
    move v0, v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x20 -> :sswitch_1
        0x24 -> :sswitch_2
    .end sparse-switch
.end method

.method private b(Lely;I)[Ljava/lang/String;
    .locals 4

    invoke-virtual {p0, p1, p2}, Lejm;->a(Lely;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->b:Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private l(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-boolean v0, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "Could not get app info for %s"

    invoke-static {v2, p1, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private m(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    const-string v0, "doRemovePackageData %s"

    invoke-static {v0, p1}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lejm;->a(Lels;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()Leit;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lejm;->f:Lelj;

    invoke-virtual {v1, v0}, Lelj;->a(Leit;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lejm;->t()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "doRemovePackageData %s: not a known client"

    invoke-static {v0, p1}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private x()V
    .locals 2

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lemd;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Lemd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private y()Lenh;
    .locals 1

    invoke-virtual {p0}, Lejm;->i()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lejm;->a(Z)Lenh;

    move-result-object v0

    return-object v0
.end method

.method private z()Lenh;
    .locals 4

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lejy;

    invoke-direct {v1, p0}, Lejy;-><init>(Lejm;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9

    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lejm;->x()V

    iget-object v1, p0, Lejm;->i:Lelt;

    invoke-virtual {v1}, Lelt;->a()Lelv;

    move-result-object v1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, v1, Lelv;->b:Z

    if-nez v4, :cond_0

    iget-boolean v1, v1, Lelv;->d:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v3, :cond_2

    const-string v1, "No operation named"

    :goto_0
    if-eqz v0, :cond_1

    const-string v3, "block"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Lenh;->e()Ljava/lang/Object;

    :cond_1
    const-string v0, "error_message"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_2
    const-string v1, "flush"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lejm;->x()V

    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v3, Lejx;

    invoke-direct {v3, p0}, Lejx;-><init>(Lejm;)V

    invoke-virtual {v1, v3, v6, v7}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_3
    const-string v1, "clear"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lejm;->y()Lenh;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_4
    const-string v1, "compact"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lejm;->z()Lenh;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_5
    const-string v1, "compactAndPurge"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "target"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;)D

    move-result-wide v3

    invoke-direct {p0}, Lejm;->x()V

    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v5, Lejz;

    invoke-direct {v5, p0, v3, v4}, Lejz;-><init>(Lejm;D)V

    invoke-virtual {v1, v5, v6, v7}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_0

    :cond_6
    const-string v1, "No target free for compactAndPurge specified"

    goto :goto_0

    :cond_7
    const-string v1, "getDebugString"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "debug"

    invoke-direct {p0}, Lejm;->x()V

    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    new-instance v4, Ljava/io/PrintWriter;

    invoke-direct {v4, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "native"

    aput-object v7, v5, v6

    invoke-virtual {p0, v0, v4, v5}, Lejm;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto/16 :goto_0

    :cond_8
    const-string v1, "readresources"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v3, Lekd;

    invoke-direct {v3, p0}, Lekd;-><init>(Lejm;)V

    invoke-virtual {v1, v3, v6, v7}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_0

    :cond_9
    const-string v1, "maintenance"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lejm;->n()Lenh;

    move-result-object v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    goto/16 :goto_0

    :cond_a
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unknown operation \""

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p2}, Lekx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "Bad get corpus status args: %s"

    invoke-static {v2, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    iget-object v3, p0, Lejm;->g:Leiy;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v2

    invoke-virtual {v3, v0, v4, v2}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lejm;->g:Leiy;

    invoke-virtual {v4, v0}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v3, v5, Lehj;->b:Lehk;

    iget v0, v3, Lehk;->d:I

    if-nez v0, :cond_4

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iget-object v1, v3, Lehk;->c:[Lehl;

    array-length v4, v1

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v2, v1, v0

    iget-object v6, v2, Lehl;->a:Ljava/lang/String;

    iget v2, v2, Lehl;->b:I

    invoke-virtual {v7, v6, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-wide v1, v3, Lehk;->a:J

    iget-wide v3, v3, Lehk;->b:J

    iget-object v0, v5, Lehj;->a:Lehh;

    iget-object v8, v0, Lehh;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    const-wide/16 v5, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>(JJJLandroid/os/Bundle;Ljava/lang/String;)V

    :goto_2
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public final a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 14

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {p1, v0, v1}, Lekx;->a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Laio;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    new-instance v4, Landroid/util/TimingLogger;

    const-string v2, "Icing"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "getDocuments "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v5, p1

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lejm;->x()V

    const-string v2, "wait index init"

    invoke-virtual {v4, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    iget-object v2, p0, Lejm;->i:Lelt;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v2

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3}, Leiy;->j()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v3, p0, Lejm;->g:Leiy;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v3, v2, v6, v7}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v2

    const-string v3, "authentication"

    invoke-virtual {v4, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p3, v3, v6

    invoke-static {v2, v3}, Lejm;->a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {v3}, Laio;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v5

    throw v2

    :cond_1
    :try_start_1
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-direct {p0, v3, v2}, Lejm;->a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {v3}, Laio;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v2

    monitor-exit v5

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, v2}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v6

    if-nez v6, :cond_3

    const-string v2, "Corpus does not exist"

    invoke-static {v2}, Laio;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v2

    monitor-exit v5

    goto/16 :goto_0

    :cond_3
    new-instance v7, Leie;

    invoke-direct {v7}, Leie;-><init>()V

    iget v2, v6, Lehh;->a:I

    iput v2, v7, Leie;->a:I

    invoke-static {v6}, Leiy;->a(Lehh;)Ljava/util/Map;

    move-result-object v8

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v2, :cond_4

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Leig;

    iput-object v2, v7, Leie;->b:[Leig;

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    iget-object v2, v7, Leie;->b:[Leig;

    new-instance v9, Leig;

    invoke-direct {v9}, Leig;-><init>()V

    aput-object v9, v2, v3

    iget-object v2, v7, Leie;->b:[Leig;

    aget-object v9, v2, v3

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lejh;

    iget v2, v2, Lejh;->a:I

    iput v2, v9, Leig;->a:I

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_4
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v2, :cond_5

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    iget-object v3, v7, Leie;->c:[Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v7, Leie;->c:[Ljava/lang/String;

    :cond_5
    const-string v2, "build corpus spec"

    invoke-virtual {v4, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-object v8, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v8, p1, v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([Ljava/lang/String;Leie;)Lekz;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v9

    sub-long v2, v9, v2

    const-string v9, "execute query"

    invoke-virtual {v4, v9}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    const-string v9, "Retrieved: %d Docs: %d Elapsed: %d ms"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v8, Lekz;->b:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, v8, Lekz;->a:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-wide/32 v12, 0xf4240

    div-long/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v10, v11

    invoke-static {v9, v10}, Lehe;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {p1, v8}, Laio;->a([Ljava/lang/String;Lekz;)[Ljava/lang/String;

    move-result-object v3

    new-instance v2, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-static {v3}, Laio;->a([Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-static {v7, v8, v0, v3}, Laio;->a(Leie;Lekz;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v10

    move-object/from16 v0, p4

    invoke-static {v7, v8, v6, v0, v3}, Laio;->a(Leie;Lekz;Lehh;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v2, v9, v10, v3}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const-string v3, "build DocumentResults"

    invoke-virtual {v4, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroid/util/TimingLogger;->dumpToLog()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;I[B)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 8

    invoke-static {p2}, Lekx;->a(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Laio;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Leht;

    invoke-direct {v0}, Leht;-><init>()V

    move-object v3, v0

    :goto_1
    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    invoke-virtual {v0}, Lely;->b()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :try_start_0
    invoke-static {p3}, Leht;->a([B)Leht;
    :try_end_0
    .catch Lizr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "Bad iter token"

    invoke-static {v0}, Laio;->c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->j()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_1
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->r()Landroid/util/SparseArray;

    move-result-object v7

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v5, v0, [Lehv;

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, v5

    if-ge v1, v0, :cond_3

    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejf;

    iget-object v0, v0, Lejf;->d:Lehv;

    aput-object v0, v5, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-wide v1, v3, Leht;->a:J

    iget v3, v3, Leht;->b:I

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JII[Lehv;)Lehx;

    move-result-object v0

    invoke-static {v0, v7}, Laio;->a(Lehx;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-result-object v0

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 13

    invoke-static {p1, p2}, Lekx;->a([Ljava/lang/String;Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Laio;->d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    invoke-virtual {v0}, Lelv;->a()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Lejm;->x()V

    invoke-virtual {p2}, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->a()[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    move-result-object v3

    new-instance v4, Leib;

    invoke-direct {v4}, Leib;-><init>()V

    iput-object p1, v4, Leib;->b:[Ljava/lang/String;

    new-instance v5, Leid;

    invoke-direct {v5}, Leid;-><init>()V

    iput-object v5, v4, Leib;->a:Leid;

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->j()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    array-length v0, v3

    new-array v0, v0, [Leic;

    iput-object v0, v4, Leib;->c:[Leic;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    array-length v0, v3

    if-ge v2, v0, :cond_6

    aget-object v0, v3, v2

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    aget-object v1, v3, v2

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a()Ljava/util/Map;

    move-result-object v8

    iget-object v1, p0, Lejm;->i:Lelt;

    iget-object v9, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-virtual {v1, v9}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v9, v1, Lels;->d:Lema;

    invoke-virtual {v9}, Lema;->d()Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package name non-existent or not globally searchable "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laio;->d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_3
    :try_start_1
    new-instance v9, Leic;

    invoke-direct {v9}, Leic;-><init>()V

    iget-object v10, p0, Lejm;->g:Leiy;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-virtual {v10, v1, v0}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1, v0}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v10

    if-eqz v10, :cond_5

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v11, v10, Lehh;->a:I

    aput v11, v0, v1

    iput-object v0, v9, Leic;->a:[I

    const/4 v1, 0x0

    :goto_2
    iget-object v0, v10, Lehh;->k:[Leii;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    iget-object v0, v10, Lehh;->k:[Leii;

    aget-object v0, v0, v1

    iget-object v0, v0, Leii;->a:Ljava/lang/String;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_8

    new-instance v11, Leih;

    invoke-direct {v11}, Leih;-><init>()V

    iget v12, v10, Lehh;->a:I

    iput v12, v11, Leih;->a:I

    iput v1, v11, Leih;->b:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v11, Leih;->c:I

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    :goto_3
    add-int/lit8 v1, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, v4, Leib;->c:[Leic;

    aput-object v9, v0, v2

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, v5, Leid;->e:[Leih;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Leih;

    iput-object v0, v5, Leid;->e:[Leih;

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Leib;)[I

    move-result-object v1

    array-length v0, v3

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/CorpusId;

    const/4 v0, 0x0

    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_7

    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    new-instance v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>([Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 14

    if-nez p4, :cond_0

    const/4 v1, 0x1

    move v2, v1

    :goto_0
    new-instance v1, Lemt;

    const/4 v3, 0x1

    invoke-static {v2}, Lell;->b(I)I

    move-result v2

    invoke-direct {v1, v3, v2}, Lemt;-><init>(II)V

    invoke-static/range {p1 .. p4}, Lekx;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, Laio;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_0
    move-object/from16 v0, p4

    iget v1, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    move v2, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lejm;->i:Lelt;

    invoke-virtual {v2}, Lelt;->a()Lelv;

    move-result-object v3

    invoke-direct {p0}, Lejm;->x()V

    invoke-virtual {v1}, Lemt;->a()V

    const-string v2, "Query global start %d num %d"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    const-string v2, "Query global: [%s]"

    invoke-static {v2, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const-string v2, "Index docs: %d pls: %d"

    iget-object v4, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->l()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->m()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2}, Leiy;->j()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    :try_start_0
    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0}, Lejm;->a(Lelv;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/util/List;

    move-result-object v4

    new-instance v5, Lemg;

    move-object/from16 v0, p4

    invoke-direct {v5, v4, v0}, Lemg;-><init>(Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V

    new-instance v6, Leid;

    invoke-direct {v6}, Leid;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v6, Leid;->g:Z

    const/4 v2, 0x1

    iput-boolean v2, v6, Leid;->h:Z

    iget-object v2, v5, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    if-nez v2, :cond_3

    const/4 v2, 0x0

    :goto_2
    iput v2, v6, Leid;->f:I

    iget-object v2, v5, Lemg;->a:[Lemf;

    array-length v2, v2

    new-array v2, v2, [Leie;

    iput-object v2, v6, Leid;->a:[Leie;

    const/4 v2, 0x1

    iput-boolean v2, v6, Leid;->d:Z

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_3
    iget-object v9, v5, Lemg;->a:[Lemf;

    array-length v9, v9

    if-ge v2, v9, :cond_4

    iget-object v9, v6, Leid;->a:[Leie;

    iget-object v10, v5, Lemg;->a:[Lemf;

    aget-object v10, v10, v2

    iget-object v11, v5, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    new-instance v12, Leie;

    invoke-direct {v12}, Leie;-><init>()V

    iget-object v13, v10, Lemf;->a:Lehh;

    iget v13, v13, Lehh;->a:I

    iput v13, v12, Leie;->a:I

    iget-object v13, v10, Lemf;->a:Lehh;

    iget-object v13, v13, Lehh;->l:[Leim;

    iput-object v13, v12, Leie;->d:[Leim;

    if-eqz v11, :cond_2

    iget-object v13, v10, Lemf;->a:Lehh;

    iget-object v13, v13, Lehh;->d:Ljava/lang/String;

    iget-object v10, v10, Lemf;->a:Lehh;

    iget-object v10, v10, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v11, v13, v10}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    move-result-object v10

    if-eqz v10, :cond_2

    iget v11, v10, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    const/4 v13, 0x1

    if-le v11, v13, :cond_2

    iget v10, v10, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    iput v10, v12, Leie;->e:I

    :cond_2
    aput-object v12, v9, v2

    iget-object v9, v5, Lemg;->a:[Lemf;

    aget-object v9, v9, v2

    invoke-virtual {v9, v7}, Lemf;->a(Ljava/util/List;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    iget-object v2, v5, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget v2, v2, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->c:I

    goto :goto_2

    :cond_4
    iget-object v2, v6, Leid;->e:[Leih;

    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Leih;

    iput-object v2, v6, Leid;->e:[Leih;

    iget-object v2, v5, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, Lell;->a(I)I

    move-result v2

    iput v2, v6, Leid;->k:I

    iget v2, v6, Leid;->f:I

    if-lez v2, :cond_6

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lehh;

    const-string v7, "Corpus: %s:%s id %d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v2, Lehh;->d:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, v2, Lehh;->b:Ljava/lang/String;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    iget v2, v2, Lehh;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v10

    invoke-static {v7, v9}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v1

    monitor-exit v8

    throw v1

    :cond_5
    :try_start_1
    iget-object v2, v5, Lemg;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget v2, v2, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    goto :goto_4

    :cond_6
    if-eqz p4, :cond_7

    iget-boolean v2, v3, Lelv;->d:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->e:I

    :goto_6
    iput v2, v6, Leid;->j:I

    :cond_7
    iget-object v2, p0, Lejm;->e:Lejk;

    iget-object v3, v2, Lejk;->b:Lejl;

    sget-object v4, Lejj;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lejl;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v2, v2, Lejk;->b:Lejl;

    sget-object v3, Lejj;->a:Ljava/lang/String;

    iget-object v2, v2, Lejl;->b:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lixm;

    iget-boolean v2, v2, Lixm;->b:Z

    :goto_7
    if-eqz v2, :cond_a

    const/4 v3, 0x0

    const-string v2, "numResults changed to 0 due to icing experiment."

    invoke-static {v2}, Lehe;->d(Ljava/lang/String;)I

    iget-object v2, p0, Lejm;->m:Leji;

    const-string v4, "expt_disable_icing_results"

    invoke-interface {v2, v4}, Leji;->a(Ljava/lang/String;)V

    :goto_8
    invoke-virtual {v1}, Lemt;->b()V

    iget-object v2, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    move/from16 v0, p2

    invoke-virtual {v2, p1, v6, v0, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;Leid;II)Lekz;

    move-result-object v2

    invoke-virtual {v1}, Lemt;->c()V

    invoke-virtual {v5, v2}, Lemg;->a(Lekz;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v7

    iget-object v9, p0, Lejm;->m:Leji;

    iget v4, v2, Lekz;->a:I

    iget v5, v2, Lekz;->b:I

    iget-object v2, p0, Lejm;->v:Leky;

    invoke-virtual {v2}, Leky;->c()I

    move-result v6

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, Lemt;->a(Ljava/lang/String;IIII)Litt;

    move-result-object v1

    invoke-interface {v9, v1}, Leji;->a(Litt;)V

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v7

    goto/16 :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    :cond_9
    const/4 v2, 0x0

    goto :goto_7

    :cond_a
    move/from16 v3, p3

    goto :goto_8
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 21

    if-nez p6, :cond_0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    new-instance v3, Lemt;

    const/4 v5, 0x0

    invoke-static {v4}, Lell;->b(I)I

    move-result v4

    invoke-direct {v3, v5, v4}, Lemt;-><init>(II)V

    invoke-static/range {p1 .. p6}, Lekx;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v4}, Laio;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    move v4, v3

    goto :goto_0

    :cond_1
    invoke-direct/range {p0 .. p0}, Lejm;->x()V

    invoke-virtual {v3}, Lemt;->a()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->i:Lelt;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v5

    const-string v4, "Query from %s start %d num %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x1

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v6}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v4, "Query: [%s]"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    const-string v4, "Index docs: %d pls: %d"

    move-object/from16 v0, p0

    iget-object v6, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->m()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->g:Leiy;

    invoke-virtual {v4}, Leiy;->j()Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->g:Leiy;

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v4, v5, v0, v6}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-static {v7, v0}, Lejm;->a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v4}, Laio;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit v10

    throw v3

    :cond_2
    :try_start_1
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v7}, Lejm;->a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v4}, Laio;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    monitor-exit v10

    goto/16 :goto_1

    :cond_3
    const/4 v4, 0x0

    if-nez p3, :cond_4

    invoke-virtual {v5}, Lely;->a()Z

    move-result v4

    :cond_4
    new-instance v9, Leid;

    invoke-direct {v9}, Leid;-><init>()V

    iput-boolean v4, v9, Leid;->b:Z

    move-object/from16 v0, p6

    iget-boolean v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Z

    iput-boolean v4, v9, Leid;->d:Z

    move-object/from16 v0, p6

    iget-boolean v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->e:Z

    iput-boolean v4, v9, Leid;->h:Z

    iget-boolean v4, v5, Lelv;->d:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p6

    iget v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->f:I

    :goto_2
    iput v4, v9, Leid;->j:I

    move-object/from16 v0, p6

    iget v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    invoke-static {v4}, Lell;->a(I)I

    move-result v4

    iput v4, v9, Leid;->k:I

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v11, Landroid/util/SparseArray;

    invoke-direct {v11}, Landroid/util/SparseArray;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lejm;->g:Leiy;

    invoke-virtual {v7, v4}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v15

    if-eqz v15, :cond_e

    iget v4, v15, Lehh;->a:I

    invoke-virtual {v11, v4, v15}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v16, Leie;

    invoke-direct/range {v16 .. v16}, Leie;-><init>()V

    iget v4, v15, Lehh;->a:I

    move-object/from16 v0, v16

    iput v4, v0, Leie;->a:I

    invoke-static {v15}, Leiy;->a(Lehh;)Ljava/util/Map;

    move-result-object v8

    if-eqz v6, :cond_d

    if-nez v5, :cond_7

    move v7, v6

    move-object v6, v8

    :goto_4
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v4, :cond_9

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_5
    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v5, v4, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v8, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lejh;

    if-eqz v5, :cond_5

    new-instance v19, Leig;

    invoke-direct/range {v19 .. v19}, Leig;-><init>()V

    iget v0, v5, Lejh;->a:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Leig;->a:I

    iget-boolean v0, v4, Lcom/google/android/gms/appdatasearch/Section;->c:Z

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput-boolean v0, v1, Leig;->b:Z

    iget v4, v4, Lcom/google/android/gms/appdatasearch/Section;->d:I

    move-object/from16 v0, v19

    iput v4, v0, Leig;->d:I

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, v5, Lejh;->b:Leii;

    iget v4, v4, Leii;->d:I

    const/16 v19, 0x1

    move/from16 v0, v19

    if-eq v4, v0, :cond_5

    new-instance v4, Leih;

    invoke-direct {v4}, Leih;-><init>()V

    iget v0, v5, Lejh;->a:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v4, Leih;->b:I

    iget v0, v15, Lehh;->a:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v4, Leih;->a:I

    iget-object v5, v5, Lejh;->b:Leii;

    iget v5, v5, Leii;->d:I

    iput v5, v4, Leih;->c:I

    invoke-interface {v13, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_7
    invoke-virtual {v5, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v6, 0x0

    move v7, v6

    move-object v6, v5

    goto/16 :goto_4

    :cond_8
    move-object/from16 v0, v16

    iget-object v4, v0, Leie;->b:[Leig;

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Leig;

    move-object/from16 v0, v16

    iput-object v4, v0, Leie;->b:[Leig;

    :cond_9
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v4, :cond_a

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    move-object/from16 v0, v16

    iget-object v5, v0, Leie;->c:[Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v4, v0, Leie;->c:[Ljava/lang/String;

    :cond_a
    move-object/from16 v0, v16

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v6

    move v5, v7

    :goto_6
    move v6, v5

    move-object v5, v4

    goto/16 :goto_3

    :cond_b
    iget-object v4, v9, Leid;->a:[Leie;

    invoke-interface {v12, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Leie;

    iput-object v4, v9, Leid;->a:[Leie;

    iget-object v4, v9, Leid;->e:[Leih;

    invoke-interface {v13, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Leih;

    iput-object v4, v9, Leid;->e:[Leih;

    if-eqz v6, :cond_c

    if-eqz v5, :cond_c

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Leif;

    iput-object v4, v9, Leid;->c:[Leif;

    const/4 v4, 0x0

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v4

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lejh;

    new-instance v8, Leif;

    invoke-direct {v8}, Leif;-><init>()V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, v8, Leif;->a:Ljava/lang/String;

    iget v4, v5, Lejh;->a:I

    iput v4, v8, Leif;->b:I

    iget-object v5, v9, Leid;->c:[Leif;

    add-int/lit8 v4, v6, 0x1

    aput-object v8, v5, v6

    move v6, v4

    goto :goto_7

    :cond_c
    invoke-virtual {v3}, Lemt;->b()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v4, v0, v9, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;Leid;II)Lekz;

    move-result-object v4

    invoke-virtual {v3}, Lemt;->c()V

    invoke-static {v9, v4, v11}, Laio;->a(Leid;Lekz;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lejm;->m:Leji;

    iget v6, v4, Lekz;->a:I

    iget v7, v4, Lekz;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->v:Leky;

    invoke-virtual {v4}, Leky;->c()I

    move-result v8

    move-object/from16 v4, p1

    move/from16 v5, p5

    invoke-virtual/range {v3 .. v8}, Lemt;->a(Ljava/lang/String;IIII)Litt;

    move-result-object v3

    invoke-interface {v11, v3}, Leji;->a(Litt;)V

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v3, v9

    goto/16 :goto_1

    :cond_d
    move v7, v6

    move-object v6, v5

    goto/16 :goto_4

    :cond_e
    move-object v4, v5

    move v5, v6

    goto/16 :goto_6
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/SuggestSpecification;)Lcom/google/android/gms/appdatasearch/SuggestionResults;
    .locals 9

    const/4 v2, 0x0

    new-instance v0, Lemt;

    const/4 v1, 0x2

    const/4 v3, 0x3

    invoke-direct {v0, v1, v3}, Lemt;-><init>(II)V

    invoke-static {p1, p2, p3, p4}, Lekx;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lejm;->x()V

    invoke-virtual {v0}, Lemt;->a()V

    iget-object v1, p0, Lejm;->i:Lelt;

    invoke-virtual {v1, p2}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v1

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3}, Leiy;->j()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    :try_start_0
    iget-object v3, p0, Lejm;->g:Leiy;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, p3, v4}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    new-array v5, v3, [I

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    add-int/lit8 v4, v3, 0x1

    iget-object v8, p0, Lejm;->g:Leiy;

    invoke-virtual {v8, v1}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v1

    iget v1, v1, Lehh;->a:I

    aput v1, v5, v3

    move v3, v4

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lemt;->b()V

    iget-object v1, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, p1, v5, p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;[II)Leik;

    move-result-object v3

    invoke-virtual {v0}, Lemt;->c()V

    iget-object v1, v3, Leik;->a:[Leil;

    array-length v1, v1

    new-array v4, v1, [Ljava/lang/String;

    iget-object v1, v3, Leik;->a:[Leil;

    array-length v1, v1

    new-array v5, v1, [Ljava/lang/String;

    :goto_2
    iget-object v1, v3, Leik;->a:[Leil;

    array-length v1, v1

    if-ge v2, v1, :cond_3

    iget-object v1, v3, Leik;->a:[Leil;

    aget-object v1, v1, v2

    iget-object v1, v1, Leil;->a:Ljava/lang/String;

    aput-object v1, v4, v2

    iget-object v1, v3, Leik;->a:[Leil;

    aget-object v1, v1, v2

    iget-object v1, v1, Leil;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v1, 0x0

    :cond_2
    aput-object v1, v5, v2

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_3
    new-instance v6, Lcom/google/android/gms/appdatasearch/SuggestionResults;

    invoke-direct {v6, v4, v5}, Lcom/google/android/gms/appdatasearch/SuggestionResults;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v8, p0, Lejm;->m:Leji;

    array-length v3, v4

    const/4 v4, 0x0

    iget-object v1, p0, Lejm;->v:Leky;

    invoke-virtual {v1}, Leky;->c()I

    move-result v5

    move-object v1, p1

    move v2, p4

    invoke-virtual/range {v0 .. v5}, Lemt;->a(Ljava/lang/String;IIII)Litt;

    move-result-object v0

    invoke-interface {v8, v0}, Leji;->a(Litt;)V

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v6

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final a(Z)Lenh;
    .locals 4

    iget-object v0, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Leki;

    invoke-direct {v1, p0, p1}, Leki;-><init>(Lejm;Z)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    return-object v0
.end method

.method final a(Lely;I)Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lejm;->g:Leiy;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v3}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, v0}, Leiy;->c(Ljava/lang/String;)Lema;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->b:Lehk;

    iget v0, v0, Lehk;->d:I

    if-nez v0, :cond_0

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehj;

    iget-object v0, v0, Lehj;->a:Lehh;

    iget-object v0, v0, Lehh;->d:Ljava/lang/String;

    iget-object v4, p1, Lely;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    invoke-virtual {v3}, Lema;->b()I

    move-result v0

    if-ne p2, v0, :cond_0

    :cond_1
    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method final a(D)V
    .locals 7

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    invoke-virtual {p0}, Lejm;->o()V

    const-string v0, "Starting purge with target free %.3f%% min disk %.3f%% min index %.3f%%"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    mul-double v3, p1, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lejm;->f:Lelj;

    invoke-virtual {v3}, Lelj;->f()D

    move-result-wide v3

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v3

    mul-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1}, Leiy;->g()[J

    move-result-object v1

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2}, Leiy;->q()[I

    move-result-object v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(D[J[I)Lehr;

    move-result-object v0

    invoke-direct {p0, v0}, Lejm;->b(Lehr;)V

    return-void
.end method

.method final a(IJ)V
    .locals 3

    iget-object v0, p0, Lejm;->m:Leji;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sub-long/2addr v1, p2

    long-to-int v1, v1

    invoke-interface {v0, p1, v1}, Leji;->a(II)V

    return-void
.end method

.method public final a(J)V
    .locals 4

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lekb;

    invoke-direct {v1, p0, p1, p2}, Lekb;-><init>(Lejm;J)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got package manager broadcast: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lehe;->b(Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "Couldn\'t handle %s intent due to initialization failure."

    invoke-static {v0, v2}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v2, "android.intent.extra.REPLACING"

    invoke-virtual {v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    if-nez v2, :cond_3

    move v5, v0

    :goto_2
    if-nez v2, :cond_1

    const-string v6, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    :goto_3
    move v4, v0

    move v6, v1

    :goto_4
    iget-object v7, p0, Lejm;->c:Lemz;

    new-instance v0, Lejt;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lejt;-><init>(Lejm;ZLjava/lang/String;ZZZ)V

    const-wide/16 v1, 0x0

    invoke-virtual {v7, v0, v1, v2}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v5, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    const-string v4, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v0

    move v6, v1

    move v5, v1

    move v2, v1

    goto :goto_4

    :cond_6
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v1

    move v6, v0

    move v5, v1

    move v2, v1

    goto :goto_4

    :cond_7
    const-string v4, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v1

    move v6, v0

    move v5, v1

    move v2, v1

    goto :goto_4

    :cond_8
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-direct {p0, v3}, Lejm;->l(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v0

    :goto_5
    if-nez v2, :cond_a

    :goto_6
    move v4, v1

    move v6, v0

    move v5, v1

    goto :goto_4

    :cond_9
    move v2, v1

    goto :goto_5

    :cond_a
    move v0, v1

    goto :goto_6

    :cond_b
    new-instance v0, Lemd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown intent action "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lemd;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lejm;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    return-void
.end method

.method final a(Lels;Ljava/lang/String;)V
    .locals 4

    const-string v0, "unregisterFromResources: %s corpus %s"

    iget-object v1, p1, Lels;->a:Ljava/lang/String;

    invoke-static {v0, v1, p2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, p1, p2}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1}, Leiy;->c(Ljava/lang/String;)Lema;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "Request to unregister non-existant resources corpus %s from package %s"

    iget-object v3, p1, Lels;->a:Ljava/lang/String;

    invoke-static {v2, p2, v3}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_0
    invoke-virtual {v0}, Lema;->b()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->a(Z)V

    invoke-virtual {p0, v1, p1}, Lejm;->a(Ljava/lang/String;Lels;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to unregister corpus from client "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lels;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Lely;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V
    .locals 3

    iget-object v0, p1, Lely;->e:Ljava/lang/String;

    invoke-static {p2}, Lekx;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lemb;

    invoke-direct {v1, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    invoke-static {p2, p3, p4}, Lema;->b(Ljava/lang/Object;J)Lema;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lejm;->a(Lely;Lema;)V
    :try_end_0
    .catch Leme; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to register corpus from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lely;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resources"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method final a(Lely;Lema;)V
    .locals 6

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Lely;)Lels;

    move-result-object v2

    invoke-virtual {v2}, Lels;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Leme;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Package "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lels;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is blocked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Leme;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v1, Lemb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ContentProvider "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not exist"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-boolean v4, p1, Lelv;->b:Z

    if-nez v4, :cond_2

    iget v4, p1, Lelv;->a:I

    iget-object v5, v3, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v4, v5, :cond_2

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "ContentProvider "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " authority "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " uid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, v3, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not match calling uid "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Lelv;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-boolean v4, p1, Lelv;->b:Z

    if-nez v4, :cond_3

    iget-object v3, v3, Landroid/content/pm/ProviderInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v4, p1, Lelv;->a:I

    invoke-static {v1, v4, v3}, Lbox;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "ContentProvider "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " package name "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not match client package names"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Lemh;

    iget-object v3, v2, Lels;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-virtual {v2}, Lels;->h()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v3, v0, v4}, Lemh;-><init>(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;Landroid/content/res/Resources;)V

    invoke-virtual {v1}, Lemh;->a()V

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    invoke-static {v2, v0}, Leiy;->a(Lels;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lehh;

    move-result-object v0

    invoke-virtual {p2, v0}, Lema;->a(Ljava/lang/Object;)Lema;

    move-result-object v3

    invoke-virtual {v1}, Lemh;->b()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v1, v1, Lehh;->l:[Leim;

    invoke-interface {v4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Leim;

    iput-object v1, v0, Lehh;->l:[Leim;

    :cond_4
    invoke-virtual {v2}, Lels;->f()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    const-string v0, "App %s registering with different sigs, clearing old corpora"

    iget-object v1, v2, Lels;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-virtual {p0, v2}, Lejm;->a(Lels;)Z

    invoke-virtual {v2}, Lels;->f()I

    move-result v0

    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, Lejm;->m:Leji;

    const-string v3, "register_auth_fail"

    invoke-interface {v1, v3}, Leji;->a(Ljava/lang/String;)V

    new-instance v1, Leme;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Package "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lels;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " cannot register: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unknwown error"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Leme;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v0, "ok"

    goto :goto_0

    :pswitch_1
    const-string v0, "App not allowed"

    goto :goto_0

    :pswitch_2
    const-string v0, "Has different fingerprint"

    goto :goto_0

    :pswitch_3
    const-string v0, "App unknown"

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1}, Leiy;->e(Ljava/lang/String;)Lema;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0, p2}, Lema;->a(Lema;)Z

    move-result v4

    if-nez v4, :cond_7

    new-instance v1, Lemb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CorpusConfig: cannot "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lema;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " when previously "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lema;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    iget-object v4, p0, Lejm;->g:Leiy;

    invoke-virtual {v4, v1}, Leiy;->f(Ljava/lang/String;)Lehk;

    move-result-object v4

    if-eqz v4, :cond_8

    iget v4, v4, Lehk;->d:I

    if-nez v4, :cond_8

    iget-object v4, p0, Lejm;->g:Leiy;

    invoke-virtual {v4, v1, v0, v3}, Leiy;->a(Ljava/lang/String;Lema;Lema;)Z

    move-result v0

    if-eqz v0, :cond_8

    :goto_1
    return-void

    :cond_8
    const-string v0, "Corpus registration info changed, replacing corpus"

    invoke-static {v0}, Lehe;->b(Ljava/lang/String;)I

    invoke-virtual {p0, v1, v2}, Lejm;->a(Ljava/lang/String;Lels;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Leme;

    const-string v1, "Could not unregister old corpus"

    invoke-direct {v0, v1}, Leme;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, p1, v0}, Leiy;->a(Lelv;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v1, Lemb;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Corpus "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lema;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "already exists in a different package from this uid"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lemb;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v0, Lekl;

    invoke-direct {v0, p0}, Lekl;-><init>(Lejm;)V

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, v1, v3, v0}, Leiy;->a(Ljava/lang/String;Lema;Lejg;)V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, v1}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lejm;->a(Ljava/lang/String;Lehh;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final a(Ljava/lang/String;IZLeix;J)V
    .locals 9

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v8, p0, Lejm;->c:Lemz;

    new-instance v0, Lekf;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lekf;-><init>(Lejm;Ljava/lang/String;IZLeix;J)V

    const-wide/16 v1, 0x0

    invoke-virtual {v8, v0, v1, v2}, Lemz;->a(Lenf;J)Lenf;

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lejm;->b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z

    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lejq;

    invoke-direct {v1, p0, p1}, Lejq;-><init>(Lejm;[Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    return-void
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;)Z
    .locals 2

    invoke-static {p1}, Lekx;->a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Bad ResultClickInfo: %s"

    invoke-static {v1, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method final a(Leko;)Z
    .locals 14

    invoke-virtual {p0}, Lejm;->o()V

    const/4 v1, 0x1

    iget-boolean v0, p1, Leko;->a:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lejm;->p()V

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v5

    iget-object v0, p0, Lejm;->u:Lemp;

    invoke-interface {v0}, Lemp;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lelj;

    iput-object v0, p0, Lejm;->f:Lelj;

    iget-object v0, p0, Lejm;->f:Lelj;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lejm;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AppDataSearch-"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lejm;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lejk;

    iget-object v7, p0, Lejm;->b:Landroid/content/Context;

    invoke-direct {v2, v7, v0}, Lejk;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lejm;->e:Lejk;

    new-instance v2, Lelt;

    new-instance v7, Lelu;

    iget-object v8, p0, Lejm;->e:Lejk;

    iget-object v9, p0, Lejm;->b:Landroid/content/Context;

    iget-object v10, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lejm;->w:Lelx;

    invoke-direct {v7, v8, v9, v10, v11}, Lelu;-><init>(Lejk;Landroid/content/Context;Landroid/content/pm/PackageManager;Lelx;)V

    invoke-direct {v2, v7}, Lelt;-><init>(Lelu;)V

    iput-object v2, p0, Lejm;->i:Lelt;

    new-instance v2, Leiy;

    iget-object v7, p0, Lejm;->i:Lelt;

    iget-object v8, p0, Lejm;->b:Landroid/content/Context;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "-config"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lejm;->f:Lelj;

    invoke-virtual {v9}, Lelj;->b()Ljava/io/File;

    move-result-object v9

    invoke-direct {v2, v7, v8, v0, v9}, Leiy;-><init>(Lelt;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    iput-object v2, p0, Lejm;->g:Leiy;

    new-instance v0, Lemv;

    iget-object v2, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v7, p0, Lejm;->g:Leiy;

    iget-object v8, p0, Lejm;->i:Lelt;

    invoke-direct {v0, v2, v7, v8}, Lemv;-><init>(Landroid/content/pm/PackageManager;Leiy;Lelt;)V

    iput-object v0, p0, Lejm;->h:Lemv;

    :try_start_0
    new-instance v0, Leky;

    iget-object v2, p0, Lejm;->b:Landroid/content/Context;

    iget-object v7, p0, Lejm;->e:Lejk;

    iget-object v8, p0, Lejm;->m:Leji;

    invoke-direct {v0, v2, v7, v8}, Leky;-><init>(Landroid/content/Context;Lejk;Leji;)V

    iput-object v0, p0, Lejm;->v:Leky;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3, v4}, Lejm;->a(IJ)V

    iget-boolean v0, p1, Leko;->a:Z

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lejm;->b(Leko;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p1, Leko;->a:Z

    iget-boolean v0, p1, Leko;->a:Z

    if-eqz v0, :cond_16

    const-string v0, "Clearing storage"

    invoke-static {v0}, Lehe;->c(Ljava/lang/String;)I

    iget-object v0, p0, Lejm;->f:Lelj;

    invoke-virtual {v0}, Lelj;->a()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Unable to clear storage, can\'t init index"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v1, "clear_storage_failed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lejm;->p()V

    const/4 v0, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not create extension manager"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {p0}, Lejm;->p()V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1}, Leiy;->a()V

    iget-object v1, p0, Lejm;->i:Lelt;

    iget-object v2, v1, Lelt;->b:Ljava/util/Map;

    monitor-enter v2

    :try_start_1
    iget-object v7, v1, Lelt;->a:Lelu;

    iget-object v7, v7, Lelu;->a:Lejk;

    invoke-virtual {v7}, Lejk;->f()V

    iget-object v1, v1, Lelt;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lejm;->e:Lejk;

    invoke-virtual {v1}, Lejk;->b()V

    :goto_2
    iget-object v1, p0, Lejm;->f:Lelj;

    invoke-virtual {v1}, Lelj;->b()Ljava/io/File;

    move-result-object v7

    iget-object v1, p0, Lejm;->b:Landroid/content/Context;

    invoke-static {v1, v7}, Leli;->a(Landroid/content/Context;Ljava/io/File;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    :cond_6
    :try_start_2
    iget-object v10, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v10, :cond_7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;)Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v10

    iput-object v10, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    :cond_7
    iget-object v10, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-nez v10, :cond_9

    new-instance v0, Ljava/io/IOException;

    const-string v10, "Could not create native index"

    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    const-string v10, "Error initializing, resetting corpora: %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v10, "init_io_exception"

    invoke-interface {v0, v10}, Leji;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p1, Leko;->b:Z

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->b()V

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->e()Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_3
    add-int/lit8 v2, v2, 0x1

    const/4 v10, 0x1

    if-gt v2, v10, :cond_8

    if-nez v0, :cond_6

    :cond_8
    move-object v2, v1

    :goto_4
    if-nez v0, :cond_e

    const-string v0, "Internal init failed"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    invoke-virtual {p0}, Lejm;->p()V

    const/4 v0, 0x0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_9
    :try_start_3
    iget-object v10, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->k()Lehg;

    move-result-object v10

    iget-object v11, p0, Lejm;->g:Leiy;

    invoke-virtual {v11}, Leiy;->n()Lehg;

    move-result-object v11

    iget-wide v12, v10, Lehg;->a:J

    iget-wide v10, v11, Lehg;->a:J

    cmp-long v10, v12, v10

    if-eqz v10, :cond_a

    new-instance v0, Ljava/io/IOException;

    const-string v10, "Index compact timestamp mismatch"

    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v10, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    iget-object v11, p0, Lejm;->g:Leiy;

    invoke-virtual {v11}, Leiy;->m()Lehr;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Lehr;)Lehy;

    move-result-object v10

    if-nez v10, :cond_b

    new-instance v0, Ljava/io/IOException;

    const-string v10, "Index init failed"

    invoke-direct {v0, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v11, p0, Lejm;->g:Leiy;

    invoke-virtual {v11, v10}, Leiy;->a(Lehy;)Ljava/util/Set;

    move-result-object v1

    iget v11, v10, Lehy;->b:I

    packed-switch v11, :pswitch_data_0

    :goto_5
    iget-boolean v10, v10, Lehy;->c:Z

    if-eqz v10, :cond_c

    iget-object v10, p0, Lejm;->m:Leji;

    const-string v11, "init_docstore_recovery"

    invoke-interface {v10, v11}, Leji;->a(Ljava/lang/String;)V

    const/4 v10, 0x1

    iput-boolean v10, p1, Leko;->b:Z

    :cond_c
    move-object v2, v1

    goto :goto_4

    :pswitch_0
    iget-object v11, p0, Lejm;->m:Leji;

    const-string v12, "init_lite_lost"

    invoke-interface {v11, v12}, Leji;->a(Ljava/lang/String;)V

    goto :goto_5

    :pswitch_1
    iget-object v11, p0, Lejm;->m:Leji;

    const-string v12, "init_full_lost"

    invoke-interface {v11, v12}, Leji;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    :cond_d
    const/4 v0, 0x0

    goto :goto_3

    :cond_e
    iget-object v1, p0, Lejm;->v:Leky;

    iget-object v0, p0, Lejm;->b:Landroid/content/Context;

    const/4 v7, 0x0

    new-instance v10, Landroid/content/IntentFilter;

    const-string v11, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v10, v11}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7, v10}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {v1, v0}, Leky;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-direct {p0}, Lejm;->A()V

    :cond_f
    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->g()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lejm;->v:Leky;

    invoke-virtual {v0}, Leky;->a()V

    :cond_10
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v8, v9}, Lejm;->a(IJ)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_11
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {v0}, Lels;->e()Z

    move-result v9

    if-nez v9, :cond_11

    invoke-virtual {p0, v0}, Lejm;->a(Lels;)Z

    goto :goto_7

    :cond_12
    const/4 v0, 0x0

    goto :goto_6

    :cond_13
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7, v8}, Lejm;->a(IJ)V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->i()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v8, "Found corpus [%s] in limbo"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v1, v1, Lehh;->b:Ljava/lang/String;

    invoke-static {v8, v1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v8, p0, Lejm;->i:Lelt;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehh;

    iget-object v1, v1, Lehh;->d:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lelt;->c(Ljava/lang/String;)Lels;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lejm;->a(Ljava/lang/String;Lels;)Z

    goto :goto_8

    :cond_14
    iget-boolean v0, p1, Leko;->b:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->d()Ljava/util/Set;

    move-result-object v0

    :goto_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-virtual {p0}, Lejm;->r()V

    const/4 v7, 0x7

    invoke-virtual {p0, v7, v1, v2}, Lejm;->a(IJ)V

    const-string v1, "Internal init done: storage state %d"

    invoke-direct {p0}, Lejm;->B()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-boolean v1, p1, Leko;->a:Z

    iget-boolean v2, p1, Leko;->c:Z

    iget-object v7, p0, Lejm;->c:Lemz;

    new-instance v8, Lekc;

    invoke-direct {v8, p0, v1, v2, v0}, Lekc;-><init>(Lejm;ZZLjava/util/Set;)V

    const-wide/16 v0, 0x0

    invoke-virtual {v7, v8, v0, v1}, Lemz;->a(Lenf;J)Lenf;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v3, v4}, Lejm;->a(IJ)V

    iget-object v0, p0, Lejm;->m:Leji;

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/Debug;->threadCpuTimeNanos()J

    move-result-wide v2

    sub-long/2addr v2, v5

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-interface {v0, v1, v2}, Leji;->a(II)V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_15
    move-object v0, v2

    goto :goto_9

    :cond_16
    move v0, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Lels;)Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, p1}, Leiy;->a(Lels;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lejm;->a(Ljava/lang/String;Lels;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    move v0, v3

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lejm;->i:Lelt;

    iget-object v4, v0, Lelt;->b:Ljava/util/Map;

    monitor-enter v4

    if-eqz p1, :cond_3

    :try_start_0
    iget-object v0, v0, Lelt;->b:Ljava/util/Map;

    iget-object v5, p1, Lels;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_4

    :goto_2
    invoke-static {v3}, Lbkm;->a(Z)V

    iget-object v2, p1, Lels;->j:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p1, Lels;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lema;->a()Lema;

    move-result-object v0

    invoke-virtual {p1, v0}, Lels;->a(Lema;)V
    :try_end_2
    .catch Lemb; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p1}, Lels;->g()V

    :cond_3
    monitor-exit v4

    return v1

    :cond_4
    move v3, v2

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v1, Lemd;

    invoke-direct {v1, v0}, Lemd;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1
.end method

.method final a(Ljava/lang/String;Lels;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lejm;->o()V

    const-string v2, "Removing corpus key %s for package %s"

    iget-object v3, p2, Lels;->a:Ljava/lang/String;

    invoke-static {v2, p1, v3}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->h(Ljava/lang/String;)Z

    move-result v2

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, p1, p2}, Leiy;->a(Ljava/lang/String;Lels;)I

    move-result v3

    if-gez v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.gms.icing.IME_NOTIFICATION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "type"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "corpus"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lejm;->c(Landroid/content/Intent;)V

    :cond_2
    iget-object v2, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->d(I)Lehr;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v0, "Failed to delete corpus key %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v2, "unregister_failed"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, v2}, Leiy;->a(Lehr;)Z

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1, p2}, Leiy;->b(Ljava/lang/String;Lels;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "Failed to completely deactivate corpus key %s"

    invoke-static {v0, p1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v2, "unregister_failed"

    invoke-interface {v0, v2}, Leji;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {p2, p3, p4}, Lekx;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "Bad request indexing args: %s"

    invoke-static {v2, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lejm;->x()V

    iget-object v1, p0, Lejm;->i:Lelt;

    invoke-virtual {v1, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v1

    iget-object v2, p0, Lejm;->g:Leiy;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p2, v3, v0

    invoke-virtual {v2, v1, v3, v0}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v2, Lekn;

    invoke-direct {v2, p0, v0, p3, p4}, Lekn;-><init>(Lejm;Ljava/util/Set;J)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    check-cast v0, Lekn;

    invoke-virtual {v0}, Lekn;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final a([BZ)Z
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->e:Lejk;

    invoke-virtual {v0, p1}, Lejk;->a([B)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lejm;->n()Lenh;

    :cond_1
    return v0
.end method

.method public final a()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 4

    invoke-virtual {p0}, Lejm;->d()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    move-result-object v1

    array-length v0, v1

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public final a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-static {}, Lekx;->a()Ljava/lang/String;

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lejm;->b(Lely;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final b(Ljava/lang/String;IZLeix;J)I
    .locals 14

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->g(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v8, 0x2

    :cond_0
    :goto_0
    return v8

    :cond_1
    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v3

    iget v2, v3, Lehh;->a:I

    move/from16 v0, p2

    if-eq v2, v0, :cond_2

    const/4 v8, 0x2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->f(Ljava/lang/String;)Lehk;

    move-result-object v6

    invoke-direct {p0}, Lejm;->B()I

    move-result v4

    if-eqz p3, :cond_6

    if-nez v4, :cond_5

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    const-string v5, "Cannot sync trimmable corpus: %s"

    invoke-static {v4}, Lekt;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_3
    :goto_2
    if-nez v2, :cond_8

    iget-boolean v2, v6, Lehk;->e:Z

    if-nez v2, :cond_4

    const/4 v2, 0x1

    iput-boolean v2, v6, Lehk;->e:Z

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1, v6}, Leiy;->a(Ljava/lang/String;Lehk;)V

    :cond_4
    const/4 v8, 0x2

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x1

    if-gt v4, v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    if-nez v2, :cond_3

    const-string v5, "Cannot sync untrimmable corpus: %s"

    invoke-static {v4}, Lekt;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    iget-boolean v2, v6, Lehk;->e:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    iput-boolean v2, v6, Lehk;->e:Z

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1, v6}, Leiy;->a(Ljava/lang/String;Lehk;)V

    :cond_9
    iget v8, v6, Lehk;->f:I

    iget-wide v4, v6, Lehk;->a:J

    const-wide/16 v9, 0x0

    cmp-long v2, v4, v9

    if-gez v2, :cond_a

    const-wide/16 v4, 0x0

    :cond_a
    new-instance v7, Lekv;

    iget-object v2, v6, Lehk;->c:[Lehl;

    invoke-direct {v7, v2, v4, v5}, Lekv;-><init>([Lehl;J)V

    move-object v2, p0

    move-object/from16 v6, p4

    :try_start_0
    invoke-direct/range {v2 .. v7}, Lejm;->a(Lehh;JLeix;Lekv;)Z
    :try_end_0
    .catch Lelf; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_c

    const/4 v2, 0x2

    move v8, v2

    move v9, v3

    :goto_4
    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v6, v7, Lekv;->b:[Lehl;

    array-length v10, v6

    const/4 v2, 0x0

    move v3, v2

    :goto_5
    if-ge v3, v10, :cond_12

    aget-object v11, v6, v3

    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    iget-object v12, v11, Lehl;->a:Ljava/lang/String;

    invoke-interface {v2, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    if-eqz v2, :cond_b

    iget v12, v11, Lehl;->b:I

    const/4 v13, 0x0

    aget v2, v2, v13

    add-int/2addr v2, v12

    iput v2, v11, Lehl;->b:I

    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    iget-object v11, v11, Lehl;->a:Ljava/lang/String;

    invoke-interface {v2, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_5

    :cond_c
    :try_start_1
    iget-wide v8, v7, Lekv;->c:J
    :try_end_1
    .catch Lelf; {:try_start_1 .. :try_end_1} :catch_1

    cmp-long v2, v8, v4

    if-lez v2, :cond_d

    const/4 v2, 0x1

    move v8, v2

    move v9, v3

    goto :goto_4

    :cond_d
    const/4 v2, 0x0

    move v8, v2

    move v9, v3

    goto :goto_4

    :catch_0
    move-exception v2

    move v3, v8

    :goto_6
    invoke-virtual {v2}, Lelf;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    const-string v6, "Cursor call threw an exception"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2, v6, v8}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    instance-of v6, v2, Landroid/os/DeadObjectException;

    if-eqz v6, :cond_10

    iget-object v2, p0, Lejm;->m:Leji;

    const-string v6, "cursor_died_exception"

    invoke-interface {v2, v6}, Leji;->a(Ljava/lang/String;)V

    const-string v2, "cursor-died"

    invoke-virtual {v7, v2}, Lekv;->a(Ljava/lang/String;)V

    if-nez v3, :cond_e

    const/4 v2, 0x3

    const-string v3, "Indexing content provider failed; will retry %d times"

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3, v6}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_7
    const/4 v3, 0x2

    move v8, v3

    move v9, v2

    goto :goto_4

    :cond_e
    add-int/lit8 v2, v3, -0x1

    if-nez v2, :cond_f

    iget-object v3, p0, Lejm;->m:Leji;

    const-string v6, "cursor_retries_failed"

    invoke-interface {v3, v6}, Leji;->a(Ljava/lang/String;)V

    :cond_f
    const-string v3, "Indexing content provider failed again; %d retries remaining"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3, v6}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_7

    :cond_10
    instance-of v2, v2, Ljava/lang/SecurityException;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lejm;->m:Leji;

    const-string v3, "cursor_security_exception"

    invoke-interface {v2, v3}, Leji;->a(Ljava/lang/String;)V

    :goto_8
    const-string v2, "cursor-exception"

    invoke-virtual {v7, v2}, Lekv;->a(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_7

    :cond_11
    iget-object v2, p0, Lejm;->m:Leji;

    const-string v3, "cursor_other_exception"

    invoke-interface {v2, v3}, Leji;->a(Ljava/lang/String;)V

    goto :goto_8

    :cond_12
    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    iget-object v2, v7, Lekv;->b:[Lehl;

    array-length v3, v2

    iget-object v2, v7, Lekv;->b:[Lehl;

    iget-object v6, v7, Lekv;->b:[Lehl;

    array-length v6, v6

    iget-object v10, v7, Lekv;->a:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v10

    add-int/2addr v6, v10

    invoke-static {v2, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lehl;

    iput-object v2, v7, Lekv;->b:[Lehl;

    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v6, v3

    :goto_9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/util/Map$Entry;

    new-instance v11, Lehl;

    invoke-direct {v11}, Lehl;-><init>()V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v11, Lehl;->a:Ljava/lang/String;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    const/4 v3, 0x0

    aget v2, v2, v3

    iput v2, v11, Lehl;->b:I

    iget-object v3, v7, Lekv;->b:[Lehl;

    add-int/lit8 v2, v6, 0x1

    aput-object v11, v3, v6

    move v6, v2

    goto :goto_9

    :cond_13
    iget-object v2, v7, Lekv;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    iget-object v2, v7, Lekv;->b:[Lehl;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lekw;->a:Lekw;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_14
    iget-wide v11, v7, Lekv;->c:J

    cmp-long v2, v11, v4

    if-lez v2, :cond_16

    const/4 v2, 0x1

    move v10, v2

    :goto_a
    if-eqz v10, :cond_17

    const-string v2, "Indexed %s from %d to %d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v3, v6

    const/4 v6, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-wide v5, v7, Lekv;->c:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lehe;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_b
    iget-object v2, p0, Lejm;->g:Leiy;

    iget-object v6, v7, Lekv;->b:[Lehl;

    move-object v3, p1

    move-wide v4, v11

    move v7, v9

    invoke-virtual/range {v2 .. v7}, Leiy;->a(Ljava/lang/String;J[Lehl;I)V

    if-lez v9, :cond_15

    const-string v2, "Retrying indexing in %dms"

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v2, p0, Lejm;->s:Leiv;

    move-wide/from16 v0, p5

    invoke-interface {v2, v0, v1}, Leiv;->a(J)V

    :cond_15
    if-eqz v10, :cond_0

    iget-object v2, p0, Lejm;->g:Leiy;

    invoke-virtual {v2, p1}, Leiy;->h(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lejm;->l:Ljava/util/Set;

    iget-object v3, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, p1}, Leiy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_16
    const/4 v2, 0x0

    move v10, v2

    goto :goto_a

    :cond_17
    const-string v2, "Query from %s found nothing"

    invoke-static {v2, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_b

    :catch_1
    move-exception v2

    goto/16 :goto_6
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 7

    invoke-static {p2}, Lekx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    iget-object v1, p0, Lejm;->i:Lelt;

    invoke-virtual {v1, v0}, Lelt;->a(Lely;)Lels;

    move-result-object v3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v6, p0, Lejm;->c:Lemz;

    new-instance v0, Lekm;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lekm;-><init>(Lejm;Ljava/lang/String;Lels;Ljava/util/List;Ljava/util/List;)V

    const-wide/16 v1, 0x0

    invoke-virtual {v6, v0, v1, v2}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    check-cast v0, Lekm;

    invoke-virtual {v0}, Lekm;->e()Ljava/lang/Object;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v1, "content_provider_uris"

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Z

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    aput-boolean v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "success"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    return-object v2
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lejm;->z()Lenh;

    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 4

    const-wide/16 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Got storage broadcast: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->b(Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Couldn\'t handle %s intent due to initialization failure."

    invoke-static {v1, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Leju;

    invoke-direct {v1, p0}, Leju;-><init>(Lejm;)V

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0

    :cond_1
    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lejv;

    invoke-direct {v1, p0}, Lejv;-><init>(Lejm;)V

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0

    :cond_2
    new-instance v1, Lemd;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown intent action "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lemd;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method final b(Lels;)V
    .locals 5

    invoke-virtual {p0}, Lejm;->o()V

    invoke-virtual {p1}, Lels;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1, v0}, Leiy;->c(Ljava/lang/String;)Lema;

    move-result-object v3

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_0

    invoke-virtual {v3}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->a:Lehh;

    invoke-static {}, Leim;->c()[Leim;

    move-result-object v4

    iput-object v4, v1, Lehh;->l:[Leim;

    :try_start_0
    iget-object v4, p0, Lejm;->g:Leiy;

    invoke-virtual {v3, v1}, Lema;->a(Ljava/lang/Object;)Lema;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Leiy;->a(Ljava/lang/String;Lema;)V
    :try_end_0
    .catch Lemb; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set corpus config on reparse"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->m:Leji;

    const-string v1, "reparse_sourcecheck_failed"

    invoke-interface {v0, v1}, Leji;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lejm;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)V

    return-void
.end method

.method public final b([Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lejr;

    invoke-direct {v1, p0, p1}, Lejr;-><init>(Lejm;[Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p2}, Lekx;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p2, v3, v4}, Lema;->a(Ljava/lang/Object;J)Lema;

    move-result-object v3

    iget-object v4, p0, Lejm;->c:Lemz;

    new-instance v5, Lekk;

    invoke-direct {v5, p0, v0, v3}, Lekk;-><init>(Lejm;Lely;Lema;)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    check-cast v0, Lekk;

    invoke-virtual {v0}, Lekk;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    if-eqz v0, :cond_3

    const-string v3, "Client exception"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    instance-of v3, v0, Lemb;

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    instance-of v3, v0, Ljava/lang/SecurityException;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/SecurityException;

    throw v0

    :cond_2
    instance-of v3, v0, Leme;

    if-eqz v3, :cond_3

    const-string v1, "Internal error"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    :goto_0
    return v2

    :cond_3
    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, Lbiq;->a(Z)V

    move v2, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    iget-object v1, p0, Lejm;->g:Leiy;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-virtual {v1, v0, v2, v3}, Leiy;->a(Lely;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Leiy;->b(Ljava/lang/String;)Lehj;

    move-result-object v0

    iget-object v0, v0, Lehj;->a:Lehh;

    invoke-static {v0}, Leiy;->c(Lehh;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 4

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lejp;

    invoke-direct {v0, p0}, Lejp;-><init>(Lejm;)V

    iget-object v1, p0, Lejm;->c:Lemz;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    invoke-virtual {v0}, Lenf;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/StorageStats;

    return-object v0
.end method

.method protected final c(Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->i:Lelt;

    iget-object v0, v0, Lelt;->a:Lelu;

    invoke-static {}, Lelu;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final d()[Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
    .locals 6

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    invoke-virtual {v0}, Lelv;->a()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1}, Leiy;->j()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v0, v2}, Lejm;->a(Lelv;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehh;

    iget-object v5, v0, Lehh;->d:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laht;

    if-nez v1, :cond_2

    iget-object v2, p0, Lejm;->i:Lelt;

    invoke-virtual {v2, v5}, Lelt;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Laht;

    invoke-direct {v2}, Laht;-><init>()V

    iget-object v1, p0, Lejm;->i:Lelt;

    invoke-virtual {v1, v5}, Lelt;->b(Ljava/lang/String;)Lels;

    move-result-object v1

    iget-object v1, v1, Lels;->d:Lema;

    invoke-virtual {v1}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iput-object v1, v2, Laht;->a:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v2

    :cond_2
    if-eqz v1, :cond_1

    invoke-static {v0}, Leiy;->b(Lehh;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, v0, Lehh;->b:Ljava/lang/String;

    iget-object v1, v1, Laht;->b:Ljava/util/Map;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laht;

    new-instance v4, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    iget-object v5, v0, Laht;->a:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v0, v0, Laht;->b:Ljava/util/Map;

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/Map;)V

    aput-object v4, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    return-object v2
.end method

.method public final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 14

    const-string v1, "Icing on the Cake"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Init failed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_4

    aget-object v5, p3, v3

    const-string v6, "native"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v2, 0x1

    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const-string v6, "verbose"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move v3, v1

    move v4, v2

    :try_start_0
    iget-object v1, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "Apk version code: %d\n"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Apk version name: %s\n"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v1, v5, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    const-string v1, "Version: %d\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lejm;->e:Lejk;

    invoke-virtual {v6}, Lejk;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v1, "Extension:\n"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    iget-object v1, p0, Lejm;->v:Leky;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Leky;->a(Ljava/io/PrintWriter;)V

    const-string v1, "Storage state: %s\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-direct {p0}, Lejm;->B()I

    move-result v6

    invoke-static {v6}, Lekt;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    iget-object v1, p0, Lejm;->g:Leiy;

    invoke-virtual {v1}, Leiy;->n()Lehg;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    iget-object v5, p0, Lejm;->g:Leiy;

    invoke-virtual {v5}, Leiy;->o()J

    move-result-wide v5

    invoke-direct {v2, v5, v6}, Ljava/util/Date;-><init>(J)V

    iget-object v5, p0, Lejm;->g:Leiy;

    invoke-virtual {v5}, Leiy;->m()Lehr;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v7, v5, Lehr;->e:J

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    new-instance v7, Ljava/util/Date;

    iget-object v8, p0, Lejm;->e:Lejk;

    invoke-virtual {v8}, Lejk;->d()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    new-instance v8, Ljava/util/Date;

    iget-wide v9, v5, Lehr;->d:J

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    new-instance v9, Ljava/util/Date;

    iget-wide v10, v1, Lehg;->a:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    const/4 v10, 0x2

    const/4 v11, 0x2

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v10, v11, v12}, Ljava/text/SimpleDateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v10

    const-string v11, "Created \"%s\"\n"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v10, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v12, v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v11, v12}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Committed \"%s\"\n"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v10, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v11, v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v11}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Maintained \"%s\"\n"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v10, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Flushed \"%s\" num docs %d\n"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v10, v8}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v5, v5, Lehr;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v2, "Compacted \"%s\" num docs %d old %d trimmed %d err %d\n"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v10, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, v1, Lehg;->b:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget v7, v1, Lehg;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget v7, v1, Lehg;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    iget v1, v1, Lehg;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    iget-object v1, p0, Lejm;->f:Lelj;

    invoke-virtual {v1}, Lelj;->g()J

    move-result-wide v1

    iget-object v5, p0, Lejm;->f:Lelj;

    invoke-virtual {v5}, Lelj;->h()J

    move-result-wide v5

    const-string v7, "Disk usage %s budget %s free frac %.3f%% index free frac %.3f%%\n"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1, v2}, Lell;->a(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v5, v6}, Lell;->a(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    cmp-long v10, v1, v5

    if-lez v10, :cond_5

    const-wide/16 v1, 0x0

    :goto_4
    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    aput-object v1, v8, v9

    const/4 v1, 0x3

    iget-object v2, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v5

    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v9

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v8, v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v1, "\nCorpora:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lejm;->g:Leiy;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Leiy;->a(Ljava/io/PrintWriter;)V

    const-string v1, "\nClientInfo:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lejm;->i:Lelt;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lelt;->a(Ljava/io/PrintWriter;)V

    const-string v1, "\nCorpus Usage Stats:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()Leit;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v2, v1, Leit;->a:[Leiu;

    array-length v5, v2

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v5, :cond_7

    aget-object v6, v2, v1

    const-string v7, "id: %d\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v6, Leiu;->a:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v7, "docs: %d\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v6, Leiu;->b:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v7, "size: %s\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v6, Leiu;->d:J

    invoke-static {v10, v11}, Lell;->a(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v7, "deleted docs: %d\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, v6, Leiu;->c:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v7, "deleted size: %d\n"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v6, Leiu;->e:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v8, v9

    move-object/from16 v0, p2

    invoke-virtual {v0, v7, v8}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual/range {p2 .. p2}, Ljava/io/PrintWriter;->println()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    long-to-double v10, v5

    long-to-double v1, v1

    sub-double v1, v10, v1

    long-to-double v5, v5

    div-double/2addr v1, v5

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double/2addr v1, v5

    goto/16 :goto_4

    :cond_6
    const-string v1, "\nError getting usage stats"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_7
    if-eqz v4, :cond_0

    const-string v1, "\nNative Index:"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto/16 :goto_3
.end method

.method public final e()[I
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->e:Lejk;

    iget-object v0, v0, Lejk;->b:Lejl;

    invoke-virtual {v0}, Lejl;->a()[I

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    invoke-virtual {v0}, Lely;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->s()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()[I
    .locals 2

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->e:Lejk;

    iget-object v0, v0, Lejk;->c:Lejl;

    invoke-virtual {v0}, Lejl;->a()[I

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-static {}, Lekx;->a()Ljava/lang/String;

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0, p1}, Lelt;->a(Ljava/lang/String;)Lely;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lejm;->b(Lely;I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final g(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Lejm;->o()V

    invoke-direct {p0, p1}, Lejm;->m(Ljava/lang/String;)V

    return-void
.end method

.method public final g()Z
    .locals 4

    invoke-direct {p0}, Lejm;->x()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->a()Lelv;

    move-result-object v0

    iget-boolean v0, v0, Lelv;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Access denied"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lejs;

    invoke-direct {v1, p0}, Lejs;-><init>(Lejm;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    check-cast v0, Lejs;

    invoke-virtual {v0}, Lejs;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final h()Landroid/os/IBinder;
    .locals 1

    new-instance v0, Lekp;

    invoke-direct {v0, p0}, Lekp;-><init>(Lejm;)V

    invoke-virtual {v0}, Lekp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final h(Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lejm;->o()V

    invoke-direct {p0}, Lejm;->x()V

    const-string v0, "handlePackageUpdating %s"

    invoke-static {v0, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->i:Lelt;

    iget-object v1, v0, Lelt;->b:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0, p1}, Lelt;->b(Ljava/lang/String;)Lels;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_1
    invoke-static {}, Lema;->a()Lema;

    move-result-object v3

    invoke-virtual {v2, v3}, Lels;->a(Lema;)V
    :try_end_1
    .catch Lemb; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_1

    iget-object v0, v0, Lelt;->a:Lelu;

    iget-object v0, v0, Lelu;->d:Lelx;

    invoke-interface {v0, v2}, Lelx;->a(Lels;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v2, Lemd;

    invoke-direct {v2, v0}, Lemd;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .locals 4

    iget-object v0, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    iget-object v0, p0, Lejm;->c:Lemz;

    iget-object v1, v0, Lemz;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Lemz;->c()V

    iget v2, v0, Lemz;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lemz;->c:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, v0, Lemz;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    iget-object v1, v0, Lemz;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget v2, v0, Lemz;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lemz;->c:I

    invoke-virtual {v0}, Lemz;->c()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Lekj;

    invoke-direct {v1, p0}, Lekj;-><init>(Lejm;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final i(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Lejm;->o()V

    invoke-direct {p0, p1}, Lejm;->m(Ljava/lang/String;)V

    return-void
.end method

.method final j(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lejm;->o()V

    invoke-direct {p0}, Lejm;->x()V

    const-string v0, "handlePackageAdded %s"

    invoke-static {v0, p1}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    new-instance v0, Lelz;

    iget-object v1, p0, Lejm;->i:Lelt;

    iget-object v2, p0, Lejm;->n:Lelw;

    invoke-direct {v0, v1, v2}, Lelz;-><init>(Lelt;Lelw;)V

    invoke-virtual {v0, p1}, Lelz;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Package "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method final j()Z
    .locals 1

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    iget-object v0, p0, Lejm;->c:Lemz;

    invoke-virtual {v0}, Lemz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lejm;->j()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    :try_start_0
    invoke-virtual {p0}, Lejm;->j()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    iget-object v1, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejm;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method final k(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0, p1}, Leiy;->d(Ljava/lang/String;)Lehh;

    move-result-object v2

    if-nez v2, :cond_1

    new-array v0, v1, [Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, v2, Lehh;->k:[Leii;

    array-length v3, v0

    new-array v0, v3, [Ljava/lang/String;

    :goto_0
    if-ge v1, v3, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "section_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lehh;->k:[Leii;

    aget-object v5, v5, v1

    iget-object v5, v5, Leii;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method final l()V
    .locals 2

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->v:Leky;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Leky;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lejm;->A()V

    :cond_0
    return-void
.end method

.method final m()V
    .locals 6

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->v:Leky;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leky;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lejm;->A()V

    :cond_0
    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->e()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p0}, Lejm;->o()V

    const-wide/16 v2, 0x3e8

    const-wide/32 v4, 0x493e0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lejm;->a(Ljava/util/Set;JJ)V

    return-void
.end method

.method public final n()Lenh;
    .locals 4

    iget-object v0, p0, Lejm;->c:Lemz;

    new-instance v1, Leka;

    invoke-direct {v1, p0}, Leka;-><init>(Lejm;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lemz;->a(Lenf;J)Lenf;

    move-result-object v0

    return-object v0
.end method

.method final o()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lejm;->c:Lemz;

    invoke-virtual {v0}, Lemz;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    const-string v1, "Not in worker thread"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, "Must be in worker thread"

    invoke-static {v3, v0}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method final p()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lejm;->o()V

    iput-object v1, p0, Lejm;->e:Lejk;

    iput-object v1, p0, Lejm;->f:Lelj;

    iput-object v1, p0, Lejm;->g:Leiy;

    iput-object v1, p0, Lejm;->i:Lelt;

    iput-object v1, p0, Lejm;->v:Leky;

    iput-object v1, p0, Lejm;->h:Lemv;

    invoke-virtual {p0}, Lejm;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->d()V

    iput-object v1, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    :cond_0
    return-void
.end method

.method final q()V
    .locals 1

    invoke-virtual {p0}, Lejm;->o()V

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->f()V

    iget-object v0, p0, Lejm;->g:Leiy;

    invoke-virtual {v0}, Leiy;->l()V

    :cond_0
    return-void
.end method

.method final r()V
    .locals 4

    invoke-virtual {p0}, Lejm;->o()V

    new-instance v1, Lelz;

    iget-object v0, p0, Lejm;->i:Lelt;

    iget-object v2, p0, Lejm;->n:Lelw;

    invoke-direct {v1, v0, v2}, Lelz;-><init>(Lelt;Lelw;)V

    iget-object v0, v1, Lelz;->a:Lelt;

    invoke-virtual {v0}, Lelt;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lelz;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Package %s no longer installed"

    invoke-static {v3, v0}, Lehe;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v3, v1, Lelz;->b:Lelw;

    invoke-interface {v3, v0}, Lelw;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method final s()V
    .locals 1

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->h()Lehr;

    move-result-object v0

    invoke-direct {p0, v0}, Lejm;->a(Lehr;)V

    return-void
.end method

.method final t()V
    .locals 6

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    invoke-virtual {p0}, Lejm;->o()V

    const-string v0, "Starting compaction min disk %.3f%% min index %.3f%%"

    iget-object v1, p0, Lejm;->f:Lelj;

    invoke-virtual {v1}, Lelj;->f()D

    move-result-wide v1

    mul-double/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iget-object v2, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lehe;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->j()Lehr;

    move-result-object v0

    invoke-direct {p0, v0}, Lejm;->b(Lehr;)V

    return-void
.end method

.method final u()V
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lejm;->o()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->e:Lejk;

    iget-object v14, v2, Lejk;->e:Ljava/lang/Object;

    monitor-enter v14

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->e:Lejk;

    invoke-virtual {v2}, Lejk;->c()Z

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lejm;->o()V

    new-instance v4, Lelz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->i:Lelt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lejm;->n:Lelw;

    invoke-direct {v4, v2, v3}, Lelz;-><init>(Lelt;Lelw;)V

    iget-object v2, v4, Lelz;->a:Lelt;

    iget-object v2, v2, Lelt;->a:Lelu;

    iget-object v2, v2, Lelu;->c:Landroid/content/pm/PackageManager;

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateResources: found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " total apps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lehe;->a(Ljava/lang/String;)I

    iget-object v2, v4, Lelz;->a:Lelt;

    invoke-virtual {v2}, Lelt;->c()Ljava/util/Set;

    move-result-object v6

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    iget-object v8, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v8, v8, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v8, :cond_0

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v6, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    invoke-virtual {v4, v2}, Lelz;->a(Landroid/content/pm/PackageInfo;)V

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_0
    const-string v8, "Package %s is disabled"

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v8, v2}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v14

    throw v2

    :cond_1
    :try_start_1
    const-string v2, "Apps that are now uninstalled (%d): %s"

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3, v6}, Lehe;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v5, v4, Lelz;->b:Lelw;

    invoke-interface {v5, v2}, Lelw;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->g:Leiy;

    invoke-virtual {v2}, Leiy;->p()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->b:Landroid/content/Context;

    sget-object v5, Lejm;->r:Ljava/lang/String;

    const-wide/32 v8, 0x240c8400

    invoke-static {v4, v5, v8, v9}, Lbhv;->a(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    add-long/2addr v2, v4

    cmp-long v2, v8, v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    move v13, v2

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->f:Lelj;

    invoke-virtual {v2}, Lelj;->g()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->f:Lelj;

    invoke-virtual {v2}, Lelj;->h()J

    move-result-wide v8

    cmp-long v2, v15, v8

    if-lez v2, :cond_7

    const-wide/16 v2, 0x0

    move-wide v5, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()D

    move-result-wide v10

    const-wide/16 v2, 0x0

    cmpl-double v2, v10, v2

    if-ltz v2, :cond_8

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpg-double v2, v10, v2

    if-gez v2, :cond_8

    long-to-double v2, v15

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    sub-double v17, v17, v10

    div-double v2, v2, v17

    double-to-long v2, v2

    move-wide v3, v2

    :goto_5
    sget-wide v17, Lejm;->o:D

    cmpg-double v2, v10, v17

    if-lez v2, :cond_3

    sget-wide v17, Lejm;->p:D

    cmpg-double v2, v5, v17

    if-gtz v2, :cond_9

    :cond_3
    const/4 v2, 0x1

    move v12, v2

    :goto_6
    invoke-static {v8, v9, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-double v2, v2

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    sget-wide v19, Lejm;->q:D

    sub-double v17, v17, v19

    mul-double v2, v2, v17

    double-to-long v0, v2

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->v:Leky;

    invoke-virtual {v2, v13}, Leky;->b(Z)Z

    move-result v2

    if-nez v2, :cond_4

    if-eqz v7, :cond_5

    :cond_4
    invoke-direct/range {p0 .. p0}, Lejm;->A()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v2, v13}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->e:Lejk;

    invoke-virtual {v2, v7}, Lejk;->a(Z)V

    const-string v2, "Performing maintenance usage %s budget %s free %.3f%% index free %.3f%% purge? %s target %s"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v4

    const/4 v4, 0x2

    const-wide/high16 v7, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double/2addr v5, v10

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lehe;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->v:Leky;

    invoke-virtual {v2}, Leky;->b()Lehd;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lejm;->m:Leji;

    invoke-direct/range {p0 .. p0}, Lejm;->B()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lejm;->f:Lelj;

    invoke-virtual {v4}, Lelj;->j()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lejm;->f:Lelj;

    invoke-virtual {v6}, Lelj;->i()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lejm;->f:Lelj;

    invoke-virtual {v8}, Lelj;->k()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lejm;->f:Lelj;

    invoke-virtual {v10}, Lelj;->c()Z

    move-result v10

    if-nez v11, :cond_a

    const/4 v11, 0x0

    :goto_7
    invoke-interface/range {v2 .. v11}, Leji;->a(IJJJZI)V

    if-eqz v12, :cond_b

    cmp-long v2, v17, v15

    if-gez v2, :cond_b

    long-to-double v2, v15

    move-wide/from16 v0, v17

    long-to-double v4, v0

    sub-double/2addr v2, v4

    long-to-double v4, v15

    div-double/2addr v2, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lejm;->a(D)V

    :goto_8
    monitor-exit v14

    return-void

    :cond_6
    const/4 v2, 0x0

    move v13, v2

    goto/16 :goto_3

    :cond_7
    sub-long v2, v8, v15

    long-to-double v2, v2

    long-to-double v4, v8

    div-double/2addr v2, v4

    move-wide v5, v2

    goto/16 :goto_4

    :cond_8
    const-wide v2, 0x7fffffffffffffffL

    move-wide v3, v2

    goto/16 :goto_5

    :cond_9
    const/4 v2, 0x0

    move v12, v2

    goto/16 :goto_6

    :cond_a
    iget v11, v11, Lehd;->a:I

    goto :goto_7

    :cond_b
    if-eqz v13, :cond_c

    invoke-virtual/range {p0 .. p0}, Lejm;->t()V

    goto :goto_8

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lejm;->s()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_8
.end method

.method public final v()V
    .locals 5

    iget-object v0, p0, Lejm;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lejm;->k()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "Global Search Section Mappings reparsing skipped because init failed"

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lejm;->c:Lemz;

    new-instance v2, Lekg;

    invoke-direct {v2, p0, v0}, Lekg;-><init>(Lejm;Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lemz;->a(Lenf;J)Lenf;

    goto :goto_0
.end method

.method final w()V
    .locals 8

    invoke-virtual {p0}, Lejm;->o()V

    iget-object v0, p0, Lejm;->i:Lelt;

    invoke-virtual {v0}, Lelt;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lels;

    invoke-virtual {v0}, Lels;->h()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t get resources for package: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lels;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Lejm;->g:Leiy;

    invoke-virtual {v5, v1}, Leiy;->c(Ljava/lang/String;)Lema;

    move-result-object v5

    invoke-virtual {v5}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v1, v1, Lehj;->b:Lehk;

    iget v1, v1, Lehk;->d:I

    if-nez v1, :cond_2

    invoke-virtual {v5}, Lema;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lehj;

    iget-object v6, v1, Lehj;->a:Lehh;

    iget-object v1, v6, Lehh;->l:[Leim;

    array-length v1, v1

    if-eqz v1, :cond_2

    new-instance v1, Lemh;

    invoke-direct {v1, v6, v3}, Lemh;-><init>(Lehh;Landroid/content/res/Resources;)V

    :try_start_0
    invoke-virtual {v1}, Lemh;->a()V

    invoke-virtual {v1}, Lemh;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {}, Leim;->c()[Leim;

    move-result-object v7

    iput-object v7, v6, Lehh;->l:[Leim;

    if-eqz v1, :cond_3

    iget-object v7, v6, Lehh;->l:[Leim;

    invoke-interface {v1, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Leim;

    iput-object v1, v6, Lehh;->l:[Leim;
    :try_end_0
    .catch Lemc; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    iget-object v1, p0, Lejm;->g:Leiy;

    iget-object v7, v6, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v7}, Leiy;->a(Lels;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_1
    iget-object v7, p0, Lejm;->g:Leiy;

    invoke-virtual {v5, v6}, Lema;->a(Ljava/lang/Object;)Lema;

    move-result-object v5

    invoke-virtual {v7, v1, v5}, Leiy;->a(Ljava/lang/String;Lema;)V
    :try_end_1
    .catch Lemb; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v5, "Failed to set corpus config on reparse"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1, v5, v6}, Lehe;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lejm;->m:Leji;

    const-string v5, "reparse_sourcecheck_failed"

    invoke-interface {v1, v5}, Leji;->a(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Error while reparsing mapping for packageName = "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lels;->a:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", corpusName = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v6, Lehh;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", error = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lemc;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lehe;->d(Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    return-void
.end method
