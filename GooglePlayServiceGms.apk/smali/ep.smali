.class public final Lep;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Let;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Les;

    invoke-direct {v0}, Les;-><init>()V

    sput-object v0, Lep;->a:Let;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Leq;

    invoke-direct {v0}, Leq;-><init>()V

    sput-object v0, Lep;->a:Let;

    goto :goto_0
.end method

.method public static a(Landroid/view/KeyEvent;)Z
    .locals 3

    sget-object v0, Lep;->a:Let;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Let;->a(II)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/KeyEvent;)Z
    .locals 2

    sget-object v0, Lep;->a:Let;

    invoke-virtual {p0}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-interface {v0, v1}, Let;->b(I)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/KeyEvent;)V
    .locals 1

    sget-object v0, Lep;->a:Let;

    invoke-interface {v0, p0}, Let;->a(Landroid/view/KeyEvent;)V

    return-void
.end method
