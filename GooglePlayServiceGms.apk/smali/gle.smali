.class public final Lgle;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lgle;->d:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lgld;
    .locals 5

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAclentryResourceEntity;

    iget-object v1, p0, Lgle;->d:Ljava/util/Set;

    iget-object v2, p0, Lgle;->a:Ljava/lang/String;

    iget-object v3, p0, Lgle;->b:Ljava/lang/String;

    iget-object v4, p0, Lgle;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAclentryResourceEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lgle;
    .locals 2

    iput-object p1, p0, Lgle;->b:Ljava/lang/String;

    iget-object v0, p0, Lgle;->d:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lgle;
    .locals 2

    iput-object p1, p0, Lgle;->c:Ljava/lang/String;

    iget-object v0, p0, Lgle;->d:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
