.class public final Lbuw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lbuv;

.field public final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lbuv;Ljava/lang/String;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbuv;

    iput-object v0, p0, Lbuw;->a:Lbuv;

    iput-object p2, p0, Lbuw;->c:Ljava/lang/String;

    iput-wide p3, p0, Lbuw;->b:J

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lbuv;Ljava/lang/String;J)Lbuw;
    .locals 1

    new-instance v0, Lbuw;

    invoke-direct {v0, p0, p1, p2, p3}, Lbuw;-><init>(Lbuv;Ljava/lang/String;J)V

    return-object v0
.end method

.method public static a(Lbuw;Ljava/lang/String;)Lbuw;
    .locals 6

    new-instance v0, Lbuw;

    iget-object v1, p0, Lbuw;->a:Lbuv;

    iget-wide v2, p0, Lbuw;->b:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-direct {v0, v1, p1, v2, v3}, Lbuw;-><init>(Lbuv;Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbuw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbuw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lbuw;->c:Ljava/lang/String;

    if-nez v3, :cond_2

    move v3, v2

    :goto_1
    if-ne v0, v3, :cond_3

    :goto_2
    invoke-static {v2}, Lbkm;->b(Z)V

    iget-object v0, p0, Lbuw;->c:Ljava/lang/String;

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    iget-wide v0, p0, Lbuw;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    invoke-virtual {p0}, Lbuw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbuw;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lbuw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lbuw;

    iget-object v2, p0, Lbuw;->a:Lbuv;

    iget-object v3, p1, Lbuw;->a:Lbuv;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbuw;->c:Ljava/lang/String;

    iget-object v3, p1, Lbuw;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lbuw;->b:J

    iget-wide v4, p1, Lbuw;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lbuw;->a:Lbuv;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lbuw;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lbuw;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "FeedState[feed=%s, nextPageToken=%s, numPagesRetrieved=%d]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbuw;->a:Lbuv;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbuw;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lbuw;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
