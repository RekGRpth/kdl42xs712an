.class public final Lbsh;
.super Lbqz;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbqz;-><init>(Landroid/os/IInterface;)V

    iput-object p1, p0, Lbsh;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/api/DriveAsyncService;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbsh;->b:Ljava/lang/String;

    const-string v1, "Drive.UninstallOperation"

    const-string v2, "Uninstall %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcbv;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {p1, v0}, Lbox;->e(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    invoke-virtual {v0}, Lcoy;->f()Lcfz;

    move-result-object v0

    iget-object v1, p0, Lbsh;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcfz;->h(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Drive.UninstallOperation"

    const-string v2, "Package still installed %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
