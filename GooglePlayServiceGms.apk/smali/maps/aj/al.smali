.class final Lmaps/aj/al;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/HashMap;

.field private b:[Lmaps/aj/al;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(III)I
    .locals 2

    shr-int v0, p0, p2

    and-int/lit8 v0, v0, 0x1

    shr-int v1, p1, p2

    and-int/lit8 v1, v1, 0x1

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Lmaps/ao/b;)Lmaps/aj/aj;
    .locals 1

    iget-object v0, p0, Lmaps/aj/al;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/aj/al;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/aj;

    goto :goto_0
.end method

.method public final a(I)Lmaps/aj/al;
    .locals 1

    iget-object v0, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final a(IIILmaps/ao/b;Lmaps/aj/aj;)V
    .locals 3

    :goto_0
    if-gtz p3, :cond_1

    iget-object v0, p0, Lmaps/aj/al;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/al;->a:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lmaps/aj/al;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p4, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    add-int/lit8 p3, p3, -0x1

    invoke-static {p1, p2, p3}, Lmaps/aj/al;->a(III)I

    move-result v1

    iget-object v0, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    if-nez v0, :cond_2

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/aj/al;

    iput-object v0, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    :cond_2
    iget-object v0, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    aget-object v0, v0, v1

    if-nez v0, :cond_3

    new-instance v0, Lmaps/aj/al;

    invoke-direct {v0}, Lmaps/aj/al;-><init>()V

    iget-object v2, p0, Lmaps/aj/al;->b:[Lmaps/aj/al;

    aput-object v0, v2, v1

    :cond_3
    move-object p0, v0

    goto :goto_0
.end method
