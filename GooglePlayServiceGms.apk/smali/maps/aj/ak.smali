.class public final Lmaps/aj/ak;
.super Ljava/lang/Object;


# static fields
.field private static final b:Lmaps/ac/bt;


# instance fields
.field private final a:Lmaps/aj/al;

.field private final c:Ljava/util/Map;

.field private d:Lmaps/ac/av;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lmaps/ac/bt;

    invoke-direct {v0, v1, v1, v1}, Lmaps/ac/bt;-><init>(III)V

    sput-object v0, Lmaps/aj/ak;->b:Lmaps/ac/bt;

    return-void
.end method

.method private constructor <init>(Lmaps/aj/al;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/aj/ak;->c:Ljava/util/Map;

    iput-object p1, p0, Lmaps/aj/ak;->a:Lmaps/aj/al;

    return-void
.end method

.method public static a(Lmaps/bv/a;)Lmaps/aj/ak;
    .locals 15

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmaps/bv/a;->j(I)I

    move-result v9

    if-nez v9, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Lmaps/bl/a;->a()V

    new-instance v0, Lmaps/aj/al;

    invoke-direct {v0}, Lmaps/aj/al;-><init>()V

    const/4 v1, 0x0

    move v8, v1

    :goto_1
    if-ge v8, v9, :cond_7

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v8}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v10

    const/4 v1, 0x3

    invoke-virtual {v10, v1}, Lmaps/bv/a;->d(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v10, v1}, Lmaps/bv/a;->j(I)I

    move-result v4

    const/4 v1, 0x5

    invoke-virtual {v10, v1}, Lmaps/bv/a;->d(I)I

    move-result v6

    if-lez v4, :cond_2

    new-array v1, v4, [I

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_3

    const/4 v5, 0x2

    invoke-virtual {v10, v5, v2}, Lmaps/bv/a;->b(II)I

    move-result v5

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v1, v3, 0x1

    sub-int/2addr v1, v6

    new-array v1, v1, [I

    const/4 v2, 0x0

    :goto_3
    sub-int v4, v3, v6

    if-gt v2, v4, :cond_3

    add-int v4, v2, v6

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lmaps/bv/a;->d(I)I

    move-result v2

    new-instance v5, Lmaps/aj/aj;

    invoke-direct {v5, v1, v6, v2, v3}, Lmaps/aj/aj;-><init>([IIII)V

    const/4 v1, 0x4

    invoke-virtual {v10, v1}, Lmaps/bv/a;->j(I)I

    move-result v11

    const/4 v1, 0x0

    move v6, v1

    :goto_4
    if-ge v6, v11, :cond_6

    const/4 v1, 0x4

    invoke-virtual {v10, v1, v6}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v12

    const/4 v1, 0x2

    invoke-virtual {v12, v1}, Lmaps/bv/a;->d(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {v12, v2}, Lmaps/bv/a;->d(I)I

    move-result v2

    const/4 v3, 0x4

    invoke-virtual {v12, v3}, Lmaps/bv/a;->d(I)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v12, v4}, Lmaps/bv/a;->j(I)I

    move-result v13

    new-instance v14, Lmaps/ac/bt;

    invoke-direct {v14, v1, v2, v3}, Lmaps/ac/bt;-><init>(III)V

    const/4 v1, 0x0

    move v7, v1

    :goto_5
    if-ge v7, v13, :cond_5

    const/4 v1, 0x1

    invoke-virtual {v12, v1, v7}, Lmaps/bv/a;->b(II)I

    move-result v1

    invoke-static {v1}, Lmaps/ao/b;->a(I)Lmaps/ao/b;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v14}, Lmaps/ac/bt;->c()I

    move-result v1

    invoke-virtual {v14}, Lmaps/ac/bt;->d()I

    move-result v2

    invoke-virtual {v14}, Lmaps/ac/bt;->b()I

    move-result v3

    invoke-virtual/range {v0 .. v5}, Lmaps/aj/al;->a(IIILmaps/ao/b;Lmaps/aj/aj;)V

    :cond_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_5

    :cond_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_4

    :cond_6
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_1

    :cond_7
    invoke-static {}, Lmaps/bl/a;->b()V

    new-instance v1, Lmaps/aj/ak;

    invoke-direct {v1, v0}, Lmaps/aj/ak;-><init>(Lmaps/aj/al;)V

    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ac/av;Lmaps/ao/b;)Lmaps/aj/aj;
    .locals 6

    iget-object v0, p0, Lmaps/aj/ak;->d:Lmaps/ac/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/ak;->d:Lmaps/ac/av;

    invoke-virtual {v0, p1}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/aj/ak;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/aj;

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/aj/ak;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    invoke-static {p1}, Lmaps/ac/bt;->a(Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lmaps/aj/ak;->b:Lmaps/ac/bt;

    :cond_2
    iput-object p1, p0, Lmaps/aj/ak;->d:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/bt;->c()I

    move-result v3

    invoke-virtual {v0}, Lmaps/ac/bt;->d()I

    move-result v2

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v1

    iget-object v0, p0, Lmaps/aj/ak;->a:Lmaps/aj/al;

    move-object v4, p0

    :cond_3
    iget-object v5, v4, Lmaps/aj/ak;->a:Lmaps/aj/al;

    if-eq v0, v5, :cond_5

    invoke-virtual {v0, p2}, Lmaps/aj/al;->a(Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v0, p2}, Lmaps/aj/al;->a(Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No zoom table for tile type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lmaps/aj/aj;->a:Lmaps/aj/aj;

    :cond_4
    :goto_1
    iget-object v1, p0, Lmaps/aj/ak;->c:Ljava/util/Map;

    invoke-interface {v1, p2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    add-int/lit8 v1, v1, -0x1

    invoke-static {v3, v2, v1}, Lmaps/aj/al;->a(III)I

    move-result v5

    invoke-virtual {v0, v5}, Lmaps/aj/al;->a(I)Lmaps/aj/al;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v0, v4, Lmaps/aj/ak;->a:Lmaps/aj/al;

    invoke-virtual {v0, p2}, Lmaps/aj/al;->a(Lmaps/ao/b;)Lmaps/aj/aj;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "ZoomTableQuadTree"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No root zoom table for tile type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lmaps/aj/aj;->a:Lmaps/aj/aj;

    goto :goto_1
.end method
