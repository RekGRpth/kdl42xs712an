.class public final Lmaps/r/a;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/r/d;


# static fields
.field private static final a:Lmaps/r/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/r/a;

    invoke-direct {v0}, Lmaps/r/a;-><init>()V

    sput-object v0, Lmaps/r/a;->a:Lmaps/r/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lmaps/r/d;
    .locals 1

    sget-object v0, Lmaps/r/a;->a:Lmaps/r/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lmaps/r/c;II)V
    .locals 3

    move v1, p2

    :goto_0
    if-gt v1, p3, :cond_1

    move v0, v1

    :goto_1
    if-le v0, p2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p1, v0, v2}, Lmaps/r/c;->c(II)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-interface {p1, v0, v2}, Lmaps/r/c;->d(II)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
