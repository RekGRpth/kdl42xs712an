.class public final Lmaps/i/a;
.super Ljava/lang/Object;


# direct methods
.method private static a(D)I
    .locals 2

    const-wide v0, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static a(I)I
    .locals 4

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    mul-int/2addr v1, v0

    div-int/lit16 v1, v1, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    mul-int/2addr v2, v0

    div-int/lit16 v2, v2, 0xff

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    mul-int/2addr v3, v0

    div-int/lit16 v3, v3, 0xff

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(Lmaps/ar/b;)Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 5

    new-instance v0, Levp;

    invoke-direct {v0}, Levp;-><init>()V

    invoke-virtual {p0}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iput-object v1, v0, Levp;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lmaps/ar/b;->b()F

    move-result v1

    iput v1, v0, Levp;->b:F

    invoke-virtual {p0}, Lmaps/ar/b;->e()F

    move-result v1

    iput v1, v0, Levp;->d:F

    invoke-virtual {p0}, Lmaps/ar/b;->d()F

    move-result v1

    iput v1, v0, Levp;->c:F

    new-instance v1, Lcom/google/android/gms/maps/model/CameraPosition;

    iget-object v2, v0, Levp;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget v3, v0, Levp;->b:F

    iget v4, v0, Levp;->c:F

    iget v0, v0, Levp;->d:F

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/gms/maps/model/CameraPosition;-><init>(Lcom/google/android/gms/maps/model/LatLng;FFF)V

    return-object v1
.end method

.method public static a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 5

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p0}, Lmaps/ac/av;->b()D

    move-result-wide v1

    invoke-virtual {p0}, Lmaps/ac/av;->d()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method public static a(Lmaps/ac/cx;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 8

    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()Levt;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Levt;->a(Lcom/google/android/gms/maps/model/LatLng;)Levt;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v1

    invoke-static {v1}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Levt;->a(Lcom/google/android/gms/maps/model/LatLng;)Levt;

    move-result-object v1

    iget-wide v2, v1, Levt;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "no included points"

    invoke-static {v0, v2}, Lbkm;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds;

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v1, Levt;->a:D

    iget-wide v5, v1, Levt;->c:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v1, Levt;->b:D

    iget-wide v6, v1, Levt;->d:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/u;
    .locals 4

    new-instance v0, Lmaps/ac/u;

    iget-wide v1, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-static {v1, v2}, Lmaps/i/a;->a(D)I

    move-result v1

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, Lmaps/i/a;->a(D)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lmaps/ac/u;-><init>(II)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/ar/b;
    .locals 6

    new-instance v0, Lmaps/ar/b;

    iget-object v1, p0, Lcom/google/android/gms/maps/model/CameraPosition;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/u;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/maps/model/CameraPosition;->b:F

    iget v3, p0, Lcom/google/android/gms/maps/model/CameraPosition;->c:F

    iget v4, p0, Lcom/google/android/gms/maps/model/CameraPosition;->d:F

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/u;FFFF)V

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v2, p0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {v0, v1, v2, v3}, Lmaps/ac/av;->a(DD)Lmaps/ac/av;

    move-result-object v0

    return-object v0
.end method
