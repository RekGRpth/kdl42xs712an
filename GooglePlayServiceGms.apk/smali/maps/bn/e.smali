.class public final Lmaps/bn/e;
.super Ljava/lang/Object;


# static fields
.field private static synthetic p:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:I

.field private o:Lmaps/bn/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/bn/d;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmaps/bn/e;->p:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/e;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lmaps/bn/e;->n:I

    return-void
.end method


# virtual methods
.method public final a()Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/bn/e;->e:Z

    return-object p0
.end method

.method public final a(I)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lmaps/bn/e;->f:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/bn/e;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->j:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final b()Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->i:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final b(I)Lmaps/bn/e;
    .locals 0

    iput p1, p0, Lmaps/bn/e;->n:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/bn/e;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Z)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->l:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final c()Lmaps/bn/d;
    .locals 5

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/bn/e;->a:Ljava/lang/String;

    iget-object v1, p0, Lmaps/bn/e;->b:Ljava/lang/String;

    iget-object v2, p0, Lmaps/bn/e;->c:Ljava/lang/String;

    iget-object v3, p0, Lmaps/bn/e;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lmaps/bn/e;->e:Z

    invoke-static {v0, v1, v2, v3, v4}, Lmaps/bn/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lmaps/bn/d;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/bn/d;->a(Lmaps/bn/d;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    const-string v1, "SYSTEM"

    invoke-virtual {v0, v1}, Lmaps/bn/d;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/e;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lmaps/bn/d;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget v1, p0, Lmaps/bn/e;->f:I

    invoke-static {v0, v1}, Lmaps/bn/d;->a(Lmaps/bn/d;I)V

    iget-object v0, p0, Lmaps/bn/e;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/e;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/d;->a(Lmaps/bn/d;Z)V

    :cond_1
    iget-object v0, p0, Lmaps/bn/e;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/e;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/d;->b(Lmaps/bn/d;Z)V

    :cond_2
    iget-object v0, p0, Lmaps/bn/e;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/e;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/d;->c(Lmaps/bn/d;Z)V

    :cond_3
    iget-object v0, p0, Lmaps/bn/e;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/e;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/d;->d(Lmaps/bn/d;Z)V

    :cond_4
    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    iget-object v0, v0, Lmaps/bn/d;->e:Lmaps/bn/j;

    iget-object v1, p0, Lmaps/bn/e;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmaps/bn/e;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/j;->a(Lmaps/bn/j;Z)V

    :cond_5
    iget-object v1, p0, Lmaps/bn/e;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmaps/bn/e;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/bn/j;->a(Lmaps/bn/j;Ljava/lang/String;)V

    :cond_6
    iget v1, p0, Lmaps/bn/e;->n:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    iget v1, p0, Lmaps/bn/e;->n:I

    invoke-virtual {v0, v1}, Lmaps/bn/j;->a(I)V

    :cond_7
    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/bn/e;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Z)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->k:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/bn/e;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Z)Lmaps/bn/e;
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/e;->m:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final d()Lmaps/bn/k;
    .locals 3

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->a(Lmaps/bn/d;)Lmaps/bn/j;

    move-result-object v0

    iget-object v1, p0, Lmaps/bn/e;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmaps/bn/e;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lmaps/bn/j;->a(Lmaps/bn/j;Z)V

    :cond_1
    iget-object v1, p0, Lmaps/bn/e;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/bn/e;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lmaps/bn/j;->a(Lmaps/bn/j;Ljava/lang/String;)V

    :cond_2
    iget v1, p0, Lmaps/bn/e;->n:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v1, p0, Lmaps/bn/e;->n:I

    invoke-virtual {v0, v1}, Lmaps/bn/j;->a(I)V

    :goto_0
    return-object v0

    :cond_3
    invoke-static {v0}, Lmaps/bn/j;->a(Lmaps/bn/j;)V

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lmaps/bn/e;
    .locals 1

    sget-boolean v0, Lmaps/bn/e;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/bn/e;->o:Lmaps/bn/d;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/bn/e;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lmaps/bn/e;
    .locals 0

    iput-object p1, p0, Lmaps/bn/e;->h:Ljava/lang/String;

    return-object p0
.end method
