.class public final Lmaps/bn/g;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:Ljava/lang/String;

.field private synthetic b:Lmaps/bn/d;


# direct methods
.method private constructor <init>(Lmaps/bn/d;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/bn/g;->a:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lmaps/bn/d;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bn/g;-><init>(Lmaps/bn/d;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->k()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->i(Lmaps/bn/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->c(Lmaps/bn/j;)Lmaps/bn/h;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmaps/bn/h;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lmaps/bn/g;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/g;->a()V

    return-void
.end method

.method static synthetic a(Lmaps/bn/g;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "REQUEST"

    invoke-static {v0, p1}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->p()V

    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/bn/d;->a(I)V

    return-void
.end method

.method static synthetic b(Lmaps/bn/g;)J
    .locals 2

    invoke-direct {p0}, Lmaps/bn/g;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->e()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->i(Lmaps/bn/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/j;

    invoke-static {v0}, Lmaps/bn/j;->d(Lmaps/bn/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/bn/g;->a()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized c()J
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->j(Lmaps/bn/d;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->k(Lmaps/bn/d;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_0
    monitor-exit p0

    return-wide v0

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->k(Lmaps/bn/d;)J

    move-result-wide v0

    const-wide/16 v2, 0x320

    add-long/2addr v0, v2

    iget-object v2, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v2}, Lmaps/bn/d;->l(Lmaps/bn/d;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    iget-object v4, p0, Lmaps/bn/g;->b:Lmaps/bn/d;

    invoke-static {v4}, Lmaps/bn/d;->k(Lmaps/bn/d;)J

    move-result-wide v4

    rem-long/2addr v2, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-long/2addr v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c(Lmaps/bn/g;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/g;->b()V

    return-void
.end method

.method static synthetic d(Lmaps/bn/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/bn/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lmaps/bn/g;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/g;->b()V

    return-void
.end method
