.class public final Lmaps/bn/b;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:J

.field private e:Ljava/lang/Object;

.field private f:Lmaps/bn/d;

.field private g:Lmaps/bs/b;


# direct methods
.method public constructor <init>(Lmaps/bn/d;Lmaps/bs/b;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/bn/b;->a:I

    iput-object v1, p0, Lmaps/bn/b;->b:Ljava/lang/String;

    iput-object v1, p0, Lmaps/bn/b;->c:Ljava/lang/String;

    iput-object p1, p0, Lmaps/bn/b;->f:Lmaps/bn/d;

    iput-object p2, p0, Lmaps/bn/b;->g:Lmaps/bs/b;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/b;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lmaps/bn/b;->a:I

    iget-object v0, p0, Lmaps/bn/b;->g:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/bn/b;->d:J

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    iget v0, p0, Lmaps/bn/b;->a:I

    if-ne v0, v2, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lmaps/bn/b;->a:I

    iput-object p1, p0, Lmaps/bn/b;->e:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Object;JII)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/bn/b;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/bn/b;->e:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    :cond_0
    monitor-exit p0

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lmaps/bn/b;->a:I

    iget-object v0, p0, Lmaps/bn/b;->c:Ljava/lang/String;

    iget-wide v1, p0, Lmaps/bn/b;->d:J

    const/4 v3, 0x0

    iput-object v3, p0, Lmaps/bn/b;->e:Ljava/lang/Object;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long v1, p2, v1

    long-to-int v1, v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "|d="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|fb="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|lb="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "u"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "|s="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "|"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x40

    invoke-static {v1, v2, v0}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method
