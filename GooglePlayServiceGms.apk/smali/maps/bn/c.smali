.class public abstract Lmaps/bn/c;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:Z

.field private b:I

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lmaps/bn/c;->a:Z

    iput v0, p0, Lmaps/bn/c;->b:I

    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmaps/bn/c;->c:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/io/DataOutput;)V
.end method

.method public abstract a(Ljava/io/DataInput;)Z
.end method

.method public af_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public ag_()Z
    .locals 2

    iget v0, p0, Lmaps/bn/c;->b:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public ai_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract b()I
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public g()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()V
    .locals 1

    iget v0, p0, Lmaps/bn/c;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bn/c;->b:I

    return-void
.end method

.method public i()V
    .locals 0

    return-void
.end method
