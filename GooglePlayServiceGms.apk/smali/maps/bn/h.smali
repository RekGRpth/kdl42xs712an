.class final Lmaps/bn/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/util/Vector;

.field private final b:Lmaps/bv/a;

.field private final c:Z

.field private final d:Z

.field private synthetic e:Lmaps/bn/d;


# direct methods
.method constructor <init>(Lmaps/bn/d;Ljava/util/Vector;Lmaps/bv/a;)V
    .locals 1

    iput-object p1, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    iput-object p3, p0, Lmaps/bn/h;->b:Lmaps/bv/a;

    invoke-static {p2}, Lmaps/bn/d;->a(Ljava/util/Vector;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/bn/h;->c:Z

    invoke-static {p2}, Lmaps/bn/d;->b(Ljava/util/Vector;)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/bn/h;->d:Z

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 5

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_6

    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/c;

    invoke-virtual {v0}, Lmaps/bn/c;->h()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-boolean v0, v0, Lmaps/bn/d;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v0, v0, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v0}, Lmaps/bt/g;->b()Z

    move-result v0

    iget-object v1, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v2, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Server 500 for request types: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lmaps/bn/h;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lmaps/bn/d;->a(IZLjava/lang/String;)V

    :cond_1
    new-instance v0, Lmaps/bn/i;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Serverside failure (HTTP"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/bn/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/bn/i;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v0, 0x193

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->e(Lmaps/bn/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->h()V

    :cond_3
    :goto_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad HTTP response code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/bn/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/16 v0, 0x1f5

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bn/d;->a(I)V

    const-string v0, "DRD"

    const-string v1, "Server side HTTP not implemented"

    invoke-static {v0, v1}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server side HTTP not implemented"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/16 v0, 0x190

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v0}, Lmaps/bn/d;->e(Lmaps/bn/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v0}, Lmaps/bn/d;->i()V

    goto :goto_1

    :cond_6
    const-string v0, "application/binary"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad HTTP content type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lmaps/bn/h;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    return-void
.end method

.method private a(Ljava/io/DataInputStream;)V
    .locals 8

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v3

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/c;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    invoke-virtual {v0}, Lmaps/bn/c;->b()I

    move-result v5

    if-eq v1, v5, :cond_2

    new-instance v3, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "RT: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " != "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/bn/c;->b()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    :try_start_2
    instance-of v3, v0, Ljava/io/EOFException;

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lmaps/bn/c;->h()V

    iget-object v3, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-boolean v3, v3, Lmaps/bn/d;->c:Z

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lmaps/bn/c;->b()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "No server support for data request: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v3, v3, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v3}, Lmaps/bt/g;->b()Z

    move-result v3

    iget-object v5, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v6, 0x7

    invoke-virtual {v5, v6, v3, v1}, Lmaps/bn/d;->a(IZLjava/lang/String;)V

    :cond_0
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    iget-object v3, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Vector;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1, v4}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    throw v0

    :cond_2
    :try_start_3
    invoke-virtual {v0, p1}, Lmaps/bn/c;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v1, v0}, Lmaps/bn/d;->a(Lmaps/bn/c;)V

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_3

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto/16 :goto_0

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/util/Vector;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_6
    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v0, v4}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    return-void

    :catch_1
    move-exception v0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method private b()Ljava/lang/String;
    .locals 4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, ""

    iget-object v1, p0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bn/c;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0}, Lmaps/bn/c;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-boolean v1, p0, Lmaps/bn/h;->c:Z

    iget-boolean v2, p0, Lmaps/bn/h;->d:Z

    invoke-static {v0, v1, v2}, Lmaps/bn/d;->a(Lmaps/bn/d;ZZ)V

    iget-object v0, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, p0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->b(Lmaps/bn/d;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lmaps/bn/d;->a(Lmaps/bn/d;J)J

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "DataRequestDispatcher"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public final run()V
    .locals 18

    :cond_0
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->c(Lmaps/bn/d;)Z

    move-result v1

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_19

    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v1}, Lmaps/bn/g;->b(Lmaps/bn/g;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_1

    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    :goto_1
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v10

    invoke-static {}, Lmaps/bl/a;->a()V

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->i:Lmaps/bn/b;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lmaps/bn/b;->a(Ljava/lang/Object;)V

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-static {v3}, Lmaps/bn/d;->c(Ljava/util/Vector;)Z

    move-result v3

    if-eqz v3, :cond_1a

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->f(Lmaps/bn/d;)Lmaps/ba/g;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ba/g;->a()Lmaps/bv/a;

    move-result-object v1

    move-object v3, v1

    :goto_2
    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->b:Lmaps/bv/a;

    invoke-virtual {v1}, Lmaps/bv/a;->a()Lmaps/bv/a;

    move-result-object v1

    const/16 v8, 0x1f

    invoke-virtual {v1, v8, v3}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    :goto_3
    new-instance v3, Lmaps/bn/a;

    invoke-direct {v3, v1}, Lmaps/bn/a;-><init>(Lmaps/bv/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bn/c;

    instance-of v1, v1, Lmaps/bn/a;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v1, v3, v8}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_4
    const/16 v1, 0x17

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v1}, Lmaps/bn/d;->r()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-static {}, Lmaps/bf/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->a:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->b:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->g(Lmaps/bn/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bn/c;

    invoke-virtual {v1}, Lmaps/bn/c;->b()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    invoke-virtual {v1, v7}, Lmaps/bn/c;->a(Ljava/io/DataOutput;)V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lmaps/bn/i; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_5

    :catch_0
    move-exception v1

    :try_start_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v2, v2, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v2, v1}, Lmaps/bn/g;->a(Lmaps/bn/g;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/bn/h;->c:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lmaps/bn/h;->d:Z

    invoke-static {v2, v3, v4}, Lmaps/bn/d;->b(Lmaps/bn/d;ZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v2, v2, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v2}, Lmaps/bn/g;->c(Lmaps/bn/g;)V

    throw v1

    :catchall_1
    move-exception v1

    :try_start_6
    monitor-exit p0

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->b:Lmaps/bv/a;

    goto/16 :goto_3

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v1, v3, v8}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V
    :try_end_7
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lmaps/bn/i; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_4

    :catch_1
    move-exception v1

    :try_start_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lmaps/bn/d;->b(Lmaps/bn/d;I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    :cond_4
    :try_start_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v1, v3, v8}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V
    :try_end_9
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lmaps/bn/i; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_4

    :catch_2
    move-exception v1

    :try_start_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lmaps/bn/d;->b(Lmaps/bn/d;I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    :cond_5
    :try_start_b
    invoke-virtual {v7}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v1, "DRD"

    invoke-direct {v11, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lmaps/bn/d;->w()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ""

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v3, v1

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bn/c;

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "|"

    invoke-virtual {v1}, Lmaps/bn/c;->b()I

    move-result v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_b
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lmaps/bn/i; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_6

    :catch_3
    move-exception v1

    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lmaps/bn/d;->b(Lmaps/bn/d;I)V

    const-string v2, "REQUEST"

    invoke-static {v2, v1}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    :cond_6
    :try_start_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->b(Lmaps/bn/d;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v1}, Lmaps/bn/g;->d(Lmaps/bn/g;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v8, v8, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v8, v1}, Lmaps/bt/g;->a(Ljava/lang/String;)Lmaps/bt/f;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    move-result-object v9

    :try_start_e
    const-string v1, "Content-Type"

    const-string v6, "application/binary"

    invoke-interface {v9, v1, v6}, Lmaps/bt/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Content-Length"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    array-length v8, v7

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v9, v1, v6}, Lmaps/bt/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v1, 0x0

    invoke-static {v1}, Lmaps/k/aa;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Bearer "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v9, v1, v6}, Lmaps/bt/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->e(Lmaps/bn/d;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v6, "X-Google-Maps-Mobile-API"

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v1}, Lmaps/bn/d;->u()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->b:Lmaps/bv/a;

    const/16 v12, 0x27

    invoke-virtual {v1, v12}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->b:Lmaps/bv/a;

    const/16 v13, 0x28

    invoke-virtual {v1, v13}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v13

    if-eqz v12, :cond_c

    const/4 v1, 0x1

    :goto_7
    const-string v14, "app version not set"

    invoke-static {v1, v14}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    if-eqz v13, :cond_d

    const/4 v1, 0x1

    :goto_8
    const-string v14, "gmm version not set"

    invoke-static {v1, v14}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    const/16 v1, 0x2c

    invoke-static {v1}, Lmaps/k/f;->a(C)Lmaps/k/f;

    move-result-object v1

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lmaps/bn/d;->b:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    const/4 v15, 0x1

    aput-object v13, v14, v15

    const/4 v13, 0x2

    move-object/from16 v0, p0

    iget-object v15, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v15, v15, Lmaps/bn/d;->a:Ljava/lang/String;

    aput-object v15, v14, v13

    invoke-virtual {v1, v8, v12, v14}, Lmaps/k/f;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v6, v1}, Lmaps/bt/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    invoke-interface {v9}, Lmaps/bt/f;->a()Ljava/io/DataOutputStream;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    move-result-object v8

    :try_start_f
    invoke-virtual {v8, v7}, Ljava/io/DataOutputStream;->write([B)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget v5, v1, Lmaps/bn/d;->f:I

    array-length v6, v7

    add-int/2addr v5, v6

    iput v5, v1, Lmaps/bn/d;->f:I

    invoke-interface {v9}, Lmaps/bt/f;->b()Ljava/io/DataInputStream;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    move-result-object v7

    :try_start_10
    invoke-interface {v9}, Lmaps/bt/f;->c()I

    move-result v1

    invoke-interface {v9}, Lmaps/bt/f;->d()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v5}, Lmaps/bn/d;->b(Lmaps/bn/d;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v5

    sub-long v12, v5, v3

    const-string v5, ", "

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v5, 0x3e8

    cmp-long v5, v12, v5

    if-gez v5, :cond_e

    const-string v5, "<1s"

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lmaps/bn/h;->a(ILjava/lang/String;)V

    invoke-interface {v9}, Lmaps/bt/f;->e()J

    move-result-wide v1

    long-to-int v14, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget v2, v1, Lmaps/bn/d;->g:I

    add-int/2addr v2, v14

    iput v2, v1, Lmaps/bn/d;->g:I

    invoke-virtual {v7}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v1

    const/16 v2, 0x17

    if-eq v1, v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/bn/d;->a(I)V

    const-string v2, "DRD"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Protocol version mismatch. Client = 23 Server = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Protocol version mismatch with the server"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :catchall_2
    move-exception v1

    move-object v2, v1

    move-object v3, v8

    move-object v4, v9

    move-object v1, v7

    :goto_a
    if-eqz v1, :cond_9

    :try_start_11
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_9
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Lmaps/bn/i; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_9
    :goto_b
    if-eqz v3, :cond_a

    :try_start_12
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Lmaps/bn/i; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12 .. :try_end_12} :catch_4
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_a
    :goto_c
    if-eqz v4, :cond_b

    :try_start_13
    invoke-interface {v4}, Lmaps/bt/f;->f()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Lmaps/bn/i; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_13} :catch_4
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_b
    :goto_d
    :try_start_14
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bn/c;

    invoke-virtual {v1}, Lmaps/bn/c;->ag_()Z

    move-result v5

    if-nez v5, :cond_17

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v5, v1}, Lmaps/bn/d;->b(Lmaps/bn/c;)V
    :try_end_14
    .catch Ljava/lang/SecurityException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Lmaps/bn/i; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_2
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_14 .. :try_end_14} :catch_4
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto :goto_e

    :catch_4
    move-exception v1

    :try_start_15
    invoke-static {}, Lmaps/bf/d;->a()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lmaps/bn/d;->b(Lmaps/bn/d;I)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_7

    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_8

    :cond_e
    const-wide/16 v5, 0x3e8

    :try_start_16
    div-long v5, v12, v5

    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "s"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lmaps/bn/h;->a(Ljava/io/DataInputStream;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->b(Lmaps/bn/d;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v1

    sub-long/2addr v1, v3

    long-to-int v6, v1

    const/16 v1, 0x16

    const-string v2, "fb"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x16

    const-string v2, "lb"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x16

    const-string v2, "flbs"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v15, "fb="

    invoke-direct {v5, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "|lb="

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "|s="

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v5}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->i:Lmaps/bn/b;

    long-to-int v5, v12

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Lmaps/bn/b;->a(Ljava/lang/Object;JII)V

    const/16 v1, 0x2000

    if-lt v14, v1, :cond_10

    int-to-long v1, v6

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-gtz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    mul-int/lit16 v2, v14, 0x3e8

    div-int/2addr v2, v6

    invoke-static {v1, v2}, Lmaps/bn/d;->c(Lmaps/bn/d;I)I

    :cond_10
    const-string v1, ", "

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3e8

    if-ge v14, v1, :cond_14

    const-string v1, "<1kb"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    :goto_f
    if-eqz v7, :cond_11

    :try_start_17
    invoke-virtual {v7}, Ljava/io/DataInputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_17 .. :try_end_17} :catch_0
    .catch Lmaps/bn/i; {:try_start_17 .. :try_end_17} :catch_1
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_17 .. :try_end_17} :catch_4
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :cond_11
    :goto_10
    if-eqz v8, :cond_12

    :try_start_18
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_7
    .catch Ljava/lang/SecurityException; {:try_start_18 .. :try_end_18} :catch_0
    .catch Lmaps/bn/i; {:try_start_18 .. :try_end_18} :catch_1
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_18 .. :try_end_18} :catch_4
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :cond_12
    :goto_11
    if-eqz v9, :cond_13

    :try_start_19
    invoke-interface {v9}, Lmaps/bt/f;->f()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_8
    .catch Ljava/lang/SecurityException; {:try_start_19 .. :try_end_19} :catch_0
    .catch Lmaps/bn/i; {:try_start_19 .. :try_end_19} :catch_1
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_19 .. :try_end_19} :catch_4
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    :cond_13
    :goto_12
    :try_start_1a
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bn/c;

    invoke-virtual {v1}, Lmaps/bn/c;->ag_()Z

    move-result v4

    if-nez v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-virtual {v4, v1}, Lmaps/bn/d;->b(Lmaps/bn/c;)V
    :try_end_1a
    .catch Ljava/lang/SecurityException; {:try_start_1a .. :try_end_1a} :catch_0
    .catch Lmaps/bn/i; {:try_start_1a .. :try_end_1a} :catch_1
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1a .. :try_end_1a} :catch_4
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    goto :goto_13

    :cond_14
    :try_start_1b
    div-int/lit16 v1, v14, 0x3e8

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "kb"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    goto :goto_f

    :cond_15
    :try_start_1c
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->removeAllElements()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lmaps/bl/a;->b()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->h:Lmaps/bt/g;

    invoke-interface {v1}, Lmaps/bt/g;->a()Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v1}, Lmaps/bn/d;->d(Lmaps/bn/d;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    invoke-static {v2}, Lmaps/bn/d;->b(Lmaps/bn/d;)Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lmaps/bn/d;->b(Lmaps/bn/d;J)J

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne v10, v1, :cond_0

    const-string v1, "DRD"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No requests are processed. Request count = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/IOException;

    const-string v2, "No requests are processed"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_17
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_e

    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->removeAllElements()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->a:Ljava/util/Vector;

    invoke-virtual {v1, v3}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lmaps/bl/a;->b()V

    throw v2
    :try_end_1c
    .catch Ljava/lang/SecurityException; {:try_start_1c .. :try_end_1c} :catch_0
    .catch Lmaps/bn/i; {:try_start_1c .. :try_end_1c} :catch_1
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1c .. :try_end_1c} :catch_4
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    :cond_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/bn/h;->c:Z

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/bn/h;->d:Z

    invoke-static {v1, v2, v3}, Lmaps/bn/d;->b(Lmaps/bn/d;ZZ)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/bn/h;->e:Lmaps/bn/d;

    iget-object v1, v1, Lmaps/bn/d;->d:Lmaps/bn/g;

    invoke-static {v1}, Lmaps/bn/g;->c(Lmaps/bn/g;)V

    return-void

    :catch_5
    move-exception v1

    goto/16 :goto_1

    :catch_6
    move-exception v1

    goto/16 :goto_10

    :catch_7
    move-exception v1

    goto/16 :goto_11

    :catch_8
    move-exception v1

    goto/16 :goto_12

    :catch_9
    move-exception v1

    goto/16 :goto_b

    :catch_a
    move-exception v1

    goto/16 :goto_c

    :catch_b
    move-exception v1

    goto/16 :goto_d

    :catchall_3
    move-exception v1

    move-object v3, v5

    move-object v4, v6

    move-object/from16 v17, v1

    move-object v1, v2

    move-object/from16 v2, v17

    goto/16 :goto_a

    :catchall_4
    move-exception v1

    move-object v3, v5

    move-object v4, v9

    move-object/from16 v17, v1

    move-object v1, v2

    move-object/from16 v2, v17

    goto/16 :goto_a

    :catchall_5
    move-exception v1

    move-object v3, v8

    move-object v4, v9

    move-object/from16 v17, v1

    move-object v1, v2

    move-object/from16 v2, v17

    goto/16 :goto_a

    :cond_1a
    move-object v3, v1

    goto/16 :goto_2
.end method
