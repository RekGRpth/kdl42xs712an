.class public final Lmaps/bu/a;
.super Lmaps/bt/b;


# static fields
.field private static d:I

.field private static final e:Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private c:Lorg/apache/http/client/HttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/bu/a;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lmaps/bt/b;-><init>()V

    iput-object p1, p0, Lmaps/bu/a;->b:Landroid/content/Context;

    const-string v0, "GoogleMobile/1.0"

    iput-object v0, p0, Lmaps/bu/a;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lmaps/bu/a;)Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lmaps/bu/a;->c:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lmaps/bu/a;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e()I
    .locals 2

    sget v0, Lmaps/bu/a;->d:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lmaps/bu/a;->d:I

    return v0
.end method

.method static synthetic f()I
    .locals 2

    sget v0, Lmaps/bu/a;->d:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lmaps/bu/a;->d:I

    return v0
.end method

.method static synthetic g()I
    .locals 1

    sget v0, Lmaps/bu/a;->d:I

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lmaps/bt/f;
    .locals 3

    iget-object v0, p0, Lmaps/bu/a;->c:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/a/a;

    iget-object v1, p0, Lmaps/bu/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lmaps/bu/a;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lmaps/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lmaps/bu/a;->c:Lorg/apache/http/client/HttpClient;

    iget-object v0, p0, Lmaps/bu/a;->c:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    new-instance v1, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    :cond_0
    new-instance v0, Lmaps/bu/b;

    invoke-direct {v0, p0, p1}, Lmaps/bu/b;-><init>(Lmaps/bu/a;Ljava/lang/String;)V

    return-object v0
.end method
