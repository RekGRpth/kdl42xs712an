.class public final Lmaps/s/l;
.super Ljava/util/concurrent/FutureTask;

# interfaces
.implements Lmaps/s/k;


# instance fields
.field private final a:Lmaps/s/d;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    new-instance v0, Lmaps/s/d;

    invoke-direct {v0}, Lmaps/s/d;-><init>()V

    iput-object v0, p0, Lmaps/s/l;->a:Lmaps/s/d;

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    new-instance v0, Lmaps/s/d;

    invoke-direct {v0}, Lmaps/s/d;-><init>()V

    iput-object v0, p0, Lmaps/s/l;->a:Lmaps/s/d;

    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lmaps/s/l;
    .locals 1

    new-instance v0, Lmaps/s/l;

    invoke-direct {v0, p0, p1}, Lmaps/s/l;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lmaps/s/l;
    .locals 1

    new-instance v0, Lmaps/s/l;

    invoke-direct {v0, p0}, Lmaps/s/l;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    iget-object v0, p0, Lmaps/s/l;->a:Lmaps/s/d;

    invoke-virtual {v0, p1, p2}, Lmaps/s/d;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method protected final done()V
    .locals 1

    iget-object v0, p0, Lmaps/s/l;->a:Lmaps/s/d;

    invoke-virtual {v0}, Lmaps/s/d;->a()V

    return-void
.end method
