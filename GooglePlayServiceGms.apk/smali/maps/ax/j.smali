.class public final Lmaps/ax/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/bn/l;


# instance fields
.field private volatile a:I

.field private final b:Lmaps/bn/d;


# direct methods
.method public constructor <init>(Lmaps/bn/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ax/j;->a:I

    iput-object p1, p0, Lmaps/ax/j;->b:Lmaps/bn/d;

    return-void
.end method

.method private c()V
    .locals 7

    new-instance v0, Lmaps/z/d;

    iget v1, p0, Lmaps/ax/j;->a:I

    iget-object v2, p0, Lmaps/ax/j;->b:Lmaps/bn/d;

    invoke-virtual {v2}, Lmaps/bn/d;->m()I

    move-result v2

    iget-object v3, p0, Lmaps/ax/j;->b:Lmaps/bn/d;

    invoke-virtual {v3}, Lmaps/bn/d;->n()I

    move-result v3

    iget-object v4, p0, Lmaps/ax/j;->b:Lmaps/bn/d;

    invoke-virtual {v4}, Lmaps/bn/d;->l()J

    move-result-wide v4

    iget-object v6, p0, Lmaps/ax/j;->b:Lmaps/bn/d;

    invoke-virtual {v6}, Lmaps/bn/d;->o()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lmaps/z/d;-><init>(IIIJI)V

    invoke-static {}, Lmaps/z/a;->a()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(IZLjava/lang/String;)V
    .locals 1

    new-instance v0, Lmaps/z/c;

    invoke-direct {v0, p1, p2}, Lmaps/z/c;-><init>(IZ)V

    invoke-static {}, Lmaps/z/a;->a()V

    invoke-direct {p0}, Lmaps/ax/j;->c()V

    return-void
.end method

.method public final a(Lmaps/bn/c;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lmaps/bn/c;)V
    .locals 2

    new-instance v0, Lmaps/z/b;

    const-string v1, "onComplete"

    invoke-direct {v0, v1, p1}, Lmaps/z/b;-><init>(Ljava/lang/String;Lmaps/bn/c;)V

    invoke-static {}, Lmaps/z/a;->a()V

    iget v0, p0, Lmaps/ax/j;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ax/j;->a:I

    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/ax/j;->c()V

    :cond_0
    return-void
.end method
