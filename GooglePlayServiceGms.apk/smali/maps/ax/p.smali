.class final Lmaps/ax/p;
.super Lmaps/bn/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Long;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    iput-object p1, p0, Lmaps/ax/p;->a:Landroid/content/Context;

    iput-object p2, p0, Lmaps/ax/p;->b:Ljava/lang/Long;

    iput-object p3, p0, Lmaps/ax/p;->d:Ljava/lang/String;

    iput-object p4, p0, Lmaps/ax/p;->c:Ljava/lang/Long;

    iput-object p5, p0, Lmaps/ax/p;->e:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;B)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lmaps/ax/p;-><init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/DataOutput;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v0, v4, v4}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    iget-object v1, p0, Lmaps/ax/p;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ax/p;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v5, v1, v2}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    :cond_0
    new-instance v1, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/f;->a:Lmaps/bv/c;

    invoke-direct {v1, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v1, v4, v0}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    new-instance v0, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-direct {v0, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v2, 0x5

    invoke-virtual {v0, v4, v2}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    iget-object v2, p0, Lmaps/ax/p;->c:Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmaps/ax/p;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v5, v2, v3}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    :cond_1
    invoke-virtual {v1, v4, v0}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    invoke-static {v1}, Lmaps/be/g;->a(Lmaps/bv/a;)V

    invoke-virtual {v1}, Lmaps/bv/a;->b()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lmaps/bv/a;->b(Ljava/io/OutputStream;)V

    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 10

    const/4 v9, 0x7

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    sget-object v0, Lmaps/cm/f;->c:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lmaps/bv/a;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v6, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/bv/a;->d(I)I

    move-result v4

    if-ne v4, v6, :cond_1

    invoke-virtual {v3, v7}, Lmaps/bv/a;->i(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v8}, Lmaps/bv/a;->i(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lmaps/ax/p;->a:Landroid/content/Context;

    iget-object v5, p0, Lmaps/ax/p;->d:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lmaps/aw/a;->a(Landroid/content/Context;Lmaps/bv/a;Ljava/lang/String;)Z

    invoke-virtual {v3, v8}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v3

    new-instance v4, Lmaps/ax/k;

    invoke-direct {v4, v3}, Lmaps/ax/k;-><init>(Lmaps/bv/a;)V

    invoke-static {v4}, Lmaps/ax/m;->a(Lmaps/ax/k;)Lmaps/ax/k;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v6}, Lmaps/bv/a;->d(I)I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    invoke-virtual {v3, v7}, Lmaps/bv/a;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v9}, Lmaps/bv/a;->i(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lmaps/ax/p;->a:Landroid/content/Context;

    iget-object v5, p0, Lmaps/ax/p;->e:Ljava/lang/String;

    invoke-static {v4, v3, v5}, Lmaps/aw/a;->a(Landroid/content/Context;Lmaps/bv/a;Ljava/lang/String;)Z

    invoke-virtual {v3, v9}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v3

    invoke-static {v3}, Lmaps/aj/ak;->a(Lmaps/bv/a;)Lmaps/aj/ak;

    move-result-object v3

    invoke-static {v3}, Lmaps/ax/m;->a(Lmaps/aj/ak;)Lmaps/aj/ak;

    goto :goto_1

    :cond_2
    invoke-static {}, Lmaps/ax/m;->h()Z

    const-class v1, Lmaps/ax/m;

    monitor-enter v1

    :try_start_0
    const-class v0, Lmaps/ax/m;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v6

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final ag_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method
