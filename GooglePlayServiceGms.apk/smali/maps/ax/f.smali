.class public Lmaps/ax/f;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/util/HashMap;

.field private b:Lmaps/ax/i;

.field private c:Lmaps/ax/i;

.field private d:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    iput p1, p0, Lmaps/ax/f;->d:I

    return-void
.end method

.method private a(Lmaps/ax/i;)V
    .locals 1

    iget-object v0, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    if-nez v0, :cond_0

    iput-object p1, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    iput-object p1, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    iput-object v0, p1, Lmaps/ax/i;->a:Lmaps/ax/i;

    iput-object p1, v0, Lmaps/ax/i;->b:Lmaps/ax/i;

    iput-object p1, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    goto :goto_0
.end method

.method private b(Lmaps/ax/i;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p1, Lmaps/ax/i;->a:Lmaps/ax/i;

    iget-object v1, p1, Lmaps/ax/i;->b:Lmaps/ax/i;

    if-eqz v0, :cond_0

    iput-object v1, v0, Lmaps/ax/i;->b:Lmaps/ax/i;

    :cond_0
    if-eqz v1, :cond_1

    iput-object v0, v1, Lmaps/ax/i;->a:Lmaps/ax/i;

    :cond_1
    iput-object v2, p1, Lmaps/ax/i;->a:Lmaps/ax/i;

    iput-object v2, p1, Lmaps/ax/i;->b:Lmaps/ax/i;

    iget-object v2, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    if-ne v2, p1, :cond_2

    iput-object v1, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    :cond_2
    iget-object v1, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    if-ne v1, p1, :cond_3

    iput-object v0, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    :cond_3
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    :goto_0
    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    iget-object v0, v0, Lmaps/ax/i;->c:Ljava/lang/Object;

    iget-object v0, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    iget-object v0, v0, Lmaps/ax/i;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmaps/ax/f;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ax/f;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ax/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ax/i;

    if-nez v0, :cond_0

    iget v1, p0, Lmaps/ax/f;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lmaps/ax/f;->a(I)V

    :cond_0
    new-instance v1, Lmaps/ax/i;

    invoke-direct {v1}, Lmaps/ax/i;-><init>()V

    iput-object p2, v1, Lmaps/ax/i;->d:Ljava/lang/Object;

    iput-object p1, v1, Lmaps/ax/i;->c:Ljava/lang/Object;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lmaps/ax/f;->b(Lmaps/ax/i;)V

    iget-object v2, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v2}, Lmaps/ax/f;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmaps/ax/f;->a(Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    iget-object v2, v1, Lmaps/ax/i;->c:Ljava/lang/Object;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v1}, Lmaps/ax/f;->a(Lmaps/ax/i;)V

    return-void
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ax/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lmaps/ax/f;->b(Lmaps/ax/i;)V

    invoke-direct {p0, v0}, Lmaps/ax/f;->a(Lmaps/ax/i;)V

    iget-object v0, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ax/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lmaps/ax/f;->b(Lmaps/ax/i;)V

    iget-object v1, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    invoke-virtual {p0, p1, v1}, Lmaps/ax/f;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmaps/ax/f;->a(I)V

    return-void
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/ax/f;->c:Lmaps/ax/i;

    iget-object v0, v0, Lmaps/ax/i;->c:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lmaps/ax/f;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/util/Collection;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lmaps/ax/f;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmaps/ax/i;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lmaps/ax/i;->b:Lmaps/ax/i;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final h()Lmaps/ax/g;
    .locals 2

    new-instance v0, Lmaps/ax/g;

    iget-object v1, p0, Lmaps/ax/f;->b:Lmaps/ax/i;

    invoke-direct {v0, v1}, Lmaps/ax/g;-><init>(Lmaps/ax/i;)V

    return-object v0
.end method
