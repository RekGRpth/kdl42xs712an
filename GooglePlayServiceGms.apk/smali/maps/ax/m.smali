.class public final Lmaps/ax/m;
.super Ljava/lang/Object;


# static fields
.field private static a:Z

.field private static volatile b:Z

.field private static volatile c:Z

.field private static volatile d:Lmaps/ax/k;

.field private static volatile e:Lmaps/aj/ak;

.field private static volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/ax/k;

    new-instance v1, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/f;->i:Lmaps/bv/c;

    invoke-direct {v1, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-direct {v0, v1}, Lmaps/ax/k;-><init>(Lmaps/bv/a;)V

    sput-object v0, Lmaps/ax/m;->d:Lmaps/ax/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lmaps/aj/ak;)Lmaps/aj/ak;
    .locals 0

    sput-object p0, Lmaps/ax/m;->e:Lmaps/aj/ak;

    return-object p0
.end method

.method public static declared-synchronized a()Lmaps/ax/k;
    .locals 2

    const-class v0, Lmaps/ax/m;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/ax/m;->d:Lmaps/ax/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(Lmaps/ax/k;)Lmaps/ax/k;
    .locals 0

    sput-object p0, Lmaps/ax/m;->d:Lmaps/ax/k;

    return-object p0
.end method

.method public static a(Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;)V
    .locals 2

    sget-boolean v0, Lmaps/ax/m;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ax/m;->a:Z

    new-instance v0, Lmaps/ax/n;

    const-string v1, "ParameterManagerLoad"

    invoke-direct {v0, v1, p0, p1, p2}, Lmaps/ax/n;-><init>(Ljava/lang/String;Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lmaps/ax/n;->start()V

    goto :goto_0
.end method

.method static synthetic a(Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lmaps/ax/m;->b(Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static declared-synchronized b(Landroid/content/Context;Lmaps/bn/d;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const-class v7, Lmaps/ax/m;

    monitor-enter v7

    :try_start_0
    invoke-static {}, Lmaps/bl/a;->a()V

    sget-object v0, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-static {p0, p3, v0}, Lmaps/aw/a;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/bv/c;)Lmaps/bv/a;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    const/4 v2, 0x1

    sput-boolean v2, Lmaps/ax/m;->c:Z

    :goto_0
    new-instance v2, Lmaps/ax/k;

    invoke-direct {v2, v0}, Lmaps/ax/k;-><init>(Lmaps/bv/a;)V

    sput-object v2, Lmaps/ax/m;->d:Lmaps/ax/k;

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lmaps/bv/a;->e(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_1
    sget-object v0, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-static {p0, p4, v0}, Lmaps/aw/a;->a(Landroid/content/Context;Ljava/lang/String;Lmaps/bv/c;)Lmaps/bv/a;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v1

    invoke-static {v1}, Lmaps/aj/ak;->a(Lmaps/bv/a;)Lmaps/aj/ak;

    move-result-object v1

    sput-object v1, Lmaps/ax/m;->e:Lmaps/aj/ak;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bv/a;->e(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    :cond_0
    const-class v0, Lmaps/ax/m;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_1
    if-eqz p1, :cond_4

    sget-object v0, Lmaps/ax/m;->e:Lmaps/aj/ak;

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/bn/d;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ax/m;->f:Z

    const-class v0, Lmaps/ax/m;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    new-instance v0, Lmaps/ax/o;

    invoke-direct {v0, p1, p2}, Lmaps/ax/o;-><init>(Lmaps/bn/d;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    new-instance v0, Lmaps/ax/p;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lmaps/ax/p;-><init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;B)V

    invoke-virtual {p1, v0}, Lmaps/bn/d;->c(Lmaps/bn/c;)V

    :cond_4
    invoke-static {}, Lmaps/bl/a;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-void

    :cond_5
    :try_start_1
    new-instance v1, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-direct {v1, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    new-instance v0, Lmaps/bv/a;

    sget-object v2, Lmaps/cm/f;->i:Lmaps/bv/c;

    invoke-direct {v0, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_6
    move-object v2, v4

    goto :goto_1
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lmaps/ax/m;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ax/m;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized c()Lmaps/aj/ak;
    .locals 2

    const-class v1, Lmaps/ax/m;

    monitor-enter v1

    :goto_0
    :try_start_0
    sget-object v0, Lmaps/ax/m;->e:Lmaps/aj/ak;

    if-nez v0, :cond_0

    sget-boolean v0, Lmaps/ax/m;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-class v0, Lmaps/ax/m;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    sget-object v0, Lmaps/ax/m;->e:Lmaps/aj/ak;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d()Z
    .locals 1

    sget-object v0, Lmaps/ax/m;->e:Lmaps/aj/ak;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/ax/m;->f:Z

    sput-boolean v0, Lmaps/ax/m;->a:Z

    sput-boolean v0, Lmaps/ax/m;->b:Z

    sput-boolean v0, Lmaps/ax/m;->c:Z

    return-void
.end method

.method static synthetic f()Lmaps/aj/ak;
    .locals 1

    sget-object v0, Lmaps/ax/m;->e:Lmaps/aj/ak;

    return-object v0
.end method

.method static synthetic g()Z
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ax/m;->f:Z

    return v0
.end method

.method static synthetic h()Z
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ax/m;->b:Z

    return v0
.end method
