.class public abstract Lmaps/an/ad;
.super Ljava/lang/Object;


# instance fields
.field a:Ljava/lang/Object;

.field b:I

.field c:I

.field volatile d:Z

.field final e:Ljava/util/List;

.field private f:I

.field private g:I

.field private h:Z

.field private i:[F


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lmaps/an/ad;->f:I

    iput v1, p0, Lmaps/an/ad;->g:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/an/ad;->a:Ljava/lang/Object;

    iput v1, p0, Lmaps/an/ad;->b:I

    iput v1, p0, Lmaps/an/ad;->c:I

    iput-boolean v3, p0, Lmaps/an/ad;->d:Z

    iput-boolean v1, p0, Lmaps/an/ad;->h:Z

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/an/ad;->i:[F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/an/ad;->e:Ljava/util/List;

    iget-boolean v0, p0, Lmaps/an/ad;->h:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/an/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/an/ad;->i:[F

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/an/ad;->i:[F

    aput v2, v0, v3

    iget-object v0, p0, Lmaps/an/ad;->i:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/an/ad;->i:[F

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lmaps/an/ad;->b:I

    return v0
.end method

.method final a(Lmaps/an/g;)V
    .locals 2

    iget-object v1, p0, Lmaps/an/ad;->e:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/an/ad;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Lmaps/an/l;)Z
    .locals 2

    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iget-boolean v1, p0, Lmaps/an/ad;->h:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/ad;->h:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/an/ad;->c:I

    return v0
.end method

.method final b(Lmaps/an/g;)V
    .locals 2

    iget-object v1, p0, Lmaps/an/ad;->e:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/an/ad;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method c()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v0, p0, Lmaps/an/ad;->d:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/an/ad;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lmaps/an/ad;->b:I

    iput v0, p0, Lmaps/an/ad;->f:I

    iget v0, p0, Lmaps/an/ad;->c:I

    iput v0, p0, Lmaps/an/ad;->g:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/ad;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lmaps/an/ad;->i:[F

    aget v0, v0, v5

    iget-object v1, p0, Lmaps/an/ad;->i:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lmaps/an/ad;->i:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lmaps/an/ad;->i:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    invoke-static {v5}, Landroid/opengl/GLES20;->glClearStencil(I)V

    const/16 v0, 0x4500

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
