.class public Lmaps/an/g;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field private b:Lmaps/an/ad;

.field private volatile c:[F

.field private d:[F

.field private e:[F

.field private f:[F

.field private g:Z

.field private h:B

.field private i:Z

.field private final j:F

.field private final k:F

.field private final l:F

.field private final m:Lmaps/an/i;

.field private n:Ljava/util/List;


# direct methods
.method public constructor <init>(Lmaps/an/ad;I[F)V
    .locals 3

    const/16 v2, 0x10

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/an/g;->c:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/an/g;->d:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/an/g;->e:[F

    new-array v0, v2, [F

    iput-object v0, p0, Lmaps/an/g;->f:[F

    iput-boolean v1, p0, Lmaps/an/g;->g:Z

    iput v1, p0, Lmaps/an/g;->a:I

    iput-boolean v1, p0, Lmaps/an/g;->i:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/an/g;->n:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    iput-byte v1, p0, Lmaps/an/g;->h:B

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lmaps/an/g;->j:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lmaps/an/g;->k:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/an/g;->l:F

    sget-object v0, Lmaps/an/i;->c:Lmaps/an/i;

    iput-object v0, p0, Lmaps/an/g;->m:Lmaps/an/i;

    iget-object v0, p0, Lmaps/an/g;->c:[F

    invoke-static {p3, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/g;->g:Z

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lmaps/an/g;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v1, p0, Lmaps/an/g;->f:[F

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/an/g;->e:[F

    iget-object v2, p0, Lmaps/an/g;->d:[F

    iget-object v4, p0, Lmaps/an/g;->c:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lmaps/an/g;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/an/g;->a:I

    return-void
.end method

.method final a(II)V
    .locals 11

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    iget-object v1, p0, Lmaps/an/g;->f:[F

    monitor-enter v1

    :try_start_0
    sget-object v2, Lmaps/an/h;->a:[I

    iget-object v3, p0, Lmaps/an/g;->m:Lmaps/an/i;

    invoke-virtual {v3}, Lmaps/an/i;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented projection type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lmaps/an/g;->m:Lmaps/an/i;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_0
    :try_start_1
    iget-object v2, p0, Lmaps/an/g;->f:[F

    if-nez p2, :cond_1

    :goto_0
    iget v3, p0, Lmaps/an/g;->j:F

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    neg-float v4, v3

    neg-float v5, v3

    div-float/2addr v5, v0

    div-float v0, v3, v0

    const/4 v6, 0x0

    iget v7, p0, Lmaps/an/g;->j:F

    mul-float/2addr v7, v10

    sub-float v8, v3, v4

    div-float/2addr v7, v8

    aput v7, v2, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v2, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v2, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput v7, v2, v6

    const/4 v6, 0x4

    const/4 v7, 0x0

    aput v7, v2, v6

    const/4 v6, 0x5

    iget v7, p0, Lmaps/an/g;->j:F

    mul-float/2addr v7, v10

    sub-float v8, v0, v5

    div-float/2addr v7, v8

    aput v7, v2, v6

    const/4 v6, 0x6

    const/4 v7, 0x0

    aput v7, v2, v6

    const/4 v6, 0x7

    const/4 v7, 0x0

    aput v7, v2, v6

    const/16 v6, 0x8

    add-float v7, v3, v4

    sub-float/2addr v3, v4

    div-float v3, v7, v3

    aput v3, v2, v6

    const/16 v3, 0x9

    add-float v4, v0, v5

    sub-float/2addr v0, v5

    div-float v0, v4, v0

    aput v0, v2, v3

    const/16 v0, 0xa

    iget v3, p0, Lmaps/an/g;->k:F

    iget v4, p0, Lmaps/an/g;->j:F

    add-float/2addr v3, v4

    neg-float v3, v3

    iget v4, p0, Lmaps/an/g;->k:F

    iget v5, p0, Lmaps/an/g;->j:F

    sub-float/2addr v4, v5

    div-float/2addr v3, v4

    aput v3, v2, v0

    const/16 v0, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v2, v0

    const/16 v0, 0xc

    const/4 v3, 0x0

    aput v3, v2, v0

    const/16 v0, 0xd

    const/4 v3, 0x0

    aput v3, v2, v0

    const/16 v0, 0xe

    iget v3, p0, Lmaps/an/g;->k:F

    mul-float/2addr v3, v10

    iget v4, p0, Lmaps/an/g;->j:F

    mul-float/2addr v3, v4

    neg-float v3, v3

    iget v4, p0, Lmaps/an/g;->k:F

    iget v5, p0, Lmaps/an/g;->j:F

    sub-float/2addr v4, v5

    div-float/2addr v3, v4

    aput v3, v2, v0

    const/16 v0, 0xf

    const/4 v3, 0x0

    aput v3, v2, v0

    :cond_0
    :goto_1
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/an/g;->g:Z

    invoke-direct {p0}, Lmaps/an/g;->e()V

    monitor-exit v1

    return-void

    :cond_1
    int-to-float v0, p1

    int-to-float v3, p2

    div-float/2addr v0, v3

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/an/g;->f:[F

    int-to-float v2, p1

    int-to-float v3, p2

    iget v4, p0, Lmaps/an/g;->j:F

    iget v5, p0, Lmaps/an/g;->k:F

    cmpl-float v6, v9, v2

    if-eqz v6, :cond_0

    cmpl-float v6, v3, v9

    if-eqz v6, :cond_0

    cmpl-float v6, v4, v5

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    sub-float v7, v2, v9

    div-float v7, v10, v7

    aput v7, v0, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v0, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v0, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput v7, v0, v6

    const/4 v6, 0x4

    const/4 v7, 0x0

    aput v7, v0, v6

    const/4 v6, 0x5

    sub-float v7, v3, v9

    div-float v7, v10, v7

    aput v7, v0, v6

    const/4 v6, 0x6

    const/4 v7, 0x0

    aput v7, v0, v6

    const/4 v6, 0x7

    const/4 v7, 0x0

    aput v7, v0, v6

    const/16 v6, 0x8

    const/4 v7, 0x0

    aput v7, v0, v6

    const/16 v6, 0x9

    const/4 v7, 0x0

    aput v7, v0, v6

    const/16 v6, 0xa

    const/high16 v7, -0x40000000    # -2.0f

    sub-float v8, v5, v4

    div-float/2addr v7, v8

    aput v7, v0, v6

    const/16 v6, 0xb

    const/4 v7, 0x0

    aput v7, v0, v6

    const/16 v6, 0xc

    add-float v7, v2, v9

    neg-float v7, v7

    sub-float/2addr v2, v9

    div-float v2, v7, v2

    aput v2, v0, v6

    const/16 v2, 0xd

    add-float v6, v3, v9

    neg-float v6, v6

    sub-float/2addr v3, v9

    div-float v3, v6, v3

    aput v3, v0, v2

    const/16 v2, 0xe

    add-float v3, v5, v4

    neg-float v3, v3

    sub-float v4, v5, v4

    div-float/2addr v3, v4

    aput v3, v0, v2

    const/16 v2, 0xf

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method final a(Lmaps/an/l;)Z
    .locals 2

    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iget-boolean v1, p0, Lmaps/an/g;->i:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p1, Lmaps/an/l;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    iput-boolean v0, p0, Lmaps/an/g;->i:Z

    iget-boolean v0, p0, Lmaps/an/g;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    invoke-virtual {v0, p0}, Lmaps/an/ad;->a(Lmaps/an/g;)V

    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    invoke-virtual {v0}, Lmaps/an/ad;->a()I

    move-result v0

    iget-object v1, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    invoke-virtual {v1}, Lmaps/an/ad;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lmaps/an/g;->a(II)V

    :goto_1
    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    invoke-virtual {v0, p1}, Lmaps/an/ad;->a(Lmaps/an/l;)Z

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    invoke-virtual {v0, p0}, Lmaps/an/ad;->b(Lmaps/an/g;)V

    goto :goto_1
.end method

.method public final b()Lmaps/an/ad;
    .locals 1

    iget-object v0, p0, Lmaps/an/g;->b:Lmaps/an/ad;

    return-object v0
.end method

.method final c()V
    .locals 6

    iget-object v1, p0, Lmaps/an/g;->f:[F

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lmaps/an/g;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/an/g;->f:[F

    const/4 v2, 0x0

    iget-object v3, p0, Lmaps/an/g;->d:[F

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0}, Lmaps/an/g;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/an/g;->g:Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()B
    .locals 1

    iget-byte v0, p0, Lmaps/an/g;->h:B

    return v0
.end method
