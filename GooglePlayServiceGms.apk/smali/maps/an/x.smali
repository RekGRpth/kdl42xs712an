.class public final Lmaps/an/x;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/an/x;->a:Landroid/graphics/Bitmap;

    iput v1, p0, Lmaps/an/x;->b:I

    iput v1, p0, Lmaps/an/x;->c:I

    iput-object p1, p0, Lmaps/an/x;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lmaps/an/x;->b:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lmaps/an/x;->c:I

    return-void
.end method


# virtual methods
.method final a(Lmaps/an/l;)Z
    .locals 9

    const/16 v2, 0x1907

    const/16 v0, 0xde1

    const/4 v1, 0x0

    iget-boolean v3, p1, Lmaps/an/l;->e:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/an/x;->a:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    iget-object v2, p0, Lmaps/an/x;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    :cond_0
    :goto_0
    iget-boolean v0, p1, Lmaps/an/l;->e:Z

    const/4 v0, 0x1

    return v0

    :cond_1
    iget v3, p0, Lmaps/an/x;->b:I

    iget v4, p0, Lmaps/an/x;->c:I

    const v7, 0x8363

    const/4 v8, 0x0

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    invoke-static {}, Lmaps/an/m;->c()V

    goto :goto_0
.end method
