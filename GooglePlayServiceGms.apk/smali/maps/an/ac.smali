.class public abstract Lmaps/an/ac;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lmaps/an/g;


# virtual methods
.method final a(Lmaps/an/g;)V
    .locals 2

    iget-object v0, p0, Lmaps/an/ac;->a:Lmaps/an/g;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setCamera can only be called once"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lmaps/an/ac;->a:Lmaps/an/g;

    return-void
.end method
