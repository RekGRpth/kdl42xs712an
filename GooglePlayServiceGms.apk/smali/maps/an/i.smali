.class final enum Lmaps/an/i;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/an/i;

.field public static final enum b:Lmaps/an/i;

.field public static final enum c:Lmaps/an/i;

.field private static final synthetic d:[Lmaps/an/i;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/an/i;

    const-string v1, "PERSPECTIVE"

    invoke-direct {v0, v1, v2}, Lmaps/an/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/an/i;->a:Lmaps/an/i;

    new-instance v0, Lmaps/an/i;

    const-string v1, "ORTHOGRAPHIC"

    invoke-direct {v0, v1, v3}, Lmaps/an/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/an/i;->b:Lmaps/an/i;

    new-instance v0, Lmaps/an/i;

    const-string v1, "USER_SET"

    invoke-direct {v0, v1, v4}, Lmaps/an/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/an/i;->c:Lmaps/an/i;

    const/4 v0, 0x3

    new-array v0, v0, [Lmaps/an/i;

    sget-object v1, Lmaps/an/i;->a:Lmaps/an/i;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/an/i;->b:Lmaps/an/i;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/an/i;->c:Lmaps/an/i;

    aput-object v1, v0, v4

    sput-object v0, Lmaps/an/i;->d:[Lmaps/an/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/an/i;
    .locals 1

    const-class v0, Lmaps/an/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/an/i;

    return-object v0
.end method

.method public static values()[Lmaps/an/i;
    .locals 1

    sget-object v0, Lmaps/an/i;->d:[Lmaps/an/i;

    invoke-virtual {v0}, [Lmaps/an/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/an/i;

    return-object v0
.end method
