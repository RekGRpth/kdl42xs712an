.class public Lmaps/ao/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/ac/cf;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ac/cf;

    invoke-direct {v0}, Lmaps/ac/cf;-><init>()V

    iput-object v0, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/cf;
    .locals 3

    iget-object v1, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/ac/cf;

    iget-object v2, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    invoke-direct {v0, v2}, Lmaps/ac/cf;-><init>(Lmaps/ac/cf;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ac/bw;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    invoke-interface {p1}, Lmaps/ac/bw;->a()Lmaps/ac/bx;

    move-result-object v3

    invoke-virtual {v2, v3}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v2

    invoke-static {v2, p1}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    invoke-virtual {v0, p1}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lmaps/ac/cf;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance p1, Lmaps/ac/cf;

    invoke-direct {p1}, Lmaps/ac/cf;-><init>()V

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/cf;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bx;

    invoke-static {p1, v0}, Lmaps/ac/cf;->a(Lmaps/ac/cf;Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v4

    iget-object v5, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    invoke-static {v5, v0}, Lmaps/ac/cf;->a(Lmaps/ac/cf;Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v0

    if-nez v0, :cond_4

    if-eqz v4, :cond_2

    invoke-interface {v4, v0}, Lmaps/ac/bw;->a(Lmaps/ac/bw;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-interface {v0, v4}, Lmaps/ac/bw;->a(Lmaps/ac/bw;)Z

    move-result v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final b()J
    .locals 4

    iget-object v1, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ao/a;->a:Lmaps/ac/cf;

    invoke-virtual {v0}, Lmaps/ac/cf;->hashCode()I

    move-result v0

    int-to-long v2, v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
