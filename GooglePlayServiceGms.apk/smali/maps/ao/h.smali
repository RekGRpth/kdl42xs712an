.class final Lmaps/ao/h;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/ag;


# instance fields
.field private a:Lmaps/ao/b;


# direct methods
.method public constructor <init>(Lmaps/ao/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ao/h;->a:Lmaps/ao/b;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;[BIJJ)Lmaps/ac/bs;
    .locals 5

    new-instance v0, Lmaps/bw/a;

    invoke-direct {v0, p2}, Lmaps/bw/a;-><init>([B)V

    invoke-virtual {v0, p3}, Lmaps/bw/a;->skipBytes(I)I

    iget-object v1, p0, Lmaps/ao/h;->a:Lmaps/ao/b;

    invoke-interface {v0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    const v3, 0x44524154

    if-eq v2, v3, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "TILE_MAGIC expected: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_1

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch: 7 or 8 expected, "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0}, Lmaps/ac/bt;->a(Ljava/io/DataInput;)Lmaps/ac/bt;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/bt;->c()I

    move-result v3

    invoke-virtual {p1}, Lmaps/ac/bt;->c()I

    move-result v4

    if-ne v3, v4, :cond_2

    invoke-virtual {v2}, Lmaps/ac/bt;->d()I

    move-result v3

    invoke-virtual {p1}, Lmaps/ac/bt;->d()I

    move-result v4

    if-ne v3, v4, :cond_2

    invoke-virtual {v2}, Lmaps/ac/bt;->b()I

    move-result v3

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v4

    if-eq v3, v4, :cond_3

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Expected tile coords: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but received "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-static {v0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    invoke-static {v0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    invoke-static {v0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v3, v3, [B

    invoke-interface {v0, v3}, Ljava/io/DataInput;->readFully([B)V

    new-instance v0, Lmaps/ac/y;

    invoke-direct {v0, p1, v2, v3, v1}, Lmaps/ac/y;-><init>(Lmaps/ac/bt;I[BLmaps/ao/b;)V

    return-object v0
.end method
