.class public final Lmaps/ab/w;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ab/d;
.implements Lmaps/ab/e;


# instance fields
.field private final b:Lmaps/ae/y;

.field private final c:Lmaps/ae/n;

.field private final d:Lmaps/ae/z;

.field private final e:Ljava/util/Map;

.field private final f:Lmaps/ax/f;

.field private final g:Ljava/util/Set;

.field private volatile h:I

.field private volatile i:I

.field private volatile j:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    new-instance v0, Lmaps/ax/f;

    const/16 v1, 0x30

    invoke-direct {v0, v1}, Lmaps/ax/f;-><init>(I)V

    iput-object v0, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lmaps/ab/w;->g:Ljava/util/Set;

    sget-object v0, Lmaps/ao/b;->a:Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v2, p0, Lmaps/ab/w;->b:Lmaps/ae/y;

    iput-object v2, p0, Lmaps/ab/w;->c:Lmaps/ae/n;

    iput-object v2, p0, Lmaps/ab/w;->d:Lmaps/ae/z;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/ao/b;->a:Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/w;->b:Lmaps/ae/y;

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/w;->c:Lmaps/ae/n;

    new-instance v0, Lmaps/ab/x;

    invoke-direct {v0, p0}, Lmaps/ab/x;-><init>(Lmaps/ab/w;)V

    iput-object v0, p0, Lmaps/ab/w;->d:Lmaps/ae/z;

    iget-object v0, p0, Lmaps/ab/w;->b:Lmaps/ae/y;

    iget-object v1, p0, Lmaps/ab/w;->d:Lmaps/ae/z;

    invoke-interface {v0, v1}, Lmaps/ae/y;->a(Lmaps/ae/z;)V

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ab/w;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lmaps/ab/w;)Lmaps/ax/f;
    .locals 1

    iget-object v0, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/ab/w;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/f;

    invoke-interface {v0}, Lmaps/ab/f;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lmaps/ab/w;)V
    .locals 0

    invoke-direct {p0}, Lmaps/ab/w;->b()V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)Ljava/util/Collection;
    .locals 5

    const/16 v1, 0xe

    iget v0, p0, Lmaps/ab/w;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ab/w;->h:I

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    if-le v0, v1, :cond_3

    invoke-virtual {p1, v1}, Lmaps/ac/bt;->a(I)Lmaps/ac/bt;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    invoke-virtual {v0, v1}, Lmaps/ax/f;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget v1, p0, Lmaps/ab/w;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lmaps/ab/w;->i:I

    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/ab/a;->a(Ljava/util/Collection;Lmaps/ac/be;)Ljava/util/Collection;

    move-result-object v0

    :goto_1
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    const/4 v3, 0x0

    iget-object v4, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    monitor-enter v4

    :try_start_1
    iget-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/b;

    const/4 v2, 0x0

    if-nez v0, :cond_2

    new-instance v0, Lmaps/ab/b;

    iget-object v2, p0, Lmaps/ab/w;->b:Lmaps/ae/y;

    iget-object v3, p0, Lmaps/ab/w;->c:Lmaps/ae/n;

    invoke-direct {v0, v2, v3, v1}, Lmaps/ab/b;-><init>(Lmaps/ae/y;Lmaps/ae/n;Lmaps/ac/bt;)V

    iget-object v2, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    :goto_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_1

    invoke-virtual {v0, p0}, Lmaps/ab/b;->a(Lmaps/ab/d;)V

    iget v0, p0, Lmaps/ab/w;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ab/w;->j:I

    :cond_1
    sget-object v0, Lmaps/ab/e;->a:Ljava/util/Collection;

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_2
    move-object v0, v2

    move v1, v3

    goto :goto_2

    :cond_3
    move-object v1, p1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    invoke-virtual {v0}, Lmaps/ax/f;->d()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Lmaps/ab/w;->b()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ab/b;Ljava/util/Collection;)V
    .locals 3

    iget-object v1, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/ab/b;->a()Lmaps/ac/bt;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/b;

    if-eq v0, p1, :cond_1

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ab/w;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lmaps/ab/b;->a()Lmaps/ac/bt;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    iget-object v1, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lmaps/ab/w;->f:Lmaps/ax/f;

    invoke-virtual {p1}, Lmaps/ab/b;->a()Lmaps/ac/bt;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Lmaps/ax/f;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0}, Lmaps/ab/w;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ab/f;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/w;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lmaps/ac/o;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b(Lmaps/ab/f;)V
    .locals 1

    iget-object v0, p0, Lmaps/ab/w;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method
