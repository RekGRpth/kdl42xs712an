.class public final Lmaps/ab/j;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ab/e;


# instance fields
.field private final b:Lmaps/m/cu;

.field private final c:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/br;->f()Lmaps/m/br;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/j;->b:Lmaps/m/cu;

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/j;->c:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Ljava/util/Collection;Lmaps/ac/cx;)V
    .locals 13

    const-wide v11, 0x40bb580000000000L    # 7000.0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/br;->g()Lmaps/m/bs;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ab/a;

    invoke-virtual {v0}, Lmaps/ab/a;->b()Lmaps/ac/bd;

    move-result-object v1

    invoke-static {v1}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/cx;->d()I

    move-result v5

    int-to-double v5, v5

    invoke-virtual {v1}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v7

    invoke-virtual {v7}, Lmaps/ac/av;->e()D

    move-result-wide v7

    div-double/2addr v5, v7

    invoke-virtual {v1}, Lmaps/ac/cx;->e()I

    move-result v7

    int-to-double v7, v7

    invoke-virtual {v1}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v9

    invoke-virtual {v9}, Lmaps/ac/av;->e()D

    move-result-wide v9

    div-double/2addr v7, v9

    cmpl-double v5, v5, v11

    if-gez v5, :cond_0

    cmpl-double v5, v7, v11

    if-gez v5, :cond_0

    invoke-virtual {v0, v3}, Lmaps/ab/a;->a(Ljava/util/Set;)V

    if-eqz p2, :cond_1

    invoke-virtual {v0}, Lmaps/ab/a;->b()Lmaps/ac/bd;

    move-result-object v5

    invoke-virtual {p2, v5}, Lmaps/ac/cx;->b(Lmaps/ac/be;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_1
    invoke-static {v1}, Lmaps/ac/bt;->a(Lmaps/ac/cx;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/bt;

    invoke-virtual {v2, v1, v0}, Lmaps/m/bs;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bs;

    goto :goto_0

    :cond_2
    invoke-static {v3}, Lmaps/m/bo;->a(Ljava/util/Collection;)Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/j;->c:Ljava/util/Set;

    invoke-virtual {v2}, Lmaps/m/bs;->a()Lmaps/m/br;

    move-result-object v0

    iput-object v0, p0, Lmaps/ab/j;->b:Lmaps/m/cu;

    return-void
.end method

.method public static a(Ljava/io/Reader;Lmaps/ac/cx;)Lmaps/ab/j;
    .locals 13

    const/4 v12, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x3

    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v2, "\\s+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v0, v6

    if-ge v0, v1, :cond_2

    move-object v0, v3

    goto :goto_1

    :cond_2
    aget-object v0, v6, v12

    invoke-static {v0}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v7

    const/4 v0, 0x1

    aget-object v0, v6, v0

    invoke-static {v0}, Lmaps/bp/a;->a(Ljava/lang/String;)Lmaps/bp/a;

    move-result-object v8

    const/4 v0, 0x2

    aget-object v0, v6, v0

    invoke-static {v0}, Lmaps/bp/a;->a(Ljava/lang/String;)Lmaps/bp/a;

    move-result-object v9

    if-eqz v7, :cond_3

    if-eqz v8, :cond_3

    if-nez v9, :cond_4

    :cond_3
    move-object v0, v3

    goto :goto_1

    :cond_4
    array-length v0, v6

    if-le v0, v1, :cond_7

    aget-object v0, v6, v1

    invoke-static {v0}, Lmaps/ab/a;->a(Ljava/lang/String;)Lmaps/ac/av;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v0, 0x4

    :goto_2
    invoke-virtual {v8}, Lmaps/bp/a;->a()I

    move-result v10

    invoke-virtual {v8}, Lmaps/bp/a;->b()I

    move-result v8

    invoke-static {v10, v8}, Lmaps/ac/av;->b(II)Lmaps/ac/av;

    move-result-object v8

    invoke-virtual {v9}, Lmaps/bp/a;->a()I

    move-result v10

    invoke-virtual {v9}, Lmaps/bp/a;->b()I

    move-result v9

    invoke-static {v10, v9}, Lmaps/ac/av;->b(II)Lmaps/ac/av;

    move-result-object v9

    array-length v10, v6

    sub-int/2addr v10, v0

    new-array v10, v10, [Ljava/lang/String;

    array-length v11, v10

    invoke-static {v6, v0, v10, v12, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Lmaps/ab/a;

    invoke-static {v8, v9}, Lmaps/ac/bd;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/bd;

    move-result-object v6

    invoke-direct {v0, v7, v6, v2, v10}, Lmaps/ab/a;-><init>(Lmaps/ac/r;Lmaps/ac/j;Lmaps/ac/av;[Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    new-instance v0, Lmaps/ab/j;

    invoke-direct {v0, v5, p1}, Lmaps/ab/j;-><init>(Ljava/util/Collection;Lmaps/ac/cx;)V

    return-object v0

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v1

    move-object v2, v3

    goto :goto_2
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;)Ljava/util/Collection;
    .locals 3

    const/16 v2, 0xf

    invoke-virtual {p1}, Lmaps/ac/bt;->a()Lmaps/ac/bt;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v1

    if-ge v1, v2, :cond_0

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lmaps/ab/j;->b:Lmaps/m/cu;

    invoke-interface {v1, v0}, Lmaps/m/cu;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Lmaps/ac/bt;->a(I)Lmaps/ac/bt;

    move-result-object v1

    iget-object v2, p0, Lmaps/ab/j;->b:Lmaps/m/cu;

    invoke-interface {v2, v1}, Lmaps/m/cu;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/ab/a;->a(Ljava/util/Collection;Lmaps/ac/be;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lmaps/ab/f;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/ac/o;)Z
    .locals 1

    iget-object v0, p0, Lmaps/ab/j;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lmaps/ab/f;)V
    .locals 0

    return-void
.end method
