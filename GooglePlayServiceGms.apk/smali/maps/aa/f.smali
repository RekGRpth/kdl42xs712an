.class final Lmaps/aa/f;
.super Lmaps/aa/u;


# instance fields
.field private d:[Z


# direct methods
.method constructor <init>(Lmaps/aa/r;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lmaps/aa/u;-><init>(Lmaps/aa/r;)V

    invoke-virtual {p0}, Lmaps/aa/f;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    new-array v0, v0, [Z

    iput-object v0, p0, Lmaps/aa/f;->d:[Z

    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([ZZ)V

    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, v2

    return-void
.end method

.method private static a([DDDII)I
    .locals 3

    move v0, p5

    :goto_0
    if-ge v0, p6, :cond_1

    mul-int/lit8 v1, v0, 0x2

    aget-wide v1, p0, v1

    cmpl-double v1, v1, p1

    if-nez v1, :cond_0

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v1, v1, 0x1

    aget-wide v1, p0, v1

    cmpl-double v1, v1, p3

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static a(DDDDDD)Z
    .locals 8

    sub-double v0, p0, p4

    sub-double v2, p2, p6

    sub-double v4, p8, p4

    sub-double v6, p10, p6

    mul-double/2addr v0, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a([I)Lmaps/aa/u;
    .locals 30

    invoke-virtual/range {p0 .. p0}, Lmaps/aa/f;->d()I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lmaps/aa/f;->e()I

    move-result v2

    move-object/from16 v0, p1

    array-length v3, v0

    add-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x2

    new-array v2, v2, [D

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lmaps/aa/a;->g(I)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5, v8}, Lmaps/aa/a;->a(I[DII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->d:[Z

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/util/Arrays;->fill([ZZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->d:[Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    aput-boolean v5, v3, v4

    const/4 v3, 0x0

    move/from16 v21, v3

    :goto_1
    move-object/from16 v0, p1

    array-length v3, v0

    move/from16 v0, v21

    if-ge v0, v3, :cond_9

    aget v4, p1, v21

    add-int/lit8 v3, v21, 0x1

    aget v5, p1, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v3, v5}, Lmaps/aa/a;->f(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/aa/f;->d:[Z

    aget-boolean v6, v6, v3

    if-eqz v6, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v3, v4}, Lmaps/aa/a;->f(I)I

    move-result v3

    move/from16 v22, v3

    move/from16 v23, v4

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Lmaps/aa/a;->g(I)I

    move-result v27

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    add-int/lit8 v4, v22, 0x1

    invoke-virtual {v3, v4}, Lmaps/aa/a;->g(I)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v3, v5}, Lmaps/aa/a;->a(I)D

    move-result-wide v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v6, v5}, Lmaps/aa/a;->b(I)D

    move-result-wide v5

    const/4 v7, 0x0

    invoke-static/range {v2 .. v8}, Lmaps/aa/f;->a([DDDII)I

    move-result v9

    add-int/lit8 v7, v9, 0x1

    invoke-static/range {v2 .. v8}, Lmaps/aa/f;->a([DDDII)I

    move-result v7

    const/4 v10, -0x1

    if-eq v7, v10, :cond_a

    const/4 v7, 0x0

    move/from16 v26, v7

    move/from16 v25, v9

    :goto_3
    if-nez v26, :cond_6

    add-int/lit8 v7, v25, -0x1

    rem-int/2addr v7, v8

    if-gez v7, :cond_1

    add-int/2addr v7, v8

    :cond_1
    add-int/lit8 v9, v25, 0x1

    rem-int/2addr v9, v8

    if-gez v9, :cond_3

    add-int/2addr v9, v8

    move/from16 v24, v9

    :goto_4
    mul-int/lit8 v9, v7, 0x2

    aget-wide v9, v2, v9

    mul-int/lit8 v11, v7, 0x2

    add-int/lit8 v11, v11, 0x1

    aget-wide v11, v2, v11

    mul-int/lit8 v13, v25, 0x2

    aget-wide v13, v2, v13

    mul-int/lit8 v15, v25, 0x2

    add-int/lit8 v15, v15, 0x1

    aget-wide v15, v2, v15

    mul-int/lit8 v17, v24, 0x2

    aget-wide v17, v2, v17

    mul-int/lit8 v19, v24, 0x2

    add-int/lit8 v19, v19, 0x1

    aget-wide v19, v2, v19

    invoke-static/range {v9 .. v20}, Lmaps/aa/f;->a(DDDDDD)Z

    move-result v29

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lmaps/aa/f;->b(I)D

    move-result-wide v9

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lmaps/aa/f;->c(I)D

    move-result-wide v11

    mul-int/lit8 v13, v25, 0x2

    aget-wide v13, v2, v13

    mul-int/lit8 v15, v25, 0x2

    add-int/lit8 v15, v15, 0x1

    aget-wide v15, v2, v15

    mul-int/lit8 v17, v24, 0x2

    aget-wide v17, v2, v17

    mul-int/lit8 v19, v24, 0x2

    add-int/lit8 v19, v19, 0x1

    aget-wide v19, v2, v19

    invoke-static/range {v9 .. v20}, Lmaps/aa/f;->a(DDDDDD)Z

    move-result v24

    mul-int/lit8 v9, v7, 0x2

    aget-wide v9, v2, v9

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-wide v11, v2, v7

    mul-int/lit8 v7, v25, 0x2

    aget-wide v13, v2, v7

    mul-int/lit8 v7, v25, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-wide v15, v2, v7

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lmaps/aa/f;->b(I)D

    move-result-wide v17

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lmaps/aa/f;->c(I)D

    move-result-wide v19

    invoke-static/range {v9 .. v20}, Lmaps/aa/f;->a(DDDDDD)Z

    move-result v7

    if-eqz v29, :cond_4

    if-eqz v24, :cond_5

    if-eqz v7, :cond_5

    :cond_2
    add-int/lit8 v7, v25, 0x1

    invoke-static/range {v2 .. v8}, Lmaps/aa/f;->a([DDDII)I

    move-result v25

    goto/16 :goto_3

    :cond_3
    move/from16 v24, v9

    goto/16 :goto_4

    :cond_4
    if-nez v24, :cond_2

    if-nez v7, :cond_2

    :cond_5
    const/4 v7, 0x1

    move/from16 v26, v7

    goto/16 :goto_3

    :cond_6
    move/from16 v7, v25

    :goto_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Lmaps/aa/a;->a(I)D

    move-result-wide v9

    cmpl-double v3, v9, v3

    if-nez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Lmaps/aa/a;->b(I)D

    move-result-wide v3

    cmpl-double v3, v3, v5

    if-eqz v3, :cond_8

    :cond_7
    sub-int v3, v8, v7

    add-int v4, v7, v28

    sub-int v4, v4, v27

    add-int/lit8 v4, v4, 0x2

    mul-int/lit8 v5, v7, 0x2

    mul-int/lit8 v4, v4, 0x2

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v2, v5, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v7, 0x1

    sub-int v4, v28, v23

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v23

    invoke-virtual {v5, v0, v2, v3, v4}, Lmaps/aa/a;->a(I[DII)V

    add-int/2addr v3, v4

    sub-int v4, v23, v27

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v27

    invoke-virtual {v5, v0, v2, v3, v4}, Lmaps/aa/a;->a(I[DII)V

    sub-int v3, v28, v27

    add-int/lit8 v3, v3, 0x2

    add-int/2addr v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->d:[Z

    const/4 v4, 0x1

    aput-boolean v4, v3, v22

    :goto_6
    add-int/lit8 v3, v21, 0x2

    move/from16 v21, v3

    goto/16 :goto_1

    :cond_8
    sub-int v3, v8, v7

    add-int/lit8 v3, v3, -0x1

    add-int v4, v7, v28

    sub-int v4, v4, v27

    add-int/lit8 v5, v7, 0x1

    mul-int/lit8 v5, v5, 0x2

    mul-int/lit8 v4, v4, 0x2

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v2, v5, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v7, 0x1

    sub-int v4, v28, v23

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v23

    invoke-virtual {v5, v0, v2, v3, v4}, Lmaps/aa/a;->a(I[DII)V

    add-int/2addr v3, v4

    sub-int v4, v23, v27

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/aa/f;->a:Lmaps/aa/a;

    move/from16 v0, v27

    invoke-virtual {v5, v0, v2, v3, v4}, Lmaps/aa/a;->a(I[DII)V

    sub-int v3, v28, v27

    add-int/lit8 v3, v3, 0x2

    add-int/2addr v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/aa/f;->d:[Z

    const/4 v4, 0x1

    aput-boolean v4, v3, v22

    goto :goto_6

    :cond_9
    new-instance p0, Lmaps/aa/u;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lmaps/aa/u;-><init>([D)V

    goto/16 :goto_0

    :cond_a
    move v7, v9

    goto/16 :goto_5

    :cond_b
    move/from16 v22, v3

    move/from16 v23, v5

    move v5, v4

    goto/16 :goto_2
.end method

.method public final a(II)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v2, p1}, Lmaps/aa/a;->f(I)I

    move-result v2

    iget-object v3, p0, Lmaps/aa/f;->a:Lmaps/aa/a;

    invoke-virtual {v3, p2}, Lmaps/aa/a;->f(I)I

    move-result v3

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v4, p0, Lmaps/aa/f;->d:[Z

    aget-boolean v4, v4, v2

    if-eqz v4, :cond_2

    iget-object v4, p0, Lmaps/aa/f;->d:[Z

    aget-boolean v4, v4, v3

    if-nez v4, :cond_0

    :cond_2
    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    aput-boolean v1, v0, v3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    aget-boolean v0, v0, v3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/aa/f;->d:[Z

    aput-boolean v1, v0, v2

    move v0, v1

    goto :goto_0

    :cond_4
    new-instance v0, Lmaps/aa/j;

    const-string v1, "Some outer chains have not been cut."

    invoke-direct {v0, v1}, Lmaps/aa/j;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lmaps/aa/f;

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lmaps/aa/f;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lmaps/aa/f;

    instance-of v2, p0, Lmaps/aa/f;

    if-eqz v2, :cond_3

    invoke-super {p0, p1}, Lmaps/aa/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmaps/aa/f;->d:[Z

    iget-object v3, p1, Lmaps/aa/f;->d:[Z

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    invoke-super {p0}, Lmaps/aa/u;->hashCode()I

    move-result v0

    iget-object v1, p0, Lmaps/aa/f;->d:[Z

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Z)I

    move-result v1

    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    return v0
.end method
