.class public final enum Lmaps/aa/q;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lmaps/aa/q;

.field public static final enum b:Lmaps/aa/q;

.field public static final enum c:Lmaps/aa/q;

.field public static final enum d:Lmaps/aa/q;

.field private static enum e:Lmaps/aa/q;

.field private static final synthetic f:[Lmaps/aa/q;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/aa/q;

    const-string v1, "NO_VERIFICATION"

    invoke-direct {v0, v1, v2}, Lmaps/aa/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aa/q;->e:Lmaps/aa/q;

    new-instance v0, Lmaps/aa/q;

    const-string v1, "NO_VERIFICATION_WITH_REASON"

    invoke-direct {v0, v1, v3}, Lmaps/aa/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aa/q;->a:Lmaps/aa/q;

    new-instance v0, Lmaps/aa/q;

    const-string v1, "AREA_VERIFICATION"

    invoke-direct {v0, v1, v4}, Lmaps/aa/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aa/q;->b:Lmaps/aa/q;

    new-instance v0, Lmaps/aa/q;

    const-string v1, "AREA_VERIFICATION_WITH_REASON"

    invoke-direct {v0, v1, v5}, Lmaps/aa/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aa/q;->c:Lmaps/aa/q;

    new-instance v0, Lmaps/aa/q;

    const-string v1, "COMPLETE_VERIFICATION"

    invoke-direct {v0, v1, v6}, Lmaps/aa/q;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/aa/q;->d:Lmaps/aa/q;

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/aa/q;

    sget-object v1, Lmaps/aa/q;->e:Lmaps/aa/q;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/aa/q;->a:Lmaps/aa/q;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/aa/q;->b:Lmaps/aa/q;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/aa/q;->c:Lmaps/aa/q;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/aa/q;->d:Lmaps/aa/q;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/aa/q;->f:[Lmaps/aa/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/aa/q;
    .locals 1

    const-class v0, Lmaps/aa/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/aa/q;

    return-object v0
.end method

.method public static values()[Lmaps/aa/q;
    .locals 1

    sget-object v0, Lmaps/aa/q;->f:[Lmaps/aa/q;

    invoke-virtual {v0}, [Lmaps/aa/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/aa/q;

    return-object v0
.end method
