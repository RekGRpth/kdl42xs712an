.class public final Lmaps/aa/t;
.super Ljava/lang/Object;


# instance fields
.field private final a:D

.field private final b:D


# direct methods
.method private constructor <init>(DD)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lmaps/aa/t;->a:D

    iput-wide p3, p0, Lmaps/aa/t;->b:D

    return-void
.end method

.method public static a(I)D
    .locals 4

    invoke-static {p0}, Lmaps/ac/av;->c(I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x41c0000000000000L    # 5.36870912E8

    div-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public static a()Lmaps/aa/t;
    .locals 5

    new-instance v0, Lmaps/aa/t;

    const-wide v1, 0x400921fb54442d18L    # Math.PI

    const-wide v3, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    return-object v0
.end method

.method public static a(DD)Lmaps/aa/t;
    .locals 5

    const-wide v3, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    const-wide v0, 0x400921fb54442d18L    # Math.PI

    cmpl-double v2, p0, v3

    if-nez v2, :cond_0

    move-wide p0, v0

    :cond_0
    cmpl-double v2, p2, v3

    if-nez v2, :cond_1

    move-wide p2, v0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lmaps/aa/t;->b(DD)D

    move-result-wide v2

    cmpg-double v0, v2, v0

    if-gtz v0, :cond_2

    new-instance v0, Lmaps/aa/t;

    invoke-direct {v0, p0, p1, p2, p3}, Lmaps/aa/t;-><init>(DD)V

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lmaps/aa/t;

    invoke-direct {v0, p2, p3, p0, p1}, Lmaps/aa/t;-><init>(DD)V

    goto :goto_0
.end method

.method private static b(DD)D
    .locals 6

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    sub-double v0, p2, p0

    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    add-double v0, p2, v4

    sub-double v2, p0, v4

    sub-double/2addr v0, v2

    goto :goto_0
.end method

.method private static e()Lmaps/aa/t;
    .locals 5

    new-instance v0, Lmaps/aa/t;

    const-wide v1, -0x3ff6de04abbbd2e8L    # -3.141592653589793

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    return-object v0
.end method

.method private f()Z
    .locals 4

    iget-wide v0, p0, Lmaps/aa/t;->a:D

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    sub-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    iget-wide v0, p0, Lmaps/aa/t;->a:D

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/aa/t;)Lmaps/aa/t;
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1}, Lmaps/aa/t;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-wide v2, p1, Lmaps/aa/t;->a:D

    invoke-virtual {p0, v2, v3}, Lmaps/aa/t;->a(D)Z

    move-result v2

    if-eqz v2, :cond_b

    iget-wide v2, p1, Lmaps/aa/t;->b:D

    invoke-virtual {p0, v2, v3}, Lmaps/aa/t;->a(D)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-direct {p0}, Lmaps/aa/t;->g()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct {p1}, Lmaps/aa/t;->g()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-wide v2, p1, Lmaps/aa/t;->a:D

    iget-wide v4, p0, Lmaps/aa/t;->a:D

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_3

    iget-wide v2, p1, Lmaps/aa/t;->b:D

    iget-wide v4, p0, Lmaps/aa/t;->b:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_3

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    invoke-static {}, Lmaps/aa/t;->e()Lmaps/aa/t;

    move-result-object p0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    iget-wide v2, p1, Lmaps/aa/t;->a:D

    iget-wide v4, p0, Lmaps/aa/t;->a:D

    cmpl-double v2, v2, v4

    if-gez v2, :cond_5

    iget-wide v2, p1, Lmaps/aa/t;->b:D

    iget-wide v4, p0, Lmaps/aa/t;->b:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_6

    :cond_5
    invoke-direct {p0}, Lmaps/aa/t;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    invoke-direct {p1}, Lmaps/aa/t;->g()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lmaps/aa/t;->b()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p1}, Lmaps/aa/t;->f()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_1

    :cond_8
    iget-wide v2, p1, Lmaps/aa/t;->a:D

    iget-wide v4, p0, Lmaps/aa/t;->a:D

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_9

    iget-wide v2, p1, Lmaps/aa/t;->b:D

    iget-wide v4, p0, Lmaps/aa/t;->b:D

    cmpg-double v2, v2, v4

    if-lez v2, :cond_2

    :cond_9
    move v0, v1

    goto :goto_1

    :cond_a
    new-instance v0, Lmaps/aa/t;

    iget-wide v1, p0, Lmaps/aa/t;->a:D

    iget-wide v3, p1, Lmaps/aa/t;->b:D

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    move-object p0, v0

    goto :goto_0

    :cond_b
    iget-wide v0, p1, Lmaps/aa/t;->b:D

    invoke-virtual {p0, v0, v1}, Lmaps/aa/t;->a(D)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lmaps/aa/t;

    iget-wide v1, p1, Lmaps/aa/t;->a:D

    iget-wide v3, p0, Lmaps/aa/t;->b:D

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    move-object p0, v0

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0}, Lmaps/aa/t;->f()Z

    move-result v0

    if-nez v0, :cond_d

    iget-wide v0, p0, Lmaps/aa/t;->a:D

    invoke-virtual {p1, v0, v1}, Lmaps/aa/t;->a(D)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    move-object p0, p1

    goto/16 :goto_0

    :cond_e
    iget-wide v0, p1, Lmaps/aa/t;->b:D

    iget-wide v2, p0, Lmaps/aa/t;->a:D

    invoke-static {v0, v1, v2, v3}, Lmaps/aa/t;->b(DD)D

    move-result-wide v0

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    iget-wide v4, p1, Lmaps/aa/t;->a:D

    invoke-static {v2, v3, v4, v5}, Lmaps/aa/t;->b(DD)D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_f

    new-instance v0, Lmaps/aa/t;

    iget-wide v1, p1, Lmaps/aa/t;->a:D

    iget-wide v3, p0, Lmaps/aa/t;->b:D

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    move-object p0, v0

    goto/16 :goto_0

    :cond_f
    new-instance v0, Lmaps/aa/t;

    iget-wide v1, p0, Lmaps/aa/t;->a:D

    iget-wide v3, p1, Lmaps/aa/t;->b:D

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    move-object p0, v0

    goto/16 :goto_0
.end method

.method public final a(D)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lmaps/aa/t;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lmaps/aa/t;->a:D

    cmpl-double v2, p1, v2

    if-gez v2, :cond_0

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_2

    :cond_0
    invoke-direct {p0}, Lmaps/aa/t;->f()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lmaps/aa/t;->a:D

    cmpl-double v2, p1, v2

    if-ltz v2, :cond_4

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    cmpg-double v2, p1, v2

    if-lez v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final b()Z
    .locals 4

    iget-wide v0, p0, Lmaps/aa/t;->b:D

    iget-wide v2, p0, Lmaps/aa/t;->a:D

    sub-double/2addr v0, v2

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()D
    .locals 8

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iget-wide v2, p0, Lmaps/aa/t;->a:D

    iget-wide v4, p0, Lmaps/aa/t;->b:D

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    invoke-direct {p0}, Lmaps/aa/t;->g()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_1

    add-double/2addr v0, v6

    goto :goto_0

    :cond_1
    sub-double/2addr v0, v6

    goto :goto_0
.end method

.method public final d()Lmaps/aa/t;
    .locals 5

    iget-wide v0, p0, Lmaps/aa/t;->a:D

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {}, Lmaps/aa/t;->e()Lmaps/aa/t;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/aa/t;

    iget-wide v1, p0, Lmaps/aa/t;->b:D

    iget-wide v3, p0, Lmaps/aa/t;->a:D

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/aa/t;-><init>(DD)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/aa/t;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/aa/t;

    iget-wide v1, p0, Lmaps/aa/t;->a:D

    iget-wide v3, p1, Lmaps/aa/t;->a:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lmaps/aa/t;->b:D

    iget-wide v3, p1, Lmaps/aa/t;->b:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 4

    const-wide/16 v0, 0x275

    iget-wide v2, p0, Lmaps/aa/t;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x25

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lmaps/aa/t;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    add-long/2addr v0, v2

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lmaps/aa/t;->a:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lmaps/aa/t;->b:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
