.class public abstract Lmaps/q/f;
.super Lmaps/q/a;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Ljava/lang/reflect/Type;

.field private transient b:Lmaps/q/b;


# direct methods
.method protected constructor <init>()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lmaps/q/a;-><init>()V

    invoke-virtual {p0}, Lmaps/q/f;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead."

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 1

    invoke-direct {p0}, Lmaps/q/a;-><init>()V

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Type;

    iput-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/reflect/Type;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/q/f;-><init>(Ljava/lang/reflect/Type;)V

    return-void
.end method

.method private static a([Ljava/lang/reflect/Type;)Lmaps/m/ay;
    .locals 5

    invoke-static {}, Lmaps/m/ay;->f()Lmaps/m/ba;

    move-result-object v1

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    invoke-static {v3}, Lmaps/q/f;->a(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v3

    iget-object v4, v3, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v4}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isInterface()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v3}, Lmaps/m/ba;->b(Ljava/lang/Object;)Lmaps/m/ba;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lmaps/m/ba;->a()Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lmaps/q/f;)Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/f;->e(Ljava/lang/reflect/Type;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Lmaps/q/f;
    .locals 1

    new-instance v0, Lmaps/q/g;

    invoke-direct {v0, p0}, Lmaps/q/g;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method private static a(Ljava/lang/reflect/Type;)Lmaps/q/f;
    .locals 1

    new-instance v0, Lmaps/q/g;

    invoke-direct {v0, p0}, Lmaps/q/g;-><init>(Ljava/lang/reflect/Type;)V

    return-object v0
.end method

.method static synthetic b(Lmaps/q/f;)Ljava/lang/reflect/Type;
    .locals 1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method private static b([Ljava/lang/reflect/Type;)Lmaps/m/bo;
    .locals 4

    invoke-static {}, Lmaps/m/bo;->k()Lmaps/m/bq;

    move-result-object v1

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    invoke-static {v3}, Lmaps/q/f;->e(Ljava/lang/reflect/Type;)Lmaps/m/bo;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/m/bq;->b(Ljava/lang/Iterable;)Lmaps/m/bq;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lmaps/m/bq;->a()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/reflect/Type;)Lmaps/q/f;
    .locals 2

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/q/f;->b:Lmaps/q/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/b;->a(Ljava/lang/reflect/Type;)Lmaps/q/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/q/f;->b:Lmaps/q/b;

    :cond_0
    invoke-virtual {v0, p1}, Lmaps/q/b;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->a(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v0

    iget-object v1, p0, Lmaps/q/f;->b:Lmaps/q/b;

    iput-object v1, v0, Lmaps/q/f;->b:Lmaps/q/b;

    return-object v0
.end method

.method private static c(Ljava/lang/reflect/Type;)Lmaps/q/f;
    .locals 2

    invoke-static {p0}, Lmaps/q/f;->a(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v0

    iget-object v1, v0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v1}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private static d(Ljava/lang/reflect/Type;)Ljava/lang/Class;
    .locals 1

    invoke-static {p0}, Lmaps/q/f;->e(Ljava/lang/reflect/Type;)Lmaps/m/bo;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bo;->b()Lmaps/m/ee;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ee;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    return-object v0
.end method

.method private static e(Ljava/lang/reflect/Type;)Lmaps/m/bo;
    .locals 3

    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Class;

    invoke-static {p0}, Lmaps/m/bo;->a(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {p0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    goto :goto_0

    :cond_1
    instance-of v0, p0, Ljava/lang/reflect/GenericArrayType;

    if-eqz v0, :cond_2

    check-cast p0, Ljava/lang/reflect/GenericArrayType;

    invoke-interface {p0}, Ljava/lang/reflect/GenericArrayType;->getGenericComponentType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/p;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    goto :goto_0

    :cond_2
    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_3

    check-cast p0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {p0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->b([Ljava/lang/reflect/Type;)Lmaps/m/bo;

    move-result-object v0

    goto :goto_0

    :cond_3
    instance-of v0, p0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_4

    check-cast p0, Ljava/lang/reflect/WildcardType;

    invoke-interface {p0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->b([Ljava/lang/reflect/Type;)Lmaps/m/bo;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unsupported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public final b()Ljava/lang/Class;
    .locals 1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method final c()Lmaps/q/f;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lmaps/q/f;->c(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-static {v0}, Lmaps/q/f;->c(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lmaps/q/f;->b(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v0

    goto :goto_0
.end method

.method final d()Lmaps/m/ay;
    .locals 5

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->a([Ljava/lang/reflect/Type;)Lmaps/m/ay;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    instance-of v0, v0, Ljava/lang/reflect/WildcardType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    check-cast v0, Ljava/lang/reflect/WildcardType;

    invoke-interface {v0}, Ljava/lang/reflect/WildcardType;->getUpperBounds()[Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lmaps/q/f;->a([Ljava/lang/reflect/Type;)Lmaps/m/ay;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lmaps/m/ay;->f()Lmaps/m/ba;

    move-result-object v1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/f;->d(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericInterfaces()[Ljava/lang/reflect/Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lmaps/q/f;->b(Ljava/lang/reflect/Type;)Lmaps/q/f;

    move-result-object v4

    invoke-virtual {v1, v4}, Lmaps/m/ba;->b(Ljava/lang/Object;)Lmaps/m/ba;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lmaps/m/ba;->a()Lmaps/m/ay;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Lmaps/q/o;
    .locals 1

    new-instance v0, Lmaps/q/o;

    invoke-direct {v0, p0}, Lmaps/q/o;-><init>(Lmaps/q/f;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lmaps/q/f;

    if-eqz v0, :cond_0

    check-cast p1, Lmaps/q/f;

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    iget-object v1, p1, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/q/f;->a:Ljava/lang/reflect/Type;

    invoke-static {v0}, Lmaps/q/p;->b(Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
