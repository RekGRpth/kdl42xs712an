.class abstract Lmaps/q/h;
.super Ljava/lang/Object;


# static fields
.field static final a:Lmaps/q/h;

.field static final b:Lmaps/q/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/q/i;

    invoke-direct {v0}, Lmaps/q/i;-><init>()V

    sput-object v0, Lmaps/q/h;->a:Lmaps/q/h;

    new-instance v0, Lmaps/q/j;

    invoke-direct {v0}, Lmaps/q/j;-><init>()V

    sput-object v0, Lmaps/q/h;->b:Lmaps/q/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lmaps/q/h;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/util/Map;)I
    .locals 3

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lmaps/q/h;->b(Ljava/lang/Object;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, p1}, Lmaps/q/h;->c(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lmaps/q/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lmaps/q/h;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v1, p2}, Lmaps/q/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_3
    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/Iterable;)Lmaps/m/ay;
    .locals 3

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lmaps/q/h;->a(Ljava/lang/Object;Ljava/util/Map;)I

    goto :goto_0

    :cond_0
    invoke-static {}, Lmaps/m/cy;->b()Lmaps/m/cy;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/m/cy;->a()Lmaps/m/cy;

    move-result-object v1

    new-instance v2, Lmaps/q/k;

    invoke-direct {v2, v1, v0}, Lmaps/q/k;-><init>(Ljava/util/Comparator;Ljava/util/Map;)V

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmaps/m/cy;->a(Ljava/lang/Iterable;)Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/lang/Object;)Lmaps/m/ay;
    .locals 1

    invoke-static {p1}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/q/h;->a(Ljava/lang/Iterable;)Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/Object;)Ljava/lang/Class;
.end method

.method abstract c(Ljava/lang/Object;)Ljava/lang/Iterable;
.end method

.method abstract d(Ljava/lang/Object;)Ljava/lang/Object;
.end method
