.class abstract enum Lmaps/q/x;
.super Ljava/lang/Enum;


# static fields
.field static final a:Lmaps/q/x;

.field private static enum b:Lmaps/q/x;

.field private static enum c:Lmaps/q/x;

.field private static final synthetic d:[Lmaps/q/x;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/q/z;

    const-string v1, "JAVA6"

    invoke-direct {v0, v1}, Lmaps/q/z;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/x;->b:Lmaps/q/x;

    new-instance v0, Lmaps/q/aa;

    const-string v1, "JAVA7"

    invoke-direct {v0, v1}, Lmaps/q/aa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmaps/q/x;->c:Lmaps/q/x;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/q/x;

    const/4 v1, 0x0

    sget-object v2, Lmaps/q/x;->b:Lmaps/q/x;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lmaps/q/x;->c:Lmaps/q/x;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/q/x;->d:[Lmaps/q/x;

    new-instance v0, Lmaps/q/y;

    invoke-direct {v0}, Lmaps/q/y;-><init>()V

    invoke-virtual {v0}, Lmaps/q/y;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/q/x;->c:Lmaps/q/x;

    :goto_0
    sput-object v0, Lmaps/q/x;->a:Lmaps/q/x;

    return-void

    :cond_0
    sget-object v0, Lmaps/q/x;->b:Lmaps/q/x;

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/q/x;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/q/x;
    .locals 1

    const-class v0, Lmaps/q/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/q/x;

    return-object v0
.end method

.method public static values()[Lmaps/q/x;
    .locals 1

    sget-object v0, Lmaps/q/x;->d:[Lmaps/q/x;

    invoke-virtual {v0}, [Lmaps/q/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/q/x;

    return-object v0
.end method


# virtual methods
.method abstract a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method

.method final a([Ljava/lang/reflect/Type;)Lmaps/m/ay;
    .locals 4

    invoke-static {}, Lmaps/m/ay;->f()Lmaps/m/ba;

    move-result-object v1

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p1, v0

    invoke-virtual {p0, v3}, Lmaps/q/x;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/m/ba;->b(Ljava/lang/Object;)Lmaps/m/ba;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lmaps/m/ba;->a()Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method abstract b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
.end method
