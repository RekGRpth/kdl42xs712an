.class public final Lmaps/ap/f;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/aj/p;
.implements Lmaps/ak/c;


# static fields
.field public static volatile a:Z

.field public static final b:Ljava/lang/ThreadLocal;

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final i:Ljava/util/Comparator;


# instance fields
.field private final A:Lmaps/az/b;

.field private B:Lmaps/az/c;

.field private final C:Ljava/util/HashSet;

.field private final D:Ljava/util/HashSet;

.field private final E:[I

.field private final F:Ljava/util/List;

.field private G:J

.field private H:Z

.field private final I:Lmaps/ay/ar;

.field private J:Z

.field private K:Landroid/graphics/Bitmap;

.field private L:Z

.field private M:F

.field private N:J

.field private volatile O:Lmaps/ap/b;

.field private final P:Ljava/util/List;

.field private final Q:Ljava/util/List;

.field private R:Lmaps/ap/e;

.field private volatile S:Lmaps/ap/e;

.field private volatile T:Z

.field private U:J

.field private V:I

.field private volatile W:Lmaps/ay/ap;

.field private X:Z

.field private volatile Y:F

.field private final Z:Ljava/lang/Object;

.field private aa:Z

.field private volatile ab:I

.field private ac:Z

.field private ad:I

.field private ae:J

.field private af:Ljava/util/Map;

.field private ag:Ljava/util/List;

.field private ah:Z

.field private ai:Z

.field private final aj:Lmaps/an/w;

.field private final ak:Lmaps/an/m;

.field private volatile al:J

.field private final am:Ljava/lang/Object;

.field private volatile g:Lmaps/av/a;

.field private volatile h:Lmaps/ar/b;

.field private j:Lmaps/as/a;

.field private volatile k:I

.field private volatile l:I

.field private final m:Ljava/util/LinkedList;

.field private final n:Ljava/util/ArrayList;

.field private final o:Ljava/util/ArrayList;

.field private final p:Ljava/util/ArrayList;

.field private final q:Lmaps/ar/a;

.field private final r:Lmaps/aj/a;

.field private final s:Lmaps/al/k;

.field private final t:Landroid/content/res/Resources;

.field private final u:F

.field private v:Lmaps/am/d;

.field private final w:Lmaps/ay/ai;

.field private final x:Lmaps/ay/ai;

.field private final y:Lmaps/ay/l;

.field private final z:Lmaps/ay/o;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x4

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/ap/f;->a:Z

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmaps/ap/f;->c:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lmaps/ap/f;->d:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lmaps/ap/f;->e:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lmaps/ap/f;->f:[I

    new-instance v0, Lmaps/ap/g;

    invoke-direct {v0}, Lmaps/ap/g;-><init>()V

    sput-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    new-instance v0, Lmaps/ap/h;

    invoke-direct {v0}, Lmaps/ap/h;-><init>()V

    sput-object v0, Lmaps/ap/f;->i:Ljava/util/Comparator;

    return-void

    nop

    :array_0
    .array-data 4
        0xed00
        0xea00
        0xe200
        0x10000
    .end array-data

    :array_1
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_2
    .array-data 4
        0x8000
        0x8000
        0x8000
        0x10000
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Lmaps/aj/a;Landroid/content/res/Resources;Lmaps/ar/a;Lmaps/ay/ap;)V
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lmaps/ap/f;->l:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->C:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->D:Ljava/util/HashSet;

    new-array v0, v3, [I

    iput-object v0, p0, Lmaps/ap/f;->E:[I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->F:Ljava/util/List;

    iput-wide v5, p0, Lmaps/ap/f;->G:J

    new-instance v0, Lmaps/ap/i;

    invoke-direct {v0, p0}, Lmaps/ap/i;-><init>(Lmaps/ap/f;)V

    iput-object v0, p0, Lmaps/ap/f;->I:Lmaps/ay/ar;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->P:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->Q:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->Z:Ljava/lang/Object;

    iput-boolean v1, p0, Lmaps/ap/f;->aa:Z

    iput v1, p0, Lmaps/ap/f;->ab:I

    iput-boolean v1, p0, Lmaps/ap/f;->ac:Z

    const v0, 0x7fffffff

    iput v0, p0, Lmaps/ap/f;->ad:I

    iput-wide v5, p0, Lmaps/ap/f;->ae:J

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->af:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    iput-boolean v3, p0, Lmaps/ap/f;->ah:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/ap/f;->al:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->am:Ljava/lang/Object;

    sget-object v0, Lmaps/av/a;->s:Lmaps/av/a;

    iput-object v0, p0, Lmaps/ap/f;->g:Lmaps/av/a;

    sget-object v0, Lmaps/ap/b;->a:Lmaps/ap/b;

    iput-object v0, p0, Lmaps/ap/f;->O:Lmaps/ap/b;

    iput-object p1, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    new-instance v0, Lmaps/al/k;

    invoke-direct {v0, p0}, Lmaps/al/k;-><init>(Lmaps/ap/f;)V

    iput-object v0, p0, Lmaps/ap/f;->s:Lmaps/al/k;

    iput-object p2, p0, Lmaps/ap/f;->t:Landroid/content/res/Resources;

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lmaps/ap/f;->u:F

    iget v0, p0, Lmaps/ap/f;->u:F

    invoke-static {v0}, Lmaps/au/ae;->a(F)V

    iget v0, p0, Lmaps/ap/f;->u:F

    invoke-static {v0}, Lmaps/au/w;->a(F)V

    iput-object p3, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    iput-object p4, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    iget-object v0, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->p:Ljava/util/ArrayList;

    new-instance v0, Lmaps/ay/o;

    invoke-direct {v0, p2}, Lmaps/ay/o;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    new-instance v0, Lmaps/ay/ai;

    const/4 v1, 0x2

    iget-object v2, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-direct {v0, v1, v2}, Lmaps/ay/ai;-><init>(ILmaps/ay/o;)V

    iput-object v0, p0, Lmaps/ap/f;->w:Lmaps/ay/ai;

    new-instance v0, Lmaps/ay/ai;

    iget-object v1, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-direct {v0, v3, v1}, Lmaps/ay/ai;-><init>(ILmaps/ay/o;)V

    iput-object v0, p0, Lmaps/ap/f;->x:Lmaps/ay/ai;

    new-instance v0, Lmaps/ay/l;

    invoke-direct {v0}, Lmaps/ay/l;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->y:Lmaps/ay/l;

    new-instance v0, Lmaps/az/b;

    invoke-direct {v0}, Lmaps/az/b;-><init>()V

    iput-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/ap/f;->w:Lmaps/ay/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->w:Lmaps/ay/ai;

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    :cond_0
    iget-object v0, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    iget-object v0, p0, Lmaps/ap/f;->y:Lmaps/ay/l;

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    new-instance v0, Lmaps/ay/aa;

    sget-object v1, Lmaps/ay/v;->d:Lmaps/ay/v;

    invoke-direct {v0, v1}, Lmaps/ay/aa;-><init>(Lmaps/ay/v;)V

    sget-object v1, Lmaps/ap/b;->c:Lmaps/ap/b;

    invoke-virtual {v0, v1}, Lmaps/ay/aa;->a(Lmaps/ap/b;)V

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    new-instance v0, Lmaps/ay/z;

    invoke-direct {v0}, Lmaps/ay/z;-><init>()V

    invoke-direct {p0, v0}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    invoke-static {}, Lmaps/ap/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lmaps/az/c;

    invoke-direct {v0, p2}, Lmaps/az/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, v1}, Lmaps/az/b;->a(Lmaps/az/a;)V

    :goto_0
    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    iget-object v1, p0, Lmaps/ap/f;->I:Lmaps/ay/ar;

    invoke-virtual {v0, v1}, Lmaps/ay/ap;->a(Lmaps/ay/ar;)V

    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-virtual {v0, v3}, Lmaps/ay/ap;->b(Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ap/f;->N:J

    iput-object v4, p0, Lmaps/ap/f;->ak:Lmaps/an/m;

    iput-object v4, p0, Lmaps/ap/f;->aj:Lmaps/an/w;

    return-void

    :cond_1
    iput-object v4, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ap/f;)Lmaps/az/c;
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    return-object v0
.end method

.method private a(Lmaps/ap/k;Z)V
    .locals 9

    const/4 v3, 0x1

    const/4 v1, 0x0

    iget-object v5, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v5

    :try_start_0
    iget-boolean v0, p0, Lmaps/ap/f;->ah:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    monitor-exit v5

    :goto_0
    return-void

    :cond_0
    iget-boolean v2, p0, Lmaps/ap/f;->ah:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ap/f;->ah:Z

    invoke-virtual {p1}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_1
    if-ge v4, v7, :cond_2

    aget-object v8, v6, v4

    iget-object v0, p0, Lmaps/ap/f;->af:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lmaps/ap/f;->af:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    :cond_1
    invoke-virtual {v8, v0}, Lmaps/ay/u;->a(Ljava/util/List;)Z

    move-result v0

    or-int/2addr v2, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_8

    iget-object v0, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p1}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v0, v2, v1

    iget-object v4, p0, Lmaps/ap/f;->af:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v4, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/ad;

    invoke-virtual {v0}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/aj/ad;->a(Z)V

    invoke-virtual {v0}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_5
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Lmaps/aj/ad;->a(Z)V

    goto :goto_3

    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/aj/ad;

    invoke-virtual {v0}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/aj/ad;->b(Z)V

    invoke-virtual {v0}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lmaps/aj/ad;->b(Z)V

    goto :goto_4

    :cond_8
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private a(Lmaps/ar/a;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v0

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v1

    if-lez v0, :cond_0

    if-lez v1, :cond_0

    iget-object v2, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    invoke-virtual {p1}, Lmaps/ar/a;->x()[F

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/16 v3, 0xc11

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    :cond_0
    return-void
.end method

.method private a(Lmaps/ar/a;IZ)V
    .locals 15

    invoke-direct {p0}, Lmaps/ap/f;->n()Lmaps/ap/k;

    move-result-object v10

    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->D()V

    iget-object v11, p0, Lmaps/ap/f;->O:Lmaps/ap/b;

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->l()F

    move-result v1

    iget v2, p0, Lmaps/ap/f;->M:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p1}, Lmaps/ap/f;->a(Lmaps/ar/a;)V

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->l()F

    move-result v1

    iput v1, p0, Lmaps/ap/f;->M:F

    :cond_0
    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    const/16 v2, 0x1700

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->w()[F

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v10}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_1

    invoke-virtual {v10}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    aget-object v2, v2, v1

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lmaps/ay/u;->a(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_7

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v10}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    array-length v2, v2

    if-ge v1, v2, :cond_2

    invoke-virtual {v10}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    aget-object v2, v2, v1

    iget-object v3, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v3}, Lmaps/ay/u;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lmaps/ap/f;->F:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    sget-boolean v1, Lmaps/ap/f;->a:Z

    if-eqz v1, :cond_6

    invoke-virtual {v10}, Lmaps/ap/k;->b()[Lmaps/ay/ap;

    move-result-object v4

    array-length v5, v4

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_6

    aget-object v1, v4, v3

    invoke-virtual {v1}, Lmaps/ay/ap;->n()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/au/ap;

    invoke-interface {v1}, Lmaps/au/ap;->l()Lmaps/au/m;

    move-result-object v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    move-object/from16 v0, p1

    invoke-virtual {v2, v1, v0}, Lmaps/am/d;->a(Lmaps/au/ap;Lmaps/ar/a;)Lmaps/au/m;

    move-result-object v2

    invoke-interface {v1, v2}, Lmaps/au/ap;->a(Lmaps/au/m;)V

    :cond_4
    move-object v1, v2

    if-eqz v1, :cond_3

    iget-object v2, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lmaps/au/m;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    iget-object v2, p0, Lmaps/ap/f;->F:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lmaps/ap/f;->y:Lmaps/ay/l;

    iget-object v2, p0, Lmaps/ap/f;->F:Ljava/util/List;

    invoke-virtual {v1, v2}, Lmaps/ay/l;->b(Ljava/util/List;)V

    :cond_7
    sget-object v1, Lmaps/ap/b;->f:Lmaps/ap/b;

    if-eq v11, v1, :cond_9

    sget-object v1, Lmaps/ap/b;->e:Lmaps/ap/b;

    if-eq v11, v1, :cond_9

    invoke-virtual {v10}, Lmaps/ap/k;->b()[Lmaps/ay/ap;

    move-result-object v9

    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    :cond_8
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_4
    iget-object v2, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lmaps/am/d;->a(I)V

    if-eqz v1, :cond_b

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->z()Lmaps/ac/cw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/am/d;->a(Lmaps/ac/cy;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/ap/f;->L:Z

    :goto_5
    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    :cond_9
    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    if-eqz v1, :cond_12

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lmaps/ap/f;->H:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/ap/f;->H:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_12

    iget-object v1, p0, Lmaps/ap/f;->C:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    iget-object v1, p0, Lmaps/ap/f;->D:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    iget-object v1, p0, Lmaps/ap/f;->E:[I

    const/4 v2, 0x0

    const/4 v3, -0x1

    aput v3, v1, v2

    iget-object v1, p0, Lmaps/ap/f;->E:[I

    const/4 v2, 0x0

    aget v7, v1, v2

    invoke-virtual {v10}, Lmaps/ap/k;->b()[Lmaps/ay/ap;

    move-result-object v9

    array-length v12, v9

    const/4 v1, 0x0

    move v8, v1

    :goto_6
    if-ge v8, v12, :cond_11

    aget-object v1, v9, v8

    iget-object v4, p0, Lmaps/ap/f;->C:Ljava/util/HashSet;

    iget-object v5, p0, Lmaps/ap/f;->D:Ljava/util/HashSet;

    iget-object v6, p0, Lmaps/ap/f;->E:[I

    move-object/from16 v2, p1

    move-object v3, v11

    invoke-virtual/range {v1 .. v6}, Lmaps/ay/ap;->a(Lmaps/ar/a;Lmaps/ap/b;Ljava/util/HashSet;Ljava/util/HashSet;[I)V

    iget-object v1, p0, Lmaps/ap/f;->E:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-le v1, v7, :cond_1b

    iget-object v1, p0, Lmaps/ap/f;->E:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    :goto_7
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v7, v1

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    if-nez p3, :cond_c

    iget-boolean v1, p0, Lmaps/ap/f;->L:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v1}, Lmaps/am/d;->c()V

    goto :goto_5

    :cond_c
    new-instance v5, Lmaps/aj/ac;

    invoke-direct {v5}, Lmaps/aj/ac;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->z()Lmaps/ac/cw;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->o()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_e

    invoke-virtual/range {p1 .. p1}, Lmaps/ar/a;->n()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_e

    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_f

    const/4 v1, 0x0

    :goto_9
    const/4 v4, 0x0

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v8

    array-length v12, v9

    const/4 v2, 0x0

    :goto_a
    if-ge v2, v12, :cond_10

    aget-object v13, v9, v2

    invoke-virtual {v13}, Lmaps/ay/ap;->j()Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-virtual {v13, v1, v5, v6}, Lmaps/ay/ap;->a(Lmaps/ac/cw;Lmaps/aj/ac;Ljava/util/Set;)I

    move-result v14

    invoke-static {v4, v14}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-virtual {v13, v7, v8}, Lmaps/ay/ap;->a(Ljava/util/Set;Ljava/util/Map;)V

    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_e
    const/4 v1, 0x0

    goto :goto_8

    :cond_f
    move-object v1, v3

    goto :goto_9

    :cond_10
    monitor-enter p0

    monitor-exit p0

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    iget-object v2, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-virtual {v2}, Lmaps/ay/ap;->l()Lmaps/ao/b;

    move-result-object v9

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lmaps/am/d;->a(Lmaps/ar/a;Lmaps/ac/cy;ILmaps/aj/ac;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;Lmaps/ao/b;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ap/f;->L:Z

    goto/16 :goto_5

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_11
    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    iget-object v2, p0, Lmaps/ap/f;->C:Ljava/util/HashSet;

    iget-object v3, p0, Lmaps/ap/f;->D:Ljava/util/HashSet;

    iget-object v4, p0, Lmaps/ap/f;->O:Lmaps/ap/b;

    invoke-virtual {v1, v2, v3, v7}, Lmaps/az/c;->a(Ljava/util/HashSet;Ljava/util/HashSet;I)V

    :cond_12
    if-nez p3, :cond_13

    if-eqz p2, :cond_16

    :cond_13
    const/4 v1, 0x1

    :goto_b
    invoke-direct {p0, v10, v1}, Lmaps/ap/f;->a(Lmaps/ap/k;Z)V

    new-instance v2, Lmaps/ap/c;

    const/4 v1, 0x0

    invoke-direct {v2, v11, v1}, Lmaps/ap/c;-><init>(Lmaps/ap/b;I)V

    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->o()V

    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->t()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmaps/ap/f;->ai:Z

    iget-object v1, p0, Lmaps/ap/f;->ag:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_14
    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/aj/ad;

    invoke-virtual {v2, v1}, Lmaps/ap/c;->a(Lmaps/aj/ad;)V

    invoke-virtual {v1}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v1

    iget-object v4, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v4, :cond_15

    iget-object v4, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    :cond_15
    iget-object v4, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->B()V

    iget-object v4, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    move-object/from16 v0, p1

    invoke-virtual {v1, v4, v0, v2}, Lmaps/ay/u;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    iget-object v4, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->C()V

    invoke-virtual {v1}, Lmaps/ay/u;->ar_()Z

    move-result v1

    iget-boolean v4, p0, Lmaps/ap/f;->ai:Z

    and-int/2addr v1, v4

    iput-boolean v1, p0, Lmaps/ap/f;->ai:Z

    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lmaps/ap/f;->R:Lmaps/ap/e;

    goto :goto_c

    :cond_16
    const/4 v1, 0x0

    goto :goto_b

    :cond_17
    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->D()V

    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v1

    if-eqz v1, :cond_19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x505

    if-ne v1, v3, :cond_18

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lmaps/ap/f;->U:J

    sub-long/2addr v3, v5

    const-string v5, "\nTime in current GL context: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/aq/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lmaps/ap/f;->T:Z

    :cond_18
    const-string v3, "Renderer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "GL Error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lmaps/aw/a;->c(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Renderer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "drawFrameInternal GL ERROR: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_19
    iget-boolean v2, p0, Lmaps/ap/f;->T:Z

    const/16 v2, 0x505

    if-ne v1, v2, :cond_1a

    invoke-virtual {v10}, Lmaps/ap/k;->a()[Lmaps/ay/u;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_d
    if-ge v1, v3, :cond_1a

    aget-object v4, v2, v1

    invoke-virtual {v4}, Lmaps/ay/u;->i()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_1a
    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ap/f;->T:Z

    return-void

    :cond_1b
    move v1, v7

    goto/16 :goto_7
.end method

.method public static a(Lmaps/ap/b;)[I
    .locals 2

    sget-object v0, Lmaps/ap/j;->a:[I

    invoke-virtual {p0}, Lmaps/ap/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lmaps/ap/f;->f:[I

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lmaps/ap/f;->c:[I

    goto :goto_0

    :pswitch_1
    sget-object v0, Lmaps/ap/f;->c:[I

    goto :goto_0

    :pswitch_2
    sget-object v0, Lmaps/ap/f;->c:[I

    goto :goto_0

    :pswitch_3
    sget-object v0, Lmaps/ap/f;->d:[I

    goto :goto_0

    :pswitch_4
    sget-object v0, Lmaps/ap/f;->e:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic b(Lmaps/ap/f;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ap/f;->H:Z

    return v0
.end method

.method private c(Lmaps/ay/u;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ap/f;->ah:Z

    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private j()V
    .locals 4

    iget v0, p0, Lmaps/ap/f;->k:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lmaps/ap/f;->X:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    :goto_1
    :try_start_0
    iget v1, p0, Lmaps/ap/f;->k:I

    invoke-static {v1, v0}, Landroid/os/Process;->setThreadPriority(II)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not set thread priority: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/ap/f;->l:I

    goto :goto_1
.end method

.method private k()I
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lmaps/ap/f;->P:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v4

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/a;

    iget-object v6, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-interface {v0, v6}, Lmaps/ap/a;->a(Lmaps/ar/a;)I

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Lmaps/ap/a;->d()Lmaps/ar/b;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {v0}, Lmaps/ap/a;->d()Lmaps/ar/b;

    move-result-object v0

    :goto_1
    or-int v1, v3, v6

    move v3, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    if-eqz v3, :cond_3

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/ap/f;->h:Lmaps/ar/b;

    invoke-virtual {v1, v0}, Lmaps/ar/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-virtual {v0, v1}, Lmaps/ay/ap;->a(Lmaps/ar/b;)V

    iput-object v1, p0, Lmaps/ap/f;->h:Lmaps/ar/b;

    :cond_1
    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v0, v4, v4}, Lmaps/aj/a;->a(ZZ)V

    :goto_2
    iget-object v0, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    if-eqz v3, :cond_2

    const/4 v4, 0x1

    :cond_2
    invoke-virtual {v0, v4}, Lmaps/ar/a;->a(Z)V

    return v3

    :cond_3
    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-virtual {v0, v2}, Lmaps/ay/ap;->a(Lmaps/ar/b;)V

    iput-object v2, p0, Lmaps/ap/f;->h:Lmaps/ar/b;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private declared-synchronized l()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ap/f;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized m()Landroid/graphics/Bitmap;
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/ap/f;->J:Z

    :goto_0
    iget-boolean v0, p0, Lmaps/ap/f;->J:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lmaps/ap/f;->K:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ap/f;->K:Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private n()Lmaps/ap/k;
    .locals 9

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    iget-object v7, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v7

    :try_start_0
    iget-boolean v0, p0, Lmaps/ap/f;->ac:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    iget-object v4, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    iget-object v6, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v0, v4, v6}, Lmaps/ay/u;->a(Lmaps/as/a;Lmaps/ap/n;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lmaps/ap/f;->ac:Z

    :cond_1
    move v6, v1

    move v2, v1

    move v4, v1

    :goto_1
    iget-object v0, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ap/f;->ah:Z

    iget-object v0, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ap/l;

    sget-object v1, Lmaps/ap/j;->b:[I

    iget-object v8, v0, Lmaps/ap/l;->b:Lmaps/ap/m;

    invoke-virtual {v8}, Lmaps/ap/m;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_2
    move v0, v2

    move v1, v4

    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    move v2, v0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    iget-object v8, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lmaps/ay/u;->c(Lmaps/as/a;)V

    iget-object v1, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    instance-of v1, v1, Lmaps/ay/ap;

    if-eqz v1, :cond_e

    iget-object v1, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    check-cast v1, Lmaps/ay/ap;

    iget-object v4, p0, Lmaps/ap/f;->I:Lmaps/ay/ar;

    invoke-virtual {v1, v4}, Lmaps/ay/ap;->a(Lmaps/ay/ar;)V

    iget-object v4, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lmaps/ay/ap;->f()Lmaps/ay/v;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ay/v;->a()I

    move-result v4

    iget v8, p0, Lmaps/ap/f;->ad:I

    if-ge v4, v8, :cond_3

    move v4, v3

    :goto_4
    invoke-virtual {v1}, Lmaps/ay/ap;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmaps/ap/f;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/ao/b;

    invoke-virtual {v1, v2}, Lmaps/ay/ap;->a(Lmaps/ao/b;)V

    goto :goto_5

    :cond_3
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lmaps/ay/ap;->b(Z)V

    move v4, v2

    goto :goto_4

    :cond_4
    move v1, v4

    :goto_6
    iget-object v2, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-direct {p0, v2}, Lmaps/ap/f;->c(Lmaps/ay/u;)V

    iget-object v2, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v2}, Lmaps/ay/u;->am_()Lmaps/ap/a;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {p0, v2}, Lmaps/ap/f;->a(Lmaps/ap/a;)V

    :cond_5
    iget-object v0, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    iget-object v2, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    iget-object v4, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v0, v2, v4}, Lmaps/ay/u;->a(Lmaps/as/a;Lmaps/ap/n;)V

    move v0, v1

    move v1, v3

    goto :goto_3

    :pswitch_1
    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    iget-object v8, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    instance-of v1, v1, Lmaps/ay/ap;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v1}, Lmaps/ay/u;->f()Lmaps/ay/v;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/v;->a()I

    move-result v1

    iget v8, p0, Lmaps/ap/f;->ad:I

    if-ne v1, v8, :cond_c

    move v1, v3

    :goto_7
    iget-object v2, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    iget-object v8, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :goto_8
    iget-object v2, p0, Lmaps/ap/f;->af:Ljava/util/Map;

    iget-object v8, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-interface {v2, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    iget-object v8, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2, v8}, Lmaps/ay/u;->c(Lmaps/as/a;)V

    iget-object v0, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    invoke-virtual {v0}, Lmaps/ay/u;->am_()Lmaps/ap/a;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v2, p0, Lmaps/ap/f;->P:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_6
    move v0, v1

    move v1, v4

    goto/16 :goto_3

    :pswitch_2
    iget-object v0, v0, Lmaps/ap/l;->a:Lmaps/ay/u;

    check-cast v0, Lmaps/ay/ap;

    iput-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_9

    const v0, 0x7fffffff

    iput v0, p0, Lmaps/ap/f;->ad:I

    iget-object v0, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v5

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/ap;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lmaps/ay/ap;->b(Z)V

    invoke-virtual {v0}, Lmaps/ay/ap;->f()Lmaps/ay/v;

    move-result-object v5

    invoke-virtual {v5}, Lmaps/ay/v;->a()I

    move-result v5

    iget v6, p0, Lmaps/ap/f;->ad:I

    if-ge v5, v6, :cond_b

    invoke-virtual {v0}, Lmaps/ay/ap;->f()Lmaps/ay/v;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ay/v;->a()I

    move-result v1

    iput v1, p0, Lmaps/ap/f;->ad:I

    :goto_a
    move-object v1, v0

    goto :goto_9

    :cond_8
    if-eqz v1, :cond_9

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/ay/ap;->b(Z)V

    :cond_9
    if-eqz v4, :cond_a

    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_a

    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    sget-object v1, Lmaps/ap/f;->i:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_a
    new-instance v0, Lmaps/ap/k;

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    iget-object v2, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lmaps/ap/k;-><init>(Ljava/util/List;Ljava/util/List;)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0

    :cond_b
    move-object v0, v1

    goto :goto_a

    :cond_c
    move v1, v2

    goto/16 :goto_7

    :cond_d
    move v1, v2

    goto/16 :goto_8

    :cond_e
    move v1, v2

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private o()V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lmaps/ap/f;->V:I

    const/4 v2, 0x0

    iput v2, p0, Lmaps/ap/f;->V:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0, v1}, Lmaps/am/d;->a(Z)V

    iget-object v2, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    invoke-virtual {v0, v1}, Lmaps/ay/u;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/ap/f;->Y:F

    return v0
.end method

.method public final a(Lmaps/ac/av;)F
    .locals 4

    const/high16 v0, 0x41a80000    # 21.0f

    iget-object v2, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lmaps/ap/f;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/ap;

    invoke-virtual {v0, p1}, Lmaps/ay/ap;->a(Lmaps/ac/av;)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lmaps/ap/f;->K:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lmaps/ap/f;->m()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ay/v;)Lmaps/ay/r;
    .locals 2

    new-instance v0, Lmaps/ay/r;

    iget-object v1, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-direct {v0, p1, v1}, Lmaps/ay/r;-><init>(Lmaps/ay/v;Lmaps/ay/o;)V

    return-object v0
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, p1, p2}, Lmaps/az/c;->a(II)V

    :cond_0
    return-void
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-static {}, Lmaps/bl/a;->a()V

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lmaps/ap/f;->k:I

    invoke-direct {p0}, Lmaps/ap/f;->j()V

    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/bb/b;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/u;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lmaps/ay/u;->c(Lmaps/as/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lmaps/ap/f;->ac:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_3

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0}, Lmaps/am/d;->a()V

    :cond_1
    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->H()Lmaps/al/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/al/a;->a()V

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/al/o;->a()V

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->D()V

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->E()V

    :cond_2
    iput-object v4, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    :cond_3
    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    if-nez v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ap/f;->U:J

    new-instance v0, Lmaps/as/a;

    iget-object v2, p0, Lmaps/ap/f;->s:Lmaps/al/k;

    iget-object v3, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    iget-object v4, p0, Lmaps/ap/f;->ak:Lmaps/an/m;

    iget-object v5, p0, Lmaps/ap/f;->t:Landroid/content/res/Resources;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lmaps/as/a;-><init>(Ljavax/microedition/khronos/opengles/GL10;Lmaps/al/k;Lmaps/aj/a;Lmaps/an/m;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->A()Lmaps/aj/a;

    move-result-object v0

    iget-boolean v1, p0, Lmaps/ap/f;->X:Z

    invoke-virtual {v0, v1}, Lmaps/aj/a;->b(Z)V

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->G()I

    move-result v0

    invoke-static {v0}, Lmaps/au/ae;->a(I)V

    new-instance v0, Lmaps/am/d;

    iget-object v1, p0, Lmaps/ap/f;->g:Lmaps/av/a;

    iget-object v2, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    iget-object v3, p0, Lmaps/ap/f;->t:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Lmaps/am/d;-><init>(Lmaps/av/a;Lmaps/as/a;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    iget-object v0, p0, Lmaps/ap/f;->w:Lmaps/ay/ai;

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0, v1}, Lmaps/ay/ai;->a(Lmaps/am/d;)V

    iget-object v0, p0, Lmaps/ap/f;->x:Lmaps/ay/ai;

    iget-object v1, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0, v1}, Lmaps/ay/ai;->a(Lmaps/am/d;)V

    :cond_4
    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v0}, Lmaps/aj/a;->h()V

    iput-boolean v6, p0, Lmaps/ap/f;->L:Z

    iget-boolean v0, p0, Lmaps/ap/f;->aa:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    iput v0, p0, Lmaps/ap/f;->ab:I

    :cond_5
    iput-boolean v6, p0, Lmaps/ap/f;->aa:Z

    invoke-static {}, Lmaps/bl/a;->b()V

    return-void
.end method

.method public final a(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 5

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lmaps/al/e;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    iget v1, p0, Lmaps/ap/f;->u:F

    invoke-virtual {v0, p2, p3, v1}, Lmaps/ar/a;->a(IIF)V

    iget-object v0, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-direct {p0, v0}, Lmaps/ap/f;->a(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v0}, Lmaps/ar/a;->l()F

    move-result v0

    iput v0, p0, Lmaps/ap/f;->M:F

    iget v0, p0, Lmaps/ap/f;->u:F

    int-to-double v1, p2

    int-to-double v3, p3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-float v1, v1

    const/high16 v2, 0x43800000    # 256.0f

    mul-float/2addr v0, v2

    div-float v0, v1, v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0}, Lmaps/ar/a;->a(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/ap/f;->Y:F

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/aj/a;->a(ZZ)V

    goto :goto_0
.end method

.method public final a(Lmaps/ap/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->P:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lmaps/ap/e;)V
    .locals 0

    iput-object p1, p0, Lmaps/ap/f;->S:Lmaps/ap/e;

    return-void
.end method

.method public final a(Lmaps/av/a;)V
    .locals 3

    iget-object v0, p0, Lmaps/ap/f;->g:Lmaps/av/a;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/ap/f;->g:Lmaps/av/a;

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0, p1}, Lmaps/am/d;->a(Lmaps/av/a;)V

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/aj/a;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public final a(Lmaps/ay/ap;)V
    .locals 4

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/ap/l;

    sget-object v2, Lmaps/ap/m;->a:Lmaps/ap/m;

    invoke-direct {v0, v2, p1}, Lmaps/ap/l;-><init>(Lmaps/ap/m;Lmaps/ay/u;)V

    iget-object v2, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ap/l;

    sget-object v2, Lmaps/ap/m;->c:Lmaps/ap/m;

    invoke-direct {v0, v2, p1}, Lmaps/ap/l;-><init>(Lmaps/ap/m;Lmaps/ay/u;)V

    iget-object v2, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/ap/l;

    sget-object v2, Lmaps/ap/m;->b:Lmaps/ap/m;

    iget-object v3, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    invoke-direct {v0, v2, v3}, Lmaps/ap/l;-><init>(Lmaps/ap/m;Lmaps/ay/u;)V

    iget-object v2, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0}, Lmaps/am/d;->b()V

    :cond_0
    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/aj/a;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lmaps/ay/b;Lmaps/ay/e;)V
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-virtual {v0, p1, p2}, Lmaps/ay/o;->a(Lmaps/ay/b;Lmaps/ay/e;)V

    return-void
.end method

.method public final a(Lmaps/ay/u;)V
    .locals 3

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/ap/l;

    sget-object v2, Lmaps/ap/m;->a:Lmaps/ap/m;

    invoke-direct {v0, v2, p1}, Lmaps/ap/l;-><init>(Lmaps/ap/m;Lmaps/ay/u;)V

    iget-object v2, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/aj/a;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/ap/f;->X:Z

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->A()Lmaps/aj/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lmaps/aj/a;->b(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/as/a;->A()Lmaps/aj/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/a;->d()V

    :cond_0
    invoke-direct {p0}, Lmaps/ap/f;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v0, v1}, Lmaps/aq/a;->a(Lmaps/as/a;)V

    :cond_0
    return-void
.end method

.method public final b(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 18

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    move-object/from16 v0, p1

    if-eq v2, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    instance-of v2, v2, Lmaps/al/e;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "GL object has changed since onSurfaceCreated"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v3, "DA:Renderer"

    invoke-static {v3, v2}, Lmaps/br/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v3, "OpenGL error during initialization."

    invoke-static {v3, v2}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->e()Z

    move-result v3

    invoke-virtual {v2, v3}, Lmaps/as/a;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->O:Lmaps/ap/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v3}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-static {v2}, Lmaps/ap/f;->a(Lmaps/ap/b;)[I

    move-result-object v2

    const/4 v4, 0x0

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    const/4 v6, 0x2

    aget v6, v2, v6

    const/4 v7, 0x3

    aget v2, v2, v7

    invoke-interface {v3, v4, v5, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClearColorx(IIII)V

    const/16 v2, 0x4000

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    const/16 v2, 0x4100

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->j()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->k()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glClearStencil(I)V

    or-int/lit16 v2, v2, 0x400

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v4}, Lmaps/as/a;->l()V

    :cond_2
    invoke-interface {v3, v2}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/ap/f;->ab:I

    if-lez v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/ap/f;->ab:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lmaps/ap/f;->ab:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lmaps/aj/a;->a(ZZ)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-wide v2, v0, Lmaps/ap/f;->al:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->am:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lmaps/ap/f;->al:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-gez v2, :cond_5

    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lmaps/ap/f;->al:J

    :cond_5
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v2}, Lmaps/aj/a;->e()V

    :cond_6
    invoke-static {}, Lmaps/ax/m;->d()Z

    move-result v2

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lmaps/aj/a;->a(ZZ)V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_7
    invoke-direct/range {p0 .. p0}, Lmaps/ap/f;->k()I

    move-result v17

    and-int/lit8 v2, v17, 0x2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_8
    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v3}, Lmaps/as/a;->A()Lmaps/aj/a;

    move-result-object v3

    invoke-virtual {v3, v2}, Lmaps/aj/a;->a(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v2}, Lmaps/aj/a;->i()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v3}, Lmaps/ar/a;->e()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-wide v5, v0, Lmaps/ap/f;->G:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_10

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v2}, Lmaps/ar/a;->e()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lmaps/ap/f;->G:J

    const/4 v2, 0x1

    move v15, v2

    :goto_3
    invoke-direct/range {p0 .. p0}, Lmaps/ap/f;->l()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->f()V

    invoke-direct/range {p0 .. p0}, Lmaps/ap/f;->o()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->S:Lmaps/ap/e;

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lmaps/ap/e;->a(Lmaps/ap/f;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    invoke-virtual {v2}, Lmaps/ap/e;->a()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v2}, Lmaps/ar/a;->m()F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v2, v1, v15}, Lmaps/ap/f;->a(Lmaps/ar/a;IZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->g()V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v2}, Lmaps/aj/a;->j()Z

    move-result v2

    if-eqz v2, :cond_11

    and-int/lit8 v2, v17, 0x2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ap/f;->ai:Z

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v3}, Lmaps/am/d;->d()Z

    move-result v3

    if-nez v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v3}, Lmaps/as/a;->d()Z

    move-result v3

    if-nez v3, :cond_12

    if-nez v2, :cond_12

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    invoke-virtual {v3, v2}, Lmaps/ap/e;->b(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->R:Lmaps/ap/e;

    invoke-virtual {v2}, Lmaps/ap/e;->b()Z

    :cond_b
    monitor-enter p0

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lmaps/ap/f;->J:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v3}, Lmaps/ar/a;->i()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->q:Lmaps/ar/a;

    invoke-virtual {v3}, Lmaps/ar/a;->j()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->K:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_c

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-ne v4, v5, :cond_c

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-eq v4, v6, :cond_15

    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ap/f;->s:Lmaps/al/k;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v3, v5, v6, v4}, Lmaps/al/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v16, v3

    :goto_6
    invoke-static/range {v16 .. v16}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v7

    invoke-static/range {v16 .. v16}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v8

    mul-int v3, v5, v6

    invoke-static {v3}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v9

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v2 .. v9}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    invoke-virtual {v9}, Ljava/nio/IntBuffer;->array()[I

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, v16

    move v10, v5

    move v13, v5

    move v14, v6

    invoke-virtual/range {v7 .. v14}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    monitor-enter p0

    :try_start_2
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lmaps/ap/f;->K:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lmaps/ap/f;->J:Z

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v2}, Lmaps/am/d;->d()Z

    move-result v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->d()Z

    move-result v2

    if-eqz v2, :cond_13

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lmaps/aj/a;->a(ZZ)V

    goto/16 :goto_0

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_5

    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    :catchall_2
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_13
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_14

    if-nez v15, :cond_14

    if-nez v17, :cond_14

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lmaps/ap/f;->N:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v4, v4, v6

    if-lez v4, :cond_14

    invoke-static {}, Ljava/lang/System;->gc()V

    move-object/from16 v0, p0

    iput-wide v2, v0, Lmaps/ap/f;->N:J

    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->j:Lmaps/as/a;

    invoke-virtual {v2}, Lmaps/as/a;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lmaps/aj/a;->a(ZZ)V

    goto/16 :goto_0

    :cond_15
    move-object/from16 v16, v3

    goto/16 :goto_6

    :cond_16
    move v15, v2

    goto/16 :goto_3
.end method

.method public final b(Lmaps/ap/b;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/ap/f;->O:Lmaps/ap/b;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lmaps/ap/f;->O:Lmaps/ap/b;

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/ap/f;->H:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->v:Lmaps/am/d;

    invoke-virtual {v0}, Lmaps/am/d;->b()V

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lmaps/aj/a;->a(ZZ)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lmaps/ay/u;)V
    .locals 3

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lmaps/ap/l;

    sget-object v2, Lmaps/ap/m;->b:Lmaps/ap/m;

    invoke-direct {v0, v2, p1}, Lmaps/ap/l;-><init>(Lmaps/ap/m;Lmaps/ay/u;)V

    iget-object v2, p0, Lmaps/ap/f;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lmaps/aj/a;->a(ZZ)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Z)V
    .locals 2

    const/4 v1, 0x0

    monitor-enter p0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    :try_start_0
    iput v0, p0, Lmaps/ap/f;->V:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ap/f;->r:Lmaps/aj/a;

    invoke-virtual {v0, v1, v1}, Lmaps/aj/a;->a(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Lmaps/ay/aj;
    .locals 4

    new-instance v0, Lmaps/ay/aj;

    iget-object v1, p0, Lmaps/ap/f;->t:Landroid/content/res/Resources;

    iget-object v2, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lmaps/ay/aj;-><init>(Landroid/content/res/Resources;Lmaps/ay/o;Z)V

    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, v1}, Lmaps/az/b;->c(Lmaps/az/a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, v1}, Lmaps/az/b;->a(Lmaps/az/a;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, v1}, Lmaps/az/b;->c(Lmaps/az/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    iget-object v1, p0, Lmaps/ap/f;->B:Lmaps/az/c;

    invoke-virtual {v0, v1}, Lmaps/az/b;->b(Lmaps/az/a;)V

    goto :goto_0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lmaps/ap/f;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    invoke-virtual {v0}, Lmaps/ay/o;->d()V

    return-void
.end method

.method public final f()Lmaps/ay/o;
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->z:Lmaps/ay/o;

    return-object v0
.end method

.method public final g()Lmaps/az/b;
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->A:Lmaps/az/b;

    return-object v0
.end method

.method public final h()Lmaps/ay/ap;
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->W:Lmaps/ay/ap;

    return-object v0
.end method

.method public final i()Lmaps/as/a;
    .locals 1

    iget-object v0, p0, Lmaps/ap/f;->j:Lmaps/as/a;

    return-object v0
.end method
