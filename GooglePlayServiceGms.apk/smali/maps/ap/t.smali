.class public final Lmaps/ap/t;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/bn/k;

.field private b:Lmaps/ac/av;

.field private c:I

.field private final d:Lmaps/ac/av;

.field private e:Lmaps/ap/u;

.field private f:Lmaps/ap/v;


# direct methods
.method public constructor <init>(Lmaps/bn/k;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0, v1, v1}, Lmaps/ac/av;-><init>(II)V

    iput-object v0, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ap/t;->c:I

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/ap/t;->d:Lmaps/ac/av;

    iput-object v2, p0, Lmaps/ap/t;->e:Lmaps/ap/u;

    iput-object v2, p0, Lmaps/ap/t;->f:Lmaps/ap/v;

    iput-object p1, p0, Lmaps/ap/t;->a:Lmaps/bn/k;

    return-void
.end method

.method private a(Lmaps/ac/av;Lmaps/ac/av;ILmaps/ac/cx;)V
    .locals 9

    iput-object p1, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    iput p3, p0, Lmaps/ap/t;->c:I

    invoke-virtual {p1}, Lmaps/ac/av;->a()I

    move-result v1

    invoke-virtual {p1}, Lmaps/ac/av;->c()I

    move-result v2

    invoke-virtual {p4}, Lmaps/ac/cx;->e()I

    move-result v0

    int-to-double v3, v0

    int-to-double v5, v1

    const-wide v7, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v5, v7

    const-wide v7, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    const-wide v5, 0x3fd5752a00000000L    # 0.33527612686157227

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-virtual {p4}, Lmaps/ac/cx;->d()I

    move-result v0

    int-to-double v4, v0

    const-wide v6, 0x3fd5752a00000000L    # 0.33527612686157227

    mul-double/2addr v4, v6

    double-to-int v4, v4

    iget-object v0, p0, Lmaps/ap/t;->f:Lmaps/ap/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ap/t;->f:Lmaps/ap/v;

    invoke-interface {v0}, Lmaps/ap/v;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {v6, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v6, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p2}, Lmaps/ac/av;->a()I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p2}, Lmaps/ac/av;->c()I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v6, p3}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v6, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v6, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget-object v0, p0, Lmaps/ap/t;->a:Lmaps/bn/k;

    const/4 v1, 0x7

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface/range {v0 .. v5}, Lmaps/bn/k;->a(I[BZZZ)V

    iget-object v0, p0, Lmaps/ap/t;->e:Lmaps/ap/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ap/t;->e:Lmaps/ap/u;

    invoke-interface {v0, p3}, Lmaps/ap/u;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "view point"

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lmaps/ap/u;)V
    .locals 0

    iput-object p1, p0, Lmaps/ap/t;->e:Lmaps/ap/u;

    return-void
.end method

.method public final a(Lmaps/ap/v;)V
    .locals 0

    iput-object p1, p0, Lmaps/ap/t;->f:Lmaps/ap/v;

    return-void
.end method

.method public final a(Lmaps/ar/a;)V
    .locals 13

    const/4 v3, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v5

    const/16 v0, 0x1e

    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    iget v0, p0, Lmaps/ap/t;->c:I

    if-eq v6, v0, :cond_1

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v0

    invoke-direct {p0, v5, v5, v6, v0}, Lmaps/ap/t;->a(Lmaps/ac/av;Lmaps/ac/av;ILmaps/ac/cx;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    invoke-virtual {v0, v5}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v7, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v2

    invoke-virtual {v7}, Lmaps/ac/av;->f()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v8, v2

    invoke-virtual {v1}, Lmaps/ac/av;->g()I

    move-result v2

    invoke-virtual {v7}, Lmaps/ac/av;->g()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v9, v2

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v4

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v2

    div-int/lit8 v10, v4, 0x2

    div-int/lit8 v11, v2, 0x2

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v12

    if-gt v12, v4, :cond_2

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v12

    if-le v12, v2, :cond_5

    :cond_2
    move-object v0, v1

    :cond_3
    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    if-eqz v1, :cond_4

    invoke-virtual {v5, v0}, Lmaps/ac/av;->d(Lmaps/ac/av;)F

    move-result v1

    iget-object v2, p0, Lmaps/ap/t;->b:Lmaps/ac/av;

    invoke-virtual {v5, v2}, Lmaps/ac/av;->d(Lmaps/ac/av;)F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    :cond_4
    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v1

    invoke-direct {p0, v0, v5, v6, v1}, Lmaps/ap/t;->a(Lmaps/ac/av;Lmaps/ac/av;ILmaps/ac/cx;)V

    goto :goto_0

    :cond_5
    neg-int v1, v10

    if-ge v8, v1, :cond_8

    neg-int v1, v4

    move v4, v1

    :cond_6
    :goto_2
    neg-int v1, v11

    if-ge v9, v1, :cond_9

    neg-int v1, v2

    :goto_3
    if-nez v1, :cond_7

    if-eqz v4, :cond_3

    :cond_7
    int-to-float v0, v4

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v1, v1

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lmaps/ap/t;->d:Lmaps/ac/av;

    invoke-virtual {v2, v0, v1}, Lmaps/ac/av;->d(II)V

    iget-object v0, p0, Lmaps/ap/t;->d:Lmaps/ac/av;

    invoke-virtual {v7, v0}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    goto :goto_1

    :cond_8
    if-gt v8, v10, :cond_6

    move v4, v3

    goto :goto_2

    :cond_9
    if-le v9, v11, :cond_a

    move v1, v2

    goto :goto_3

    :cond_a
    move v1, v3

    goto :goto_3
.end method
