.class public final Lmaps/ap/p;
.super Ljava/lang/Object;


# static fields
.field private static a:[Lmaps/ao/b;

.field private static b:[Lmaps/ao/b;

.field private static c:Z

.field private static d:Z

.field private static volatile e:I

.field private static volatile f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lmaps/ao/b;

    sget-object v1, Lmaps/ao/b;->b:Lmaps/ao/b;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/ao/b;->d:Lmaps/ao/b;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/ao/b;->g:Lmaps/ao/b;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/ao/b;->h:Lmaps/ao/b;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/ao/b;->i:Lmaps/ao/b;

    aput-object v1, v0, v6

    sput-object v0, Lmaps/ap/p;->a:[Lmaps/ao/b;

    const/16 v0, 0xc

    new-array v0, v0, [Lmaps/ao/b;

    sget-object v1, Lmaps/ao/b;->a:Lmaps/ao/b;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/ao/b;->c:Lmaps/ao/b;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/ao/b;->d:Lmaps/ao/b;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/ao/b;->f:Lmaps/ao/b;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/ao/b;->e:Lmaps/ao/b;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lmaps/ao/b;->j:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/ao/b;->l:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/ao/b;->k:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/ao/b;->m:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lmaps/ao/b;->n:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lmaps/ao/b;->o:Lmaps/ao/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lmaps/ao/b;->p:Lmaps/ao/b;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/ap/p;->b:[Lmaps/ao/b;

    sput-boolean v3, Lmaps/ap/p;->c:Z

    const/16 v0, 0xa

    sput v0, Lmaps/ap/p;->e:I

    const/4 v0, -0x1

    sput v0, Lmaps/ap/p;->f:I

    return-void
.end method

.method public static declared-synchronized a(Lmaps/ao/b;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/ae/y;
    .locals 2

    const-class v1, Lmaps/ap/p;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v0

    invoke-static {p0, v0, p1, p2}, Lmaps/ap/p;->a(Lmaps/ao/b;Lmaps/bn/k;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/ae/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Lmaps/ao/b;Lmaps/bn/k;Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/ae/y;
    .locals 4

    const-class v1, Lmaps/ap/p;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmaps/ap/p;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "VectorGlobalState.initialize() must be called first"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Lmaps/ao/b;

    const/4 v2, 0x0

    aput-object p0, v0, v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p2}, Lmaps/aw/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    invoke-static {v0, p1, v2, v3, p3}, Lmaps/ap/p;->a([Lmaps/ao/b;Lmaps/bn/k;Ljava/util/Locale;Ljava/io/File;Landroid/content/res/Resources;)V

    invoke-static {p0}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/bn/d;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/bn/e;

    invoke-direct {v0}, Lmaps/bn/e;-><init>()V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->a(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v0

    invoke-static {}, Lmaps/bf/a;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->b(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v0

    invoke-virtual {v0, p3}, Lmaps/bn/e;->c(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v0

    invoke-static {}, Lmaps/bf/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->d(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bn/e;->a()Lmaps/bn/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bn/e;->b()Lmaps/bn/e;

    move-result-object v0

    invoke-static {p0}, Lmaps/aw/a;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->e(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v0

    invoke-static {p1}, Lmaps/bd/a;->a(Landroid/content/res/Resources;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->a(Z)Lmaps/bn/e;

    move-result-object v0

    invoke-static {p0}, Lmaps/bd/a;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lmaps/bn/e;->c(Z)Lmaps/bn/e;

    move-result-object v3

    invoke-static {}, Lmaps/bd/a;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lmaps/bn/e;->b(Z)Lmaps/bn/e;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v0, v3}, Lmaps/bn/e;->a(I)Lmaps/bn/e;

    move-result-object v3

    const-string v0, "DriveAbout"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v0, "GMM"

    :goto_1
    invoke-virtual {v3, v0}, Lmaps/bn/e;->f(Ljava/lang/String;)Lmaps/bn/e;

    invoke-virtual {v3, v1}, Lmaps/bn/e;->b(I)Lmaps/bn/e;

    invoke-virtual {v3, v1}, Lmaps/bn/e;->d(Z)Lmaps/bn/e;

    invoke-virtual {v3}, Lmaps/bn/e;->c()Lmaps/bn/d;

    move-result-object v0

    const-string v1, "GMM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v4, :cond_1

    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v3, v1}, Lmaps/bn/e;->b(I)Lmaps/bn/e;

    move-result-object v1

    invoke-virtual {v1, v2}, Lmaps/bn/e;->d(Z)Lmaps/bn/e;

    move-result-object v1

    const-string v3, "DriveAbout"

    invoke-virtual {v1, v3}, Lmaps/bn/e;->f(Ljava/lang/String;)Lmaps/bn/e;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bn/e;->d()Lmaps/bn/k;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bn/d;->a(Lmaps/bn/k;)V

    :cond_1
    new-instance v1, Lmaps/ap/q;

    invoke-direct {v1, v2}, Lmaps/ap/q;-><init>(B)V

    invoke-virtual {v0, v1}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move-object v0, p2

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/content/res/Resources;[Lmaps/ao/b;Ljava/lang/String;ILmaps/br/k;)V
    .locals 9

    const/4 v1, 0x1

    const-wide/high16 v7, 0x3fd0000000000000L    # 0.25

    const-class v3, Lmaps/ap/p;

    monitor-enter v3

    :try_start_0
    sget-boolean v0, Lmaps/ap/p;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit v3

    return-void

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    invoke-static {}, Lmaps/bl/a;->a()V

    invoke-static {p0}, Lmaps/bf/a;->a(Landroid/content/Context;)Lmaps/bf/a;

    invoke-static {}, Lmaps/aw/a;->a()V

    invoke-static {p5}, Lmaps/br/g;->a(Lmaps/br/k;)V

    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sput v0, Lmaps/ap/p;->f:I

    invoke-static {p0}, Lmaps/aw/a;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    invoke-static {p0}, Lmaps/aw/a;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    invoke-static {p0}, Lmaps/aw/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lmaps/aw/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p3, v0}, Lmaps/ap/p;->a(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Lmaps/bn/d;

    move-result-object v0

    const-string v2, "DriveAbout"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lmaps/bn/d;->d()V

    :cond_1
    invoke-static {v0}, Lmaps/be/g;->a(Lmaps/bn/d;)V

    new-instance v2, Lmaps/ax/j;

    invoke-direct {v2, v0}, Lmaps/ax/j;-><init>(Lmaps/bn/d;)V

    invoke-virtual {v0, v2}, Lmaps/bn/d;->a(Lmaps/bn/l;)V

    :cond_2
    move-object v2, v0

    invoke-virtual {v2}, Lmaps/bn/d;->q()V

    invoke-static {p0}, Lmaps/aw/a;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-static {v2, v4}, Lmaps/ai/c;->a(Lmaps/bn/d;Ljava/io/File;)Lmaps/ai/c;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    const/4 v0, -0x1

    if-eq p4, v0, :cond_3

    :try_start_2
    invoke-virtual {p1, p4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lmaps/ae/x;->a(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :goto_1
    :try_start_3
    const-string v0, "DriveAbout"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v6, "GMM"

    invoke-virtual {v6, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v0, :cond_4

    if-eqz v6, :cond_8

    :cond_4
    sget-object v0, Lmaps/ap/p;->b:[Lmaps/ao/b;

    invoke-static {v0, v2, v5, v4, p1}, Lmaps/ap/p;->a([Lmaps/ao/b;Lmaps/bn/k;Ljava/util/Locale;Ljava/io/File;Landroid/content/res/Resources;)V

    sget-object v0, Lmaps/ap/p;->a:[Lmaps/ao/b;

    invoke-static {}, Lmaps/bn/d;->b()Lmaps/bn/k;

    move-result-object v6

    invoke-static {v0, v6, v5, v4, p1}, Lmaps/ap/p;->a([Lmaps/ao/b;Lmaps/bn/k;Ljava/util/Locale;Ljava/io/File;Landroid/content/res/Resources;)V

    :cond_5
    :goto_2
    invoke-static {}, Lmaps/ba/f;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lmaps/bs/b;

    invoke-direct {v0}, Lmaps/bs/b;-><init>()V

    invoke-static {v2, v4, v5, v0}, Lmaps/ae/n;->a(Lmaps/bn/d;Ljava/io/File;Ljava/util/Locale;Lmaps/bs/b;)Lmaps/ae/n;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lmaps/ae/n;->d()V

    invoke-static {v0}, Lmaps/ab/q;->a(Lmaps/ae/n;)Lmaps/ab/q;

    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v2, v4, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v5, v2

    cmpl-double v2, v5, v7

    if-gtz v2, :cond_7

    iget v2, v4, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v2, v0

    float-to-double v5, v2

    cmpl-double v2, v5, v7

    if-lez v2, :cond_9

    :cond_7
    move v2, v0

    :goto_3
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v5, v5

    div-float v0, v5, v0

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    div-float v2, v4, v2

    mul-float/2addr v0, v0

    mul-float/2addr v2, v2

    add-float/2addr v0, v2

    const/high16 v2, 0x41c80000    # 25.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_a

    move v0, v1

    :goto_4
    sput-boolean v0, Lmaps/ap/p;->d:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/ap/p;->c:Z

    invoke-static {}, Lmaps/bl/a;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v6, "Could not load encryption key"

    invoke-static {v6, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_8
    if-eqz p2, :cond_5

    invoke-static {p2, v2, v5, v4, p1}, Lmaps/ap/p;->a([Lmaps/ao/b;Lmaps/bn/k;Ljava/util/Locale;Ljava/io/File;Landroid/content/res/Resources;)V

    goto :goto_2

    :cond_9
    iget v0, v4, Landroid/util/DisplayMetrics;->xdpi:F

    iget v2, v4, Landroid/util/DisplayMetrics;->ydpi:F
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private static declared-synchronized a([Lmaps/ao/b;Lmaps/bn/k;Ljava/util/Locale;Ljava/io/File;Landroid/content/res/Resources;)V
    .locals 10

    const-class v8, Lmaps/ap/p;

    monitor-enter v8

    :try_start_0
    const-string v0, "GMM"

    invoke-interface {p1}, Lmaps/bn/k;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x1

    sput v0, Lmaps/ap/p;->e:I

    :cond_0
    array-length v9, p0

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v9, :cond_2

    aget-object v0, p0, v7

    invoke-static {v0}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lmaps/ao/b;->a(Lmaps/bn/k;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Lmaps/ae/y;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lmaps/ae/y;->i()V

    invoke-static {v0, v1}, Lmaps/ae/ab;->a(Lmaps/ao/b;Lmaps/ae/y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_2
    monitor-exit v8

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lmaps/ap/p;->d:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    invoke-static {}, Lmaps/ba/f;->g()Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()V
    .locals 4

    const-class v1, Lmaps/ap/p;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmaps/ap/p;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lmaps/ao/b;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/y;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lmaps/ai/c;->c()Lmaps/ai/c;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lmaps/ai/c;->c()Lmaps/ai/c;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/ai/c;->a(Z)V

    :cond_4
    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/n;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static d()I
    .locals 1

    sget v0, Lmaps/ap/p;->e:I

    return v0
.end method

.method public static declared-synchronized e()V
    .locals 4

    const-class v1, Lmaps/ap/p;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lmaps/ap/p;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lmaps/ao/b;->c()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ao/b;

    invoke-static {v0}, Lmaps/ae/ab;->a(Lmaps/ao/b;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Lmaps/ae/ab;->b(Lmaps/ao/b;)Lmaps/ae/y;

    move-result-object v0

    invoke-interface {v0}, Lmaps/ae/y;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_2
    invoke-static {}, Lmaps/ai/c;->c()Lmaps/ai/c;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/ai/c;->a(Z)V

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ae/n;->c()Lmaps/ae/n;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ae/n;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static f()I
    .locals 1

    sget v0, Lmaps/ap/p;->f:I

    return v0
.end method
