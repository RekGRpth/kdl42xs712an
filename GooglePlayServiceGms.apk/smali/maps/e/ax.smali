.class public final Lmaps/e/ax;
.super Lewm;

# interfaces
.implements Lmaps/e/bq;


# static fields
.field private static final a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lmaps/e/bs;

.field private d:Lmaps/e/f;

.field private final e:Lmaps/e/n;

.field private final f:F

.field private final g:F

.field private h:Landroid/graphics/Bitmap;

.field private i:I

.field private j:I

.field private final k:Lmaps/ac/av;

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:Z

.field private q:F

.field private r:Lmaps/ac/cx;

.field private s:Z

.field private t:Lmaps/at/n;

.field private u:Lmaps/as/b;

.field private v:Lmaps/at/i;

.field private final w:Lmaps/h/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;-><init>()V

    sput-object v0, Lmaps/e/ax;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lmaps/e/n;Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lmaps/e/bs;Lmaps/h/a;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lewm;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    iput-object p1, p0, Lmaps/e/ax;->b:Ljava/lang/String;

    iput-object p4, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    iput-object p2, p0, Lmaps/e/ax;->e:Lmaps/e/n;

    iput-object p5, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->o:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->m()Z

    move-result v0

    iput-boolean v0, p0, Lmaps/e/ax;->p:Z

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->q:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->c()Levo;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    const-string v3, "Options doesn\'t specify an image"

    invoke-static {v0, v3}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->c()Levo;

    move-result-object v0

    iget-object v0, v0, Levo;->a:Lcrv;

    invoke-static {v0}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/e/f;

    iput-object v0, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    iget-object v0, p0, Lmaps/e/ax;->e:Lmaps/e/n;

    iget-object v3, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    invoke-virtual {v0, v3}, Lmaps/e/n;->a(Lmaps/e/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lmaps/e/ax;->i:I

    iget-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lmaps/e/ax;->j:I

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->k()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->f:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->l()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->g:F

    invoke-direct {p0}, Lmaps/e/ax;->r()V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->d()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    const-string v0, "Options doesn\'t specify a position"

    invoke-static {v2, v0}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->g()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/e/ax;->b(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    iget-wide v3, v1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v5, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v2, v3, v4, v5, v6}, Lmaps/ac/av;->b(DD)V

    invoke-direct {p0, v0}, Lmaps/e/ax;->c(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v1

    iput v1, p0, Lmaps/e/ax;->l:F

    invoke-direct {p0, v0}, Lmaps/e/ax;->d(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    move-object v1, p0

    :goto_1
    iput v0, v1, Lmaps/e/ax;->m:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->n:F

    invoke-direct {p0}, Lmaps/e/ax;->s()V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()F

    move-result v0

    sget-object v1, Lmaps/e/ax;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->h()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->U:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_2
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v0

    sget-object v1, Lmaps/e/ax;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->j()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Z:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->m()Z

    move-result v0

    sget-object v1, Lmaps/e/ax;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->m()Z

    move-result v1

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Y:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_4
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v0

    sget-object v1, Lmaps/e/ax;->a:Lcom/google/android/gms/maps/model/GroundOverlayOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->i()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->X:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    :cond_5
    return-void

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->d()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3, v4, v5}, Lmaps/ac/av;->b(DD)V

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->e()F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->l:F

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->f()F

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_8

    invoke-virtual {p3}, Lcom/google/android/gms/maps/model/GroundOverlayOptions;->f()F

    move-result v0

    move-object v1, p0

    goto/16 :goto_1

    :cond_8
    iget v0, p0, Lmaps/e/ax;->j:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/e/ax;->i:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/e/ax;->l:F

    mul-float/2addr v0, v1

    move-object v1, p0

    goto/16 :goto_1
.end method

.method private a(D)D
    .locals 4

    iget-object v0, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    iget v2, p0, Lmaps/e/ax;->l:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lmaps/e/ax;->i:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private b(D)D
    .locals 4

    iget-object v0, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->e()D

    move-result-wide v0

    mul-double/2addr v0, p1

    iget v2, p0, Lmaps/e/ax;->m:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lmaps/e/ax;->j:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 8

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v0

    new-instance v1, Lmaps/ac/av;

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v0, v1}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    :goto_0
    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v1

    iget v2, p0, Lmaps/e/ax;->f:F

    iget v3, p0, Lmaps/e/ax;->g:F

    new-instance v4, Lmaps/ac/av;

    sub-float v5, v7, v2

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v2, v6

    add-float/2addr v2, v5

    float-to-int v2, v2

    sub-float v5, v7, v3

    invoke-virtual {v0}, Lmaps/ac/av;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    invoke-virtual {v1}, Lmaps/ac/av;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {v4, v2, v0}, Lmaps/ac/av;-><init>(II)V

    invoke-static {v4}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/maps/model/LatLngBounds;)F
    .locals 6

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    const-wide v4, 0x4076800000000000L    # 360.0

    add-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lmaps/ac/av;->a(DD)Lmaps/ac/av;

    move-result-object v0

    :goto_0
    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v0

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-object v2, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->e()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Lcom/google/android/gms/maps/model/LatLngBounds;)F
    .locals 4

    iget-object v0, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lmaps/i/a;->b(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ac/av;->g()I

    move-result v0

    invoke-virtual {v1}, Lmaps/ac/av;->g()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-double v0, v0

    iget-object v2, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v2}, Lmaps/ac/av;->e()D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private p()F
    .locals 2

    iget v0, p0, Lmaps/e/ax;->f:F

    iget v1, p0, Lmaps/e/ax;->i:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private q()F
    .locals 2

    iget v0, p0, Lmaps/e/ax;->g:F

    iget v1, p0, Lmaps/e/ax;->j:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private r()V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Lmaps/at/n;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lmaps/at/n;-><init>(I)V

    iput-object v0, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    iget-object v0, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v1

    neg-float v1, v1

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lmaps/at/n;->a(FFF)V

    iget-object v0, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v1

    neg-float v1, v1

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v2

    iget v3, p0, Lmaps/e/ax;->j:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2, v4}, Lmaps/at/n;->a(FFF)V

    iget-object v0, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    iget v1, p0, Lmaps/e/ax;->i:I

    int-to-float v1, v1

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v2

    invoke-virtual {v0, v1, v2, v4}, Lmaps/at/n;->a(FFF)V

    iget-object v0, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    iget v1, p0, Lmaps/e/ax;->i:I

    int-to-float v1, v1

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v2

    iget v3, p0, Lmaps/e/ax;->j:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2, v4}, Lmaps/at/n;->a(FFF)V

    return-void
.end method

.method private s()V
    .locals 6

    iget v0, p0, Lmaps/e/ax;->n:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmaps/e/ax;->u()Lmaps/ac/cx;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ax;->r:Lmaps/ac/cx;

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lmaps/e/ax;->l:F

    iget v1, p0, Lmaps/e/ax;->l:F

    mul-float/2addr v0, v1

    iget v1, p0, Lmaps/e/ax;->m:F

    iget v2, p0, Lmaps/e/ax;->m:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    new-instance v2, Lmaps/ac/av;

    iget-object v3, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ac/av;->e()D

    move-result-wide v3

    mul-double/2addr v3, v0

    double-to-int v3, v3

    iget-object v4, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v4}, Lmaps/ac/av;->e()D

    move-result-wide v4

    mul-double/2addr v0, v4

    double-to-int v0, v0

    invoke-direct {v2, v3, v0}, Lmaps/ac/av;-><init>(II)V

    new-instance v0, Lmaps/ac/bd;

    iget-object v1, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v1, v2}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v1

    iget-object v3, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-virtual {v3, v2}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v0}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ax;->r:Lmaps/ac/cx;

    goto :goto_0
.end method

.method private t()V
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    :cond_0
    return-void
.end method

.method private u()Lmaps/ac/cx;
    .locals 7

    new-instance v0, Lmaps/ac/bd;

    iget-object v1, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    new-instance v2, Lmaps/ac/av;

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v3

    float-to-double v3, v3

    invoke-direct {p0, v3, v4}, Lmaps/e/ax;->a(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p0, Lmaps/e/ax;->j:I

    int-to-float v4, v4

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-direct {p0, v4, v5}, Lmaps/e/ax;->b(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v2, v3, v4}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v1, v2}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v1

    iget-object v2, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    new-instance v3, Lmaps/ac/av;

    iget v4, p0, Lmaps/e/ax;->i:I

    int-to-float v4, v4

    invoke-direct {p0}, Lmaps/e/ax;->p()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-double v4, v4

    invoke-direct {p0, v4, v5}, Lmaps/e/ax;->a(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {p0}, Lmaps/e/ax;->q()F

    move-result v5

    float-to-double v5, v5

    invoke-direct {p0, v5, v6}, Lmaps/e/ax;->b(D)D

    move-result-wide v5

    double-to-int v5, v5

    invoke-direct {v3, v4, v5}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {v2, v3}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v0}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->T:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->a(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, p1, v0}, Lmaps/e/ax;->a(FF)V

    return-void
.end method

.method public final a(FF)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->V:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ax;->l:F

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_0

    :goto_0
    iput p2, p0, Lmaps/e/ax;->m:F

    invoke-direct {p0}, Lmaps/e/ax;->s()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lmaps/e/ax;->j:I

    int-to-float v0, v0

    iget v1, p0, Lmaps/e/ax;->i:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lmaps/e/ax;->l:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    mul-float p2, v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 5

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->W:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v1, v2, v3, v4}, Lmaps/ac/av;->b(DD)V

    invoke-direct {p0}, Lmaps/e/ax;->s()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 6

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lmaps/e/ax;->b(Lcom/google/android/gms/maps/model/LatLngBounds;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3, v4, v5}, Lmaps/ac/av;->b(DD)V

    invoke-direct {p0, p1}, Lmaps/e/ax;->c(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->l:F

    invoke-direct {p0, p1}, Lmaps/e/ax;->d(Lcom/google/android/gms/maps/model/LatLngBounds;)F

    move-result v0

    iput v0, p0, Lmaps/e/ax;->m:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcrv;)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    iget-object v0, v0, Lmaps/e/bs;->a:Lmaps/i/g;

    invoke-virtual {v0}, Lmaps/i/g;->b()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aa:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->e:Lmaps/e/n;

    iget-object v1, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    invoke-virtual {v0, v1}, Lmaps/e/n;->b(Lmaps/e/f;)V

    invoke-static {p1}, Lmaps/e/f;->a(Lcrv;)Lmaps/e/f;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    iget-object v0, p0, Lmaps/e/ax;->e:Lmaps/e/n;

    iget-object v1, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    invoke-virtual {v0, v1}, Lmaps/e/n;->a(Lmaps/e/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lmaps/e/ax;->i:I

    iget-object v0, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lmaps/e/ax;->j:I

    invoke-direct {p0}, Lmaps/e/ax;->r()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ar/a;Lmaps/as/a;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ax;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/e/ax;->r:Lmaps/ac/cx;

    invoke-virtual {v0}, Lmaps/ac/cx;->f()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v0

    iget-object v1, p0, Lmaps/e/ax;->r:Lmaps/ac/cx;

    invoke-virtual {v1}, Lmaps/ac/cx;->g()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v1

    if-gt v0, v1, :cond_1

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/ax;->r:Lmaps/ac/cx;

    invoke-virtual {v1}, Lmaps/ac/cx;->b()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/cw;->b(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lmaps/e/ax;->s:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 0

    return-void
.end method

.method public final declared-synchronized a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ax;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lmaps/e/ax;->s:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget-object v1, p0, Lmaps/e/ax;->t:Lmaps/at/n;

    invoke-virtual {v1, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    if-nez v1, :cond_2

    new-instance v1, Lmaps/as/b;

    invoke-direct {v1, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    iput-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmaps/as/b;->c(Z)V

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmaps/as/b;->d(Z)V

    invoke-static {}, Lmaps/bb/a;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    iget-object v2, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lmaps/as/b;->a(Landroid/graphics/Bitmap;)V

    :cond_2
    :goto_1
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget v1, p0, Lmaps/e/ax;->q:F

    sub-float v1, v5, v1

    iget v2, p0, Lmaps/e/ax;->q:F

    sub-float v2, v5, v2

    iget v3, p0, Lmaps/e/ax;->q:F

    sub-float v3, v5, v3

    iget v4, p0, Lmaps/e/ax;->q:F

    sub-float v4, v5, v4

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    iget-object v1, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    if-nez v1, :cond_3

    new-instance v1, Lmaps/at/i;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lmaps/at/i;-><init>(I)V

    iput-object v1, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->e()F

    move-result v1

    iget-object v2, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    invoke-virtual {v2}, Lmaps/as/b;->f()F

    move-result v2

    iget-object v3, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lmaps/at/i;->a(FF)V

    iget-object v3, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lmaps/at/i;->a(FF)V

    iget-object v3, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lmaps/at/i;->a(FF)V

    iget-object v3, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    invoke-virtual {v3, v1, v2}, Lmaps/at/i;->a(FF)V

    :cond_3
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget-object v1, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p1, p2, v1, v2}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    iget v1, p0, Lmaps/e/ax;->n:F

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v1, v2}, Lmaps/e/ax;->a(D)D

    move-result-wide v1

    double-to-float v1, v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v2, v3}, Lmaps/e/ax;->b(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    iget-object v1, p0, Lmaps/e/ax;->v:Lmaps/at/i;

    invoke-virtual {v1, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    invoke-virtual {v1, v0}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    iget-object v1, p0, Lmaps/e/ax;->u:Lmaps/as/b;

    iget-object v2, p0, Lmaps/e/ax;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Y:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lmaps/e/ax;->p:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lewl;)Z
    .locals 1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->U:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ax;->n:F

    invoke-direct {p0}, Lmaps/e/ax;->s()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final c()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->k:Lmaps/ac/av;

    invoke-static {v0}, Lmaps/i/a;->a(Lmaps/ac/av;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->X:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    iget-object v1, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->b(Lmaps/e/bq;)V

    iput p1, p0, Lmaps/e/ax;->o:F

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0, p0}, Lmaps/e/bs;->c(Lmaps/e/bq;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final d()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ax;->l:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(F)V
    .locals 2

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget-object v0, p0, Lmaps/e/ax;->w:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->Z:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->b(Lmaps/h/b;)V

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Transparency must be in the range [0..1]"

    invoke-static {v0, v1}, Lmaps/k/o;->a(ZLjava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/e/ax;->q:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->j()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ax;->m:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/e/ax;->u()Lmaps/ac/cx;

    move-result-object v0

    invoke-static {v0}, Lmaps/i/a;->a(Lmaps/ac/cx;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ax;->n:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    iget v0, p0, Lmaps/e/ax;->o:F

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/ax;->p:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()F
    .locals 1

    iget-object v0, p0, Lmaps/e/ax;->c:Lmaps/e/bs;

    invoke-virtual {v0}, Lmaps/e/bs;->d()V

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lmaps/e/ax;->q:F

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final k()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final declared-synchronized l()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/e/ax;->t()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final n()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized o()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/e/ax;->e:Lmaps/e/n;

    iget-object v1, p0, Lmaps/e/ax;->d:Lmaps/e/f;

    invoke-virtual {v0, v1}, Lmaps/e/n;->b(Lmaps/e/f;)V

    invoke-direct {p0}, Lmaps/e/ax;->t()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
