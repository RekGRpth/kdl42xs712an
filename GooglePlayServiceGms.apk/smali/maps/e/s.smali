.class public final Lmaps/e/s;
.super Letc;


# instance fields
.field private a:Lmaps/e/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Letc;-><init>()V

    new-instance v0, Lmaps/e/t;

    invoke-direct {v0}, Lmaps/e/t;-><init>()V

    iput-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    return-void
.end method


# virtual methods
.method public final a()Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {}, Lmaps/e/t;->a()Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1}, Lmaps/e/t;->a(F)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(FF)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1, p2}, Lmaps/e/t;->a(FF)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(FII)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1, p2, p3}, Lmaps/e/t;->a(FII)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/CameraPosition;)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1}, Lmaps/e/t;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1}, Lmaps/e/t;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;F)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1, p2}, Lmaps/e/t;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1, p2}, Lmaps/e/t;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1, p2, p3, p4}, Lmaps/e/t;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {}, Lmaps/e/t;->b()Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method

.method public final b(F)Lcrv;
    .locals 1

    iget-object v0, p0, Lmaps/e/s;->a:Lmaps/e/t;

    invoke-static {p1}, Lmaps/e/t;->b(F)Lmaps/e/af;

    move-result-object v0

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    return-object v0
.end method
