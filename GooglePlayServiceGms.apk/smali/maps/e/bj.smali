.class public Lmaps/e/bj;
.super Lmaps/ap/e;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private c:Leul;

.field private d:Lmaps/e/ah;

.field private final e:Lmaps/e/ai;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/e/bj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/e/bj;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lmaps/e/ah;Lmaps/e/ai;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/ap/e;-><init>()V

    iput-boolean v0, p0, Lmaps/e/bj;->f:Z

    iput-boolean v0, p0, Lmaps/e/bj;->g:Z

    iput-boolean v0, p0, Lmaps/e/bj;->h:Z

    iput-boolean v0, p0, Lmaps/e/bj;->i:Z

    iput-boolean v0, p0, Lmaps/e/bj;->j:Z

    const-string v0, "Handler is null"

    invoke-static {p1, v0}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lmaps/e/bj;->b:Landroid/os/Handler;

    iput-object p2, p0, Lmaps/e/bj;->d:Lmaps/e/ah;

    iput-object p3, p0, Lmaps/e/bj;->e:Lmaps/e/ai;

    return-void
.end method

.method public static a(Lmaps/c/m;Landroid/os/Handler;Lmaps/e/ah;Lmaps/e/ai;)Lmaps/e/bj;
    .locals 1

    new-instance v0, Lmaps/e/bj;

    invoke-direct {v0, p1, p2, p3}, Lmaps/e/bj;-><init>(Landroid/os/Handler;Lmaps/e/ah;Lmaps/e/ai;)V

    invoke-interface {p0, v0}, Lmaps/c/m;->a(Lmaps/e/bj;)V

    return-object v0
.end method

.method private declared-synchronized f()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lmaps/e/bj;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/bj;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/bj;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/bj;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/bj;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/bj;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/bj;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Leul;)V
    .locals 1

    iput-object p1, p0, Lmaps/e/bj;->c:Leul;

    invoke-direct {p0}, Lmaps/e/bj;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/e/bj;->run()V

    :cond_0
    return-void
.end method

.method protected final declared-synchronized a(Z)Z
    .locals 3

    const/4 v2, 0x0

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/bj;->i:Z

    iput-boolean p1, p0, Lmaps/e/bj;->j:Z

    invoke-direct {p0}, Lmaps/e/bj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/bj;->d:Lmaps/e/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bj;->d:Lmaps/e/ah;

    iget-object v1, p0, Lmaps/e/bj;->e:Lmaps/e/ai;

    invoke-interface {v0, v1}, Lmaps/e/ah;->a(Lmaps/e/ai;)V

    iget-object v0, p0, Lmaps/e/bj;->d:Lmaps/e/ah;

    invoke-interface {v0}, Lmaps/e/ah;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/bj;->d:Lmaps/e/ah;

    :cond_0
    iget-object v0, p0, Lmaps/e/bj;->c:Leul;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/e/bj;->b:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/bj;->g:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/bj;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/bj;->f:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/bj;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lmaps/e/bj;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lmaps/e/bj;->c:Leul;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bj;->c:Leul;

    invoke-interface {v0}, Leul;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/e/bj;->c:Leul;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
