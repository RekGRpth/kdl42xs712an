.class public final Lmaps/e/be;
.super Ljava/lang/Object;


# static fields
.field private static a:Landroid/content/res/Resources;

.field private static b:Landroid/content/res/Resources;


# direct methods
.method static a()Landroid/content/res/Resources;
    .locals 2

    sget-object v0, Lmaps/e/be;->a:Landroid/content/res/Resources;

    const-string v1, "Library Resources have not been initialized"

    invoke-static {v0, v1}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;)V
    .locals 0

    sput-object p0, Lmaps/e/be;->a:Landroid/content/res/Resources;

    return-void
.end method

.method static b()Landroid/content/res/Resources;
    .locals 2

    sget-object v0, Lmaps/e/be;->b:Landroid/content/res/Resources;

    const-string v1, "Client App Resources have not been initialized"

    invoke-static {v0, v1}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    return-object v0
.end method

.method public static b(Landroid/content/res/Resources;)V
    .locals 0

    sput-object p0, Lmaps/e/be;->b:Landroid/content/res/Resources;

    return-void
.end method
