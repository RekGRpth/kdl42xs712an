.class final Lmaps/e/cc;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Landroid/graphics/Bitmap;

.field private synthetic b:Levg;

.field private synthetic c:Lmaps/e/ca;


# direct methods
.method constructor <init>(Lmaps/e/ca;Landroid/graphics/Bitmap;Levg;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/cc;->c:Lmaps/e/ca;

    iput-object p2, p0, Lmaps/e/cc;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lmaps/e/cc;->b:Levg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lmaps/e/cc;->c:Lmaps/e/ca;

    invoke-static {v0}, Lmaps/e/ca;->a(Lmaps/e/ca;)Lmaps/c/m;

    move-result-object v0

    invoke-interface {v0}, Lmaps/c/m;->getWidth()I

    move-result v0

    iget-object v1, p0, Lmaps/e/cc;->c:Lmaps/e/ca;

    invoke-static {v1}, Lmaps/e/ca;->a(Lmaps/e/ca;)Lmaps/c/m;

    move-result-object v1

    invoke-interface {v1}, Lmaps/c/m;->getHeight()I

    move-result v1

    iget-object v2, p0, Lmaps/e/cc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lmaps/e/cc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v2, v1, :cond_1

    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/e/cc;->c:Lmaps/e/ca;

    invoke-static {v0}, Lmaps/e/ca;->a(Lmaps/e/ca;)Lmaps/c/m;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->getBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lmaps/e/cc;->c:Lmaps/e/ca;

    invoke-static {v0, v1}, Lmaps/e/ca;->a(Lmaps/e/ca;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lmaps/e/cc;->b:Levg;

    invoke-static {v0, v1}, Lmaps/e/ca;->a(Levg;Landroid/graphics/Bitmap;)V

    return-void

    :cond_1
    iget-object v0, p0, Lmaps/e/cc;->a:Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_0
.end method
