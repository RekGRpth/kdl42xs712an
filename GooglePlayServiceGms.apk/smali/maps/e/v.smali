.class final Lmaps/e/v;
.super Lmaps/e/af;


# instance fields
.field private synthetic a:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private synthetic b:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/model/LatLngBounds;I)V
    .locals 0

    iput-object p1, p0, Lmaps/e/v;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput p2, p0, Lmaps/e/v;->b:I

    invoke-direct {p0}, Lmaps/e/af;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lmaps/c/m;Lmaps/c/l;ILmaps/h/a;)V
    .locals 7

    sget-object v0, Lmaps/h/b;->av:Lmaps/h/b;

    invoke-interface {p4, v0}, Lmaps/h/a;->b(Lmaps/h/b;)V

    invoke-interface {p1}, Lmaps/c/m;->getWidth()I

    move-result v4

    invoke-interface {p1}, Lmaps/c/m;->getHeight()I

    move-result v5

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Map size should not be 0. Most likely, layout has not yet occured for the map view."

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v3, p0, Lmaps/e/v;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    iget v6, p0, Lmaps/e/v;->b:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lmaps/e/t;->a(Lmaps/c/m;Lmaps/c/l;ILcom/google/android/gms/maps/model/LatLngBounds;III)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
