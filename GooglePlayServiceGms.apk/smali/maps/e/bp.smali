.class final Lmaps/e/bp;
.super Leug;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lmaps/e/bo;


# instance fields
.field private final a:Lmaps/c/l;

.field private final b:Lmaps/c/m;

.field private final c:Letq;

.field private final d:Lmaps/g/c;

.field private final e:Landroid/content/res/Resources;

.field private f:Lmaps/ay/aj;

.field private g:Landroid/location/Location;

.field private h:Letq;

.field private final i:Lmaps/h/a;

.field private j:Z

.field private k:Z

.field private l:Leva;

.field private m:Leux;


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;Lmaps/c/l;Lmaps/c/m;Lmaps/g/c;Letq;Lmaps/h/a;)V
    .locals 1

    invoke-direct {p0}, Leug;-><init>()V

    iput-object p2, p0, Lmaps/e/bp;->a:Lmaps/c/l;

    iput-object p3, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    iput-object p5, p0, Lmaps/e/bp;->c:Letq;

    iput-object p4, p0, Lmaps/e/bp;->d:Lmaps/g/c;

    iput-object p5, p0, Lmaps/e/bp;->h:Letq;

    iput-object p6, p0, Lmaps/e/bp;->i:Lmaps/h/a;

    iput-object p1, p0, Lmaps/e/bp;->e:Landroid/content/res/Resources;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/e/bp;->k:Z

    return-void
.end method

.method public static a(Landroid/content/res/Resources;Lmaps/c/l;Lmaps/c/m;Lmaps/g/c;Letq;Lmaps/h/a;)Lmaps/e/bp;
    .locals 7

    new-instance v0, Lmaps/e/bp;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lmaps/e/bp;-><init>(Landroid/content/res/Resources;Lmaps/c/l;Lmaps/c/m;Lmaps/g/c;Letq;Lmaps/h/a;)V

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lmaps/e/bp;->k:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lmaps/e/bp;->d:Lmaps/g/c;

    invoke-virtual {v1, v0}, Lmaps/g/c;->a(Z)V

    iget-object v1, p0, Lmaps/e/bp;->d:Lmaps/g/c;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v1, p0}, Lmaps/g/c;->a(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 10

    const v9, 0x73217bce

    const v8, 0x338cc6ef

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v7, p0, Lmaps/e/bp;->j:Z

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    invoke-interface {v0}, Lmaps/c/m;->i()Lmaps/ay/aj;

    move-result-object v0

    iput-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    const/4 v1, 0x5

    new-array v1, v1, [Lmaps/ay/al;

    new-instance v2, Lmaps/ay/am;

    invoke-direct {v2}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v2, v7}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2, v6}, Lmaps/ay/am;->b(Z)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->a()Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->b()Lmaps/ay/am;

    move-result-object v2

    sget v3, Lmaps/b/e;->W:I

    sget v4, Lmaps/b/e;->X:I

    invoke-virtual {v2, v3, v4}, Lmaps/ay/am;->a(II)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Lmaps/ay/am;->b(II)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v2

    aput-object v2, v1, v6

    new-instance v2, Lmaps/ay/am;

    invoke-direct {v2}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v2, v6}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2, v6}, Lmaps/ay/am;->b(Z)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->a()Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->c()Lmaps/ay/am;

    move-result-object v2

    sget v3, Lmaps/b/e;->S:I

    sget v4, Lmaps/b/e;->T:I

    invoke-virtual {v2, v3, v4}, Lmaps/ay/am;->a(II)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Lmaps/ay/am;->b(II)Lmaps/ay/am;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    new-instance v3, Lmaps/ay/am;

    invoke-direct {v3}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v3, v7}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/ay/am;->b(Z)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->b()Lmaps/ay/am;

    move-result-object v3

    sget v4, Lmaps/b/e;->Y:I

    sget v5, Lmaps/b/e;->Z:I

    invoke-virtual {v3, v4, v5}, Lmaps/ay/am;->a(II)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3, v9, v8}, Lmaps/ay/am;->b(II)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lmaps/ay/am;

    invoke-direct {v3}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v3, v6}, Lmaps/ay/am;->a(Z)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3, v6}, Lmaps/ay/am;->b(Z)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->c()Lmaps/ay/am;

    move-result-object v3

    sget v4, Lmaps/b/e;->U:I

    sget v5, Lmaps/b/e;->V:I

    invoke-virtual {v3, v4, v5}, Lmaps/ay/am;->a(II)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3, v9, v8}, Lmaps/ay/am;->b(II)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lmaps/ay/am;

    invoke-direct {v3}, Lmaps/ay/am;-><init>()V

    invoke-virtual {v3, v7}, Lmaps/ay/am;->b(Z)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->c()Lmaps/ay/am;

    move-result-object v3

    sget v4, Lmaps/b/e;->aa:I

    sget v5, Lmaps/b/e;->ab:I

    invoke-virtual {v3, v4, v5}, Lmaps/ay/am;->a(II)Lmaps/ay/am;

    move-result-object v3

    const v4, 0x73aaaaaa

    const v5, 0x33cccccc

    invoke-virtual {v3, v4, v5}, Lmaps/ay/am;->b(II)Lmaps/ay/am;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ay/am;->d()Lmaps/ay/al;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmaps/ay/aj;->a([Lmaps/ay/al;)V

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    iget-object v1, p0, Lmaps/e/bp;->e:Landroid/content/res/Resources;

    sget v2, Lmaps/b/d;->k:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v2, p0, Lmaps/e/bp;->e:Landroid/content/res/Resources;

    sget v3, Lmaps/b/f;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Lmaps/e/bp;->e:Landroid/content/res/Resources;

    sget v4, Lmaps/b/f;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ay/aj;->a(FII)V

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    sget-object v1, Lmaps/ay/v;->p:Lmaps/ay/v;

    invoke-virtual {v0, v1}, Lmaps/ay/aj;->a(Lmaps/ay/v;)V

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    invoke-virtual {v0}, Lmaps/ay/aj;->b()V

    iget-object v0, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lmaps/ay/aj;->b(I)V

    :cond_2
    iget-object v0, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    invoke-interface {v0, v1}, Lmaps/c/m;->a(Lmaps/ay/u;)V

    :try_start_0
    iget-object v0, p0, Lmaps/e/bp;->h:Letq;

    invoke-interface {v0, p0}, Letq;->a(Leuf;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lmaps/e/bp;->f()V

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/e/bp;->a(Lcrv;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcrv;)V
    .locals 6

    const/4 v5, 0x1

    invoke-static {p1}, Lcry;->a(Lcrv;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lmaps/ac/av;->a(DD)Lmaps/ac/av;

    move-result-object v1

    new-instance v2, Lmaps/ac/au;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v2, v1, v3, v4}, Lmaps/ac/au;-><init>(Lmaps/ac/av;FI)V

    invoke-virtual {v2, v1}, Lmaps/ac/au;->a(Lmaps/ac/av;)V

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    invoke-virtual {v2, v1}, Lmaps/ac/au;->a(Z)V

    iget-object v1, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    invoke-virtual {v1, v2}, Lmaps/ay/aj;->a(Lmaps/ac/au;)V

    iget-object v1, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    invoke-interface {v1, v5, v5}, Lmaps/c/m;->a(ZZ)V

    iget-object v1, p0, Lmaps/e/bp;->l:Leva;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lmaps/e/bp;->l:Leva;

    new-instance v2, Landroid/location/Location;

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-static {v2}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v2

    invoke-interface {v1, v2}, Leva;->a(Lcrv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iput-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Letq;)V
    .locals 2

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lmaps/e/bp;->h:Letq;

    invoke-interface {v0}, Letq;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    if-eqz p1, :cond_2

    :goto_0
    iput-object p1, p0, Lmaps/e/bp;->h:Letq;

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lmaps/e/bp;->h:Letq;

    invoke-interface {v0, p0}, Letq;->a(Leuf;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_2
    iget-object p1, p0, Lmaps/e/bp;->c:Letq;

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Leux;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/bp;->m:Leux;

    return-void
.end method

.method public final a(Leva;)V
    .locals 0

    iput-object p1, p0, Lmaps/e/bp;->l:Leva;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lmaps/e/bp;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lmaps/e/bp;->k:Z

    invoke-direct {p0}, Lmaps/e/bp;->f()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/e/bp;->j:Z

    invoke-direct {p0}, Lmaps/e/bp;->f()V

    :try_start_0
    iget-object v0, p0, Lmaps/e/bp;->h:Letq;

    invoke-interface {v0}, Letq;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    iget-object v1, p0, Lmaps/e/bp;->f:Lmaps/ay/aj;

    invoke-interface {v0, v1}, Lmaps/c/m;->b(Lmaps/ay/u;)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/e/bp;->k:Z

    return v0
.end method

.method public final e()Landroid/location/Location;
    .locals 2

    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 13

    const/4 v12, -0x1

    const-wide v10, 0x412e848000000000L    # 1000000.0

    const/high16 v2, -0x40800000    # -1.0f

    iget-object v0, p0, Lmaps/e/bp;->i:Lmaps/h/a;

    sget-object v1, Lmaps/h/b;->aX:Lmaps/h/b;

    invoke-interface {v0, v1}, Lmaps/h/a;->a(Lmaps/h/b;)V

    iget-object v0, p0, Lmaps/e/bp;->m:Leux;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lmaps/e/bp;->m:Leux;

    invoke-interface {v0}, Leux;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Levz;

    invoke-direct {v1, v0}, Levz;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :cond_1
    iget-boolean v0, p0, Lmaps/e/bp;->j:Z

    const-string v1, "MyLocation layer not enabled"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    if-eqz v0, :cond_0

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v4, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v0, p0, Lmaps/e/bp;->g:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    iget-object v0, p0, Lmaps/e/bp;->a:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->c()Lmaps/ar/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ar/b;->b()F

    move-result v0

    const/high16 v4, 0x41200000    # 10.0f

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_2

    iget-wide v4, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    const-wide v8, 0x4037e2220bc382a1L    # 23.883332

    cmpl-double v0, v4, v8

    if-lez v0, :cond_4

    const-wide v8, 0x4047094067cf1c32L    # 46.072278

    cmpg-double v0, v4, v8

    if-gez v0, :cond_4

    const-wide v4, 0x405eefe9813879c4L    # 123.748627

    cmpl-double v0, v6, v4

    if-lez v0, :cond_4

    const-wide v4, 0x4061f940010c6f7aL    # 143.789063

    cmpg-double v0, v6, v4

    if-gez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    const/high16 v0, 0x41980000    # 19.0f

    :cond_2
    :goto_2
    iget-object v4, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    invoke-interface {v4}, Lmaps/c/m;->getWidth()I

    move-result v4

    iget-object v5, p0, Lmaps/e/bp;->b:Lmaps/c/m;

    invoke-interface {v5}, Lmaps/c/m;->getHeight()I

    move-result v5

    if-eqz v4, :cond_3

    if-nez v5, :cond_6

    :cond_3
    move v1, v2

    :goto_3
    cmpl-float v2, v1, v2

    if-nez v2, :cond_9

    move v2, v0

    :goto_4
    iget-object v0, p0, Lmaps/e/bp;->a:Lmaps/c/l;

    invoke-virtual {v0}, Lmaps/c/l;->c()Lmaps/ar/b;

    move-result-object v5

    new-instance v0, Lmaps/ar/b;

    invoke-static {v3}, Lmaps/i/a;->a(Lcom/google/android/gms/maps/model/LatLng;)Lmaps/ac/u;

    move-result-object v1

    invoke-virtual {v5}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {v5}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {v5}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/u;FFFF)V

    iget-object v1, p0, Lmaps/e/bp;->a:Lmaps/c/l;

    invoke-virtual {v1, v0, v12, v12}, Lmaps/c/l;->a(Lmaps/ar/c;II)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/high16 v0, 0x41700000    # 15.0f

    goto :goto_2

    :cond_6
    new-instance v6, Lmaps/bp/a;

    iget-wide v7, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    mul-double/2addr v7, v10

    double-to-int v7, v7

    iget-wide v8, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    mul-double/2addr v8, v10

    double-to-int v8, v8

    invoke-direct {v6, v7, v8}, Lmaps/bp/a;-><init>(II)V

    mul-float/2addr v1, v1

    float-to-long v7, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    div-int/lit8 v4, v1, 0x2

    const/16 v1, 0x15

    invoke-static {v1}, Lmaps/bp/c;->a(I)Lmaps/bp/c;

    move-result-object v1

    :goto_5
    if-eqz v1, :cond_7

    invoke-virtual {v6, v4, v1}, Lmaps/bp/a;->a(ILmaps/bp/c;)Lmaps/bp/a;

    move-result-object v5

    invoke-virtual {v6, v5}, Lmaps/bp/a;->a(Lmaps/bp/a;)J

    move-result-wide v9

    cmp-long v5, v7, v9

    if-lez v5, :cond_7

    invoke-virtual {v1}, Lmaps/bp/c;->c()Lmaps/bp/c;

    move-result-object v1

    goto :goto_5

    :cond_7
    if-nez v1, :cond_8

    move v1, v2

    goto :goto_3

    :cond_8
    invoke-virtual {v1}, Lmaps/bp/c;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    goto :goto_3

    :cond_9
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    goto :goto_4
.end method
