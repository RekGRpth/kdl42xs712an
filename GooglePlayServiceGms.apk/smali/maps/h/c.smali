.class public final Lmaps/h/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;
.implements Lmaps/h/a;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;

.field private c:J

.field private final d:Lmaps/bs/b;

.field private final e:Landroid/os/Handler;

.field private final f:Lmaps/h/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lmaps/h/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lmaps/h/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lmaps/bs/b;Lmaps/h/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    iput-object p1, p0, Lmaps/h/c;->e:Landroid/os/Handler;

    iput-object p2, p0, Lmaps/h/c;->d:Lmaps/bs/b;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/h/c;->c:J

    iput-object p3, p0, Lmaps/h/c;->f:Lmaps/h/e;

    return-void
.end method

.method public static b()Lmaps/h/a;
    .locals 4

    new-instance v0, Lmaps/h/c;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lmaps/bs/b;

    invoke-direct {v2}, Lmaps/bs/b;-><init>()V

    new-instance v3, Lmaps/h/e;

    invoke-direct {v3}, Lmaps/h/e;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lmaps/h/c;-><init>(Landroid/os/Handler;Lmaps/bs/b;Lmaps/h/e;)V

    return-object v0
.end method

.method private c()V
    .locals 6

    iget-wide v0, p0, Lmaps/h/c;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lmaps/h/c;->c:J

    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/h/d;

    iget-wide v2, p0, Lmaps/h/c;->c:J

    invoke-static {v0}, Lmaps/h/d;->a(Lmaps/h/d;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/h/c;->c:J

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/h/c;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lmaps/h/c;->e:Landroid/os/Handler;

    iget-wide v1, p0, Lmaps/h/c;->c:J

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/h/d;

    iget-object v2, p0, Lmaps/h/c;->f:Lmaps/h/e;

    iget-object v2, v0, Lmaps/h/d;->a:Lmaps/h/b;

    iget-object v2, v2, Lmaps/h/b;->bt:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "c="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lmaps/h/d;->b:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/h/e;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lmaps/h/c;->f:Lmaps/h/e;

    invoke-static {}, Lmaps/h/e;->a()V

    iget-object v0, p0, Lmaps/h/c;->e:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lmaps/h/b;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/c;->f:Lmaps/h/e;

    iget-object v0, p1, Lmaps/h/b;->bt:Ljava/lang/String;

    invoke-static {v0}, Lmaps/h/e;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lmaps/h/b;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/h/d;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/h/d;

    iget-object v1, p0, Lmaps/h/c;->d:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->c()J

    move-result-wide v1

    const-wide/16 v3, 0x2710

    add-long/2addr v1, v3

    invoke-direct {v0, p1, v1, v2}, Lmaps/h/d;-><init>(Lmaps/h/b;J)V

    iget-object v1, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v1, v0, Lmaps/h/d;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lmaps/h/d;->b:I

    invoke-direct {p0}, Lmaps/h/c;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized run()V
    .locals 8

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lmaps/h/c;->c:J

    iget-object v0, p0, Lmaps/h/c;->d:Lmaps/bs/b;

    invoke-static {}, Lmaps/bs/b;->c()J

    move-result-wide v2

    iget-object v0, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/ck;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/h/b;

    iget-object v1, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/h/d;

    invoke-static {v1}, Lmaps/h/d;->a(Lmaps/h/d;)J

    move-result-wide v5

    cmp-long v5, v2, v5

    if-ltz v5, :cond_0

    iget-object v5, p0, Lmaps/h/c;->f:Lmaps/h/e;

    iget-object v5, v1, Lmaps/h/d;->a:Lmaps/h/b;

    iget-object v5, v5, Lmaps/h/b;->bt:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "c="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v1, Lmaps/h/d;->b:I

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lmaps/h/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/h/c;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lmaps/h/c;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method
