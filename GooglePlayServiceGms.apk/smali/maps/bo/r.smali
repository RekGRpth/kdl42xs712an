.class public final Lmaps/bo/r;
.super Lmaps/bo/e;


# direct methods
.method public constructor <init>(Lmaps/bo/n;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/bo/e;-><init>(Lmaps/bo/n;)V

    return-void
.end method


# virtual methods
.method public final a(JLjava/util/LinkedList;Ljava/util/List;)Lmaps/bo/f;
    .locals 7

    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    sget-object v0, Lmaps/bo/f;->b:Lmaps/bo/f;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->f()F

    move-result v3

    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/bo/j;

    invoke-virtual {v1}, Lmaps/bo/j;->b()I

    move-result v5

    invoke-virtual {v0}, Lmaps/bo/j;->b()I

    move-result v6

    if-ne v5, v6, :cond_2

    invoke-virtual {v1}, Lmaps/bo/j;->f()F

    move-result v2

    invoke-static {v3, v2}, Lmaps/bo/r;->a(FF)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v5, 0x3e32b8c2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1

    sget-object v0, Lmaps/bo/f;->a:Lmaps/bo/f;

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    :cond_2
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x3dcccccd    # 0.1f

    :goto_2
    invoke-virtual {v2}, Lmaps/bo/j;->g()F

    move-result v2

    invoke-virtual {v0}, Lmaps/bo/j;->g()F

    move-result v3

    invoke-virtual {v0}, Lmaps/bo/j;->c()F

    move-result v4

    invoke-virtual {v0}, Lmaps/bo/j;->d()F

    move-result v0

    add-float/2addr v0, v4

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v0, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v0, v2, v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    sget-object v0, Lmaps/bo/f;->b:Lmaps/bo/f;

    goto :goto_0

    :cond_3
    const v1, 0x3e4ccccd    # 0.2f

    goto :goto_2

    :cond_4
    sget-object v0, Lmaps/bo/f;->c:Lmaps/bo/f;

    goto :goto_0
.end method

.method protected final b(Lmaps/bo/k;)Z
    .locals 2

    iget-object v0, p0, Lmaps/bo/r;->a:Lmaps/bo/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/bo/n;->b(Lmaps/bo/k;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final d(Lmaps/bo/k;)V
    .locals 2

    iget-object v0, p0, Lmaps/bo/r;->a:Lmaps/bo/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/bo/n;->c(Lmaps/bo/k;Z)V

    return-void
.end method

.method protected final f(Lmaps/bo/k;)Z
    .locals 2

    iget-object v0, p0, Lmaps/bo/r;->a:Lmaps/bo/n;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lmaps/bo/n;->a(Lmaps/bo/k;Z)Z

    move-result v0

    return v0
.end method
