.class public final Lmaps/bo/k;
.super Ljava/lang/Object;


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Lmaps/bo/h;

.field private a:Landroid/content/Context;

.field private b:Landroid/view/MotionEvent;

.field private c:Landroid/view/MotionEvent;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lmaps/bo/e;

.field private final g:Lmaps/bo/e;

.field private final h:Lmaps/bo/e;

.field private final i:Ljava/util/LinkedList;

.field private j:J

.field private k:F

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field private r:F

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmaps/bo/n;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/bo/k;->e:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    iput-object p1, p0, Lmaps/bo/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/bo/k;->A:F

    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v1, Lmaps/bo/v;

    invoke-direct {v1, p2}, Lmaps/bo/v;-><init>(Lmaps/bo/n;)V

    iput-object v1, p0, Lmaps/bo/k;->g:Lmaps/bo/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v1, Lmaps/bo/p;

    invoke-direct {v1, p2}, Lmaps/bo/p;-><init>(Lmaps/bo/n;)V

    iput-object v1, p0, Lmaps/bo/k;->h:Lmaps/bo/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "MD"

    const-string v1, "T"

    invoke-static {v0, v1}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v1, Lmaps/bo/r;

    invoke-direct {v1, p2}, Lmaps/bo/r;-><init>(Lmaps/bo/n;)V

    iput-object v1, p0, Lmaps/bo/k;->f:Lmaps/bo/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v1, Lmaps/bo/x;

    invoke-direct {v1, p2}, Lmaps/bo/x;-><init>(Lmaps/bo/n;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lmaps/bo/h;

    invoke-direct {v0, p1, p2}, Lmaps/bo/h;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lmaps/bo/k;->H:Lmaps/bo/h;

    iget-object v0, p0, Lmaps/bo/k;->H:Lmaps/bo/h;

    invoke-virtual {v0}, Lmaps/bo/h;->a()V

    iget-object v0, p0, Lmaps/bo/k;->H:Lmaps/bo/h;

    invoke-virtual {v0, p2}, Lmaps/bo/h;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    new-instance v1, Lmaps/bo/t;

    invoke-direct {v1, p2}, Lmaps/bo/t;-><init>(Lmaps/bo/n;)V

    iput-object v1, p0, Lmaps/bo/k;->h:Lmaps/bo/e;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "MD"

    const-string v1, "F"

    invoke-static {v0, v1}, Lmaps/br/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/view/MotionEvent;I)F
    .locals 2

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private static a(Lmaps/bo/e;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lmaps/bo/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/view/MotionEvent;I)F
    .locals 2

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, p1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 9

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/bo/k;->j:J

    :cond_0
    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    new-instance v2, Lmaps/bo/a;

    invoke-direct {v2, p1}, Lmaps/bo/a;-><init>(Landroid/view/MotionEvent;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_1

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->e()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->a()J

    move-result-wide v2

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->a()J

    move-result-wide v7

    sub-long v2, v7, v2

    const-wide/16 v7, 0xfa

    cmp-long v0, v2, v7

    if-ltz v0, :cond_2

    move v0, v4

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->e()V

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1

    :cond_3
    sparse-switch v1, :sswitch_data_0

    :goto_2
    move v4, v6

    :sswitch_0
    iget-boolean v0, p0, Lmaps/bo/k;->G:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_3
    :pswitch_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/e;

    invoke-virtual {v0}, Lmaps/bo/e;->a()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v8, Lmaps/bo/l;->a:[I

    iget-wide v1, p0, Lmaps/bo/k;->j:J

    iget-object v3, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    iget-object v5, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-virtual/range {v0 .. v5}, Lmaps/bo/e;->a(JLjava/util/LinkedList;ZLjava/util/List;)Lmaps/bo/f;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bo/f;->ordinal()I

    move-result v1

    aget v1, v8, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_3

    :cond_5
    :pswitch_1
    if-eqz v4, :cond_6

    invoke-direct {p0}, Lmaps/bo/k;->g()V

    iput-boolean v6, p0, Lmaps/bo/k;->G:Z

    :cond_6
    return-void

    :sswitch_1
    iput-boolean v6, p0, Lmaps/bo/k;->G:Z

    goto :goto_2

    :pswitch_2
    invoke-virtual {v0, p0}, Lmaps/bo/e;->a(Lmaps/bo/k;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x106 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 11

    const/high16 v10, 0x3f000000    # 0.5f

    const/high16 v1, -0x40800000    # -1.0f

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    iput v1, p0, Lmaps/bo/k;->r:F

    iput v1, p0, Lmaps/bo/k;->s:F

    iput v1, p0, Lmaps/bo/k;->v:F

    const/4 v0, 0x0

    iput v0, p0, Lmaps/bo/k;->w:F

    iput-boolean v9, p0, Lmaps/bo/k;->E:Z

    iput-boolean v9, p0, Lmaps/bo/k;->F:Z

    iget-object v0, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    sub-float/2addr v3, v1

    sub-float/2addr v4, v2

    sub-float/2addr v7, v5

    sub-float/2addr v8, v6

    iput v3, p0, Lmaps/bo/k;->n:F

    iput v4, p0, Lmaps/bo/k;->o:F

    iput v7, p0, Lmaps/bo/k;->p:F

    iput v8, p0, Lmaps/bo/k;->q:F

    iput v2, p0, Lmaps/bo/k;->t:F

    iput v6, p0, Lmaps/bo/k;->u:F

    mul-float v2, v7, v10

    add-float/2addr v2, v5

    iput v2, p0, Lmaps/bo/k;->k:F

    mul-float v2, v8, v10

    add-float/2addr v2, v6

    iput v2, p0, Lmaps/bo/k;->l:F

    mul-float v2, v3, v10

    add-float/2addr v1, v2

    iput v1, p0, Lmaps/bo/k;->m:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lmaps/bo/k;->y:F

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v1

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/bo/k;->z:F

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/e;

    invoke-virtual {v0, p0}, Lmaps/bo/e;->c(Lmaps/bo/k;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    iput-object v1, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    iput-boolean v0, p0, Lmaps/bo/k;->D:Z

    iput-boolean v0, p0, Lmaps/bo/k;->G:Z

    iget-object v0, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/j;

    invoke-virtual {v0}, Lmaps/bo/j;->e()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/bo/k;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lmaps/bo/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/e;

    invoke-virtual {v0}, Lmaps/bo/e;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, p0}, Lmaps/bo/e;->c(Lmaps/bo/k;)V

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/bo/k;->k:F

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 11

    const/high16 v10, -0x40800000    # -1.0f

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const v1, 0xff00

    and-int/2addr v1, v3

    shr-int/lit8 v4, v1, 0x8

    iget-boolean v1, p0, Lmaps/bo/k;->G:Z

    if-nez v1, :cond_14

    const/4 v1, 0x5

    if-eq v3, v1, :cond_0

    const/16 v1, 0x105

    if-eq v3, v1, :cond_0

    if-nez v3, :cond_9

    :cond_0
    iget-object v1, p0, Lmaps/bo/k;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, Lmaps/bo/k;->A:F

    sub-float/2addr v3, v4

    iput v3, p0, Lmaps/bo/k;->B:F

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    iget v3, p0, Lmaps/bo/k;->A:F

    sub-float/2addr v1, v3

    iput v1, p0, Lmaps/bo/k;->C:F

    invoke-direct {p0}, Lmaps/bo/k;->h()V

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lmaps/bo/k;->c(Landroid/view/MotionEvent;)V

    iget v1, p0, Lmaps/bo/k;->A:F

    iget v4, p0, Lmaps/bo/k;->B:F

    iget v5, p0, Lmaps/bo/k;->C:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lmaps/bo/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lmaps/bo/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    cmpg-float v9, v3, v1

    if-ltz v9, :cond_1

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_1

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_1

    cmpl-float v3, v6, v5

    if-lez v3, :cond_4

    :cond_1
    move v3, v2

    :goto_0
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_2

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_2

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_2

    cmpl-float v1, v8, v5

    if-lez v1, :cond_5

    :cond_2
    move v1, v2

    :goto_1
    if-eqz v3, :cond_6

    if-eqz v1, :cond_6

    iput v10, p0, Lmaps/bo/k;->k:F

    iput v10, p0, Lmaps/bo/k;->l:F

    iput-boolean v2, p0, Lmaps/bo/k;->D:Z

    :cond_3
    :goto_2
    iget-object v0, p0, Lmaps/bo/k;->H:Lmaps/bo/h;

    invoke-virtual {v0, p1}, Lmaps/bo/h;->a(Landroid/view/MotionEvent;)Z

    return v2

    :cond_4
    move v3, v0

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    iput-boolean v2, p0, Lmaps/bo/k;->D:Z

    goto :goto_2

    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    iput-boolean v2, p0, Lmaps/bo/k;->D:Z

    goto :goto_2

    :cond_8
    iput-boolean v2, p0, Lmaps/bo/k;->G:Z

    goto :goto_2

    :cond_9
    const/4 v1, 0x2

    if-ne v3, v1, :cond_11

    iget-boolean v1, p0, Lmaps/bo/k;->D:Z

    if-eqz v1, :cond_11

    iget v1, p0, Lmaps/bo/k;->A:F

    iget v4, p0, Lmaps/bo/k;->B:F

    iget v5, p0, Lmaps/bo/k;->C:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Lmaps/bo/k;->a(Landroid/view/MotionEvent;I)F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {p1, v8}, Lmaps/bo/k;->b(Landroid/view/MotionEvent;I)F

    move-result v8

    cmpg-float v9, v3, v1

    if-ltz v9, :cond_a

    cmpg-float v9, v6, v1

    if-ltz v9, :cond_a

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_a

    cmpl-float v3, v6, v5

    if-lez v3, :cond_c

    :cond_a
    move v3, v2

    :goto_3
    cmpg-float v6, v7, v1

    if-ltz v6, :cond_b

    cmpg-float v1, v8, v1

    if-ltz v1, :cond_b

    cmpl-float v1, v7, v4

    if-gtz v1, :cond_b

    cmpl-float v1, v8, v5

    if-lez v1, :cond_d

    :cond_b
    move v1, v2

    :goto_4
    if-eqz v3, :cond_e

    if-eqz v1, :cond_e

    iput v10, p0, Lmaps/bo/k;->k:F

    iput v10, p0, Lmaps/bo/k;->l:F

    goto/16 :goto_2

    :cond_c
    move v3, v0

    goto :goto_3

    :cond_d
    move v1, v0

    goto :goto_4

    :cond_e
    if-eqz v3, :cond_f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    goto/16 :goto_2

    :cond_f
    if-eqz v1, :cond_10

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    goto/16 :goto_2

    :cond_10
    iput-boolean v0, p0, Lmaps/bo/k;->D:Z

    iput-boolean v2, p0, Lmaps/bo/k;->G:Z

    goto/16 :goto_2

    :cond_11
    const/4 v1, 0x6

    if-eq v3, v1, :cond_12

    const/16 v1, 0x106

    if-eq v3, v1, :cond_12

    if-ne v3, v2, :cond_3

    :cond_12
    iget-boolean v1, p0, Lmaps/bo/k;->D:Z

    if-eqz v1, :cond_3

    if-nez v4, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_13
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    goto/16 :goto_2

    :cond_14
    iget-object v1, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    move v1, v2

    :goto_5
    if-nez v1, :cond_16

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/bo/k;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    :cond_15
    move v1, v0

    goto :goto_5

    :cond_16
    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    invoke-direct {p0, p1}, Lmaps/bo/k;->c(Landroid/view/MotionEvent;)V

    if-nez v4, :cond_17

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_17
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lmaps/bo/k;->k:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->l:F

    iget-boolean v0, p0, Lmaps/bo/k;->D:Z

    if-nez v0, :cond_18

    invoke-direct {p0}, Lmaps/bo/k;->g()V

    :cond_18
    invoke-direct {p0}, Lmaps/bo/k;->h()V

    goto/16 :goto_2

    :sswitch_1
    iget-boolean v0, p0, Lmaps/bo/k;->D:Z

    if-nez v0, :cond_19

    invoke-direct {p0}, Lmaps/bo/k;->g()V

    :cond_19
    invoke-direct {p0}, Lmaps/bo/k;->h()V

    goto/16 :goto_2

    :sswitch_2
    invoke-direct {p0, p1}, Lmaps/bo/k;->c(Landroid/view/MotionEvent;)V

    iget-object v1, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-direct {p0, v1}, Lmaps/bo/k;->b(Landroid/view/MotionEvent;)V

    iget v1, p0, Lmaps/bo/k;->y:F

    iget v3, p0, Lmaps/bo/k;->z:F

    div-float/2addr v1, v3

    const v3, 0x3f2b851f    # 0.67f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    iget-object v1, p0, Lmaps/bo/k;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/bo/e;

    invoke-virtual {v0, p0}, Lmaps/bo/e;->e(Lmaps/bo/k;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_6

    :cond_1a
    if-eqz v1, :cond_3

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x106 -> :sswitch_0
    .end sparse-switch
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/bo/k;->l:F

    return v0
.end method

.method public final c()F
    .locals 1

    iget v0, p0, Lmaps/bo/k;->m:F

    return v0
.end method

.method public final d()F
    .locals 4

    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    iget-object v1, p0, Lmaps/bo/k;->f:Lmaps/bo/e;

    invoke-static {v1}, Lmaps/bo/k;->a(Lmaps/bo/e;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v0, p0, Lmaps/bo/k;->v:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_4

    iget v0, p0, Lmaps/bo/k;->r:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    iget v0, p0, Lmaps/bo/k;->p:F

    iget v1, p0, Lmaps/bo/k;->q:F

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lmaps/bo/k;->r:F

    :cond_2
    iget v0, p0, Lmaps/bo/k;->r:F

    iget v1, p0, Lmaps/bo/k;->s:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_3

    iget v1, p0, Lmaps/bo/k;->n:F

    iget v2, p0, Lmaps/bo/k;->o:F

    mul-float/2addr v1, v1

    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v1, v1

    iput v1, p0, Lmaps/bo/k;->s:F

    :cond_3
    iget v1, p0, Lmaps/bo/k;->s:F

    div-float/2addr v0, v1

    iput v0, p0, Lmaps/bo/k;->v:F

    :cond_4
    iget v0, p0, Lmaps/bo/k;->v:F

    goto :goto_0
.end method

.method public final e()F
    .locals 2

    iget-object v0, p0, Lmaps/bo/k;->g:Lmaps/bo/e;

    invoke-static {v0}, Lmaps/bo/k;->a(Lmaps/bo/e;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lmaps/bo/k;->E:Z

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/bo/k;->u:F

    iget v1, p0, Lmaps/bo/k;->t:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float/2addr v0, v1

    iput v0, p0, Lmaps/bo/k;->w:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bo/k;->E:Z

    :cond_1
    iget v0, p0, Lmaps/bo/k;->w:F

    goto :goto_0
.end method

.method public final f()F
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lmaps/bo/k;->h:Lmaps/bo/e;

    invoke-static {v1}, Lmaps/bo/k;->a(Lmaps/bo/e;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-boolean v0, p0, Lmaps/bo/k;->F:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    iget-object v3, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iget-object v3, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    iget-object v4, p0, Lmaps/bo/k;->c:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lmaps/bo/j;->a(FFFF)F

    move-result v0

    iget-object v1, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget-object v2, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iget-object v3, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    iget-object v4, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget-object v4, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    iget-object v5, p0, Lmaps/bo/k;->b:Landroid/view/MotionEvent;

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lmaps/bo/j;->a(FFFF)F

    move-result v1

    invoke-static {v1, v0}, Lmaps/bo/e;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/bo/k;->x:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/bo/k;->F:Z

    :cond_2
    iget v0, p0, Lmaps/bo/k;->x:F

    goto :goto_0
.end method
