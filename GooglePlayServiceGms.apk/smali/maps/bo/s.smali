.class public final Lmaps/bo/s;
.super Lmaps/bo/b;


# instance fields
.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(ILmaps/bo/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmaps/bo/b;-><init>(ILmaps/bo/k;)V

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/bo/s;->b:F

    return v0
.end method

.method public final a(FF)V
    .locals 2

    const/high16 v1, 0x40000000    # 2.0f

    div-float v0, p1, v1

    iput v0, p0, Lmaps/bo/s;->b:F

    div-float v0, p2, v1

    iput v0, p0, Lmaps/bo/s;->c:F

    return-void
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/bo/s;->c:F

    return v0
.end method

.method public final c()F
    .locals 4

    iget-object v0, p0, Lmaps/bo/s;->a:Lmaps/bo/k;

    invoke-virtual {v0}, Lmaps/bo/k;->c()F

    move-result v0

    iget-object v1, p0, Lmaps/bo/s;->a:Lmaps/bo/k;

    invoke-virtual {v1}, Lmaps/bo/k;->a()F

    move-result v1

    iget-object v2, p0, Lmaps/bo/s;->a:Lmaps/bo/k;

    invoke-virtual {v2}, Lmaps/bo/k;->b()F

    move-result v2

    iget v3, p0, Lmaps/bo/s;->c:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    sub-float v0, v1, v0

    mul-float/2addr v0, v2

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4070000000000000L    # 256.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method
