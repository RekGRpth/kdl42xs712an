.class public final Lmaps/au/l;
.super Lmaps/au/ah;

# interfaces
.implements Lmaps/ay/b;


# instance fields
.field private final q:Lmaps/ac/av;


# direct methods
.method constructor <init>(Lmaps/ac/aw;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V
    .locals 14

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lmaps/au/ah;-><init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/a;Lmaps/ac/a;FFZZLmaps/au/o;Lmaps/au/o;[Lmaps/au/ak;Z)V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/au/l;->q:Lmaps/ac/av;

    return-void
.end method


# virtual methods
.method public final a(FFLmaps/ar/a;)I
    .locals 3

    iget-object v0, p0, Lmaps/au/l;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p3, v0}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    sub-float v1, p1, v1

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public final ap_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b(Lmaps/ar/a;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    iget-object v2, p0, Lmaps/au/l;->a:Lmaps/ac/a;

    invoke-virtual {v2}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {p1, v2}, Lmaps/ar/a;->b(Lmaps/ac/av;)[I

    move-result-object v2

    aget v3, v2, v1

    int-to-float v3, v3

    aget v2, v2, v0

    int-to-float v2, v2

    iget v4, p0, Lmaps/au/l;->e:F

    add-float/2addr v4, v3

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    iget v4, p0, Lmaps/au/l;->f:F

    add-float/2addr v3, v4

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_0

    iget v3, p0, Lmaps/au/l;->g:F

    add-float/2addr v3, v2

    invoke-virtual {p1}, Lmaps/ar/a;->j()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    iget v3, p0, Lmaps/au/l;->h:F

    add-float/2addr v2, v3

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 5

    iget-object v0, p0, Lmaps/au/l;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/au/l;->b:Lmaps/au/o;

    invoke-virtual {v0}, Lmaps/au/o;->b()F

    move-result v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    iget-object v3, p0, Lmaps/au/l;->c:Lmaps/au/o;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lmaps/au/l;->d:Lmaps/au/ak;

    iget-object v3, v3, Lmaps/au/ak;->a:Lmaps/au/aj;

    sget-object v4, Lmaps/au/aj;->b:Lmaps/au/aj;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lmaps/au/l;->c:Lmaps/au/o;

    invoke-virtual {v3}, Lmaps/au/o;->b()F

    move-result v3

    add-float/2addr v0, v3

    :cond_0
    float-to-int v0, v0

    iget-object v3, p0, Lmaps/au/l;->q:Lmaps/ac/av;

    invoke-static {p1, v1, v2, v0, v3}, Lmaps/ay/o;->a(Lmaps/ar/a;Lmaps/ac/av;IILmaps/ac/av;)V

    invoke-super {p0, p1, p2}, Lmaps/au/ah;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v0

    return v0
.end method

.method public final l()V
    .locals 0

    return-void
.end method

.method public final m()V
    .locals 0

    return-void
.end method

.method public final o()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/au/l;->a:Lmaps/ac/a;

    invoke-virtual {v0}, Lmaps/ac/a;->b()Lmaps/ac/av;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lmaps/ac/ad;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final q()Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/au/l;->q:Lmaps/ac/av;

    return-object v0
.end method
