.class public final Lmaps/au/ac;
.super Lmaps/au/m;


# instance fields
.field private A:I

.field private B:I

.field private C:F

.field private final D:I

.field private E:Z

.field private F:I

.field private G:Z

.field private final H:Ljava/lang/String;

.field private final I:F

.field private final J:[F

.field private final a:Ljava/lang/String;

.field private final b:Lmaps/aj/ah;

.field private final c:Lmaps/ac/az;

.field private d:Lmaps/ac/az;

.field private final e:F

.field private f:Lmaps/ac/ay;

.field private g:[Lmaps/au/ad;

.field private final h:Lmaps/aj/ag;

.field private q:Lmaps/as/b;

.field private r:Lmaps/at/i;

.field private s:Lmaps/at/i;

.field private t:Z

.field private u:Lmaps/at/i;

.field private v:Lmaps/at/n;

.field private w:Lmaps/v/e;

.field private final x:F

.field private final y:F

.field private final z:F


# direct methods
.method private constructor <init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/bl;IIZFILmaps/ac/az;FFLmaps/aj/ah;FLmaps/aj/ag;Z)V
    .locals 10

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, -0x40800000    # -1.0f

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move v7, p5

    move/from16 v8, p7

    move/from16 v9, p16

    invoke-direct/range {v1 .. v9}, Lmaps/au/m;-><init>(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/bl;FFIZZ)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/au/ac;->t:Z

    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, Lmaps/au/ac;->J:[F

    iput-object p3, p0, Lmaps/au/ac;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "L"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lmaps/au/ac;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/au/ac;->H:Ljava/lang/String;

    move-object/from16 v0, p10

    iput-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    mul-float v1, p14, p8

    iput v1, p0, Lmaps/au/ac;->e:F

    move/from16 v0, p6

    iput v0, p0, Lmaps/au/ac;->D:I

    move-object/from16 v0, p15

    iput-object v0, p0, Lmaps/au/ac;->h:Lmaps/aj/ag;

    move/from16 v0, p8

    iput v0, p0, Lmaps/au/ac;->x:F

    move/from16 v0, p11

    iput v0, p0, Lmaps/au/ac;->y:F

    move/from16 v0, p12

    iput v0, p0, Lmaps/au/ac;->z:F

    move-object/from16 v0, p13

    iput-object v0, p0, Lmaps/au/ac;->b:Lmaps/aj/ah;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/au/ac;->G:Z

    move/from16 v0, p9

    iput v0, p0, Lmaps/au/ac;->B:I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/au/ac;->F:I

    mul-float v1, p14, p11

    iput v1, p0, Lmaps/au/ac;->I:F

    return-void
.end method

.method private static a(F[FI)I
    .locals 4

    const/4 v3, 0x0

    aget v0, p1, p2

    sub-float v0, p0, v0

    cmpg-float v1, v0, v3

    if-gtz v1, :cond_2

    :cond_0
    :goto_0
    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    move v0, v1

    :cond_2
    cmpl-float v1, v0, v3

    if-lez v1, :cond_3

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge p2, v1, :cond_3

    add-int/lit8 v1, p2, 0x1

    aget v1, p1, v1

    sub-float v1, p0, v1

    cmpg-float v2, v1, v3

    if-gtz v2, :cond_1

    neg-float v1, v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_3
    array-length v0, p1

    add-int/lit8 p2, v0, -0x1

    goto :goto_0
.end method

.method private static a(Lmaps/ac/az;FF)Lmaps/ac/az;
    .locals 12

    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v6, v0, Lmaps/al/l;->g:Lmaps/ac/av;

    iget-object v7, v0, Lmaps/al/l;->h:Lmaps/ac/av;

    iget-object v8, v0, Lmaps/al/l;->i:Lmaps/ac/av;

    iget-object v9, v0, Lmaps/al/l;->j:Lmaps/ac/av;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lmaps/ac/az;->b()I

    move-result v2

    add-int/lit8 v10, v2, -0x1

    const/4 v2, 0x0

    move v3, p2

    :goto_0
    if-ge v2, v10, :cond_0

    invoke-virtual {p0, v2}, Lmaps/ac/az;->b(I)F

    move-result v4

    sub-float/2addr p1, v4

    const v5, 0x38d1b717    # 1.0E-4f

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_1

    const v5, -0x472e48e9    # -1.0E-4f

    cmpg-float v5, p1, v5

    if-gez v5, :cond_0

    const/4 v0, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    div-float v4, p1, v4

    add-float/2addr v4, v5

    invoke-virtual {p0, v2, v9}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5, v8}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v9, v8, v4, v6}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;FLmaps/ac/av;)V

    :cond_0
    move v4, v2

    :goto_1
    if-ge v4, v10, :cond_8

    invoke-virtual {p0, v4}, Lmaps/ac/az;->b(I)F

    move-result v11

    sub-float v5, v3, v11

    const v3, 0x38d1b717    # 1.0E-4f

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_2

    const v3, 0x38d1b717    # 1.0E-4f

    cmpg-float v3, v5, v3

    if-gez v3, :cond_8

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v5, v11

    add-float/2addr v3, v5

    invoke-virtual {p0, v4, v9}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v5, v8}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v9, v8, v3, v7}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;FLmaps/ac/av;)V

    move v3, v1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    sub-int v1, v4, v2

    add-int/lit8 v5, v1, 0x1

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    :goto_3
    add-int/2addr v5, v1

    if-eqz v3, :cond_4

    const/4 v1, 0x1

    :goto_4
    add-int/2addr v1, v5

    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [I

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {v6, v5, v1}, Lmaps/ac/av;->a([II)V

    :goto_5
    move v1, v0

    move v0, v2

    :goto_6
    if-gt v0, v4, :cond_5

    invoke-virtual {p0, v0, v8}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v8, v5, v1}, Lmaps/ac/av;->a([II)V

    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_6

    :cond_1
    sub-float/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v7, v5, v1}, Lmaps/ac/av;->a([II)V

    :cond_6
    invoke-static {v5}, Lmaps/ac/az;->b([I)Lmaps/ac/az;

    move-result-object v0

    return-object v0

    :cond_7
    move v0, v1

    goto :goto_5

    :cond_8
    move v3, v1

    goto :goto_2
.end method

.method public static a(Lmaps/ac/aj;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;ZFLmaps/aj/ah;FLmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;
    .locals 12

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, Lmaps/au/ac;->a(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;IZFFLmaps/aj/ah;Lmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lmaps/ac/bg;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;IZFFLmaps/aj/ah;Lmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;
    .locals 1

    invoke-static/range {p0 .. p11}, Lmaps/au/ac;->a(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;IZFFLmaps/aj/ah;Lmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/ag;Lmaps/ac/az;IZFFLmaps/aj/ah;Lmaps/ar/a;Lmaps/aj/ag;Z)Lmaps/au/ac;
    .locals 20

    invoke-virtual/range {p9 .. p9}, Lmaps/ar/a;->v()F

    move-result v11

    invoke-virtual/range {p3 .. p3}, Lmaps/ac/az;->b()I

    move-result v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_6

    mul-float v3, p6, v11

    const v4, 0x3e4ccccd    # 0.2f

    mul-float/2addr v3, v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lmaps/ac/az;->b(F)Lmaps/ac/az;

    move-result-object v13

    :goto_0
    invoke-interface/range {p0 .. p0}, Lmaps/ac/n;->d()Lmaps/ac/bl;

    move-result-object v7

    const/4 v3, 0x0

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lmaps/ac/ag;->b()I

    move-result v4

    if-ge v3, v4, :cond_0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/ah;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/ah;->j()Lmaps/ac/bl;

    move-result-object v7

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lmaps/ac/ag;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v3

    :goto_2
    move-object/from16 v0, p10

    move-object/from16 v1, p8

    move/from16 v2, p6

    invoke-virtual {v0, v6, v1, v3, v2}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;F)F

    move-result v17

    const/high16 v3, 0x3f800000    # 1.0f

    add-float v3, v3, v17

    invoke-virtual/range {p9 .. p9}, Lmaps/ar/a;->m()F

    move-result v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v4}, Lmaps/ar/a;->a(FF)F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v4, v17, v4

    if-nez v4, :cond_4

    const/4 v3, 0x0

    :cond_1
    :goto_3
    return-object v3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    :cond_4
    invoke-virtual {v13}, Lmaps/ac/az;->d()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    new-instance v3, Lmaps/au/ac;

    invoke-interface/range {p0 .. p0}, Lmaps/ac/n;->f()I

    move-result v8

    invoke-virtual/range {p9 .. p9}, Lmaps/ar/a;->n()F

    move-result v4

    float-to-int v12, v4

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v14, p6

    move/from16 v15, p7

    move-object/from16 v16, p8

    move-object/from16 v18, p10

    move/from16 v19, p11

    invoke-direct/range {v3 .. v19}, Lmaps/au/ac;-><init>(Lmaps/ac/n;Lmaps/am/b;Ljava/lang/String;Lmaps/ac/bl;IIZFILmaps/ac/az;FFLmaps/aj/ah;FLmaps/aj/ag;Z)V

    invoke-direct {v3}, Lmaps/au/ac;->l()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    goto :goto_3

    :cond_6
    move-object/from16 v13, p3

    goto/16 :goto_0
.end method

.method private l()Z
    .locals 12

    const/high16 v11, 0x3e800000    # 0.25f

    const/4 v1, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_0
    iget v0, p0, Lmaps/au/ac;->F:I

    const/4 v4, 0x6

    if-ge v0, v4, :cond_c

    iget v0, p0, Lmaps/au/ac;->F:I

    if-le v0, v3, :cond_0

    iget-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->d()F

    move-result v0

    iget v4, p0, Lmaps/au/ac;->e:F

    mul-float/2addr v4, v10

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v2

    :goto_1
    return v0

    :cond_0
    iget v0, p0, Lmaps/au/ac;->F:I

    const/4 v4, 0x3

    if-le v0, v4, :cond_1

    iget-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->d()F

    move-result v0

    iget v4, p0, Lmaps/au/ac;->e:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    iget v0, p0, Lmaps/au/ac;->F:I

    if-nez v0, :cond_6

    move v4, v2

    :goto_2
    if-ge v4, v5, :cond_5

    iget-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v0, v4}, Lmaps/ac/az;->b(I)F

    move-result v6

    iget v0, p0, Lmaps/au/ac;->e:F

    cmpl-float v0, v6, v0

    if-lez v0, :cond_4

    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v5, v0, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v7, v0, Lmaps/al/l;->b:Lmaps/ac/av;

    iget-object v8, v0, Lmaps/al/l;->c:Lmaps/ac/av;

    iget-object v0, v0, Lmaps/al/l;->d:Lmaps/ac/av;

    iget-object v9, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v9, v4, v8}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget-object v9, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v9, v4, v0}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget v4, p0, Lmaps/au/ac;->e:F

    sub-float v4, v6, v4

    div-float/2addr v4, v6

    mul-float v6, v4, v11

    invoke-static {v8, v0, v6, v5}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;FLmaps/ac/av;)V

    const/high16 v6, 0x3f400000    # 0.75f

    mul-float/2addr v4, v6

    invoke-static {v0, v8, v4, v7}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-static {v5, v7}, Lmaps/ac/az;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/az;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_2

    add-int/lit8 v4, v4, -0x1

    mul-int/lit8 v4, v4, 0x3

    iget-object v5, p0, Lmaps/au/ac;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-le v4, v5, :cond_7

    move-object v0, v1

    :cond_2
    :goto_4
    iput-object v0, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    iget-object v0, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    if-eqz v0, :cond_b

    iget v1, p0, Lmaps/au/ac;->z:F

    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v4, v0, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v5, v0, Lmaps/al/l;->b:Lmaps/ac/av;

    iget-object v6, v0, Lmaps/al/l;->c:Lmaps/ac/av;

    iget-object v7, v0, Lmaps/al/l;->d:Lmaps/ac/av;

    iget-object v0, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v8

    iget v0, p0, Lmaps/au/ac;->x:F

    iget v9, p0, Lmaps/au/ac;->y:F

    mul-float/2addr v0, v9

    mul-float/2addr v0, v1

    div-float v1, v0, v10

    mul-int/lit8 v0, v8, 0x2

    new-array v9, v0, [Lmaps/ac/av;

    iget-object v0, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v0, v2, v7}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move v0, v3

    :goto_5
    if-ge v0, v8, :cond_a

    iget-object v10, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v10, v0, v6}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-static {v6, v7, v4}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v4, v1, v5}, Lmaps/ac/ax;->a(Lmaps/ac/av;FLmaps/ac/av;)V

    invoke-virtual {v6, v5}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v10

    aput-object v10, v9, v0

    mul-int/lit8 v10, v8, 0x2

    sub-int/2addr v10, v0

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v6, v5}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v11

    aput-object v11, v9, v10

    if-ne v0, v3, :cond_3

    invoke-virtual {v7, v5}, Lmaps/ac/av;->f(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v10

    aput-object v10, v9, v2

    mul-int/lit8 v10, v8, 0x2

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v7, v5}, Lmaps/ac/av;->e(Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v11

    aput-object v11, v9, v10

    :cond_3
    invoke-virtual {v7, v6}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    :cond_5
    iput v3, p0, Lmaps/au/ac;->F:I

    :cond_6
    iget-object v0, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-virtual {v0}, Lmaps/ac/az;->d()F

    move-result v0

    iget v4, p0, Lmaps/au/ac;->F:I

    packed-switch v4, :pswitch_data_0

    move-object v0, v1

    goto/16 :goto_3

    :pswitch_0
    iget v4, p0, Lmaps/au/ac;->e:F

    sub-float/2addr v0, v4

    mul-float/2addr v0, v11

    iget v4, p0, Lmaps/au/ac;->e:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-static {v5, v0, v4}, Lmaps/au/ac;->a(Lmaps/ac/az;FF)Lmaps/ac/az;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_1
    iget v4, p0, Lmaps/au/ac;->x:F

    mul-float/2addr v4, v10

    iget v5, p0, Lmaps/au/ac;->y:F

    mul-float/2addr v4, v5

    iget v5, p0, Lmaps/au/ac;->e:F

    sub-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v4, p0, Lmaps/au/ac;->e:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-static {v5, v0, v4}, Lmaps/au/ac;->a(Lmaps/ac/az;FF)Lmaps/ac/az;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_2
    const/4 v4, 0x0

    iget v5, p0, Lmaps/au/ac;->e:F

    sub-float/2addr v0, v5

    iget v5, p0, Lmaps/au/ac;->x:F

    mul-float/2addr v5, v10

    iget v6, p0, Lmaps/au/ac;->y:F

    mul-float/2addr v5, v6

    sub-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v4, p0, Lmaps/au/ac;->e:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-static {v5, v0, v4}, Lmaps/au/ac;->a(Lmaps/ac/az;FF)Lmaps/ac/az;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_3
    iget v4, p0, Lmaps/au/ac;->e:F

    sub-float/2addr v0, v4

    const v4, 0x3ea8f5c3    # 0.33f

    mul-float/2addr v0, v4

    iget v4, p0, Lmaps/au/ac;->e:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-static {v5, v0, v4}, Lmaps/au/ac;->a(Lmaps/ac/az;FF)Lmaps/ac/az;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_4
    iget v4, p0, Lmaps/au/ac;->e:F

    sub-float/2addr v0, v4

    const v4, 0x3f2b851f    # 0.67f

    mul-float/2addr v0, v4

    iget v4, p0, Lmaps/au/ac;->e:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lmaps/au/ac;->c:Lmaps/ac/az;

    invoke-static {v5, v0, v4}, Lmaps/au/ac;->a(Lmaps/ac/az;FF)Lmaps/ac/az;

    move-result-object v0

    goto/16 :goto_3

    :cond_7
    invoke-virtual {v0}, Lmaps/ac/az;->b()I

    move-result v4

    add-int/lit8 v5, v4, -0x1

    const/4 v4, 0x2

    if-lt v5, v4, :cond_9

    invoke-virtual {v0, v2}, Lmaps/ac/az;->d(I)F

    move-result v6

    move v4, v3

    :goto_6
    if-ge v4, v5, :cond_9

    invoke-virtual {v0, v4}, Lmaps/ac/az;->d(I)F

    move-result v7

    sub-float/2addr v7, v6

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x42700000    # 60.0f

    cmpl-float v8, v7, v8

    if-lez v8, :cond_8

    const/high16 v8, 0x43960000    # 300.0f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_8

    move v4, v3

    :goto_7
    if-eqz v4, :cond_2

    move-object v0, v1

    goto/16 :goto_4

    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_9
    move v4, v2

    goto :goto_7

    :cond_a
    new-instance v0, Lmaps/ac/ay;

    invoke-direct {v0, v9}, Lmaps/ac/ay;-><init>([Lmaps/ac/av;)V

    iput-object v0, p0, Lmaps/au/ac;->f:Lmaps/ac/ay;

    iget-object v1, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    sget-object v0, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/al/l;

    iget-object v4, v0, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v0, v0, Lmaps/al/l;->b:Lmaps/ac/av;

    invoke-virtual {v1, v2, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    invoke-virtual {v1, v0}, Lmaps/ac/az;->a(Lmaps/ac/av;)V

    invoke-static {v4, v0}, Lmaps/ac/ax;->b(Lmaps/ac/av;Lmaps/ac/av;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lmaps/au/ac;->A:I

    move v0, v3

    goto/16 :goto_1

    :cond_b
    iget v0, p0, Lmaps/au/ac;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/au/ac;->F:I

    goto/16 :goto_0

    :cond_c
    move v0, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Lmaps/as/a;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/au/m;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ac;->q:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/ac;->q:Lmaps/as/b;

    :cond_0
    iget-object v0, p0, Lmaps/au/ac;->r:Lmaps/at/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/ac;->r:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ac;->s:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    :cond_1
    iget-object v0, p0, Lmaps/au/ac;->v:Lmaps/at/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/au/ac;->v:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    :cond_2
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 18

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/au/ac;->G:Z

    if-nez v1, :cond_11

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    invoke-static {v1, v2}, Lmaps/au/ac;->a(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->i:Lmaps/ac/n;

    invoke-interface {v1}, Lmaps/ac/n;->e()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    sget-object v1, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-ne v2, v1, :cond_2

    invoke-virtual {v3}, Lmaps/ac/bl;->b()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v3}, Lmaps/ac/bl;->b()I

    move-result v1

    const/4 v4, 0x2

    if-gt v1, v4, :cond_2

    invoke-virtual {v3}, Lmaps/ac/bl;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v3, v1}, Lmaps/ac/bl;->b(I)Lmaps/ac/bk;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bk;->b()I

    move-result v7

    invoke-static {v7}, Lmaps/al/d;->a(I)I

    move-result v1

    const/16 v4, 0x80

    if-lt v1, v4, :cond_2

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    invoke-virtual {v1}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v4

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->h:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->b:Lmaps/aj/ah;

    move-object/from16 v0, p0

    iget v5, v0, Lmaps/au/ac;->y:F

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lmaps/aj/ag;->a(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    if-nez v1, :cond_5

    const/16 v1, 0x2710

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lmaps/as/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    :goto_3
    if-nez v1, :cond_11

    :cond_0
    :goto_4
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v3, v2}, Lmaps/au/m;->b(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v7

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/au/ac;->h:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/au/ac;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/au/ac;->b:Lmaps/aj/ah;

    move-object/from16 v0, p0

    iget v13, v0, Lmaps/au/ac;->y:F

    const/16 v16, 0x0

    move-object/from16 v9, p1

    move-object v12, v4

    move v14, v6

    move v15, v7

    invoke-virtual/range {v8 .. v16}, Lmaps/aj/ag;->a(Lmaps/as/a;Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;FIII)Lmaps/as/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->g()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/au/ac;->x:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3fc00000    # 1.5f

    div-float v4, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v1}, Lmaps/ac/az;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_9

    sget-object v1, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/al/l;

    iget-object v2, v1, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v1, v1, Lmaps/al/l;->b:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    const/4 v5, 0x0

    invoke-virtual {v3, v5, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    const/4 v5, 0x1

    invoke-virtual {v3, v5, v1}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    const/4 v3, 0x1

    new-array v3, v3, [Lmaps/au/ad;

    move-object/from16 v0, p0

    iput-object v3, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    const/4 v5, 0x0

    new-instance v6, Lmaps/au/ad;

    const/4 v7, 0x0

    invoke-direct {v6, v2, v1, v4, v7}, Lmaps/au/ad;-><init>(Lmaps/ac/av;Lmaps/ac/av;FB)V

    aput-object v6, v3, v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->e()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v2}, Lmaps/as/b;->f()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iput v1, v3, Lmaps/au/ad;->e:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    iput v2, v1, Lmaps/au/ad;->f:F

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v1, Lmaps/au/ad;->d:F

    const/4 v1, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v3, v3, v1

    iget v3, v3, Lmaps/au/ad;->d:F

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41f00000    # 30.0f

    cmpl-float v4, v3, v4

    if-lez v4, :cond_10

    const/high16 v4, 0x43a50000    # 330.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_10

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/ac;->E:Z

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/au/ac;->m:Z

    if-eqz v1, :cond_8

    new-instance v1, Lmaps/v/e;

    sget-object v2, Lmaps/v/g;->a:Lmaps/v/g;

    invoke-direct {v1, v2}, Lmaps/v/e;-><init>(Lmaps/v/g;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->w:Lmaps/v/e;

    :cond_8
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/ac;->G:Z

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    sget-object v1, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/al/l;

    iget-object v2, v1, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v3, v1, Lmaps/al/l;->b:Lmaps/ac/av;

    invoke-virtual {v5}, Lmaps/ac/az;->b()I

    move-result v6

    add-int/lit8 v7, v6, -0x1

    mul-int/lit8 v1, v7, 0x4

    new-instance v8, Lmaps/at/k;

    const/4 v9, 0x0

    invoke-direct {v8, v1, v9}, Lmaps/at/k;-><init>(IB)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    new-instance v8, Lmaps/at/k;

    const/4 v9, 0x0

    invoke-direct {v8, v1, v9}, Lmaps/at/k;-><init>(IB)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    new-array v1, v7, [Lmaps/au/ad;

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    new-array v8, v6, [F

    const/4 v1, 0x0

    const/4 v9, 0x0

    aput v9, v8, v1

    const/4 v1, 0x0

    invoke-virtual {v5, v1, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v7, :cond_a

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v5, v9, v3}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    new-instance v10, Lmaps/au/ad;

    const/4 v11, 0x0

    invoke-direct {v10, v2, v3, v4, v11}, Lmaps/au/ad;-><init>(Lmaps/ac/av;Lmaps/ac/av;FB)V

    aput-object v10, v9, v1

    invoke-virtual {v2, v3}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v9

    add-int/lit8 v10, v1, 0x1

    aget v11, v8, v1

    add-float/2addr v9, v11

    aput v9, v8, v10

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v17, v3

    move-object v3, v2

    move-object/from16 v2, v17

    goto :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->h:Lmaps/aj/ag;

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/au/ac;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/au/ac;->b:Lmaps/aj/ah;

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->j:Lmaps/ac/bl;

    invoke-virtual {v1}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v1

    :goto_7
    move-object/from16 v0, p0

    iget v5, v0, Lmaps/au/ac;->y:F

    invoke-virtual {v2, v3, v4, v1, v5}, Lmaps/aj/ag;->b(Ljava/lang/String;Lmaps/aj/ah;Lmaps/ac/br;F)[F

    move-result-object v4

    const/high16 v1, 0x3f800000    # 1.0f

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget v2, v4, v2

    div-float v2, v1, v2

    const/4 v1, 0x0

    :goto_8
    array-length v3, v4

    if-ge v1, v3, :cond_c

    aget v3, v4, v1

    mul-float/2addr v3, v2

    aput v3, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_b
    const/4 v1, 0x0

    goto :goto_7

    :cond_c
    const/high16 v1, 0x3f800000    # 1.0f

    array-length v2, v8

    add-int/lit8 v2, v2, -0x1

    aget v2, v8, v2

    div-float v2, v1, v2

    new-array v5, v6, [F

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v6, :cond_d

    aget v3, v8, v1

    mul-float/2addr v3, v2

    aput v3, v8, v1

    sub-int v3, v7, v1

    const/high16 v9, 0x3f800000    # 1.0f

    aget v10, v8, v1

    sub-float/2addr v9, v10

    aput v9, v5, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_d
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_a
    if-ge v1, v6, :cond_e

    aget v7, v8, v1

    invoke-static {v7, v4, v3}, Lmaps/au/ac;->a(F[FI)I

    move-result v3

    aget v7, v4, v3

    aput v7, v8, v1

    aget v7, v5, v1

    invoke-static {v7, v4, v2}, Lmaps/au/ac;->a(F[FI)I

    move-result v2

    aget v7, v4, v2

    aput v7, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->e()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->f()F

    move-result v3

    const/4 v1, 0x0

    :goto_b
    if-ge v1, v6, :cond_6

    aget v4, v8, v1

    mul-float/2addr v4, v2

    sub-int v7, v6, v1

    add-int/lit8 v7, v7, -0x1

    aget v7, v5, v7

    mul-float/2addr v7, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    const/4 v10, 0x0

    invoke-virtual {v9, v4, v10}, Lmaps/at/i;->a(FF)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    invoke-virtual {v9, v4, v3}, Lmaps/at/i;->a(FF)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    invoke-virtual {v9, v7, v3}, Lmaps/at/i;->a(FF)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    const/4 v10, 0x0

    invoke-virtual {v9, v7, v10}, Lmaps/at/i;->a(FF)V

    if-lez v1, :cond_f

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    add-int/lit8 v9, v1, -0x1

    aget-object v7, v7, v9

    iput v4, v7, Lmaps/au/ad;->e:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    add-int/lit8 v7, v1, -0x1

    aget-object v4, v4, v7

    iput v3, v4, Lmaps/au/ad;->f:F

    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    :cond_11
    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->b()I

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    if-lez v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->r()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_18

    move-object/from16 v0, p1

    iget-object v1, v0, Lmaps/as/a;->c:Lmaps/at/g;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/g;->a(Lmaps/as/a;)V

    :goto_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->q:Lmaps/as/b;

    invoke-virtual {v1, v4}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->w:Lmaps/v/e;

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->w:Lmaps/v/e;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/v/e;->a(Lmaps/as/a;)I

    move-result v1

    const/high16 v2, 0x10000

    if-ne v1, v2, :cond_12

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/au/ac;->w:Lmaps/v/e;

    :cond_12
    :goto_d
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    if-eqz v1, :cond_13

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->o()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_13

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->n()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_25

    :cond_13
    sget-object v1, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/al/l;

    iget-object v2, v1, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v1, v1, Lmaps/al/l;->b:Lmaps/ac/av;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v2}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v5, v1}, Lmaps/ac/az;->a(Lmaps/ac/av;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->J:[F

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v5}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->J:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->J:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lmaps/au/ac;->J:[F

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v6}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->J:[F

    const/4 v6, 0x0

    aget v7, v1, v6

    sub-float v2, v7, v2

    aput v2, v1, v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->J:[F

    const/4 v2, 0x1

    aget v6, v1, v2

    sub-float v5, v6, v5

    aput v5, v1, v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->J:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->J:[F

    const/4 v5, 0x1

    aget v5, v2, v5

    mul-float v2, v1, v1

    mul-float v6, v5, v5

    add-float/2addr v2, v6

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v6

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1a

    const/high16 v1, 0x3f800000    # 1.0f

    move v2, v1

    :goto_e
    const/4 v1, 0x0

    cmpl-float v1, v5, v1

    if-ltz v1, :cond_1b

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_f
    const/high16 v7, 0x3f800000    # 1.0f

    mul-float/2addr v5, v1

    div-float/2addr v5, v6

    sub-float v5, v7, v5

    mul-float/2addr v5, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    if-nez v2, :cond_14

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1c

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    :goto_10
    move-object/from16 v0, p0

    iput-object v2, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1d

    const/4 v1, 0x1

    :goto_11
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/ac;->t:Z

    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    if-ne v1, v2, :cond_20

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v1, v5, v1

    if-gez v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    :goto_12
    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    const v1, 0x3a83126f    # 0.001f

    cmpg-float v1, v5, v1

    if-gez v1, :cond_1f

    const/4 v1, 0x1

    :goto_13
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/ac;->t:Z

    :goto_14
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/au/ac;->E:Z

    if-nez v1, :cond_25

    const/high16 v1, 0x3f400000    # 0.75f

    cmpl-float v1, v5, v1

    if-gtz v1, :cond_15

    const/high16 v1, -0x40c00000    # -0.75f

    cmpg-float v1, v5, v1

    if-gez v1, :cond_25

    :cond_15
    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->o()F

    move-result v1

    mul-float/2addr v1, v5

    move v2, v1

    :goto_15
    const/4 v1, 0x0

    move v3, v1

    :goto_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    if-ge v3, v1, :cond_24

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    const/4 v5, 0x1

    if-ne v1, v5, :cond_23

    const/16 v1, 0x1702

    invoke-interface {v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lmaps/au/ac;->t:Z

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v1, v1, v3

    iget v1, v1, Lmaps/au/ad;->e:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v5, v5, v3

    iget v5, v5, Lmaps/au/ad;->f:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-interface {v4, v1, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v1, 0x43340000    # 180.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v4, v1, v5, v6, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v1, v1, v3

    iget v1, v1, Lmaps/au/ad;->e:F

    neg-float v1, v1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v5, v5, v3

    iget v5, v5, Lmaps/au/ad;->f:F

    neg-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    const/4 v6, 0x0

    invoke-interface {v4, v1, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v1, v1, v3

    iget v1, v1, Lmaps/au/ad;->e:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v5, v5, v3

    iget v5, v5, Lmaps/au/ad;->f:F

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v4, v1, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/16 v1, 0x1700

    invoke-interface {v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    :goto_17
    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    aget-object v5, v1, v3

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    sget-object v1, Lmaps/ap/f;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/al/l;

    iget-object v7, v1, Lmaps/al/l;->a:Lmaps/ac/av;

    iget-object v1, v1, Lmaps/al/l;->b:Lmaps/ac/av;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lmaps/ar/a;->a(Lmaps/ac/av;)V

    iget-object v8, v5, Lmaps/au/ad;->a:Lmaps/ac/av;

    invoke-static {v8, v1, v7}, Lmaps/ac/av;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-virtual/range {p2 .. p2}, Lmaps/ar/a;->u()F

    move-result v1

    invoke-virtual {v7}, Lmaps/ac/av;->f()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-virtual {v7}, Lmaps/ac/av;->g()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v1

    invoke-virtual {v7}, Lmaps/ac/av;->h()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v1

    invoke-interface {v6, v8, v9, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/high16 v7, 0x42b40000    # 90.0f

    iget v8, v5, Lmaps/au/ad;->d:F

    sub-float/2addr v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v6, v7, v8, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    const/4 v7, 0x0

    cmpl-float v7, v2, v7

    if-eqz v7, :cond_17

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface {v6, v2, v7, v8, v9}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_17
    iget v7, v5, Lmaps/au/ad;->b:F

    mul-float/2addr v7, v1

    iget v5, v5, Lmaps/au/ad;->c:F

    mul-float/2addr v1, v5

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v6, v7, v1, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    const/4 v1, 0x5

    const/4 v5, 0x0

    const/4 v7, 0x4

    invoke-interface {v6, v1, v5, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_16

    :cond_18
    move-object/from16 v0, p1

    iget-object v1, v0, Lmaps/as/a;->f:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    goto/16 :goto_c

    :cond_19
    move-object/from16 v0, p0

    iget v1, v0, Lmaps/au/ac;->p:I

    goto/16 :goto_d

    :cond_1a
    const/high16 v1, -0x40800000    # -1.0f

    move v2, v1

    goto/16 :goto_e

    :cond_1b
    const/high16 v1, -0x40800000    # -1.0f

    goto/16 :goto_f

    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    goto/16 :goto_10

    :cond_1d
    const/4 v1, 0x0

    goto/16 :goto_11

    :cond_1e
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    goto/16 :goto_12

    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_13

    :cond_20
    const v1, -0x457ced91    # -0.001f

    cmpg-float v1, v5, v1

    if-gez v1, :cond_21

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->s:Lmaps/at/i;

    :goto_18
    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    const v1, -0x457ced91    # -0.001f

    cmpg-float v1, v5, v1

    if-gez v1, :cond_22

    const/4 v1, 0x1

    :goto_19
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/ac;->t:Z

    goto/16 :goto_14

    :cond_21
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->r:Lmaps/at/i;

    goto :goto_18

    :cond_22
    const/4 v1, 0x0

    goto :goto_19

    :cond_23
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->u:Lmaps/at/i;

    mul-int/lit8 v5, v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v5}, Lmaps/at/i;->a(Lmaps/as/a;I)V

    goto/16 :goto_17

    :cond_24
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/ac;->g:[Lmaps/au/ad;

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/16 v1, 0x1702

    invoke-interface {v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/16 v1, 0x1700

    invoke-interface {v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_4

    :cond_25
    move v2, v3

    goto/16 :goto_15
.end method

.method public final a(Lmaps/ac/cy;)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v1, v0}, Lmaps/ac/az;->a(I)Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmaps/ac/cy;->a(Lmaps/ac/av;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/au/ac;->d:Lmaps/ac/az;

    invoke-virtual {v1}, Lmaps/ac/az;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmaps/ac/cy;->a(Lmaps/ac/av;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final a(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    iget v0, p0, Lmaps/au/ac;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/au/ac;->F:I

    invoke-direct {p0}, Lmaps/au/ac;->l()Z

    move-result v0

    return v0
.end method

.method public final aj_()F
    .locals 1

    iget v0, p0, Lmaps/au/ac;->I:F

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/au/ac;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    invoke-super {p0, p1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ac;->r:Lmaps/at/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/ac;->r:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/ac;->s:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/ac;->v:Lmaps/at/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/ac;->v:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    :cond_1
    return-void
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->v()F

    move-result v0

    iget v3, p0, Lmaps/au/ac;->x:F

    div-float v3, v0, v3

    iget-boolean v0, p0, Lmaps/au/m;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/au/ac;->o:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3e800000    # 0.25f

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3}, Lmaps/au/ac;->a(F)I

    move-result v3

    iput v3, p0, Lmaps/au/ac;->p:I

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lmaps/ar/a;->n()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lmaps/au/ac;->B:I

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v0

    iput v0, p0, Lmaps/au/ac;->C:F

    :goto_2
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const v0, 0x3f666666    # 0.9f

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_2

    const/high16 v0, 0x3fa00000    # 1.25f

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_2

    move v0, v1

    :goto_3
    const/high16 v3, 0x10000

    iput v3, p0, Lmaps/au/ac;->p:I

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final c()Lmaps/ac/be;
    .locals 1

    iget-object v0, p0, Lmaps/au/ac;->f:Lmaps/ac/ay;

    return-object v0
.end method

.method public final d()I
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, Lmaps/au/ac;->l:I

    iget v0, p0, Lmaps/au/ac;->F:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    iget v2, p0, Lmaps/au/ac;->C:F

    const/high16 v3, 0x41f00000    # 30.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget v1, p0, Lmaps/au/ac;->B:I

    iget v2, p0, Lmaps/au/ac;->A:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lmaps/au/ac;->D:I

    int-to-float v2, v2

    int-to-float v1, v1

    const v3, 0x3c8efa35

    mul-float/2addr v1, v3

    invoke-static {v1}, Landroid/util/FloatMath;->sin(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    mul-float/2addr v1, v2

    float-to-int v1, v1

    goto :goto_1
.end method
