.class final Lmaps/au/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/at/n;

.field private final b:Lmaps/at/d;

.field private final c:Lmaps/at/i;

.field private final d:Lmaps/at/n;

.field private final e:Lmaps/at/i;

.field private final f:I


# direct methods
.method public constructor <init>(Lmaps/ac/bd;Ljava/util/List;Ljava/util/List;Lmaps/au/d;)V
    .locals 13

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lmaps/al/h;->a(Ljava/util/List;)I

    move-result v1

    new-instance v2, Lmaps/at/p;

    invoke-direct {v2, v1}, Lmaps/at/p;-><init>(I)V

    iput-object v2, p0, Lmaps/au/c;->a:Lmaps/at/n;

    new-instance v2, Lmaps/at/k;

    invoke-direct {v2, v1}, Lmaps/at/k;-><init>(I)V

    iput-object v2, p0, Lmaps/au/c;->c:Lmaps/at/i;

    new-instance v1, Lmaps/at/f;

    invoke-static {p2}, Lmaps/al/h;->b(Ljava/util/List;)I

    move-result v2

    invoke-direct {v1, v2}, Lmaps/at/f;-><init>(I)V

    iput-object v1, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/az;

    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/av;->f()I

    move-result v2

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->d()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/av;->g()I

    move-result v3

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->e()F

    move-result v4

    float-to-int v4, v4

    add-int/2addr v3, v4

    new-instance v5, Lmaps/ac/av;

    invoke-direct {v5, v2, v3}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {p1}, Lmaps/ac/bd;->f()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v7, 0x10000

    :goto_1
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v8, 0x0

    :goto_2
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->g()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_8

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->g()F

    move-result v2

    invoke-virtual {v1, v2}, Lmaps/ac/az;->c(F)Lmaps/ac/az;

    move-result-object v2

    :goto_3
    invoke-static {}, Lmaps/al/h;->a()Lmaps/al/h;

    move-result-object v1

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->b()F

    move-result v3

    :goto_4
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->a()F

    move-result v4

    :goto_5
    iget-object v9, p0, Lmaps/au/c;->a:Lmaps/at/n;

    iget-object v10, p0, Lmaps/au/c;->b:Lmaps/at/d;

    iget-object v11, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual/range {v1 .. v11}, Lmaps/al/h;->a(Lmaps/ac/az;FFLmaps/ac/av;IIILmaps/at/o;Lmaps/at/e;Lmaps/at/j;)V

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    goto :goto_1

    :cond_1
    const/high16 v8, 0x10000

    goto :goto_2

    :cond_2
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->a()F

    move-result v3

    goto :goto_4

    :cond_3
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->b()F

    move-result v4

    goto :goto_5

    :cond_4
    const/4 v1, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v1

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/f;

    invoke-static {v1}, Lmaps/au/a;->a(Lmaps/ac/f;)I

    move-result v1

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_6

    :cond_5
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    if-lez v2, :cond_6

    new-instance v1, Lmaps/at/p;

    invoke-direct {v1, v2}, Lmaps/at/p;-><init>(I)V

    iput-object v1, p0, Lmaps/au/c;->d:Lmaps/at/n;

    new-instance v1, Lmaps/at/k;

    invoke-direct {v1, v2}, Lmaps/at/k;-><init>(I)V

    iput-object v1, p0, Lmaps/au/c;->e:Lmaps/at/i;

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/f;

    move-object/from16 v0, p4

    invoke-direct {p0, p1, v1, v0}, Lmaps/au/c;->a(Lmaps/ac/bd;Lmaps/ac/f;Lmaps/au/d;)V

    goto :goto_7

    :cond_6
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/c;->d:Lmaps/at/n;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/au/c;->e:Lmaps/at/i;

    :cond_7
    invoke-virtual/range {p4 .. p4}, Lmaps/au/d;->h()I

    move-result v1

    iput v1, p0, Lmaps/au/c;->f:I

    return-void

    :cond_8
    move-object v2, v1

    goto/16 :goto_3
.end method

.method private a(Lmaps/ac/bd;Lmaps/ac/f;Lmaps/au/d;)V
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p2}, Lmaps/ac/f;->d()Lmaps/ac/bl;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/bl;->c()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lmaps/ac/f;->b()Lmaps/ac/cj;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/cj;->a()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/av;->f()I

    move-result v1

    invoke-virtual {p3}, Lmaps/au/d;->d()F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/av;->g()I

    move-result v2

    invoke-virtual {p3}, Lmaps/au/d;->e()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v3, v2

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2, v1, v3}, Lmaps/ac/av;-><init>(II)V

    invoke-virtual {p1}, Lmaps/ac/bd;->f()I

    move-result v9

    invoke-static {}, Lmaps/au/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/ac/av;

    aget-object v3, v1, v6

    invoke-static {}, Lmaps/au/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/ac/av;

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-static {}, Lmaps/au/a;->c()Ljava/lang/ThreadLocal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lmaps/ac/av;

    const/4 v5, 0x2

    aget-object v5, v1, v5

    move v1, v6

    move v7, v6

    :goto_1
    if-ge v1, v8, :cond_2

    invoke-virtual/range {v0 .. v5}, Lmaps/ac/cj;->a(ILmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)V

    iget-object v10, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v10, v3, v9}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v10, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v10, v4, v9}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    iget-object v10, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v10, v5, v9}, Lmaps/at/n;->a(Lmaps/ac/av;I)V

    add-int/lit8 v7, v7, 0x3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p3}, Lmaps/au/d;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x10000

    :goto_2
    iget-object v1, p0, Lmaps/au/c;->e:Lmaps/at/i;

    invoke-virtual {v1, v0, v6, v7}, Lmaps/at/i;->a(III)V

    goto :goto_0

    :cond_3
    move v0, v6

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/au/c;->a:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    iget-object v2, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-virtual {v2}, Lmaps/at/d;->c()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual {v2}, Lmaps/at/i;->b()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->c()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/au/c;->e:Lmaps/at/i;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lmaps/au/c;->e:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->b()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 5

    const/4 v4, 0x4

    const/high16 v3, 0x10000

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    iget v1, p0, Lmaps/au/c;->f:I

    invoke-static {v0, v1}, Lmaps/al/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    invoke-virtual {p1}, Lmaps/as/a;->a()Lmaps/al/o;

    move-result-object v0

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lmaps/al/o;->a(I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/au/c;->a:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-virtual {v0, p1, v4}, Lmaps/at/d;->a(Lmaps/as/a;I)V

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/c;->e:Lmaps/at/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->e:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v2}, Lmaps/at/n;->a()I

    move-result v2

    invoke-interface {v0, v4, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    return-void
.end method

.method public final b()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/au/c;->a:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x24

    iget-object v2, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-virtual {v2}, Lmaps/at/d;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual {v2}, Lmaps/at/i;->c()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/c;->d:Lmaps/at/n;

    invoke-virtual {v0}, Lmaps/at/n;->d()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lmaps/au/c;->e:Lmaps/at/i;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lmaps/au/c;->e:Lmaps/at/i;

    invoke-virtual {v1}, Lmaps/at/i;->c()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/c;->a:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    return-void
.end method

.method public final c(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/c;->a:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->b:Lmaps/at/d;

    invoke-virtual {v0, p1}, Lmaps/at/d;->c(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/c;->c:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    return-void
.end method
