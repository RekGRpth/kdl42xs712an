.class public abstract Lmaps/au/m;
.super Lmaps/ay/u;


# instance fields
.field private a:F

.field private b:F

.field private c:Z

.field private d:Ljava/util/List;

.field protected final i:Lmaps/ac/n;

.field protected final j:Lmaps/ac/bl;

.field protected final k:Lmaps/am/b;

.field protected final l:I

.field protected m:Z

.field n:Z

.field protected o:Z

.field protected p:I


# direct methods
.method protected constructor <init>(Lmaps/ac/n;Lmaps/am/b;Lmaps/ac/bl;FFIZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    iput-boolean v0, p0, Lmaps/au/m;->n:Z

    iput-boolean v0, p0, Lmaps/au/m;->o:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/au/m;->d:Ljava/util/List;

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/au/m;->p:I

    iput-object p1, p0, Lmaps/au/m;->i:Lmaps/ac/n;

    iput-object p3, p0, Lmaps/au/m;->j:Lmaps/ac/bl;

    iput-object p2, p0, Lmaps/au/m;->k:Lmaps/am/b;

    iput p4, p0, Lmaps/au/m;->a:F

    iput p5, p0, Lmaps/au/m;->b:F

    iput p6, p0, Lmaps/au/m;->l:I

    iput-boolean p7, p0, Lmaps/au/m;->c:Z

    iput-boolean p8, p0, Lmaps/au/m;->n:Z

    return-void
.end method

.method public static a(Lmaps/ac/bl;FIIF)F
    .locals 3

    invoke-virtual {p0}, Lmaps/ac/bl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/br;->f()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    mul-float/2addr v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, p4

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(F)I
    .locals 6

    const/high16 v4, 0x3e800000    # 0.25f

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    cmpl-float v0, p0, v4

    if-ltz v0, :cond_0

    const-wide/high16 v0, 0x40f0000000000000L    # 65536.0

    const-wide v2, 0x3ff5555560000000L    # 1.3333333730697632

    sub-float v4, p0, v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x10000

    goto :goto_0
.end method

.method private static a(II)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x18

    mul-int/2addr v0, p1

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public static a(Lmaps/ac/bl;Lmaps/ap/b;)I
    .locals 3

    const/high16 v0, -0x1000000

    sget-object v1, Lmaps/au/n;->a:[I

    invoke-virtual {p1}, Lmaps/ap/b;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p0}, Lmaps/ac/bl;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/br;->d()I

    move-result v1

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return v0

    :pswitch_0
    const/4 v0, -0x1

    goto :goto_1

    :pswitch_1
    const v0, -0x3f3f40

    goto :goto_1

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static b(I)I
    .locals 3

    const/16 v0, 0xff

    invoke-static {p0, v0}, Lmaps/au/m;->a(II)I

    move-result v0

    invoke-static {p0}, Lmaps/au/m;->c(I)I

    move-result v1

    const/16 v2, 0xc0

    if-lt v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method public static b(Lmaps/ac/bl;Lmaps/ap/b;)I
    .locals 3

    sget-object v0, Lmaps/au/n;->a:[I

    invoke-virtual {p1}, Lmaps/ap/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lmaps/ac/bl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/br;->e()I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/high16 v0, -0x60000000

    goto :goto_0

    :pswitch_1
    const/high16 v0, -0x80000000

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lmaps/au/m;->a(Lmaps/ac/bl;Lmaps/ap/b;)I

    move-result v0

    const/16 v1, 0xa0

    invoke-static {v0, v1}, Lmaps/au/m;->a(II)I

    move-result v1

    invoke-static {v0}, Lmaps/au/m;->c(I)I

    move-result v0

    const/16 v2, 0xc0

    if-lt v0, v2, :cond_1

    const v0, 0x808080

    or-int/2addr v0, v1

    goto :goto_0

    :cond_1
    const v0, 0xffffff

    or-int/2addr v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static c(I)I
    .locals 2

    ushr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x4d

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    mul-int/lit16 v1, v1, 0x97

    add-int/2addr v0, v1

    and-int/lit16 v1, p0, 0xff

    mul-int/lit8 v1, v1, 0x1c

    add-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/au/m;->o:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/as/a;)V
    .locals 0

    invoke-super {p0, p1}, Lmaps/ay/u;->a(Lmaps/as/a;)V

    return-void
.end method

.method public a(Lmaps/ac/cy;)Z
    .locals 2

    invoke-virtual {p1}, Lmaps/ac/cy;->a()Lmaps/ac/cx;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/cx;->a(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/au/m;->c()Lmaps/ac/be;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmaps/ac/cy;->a(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract aj_()F
.end method

.method public final ak_()Lmaps/am/b;
    .locals 1

    iget-object v0, p0, Lmaps/au/m;->k:Lmaps/am/b;

    return-object v0
.end method

.method public final al_()F
    .locals 1

    iget v0, p0, Lmaps/au/m;->b:F

    return v0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public b(Lmaps/as/a;)V
    .locals 0

    invoke-super {p0, p1}, Lmaps/ay/u;->b(Lmaps/as/a;)V

    return-void
.end method

.method public abstract c()Lmaps/ac/be;
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmaps/au/m;->l:I

    return v0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->i:Lmaps/ay/v;

    return-object v0
.end method

.method public final h()F
    .locals 1

    iget v0, p0, Lmaps/au/m;->a:F

    return v0
.end method

.method public final j()Lmaps/ac/n;
    .locals 1

    iget-object v0, p0, Lmaps/au/m;->i:Lmaps/ac/n;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/au/m;->c:Z

    return v0
.end method

.method public final n()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/m;->m:Z

    return-void
.end method
