.class public final Lmaps/au/aq;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/h;


# static fields
.field static final a:Lmaps/au/aq;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/au/aq;

    invoke-direct {v0}, Lmaps/au/aq;-><init>()V

    sput-object v0, Lmaps/au/aq;->a:Lmaps/au/aq;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lmaps/as/a;Lmaps/ap/c;)V
    .locals 3

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {p1}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/ad;->a()Lmaps/ay/u;

    move-result-object v0

    check-cast v0, Lmaps/ay/ap;

    invoke-virtual {v0}, Lmaps/ay/ap;->l()Lmaps/ao/b;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ao/b;->f()Z

    move-result v2

    sput-boolean v2, Lmaps/au/aq;->b:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lmaps/ao/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/high16 v0, 0x40a00000    # 5.0f

    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidth(F)V

    iget-object v0, p0, Lmaps/as/a;->g:Lmaps/at/n;

    invoke-virtual {v0, p0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    :cond_0
    return-void
.end method

.method public static c(Lmaps/as/a;)V
    .locals 4

    const/high16 v1, 0x10000

    invoke-virtual {p0}, Lmaps/as/a;->B()V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v1, v1, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    iget-object v0, p0, Lmaps/as/a;->d:Lmaps/at/n;

    invoke-virtual {v0, p0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-virtual {p0}, Lmaps/as/a;->q()V

    invoke-virtual {p0}, Lmaps/as/a;->s()V

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-virtual {p0}, Lmaps/as/a;->C()V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/as/a;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    sget-boolean v0, Lmaps/au/aq;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    :cond_0
    return-void
.end method

.method public final b(Lmaps/as/a;)V
    .locals 0

    return-void
.end method
