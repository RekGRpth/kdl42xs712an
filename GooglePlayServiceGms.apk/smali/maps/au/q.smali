.class public final Lmaps/au/q;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/u;


# instance fields
.field private final a:Landroid/graphics/Bitmap;

.field private final b:Lmaps/as/c;

.field private final c:I

.field private final d:I


# direct methods
.method constructor <init>(Landroid/graphics/Bitmap;FLmaps/as/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lmaps/au/q;->b:Lmaps/as/c;

    iget-object v0, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lmaps/au/q;->c:I

    iget-object v0, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lmaps/au/q;->d:I

    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/au/q;->c:I

    int-to-float v0, v0

    return v0
.end method

.method public final a(Lmaps/ap/b;)Lmaps/as/b;
    .locals 2

    iget-object v0, p0, Lmaps/au/q;->b:Lmaps/as/c;

    iget-object v1, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/as/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmaps/as/b;->h()V

    :cond_0
    return-object v0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/b;)Lmaps/as/b;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lmaps/au/q;->b:Lmaps/as/c;

    iget-object v1, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/as/c;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/as/b;

    invoke-direct {v0, p1, v2}, Lmaps/as/b;-><init>(Lmaps/as/a;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->c(Z)V

    invoke-virtual {v0, v2}, Lmaps/as/b;->d(Z)V

    invoke-static {}, Lmaps/bb/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lmaps/bb/b;->c:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v1, p0, Lmaps/au/q;->b:Lmaps/as/c;

    iget-object v2, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0}, Lmaps/as/c;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Lmaps/as/b;->h()V

    return-object v0

    :cond_1
    iget-object v1, p0, Lmaps/au/q;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lmaps/as/b;->b(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/au/q;->d:I

    int-to-float v0, v0

    return v0
.end method

.method public final c()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    iget v0, p0, Lmaps/au/q;->d:I

    int-to-float v0, v0

    return v0
.end method
