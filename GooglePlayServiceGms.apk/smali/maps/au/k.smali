.class public final Lmaps/au/k;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/ap;


# instance fields
.field private final a:Lmaps/ac/bd;

.field private final b:Lmaps/ac/bt;

.field private final c:Lmaps/ao/b;

.field private d:[Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:I

.field private g:Lmaps/au/al;

.field private final h:[F

.field private i:J

.field private j:Lmaps/au/m;


# direct methods
.method private constructor <init>(Lmaps/ac/bt;Lmaps/ao/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/au/k;->h:[F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/au/k;->i:J

    iput-object p1, p0, Lmaps/au/k;->b:Lmaps/ac/bt;

    iput-object p2, p0, Lmaps/au/k;->c:Lmaps/ao/b;

    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/au/k;->a:Lmaps/ac/bd;

    return-void
.end method

.method public static a(Lmaps/ac/bs;Lmaps/as/a;)Lmaps/au/k;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lmaps/au/k;

    invoke-interface {p0}, Lmaps/ac/bs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-interface {p0}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lmaps/au/k;-><init>(Lmaps/ac/bt;Lmaps/ao/b;)V

    instance-of v1, p0, Lmaps/ac/y;

    if-eqz v1, :cond_0

    check-cast p0, Lmaps/ac/y;

    invoke-virtual {p0}, Lmaps/ac/y;->m()[B

    move-result-object v1

    iget-object v2, v0, Lmaps/au/k;->b:Lmaps/ac/bt;

    invoke-static {v1}, Lmaps/au/al;->a([B)Lmaps/au/al;

    move-result-object v1

    iput-object v1, v0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {p0}, Lmaps/ac/y;->i()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmaps/au/k;->d:[Ljava/lang/String;

    invoke-virtual {p0}, Lmaps/ac/y;->j()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lmaps/au/k;->e:[Ljava/lang/String;

    invoke-virtual {p0}, Lmaps/ac/y;->k()I

    move-result v1

    iput v1, v0, Lmaps/au/k;->f:I

    invoke-virtual {p0}, Lmaps/ac/y;->l()Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    new-array v1, v3, [Ljava/lang/String;

    iput-object v1, v0, Lmaps/au/k;->d:[Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/String;

    iput-object v1, v0, Lmaps/au/k;->e:[Ljava/lang/String;

    const/4 v1, -0x1

    iput v1, v0, Lmaps/au/k;->f:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lmaps/ar/a;Lmaps/ap/b;)I
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lmaps/ap/f;->a:Z

    if-eqz v1, :cond_0

    const v0, 0x8000

    :cond_0
    iget-object v1, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    :cond_1
    return v0
.end method

.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->b:Lmaps/ac/bt;

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 4

    iget-object v1, p0, Lmaps/au/k;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lmaps/am/b;)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0, p1}, Lmaps/au/al;->a(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    invoke-virtual {v0, p1}, Lmaps/au/m;->a(Lmaps/as/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    :cond_1
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-nez v0, :cond_0

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-ne v0, v6, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {p2}, Lmaps/ar/a;->e()J

    move-result-wide v2

    iget-wide v4, p0, Lmaps/au/k;->i:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lmaps/ar/a;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lmaps/au/k;->i:J

    iget-object v0, p0, Lmaps/au/k;->a:Lmaps/ac/bd;

    invoke-virtual {v0}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {p2}, Lmaps/ar/a;->h()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_1

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v2

    invoke-virtual {p2}, Lmaps/ar/a;->p()F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    iget-object v2, p1, Lmaps/as/a;->h:[F

    invoke-virtual {p2, v0, v2}, Lmaps/ar/a;->a(Lmaps/ac/av;[F)V

    iget-object v0, p1, Lmaps/as/a;->h:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p1, Lmaps/as/a;->h:[F

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v0

    :cond_1
    iget-object v2, p0, Lmaps/au/k;->a:Lmaps/ac/bd;

    invoke-virtual {v2}, Lmaps/ac/bd;->f()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lmaps/au/k;->h:[F

    invoke-static {p1, p2, v0, v2, v3}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F[F)V

    :cond_2
    iget-object v0, p0, Lmaps/au/k;->h:[F

    invoke-static {v1, v0}, Lmaps/ap/o;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-ne v0, v6, :cond_4

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/al;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0

    :cond_4
    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    const/16 v2, 0xf

    if-ne v0, v2, :cond_3

    sget-object v0, Lmaps/au/aq;->a:Lmaps/au/aq;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/aq;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto :goto_1
.end method

.method public final a(Lmaps/au/m;)V
    .locals 0

    iput-object p1, p0, Lmaps/au/k;->j:Lmaps/au/m;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final a(Lmaps/aj/ac;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final b(Ljava/util/Collection;)V
    .locals 4

    iget-object v1, p0, Lmaps/au/k;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-interface {p1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0, p1}, Lmaps/au/al;->b(Lmaps/as/a;)V

    :cond_0
    iget-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    invoke-virtual {v0, p1}, Lmaps/au/m;->b(Lmaps/as/a;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    :cond_1
    return-void
.end method

.method public final b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 0

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    invoke-static {p1}, Lmaps/au/al;->c(Lmaps/as/a;)V

    return-void
.end method

.method public final c()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->c:Lmaps/ao/b;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0}, Lmaps/au/al;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0}, Lmaps/au/al;->d()V

    :cond_0
    return-void
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lmaps/au/k;->f:I

    return v0
.end method

.method public final i()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final j()I
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0}, Lmaps/au/al;->a()I

    move-result v0

    goto :goto_0
.end method

.method public final k()I
    .locals 2

    const/16 v0, 0x88

    iget-object v1, p0, Lmaps/au/k;->g:Lmaps/au/al;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/au/k;->g:Lmaps/au/al;

    invoke-virtual {v0}, Lmaps/au/al;->b()I

    move-result v0

    add-int/lit16 v0, v0, 0x88

    :cond_0
    return v0
.end method

.method public final l()Lmaps/au/m;
    .locals 1

    iget-object v0, p0, Lmaps/au/k;->j:Lmaps/au/m;

    return-object v0
.end method
