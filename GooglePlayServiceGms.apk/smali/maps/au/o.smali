.class public final Lmaps/au/o;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/au/h;


# instance fields
.field private a:Lmaps/au/s;

.field private final b:Lmaps/au/v;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Lmaps/at/o;

.field private final g:Lmaps/at/j;

.field private final h:Lmaps/al/j;

.field private final i:Lmaps/at/g;

.field private j:F

.field private k:F

.field private l:F

.field private m:F

.field private n:Z

.field private o:Z


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Lmaps/au/s;Lmaps/au/v;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lmaps/au/o;->e:Ljava/util/ArrayList;

    new-instance v0, Lmaps/al/j;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v0, v1}, Lmaps/al/j;-><init>(I)V

    iput-object v0, p0, Lmaps/au/o;->h:Lmaps/al/j;

    new-instance v0, Lmaps/at/g;

    iget-object v1, p0, Lmaps/au/o;->h:Lmaps/al/j;

    invoke-virtual {v1}, Lmaps/al/j;->d()I

    move-result v1

    invoke-direct {v0, v1}, Lmaps/at/g;-><init>(I)V

    iput-object v0, p0, Lmaps/au/o;->i:Lmaps/at/g;

    iget-object v0, p0, Lmaps/au/o;->h:Lmaps/al/j;

    iput-object v0, p0, Lmaps/au/o;->f:Lmaps/at/o;

    iget-object v0, p0, Lmaps/au/o;->h:Lmaps/al/j;

    iput-object v0, p0, Lmaps/au/o;->g:Lmaps/at/j;

    iput-object p2, p0, Lmaps/au/o;->a:Lmaps/au/s;

    iput-object p3, p0, Lmaps/au/o;->b:Lmaps/au/v;

    invoke-direct {p0}, Lmaps/au/o;->d()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/au/o;->n:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/o;->o:Z

    return-void
.end method

.method public static a(Lmaps/ac/ag;Lmaps/ac/n;Lmaps/ar/a;Lmaps/as/c;Lmaps/aj/ag;Lmaps/av/a;)Lmaps/au/o;
    .locals 9

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    move v6, v0

    move-object v7, v1

    :goto_0
    invoke-virtual {p0}, Lmaps/ac/ag;->b()I

    move-result v0

    if-ge v6, v0, :cond_b

    invoke-virtual {p0, v6}, Lmaps/ac/ag;->a(I)Lmaps/ac/ah;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/ah;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lmaps/ac/ah;->j()Lmaps/ac/bl;

    move-result-object v4

    invoke-virtual {v1}, Lmaps/ac/ah;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lmaps/ac/bl;->e()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v5, p5, Lmaps/av/a;->d:Lmaps/aj/ah;

    instance-of v0, p1, Lmaps/ac/bg;

    if-eqz v0, :cond_5

    iget-object v5, p5, Lmaps/av/a;->a:Lmaps/aj/ah;

    :cond_1
    :goto_2
    new-instance v0, Lmaps/au/t;

    invoke-virtual {v1}, Lmaps/ac/ah;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lmaps/ar/a;->k()F

    move-result v1

    invoke-static {v4, p5, v1}, Lmaps/au/ah;->a(Lmaps/ac/bl;Lmaps/av/a;F)I

    move-result v3

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, Lmaps/au/t;-><init>(Lmaps/aj/ag;Ljava/lang/String;ILmaps/ac/bl;Lmaps/aj/ah;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object v1, v7

    :goto_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v7, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/br;->d()I

    move-result v0

    invoke-virtual {v4}, Lmaps/ac/bl;->i()Lmaps/ac/br;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/br;->f()I

    move-result v2

    if-lez v2, :cond_4

    const/high16 v2, -0x1000000

    and-int/2addr v0, v2

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    instance-of v0, p1, Lmaps/ac/aj;

    if-eqz v0, :cond_1

    iget-object v5, p5, Lmaps/av/a;->h:Lmaps/aj/ah;

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Lmaps/ac/ah;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lmaps/ai/c;->c()Lmaps/ai/c;

    move-result-object v0

    invoke-virtual {v1}, Lmaps/ac/ah;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lmaps/ai/c;->a(Ljava/lang/String;Lmaps/ai/b;)Lmaps/ai/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ai/a;->c()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0}, Lmaps/ai/a;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1}, Lmaps/ac/ah;->h()F

    move-result v0

    invoke-static {}, Lmaps/ax/m;->a()Lmaps/ax/k;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ax/k;->a()I

    invoke-virtual {v1}, Lmaps/ac/ah;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/road_shields/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget v1, p5, Lmaps/av/a;->m:F

    mul-float/2addr v0, v1

    :goto_4
    invoke-virtual {p2}, Lmaps/ar/a;->k()F

    move-result v1

    mul-float/2addr v0, v1

    new-instance v1, Lmaps/au/q;

    invoke-direct {v1, v2, v0, p3}, Lmaps/au/q;-><init>(Landroid/graphics/Bitmap;FLmaps/as/c;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_3

    :cond_7
    iget v1, p5, Lmaps/av/a;->n:F

    mul-float/2addr v0, v1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_9
    invoke-virtual {v1}, Lmaps/ac/ah;->e()Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lmaps/au/r;

    invoke-virtual {v1}, Lmaps/ac/ah;->k()F

    move-result v1

    invoke-direct {v0, v1}, Lmaps/au/r;-><init>(F)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto/16 :goto_3

    :cond_a
    invoke-virtual {v1}, Lmaps/ac/ah;->f()Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto/16 :goto_3

    :cond_b
    new-instance v0, Lmaps/au/o;

    invoke-virtual {p0}, Lmaps/ac/ag;->c()Lmaps/ac/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/b;->a()I

    move-result v1

    invoke-static {v1}, Lmaps/au/s;->a(I)Lmaps/au/s;

    move-result-object v1

    invoke-virtual {p0}, Lmaps/ac/ag;->c()Lmaps/ac/b;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/b;->b()I

    move-result v2

    invoke-static {v2}, Lmaps/au/v;->a(I)Lmaps/au/v;

    move-result-object v2

    invoke-direct {v0, v8, v1, v2}, Lmaps/au/o;-><init>(Ljava/util/ArrayList;Lmaps/au/s;Lmaps/au/v;)V

    goto :goto_5

    :cond_c
    move-object v1, v7

    goto/16 :goto_3
.end method

.method private c(Lmaps/as/a;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->h:Lmaps/al/j;

    invoke-virtual {v1}, Lmaps/al/j;->f()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->i:Lmaps/at/g;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lmaps/at/g;->b(Lmaps/as/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->h:Lmaps/al/j;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Lmaps/al/j;->a(I)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lmaps/au/o;->k:F

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/au/o;->l:F

    sub-float v2, v1, v2

    const/4 v1, 0x0

    move v4, v2

    move v5, v3

    move v3, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v7, v6

    move v6, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmaps/au/u;

    invoke-interface {v2}, Lmaps/au/u;->e()F

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(FF)F

    move-result v7

    invoke-interface {v2}, Lmaps/au/u;->a()F

    move-result v2

    add-float/2addr v2, v6

    move v6, v2

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/au/o;->a:Lmaps/au/s;

    sget-object v9, Lmaps/au/s;->a:Lmaps/au/s;

    if-ne v8, v9, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/au/o;->j:F

    sub-float/2addr v2, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v2, v6

    :cond_1
    :goto_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v6, v2

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lmaps/au/u;

    instance-of v1, v2, Lmaps/au/r;

    if-eqz v1, :cond_3

    invoke-interface {v2}, Lmaps/au/u;->a()F

    move-result v1

    add-float v2, v6, v1

    move v6, v2

    goto :goto_3

    :cond_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lmaps/au/o;->a:Lmaps/au/s;

    sget-object v9, Lmaps/au/s;->c:Lmaps/au/s;

    if-ne v8, v9, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lmaps/au/o;->j:F

    sub-float/2addr v2, v6

    goto :goto_2

    :cond_3
    invoke-interface {v2}, Lmaps/au/u;->a()F

    move-result v10

    invoke-interface {v2}, Lmaps/au/u;->b()F

    move-result v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->b:Lmaps/au/v;

    sget-object v8, Lmaps/au/v;->a:Lmaps/au/v;

    if-ne v1, v8, :cond_4

    invoke-interface {v2}, Lmaps/au/u;->e()F

    move-result v1

    sub-float v1, v7, v1

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v1, v8

    sub-float v1, v4, v1

    :goto_4
    invoke-interface {v2}, Lmaps/au/u;->c()F

    move-result v8

    add-float v12, v1, v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    add-int/lit8 v8, v5, 0x1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/as/b;

    invoke-virtual {v1}, Lmaps/as/b;->e()F

    move-result v5

    invoke-virtual {v1}, Lmaps/as/b;->f()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/au/o;->f:Lmaps/at/o;

    const/4 v14, 0x0

    sub-float v15, v12, v11

    invoke-interface {v13, v6, v14, v15}, Lmaps/at/o;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lmaps/au/o;->f:Lmaps/at/o;

    add-float v14, v6, v10

    const/4 v15, 0x0

    sub-float v11, v12, v11

    invoke-interface {v13, v14, v15, v11}, Lmaps/at/o;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lmaps/au/o;->f:Lmaps/at/o;

    add-float/2addr v10, v6

    const/4 v13, 0x0

    invoke-interface {v11, v10, v13, v12}, Lmaps/at/o;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/au/o;->f:Lmaps/at/o;

    const/4 v11, 0x0

    invoke-interface {v10, v6, v11, v12}, Lmaps/at/o;->a(FFF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/au/o;->g:Lmaps/at/j;

    const/4 v11, 0x0

    invoke-interface {v10, v11, v1}, Lmaps/at/j;->a(FF)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lmaps/au/o;->g:Lmaps/at/j;

    invoke-interface {v10, v5, v1}, Lmaps/at/j;->a(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->g:Lmaps/at/j;

    const/4 v10, 0x0

    invoke-interface {v1, v5, v10}, Lmaps/at/j;->a(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->g:Lmaps/at/j;

    const/4 v5, 0x0

    const/4 v10, 0x0

    invoke-interface {v1, v5, v10}, Lmaps/at/j;->a(FF)V

    invoke-interface {v2}, Lmaps/au/u;->a()F

    move-result v1

    add-float v2, v6, v1

    move v6, v2

    move v5, v8

    goto/16 :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->b:Lmaps/au/v;

    sget-object v8, Lmaps/au/v;->c:Lmaps/au/v;

    if-ne v1, v8, :cond_7

    invoke-interface {v2}, Lmaps/au/u;->e()F

    move-result v1

    sub-float v1, v7, v1

    sub-float v1, v4, v1

    goto :goto_4

    :cond_5
    sub-float v2, v4, v7

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lmaps/au/o;->o:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->h:Lmaps/al/j;

    invoke-virtual {v1}, Lmaps/al/j;->c()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/au/o;->i:Lmaps/at/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/au/o;->h:Lmaps/al/j;

    invoke-virtual {v2}, Lmaps/al/j;->e()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmaps/at/g;->a(Ljava/nio/ByteBuffer;)V

    return-void

    :cond_7
    move v1, v4

    goto/16 :goto_4
.end method

.method private d()V
    .locals 10

    const/4 v2, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v4, 0x0

    iput v4, p0, Lmaps/au/o;->j:F

    move v1, v2

    move v3, v4

    :goto_0
    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v4

    move v6, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    invoke-interface {v0}, Lmaps/au/u;->a()F

    move-result v8

    add-float/2addr v6, v8

    invoke-interface {v0}, Lmaps/au/u;->e()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v5, v0

    goto :goto_1

    :cond_0
    iget v0, p0, Lmaps/au/o;->j:F

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/o;->j:F

    add-float/2addr v3, v5

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput v4, p0, Lmaps/au/o;->l:F

    iput v4, p0, Lmaps/au/o;->m:F

    sget-object v2, Lmaps/au/p;->a:[I

    iget-object v5, p0, Lmaps/au/o;->b:Lmaps/au/v;

    invoke-virtual {v5}, Lmaps/au/v;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_2
    iget v0, p0, Lmaps/au/o;->l:F

    add-float/2addr v0, v3

    iget v1, p0, Lmaps/au/o;->m:F

    add-float/2addr v0, v1

    iput v0, p0, Lmaps/au/o;->k:F

    return-void

    :pswitch_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    iget v5, p0, Lmaps/au/o;->l:F

    invoke-interface {v0}, Lmaps/au/u;->c()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/o;->l:F

    goto :goto_3

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    invoke-interface {v0}, Lmaps/au/u;->e()F

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, Lmaps/au/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_4

    :cond_4
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, Lmaps/au/o;->m:F

    goto :goto_2

    :pswitch_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v4

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    invoke-interface {v0}, Lmaps/au/u;->e()F

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-interface {v0}, Lmaps/au/u;->c()F

    move-result v0

    add-float/2addr v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_5

    :cond_5
    cmpl-float v0, v4, v2

    if-lez v0, :cond_6

    sub-float v0, v4, v2

    iput v0, p0, Lmaps/au/o;->l:F

    :cond_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    iget v2, p0, Lmaps/au/o;->m:F

    invoke-interface {v0}, Lmaps/au/u;->d()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lmaps/au/o;->m:F

    goto :goto_6

    :pswitch_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v4

    move v5, v4

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    invoke-interface {v0}, Lmaps/au/u;->e()F

    move-result v7

    div-float/2addr v7, v9

    invoke-static {v5, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-interface {v0}, Lmaps/au/u;->c()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v2, v0

    goto :goto_7

    :cond_7
    cmpl-float v0, v2, v5

    if-lez v0, :cond_8

    sub-float v0, v2, v5

    iput v0, p0, Lmaps/au/o;->l:F

    :cond_8
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    invoke-interface {v0}, Lmaps/au/u;->e()F

    move-result v5

    div-float/2addr v5, v9

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-interface {v0}, Lmaps/au/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_8

    :cond_9
    cmpl-float v0, v4, v1

    if-lez v0, :cond_2

    sub-float v0, v4, v1

    iput v0, p0, Lmaps/au/o;->m:F

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()F
    .locals 1

    iget v0, p0, Lmaps/au/o;->j:F

    return v0
.end method

.method public final a(Lmaps/as/a;)V
    .locals 4

    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/au/o;->i:Lmaps/at/g;

    invoke-virtual {v0, p1}, Lmaps/at/g;->d(Lmaps/as/a;)V

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lmaps/au/o;->n:Z

    if-nez v0, :cond_c

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v3

    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    instance-of v6, v0, Lmaps/au/r;

    if-nez v6, :cond_2

    invoke-interface {v0, v3}, Lmaps/au/u;->a(Lmaps/ap/b;)Lmaps/as/b;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    :goto_2
    if-nez v0, :cond_b

    const/16 v0, 0x2710

    invoke-virtual {p1, v0}, Lmaps/as/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    if-nez v0, :cond_c

    :cond_4
    return-void

    :cond_5
    iget-object v6, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty on initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_a
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/u;

    instance-of v6, v0, Lmaps/au/r;

    if-nez v6, :cond_a

    iget-object v6, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-interface {v0, p1, v3}, Lmaps/au/u;->a(Lmaps/as/a;Lmaps/ap/b;)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_b
    invoke-direct {p0, p1}, Lmaps/au/o;->c(Lmaps/as/a;)V

    iput-boolean v2, p0, Lmaps/au/o;->n:Z

    move v0, v2

    goto :goto_3

    :cond_c
    iget-boolean v0, p0, Lmaps/au/o;->o:Z

    if-eqz v0, :cond_d

    invoke-direct {p0, p1}, Lmaps/au/o;->c(Lmaps/as/a;)V

    :cond_d
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    iget-object v0, p0, Lmaps/au/o;->i:Lmaps/at/g;

    invoke-virtual {v0}, Lmaps/at/g;->a()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/au/o;->i:Lmaps/at/g;

    invoke-virtual {v0, p1}, Lmaps/at/g;->a(Lmaps/as/a;)V

    :goto_5
    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lmaps/au/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    invoke-virtual {v0, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    const/4 v0, 0x6

    mul-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5
.end method

.method public final a(Lmaps/au/s;)V
    .locals 1

    iget-object v0, p0, Lmaps/au/o;->a:Lmaps/au/s;

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/au/o;->o:Z

    :cond_0
    iput-object p1, p0, Lmaps/au/o;->a:Lmaps/au/s;

    return-void
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/au/o;->k:F

    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/au/o;->a(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/au/o;->i:Lmaps/at/g;

    invoke-virtual {v0, p1}, Lmaps/at/g;->c(Lmaps/as/a;)V

    return-void
.end method

.method public final c()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/au/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lmaps/au/o;->j:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/au/o;->k:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
