.class public final Lmaps/v/l;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/v/k;


# instance fields
.field private final a:Lmaps/v/n;

.field private final b:Lmaps/v/i;

.field private final c:Lmaps/v/j;

.field private final d:Lmaps/v/j;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x1388

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/v/n;

    new-instance v1, Lmaps/v/d;

    invoke-direct {v1}, Lmaps/v/d;-><init>()V

    invoke-direct {v0, v1}, Lmaps/v/n;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    new-instance v0, Lmaps/v/j;

    new-instance v1, Lmaps/v/d;

    invoke-direct {v1}, Lmaps/v/d;-><init>()V

    invoke-direct {v0, v1}, Lmaps/v/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    new-instance v0, Lmaps/v/i;

    new-instance v1, Lmaps/v/b;

    invoke-direct {v1}, Lmaps/v/b;-><init>()V

    invoke-direct {v0, v1}, Lmaps/v/i;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0, v2, v3}, Lmaps/v/n;->setDuration(J)V

    iget-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v0, v2, v3}, Lmaps/v/i;->setDuration(J)V

    iget-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v0, v2, v3}, Lmaps/v/j;->setDuration(J)V

    new-instance v0, Lmaps/v/j;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-direct {v0, v1}, Lmaps/v/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lmaps/v/j;->setDuration(J)V

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lmaps/v/j;->setRepeatCount(I)V

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->start()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(J)I
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v1}, Lmaps/v/n;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lmaps/v/l;->d:Lmaps/v/j;

    invoke-virtual {v1, p1, p2}, Lmaps/v/j;->b(J)V

    iget-object v1, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v1}, Lmaps/v/n;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v1}, Lmaps/v/i;->hasEnded()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v1}, Lmaps/v/j;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v1}, Lmaps/v/n;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmaps/ac/av;

    invoke-virtual {v0, v1}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v0

    float-to-double v1, v0

    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/av;

    invoke-virtual {v0}, Lmaps/ac/av;->e()D

    move-result-wide v3

    div-double v0, v1, v3

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    iget-object v1, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v1}, Lmaps/v/n;->b()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v2}, Lmaps/v/n;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/v/n;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_3
    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0, p1, p2}, Lmaps/v/n;->c(J)V

    iget-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v0, p1, p2}, Lmaps/v/i;->c(J)V

    iget-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v0, p1, p2}, Lmaps/v/j;->b(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lmaps/ac/au;)Z
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->isInitialized()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/av;

    iget-object v1, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v1}, Lmaps/v/i;->b()F

    move-result v1

    iget-object v2, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v2}, Lmaps/v/j;->b()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lmaps/ac/au;->a(Lmaps/ac/av;FI)V

    iget-object v0, p0, Lmaps/v/l;->d:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lmaps/ac/au;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lmaps/ac/au;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v0

    iget-object v1, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v1}, Lmaps/v/n;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/k/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {p1}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/v/n;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lmaps/v/l;->a:Lmaps/v/n;

    invoke-virtual {v0}, Lmaps/v/n;->start()V

    :cond_1
    iget-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v0}, Lmaps/v/i;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lmaps/ac/au;->b()F

    move-result v0

    iget-object v1, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v1}, Lmaps/v/i;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {p1}, Lmaps/ac/au;->b()F

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/v/i;->a(F)V

    iget-object v0, p0, Lmaps/v/l;->b:Lmaps/v/i;

    invoke-virtual {v0}, Lmaps/v/i;->start()V

    :cond_3
    iget-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lmaps/ac/au;->c()I

    move-result v0

    iget-object v1, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v1}, Lmaps/v/j;->a()I

    move-result v1

    if-eq v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {p1}, Lmaps/ac/au;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/l;->c:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
