.class public final Lmaps/v/e;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/v/j;


# direct methods
.method public constructor <init>(JJLmaps/v/g;)V
    .locals 8

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lmaps/v/e;-><init>(JJLmaps/v/g;II)V

    return-void
.end method

.method public constructor <init>(JJLmaps/v/g;II)V
    .locals 7

    const/high16 v6, 0x10000

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/v/j;

    new-instance v1, Lmaps/v/c;

    long-to-float v2, p1

    add-long v3, p1, p3

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lmaps/v/c;-><init>(F)V

    invoke-direct {v0, v1}, Lmaps/v/j;-><init>(Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    add-long v1, p1, p3

    invoke-virtual {v0, v1, v2}, Lmaps/v/j;->setDuration(J)V

    sget-object v0, Lmaps/v/f;->a:[I

    invoke-virtual {p5}, Lmaps/v/g;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, v5}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, v6}, Lmaps/v/j;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, v6}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, v5}, Lmaps/v/j;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, p6}, Lmaps/v/j;->a(I)V

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0, p7}, Lmaps/v/j;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Lmaps/v/g;)V
    .locals 6

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x1f4

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmaps/v/e;-><init>(JJLmaps/v/g;)V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/as/a;)I
    .locals 3

    invoke-virtual {p1}, Lmaps/as/a;->e()J

    move-result-wide v0

    iget-object v2, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v2}, Lmaps/v/j;->hasStarted()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v2}, Lmaps/v/j;->start()V

    :cond_0
    iget-object v2, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v2, v0, v1}, Lmaps/v/j;->b(J)V

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->b()I

    move-result v0

    iget-object v1, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v1}, Lmaps/v/j;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lmaps/as/a;->b()V

    :cond_1
    return v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lmaps/v/e;->a:Lmaps/v/j;

    invoke-virtual {v0}, Lmaps/v/j;->start()V

    return-void
.end method
