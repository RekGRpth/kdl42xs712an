.class public final Lmaps/ba/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/bv/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lmaps/bv/a;
    .locals 4

    iget-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    if-nez v0, :cond_1

    :try_start_0
    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v0

    const-string v1, "ShortbreadToken"

    invoke-interface {v0, v1}, Lmaps/bt/k;->a(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    sget-object v1, Lmaps/cm/g;->c:Lmaps/bv/c;

    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1, v2}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    if-nez v0, :cond_1

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/g;->c:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v1, 0x1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    iput-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    :cond_1
    iget-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lmaps/bv/a;)V
    .locals 5

    const/4 v3, 0x2

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iput-object p1, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    :try_start_0
    new-instance v0, Lmaps/bj/a;

    invoke-direct {v0}, Lmaps/bj/a;-><init>()V

    iget-object v1, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    invoke-static {v0, v1}, Lmaps/bv/e;->a(Ljava/io/DataOutput;Lmaps/bv/a;)V

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v1

    const-string v2, "ShortbreadToken"

    invoke-virtual {v0}, Lmaps/bj/a;->a()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lmaps/bt/k;->a(Ljava/lang/String;[B)Z

    invoke-static {}, Lmaps/br/b;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    invoke-virtual {v1, v0}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    invoke-virtual {v1, v3}, Lmaps/bv/a;->e(I)J

    move-result-wide v1

    invoke-virtual {p1, v3}, Lmaps/bv/a;->e(I)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ba/g;->a:Lmaps/bv/a;

    goto :goto_1
.end method
