.class public final enum Lmaps/ba/d;
.super Ljava/lang/Enum;


# static fields
.field private static enum a:Lmaps/ba/d;

.field private static enum b:Lmaps/ba/d;

.field private static final synthetic c:[Lmaps/ba/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lmaps/ba/d;

    const-string v1, "ORIENTATION_PROVIDER_ACTIVITY_RESUME"

    invoke-direct {v0, v1, v2}, Lmaps/ba/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ba/d;->a:Lmaps/ba/d;

    new-instance v0, Lmaps/ba/d;

    const-string v1, "START_MOTION_RECOGNIZER"

    invoke-direct {v0, v1, v3}, Lmaps/ba/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ba/d;->b:Lmaps/ba/d;

    const/4 v0, 0x2

    new-array v0, v0, [Lmaps/ba/d;

    sget-object v1, Lmaps/ba/d;->a:Lmaps/ba/d;

    aput-object v1, v0, v2

    sget-object v1, Lmaps/ba/d;->b:Lmaps/ba/d;

    aput-object v1, v0, v3

    sput-object v0, Lmaps/ba/d;->c:[Lmaps/ba/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ba/d;
    .locals 1

    const-class v0, Lmaps/ba/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ba/d;

    return-object v0
.end method

.method public static values()[Lmaps/ba/d;
    .locals 1

    sget-object v0, Lmaps/ba/d;->c:[Lmaps/ba/d;

    invoke-virtual {v0}, [Lmaps/ba/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ba/d;

    return-object v0
.end method
