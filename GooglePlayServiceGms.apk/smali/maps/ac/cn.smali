.class public Lmaps/ac/cn;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lmaps/ac/o;

.field protected final b:Lmaps/ac/n;

.field protected final c:I

.field protected final d:I


# direct methods
.method protected constructor <init>(Lmaps/ac/o;Lmaps/ac/n;II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/cn;->a:Lmaps/ac/o;

    iput-object p2, p0, Lmaps/ac/cn;->b:Lmaps/ac/n;

    iput p3, p0, Lmaps/ac/cn;->c:I

    iput p4, p0, Lmaps/ac/cn;->d:I

    return-void
.end method

.method public static a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/cn;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    invoke-static {v2, v3}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lmaps/ac/cs;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/n;

    move-result-object v1

    new-instance v0, Lmaps/ac/cp;

    invoke-direct {v0, v1}, Lmaps/ac/cp;-><init>(Lmaps/ac/n;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/ac/cr;

    invoke-direct {v0}, Lmaps/ac/cr;-><init>()V

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    invoke-static {v2, v3}, Lmaps/ac/ar;->a(II)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0, p1}, Lmaps/ac/cs;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/n;

    move-result-object v3

    invoke-static {v2, v4}, Lmaps/ac/ar;->a(II)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v0

    :cond_2
    new-instance v1, Lmaps/ac/co;

    invoke-direct {v1, v3, v2, v0}, Lmaps/ac/co;-><init>(Lmaps/ac/n;II)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-static {v2, v4}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v0

    :goto_1
    new-instance v1, Lmaps/ac/cq;

    invoke-direct {v1, v0}, Lmaps/ac/cq;-><init>(Lmaps/ac/o;)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-static {p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v0

    goto :goto_1
.end method
