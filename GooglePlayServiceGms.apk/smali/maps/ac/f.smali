.class public final Lmaps/ac/f;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:Lmaps/ac/o;

.field private final b:Lmaps/ac/cj;

.field private final c:[B

.field private final d:Lmaps/ac/bl;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:[I


# direct methods
.method public constructor <init>(Lmaps/ac/o;Lmaps/ac/cj;[BLmaps/ac/bl;ILjava/lang/String;II[I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/f;->a:Lmaps/ac/o;

    iput-object p2, p0, Lmaps/ac/f;->b:Lmaps/ac/cj;

    iput-object p3, p0, Lmaps/ac/f;->c:[B

    iput-object p4, p0, Lmaps/ac/f;->d:Lmaps/ac/bl;

    iput p5, p0, Lmaps/ac/f;->e:I

    iput-object p6, p0, Lmaps/ac/f;->f:Ljava/lang/String;

    iput p7, p0, Lmaps/ac/f;->g:I

    iput p8, p0, Lmaps/ac/f;->h:I

    iput-object p9, p0, Lmaps/ac/f;->i:[I

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/o;
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final b()Lmaps/ac/cj;
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->b:Lmaps/ac/cj;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->c:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->d:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/f;->g:I

    return v0
.end method

.method public final g()[B
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->c:[B

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lmaps/ac/f;->e:I

    return v0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->i:[I

    return-object v0
.end method

.method public final j()I
    .locals 3

    iget-object v0, p0, Lmaps/ac/f;->b:Lmaps/ac/cj;

    invoke-virtual {v0}, Lmaps/ac/cj;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x3c

    iget-object v1, p0, Lmaps/ac/f;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/f;->a:Lmaps/ac/o;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/o;)I

    move-result v1

    iget-object v2, p0, Lmaps/ac/f;->f:Ljava/lang/String;

    invoke-static {v2}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lmaps/ac/f;->d:Lmaps/ac/bl;

    invoke-static {v2}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    iget v0, p0, Lmaps/ac/f;->h:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lmaps/ac/f;->h:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method
