.class public final Lmaps/ac/h;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/ac/be;

.field private b:I

.field private c:[Lmaps/ac/av;

.field private d:Z

.field private final e:Ljava/util/ArrayList;

.field private final f:Ljava/util/ArrayList;

.field private g:I


# direct methods
.method public constructor <init>(Lmaps/ac/be;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/h;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ac/h;->f:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lmaps/ac/h;->a(Lmaps/ac/be;)V

    return-void
.end method

.method private a(ILmaps/ac/av;IZ)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lmaps/ac/h;->b:I

    if-ne p1, v0, :cond_3

    if-eqz p4, :cond_1

    iget v0, p0, Lmaps/ac/h;->g:I

    iget-object v1, p0, Lmaps/ac/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ac/h;->e:Ljava/util/ArrayList;

    new-instance v1, Lmaps/ac/bb;

    invoke-direct {v1}, Lmaps/ac/bb;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ac/h;->f:Ljava/util/ArrayList;

    new-instance v1, Lmaps/ac/i;

    invoke-direct {v1}, Lmaps/ac/i;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v0, p0, Lmaps/ac/h;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ac/h;->g:I

    :cond_1
    iget-object v0, p0, Lmaps/ac/h;->e:Ljava/util/ArrayList;

    iget v1, p0, Lmaps/ac/h;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bb;

    invoke-virtual {v0, p2}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lmaps/ac/h;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ac/h;->f:Ljava/util/ArrayList;

    iget v1, p0, Lmaps/ac/h;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/i;

    invoke-virtual {v0, p3}, Lmaps/ac/i;->a(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-nez p1, :cond_6

    iget-object v0, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {v0}, Lmaps/ac/be;->h()Lmaps/ac/av;

    move-result-object v1

    iget-object v0, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {v0, v4}, Lmaps/ac/be;->a(I)Lmaps/ac/av;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0, p2}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)I

    move-result v2

    if-ltz v2, :cond_7

    if-nez p4, :cond_4

    iget-object v2, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)I

    move-result v2

    if-gez v2, :cond_4

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    iget-object v3, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    aget-object v3, v3, p1

    invoke-static {v1, v0, p2, v3, v2}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Z

    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, p3, v1}, Lmaps/ac/h;->a(ILmaps/ac/av;IZ)V

    :cond_4
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, p2, p3, p4}, Lmaps/ac/h;->a(ILmaps/ac/av;IZ)V

    :cond_5
    :goto_2
    iget-object v0, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lmaps/ac/av;->b(Lmaps/ac/av;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lmaps/ac/be;->a(I)Lmaps/ac/av;

    move-result-object v1

    iget-object v0, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {v0, p1}, Lmaps/ac/be;->a(I)Lmaps/ac/av;

    move-result-object v0

    goto :goto_1

    :cond_7
    if-nez p4, :cond_5

    iget-object v2, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    aget-object v2, v2, p1

    invoke-static {v1, v0, v2}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)I

    move-result v2

    if-ltz v2, :cond_5

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    iget-object v3, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    aget-object v3, v3, p1

    invoke-static {v1, v0, v3, p2, v2}, Lmaps/ac/ax;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Z

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0, v2, p3, v4}, Lmaps/ac/h;->a(ILmaps/ac/av;IZ)V

    goto :goto_2
.end method

.method private a(Lmaps/ac/be;)V
    .locals 3

    iput-object p1, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {p1}, Lmaps/ac/be;->b()I

    move-result v0

    iput v0, p0, Lmaps/ac/h;->b:I

    iget v0, p0, Lmaps/ac/h;->b:I

    new-array v0, v0, [Lmaps/ac/av;

    iput-object v0, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lmaps/ac/h;->c:[Lmaps/ac/av;

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/az;Ljava/util/List;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    iput v1, p0, Lmaps/ac/h;->g:I

    iput-boolean v1, p0, Lmaps/ac/h;->d:Z

    invoke-virtual {p1}, Lmaps/ac/az;->a()Lmaps/ac/bd;

    move-result-object v0

    iget-object v2, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {v2, v0}, Lmaps/ac/be;->a(Lmaps/ac/be;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/ac/h;->a:Lmaps/ac/be;

    invoke-virtual {v2, v0}, Lmaps/ac/be;->b(Lmaps/ac/be;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lmaps/ac/h;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {v6, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    new-instance v4, Lmaps/ac/av;

    invoke-direct {v4}, Lmaps/ac/av;-><init>()V

    invoke-virtual {p1}, Lmaps/ac/az;->b()I

    move-result v5

    invoke-virtual {p1, v1, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget-boolean v0, p0, Lmaps/ac/h;->d:Z

    if-eqz v0, :cond_2

    aget v0, v6, v1

    :goto_0
    invoke-direct {p0, v1, v4, v0, v3}, Lmaps/ac/h;->a(ILmaps/ac/av;IZ)V

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_4

    invoke-virtual {p1, v2, v4}, Lmaps/ac/az;->a(ILmaps/ac/av;)V

    iget-boolean v0, p0, Lmaps/ac/h;->d:Z

    if-eqz v0, :cond_3

    aget v0, v6, v2

    :goto_2
    invoke-direct {p0, v1, v4, v0, v1}, Lmaps/ac/h;->a(ILmaps/ac/av;IZ)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    :goto_3
    iget v0, p0, Lmaps/ac/h;->g:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lmaps/ac/h;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bb;

    invoke-virtual {v0}, Lmaps/ac/bb;->a()I

    move-result v2

    if-le v2, v3, :cond_5

    invoke-virtual {v0}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Lmaps/ac/bb;->c()V

    iget-boolean v0, p0, Lmaps/ac/h;->d:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/ac/h;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/i;

    invoke-virtual {v0}, Lmaps/ac/i;->c()I

    move-result v2

    if-le v2, v3, :cond_6

    invoke-virtual {v0}, Lmaps/ac/i;->a()[I

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {v0}, Lmaps/ac/i;->b()V

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method
