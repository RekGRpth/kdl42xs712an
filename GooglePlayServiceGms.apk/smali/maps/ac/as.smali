.class public final Lmaps/ac/as;
.super Lmaps/ac/cs;


# instance fields
.field private final j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/Set;

.field private m:Ljava/util/List;

.field private n:Ljava/util/List;

.field private o:J


# direct methods
.method private constructor <init>(Lmaps/ac/cs;)V
    .locals 16

    move-object/from16 v0, p1

    iget-object v2, v0, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    move-object/from16 v0, p1

    iget v3, v0, Lmaps/ac/cs;->b:I

    move-object/from16 v0, p1

    iget-byte v4, v0, Lmaps/ac/cs;->c:B

    move-object/from16 v0, p1

    iget v5, v0, Lmaps/ac/cs;->h:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget v8, v0, Lmaps/ac/cs;->e:I

    const/4 v9, 0x0

    move-object/from16 v0, p1

    iget-object v10, v0, Lmaps/ac/cs;->f:Lmaps/ao/b;

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    iget-wide v14, v0, Lmaps/ac/cs;->i:J

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v15}, Lmaps/ac/cs;-><init>(Lmaps/ac/bt;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/ac/n;Lmaps/ao/b;[Lmaps/ac/cn;JJ)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ac/as;->l:Ljava/util/Set;

    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/ac/as;->o:J

    move-object/from16 v0, p1

    iget-object v1, v0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ac/as;->j:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->l()Lmaps/ac/cu;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Lmaps/ac/cu;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v1}, Lmaps/ac/cu;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ac/as;->m:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->j()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/as;->m:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->j()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lmaps/ac/as;->n:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->i()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ac/as;->n:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->i()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lmaps/ac/cs;->n()J

    move-result-wide v1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lmaps/ac/as;->o:J

    return-void
.end method

.method static synthetic a(Lmaps/ac/as;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    return-object v0
.end method

.method public static a(Lmaps/ac/cs;Lmaps/ac/cs;)Lmaps/ac/cs;
    .locals 11

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lmaps/ac/cs;->n()J

    move-result-wide v0

    cmp-long v2, v0, v5

    if-ltz v2, :cond_0

    invoke-virtual {p1}, Lmaps/ac/cs;->n()J

    move-result-wide v2

    cmp-long v2, v2, v5

    if-ltz v2, :cond_16

    invoke-virtual {p1}, Lmaps/ac/cs;->n()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_16

    :cond_0
    invoke-virtual {p1}, Lmaps/ac/cs;->n()J

    move-result-wide v0

    move-wide v2, v0

    :goto_0
    invoke-virtual {p1}, Lmaps/ac/cs;->p()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lmaps/ac/cs;->n()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    :goto_1
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lmaps/ac/cs;->p()I

    move-result v0

    if-lez v0, :cond_14

    invoke-static {p0}, Lmaps/ac/as;->b(Lmaps/ac/cs;)Lmaps/ac/as;

    move-result-object p0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmaps/ac/as;->l:Ljava/util/Set;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v1, v4

    :goto_2
    invoke-virtual {p1}, Lmaps/ac/cs;->p()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p1, Lmaps/ac/cs;->g:[Lmaps/ac/cn;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lmaps/ac/cs;->g:[Lmaps/ac/cn;

    aget-object v0, v0, v1

    :goto_3
    instance-of v8, v0, Lmaps/ac/co;

    if-eqz v8, :cond_3

    check-cast v0, Lmaps/ac/co;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    instance-of v8, v0, Lmaps/ac/cq;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lmaps/ac/as;->l:Ljava/util/Set;

    check-cast v0, Lmaps/ac/cq;

    invoke-virtual {v0}, Lmaps/ac/cq;->a()Lmaps/ac/o;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    instance-of v8, v0, Lmaps/ac/cp;

    if-eqz v8, :cond_5

    check-cast v0, Lmaps/ac/cp;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_5
    instance-of v8, v0, Lmaps/ac/cr;

    if-eqz v8, :cond_6

    check-cast v0, Lmaps/ac/cr;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong modifier: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    iget-object v8, p0, Lmaps/ac/as;->l:Ljava/util/Set;

    invoke-interface {v0}, Lmaps/ac/n;->a()Lmaps/ac/o;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    :cond_9
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lmaps/ac/co;

    move v0, v4

    :goto_7
    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v8

    invoke-interface {v8}, Lmaps/ac/n;->i()[I

    move-result-object v8

    array-length v8, v8

    if-ge v0, v8, :cond_a

    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v8

    invoke-interface {v8}, Lmaps/ac/n;->i()[I

    move-result-object v8

    aget v9, v8, v0

    iget-object v10, p0, Lmaps/ac/as;->m:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/2addr v9, v10

    aput v9, v8, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_a
    invoke-virtual {v1}, Lmaps/ac/co;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v1}, Lmaps/ac/co;->c()I

    move-result v0

    iget-object v8, p0, Lmaps/ac/as;->j:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v0, v8, :cond_d

    iget-object v0, p0, Lmaps/ac/as;->j:Ljava/util/List;

    invoke-virtual {v1}, Lmaps/ac/co;->c()I

    move-result v8

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    iget-object v8, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v8, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_c

    invoke-virtual {v1}, Lmaps/ac/co;->d()Z

    move-result v8

    if-eqz v8, :cond_b

    iget-object v8, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_6

    :cond_b
    iget-object v8, p0, Lmaps/ac/as;->k:Ljava/util/List;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_6

    :cond_c
    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_d
    invoke-virtual {v1}, Lmaps/ac/co;->c()I

    move-result v0

    iget-object v8, p0, Lmaps/ac/as;->j:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lt v0, v8, :cond_e

    const-string v0, "MutableVectorTile"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Invalid plane index on tile "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p1, Lmaps/ac/cs;->f:Lmaps/ao/b;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-virtual {v1}, Lmaps/ac/co;->a()Lmaps/ac/n;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_f
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_8

    :cond_10
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/cp;

    iget-object v5, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-virtual {v0}, Lmaps/ac/cp;->a()Lmaps/ac/n;

    move-result-object v0

    invoke-interface {v5, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_9

    :cond_11
    invoke-virtual {p1}, Lmaps/ac/cs;->i()[Ljava/lang/String;

    move-result-object v0

    :goto_a
    array-length v1, v0

    if-ge v4, v1, :cond_13

    iget-object v1, p0, Lmaps/ac/as;->n:Ljava/util/List;

    aget-object v5, v0, v4

    invoke-interface {v1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    iget-object v1, p0, Lmaps/ac/as;->n:Ljava/util/List;

    aget-object v5, v0, v4

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_12
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_13
    iget-object v0, p0, Lmaps/ac/as;->m:Ljava/util/List;

    invoke-virtual {p1}, Lmaps/ac/cs;->j()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-wide v2, p0, Lmaps/ac/as;->o:J

    goto/16 :goto_1

    :cond_14
    instance-of v0, p0, Lmaps/ac/as;

    if-eqz v0, :cond_15

    move-object v0, p0

    check-cast v0, Lmaps/ac/as;

    iput-wide v2, v0, Lmaps/ac/as;->o:J

    goto/16 :goto_1

    :cond_15
    new-instance v0, Lmaps/ac/ct;

    invoke-direct {v0}, Lmaps/ac/ct;-><init>()V

    iget-object v1, p0, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a(Lmaps/ac/bt;)Lmaps/ac/ct;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cs;->b:I

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->b(I)Lmaps/ac/ct;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cs;->h:I

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a(I)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/cs;->i()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a([Ljava/lang/String;)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/ac/cs;->j()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->b([Ljava/lang/String;)Lmaps/ac/ct;

    move-result-object v0

    iget v1, p0, Lmaps/ac/cs;->e:I

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->c(I)Lmaps/ac/ct;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a([Lmaps/ac/n;)Lmaps/ac/ct;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cs;->f:Lmaps/ao/b;

    invoke-virtual {v0, v1}, Lmaps/ac/ct;->a(Lmaps/ao/b;)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lmaps/ac/ct;->a(J)Lmaps/ac/ct;

    move-result-object v0

    iget-wide v1, p0, Lmaps/ac/cs;->i:J

    invoke-virtual {v0, v1, v2}, Lmaps/ac/ct;->b(J)Lmaps/ac/ct;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/ct;->a()Lmaps/ac/cs;

    move-result-object p0

    goto/16 :goto_1

    :cond_16
    move-wide v2, v0

    goto/16 :goto_0
.end method

.method private static b(Lmaps/ac/cs;)Lmaps/ac/as;
    .locals 1

    instance-of v0, p0, Lmaps/ac/as;

    if-eqz v0, :cond_0

    check-cast p0, Lmaps/ac/as;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lmaps/ac/as;

    invoke-direct {v0, p0}, Lmaps/ac/as;-><init>(Lmaps/ac/cs;)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static b(Lmaps/ac/cs;Lmaps/ac/cs;)Lmaps/ac/cs;
    .locals 9

    const/4 v8, 0x6

    const/4 v2, 0x0

    invoke-static {p0}, Lmaps/ac/as;->b(Lmaps/ac/cs;)Lmaps/ac/as;

    move-result-object v4

    iget-object v5, p1, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    invoke-interface {v7}, Lmaps/ac/n;->e()I

    move-result v0

    if-ne v0, v8, :cond_2

    move v1, v2

    :goto_1
    iget-object v0, v4, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v4, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    invoke-interface {v0}, Lmaps/ac/n;->e()I

    move-result v0

    if-ne v0, v8, :cond_0

    iget-object v0, v4, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0, v1, v7}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, v4, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v0, v4, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-static {v4, p1}, Lmaps/ac/as;->a(Lmaps/ac/cs;Lmaps/ac/cs;)Lmaps/ac/cs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Lmaps/ac/n;
    .locals 1

    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/n;

    return-object v0
.end method

.method public final d()Z
    .locals 4

    iget-wide v0, p0, Lmaps/ac/as;->o:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ac/as;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/ac/as;->n:Ljava/util/List;

    iget-object v1, p0, Lmaps/ac/as;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final j()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lmaps/ac/as;->m:Ljava/util/List;

    iget-object v1, p0, Lmaps/ac/as;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/as;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final l()Lmaps/ac/cu;
    .locals 2

    new-instance v0, Lmaps/ac/at;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/ac/at;-><init>(Lmaps/ac/as;B)V

    return-object v0
.end method

.method public final m()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ac/as;->l:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final n()J
    .locals 2

    iget-wide v0, p0, Lmaps/ac/as;->o:J

    return-wide v0
.end method
