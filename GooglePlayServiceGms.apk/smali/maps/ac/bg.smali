.class public final Lmaps/ac/bg;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/n;


# instance fields
.field private final a:Lmaps/ac/o;

.field private final b:Lmaps/ac/az;

.field private final c:[Lmaps/ac/ag;

.field private final d:Lmaps/ac/bl;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:[I


# direct methods
.method public constructor <init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;III[I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/bg;->a:Lmaps/ac/o;

    iput-object p2, p0, Lmaps/ac/bg;->b:Lmaps/ac/az;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    new-array p3, v0, [Lmaps/ac/ag;

    :cond_0
    iput-object p3, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    iput-object p4, p0, Lmaps/ac/bg;->d:Lmaps/ac/bl;

    iput-object p5, p0, Lmaps/ac/bg;->e:Ljava/lang/String;

    iput p6, p0, Lmaps/ac/bg;->f:I

    iput p7, p0, Lmaps/ac/bg;->g:I

    iput p8, p0, Lmaps/ac/bg;->h:I

    iput-object p9, p0, Lmaps/ac/bg;->i:[I

    return-void
.end method


# virtual methods
.method public final a(I)Lmaps/ac/ag;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()Lmaps/ac/o;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->a:Lmaps/ac/o;

    return-object v0
.end method

.method public final b()Lmaps/ac/az;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->b:Lmaps/ac/az;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    array-length v0, v0

    goto :goto_0
.end method

.method public final d()Lmaps/ac/bl;
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->d:Lmaps/ac/bl;

    return-object v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lmaps/ac/bg;->g:I

    return v0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 2

    iget-object v0, p0, Lmaps/ac/bg;->d:Lmaps/ac/bl;

    invoke-virtual {v0}, Lmaps/ac/bl;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmaps/ac/bg;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lmaps/ac/bg;->g()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, p0, Lmaps/ac/bg;->f:I

    const/16 v1, 0x80

    if-ge v0, v1, :cond_2

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()[I
    .locals 1

    iget-object v0, p0, Lmaps/ac/bg;->i:[I

    return-object v0
.end method

.method public final j()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ac/bg;->b:Lmaps/ac/az;

    invoke-virtual {v1}, Lmaps/ac/az;->i()I

    move-result v1

    add-int/lit8 v3, v1, 0x3c

    iget-object v1, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    if-eqz v1, :cond_0

    iget-object v4, p0, Lmaps/ac/bg;->c:[Lmaps/ac/ag;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-virtual {v2}, Lmaps/ac/ag;->d()I

    move-result v2

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lmaps/ac/bg;->a:Lmaps/ac/o;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/o;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/bg;->d:Lmaps/ac/bl;

    invoke-static {v1}, Lmaps/ac/ar;->a(Lmaps/ac/bl;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lmaps/ac/bg;->e:Ljava/lang/String;

    invoke-static {v1}, Lmaps/ac/ar;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    return v0
.end method

.method public final k()Z
    .locals 2

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    iget v0, p0, Lmaps/ac/bg;->h:I

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lmaps/ac/ar;->a(II)Z

    move-result v0

    return v0
.end method
