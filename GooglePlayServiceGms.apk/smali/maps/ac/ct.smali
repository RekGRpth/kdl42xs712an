.class public final Lmaps/ac/ct;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/ac/bt;

.field private b:I

.field private c:I

.field private d:[Lmaps/ac/n;

.field private e:Lmaps/ac/bp;

.field private f:J

.field private g:[Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:I

.field private j:Lmaps/ao/b;

.field private k:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lmaps/ac/ct;->c:I

    iput-wide v1, p0, Lmaps/ac/ct;->f:J

    iput v0, p0, Lmaps/ac/ct;->i:I

    sget-object v0, Lmaps/ao/b;->a:Lmaps/ao/b;

    iput-object v0, p0, Lmaps/ac/ct;->j:Lmaps/ao/b;

    iput-wide v1, p0, Lmaps/ac/ct;->k:J

    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/cs;
    .locals 15

    const/4 v3, 0x0

    new-instance v0, Lmaps/ac/cs;

    iget-object v1, p0, Lmaps/ac/ct;->e:Lmaps/ac/bp;

    iget-object v1, p0, Lmaps/ac/ct;->a:Lmaps/ac/bt;

    iget v2, p0, Lmaps/ac/ct;->b:I

    iget v4, p0, Lmaps/ac/ct;->c:I

    iget-object v5, p0, Lmaps/ac/ct;->g:[Ljava/lang/String;

    if-nez v5, :cond_0

    new-array v5, v3, [Ljava/lang/String;

    :goto_0
    iget-object v6, p0, Lmaps/ac/ct;->h:[Ljava/lang/String;

    if-nez v6, :cond_1

    new-array v6, v3, [Ljava/lang/String;

    :goto_1
    iget v7, p0, Lmaps/ac/ct;->i:I

    iget-object v8, p0, Lmaps/ac/ct;->d:[Lmaps/ac/n;

    if-nez v8, :cond_2

    new-array v8, v3, [Lmaps/ac/n;

    :goto_2
    iget-object v9, p0, Lmaps/ac/ct;->j:Lmaps/ao/b;

    const/4 v10, 0x0

    iget-wide v11, p0, Lmaps/ac/ct;->f:J

    iget-wide v13, p0, Lmaps/ac/ct;->k:J

    invoke-direct/range {v0 .. v14}, Lmaps/ac/cs;-><init>(Lmaps/ac/bt;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/ac/n;Lmaps/ao/b;[Lmaps/ac/cn;JJ)V

    return-object v0

    :cond_0
    iget-object v5, p0, Lmaps/ac/ct;->g:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lmaps/ac/ct;->h:[Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v8, p0, Lmaps/ac/ct;->d:[Lmaps/ac/n;

    goto :goto_2
.end method

.method public final a(I)Lmaps/ac/ct;
    .locals 0

    iput p1, p0, Lmaps/ac/ct;->c:I

    return-object p0
.end method

.method public final a(J)Lmaps/ac/ct;
    .locals 0

    iput-wide p1, p0, Lmaps/ac/ct;->f:J

    return-object p0
.end method

.method public final a(Lmaps/ac/bp;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->e:Lmaps/ac/bp;

    return-object p0
.end method

.method public final a(Lmaps/ac/bt;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->a:Lmaps/ac/bt;

    return-object p0
.end method

.method public final a(Lmaps/ao/b;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->j:Lmaps/ao/b;

    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->g:[Ljava/lang/String;

    return-object p0
.end method

.method public final a([Lmaps/ac/n;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->d:[Lmaps/ac/n;

    return-object p0
.end method

.method public final b(I)Lmaps/ac/ct;
    .locals 0

    iput p1, p0, Lmaps/ac/ct;->b:I

    return-object p0
.end method

.method public final b(J)Lmaps/ac/ct;
    .locals 0

    iput-wide p1, p0, Lmaps/ac/ct;->k:J

    return-object p0
.end method

.method public final b([Ljava/lang/String;)Lmaps/ac/ct;
    .locals 0

    iput-object p1, p0, Lmaps/ac/ct;->h:[Ljava/lang/String;

    return-object p0
.end method

.method public final c(I)Lmaps/ac/ct;
    .locals 0

    iput p1, p0, Lmaps/ac/ct;->i:I

    return-object p0
.end method
