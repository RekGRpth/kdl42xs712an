.class public Lmaps/ac/cs;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/m;


# instance fields
.field final a:Lmaps/ac/bt;

.field final b:I

.field final c:B

.field final d:[Lmaps/ac/n;

.field final e:I

.field final f:Lmaps/ao/b;

.field final g:[Lmaps/ac/cn;

.field final h:I

.field i:J

.field private final j:J

.field private final k:[Ljava/lang/String;

.field private final l:[Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lmaps/ac/bt;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/ac/n;Lmaps/ao/b;[Lmaps/ac/cn;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    iput p2, p0, Lmaps/ac/cs;->b:I

    iput-byte p3, p0, Lmaps/ac/cs;->c:B

    iput-object p5, p0, Lmaps/ac/cs;->k:[Ljava/lang/String;

    iput-object p6, p0, Lmaps/ac/cs;->l:[Ljava/lang/String;

    iput p7, p0, Lmaps/ac/cs;->e:I

    iput-object p8, p0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    iput-object p9, p0, Lmaps/ac/cs;->f:Lmaps/ao/b;

    iput-object p10, p0, Lmaps/ac/cs;->g:[Lmaps/ac/cn;

    iput p4, p0, Lmaps/ac/cs;->h:I

    iput-wide p11, p0, Lmaps/ac/cs;->j:J

    iput-wide p13, p0, Lmaps/ac/cs;->i:J

    return-void
.end method

.method private static a(Lmaps/ac/bt;Ljava/io/DataInput;IBIILmaps/ao/b;JJ)Lmaps/ac/cs;
    .locals 16

    invoke-static/range {p1 .. p1}, Lmaps/ac/cs;->a(Ljava/io/DataInput;)V

    invoke-static/range {p1 .. p1}, Lmaps/ac/bt;->a(Ljava/io/DataInput;)Lmaps/ac/bt;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ac/bt;->c()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/bt;->c()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lmaps/ac/bt;->d()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/bt;->d()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lmaps/ac/bt;->b()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lmaps/ac/bt;->b()I

    move-result v3

    if-eq v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected tile coords: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " but received "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v8

    if-lez v8, :cond_2

    add-int/lit16 v8, v8, 0x7d0

    :cond_2
    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v10, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v7, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    const/16 v1, 0xb

    move/from16 v0, p2

    if-ne v0, v1, :cond_5

    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_5

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    invoke-static/range {p1 .. p2}, Lmaps/ac/bp;->a(Ljava/io/DataInput;I)Lmaps/ac/bp;

    move-result-object v4

    const/16 v1, 0xb

    move/from16 v0, p2

    if-ne v0, v1, :cond_7

    const/4 v1, 0x0

    new-array v5, v1, [Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lmaps/ac/bn;->a(Ljava/io/DataInput;Lmaps/ac/bp;)Lmaps/ac/bn;

    move-result-object v6

    :cond_6
    new-instance v1, Lmaps/ac/bv;

    move/from16 v2, p2

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v6}, Lmaps/ac/bv;-><init>(ILmaps/ac/bt;Lmaps/ac/bp;[Ljava/lang/String;Lmaps/ac/bn;)V

    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v9, v3, [Lmaps/ac/n;

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_8

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lmaps/ac/cs;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/n;

    move-result-object v4

    aput-object v4, v9, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v2, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    invoke-static/range {p1 .. p1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v11, v3, [Lmaps/ac/cn;

    const/4 v2, 0x0

    :goto_5
    if-ge v2, v3, :cond_9

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lmaps/ac/cn;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/cn;

    move-result-object v4

    aput-object v4, v11, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_9
    new-instance v1, Lmaps/ac/cs;

    move-object/from16 v2, p0

    move/from16 v3, p4

    move/from16 v4, p3

    move/from16 v5, p5

    move-object v6, v10

    move-object/from16 v10, p6

    move-wide/from16 v12, p7

    move-wide/from16 v14, p9

    invoke-direct/range {v1 .. v15}, Lmaps/ac/cs;-><init>(Lmaps/ac/bt;IBI[Ljava/lang/String;[Ljava/lang/String;I[Lmaps/ac/n;Lmaps/ao/b;[Lmaps/ac/cn;JJ)V

    return-object v1
.end method

.method public static a(Lmaps/ac/bt;[BILmaps/ao/b;JJ)Lmaps/ac/cs;
    .locals 15

    new-instance v1, Lmaps/bw/a;

    move-object/from16 v0, p1

    invoke-direct {v1, v0}, Lmaps/bw/a;-><init>([B)V

    move/from16 v0, p2

    invoke-virtual {v1, v0}, Lmaps/bw/a;->skipBytes(I)I

    invoke-virtual {v1}, Lmaps/bw/a;->readInt()I

    move-result v2

    invoke-virtual {v1}, Lmaps/bw/a;->readInt()I

    move-result v3

    invoke-static {v1}, Lmaps/ac/cs;->a(Ljava/io/DataInput;)V

    invoke-virtual {v1}, Lmaps/bw/a;->readUnsignedShort()I

    move-result v4

    const/16 v5, 0xa

    if-eq v4, v5, :cond_0

    const/16 v5, 0xb

    if-eq v4, v5, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch: 10 or 11 expected, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {v1}, Lmaps/bw/a;->readInt()I

    move-result v5

    invoke-virtual {v1}, Lmaps/bw/a;->readLong()J

    move-result-wide v6

    invoke-virtual {v1}, Lmaps/bw/a;->readUnsignedByte()I

    move-result v1

    const/4 v8, 0x6

    new-array v8, v8, [J

    const/4 v9, 0x0

    int-to-long v10, v2

    aput-wide v10, v8, v9

    const/4 v2, 0x1

    int-to-long v9, v3

    aput-wide v9, v8, v2

    const/4 v2, 0x2

    int-to-long v3, v4

    aput-wide v3, v8, v2

    const/4 v2, 0x3

    int-to-long v3, v5

    aput-wide v3, v8, v2

    const/4 v2, 0x4

    aput-wide v6, v8, v2

    const/4 v2, 0x5

    int-to-long v3, v1

    aput-wide v3, v8, v2

    const/4 v1, 0x0

    aget-wide v1, v8, v1

    long-to-int v9, v1

    const/4 v1, 0x1

    aget-wide v1, v8, v1

    long-to-int v10, v1

    const/4 v1, 0x2

    aget-wide v1, v8, v1

    long-to-int v4, v1

    const/4 v1, 0x3

    aget-wide v1, v8, v1

    long-to-int v5, v1

    const/4 v1, 0x4

    aget-wide v6, v8, v1

    const/4 v1, 0x5

    aget-wide v1, v8, v1

    long-to-int v1, v1

    int-to-byte v11, v1

    add-int/lit8 v12, p2, 0x1b

    move-object/from16 v0, p1

    array-length v1, v0

    sub-int v13, v1, v12

    new-instance v14, Lmaps/ae/x;

    invoke-direct {v14}, Lmaps/ae/x;-><init>()V

    const/16 v1, 0x28

    new-array v8, v1, [B

    invoke-virtual {p0}, Lmaps/ac/bt;->c()I

    move-result v1

    invoke-virtual {p0}, Lmaps/ac/bt;->d()I

    move-result v2

    invoke-virtual {p0}, Lmaps/ac/bt;->b()I

    move-result v3

    invoke-static/range {v1 .. v8}, Lmaps/ae/x;->a(IIIIIJ[B)V

    invoke-virtual {v14, v8}, Lmaps/ae/x;->a([B)V

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v12, v13}, Lmaps/ae/x;->a([BII)V

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lmaps/ax/d;->a([BII)Lmaps/ax/e;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ax/e;->a()[B

    move-result-object v13

    invoke-virtual {v1}, Lmaps/ax/e;->b()I

    move-result v1

    new-instance v3, Lmaps/bw/a;

    invoke-direct {v3, v13}, Lmaps/bw/a;-><init>([B)V

    move-object v2, p0

    move v5, v11

    move v6, v9

    move v7, v10

    move-object/from16 v8, p3

    move-wide/from16 v9, p4

    move-wide/from16 v11, p6

    invoke-static/range {v2 .. v12}, Lmaps/ac/cs;->a(Lmaps/ac/bt;Ljava/io/DataInput;IBIILmaps/ao/b;JJ)Lmaps/ac/cs;

    move-result-object v2

    invoke-virtual {v3}, Lmaps/bw/a;->a()I

    move-result v3

    if-eq v3, v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Byte stream not fully read for: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    invoke-virtual {v1}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :try_start_1
    invoke-static {v13}, Lmaps/br/d;->a([B)V
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v2
.end method

.method static a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/n;
    .locals 12

    const/4 v1, 0x0

    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v0, 0x0

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v2

    const/16 v4, 0xb

    if-ne v2, v4, :cond_0

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    invoke-interface {p0, v2}, Ljava/io/DataInput;->skipBytes(I)I

    new-instance v2, Lmaps/ac/bf;

    invoke-direct {v2, v4}, Lmaps/ac/bf;-><init>(I)V

    move-object v10, v2

    :goto_0
    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown feature type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v2, Lmaps/ac/bf;

    const/4 v4, -0x1

    invoke-direct {v2, v4}, Lmaps/ac/bf;-><init>(I)V

    move-object v10, v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lmaps/ac/bv;->a()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lmaps/ac/l;

    invoke-virtual {v10}, Lmaps/ac/bf;->a()I

    invoke-static {}, Lmaps/ac/bl;->a()Lmaps/ac/bl;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/ac/l;-><init>(Lmaps/ac/bl;)V

    :goto_2
    return-object v0

    :pswitch_2
    invoke-virtual {p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v2

    invoke-static {p0, v2}, Lmaps/ac/az;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/az;

    move-result-object v2

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v5

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v6

    new-array v3, v6, [Lmaps/ac/ag;

    move v4, v0

    :goto_3
    if-ge v4, v6, :cond_2

    invoke-static {p0, p1, v5}, Lmaps/ac/ag;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bo;)Lmaps/ac/ag;

    move-result-object v7

    aput-object v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v6

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    invoke-static {v9, v4}, Lmaps/ac/ar;->a(II)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v1

    :cond_3
    :goto_4
    ushr-int/lit8 v8, v4, 0x2

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v9, v4, [I

    :goto_5
    if-ge v0, v4, :cond_5

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v11

    aput v11, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    invoke-static {v11, v4}, Lmaps/ac/ar;->a(II)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v1

    goto :goto_4

    :cond_5
    new-instance v0, Lmaps/ac/bg;

    invoke-virtual {v10}, Lmaps/ac/bf;->a()I

    invoke-virtual {v5}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v4

    invoke-virtual {v5}, Lmaps/ac/bo;->c()I

    invoke-virtual {v5}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v9}, Lmaps/ac/bg;-><init>(Lmaps/ac/o;Lmaps/ac/az;[Lmaps/ac/ag;Lmaps/ac/bl;Ljava/lang/String;III[I)V

    goto :goto_2

    :pswitch_3
    invoke-virtual {p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v2

    invoke-static {p0, v2}, Lmaps/ac/cj;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/cj;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/ac/cj;->a()I

    move-result v3

    new-array v3, v3, [B

    invoke-interface {p0, v3}, Ljava/io/DataInput;->readFully([B)V

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v6

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v8

    invoke-static {v9, v8}, Lmaps/ac/ar;->a(II)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static {p0}, Lmaps/ac/o;->a(Ljava/io/DataInput;)Lmaps/ac/p;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    new-array v9, v4, [I

    :goto_7
    if-ge v0, v4, :cond_8

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    invoke-static {v11, v8}, Lmaps/ac/ar;->a(II)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {p0}, Lmaps/ac/o;->b(Ljava/io/DataInput;)Lmaps/ac/q;

    move-result-object v1

    goto :goto_6

    :cond_8
    new-instance v0, Lmaps/ac/f;

    invoke-virtual {v10}, Lmaps/ac/bf;->a()I

    invoke-virtual {v6}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v4

    invoke-virtual {v6}, Lmaps/ac/bo;->c()I

    move-result v5

    invoke-virtual {v6}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v9}, Lmaps/ac/f;-><init>(Lmaps/ac/o;Lmaps/ac/cj;[BLmaps/ac/bl;ILjava/lang/String;II[I)V

    goto/16 :goto_2

    :pswitch_4
    invoke-static {p0, p1, v10}, Lmaps/ac/g;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/g;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_5
    invoke-virtual {p1}, Lmaps/ac/bv;->b()Lmaps/ac/bt;

    move-result-object v1

    invoke-static {p0, v1}, Lmaps/ac/az;->a(Ljava/io/DataInput;Lmaps/ac/bt;)Lmaps/ac/az;

    move-result-object v1

    invoke-static {p0, p1}, Lmaps/ac/bo;->a(Ljava/io/DataInput;Lmaps/ac/bv;)Lmaps/ac/bo;

    move-result-object v3

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v5, v2, [I

    :goto_8
    if-ge v0, v2, :cond_9

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v6

    aput v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    new-instance v0, Lmaps/ac/al;

    invoke-virtual {v10}, Lmaps/ac/bf;->a()I

    invoke-virtual {v3}, Lmaps/ac/bo;->a()Lmaps/ac/bl;

    move-result-object v2

    invoke-virtual {v3}, Lmaps/ac/bo;->c()I

    invoke-virtual {v3}, Lmaps/ac/bo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lmaps/ac/al;-><init>(Lmaps/ac/az;Lmaps/ac/bl;Ljava/lang/String;I[I)V

    goto/16 :goto_2

    :pswitch_6
    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    new-array v1, v1, [B

    invoke-interface {p0, v1}, Ljava/io/DataInput;->readFully([B)V

    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v3

    new-array v4, v3, [I

    :goto_9
    if-ge v0, v3, :cond_a

    invoke-static {p0}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_a
    new-instance v0, Lmaps/ac/bc;

    invoke-virtual {v10}, Lmaps/ac/bf;->a()I

    invoke-static {}, Lmaps/ac/bl;->a()Lmaps/ac/bl;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v4}, Lmaps/ac/bc;-><init>([BILmaps/ac/bl;[I)V

    goto/16 :goto_2

    :pswitch_7
    invoke-static {p0, p1, v10}, Lmaps/ac/aw;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/aw;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_8
    invoke-static {p0, p1, v10}, Lmaps/ac/aj;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/aj;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_9
    invoke-static {p0, p1, v10}, Lmaps/ac/cg;->a(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/n;

    move-result-object v0

    goto/16 :goto_2

    :pswitch_a
    invoke-static {p0, p1, v10}, Lmaps/ac/ak;->b(Ljava/io/DataInput;Lmaps/ac/bv;Lmaps/ac/bf;)Lmaps/ac/aj;

    move-result-object v0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method private static a(Ljava/io/DataInput;)V
    .locals 4

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    const v1, 0x44524154

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TILE_MAGIC expected. Found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-void
.end method

.method public static a(Lmaps/ac/bs;)Z
    .locals 2

    invoke-interface {p0}, Lmaps/ac/bs;->b()Lmaps/ao/b;

    move-result-object v0

    sget-object v1, Lmaps/ao/b;->q:Lmaps/ao/b;

    if-ne v0, v1, :cond_0

    move-object v0, p0

    check-cast v0, Lmaps/ac/cs;

    invoke-virtual {v0}, Lmaps/ac/cs;->p()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    instance-of v0, p0, Lmaps/ac/aq;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/ac/cs;)[Lmaps/ac/n;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    return-object v0
.end method

.method private m()Z
    .locals 1

    iget-byte v0, p0, Lmaps/ac/cs;->c:B

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r()I
    .locals 4

    invoke-static {}, Lmaps/ac/cs;->s()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lmaps/ac/cs;->s()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method private static s()J
    .locals 2

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/be/l;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->a:Lmaps/ac/bt;

    return-object v0
.end method

.method public a(I)Lmaps/ac/n;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->f:Lmaps/ao/b;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ac/cs;->b:I

    return v0
.end method

.method public d()Z
    .locals 4

    iget-wide v0, p0, Lmaps/ac/cs;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ac/cs;->j:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lmaps/ac/cs;->h:I

    return v0
.end method

.method public final f()Z
    .locals 4

    iget-wide v0, p0, Lmaps/ac/cs;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ac/cs;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 4

    invoke-static {}, Lmaps/ac/cs;->s()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    invoke-static {}, Lmaps/ac/cs;->s()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/ac/cs;->i:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/ac/cs;->i:J

    goto :goto_0
.end method

.method public final h()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lmaps/be/g;->c()Lmaps/be/l;

    move-result-object v2

    invoke-virtual {v2}, Lmaps/be/l;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    iget-byte v2, p0, Lmaps/ac/cs;->c:B

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    invoke-direct {p0}, Lmaps/ac/cs;->m()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lmaps/ac/cs;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public i()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public j()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->l:[Ljava/lang/String;

    return-object v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->d:[Lmaps/ac/n;

    array-length v0, v0

    return v0
.end method

.method public l()Lmaps/ac/cu;
    .locals 2

    new-instance v0, Lmaps/ac/cv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmaps/ac/cv;-><init>(Lmaps/ac/cs;B)V

    return-object v0
.end method

.method public n()J
    .locals 2

    iget-wide v0, p0, Lmaps/ac/cs;->j:J

    return-wide v0
.end method

.method public final o()I
    .locals 1

    iget v0, p0, Lmaps/ac/cs;->e:I

    return v0
.end method

.method public final p()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cs;->g:[Lmaps/ac/cn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/cs;->g:[Lmaps/ac/cn;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()J
    .locals 2

    iget-wide v0, p0, Lmaps/ac/cs;->i:J

    return-wide v0
.end method
