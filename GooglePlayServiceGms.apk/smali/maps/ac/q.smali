.class public final Lmaps/ac/q;
.super Lmaps/ac/o;


# instance fields
.field protected final b:J


# direct methods
.method constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Lmaps/ac/o;-><init>()V

    iput-wide p1, p0, Lmaps/ac/q;->b:J

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lmaps/ac/q;

    if-eqz v1, :cond_0

    check-cast p1, Lmaps/ac/q;

    iget-wide v1, p1, Lmaps/ac/q;->b:J

    iget-wide v3, p0, Lmaps/ac/q;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    instance-of v0, p1, Lmaps/ac/p;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/ac/p;

    iget-wide v0, p1, Lmaps/ac/p;->b:J

    iget-wide v2, p0, Lmaps/ac/q;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lmaps/ac/q;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    iget-wide v0, p0, Lmaps/ac/q;->b:J

    iget-wide v2, p0, Lmaps/ac/q;->b:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[hash:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lmaps/ac/q;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
