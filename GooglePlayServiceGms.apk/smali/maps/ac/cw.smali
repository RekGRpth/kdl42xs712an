.class public final Lmaps/ac/cw;
.super Lmaps/ac/cy;


# instance fields
.field private final b:[Lmaps/ac/av;

.field private final c:[Lmaps/ac/av;

.field private final d:Lmaps/ac/t;

.field private final e:Lmaps/ac/cx;

.field private final f:Lmaps/ac/bd;

.field private g:[[Lmaps/ac/av;


# direct methods
.method private constructor <init>([Lmaps/ac/av;)V
    .locals 10

    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0}, Lmaps/ac/cy;-><init>()V

    array-length v0, p1

    new-array v0, v0, [Lmaps/ac/av;

    iput-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    move v0, v4

    :goto_0
    if-ge v0, v7, :cond_0

    iget-object v1, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    new-instance v2, Lmaps/ac/av;

    invoke-direct {v2}, Lmaps/ac/av;-><init>()V

    aput-object v2, v1, v0

    aget-object v1, p1, v0

    iget-object v2, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    new-instance v0, Lmaps/ac/t;

    invoke-direct {v0, p1}, Lmaps/ac/t;-><init>([Lmaps/ac/av;)V

    iput-object v0, p0, Lmaps/ac/cw;->d:Lmaps/ac/t;

    iget-object v0, p0, Lmaps/ac/cw;->d:Lmaps/ac/t;

    invoke-virtual {v0}, Lmaps/ac/t;->a()Lmaps/ac/bd;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/cw;->f:Lmaps/ac/bd;

    iget-object v0, p0, Lmaps/ac/cw;->f:Lmaps/ac/bd;

    invoke-static {v0}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/cw;->e:Lmaps/ac/cx;

    iget-object v0, p0, Lmaps/ac/cw;->e:Lmaps/ac/cx;

    iget-boolean v0, v0, Lmaps/ac/cx;->a:Z

    iput-boolean v0, p0, Lmaps/ac/cw;->a:Z

    iget-boolean v0, p0, Lmaps/ac/cw;->a:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    filled-new-array {v9, v0}, [I

    move-result-object v0

    const-class v1, Lmaps/ac/av;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lmaps/ac/av;

    iput-object v0, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    move v5, v4

    move v0, v4

    move v2, v4

    :goto_1
    if-ge v5, v7, :cond_4

    iget-object v1, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v1, v1, v5

    iget-object v6, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    aget-object v6, v6, v5

    invoke-virtual {v1, v6}, Lmaps/ac/av;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v3

    :goto_2
    if-eq v1, v2, :cond_7

    if-lez v5, :cond_1

    if-ge v0, v8, :cond_1

    iget-object v2, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    add-int/lit8 v6, v5, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    aget-object v6, v6, v5

    invoke-direct {p0, v2, v6, v0}, Lmaps/ac/cw;->a(Lmaps/ac/av;Lmaps/ac/av;I)V

    add-int/lit8 v0, v0, 0x1

    :cond_1
    :goto_3
    if-lez v5, :cond_2

    iget-object v2, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    add-int/lit8 v6, v0, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v6, v6, v5

    aput-object v6, v2, v3

    :cond_2
    iget-object v2, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v2, v2, v0

    iget-object v6, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v6, v6, v5

    aput-object v6, v2, v4

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v2

    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    if-ge v0, v9, :cond_5

    iget-object v1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    aget-object v2, v2, v4

    invoke-direct {p0, v1, v2, v0}, Lmaps/ac/cw;->a(Lmaps/ac/av;Lmaps/ac/av;I)V

    :cond_5
    iget-object v0, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v0, v0, v8

    iget-object v1, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v1, v1, v4

    aput-object v1, v0, v3

    :cond_6
    return-void

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public static a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cw;
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [Lmaps/ac/av;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    new-instance v1, Lmaps/ac/cw;

    invoke-direct {v1, v0}, Lmaps/ac/cw;-><init>([Lmaps/ac/av;)V

    return-object v1
.end method

.method public static a(Lmaps/ac/cw;Lmaps/ac/av;)Lmaps/ac/cw;
    .locals 5

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0, p1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1, p1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v1

    iget-object v2, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-static {v2, p1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v2

    iget-object v3, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-static {v3, p1}, Lmaps/ac/av;->a(Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/av;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lmaps/ac/cw;->a(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Lmaps/ac/cw;

    move-result-object v0

    return-object v0
.end method

.method private a(Lmaps/ac/av;Lmaps/ac/av;I)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v1, 0x20000001

    const v2, -0x20000001

    iget v0, p2, Lmaps/ac/av;->a:I

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget v3, p1, Lmaps/ac/av;->a:I

    sub-int/2addr v0, v3

    int-to-double v3, v0

    iget v0, p2, Lmaps/ac/av;->a:I

    iget v5, p1, Lmaps/ac/av;->a:I

    sub-int/2addr v0, v5

    int-to-double v5, v0

    div-double/2addr v3, v5

    iget v0, p2, Lmaps/ac/av;->b:I

    iget v5, p1, Lmaps/ac/av;->b:I

    sub-int/2addr v0, v5

    int-to-double v5, v0

    mul-double/2addr v3, v5

    iget v0, p1, Lmaps/ac/av;->b:I

    int-to-double v5, v0

    add-double/2addr v3, v5

    double-to-int v0, v3

    iget v3, p1, Lmaps/ac/av;->a:I

    iget v4, p2, Lmaps/ac/av;->a:I

    if-le v3, v4, :cond_1

    iget-object v3, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lmaps/ac/av;

    invoke-direct {v4, v2, v0}, Lmaps/ac/av;-><init>(II)V

    aput-object v4, v3, v8

    iget-object v2, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v2, v2, p3

    new-instance v3, Lmaps/ac/av;

    invoke-direct {v3, v1, v0}, Lmaps/ac/av;-><init>(II)V

    aput-object v3, v2, v7

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lmaps/ac/av;

    invoke-direct {v4, v1, v0}, Lmaps/ac/av;-><init>(II)V

    aput-object v4, v3, v8

    iget-object v1, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v1, v1, p3

    new-instance v3, Lmaps/ac/av;

    invoke-direct {v3, v2, v0}, Lmaps/ac/av;-><init>(II)V

    aput-object v3, v1, v7

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lmaps/ac/av;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()Lmaps/ac/cx;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cw;->e:Lmaps/ac/cx;

    return-object v0
.end method

.method public final a(I[Lmaps/ac/av;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lmaps/ac/cw;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v0, v0, p1

    aget-object v0, v0, v1

    aput-object v0, p2, v1

    iget-object v0, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v0, v0, p1

    aget-object v0, v0, v2

    aput-object v0, p2, v2

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v0, v0, p1

    aput-object v0, p2, v1

    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    aget-object v0, v1, v0

    aput-object v0, p2, v2

    goto :goto_0
.end method

.method public final a(Lmaps/ac/av;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lmaps/ac/cw;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ac/cw;->d:Lmaps/ac/t;

    invoke-virtual {v0, p1}, Lmaps/ac/t;->a(Lmaps/ac/av;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    move v3, v2

    move v0, v2

    :goto_1
    const/4 v4, 0x6

    if-ge v3, v4, :cond_2

    iget-object v4, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v4, v4, v3

    aget-object v4, v4, v2

    iget-object v5, p0, Lmaps/ac/cw;->g:[[Lmaps/ac/av;

    aget-object v5, v5, v3

    aget-object v5, v5, v1

    invoke-static {v4, v5, p1}, Lmaps/ac/ax;->b(Lmaps/ac/av;Lmaps/ac/av;Lmaps/ac/av;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    if-ne v0, v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final a(Lmaps/ac/be;)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ac/cw;->e:Lmaps/ac/cx;

    invoke-virtual {p1}, Lmaps/ac/be;->a()Lmaps/ac/bd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/ac/cx;->b(Lmaps/ac/be;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lmaps/ac/be;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p1, v0}, Lmaps/ac/be;->a(I)Lmaps/ac/av;

    move-result-object v2

    invoke-virtual {p0, v2}, Lmaps/ac/cw;->a(Lmaps/ac/av;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b()Lmaps/ac/bd;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cw;->f:Lmaps/ac/bd;

    return-object v0
.end method

.method public final c()Lmaps/ac/be;
    .locals 1

    iget-object v0, p0, Lmaps/ac/cw;->d:Lmaps/ac/t;

    return-object v0
.end method

.method public final d()Lmaps/ac/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final e()Lmaps/ac/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lmaps/ac/cw;

    if-eqz v0, :cond_1

    check-cast p1, Lmaps/ac/cw;

    iget-object v0, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    iget-object v1, p1, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lmaps/ac/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final g()Lmaps/ac/av;
    .locals 2

    iget-object v0, p0, Lmaps/ac/cw;->c:[Lmaps/ac/av;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget-boolean v0, p0, Lmaps/ac/cw;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lmaps/ac/cw;->b:[Lmaps/ac/av;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
