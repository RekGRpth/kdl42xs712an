.class public final Lmaps/ac/y;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/bs;


# instance fields
.field private final a:Lmaps/ac/bt;

.field private final b:Lmaps/ao/b;

.field private final c:I

.field private final d:[B

.field private e:[Ljava/lang/String;

.field private f:[Ljava/lang/String;

.field private g:Ljava/util/List;

.field private h:I


# direct methods
.method public constructor <init>(Lmaps/ac/bt;I[BLmaps/ao/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ac/y;->h:I

    iput-object p1, p0, Lmaps/ac/y;->a:Lmaps/ac/bt;

    iput-object p4, p0, Lmaps/ac/y;->b:Lmaps/ao/b;

    iput p2, p0, Lmaps/ac/y;->c:I

    if-eqz p3, :cond_2

    array-length v0, p3

    if-eqz v0, :cond_2

    new-instance v0, Lmaps/bp/b;

    invoke-direct {v0}, Lmaps/bp/b;-><init>()V

    invoke-virtual {v0, p3}, Lmaps/bp/b;->a([B)[B

    move-result-object p3

    invoke-static {}, Lmaps/ap/p;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lmaps/bp/b;->a()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/ac/y;->e:[Ljava/lang/String;

    invoke-virtual {v0}, Lmaps/bp/b;->b()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lmaps/ac/y;->f:[Ljava/lang/String;

    invoke-virtual {v0}, Lmaps/bp/b;->c()I

    move-result v1

    iput v1, p0, Lmaps/ac/y;->h:I

    :cond_0
    sget-object v1, Lmaps/ao/b;->x:Lmaps/ao/b;

    if-ne p4, v1, :cond_1

    invoke-virtual {v0}, Lmaps/bp/b;->d()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/y;->g:Ljava/util/List;

    :cond_1
    aget-byte v0, p3, v2

    const/16 v1, 0x43

    if-ne v0, v1, :cond_2

    :try_start_0
    invoke-static {p3}, Lmaps/bi/c;->a([B)[B
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    :cond_2
    iget-object v0, p0, Lmaps/ac/y;->e:[Ljava/lang/String;

    if-nez v0, :cond_3

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/ac/y;->e:[Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lmaps/ac/y;->f:[Ljava/lang/String;

    if-nez v0, :cond_4

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lmaps/ac/y;->f:[Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lmaps/ac/y;->g:Ljava/util/List;

    if-nez v0, :cond_5

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lmaps/ac/y;->g:Ljava/util/List;

    :cond_5
    iput-object p3, p0, Lmaps/ac/y;->d:[B

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input image is not Compact JPEG"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->a:Lmaps/ac/bt;

    return-object v0
.end method

.method public final b()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->b:Lmaps/ao/b;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ac/y;->c:I

    return v0
.end method

.method public final d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final i()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->e:[Ljava/lang/String;

    return-object v0
.end method

.method public final j()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Lmaps/ac/y;->h:I

    return v0
.end method

.method public final l()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->g:Ljava/util/List;

    return-object v0
.end method

.method public final m()[B
    .locals 1

    iget-object v0, p0, Lmaps/ac/y;->d:[B

    return-object v0
.end method
