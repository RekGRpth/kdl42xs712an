.class public final Lmaps/ac/aa;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lmaps/ac/ad;


# direct methods
.method private constructor <init>(Lmaps/ac/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmaps/ac/aa;->a:Ljava/util/List;

    iput-object p3, p0, Lmaps/ac/aa;->b:Ljava/lang/String;

    iput-object p4, p0, Lmaps/ac/aa;->c:Ljava/lang/String;

    iput p5, p0, Lmaps/ac/aa;->d:I

    new-instance v0, Lmaps/ac/ad;

    invoke-direct {v0, p1, p6}, Lmaps/ac/ad;-><init>(Lmaps/ac/r;I)V

    iput-object v0, p0, Lmaps/ac/aa;->e:Lmaps/ac/ad;

    return-void
.end method

.method public static a(Lmaps/bv/a;)Lmaps/ac/aa;
    .locals 10

    const/4 v9, 0x7

    const/4 v6, 0x5

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x2

    invoke-virtual {p0, v7}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v8}, Lmaps/bv/a;->j(I)I

    move-result v3

    invoke-static {v3}, Lmaps/m/ck;->a(I)Ljava/util/ArrayList;

    move-result-object v2

    move v0, v5

    :goto_1
    if-ge v0, v3, :cond_2

    invoke-virtual {p0, v8, v0}, Lmaps/bv/a;->d(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lmaps/ac/r;->b(Ljava/lang/String;)Lmaps/ac/r;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v4

    if-nez v3, :cond_3

    if-eqz v4, :cond_9

    move-object v0, v4

    :goto_2
    move-object v3, v0

    :cond_3
    if-nez v4, :cond_4

    move-object v4, v3

    :cond_4
    invoke-virtual {p0, v6}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v6}, Lmaps/bv/a;->d(I)I

    move-result v5

    :cond_5
    const/high16 v6, -0x80000000

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lmaps/bv/a;->d(I)I

    move-result v6

    :cond_6
    invoke-virtual {p0, v9}, Lmaps/bv/a;->i(I)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, v9}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    invoke-virtual {v0, v7}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v7

    invoke-static {v7}, Lmaps/ac/av;->a(Lmaps/bv/a;)Lmaps/ac/av;

    move-result-object v7

    invoke-virtual {v0, v8}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    invoke-static {v0}, Lmaps/ac/av;->a(Lmaps/bv/a;)Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v7}, Lmaps/ac/av;->f()I

    move-result v8

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v9

    if-le v8, v9, :cond_7

    invoke-virtual {v0}, Lmaps/ac/av;->f()I

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    add-int/2addr v8, v9

    invoke-virtual {v0, v8}, Lmaps/ac/av;->a(I)V

    :cond_7
    new-instance v8, Lmaps/ac/bd;

    invoke-direct {v8, v7, v0}, Lmaps/ac/bd;-><init>(Lmaps/ac/av;Lmaps/ac/av;)V

    invoke-static {v8}, Lmaps/ac/cx;->a(Lmaps/ac/bd;)Lmaps/ac/cx;

    :cond_8
    new-instance v0, Lmaps/ac/aa;

    invoke-direct/range {v0 .. v6}, Lmaps/ac/aa;-><init>(Lmaps/ac/r;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_9
    const-string v0, ""

    goto :goto_2
.end method


# virtual methods
.method public final a()Lmaps/ac/ad;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aa;->e:Lmaps/ac/ad;

    return-object v0
.end method

.method public final b()Lmaps/ac/r;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aa;->e:Lmaps/ac/ad;

    invoke-virtual {v0}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aa;->a:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/ac/aa;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lmaps/ac/aa;->d:I

    return v0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lmaps/ac/aa;->e:Lmaps/ac/ad;

    invoke-virtual {v0}, Lmaps/ac/ad;->b()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Level: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ac/aa;->e:Lmaps/ac/ad;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
