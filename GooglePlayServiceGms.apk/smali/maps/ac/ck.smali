.class public final Lmaps/ac/ck;
.super Ljava/lang/Object;


# direct methods
.method public static a(III)J
    .locals 12

    const-wide/16 v10, 0x1

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    mul-int/lit8 v6, p2, 0x2

    move v2, v3

    move-wide v0, v4

    :goto_0
    if-ge v2, v6, :cond_3

    and-int/lit8 v7, p0, 0x1

    and-int/lit8 v8, p1, 0x1

    if-nez v7, :cond_0

    if-nez v8, :cond_0

    shl-long v7, v4, v2

    or-long/2addr v0, v7

    :goto_1
    shr-int/lit8 p0, p0, 0x1

    shr-int/lit8 p1, p1, 0x1

    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_0
    if-nez v7, :cond_1

    if-ne v8, v9, :cond_1

    shl-long v7, v10, v2

    or-long/2addr v0, v7

    goto :goto_1

    :cond_1
    if-ne v7, v9, :cond_2

    if-ne v8, v9, :cond_2

    const-wide/16 v7, 0x2

    shl-long/2addr v7, v2

    or-long/2addr v0, v7

    goto :goto_1

    :cond_2
    const-wide/16 v7, 0x3

    shl-long/2addr v7, v2

    or-long/2addr v0, v7

    goto :goto_1

    :cond_3
    if-nez p0, :cond_4

    if-nez p1, :cond_4

    const/16 v2, 0x3e

    if-le v6, v2, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid map tile proto X = %d, Y = %d, zoom = %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    shl-long v2, v10, v6

    or-long/2addr v0, v2

    return-wide v0
.end method
