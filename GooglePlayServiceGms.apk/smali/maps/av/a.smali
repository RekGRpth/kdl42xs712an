.class public final Lmaps/av/a;
.super Ljava/lang/Object;


# static fields
.field public static final s:Lmaps/av/a;

.field public static final t:Lmaps/av/a;

.field public static final u:Lmaps/av/a;


# instance fields
.field public final a:Lmaps/aj/ah;

.field public final b:I

.field public final c:I

.field public final d:Lmaps/aj/ah;

.field public final e:F

.field public final f:I

.field public final g:I

.field public final h:Lmaps/aj/ah;

.field public final i:F

.field public final j:I

.field public final k:I

.field public final l:F

.field public final m:F

.field public final n:F

.field public final o:Z

.field public final p:Z

.field public final q:Z

.field public final r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 18

    new-instance v0, Lmaps/av/a;

    sget-object v1, Lmaps/aj/ag;->d:Lmaps/aj/ah;

    const/16 v2, 0x12

    const/16 v3, 0xc

    sget-object v4, Lmaps/aj/ag;->c:Lmaps/aj/ah;

    const v5, 0x3fcccccd    # 1.6f

    const/16 v6, 0xe

    const/16 v7, 0x20

    sget-object v8, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v9, 0x8

    const/16 v10, 0x10

    const v11, 0x3feccccd    # 1.85f

    const/high16 v12, 0x3fc00000    # 1.5f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v0 .. v17}, Lmaps/av/a;-><init>(Lmaps/aj/ah;IILmaps/aj/ah;FIILmaps/aj/ah;IIFFFZZZZ)V

    sput-object v0, Lmaps/av/a;->s:Lmaps/av/a;

    new-instance v0, Lmaps/av/a;

    sget-object v1, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v2, 0xb

    const/16 v3, 0xb

    sget-object v4, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v6, 0x8

    const/16 v7, 0x18

    sget-object v8, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v9, 0x8

    const/16 v10, 0x10

    const v11, 0x3f99999a    # 1.2f

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    invoke-direct/range {v0 .. v17}, Lmaps/av/a;-><init>(Lmaps/aj/ah;IILmaps/aj/ah;FIILmaps/aj/ah;IIFFFZZZZ)V

    sput-object v0, Lmaps/av/a;->t:Lmaps/av/a;

    new-instance v0, Lmaps/av/a;

    sget-object v1, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const v5, 0x3f99999a    # 1.2f

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v9, 0xa

    const/16 v10, 0x14

    const v11, 0x3f99999a    # 1.2f

    const/high16 v12, 0x3fa00000    # 1.25f

    const/high16 v13, 0x3fc00000    # 1.5f

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    invoke-direct/range {v0 .. v17}, Lmaps/av/a;-><init>(Lmaps/aj/ah;IILmaps/aj/ah;FIILmaps/aj/ah;IIFFFZZZZ)V

    sput-object v0, Lmaps/av/a;->u:Lmaps/av/a;

    new-instance v0, Lmaps/av/a;

    sget-object v1, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v2, 0xd

    const/16 v3, 0xd

    sget-object v4, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const v5, 0x3f99999a    # 1.2f

    const/16 v6, 0xa

    const/16 v7, 0x1c

    sget-object v8, Lmaps/aj/ag;->b:Lmaps/aj/ah;

    const/16 v9, 0xa

    const/16 v10, 0x14

    const v11, 0x3f99999a    # 1.2f

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, 0x3fa66666    # 1.3f

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    invoke-direct/range {v0 .. v17}, Lmaps/av/a;-><init>(Lmaps/aj/ah;IILmaps/aj/ah;FIILmaps/aj/ah;IIFFFZZZZ)V

    return-void
.end method

.method private constructor <init>(Lmaps/aj/ah;IILmaps/aj/ah;FIILmaps/aj/ah;IIFFFZZZZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/av/a;->a:Lmaps/aj/ah;

    iput p2, p0, Lmaps/av/a;->b:I

    iput p3, p0, Lmaps/av/a;->c:I

    iput-object p4, p0, Lmaps/av/a;->d:Lmaps/aj/ah;

    iput p5, p0, Lmaps/av/a;->e:F

    iput p6, p0, Lmaps/av/a;->f:I

    iput p7, p0, Lmaps/av/a;->g:I

    iput-object p8, p0, Lmaps/av/a;->h:Lmaps/aj/ah;

    const v1, 0x3f99999a    # 1.2f

    iput v1, p0, Lmaps/av/a;->i:F

    iput p9, p0, Lmaps/av/a;->j:I

    iput p10, p0, Lmaps/av/a;->k:I

    iput p11, p0, Lmaps/av/a;->l:F

    iput p12, p0, Lmaps/av/a;->m:F

    iput p13, p0, Lmaps/av/a;->n:F

    move/from16 v0, p14

    iput-boolean v0, p0, Lmaps/av/a;->o:Z

    move/from16 v0, p15

    iput-boolean v0, p0, Lmaps/av/a;->p:Z

    move/from16 v0, p16

    iput-boolean v0, p0, Lmaps/av/a;->q:Z

    move/from16 v0, p17

    iput-boolean v0, p0, Lmaps/av/a;->r:Z

    return-void
.end method
