.class final Lmaps/ay/ak;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/an/b;


# instance fields
.field private a:Lmaps/an/t;

.field private b:Ljava/util/List;

.field private c:Ljava/lang/ref/WeakReference;

.field private d:Landroid/content/res/Resources;

.field private e:Lmaps/an/c;

.field private f:Lmaps/ar/a;

.field private g:Lmaps/v/k;

.field private h:Z

.field private i:Lmaps/ac/au;

.field private j:Lmaps/ay/al;

.field private k:I

.field private l:Ljava/util/Map;

.field private m:F

.field private n:F

.field private o:F

.field private p:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lmaps/an/ao;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Lmaps/an/ao;-><init>(FF)V

    return-void
.end method

.method private declared-synchronized a(Lmaps/as/a;I)Lmaps/as/b;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ak;->l:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/b;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/as/b;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lmaps/as/b;-><init>(Lmaps/as/a;Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/as/b;->c(Z)V

    iget-object v1, p0, Lmaps/ay/ak;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lmaps/as/b;->a(Landroid/content/res/Resources;I)V

    iget-object v1, p0, Lmaps/ay/ak;->l:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 8

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ak;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/as/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v4, p0, Lmaps/ay/ak;->g:Lmaps/v/k;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lmaps/v/k;->a(J)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    iput-boolean v4, p0, Lmaps/ay/ak;->h:Z

    :cond_2
    iget-object v4, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget v4, v4, Lmaps/ay/al;->c:I

    iget-object v5, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget v5, v5, Lmaps/ay/al;->b:I

    if-eq v4, v5, :cond_4

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lmaps/ay/ak;->i:Lmaps/ac/au;

    invoke-virtual {v2}, Lmaps/ac/au;->j()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_5

    iget-object v2, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget v2, v2, Lmaps/ay/al;->c:I

    :goto_2
    iget v3, p0, Lmaps/ay/ak;->k:I

    if-eq v3, v2, :cond_3

    iput v2, p0, Lmaps/ay/ak;->k:I

    const/4 v2, 0x1

    iput-boolean v2, p0, Lmaps/ay/ak;->h:Z

    :cond_3
    iget-boolean v2, p0, Lmaps/ay/ak;->h:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lmaps/ay/ak;->h:Z

    iget-object v2, p0, Lmaps/ay/ak;->a:Lmaps/an/t;

    iget v3, p0, Lmaps/ay/ak;->k:I

    invoke-direct {p0, v0, v3}, Lmaps/ay/ak;->a(Lmaps/as/a;I)Lmaps/as/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmaps/an/t;->a(Lmaps/an/q;)V

    iget-object v0, p0, Lmaps/ay/ak;->g:Lmaps/v/k;

    iget-object v2, p0, Lmaps/ay/ak;->i:Lmaps/ac/au;

    invoke-interface {v0, v2}, Lmaps/v/k;->a(Lmaps/ac/au;)Z

    iget-object v0, p0, Lmaps/ay/ak;->i:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->a()Lmaps/ac/av;

    move-result-object v3

    iget-object v0, p0, Lmaps/ay/ak;->f:Lmaps/ar/a;

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    iget-object v0, p0, Lmaps/ay/ak;->i:Lmaps/ac/au;

    invoke-virtual {v0}, Lmaps/ac/au;->c()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {v3}, Lmaps/ac/av;->e()D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v2, v4

    iget-object v0, p0, Lmaps/ay/ak;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/an/t;

    invoke-virtual {v0, v3, v2}, Lmaps/an/t;->b(Lmaps/ac/av;F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v2, v3

    goto :goto_1

    :cond_5
    :try_start_2
    iget-object v2, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget v2, v2, Lmaps/ay/al;->b:I

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lmaps/ay/ak;->n:F

    move v2, v0

    :goto_4
    iget v4, p0, Lmaps/ay/ak;->p:F

    iget-object v0, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_5
    mul-float/2addr v0, v4

    mul-float/2addr v0, v2

    iget-object v1, p0, Lmaps/ay/ak;->a:Lmaps/an/t;

    invoke-virtual {v1, v3, v0}, Lmaps/an/t;->a(Lmaps/ac/av;F)V

    iget-object v0, p0, Lmaps/ay/ak;->j:Lmaps/ay/al;

    iget-boolean v0, v0, Lmaps/ay/al;->a:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmaps/ay/ak;->a:Lmaps/an/t;

    iget-object v1, p0, Lmaps/ay/ak;->i:Lmaps/ac/au;

    invoke-virtual {v1}, Lmaps/ac/au;->b()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lmaps/an/t;->a(F)V

    :cond_7
    iget-object v0, p0, Lmaps/ay/ak;->e:Lmaps/an/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ak;->e:Lmaps/an/c;

    sget-object v1, Lmaps/an/au;->b:Lmaps/an/au;

    invoke-interface {v0, p0, v1}, Lmaps/an/c;->a(Lmaps/an/b;Lmaps/an/ar;)V

    goto/16 :goto_0

    :cond_8
    iget v0, p0, Lmaps/ay/ak;->m:F

    move v2, v0

    goto :goto_4

    :cond_9
    iget v0, p0, Lmaps/ay/ak;->o:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5
.end method

.method final declared-synchronized a(FFF)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lmaps/ay/ak;->m:F

    iput p2, p0, Lmaps/ay/ak;->n:F

    iput p3, p0, Lmaps/ay/ak;->o:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/an/c;)V
    .locals 1

    iput-object p1, p0, Lmaps/ay/ak;->e:Lmaps/an/c;

    sget-object v0, Lmaps/an/au;->b:Lmaps/an/au;

    invoke-interface {p1, p0, v0}, Lmaps/an/c;->a(Lmaps/an/b;Lmaps/an/ar;)V

    return-void
.end method
