.class final Lmaps/ay/s;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/an/b;


# instance fields
.field private a:Lmaps/as/a;

.field private b:Lmaps/an/c;

.field private c:Lmaps/ar/a;

.field private d:Ljava/util/List;

.field private synthetic e:Lmaps/ay/r;


# direct methods
.method private static a(Ljava/util/List;)V
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lmaps/au/af;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/ay/s;->d:Ljava/util/List;

    invoke-static {v0}, Lmaps/ay/s;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmaps/ay/s;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    invoke-static {v0}, Lmaps/ay/r;->a(Lmaps/ay/r;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lmaps/ay/s;->a(Ljava/util/List;)V

    iget-object v0, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    iget-object v2, p0, Lmaps/ay/s;->c:Lmaps/ar/a;

    invoke-virtual {v0, v2}, Lmaps/ay/r;->a(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    iget-object v2, p0, Lmaps/ay/s;->c:Lmaps/ar/a;

    invoke-static {v0, v2}, Lmaps/ay/r;->a(Lmaps/ay/r;Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    iget-object v2, p0, Lmaps/ay/s;->c:Lmaps/ar/a;

    invoke-static {v0, v2}, Lmaps/ay/r;->b(Lmaps/ay/r;Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/s;->e:Lmaps/ay/r;

    invoke-static {v0}, Lmaps/ay/r;->a(Lmaps/ay/r;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    iget-object v3, p0, Lmaps/ay/s;->a:Lmaps/as/a;

    invoke-virtual {v0, v3}, Lmaps/au/af;->a(Lmaps/as/a;)V

    iget-object v3, p0, Lmaps/ay/s;->a:Lmaps/as/a;

    invoke-virtual {v0}, Lmaps/au/af;->v()V

    const/16 v3, 0xff

    invoke-virtual {v0, v3}, Lmaps/au/af;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public final a(Lmaps/an/c;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/s;->b:Lmaps/an/c;

    return-void
.end method

.method public final declared-synchronized a(Lmaps/au/af;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/s;->a:Lmaps/as/a;

    invoke-virtual {p1, v0}, Lmaps/au/af;->b(Lmaps/as/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
