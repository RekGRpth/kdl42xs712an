.class public final enum Lmaps/ay/v;
.super Ljava/lang/Enum;


# static fields
.field private static enum A:Lmaps/ay/v;

.field private static enum B:Lmaps/ay/v;

.field private static final synthetic C:[Lmaps/ay/v;

.field public static final enum a:Lmaps/ay/v;

.field public static final enum b:Lmaps/ay/v;

.field public static final enum c:Lmaps/ay/v;

.field public static final enum d:Lmaps/ay/v;

.field public static final enum e:Lmaps/ay/v;

.field public static final enum f:Lmaps/ay/v;

.field public static final enum g:Lmaps/ay/v;

.field public static final enum h:Lmaps/ay/v;

.field public static final enum i:Lmaps/ay/v;

.field public static final enum j:Lmaps/ay/v;

.field public static final enum k:Lmaps/ay/v;

.field public static final enum l:Lmaps/ay/v;

.field public static final enum m:Lmaps/ay/v;

.field public static final enum n:Lmaps/ay/v;

.field public static final enum o:Lmaps/ay/v;

.field public static final enum p:Lmaps/ay/v;

.field public static final enum q:Lmaps/ay/v;

.field public static final enum r:Lmaps/ay/v;

.field public static final enum s:Lmaps/ay/v;

.field private static enum t:Lmaps/ay/v;

.field private static enum u:Lmaps/ay/v;

.field private static enum v:Lmaps/ay/v;

.field private static enum w:Lmaps/ay/v;

.field private static enum x:Lmaps/ay/v;

.field private static enum y:Lmaps/ay/v;

.field private static enum z:Lmaps/ay/v;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lmaps/ay/v;

    const-string v1, "UNUSED"

    invoke-direct {v0, v1, v3}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->a:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "BASE_IMAGERY"

    invoke-direct {v0, v1, v4}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->b:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "VECTORS"

    invoke-direct {v0, v1, v5}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->c:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "NIGHT_DIMMER"

    invoke-direct {v0, v1, v6}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->d:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "DESATURATE"

    invoke-direct {v0, v1, v7}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->e:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "TRAFFIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->f:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "INDOOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->g:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LAYER_SHAPES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->t:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "TRANSIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->u:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "BUILDINGS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->h:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "POLYLINE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->v:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LABELS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->i:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "FADE_OUT_OVERLAY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->w:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "ROUTE_OVERVIEW_POLYLINE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->x:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "TURN_ARROW_OVERLAY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->y:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "IMPORTANT_LABELS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->j:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "DEBUG_LABELS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->k:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "INTERSECTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->z:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "SKY"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->l:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "MY_LOCATION_OVERLAY_DA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->m:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LAYERS_RAW_GPS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->A:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LAYER_MARKERS_SHADOW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->n:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LAYER_MARKERS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->o:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "MY_LOCATION_OVERLAY_VECTORMAPS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->p:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "COMPASS_OVERLAY"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->q:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "LOADING_SPINNY"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->B:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "BUBBLE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->r:Lmaps/ay/v;

    new-instance v0, Lmaps/ay/v;

    const-string v1, "HEADS_UP_DISPLAY"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lmaps/ay/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmaps/ay/v;->s:Lmaps/ay/v;

    const/16 v0, 0x1c

    new-array v0, v0, [Lmaps/ay/v;

    sget-object v1, Lmaps/ay/v;->a:Lmaps/ay/v;

    aput-object v1, v0, v3

    sget-object v1, Lmaps/ay/v;->b:Lmaps/ay/v;

    aput-object v1, v0, v4

    sget-object v1, Lmaps/ay/v;->c:Lmaps/ay/v;

    aput-object v1, v0, v5

    sget-object v1, Lmaps/ay/v;->d:Lmaps/ay/v;

    aput-object v1, v0, v6

    sget-object v1, Lmaps/ay/v;->e:Lmaps/ay/v;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lmaps/ay/v;->f:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lmaps/ay/v;->g:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lmaps/ay/v;->t:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lmaps/ay/v;->u:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lmaps/ay/v;->h:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lmaps/ay/v;->v:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lmaps/ay/v;->i:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lmaps/ay/v;->w:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lmaps/ay/v;->x:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lmaps/ay/v;->y:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lmaps/ay/v;->j:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lmaps/ay/v;->k:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lmaps/ay/v;->z:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lmaps/ay/v;->l:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lmaps/ay/v;->m:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lmaps/ay/v;->A:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lmaps/ay/v;->n:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lmaps/ay/v;->o:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lmaps/ay/v;->p:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lmaps/ay/v;->q:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lmaps/ay/v;->B:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lmaps/ay/v;->r:Lmaps/ay/v;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lmaps/ay/v;->s:Lmaps/ay/v;

    aput-object v2, v0, v1

    sput-object v0, Lmaps/ay/v;->C:[Lmaps/ay/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmaps/ay/v;
    .locals 1

    const-class v0, Lmaps/ay/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmaps/ay/v;

    return-object v0
.end method

.method public static values()[Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->C:[Lmaps/ay/v;

    invoke-virtual {v0}, [Lmaps/ay/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmaps/ay/v;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    invoke-virtual {p0}, Lmaps/ay/v;->ordinal()I

    move-result v0

    return v0
.end method
