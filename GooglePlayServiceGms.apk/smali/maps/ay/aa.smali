.class public Lmaps/ay/aa;
.super Lmaps/ay/u;


# instance fields
.field private a:Lmaps/ay/v;

.field private b:[I


# direct methods
.method public constructor <init>(Lmaps/ay/v;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    sget v0, Lmaps/ap/b;->g:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lmaps/ay/aa;->b:[I

    iput-object p1, p0, Lmaps/ay/aa;->a:Lmaps/ay/v;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ap/b;)V
    .locals 3

    iget-object v0, p0, Lmaps/ay/aa;->b:[I

    invoke-virtual {p1}, Lmaps/ap/b;->a()I

    move-result v1

    const/high16 v2, -0x80000000

    aput v2, v0, v1

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lmaps/ay/aa;->b:[I

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/ap/b;->a()I

    move-result v1

    aget v0, v0, v1

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v1

    if-gtz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/high16 v2, -0x40800000    # -1.0f

    invoke-interface {v1, v3, v3, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-static {v1, v0}, Lmaps/al/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    iget-object v0, p1, Lmaps/as/a;->f:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/aa;->b:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([II)V

    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    iget-object v0, p0, Lmaps/ay/aa;->a:Lmaps/ay/v;

    return-object v0
.end method
