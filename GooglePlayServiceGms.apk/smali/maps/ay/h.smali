.class public final Lmaps/ay/h;
.super Lmaps/ay/u;


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Lmaps/as/b;

.field private c:Lmaps/ac/av;

.field private d:F

.field private e:I

.field private f:I

.field private g:I

.field private final h:I

.field private i:I

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lmaps/v/e;

.field private volatile n:I

.field private final o:I

.field private final p:Lmaps/ay/k;

.field private q:Lmaps/ay/j;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lmaps/ay/k;)V
    .locals 1

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/h;->k:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/h;->l:Z

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/ay/h;->n:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/h;->q:Lmaps/ay/j;

    iput-object p1, p0, Lmaps/ay/h;->a:Landroid/content/res/Resources;

    iput-object p2, p0, Lmaps/ay/h;->p:Lmaps/ay/k;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->b()Z

    :cond_0
    sget v0, Lmaps/b/d;->g:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lmaps/ay/h;->h:I

    sget v0, Lmaps/b/c;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lmaps/ay/h;->o:I

    return-void
.end method

.method private a(FF)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    iget v2, p0, Lmaps/ay/h;->n:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lmaps/ay/h;->f:I

    add-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    sub-float v2, p2, v2

    sget-object v3, Lmaps/ay/i;->a:[I

    iget-object v4, p0, Lmaps/ay/h;->p:Lmaps/ay/k;

    invoke-virtual {v4}, Lmaps/ay/k;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    iget v3, p0, Lmaps/ay/h;->g:I

    add-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    add-float/2addr v3, p1

    iget v4, p0, Lmaps/ay/h;->i:I

    iget v5, p0, Lmaps/ay/h;->h:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    iget v4, p0, Lmaps/ay/h;->i:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_0

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_0

    iget v3, p0, Lmaps/ay/h;->h:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_0
    iget v3, p0, Lmaps/ay/h;->e:I

    add-int/lit8 v3, v3, 0x0

    int-to-float v3, v3

    sub-float v3, p1, v3

    cmpl-float v4, v3, v6

    if-ltz v4, :cond_0

    iget v4, p0, Lmaps/ay/h;->h:I

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_0

    cmpl-float v3, v2, v6

    if-ltz v3, :cond_0

    iget v3, p0, Lmaps/ay/h;->h:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/h;->m:Lmaps/v/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(III)V
    .locals 0

    iput p1, p0, Lmaps/ay/h;->e:I

    iput p3, p0, Lmaps/ay/h;->g:I

    iput p2, p0, Lmaps/ay/h;->f:I

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 8

    const/4 v5, 0x1

    const/high16 v7, 0x420c0000    # 35.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/ay/h;->n:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    if-nez v0, :cond_3

    new-instance v0, Lmaps/as/b;

    invoke-direct {v0, p1}, Lmaps/as/b;-><init>(Lmaps/as/a;)V

    iput-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    iget-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    invoke-virtual {v0, v5}, Lmaps/as/b;->c(Z)V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->b()Z

    :cond_2
    sget v0, Lmaps/b/e;->h:I

    iget-object v2, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    iget-object v3, p0, Lmaps/ay/h;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v3, v0}, Lmaps/as/b;->a(Landroid/content/res/Resources;I)V

    :cond_3
    invoke-virtual {p1}, Lmaps/as/a;->r()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v0, 0x2300

    const/16 v3, 0x2200

    const/16 v4, 0x2100

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const/16 v0, 0x303

    invoke-interface {v2, v5, v0}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    iget-object v0, p1, Lmaps/as/a;->b:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    invoke-direct {p0}, Lmaps/ay/h;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/ay/h;->m:Lmaps/v/e;

    invoke-virtual {v0, p1}, Lmaps/v/e;->a(Lmaps/as/a;)I

    move-result v0

    iput v0, p0, Lmaps/ay/h;->n:I

    iget v0, p0, Lmaps/ay/h;->n:I

    if-nez v0, :cond_4

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/h;->m:Lmaps/v/e;

    :cond_4
    iget-boolean v0, p0, Lmaps/ay/h;->j:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lmaps/ay/h;->o:I

    invoke-static {v2, v0}, Lmaps/ay/h;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    :goto_1
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    iget v3, p0, Lmaps/ay/h;->d:F

    iget-boolean v0, p0, Lmaps/ay/h;->j:Z

    if-eqz v0, :cond_7

    const v0, 0x3faa3d71    # 1.33f

    :goto_2
    mul-float/2addr v0, v3

    iget-object v3, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    invoke-static {p1, p2, v3, v0}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-virtual {p2}, Lmaps/ar/a;->o()F

    move-result v0

    cmpl-float v3, v0, v7

    if-lez v3, :cond_5

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v3

    neg-float v3, v3

    invoke-interface {v2, v3, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    sub-float/2addr v0, v7

    invoke-interface {v2, v0, v1, v6, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    invoke-virtual {p2}, Lmaps/ar/a;->n()F

    move-result v0

    invoke-interface {v2, v0, v6, v6, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    :cond_5
    iget-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    invoke-virtual {v0, v2}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p1, Lmaps/as/a;->f:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v3, 0x4

    invoke-interface {v2, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_0

    :cond_6
    iget v0, p0, Lmaps/ay/h;->n:I

    iget v3, p0, Lmaps/ay/h;->n:I

    iget v4, p0, Lmaps/ay/h;->n:I

    iget v5, p0, Lmaps/ay/h;->n:I

    invoke-interface {v2, v0, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public final a(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/ay/h;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmaps/ay/h;->au_()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(FFLmaps/ar/a;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2}, Lmaps/ay/h;->a(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lmaps/ay/h;->j:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final at_()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/h;->j:Z

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/h;->k:Z

    iget-boolean v0, p0, Lmaps/ay/h;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/ay/h;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/ay/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ay/h;->n:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/ay/h;->n:I

    goto :goto_0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget v0, p0, Lmaps/ay/h;->h:I

    div-int/lit8 v0, v0, 0x2

    sget-object v1, Lmaps/ay/i;->a:[I

    iget-object v2, p0, Lmaps/ay/h;->p:Lmaps/ay/k;

    invoke-virtual {v2}, Lmaps/ay/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v1

    add-int/lit8 v2, v0, 0x0

    iget v3, p0, Lmaps/ay/h;->g:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-int/lit8 v2, v0, 0x0

    iget v3, p0, Lmaps/ay/h;->f:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    :goto_0
    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v1

    iput v1, p0, Lmaps/ay/h;->i:I

    iget-object v1, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    if-eqz v1, :cond_0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    invoke-virtual {p1, v1, v5}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->a(FF)F

    move-result v0

    iput v0, p0, Lmaps/ay/h;->d:F

    :cond_0
    iget-boolean v0, p0, Lmaps/ay/h;->k:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lmaps/ar/a;->n()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lmaps/ay/h;->l:Z

    if-nez v0, :cond_1

    iput-boolean v6, p0, Lmaps/ay/h;->l:Z

    new-instance v0, Lmaps/v/e;

    const-wide/16 v1, 0x7d0

    const-wide/16 v3, 0x1f4

    sget-object v5, Lmaps/v/g;->b:Lmaps/v/g;

    invoke-direct/range {v0 .. v5}, Lmaps/v/e;-><init>(JJLmaps/v/g;)V

    iput-object v0, p0, Lmaps/ay/h;->m:Lmaps/v/e;

    :cond_1
    :goto_1
    return v6

    :pswitch_0
    add-int/lit8 v1, v0, 0x0

    iget v2, p0, Lmaps/ay/h;->e:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    add-int/lit8 v2, v0, 0x0

    iget v3, p0, Lmaps/ay/h;->f:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/h;->c:Lmaps/ac/av;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/h;->m:Lmaps/v/e;

    iput-boolean v5, p0, Lmaps/ay/h;->l:Z

    const/high16 v0, 0x10000

    iput v0, p0, Lmaps/ay/h;->n:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/h;->b:Lmaps/as/b;

    :cond_0
    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->q:Lmaps/ay/v;

    return-object v0
.end method
