.class public final Lmaps/ay/ai;
.super Lmaps/ay/c;


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private c:Lmaps/am/d;

.field private final d:I


# direct methods
.method public constructor <init>(ILmaps/ay/o;)V
    .locals 1

    invoke-direct {p0, p2}, Lmaps/ay/c;-><init>(Lmaps/ay/o;)V

    iput p1, p0, Lmaps/ay/ai;->d:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/List;FFLmaps/ar/a;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    instance-of v2, v0, Lmaps/ay/b;

    if-eqz v2, :cond_0

    check-cast v0, Lmaps/ay/b;

    invoke-interface {v0, p2, p3, p4}, Lmaps/ay/b;->a(FFLmaps/ar/a;)I

    move-result v2

    if-ge v2, p5, :cond_0

    new-instance v3, Lmaps/ay/n;

    invoke-direct {v3, v0, p0, v2}, Lmaps/ay/n;-><init>(Lmaps/ay/b;Lmaps/ay/c;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final a(Lmaps/am/d;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/ai;->c:Lmaps/am/d;

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lmaps/ay/ai;->c:Lmaps/am/d;

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    sget-object v1, Lmaps/ap/b;->f:Lmaps/ap/b;

    if-eq v0, v1, :cond_0

    invoke-virtual {p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v0

    sget-object v1, Lmaps/ap/b;->e:Lmaps/ap/b;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/as/a;->r()V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x303

    invoke-interface {v0, v4, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/ay/ai;->c:Lmaps/am/d;

    invoke-virtual {v0}, Lmaps/am/d;->e()Lmaps/am/f;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-virtual {v0}, Lmaps/am/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lmaps/am/f;->a()Lmaps/au/m;

    move-result-object v1

    iget v2, p0, Lmaps/ay/ai;->d:I

    if-ne v2, v4, :cond_3

    invoke-virtual {v1}, Lmaps/au/m;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_3
    iget v2, p0, Lmaps/ay/ai;->d:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    invoke-virtual {v1}, Lmaps/au/m;->k()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_4
    iget-object v2, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    :try_start_1
    iget-object v0, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lmaps/ay/ai;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/m;

    invoke-virtual {p1}, Lmaps/as/a;->B()V

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/m;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    invoke-virtual {p1}, Lmaps/as/a;->C()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final f()Lmaps/ay/v;
    .locals 2

    iget v0, p0, Lmaps/ay/ai;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, Lmaps/ay/v;->j:Lmaps/ay/v;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lmaps/ay/v;->i:Lmaps/ay/v;

    goto :goto_0
.end method
