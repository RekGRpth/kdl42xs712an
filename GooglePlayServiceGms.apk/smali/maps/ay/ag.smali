.class final Lmaps/ay/ag;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/aj/af;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/m/bo;->a(Ljava/util/Collection;)Lmaps/m/bo;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/ag;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ag;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/c;)V
    .locals 2

    invoke-virtual {p2}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/aj/ad;->b()Lmaps/aj/ae;

    move-result-object v0

    sget-object v1, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->y()V

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;Lmaps/ac/av;)V
    .locals 5

    const/16 v4, 0x80

    const/16 v3, 0x1e01

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/aj/ad;->b()Lmaps/aj/ae;

    move-result-object v1

    sget-object v2, Lmaps/aj/ae;->e:Lmaps/aj/ae;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lmaps/as/a;->x()V

    invoke-interface {v0, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    const/16 v1, 0x207

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    const v1, -0x9f9fa0

    invoke-static {v0, v1}, Lmaps/al/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
