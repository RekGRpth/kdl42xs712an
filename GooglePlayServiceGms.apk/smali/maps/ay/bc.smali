.class public final Lmaps/ay/bc;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;

# interfaces
.implements Lmaps/bo/m;


# static fields
.field private static final c:F

.field private static final d:F

.field private static final e:D


# instance fields
.field private final a:F

.field private final b:F

.field private final f:Lmaps/ay/be;

.field private g:Lmaps/ay/bf;

.field private h:Landroid/view/MotionEvent;

.field private i:F

.field private j:F

.field private k:Lmaps/ay/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-boolean v0, Lmaps/bb/b;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x3f7f3b64    # 0.997f

    :goto_0
    sput v0, Lmaps/ay/bc;->c:F

    const/high16 v0, 0x3f800000    # 1.0f

    sget v1, Lmaps/ay/bc;->c:F

    div-float/2addr v0, v1

    sput v0, Lmaps/ay/bc;->d:F

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lmaps/ay/bc;->e:D

    return-void

    :cond_0
    const v0, 0x3f7fbe77    # 0.999f

    goto :goto_0
.end method

.method public constructor <init>(Lmaps/ay/be;)V
    .locals 1

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    sget-object v0, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    iput-object v0, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    iput-object p1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    new-instance v0, Lmaps/ay/bf;

    invoke-direct {v0}, Lmaps/ay/bf;-><init>()V

    iput-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->l()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/ay/bc;->b:F

    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->l()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lmaps/ay/bc;->a:F

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iput-boolean p1, v0, Lmaps/ay/bf;->a:Z

    return-void
.end method

.method public final a()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->a:Z

    return v0
.end method

.method public final a(Lmaps/bo/o;)Z
    .locals 4

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lmaps/bo/o;->a(FF)V

    invoke-virtual {p1}, Lmaps/bo/o;->c()F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    invoke-virtual {p1}, Lmaps/bo/o;->a()F

    move-result v1

    invoke-virtual {p1}, Lmaps/bo/o;->b()F

    move-result v2

    iget-object v3, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v3}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lmaps/ay/au;->b(FFF)F

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lmaps/bo/q;)Z
    .locals 6

    const/4 v2, 0x1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/bo/q;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v0

    const/high16 v1, -0x40800000    # -1.0f

    const/16 v3, 0x14a

    invoke-virtual {v0, v1, v3}, Lmaps/ay/au;->a(FI)F

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->getWidth()I

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->getHeight()I

    invoke-interface {v0}, Lmaps/ay/be;->o()V

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p1}, Lmaps/bo/q;->c()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v3, Lmaps/ay/bc;->e:D

    div-double/2addr v0, v3

    double-to-float v0, v0

    invoke-virtual {p1}, Lmaps/bo/q;->a()F

    move-result v3

    invoke-virtual {p1}, Lmaps/bo/q;->b()F

    move-result v4

    invoke-virtual {p1}, Lmaps/bo/q;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lmaps/bo/q;->c()F

    move-result v1

    sget v5, Lmaps/ay/bc;->c:F

    cmpl-float v1, v1, v5

    if-lez v1, :cond_3

    invoke-virtual {p1}, Lmaps/bo/q;->c()F

    move-result v1

    sget v5, Lmaps/ay/bc;->d:F

    cmpg-float v1, v1, v5

    if-gez v1, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v4}, Lmaps/ay/au;->a(FFF)F

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->o()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Lmaps/bo/u;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/bo/u;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Lmaps/ay/au;->a(F)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iput-boolean p1, v0, Lmaps/ay/bf;->b:Z

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->b:Z

    return v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iput-boolean p1, v0, Lmaps/ay/bf;->d:Z

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->d:Z

    return v0
.end method

.method public final d(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iput-boolean p1, v0, Lmaps/ay/bf;->e:Z

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->e:Z

    return v0
.end method

.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->p()V

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/ay/be;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lmaps/ay/bd;->b:Lmaps/ay/bd;

    iput-object v0, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    iput-object p1, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lmaps/ay/bc;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lmaps/ay/bc;->j:F

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v0, 0x1

    iget-object v2, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v2}, Lmaps/ay/be;->p()V

    iget-object v2, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-object v2, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v2, v2, Lmaps/ay/bf;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    sget-object v3, Lmaps/ay/bd;->b:Lmaps/ay/bd;

    if-ne v2, v3, :cond_1

    iget-object v1, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-object v3, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v3}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v5, 0x14a

    invoke-virtual {v3, v4, v1, v2, v5}, Lmaps/ay/au;->a(FFFI)F

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->o()V

    iput-object v7, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    sget-object v1, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    iput-object v1, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput-object v7, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    sget-object v2, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    iput-object v2, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    :cond_2
    iget-object v2, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lmaps/ay/bc;->j:F

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lmaps/ay/bc;->i:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    sget-object v5, Lmaps/ay/bd;->b:Lmaps/ay/bd;

    if-ne v4, v5, :cond_3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lmaps/ay/bc;->b:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lmaps/ay/bc;->b:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    iget-object v3, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    iget-object v3, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lmaps/ay/bc;->a:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    iget-object v3, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v3, v3, Lmaps/ay/bf;->b:Z

    if-eqz v3, :cond_0

    sget-object v3, Lmaps/ay/bd;->c:Lmaps/ay/bd;

    iput-object v3, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    const-string v3, "d"

    const/16 v4, 0x63

    invoke-static {v4, v3}, Lmaps/br/g;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    sget-object v4, Lmaps/ay/bd;->c:Lmaps/ay/bd;

    if-ne v3, v4, :cond_5

    iget-object v3, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v3, v3, Lmaps/ay/bf;->b:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v3}, Lmaps/ay/be;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x40c00000    # 6.0f

    mul-float/2addr v2, v3

    iget-object v3, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v3}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lmaps/ay/au;->a(FI)F

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    iget-object v2, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    iget-object v2, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    invoke-interface {v1}, Lmaps/ay/be;->o()V

    :cond_4
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lmaps/ay/bc;->i:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lmaps/ay/bc;->j:F

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    sget-object v2, Lmaps/ay/bd;->d:Lmaps/ay/bd;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v1, v1, Lmaps/ay/bf;->e:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v1}, Lmaps/ay/be;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    iget-object v2, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v2}, Lmaps/ay/be;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v6

    iget v3, p0, Lmaps/ay/bc;->i:F

    iget v4, p0, Lmaps/ay/bc;->j:F

    invoke-static {v1, v2, v3, v4}, Lmaps/bo/j;->a(FFFF)F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v1, v2, v4, v5}, Lmaps/bo/j;->a(FFFF)F

    move-result v4

    iget-object v5, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v5}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v5

    sub-float v3, v4, v3

    const/high16 v4, 0x43340000    # 180.0f

    mul-float/2addr v3, v4

    float-to-double v3, v3

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v3, v6

    double-to-float v3, v3

    invoke-virtual {v5, v1, v2, v3}, Lmaps/ay/au;->b(FFF)F

    goto :goto_1

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/ay/bc;->k:Lmaps/ay/bd;

    sget-object v1, Lmaps/ay/bd;->a:Lmaps/ay/bd;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/ay/be;->d(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lmaps/ay/bc;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmaps/ay/au;->b(FF)V

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->p()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Lmaps/ay/bc;->h:Landroid/view/MotionEvent;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/ay/be;->c(FF)V

    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0, p2}, Lmaps/ay/be;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->l()Lmaps/ay/au;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lmaps/ay/au;->a(FF)V

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->n()V

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->p()V

    goto :goto_0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/ay/bc;->g:Lmaps/ay/bf;

    iget-boolean v0, v0, Lmaps/ay/bf;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-interface {v0}, Lmaps/ay/be;->p()V

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/ay/be;->b(FF)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    iget-object v0, p0, Lmaps/ay/bc;->f:Lmaps/ay/be;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lmaps/ay/be;->e(FF)Z

    move-result v0

    return v0
.end method
