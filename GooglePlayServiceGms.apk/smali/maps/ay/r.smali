.class public final Lmaps/ay/r;
.super Lmaps/ay/c;


# instance fields
.field private final b:Ljava/util/LinkedList;

.field private c:Z

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/HashMap;

.field private f:Lmaps/ac/cw;

.field private g:Ljava/util/List;

.field private final h:Lmaps/ay/v;

.field private i:I

.field private j:Z

.field private k:Lmaps/au/af;

.field private l:Lmaps/ay/t;

.field private m:Lmaps/ay/s;


# direct methods
.method public constructor <init>(Lmaps/ay/v;Lmaps/ay/o;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lmaps/ay/c;-><init>(Lmaps/ay/o;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ay/r;->e:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    iput v1, p0, Lmaps/ay/r;->i:I

    iput-boolean v1, p0, Lmaps/ay/r;->j:Z

    iput-object p1, p0, Lmaps/ay/r;->h:Lmaps/ay/v;

    return-void
.end method

.method static synthetic a(Lmaps/ay/r;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lmaps/ay/r;Lmaps/ar/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ay/r;->b(Lmaps/ar/a;)V

    return-void
.end method

.method private b(Lmaps/ar/a;)V
    .locals 5

    invoke-static {}, Lmaps/ab/q;->a()Lmaps/ab/q;

    move-result-object v1

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->p()Lmaps/ac/ad;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lmaps/ac/ad;->a()Lmaps/ac/r;

    move-result-object v3

    invoke-virtual {v1, v3}, Lmaps/ab/q;->e(Lmaps/ac/r;)Lmaps/ab/k;

    move-result-object v3

    if-eqz v3, :cond_0

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Lmaps/au/af;->o()Lmaps/ac/av;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lmaps/ab/k;->a(Lmaps/ar/a;Lmaps/ac/av;)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4, v3}, Lmaps/ac/av;->b(I)V

    invoke-virtual {v0, v4}, Lmaps/au/af;->a(Lmaps/ac/av;)V

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_1
    return-void
.end method

.method static synthetic b(Lmaps/ay/r;Lmaps/ar/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ay/r;->c(Lmaps/ar/a;)V

    return-void
.end method

.method private c(Lmaps/ar/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0, p1}, Lmaps/au/af;->a(Lmaps/ar/a;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method private d(Lmaps/au/af;)V
    .locals 2

    invoke-virtual {p1}, Lmaps/au/af;->t()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Lmaps/au/af;->u()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ay/r;->e:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->j()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method private e(FFLmaps/ar/a;)V
    .locals 2

    const/high16 v0, 0x428c0000    # 70.0f

    sub-float v0, p2, v0

    invoke-virtual {p3, p1, v0}, Lmaps/ar/a;->d(FF)Lmaps/ac/av;

    move-result-object v0

    iget-object v1, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-virtual {v1, v0}, Lmaps/au/af;->a(Lmaps/ac/av;)V

    return-void
.end method

.method private e(Lmaps/au/af;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/r;->m:Lmaps/ay/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->m:Lmaps/ay/s;

    invoke-virtual {v0, p1}, Lmaps/ay/s;->a(Lmaps/au/af;)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/r;->c:Z

    return-void
.end method

.method private declared-synchronized l()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->t()I

    invoke-virtual {v0}, Lmaps/au/af;->u()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lmaps/ay/r;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/util/List;FFLmaps/ar/a;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p4}, Lmaps/ay/r;->a(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/b;

    invoke-interface {v0}, Lmaps/ay/b;->ap_()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p2, p3, p4}, Lmaps/ay/b;->a(FFLmaps/ar/a;)I

    move-result v2

    if-ge v2, p5, :cond_0

    new-instance v3, Lmaps/ay/n;

    invoke-direct {v3, v0, p0, v2}, Lmaps/ay/n;-><init>(Lmaps/ay/b;Lmaps/ay/c;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method final a(Lmaps/ar/a;)V
    .locals 8

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v1

    iget-boolean v0, p0, Lmaps/ay/r;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->f:Lmaps/ac/cw;

    invoke-virtual {v1, v0}, Lmaps/ac/cw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    :goto_1
    invoke-virtual {v1}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v2

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/ac/cw;->a(Lmaps/ac/cw;Lmaps/ac/av;)Lmaps/ac/cw;

    move-result-object v3

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lmaps/au/af;->i()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v5, Lmaps/ac/av;

    invoke-direct {v5}, Lmaps/ac/av;-><init>()V

    invoke-virtual {v0}, Lmaps/au/af;->o()Lmaps/ac/av;

    move-result-object v6

    invoke-virtual {v6, v5}, Lmaps/ac/av;->i(Lmaps/ac/av;)V

    invoke-virtual {v2, v5}, Lmaps/ac/cx;->a(Lmaps/ac/av;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v1, v5}, Lmaps/ac/cw;->a(Lmaps/ac/av;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    invoke-virtual {v3, v5}, Lmaps/ac/cw;->a(Lmaps/ac/av;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, p1}, Lmaps/au/af;->b(Lmaps/ar/a;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_3
    iget-object v5, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v2, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x200

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "#:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lmaps/ay/r;->i:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lmaps/ay/r;->i:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " T:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " E:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " C:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " numM:"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "GLMarkerOverlay"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmaps/aw/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iput-object v1, p0, Lmaps/ay/r;->f:Lmaps/ac/cw;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/r;->c:Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/16 :goto_1
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 5

    invoke-virtual {p3}, Lmaps/ap/c;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p2}, Lmaps/ay/r;->a(Lmaps/ar/a;)V

    invoke-direct {p0, p2}, Lmaps/ay/r;->b(Lmaps/ar/a;)V

    invoke-direct {p0, p2}, Lmaps/ay/r;->c(Lmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    monitor-exit p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/as/a;->r()V

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    iget-object v0, p1, Lmaps/as/a;->e:Lmaps/at/n;

    invoke-virtual {v0, p1}, Lmaps/at/n;->d(Lmaps/as/a;)V

    new-instance v2, Lmaps/ap/c;

    invoke-direct {v2, p3}, Lmaps/ap/c;-><init>(Lmaps/ap/c;)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lmaps/ap/c;->a(I)V

    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->j()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, p1, p2, v2}, Lmaps/au/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {v2, v0}, Lmaps/ap/c;->a(I)V

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->n()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v1, v0

    goto :goto_2

    :cond_4
    invoke-virtual {v0, p1, p2, v2}, Lmaps/au/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1, p1, p2, v2}, Lmaps/au/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    :cond_6
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lmaps/au/af;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p0}, Lmaps/au/af;->a(Lmaps/ay/r;)V

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lmaps/ay/r;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lmaps/ay/t;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v1, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    invoke-virtual {v0}, Lmaps/ay/o;->b()Lmaps/ay/b;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v2, v0, Lmaps/au/af;

    if-eqz v2, :cond_0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->h()Lmaps/ay/r;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    invoke-virtual {v0}, Lmaps/ay/o;->d()V

    :cond_0
    invoke-direct {p0}, Lmaps/ay/r;->l()V

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-direct {p0, v0}, Lmaps/ay/r;->e(Lmaps/au/af;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-direct {p0}, Lmaps/ay/r;->k()V

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void
.end method

.method public final b(Lmaps/au/af;)V
    .locals 3

    iget-object v1, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lmaps/au/af;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmaps/au/af;->s()V

    iget-object v0, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    invoke-virtual {v0}, Lmaps/ay/o;->d()V

    :cond_0
    invoke-direct {p0, p1}, Lmaps/ay/r;->d(Lmaps/au/af;)V

    invoke-direct {p0, p1}, Lmaps/ay/r;->e(Lmaps/au/af;)V

    invoke-direct {p0}, Lmaps/ay/r;->k()V

    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lmaps/ay/r;->j:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, p2, p4}, Lmaps/ay/r;->e(FFLmaps/ar/a;)V

    iget-object v1, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-static {}, Lmaps/au/af;->g()Lmaps/au/ag;

    iget-object v1, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    iget-object v2, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-interface {v1, v2}, Lmaps/ay/t;->c(Lmaps/au/af;)V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    iput-boolean v0, p0, Lmaps/ay/r;->j:Z

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final c(Lmaps/as/a;)V
    .locals 2

    iget-object v1, p0, Lmaps/ay/r;->a:Lmaps/ay/o;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0}, Lmaps/ay/r;->l()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final declared-synchronized c(Lmaps/au/af;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lmaps/au/af;->k()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/ay/r;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lmaps/ay/r;->d(Lmaps/au/af;)V

    invoke-direct {p0, p1}, Lmaps/ay/r;->e(Lmaps/au/af;)V

    invoke-direct {p0}, Lmaps/ay/r;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(FFLmaps/ar/a;)Z
    .locals 4

    const/4 v1, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmaps/ay/r;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/af;

    invoke-virtual {v0}, Lmaps/au/af;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lmaps/au/af;->b(FFLmaps/ar/a;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lmaps/ay/r;->j:Z

    iput-object v0, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/r;->e(FFLmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    iget-object v2, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-interface {v0, v2}, Lmaps/ay/t;->a(Lmaps/au/af;)V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :goto_0
    return v0

    :cond_2
    monitor-exit p0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lmaps/ay/r;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(FFLmaps/ar/a;)Z
    .locals 2

    iget-boolean v0, p0, Lmaps/ay/r;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lmaps/ay/r;->e(FFLmaps/ar/a;)V

    iget-object v0, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ay/r;->l:Lmaps/ay/t;

    iget-object v1, p0, Lmaps/ay/r;->k:Lmaps/au/af;

    invoke-interface {v0, v1}, Lmaps/ay/t;->b(Lmaps/au/af;)V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    iget-object v0, p0, Lmaps/ay/r;->h:Lmaps/ay/v;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/r;->j:Z

    return v0
.end method

.method public final j()Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lmaps/ay/r;->e:Ljava/util/HashMap;

    return-object v0
.end method
