.class public Lmaps/ay/ap;
.super Lmaps/ay/u;


# static fields
.field private static final M:Lmaps/u/c;

.field private static final e:Lmaps/ax/a;

.field private static final f:Lmaps/ax/a;


# instance fields
.field private volatile A:Z

.field private final B:Ljava/util/Set;

.field private final C:Lmaps/ax/f;

.field private final D:Lmaps/ac/av;

.field private E:Lmaps/ar/b;

.field private F:J

.field private G:J

.field private H:J

.field private I:Z

.field private final J:Z

.field private final K:Z

.field private final L:Lmaps/u/c;

.field private N:I

.field private O:J

.field private final P:Ljava/util/Set;

.field private final Q:Lmaps/ao/a;

.field private R:Ljava/lang/ref/WeakReference;

.field final a:Ljava/util/ArrayList;

.field protected volatile b:Lmaps/ap/n;

.field protected volatile c:Z

.field protected final d:Lmaps/am/b;

.field private final g:I

.field private final h:I

.field private final i:Lmaps/ay/v;

.field private j:Z

.field private final k:I

.field private final l:I

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Lmaps/ao/b;

.field private final r:Lmaps/ah/d;

.field private final s:Ljava/util/ArrayList;

.field private final t:[I

.field private final u:Ljava/util/ArrayList;

.field private final v:[I

.field private final w:Lmaps/ay/as;

.field private x:Lmaps/u/a;

.field private y:Lmaps/y/f;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x4

    aput v2, v0, v1

    invoke-static {v0}, Lmaps/ax/a;->a([I)Lmaps/ax/a;

    move-result-object v0

    sput-object v0, Lmaps/ay/ap;->e:Lmaps/ax/a;

    sget-object v0, Lmaps/au/at;->a:Lmaps/ax/a;

    sget-object v1, Lmaps/ay/ap;->e:Lmaps/ax/a;

    invoke-static {v0, v1}, Lmaps/ax/a;->a(Lmaps/ax/a;Lmaps/ax/a;)Lmaps/ax/a;

    move-result-object v0

    sput-object v0, Lmaps/ay/ap;->f:Lmaps/ax/a;

    new-instance v0, Lmaps/u/b;

    invoke-direct {v0}, Lmaps/u/b;-><init>()V

    sput-object v0, Lmaps/ay/ap;->M:Lmaps/u/c;

    return-void
.end method

.method constructor <init>(Lmaps/ao/b;Lmaps/ah/d;Lmaps/u/c;IIILmaps/ay/v;IZZZZZZ)V
    .locals 5

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    new-instance v1, Lmaps/ay/as;

    invoke-direct {v1}, Lmaps/ay/as;-><init>()V

    iput-object v1, p0, Lmaps/ay/ap;->w:Lmaps/ay/as;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/ap;->B:Ljava/util/Set;

    new-instance v1, Lmaps/ax/f;

    const/16 v2, 0x50

    invoke-direct {v1, v2}, Lmaps/ax/f;-><init>(I)V

    iput-object v1, p0, Lmaps/ay/ap;->C:Lmaps/ax/f;

    new-instance v1, Lmaps/ac/av;

    invoke-direct {v1}, Lmaps/ac/av;-><init>()V

    iput-object v1, p0, Lmaps/ay/ap;->D:Lmaps/ac/av;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lmaps/ay/ap;->F:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lmaps/ay/ap;->G:J

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lmaps/ay/ap;->H:J

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmaps/ay/ap;->c:Z

    invoke-static {}, Lmaps/m/dk;->a()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lmaps/ay/ap;->P:Ljava/util/Set;

    iput-object p1, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    iput-object p2, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    iput-object p3, p0, Lmaps/ay/ap;->L:Lmaps/u/c;

    iput p4, p0, Lmaps/ay/ap;->g:I

    iput p5, p0, Lmaps/ay/ap;->h:I

    iput-object p7, p0, Lmaps/ay/ap;->i:Lmaps/ay/v;

    const/16 v1, 0x100

    iput v1, p0, Lmaps/ay/ap;->k:I

    iput p8, p0, Lmaps/ay/ap;->l:I

    iput-boolean p9, p0, Lmaps/ay/ap;->p:Z

    iput-boolean p10, p0, Lmaps/ay/ap;->m:Z

    move/from16 v0, p11

    iput-boolean v0, p0, Lmaps/ay/ap;->n:Z

    move/from16 v0, p12

    iput-boolean v0, p0, Lmaps/ay/ap;->o:Z

    move/from16 v0, p13

    iput-boolean v0, p0, Lmaps/ay/ap;->J:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lmaps/ay/ap;->K:Z

    new-instance v1, Lmaps/ao/a;

    invoke-direct {v1}, Lmaps/ao/a;-><init>()V

    iput-object v1, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    iget-object v1, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    iget-object v2, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-virtual {v1, v2}, Lmaps/ah/d;->a(Lmaps/ao/a;)V

    iget-boolean v1, p0, Lmaps/ay/ap;->J:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lmaps/ao/b;->e()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lmaps/am/b;

    invoke-virtual {p0}, Lmaps/ay/ap;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lmaps/am/b;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lmaps/ay/ap;->d:Lmaps/am/b;

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    new-array v1, p4, [I

    iput-object v1, p0, Lmaps/ay/ap;->t:[I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lmaps/ay/ap;->u:Ljava/util/ArrayList;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, p6, :cond_1

    iget-object v2, p0, Lmaps/ay/ap;->u:Ljava/util/ArrayList;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ay/ap;->d:Lmaps/am/b;

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p6, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lmaps/ay/ap;->v:[I

    const/4 v1, 0x0

    iput v1, p0, Lmaps/ay/ap;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, 0xfa0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lmaps/ay/ap;->O:J

    iget-object v1, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    new-instance v2, Lmaps/ay/aq;

    invoke-direct {v2, p0}, Lmaps/ay/aq;-><init>(Lmaps/ay/ap;)V

    invoke-virtual {v1, v2}, Lmaps/ah/d;->a(Lmaps/ah/i;)V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;)I
    .locals 3

    const/high16 v2, 0x43000000    # 128.0f

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, 0x2

    mul-int/2addr v0, v1

    return v0
.end method

.method public static a(Landroid/content/res/Resources;I)I
    .locals 3

    const v1, 0x64140

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v2

    if-ge v0, v1, :cond_0

    move v0, v1

    :cond_0
    div-int/lit16 v2, p1, 0x100

    int-to-float v2, v2

    mul-float/2addr v2, v2

    mul-int/lit8 v0, v0, 0x18

    div-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v0, v0

    return v0
.end method

.method private a(Lmaps/ar/a;Lmaps/ap/b;II)I
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lmaps/ap/b;->f:Lmaps/ap/b;

    if-eq p2, v1, :cond_1

    move v1, v0

    :goto_0
    if-ge p3, p4, :cond_0

    iget-object v2, p0, Lmaps/ay/ap;->t:[I

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    invoke-interface {v0, p1, p2}, Lmaps/au/ap;->a(Lmaps/ar/a;Lmaps/ap/b;)I

    move-result v0

    aput v0, v2, p3

    iget-object v0, p0, Lmaps/ay/ap;->t:[I

    aget v0, v0, p3

    or-int/2addr v0, v1

    add-int/lit8 p3, p3, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    invoke-virtual {v0, v1, p2}, Lmaps/ao/b;->a(ILmaps/ap/b;)I

    move-result v0

    :cond_1
    return v0
.end method

.method static synthetic a(Lmaps/ay/ap;)Lmaps/ah/d;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;Lmaps/ab/q;)Lmaps/ay/ap;
    .locals 7

    const/16 v0, 0x100

    invoke-static {p1, v0}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v0

    mul-int/lit8 v3, v0, 0x2

    mul-int/lit8 v4, v3, 0x2

    new-instance v0, Lmaps/ah/a;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-direct {v0, v1, v4, v2, v5}, Lmaps/ah/a;-><init>(IIZZ)V

    new-instance v1, Lmaps/ah/d;

    sget-object v2, Lmaps/ao/b;->n:Lmaps/ao/b;

    invoke-direct {v1, v2, v0}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/ah/a;)V

    new-instance v0, Lmaps/ay/ad;

    sget-object v2, Lmaps/ay/ap;->M:Lmaps/u/c;

    sget-object v5, Lmaps/ay/v;->g:Lmaps/ay/v;

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lmaps/ay/ad;-><init>(Lmaps/ah/d;Lmaps/u/c;IILmaps/ay/v;Lmaps/ab/q;)V

    invoke-virtual {v0}, Lmaps/ay/ad;->e()V

    return-object v0
.end method

.method public static a(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/ap;
    .locals 16

    sget-object v1, Lmaps/ao/b;->a:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-eq v0, v1, :cond_0

    sget-object v1, Lmaps/ao/b;->b:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-eq v0, v1, :cond_0

    sget-object v1, Lmaps/ao/b;->c:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x100

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v5

    mul-int/lit8 v6, v5, 0x2

    if-nez v1, :cond_1

    sget-object v2, Lmaps/ao/b;->j:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-ne v0, v2, :cond_4

    :cond_1
    const/4 v12, 0x1

    :goto_1
    if-nez v12, :cond_2

    sget-object v2, Lmaps/ao/b;->o:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-eq v0, v2, :cond_2

    sget-object v2, Lmaps/ao/b;->p:Lmaps/ao/b;

    move-object/from16 v0, p0

    if-ne v0, v2, :cond_5

    :cond_2
    const/4 v15, 0x1

    :goto_2
    if-eqz v1, :cond_6

    sget-object v1, Lmaps/ay/ap;->f:Lmaps/ax/a;

    move-object v2, v1

    :goto_3
    if-eqz v12, :cond_7

    const/4 v1, 0x1

    :goto_4
    new-instance v4, Lmaps/ah/a;

    const/16 v3, 0x8

    const/4 v7, 0x0

    invoke-direct {v4, v3, v6, v1, v7}, Lmaps/ah/a;-><init>(IIZZ)V

    new-instance v3, Lmaps/ah/d;

    new-instance v1, Lmaps/aq/e;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v7, v2}, Lmaps/aq/e;-><init>(Lmaps/ao/b;Ljava/util/Set;Lmaps/ax/a;)V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v1, v4, v2}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/aq/e;Lmaps/ah/a;Lmaps/ax/a;)V

    new-instance v1, Lmaps/ay/ap;

    new-instance v4, Lmaps/y/e;

    invoke-direct {v4}, Lmaps/y/e;-><init>()V

    const/16 v7, 0x8

    sget-object v8, Lmaps/ay/v;->c:Lmaps/ay/v;

    const/16 v9, 0x100

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v14, 0x1

    move-object/from16 v2, p0

    move v13, v12

    invoke-direct/range {v1 .. v15}, Lmaps/ay/ap;-><init>(Lmaps/ao/b;Lmaps/ah/d;Lmaps/u/c;IIILmaps/ay/v;IZZZZZZ)V

    return-object v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    goto :goto_1

    :cond_5
    const/4 v15, 0x0

    goto :goto_2

    :cond_6
    sget-object v1, Lmaps/ax/a;->a:Lmaps/ax/a;

    move-object v2, v1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public static a(Lmaps/u/c;Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/ap;
    .locals 7

    const/4 v1, 0x0

    const/16 v0, 0x100

    invoke-static {p2, v0}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    new-instance v0, Lmaps/ah/a;

    invoke-direct {v0, v1, v5, v1, v1}, Lmaps/ah/a;-><init>(IIZZ)V

    new-instance v2, Lmaps/ah/d;

    new-instance v1, Lmaps/aq/e;

    const/4 v3, 0x0

    sget-object v6, Lmaps/ay/ap;->e:Lmaps/ax/a;

    invoke-direct {v1, p1, v3, v6}, Lmaps/aq/e;-><init>(Lmaps/ao/b;Ljava/util/Set;Lmaps/ax/a;)V

    sget-object v3, Lmaps/ay/ap;->e:Lmaps/ax/a;

    invoke-direct {v2, p1, v1, v0, v3}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/aq/e;Lmaps/ah/a;Lmaps/ax/a;)V

    new-instance v0, Lmaps/ay/g;

    sget-object v6, Lmaps/ay/v;->h:Lmaps/ay/v;

    move-object v1, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lmaps/ay/g;-><init>(Lmaps/ao/b;Lmaps/ah/d;Lmaps/u/c;IILmaps/ay/v;)V

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Lmaps/ao/b;)Lmaps/ay/at;
    .locals 6

    const/4 v2, 0x0

    const/16 v0, 0x100

    invoke-static {p0, v0}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;I)I

    move-result v3

    mul-int/lit8 v4, v3, 0x2

    new-instance v0, Lmaps/ah/a;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v4, v2, v1}, Lmaps/ah/a;-><init>(IIZZ)V

    new-instance v1, Lmaps/ah/d;

    invoke-direct {v1, p1, v0}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/ah/a;)V

    new-instance v0, Lmaps/ay/at;

    sget-object v2, Lmaps/ay/ap;->M:Lmaps/u/c;

    sget-object v5, Lmaps/ay/v;->f:Lmaps/ay/v;

    invoke-direct/range {v0 .. v5}, Lmaps/ay/at;-><init>(Lmaps/ah/d;Lmaps/u/c;IILmaps/ay/v;)V

    return-object v0
.end method

.method static synthetic b(Lmaps/ay/ap;)Lmaps/ax/f;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->C:Lmaps/ax/f;

    return-object v0
.end method

.method public static b(Lmaps/ao/b;Landroid/content/res/Resources;)Lmaps/ay/ap;
    .locals 15

    invoke-static/range {p1 .. p1}, Lmaps/ay/ap;->a(Landroid/content/res/Resources;)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    new-instance v0, Lmaps/ah/a;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v5, v2, v3}, Lmaps/ah/a;-><init>(IIZZ)V

    new-instance v2, Lmaps/ah/d;

    invoke-direct {v2, p0, v0}, Lmaps/ah/d;-><init>(Lmaps/ao/b;Lmaps/ah/a;)V

    new-instance v0, Lmaps/ay/ap;

    sget-object v3, Lmaps/ay/ap;->M:Lmaps/u/c;

    const/4 v6, 0x4

    sget-object v7, Lmaps/ay/v;->b:Lmaps/ay/v;

    const/16 v8, 0x180

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v14}, Lmaps/ay/ap;-><init>(Lmaps/ao/b;Lmaps/ah/d;Lmaps/u/c;IIILmaps/ay/v;IZZZZZZ)V

    return-object v0
.end method

.method static synthetic c(Lmaps/ay/ap;)V
    .locals 8

    const/4 v7, 0x0

    iget v0, p0, Lmaps/ay/ap;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ay/ap;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ay/ap;->O:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/16 v0, 0x6e

    const-string v1, "l"

    iget v2, p0, Lmaps/ay/ap;->N:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "t="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    iget v5, v5, Lmaps/ao/b;->B:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "n="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-static {v3}, Lmaps/br/g;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmaps/br/g;->a(ILjava/lang/String;Ljava/lang/String;)V

    iput v7, p0, Lmaps/ay/ap;->N:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0xfa0

    add-long/2addr v0, v2

    iput-wide v0, p0, Lmaps/ay/ap;->O:J

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/av;)F
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    if-nez v0, :cond_0

    const/high16 v0, 0x41a80000    # 21.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    invoke-interface {v0, p1}, Lmaps/u/a;->b(Lmaps/ac/av;)F

    move-result v0

    goto :goto_0
.end method

.method public a(Lmaps/ac/cw;Lmaps/aj/ac;Ljava/util/Set;)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v3

    invoke-virtual {p1, v3}, Lmaps/ac/cw;->b(Lmaps/ac/be;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    iget-object v3, p0, Lmaps/ay/ap;->d:Lmaps/am/b;

    invoke-interface {v0, v3}, Lmaps/au/ap;->a(Lmaps/am/b;)V

    invoke-interface {v0, p2}, Lmaps/au/ap;->a(Lmaps/aj/ac;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bt;->b()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_2
    instance-of v3, v0, Lmaps/au/at;

    if-eqz v3, :cond_0

    check-cast v0, Lmaps/au/at;

    invoke-virtual {v0, p3}, Lmaps/au/at;->a(Ljava/util/Set;)Z

    goto :goto_0

    :cond_3
    return v1
.end method

.method public final a(I)V
    .locals 1

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lmaps/ay/ap;->z:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->d:Lmaps/am/b;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lmaps/ao/b;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0, p1}, Lmaps/ah/d;->a(Lmaps/ao/b;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ap;->A:Z

    return-void
.end method

.method public final a(Lmaps/ar/a;Lmaps/ap/b;Ljava/util/HashSet;Ljava/util/HashSet;[I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, -0x1

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0, p1, p2, v0, v3}, Lmaps/ay/ap;->a(Lmaps/ar/a;Lmaps/ap/b;II)I

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    invoke-interface {v0, p3}, Lmaps/au/ap;->a(Ljava/util/Collection;)V

    invoke-interface {v0, p4}, Lmaps/au/ap;->b(Ljava/util/Collection;)V

    invoke-interface {v0}, Lmaps/au/ap;->h()I

    move-result v0

    if-le v0, v1, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    array-length v0, p5

    if-lez v0, :cond_1

    aput v1, p5, v3

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lmaps/ar/b;)V
    .locals 0

    iput-object p1, p0, Lmaps/ay/ap;->E:Lmaps/ar/b;

    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 5

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmaps/ay/ap;->R:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0, p1}, Lmaps/ah/d;->a(Lmaps/as/a;)V

    iput-object p2, p0, Lmaps/ay/ap;->b:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ap;->L:Lmaps/u/c;

    iget-object v1, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    iget v2, p0, Lmaps/ay/ap;->l:I

    iget-boolean v3, p0, Lmaps/ay/ap;->p:Z

    iget-object v4, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-interface {v0, v1, v2, v3, v4}, Lmaps/u/c;->a(Lmaps/ao/b;IZLmaps/ao/a;)Lmaps/u/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/ay/ap;->a(Lmaps/u/a;)V

    iget-object v0, p0, Lmaps/ay/ap;->L:Lmaps/u/c;

    iget-object v1, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    iget-boolean v2, p0, Lmaps/ay/ap;->p:Z

    iget-object v3, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-interface {v0, v1, v2, v3}, Lmaps/u/c;->a(Lmaps/ao/b;ZLmaps/ao/a;)Lmaps/y/f;

    move-result-object v0

    iput-object v0, p0, Lmaps/ay/ap;->y:Lmaps/y/f;

    iget-object v0, p0, Lmaps/ay/ap;->y:Lmaps/y/f;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    instance-of v0, v0, Lmaps/y/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    check-cast v0, Lmaps/y/f;

    iput-object v0, p0, Lmaps/ay/ap;->y:Lmaps/y/f;

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad out-of-bounds coord generator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 23

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->b()I

    move-result v3

    if-lez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->a()Lmaps/ap/b;

    move-result-object v15

    invoke-virtual/range {p3 .. p3}, Lmaps/ap/c;->c()Lmaps/aj/ad;

    move-result-object v16

    new-instance v17, Lmaps/ap/c;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lmaps/ap/c;-><init>(Lmaps/ap/c;)V

    invoke-virtual/range {v16 .. v16}, Lmaps/aj/ad;->f()Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->A:Z

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    move-object/from16 v0, p0

    iget-object v5, v0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v5}, Lmaps/ah/d;->i()Lmaps/bs/b;

    invoke-interface {v3}, Lmaps/au/ap;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/ay/ap;->A:Z

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->A:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->z:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lmaps/ay/ap;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->B()V

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lmaps/ap/c;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/au/ap;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-interface {v3, v0, v1, v2}, Lmaps/au/ap;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->C()V

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->I:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lmaps/ah/d;->a(Ljava/util/List;)V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->n:Z

    if-eqz v3, :cond_e

    sget-object v3, Lmaps/ap/b;->a:Lmaps/ap/b;

    if-eq v15, v3, :cond_9

    sget-object v3, Lmaps/ap/b;->c:Lmaps/ap/b;

    if-ne v15, v3, :cond_e

    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->v:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_e

    :cond_a
    invoke-virtual/range {v16 .. v16}, Lmaps/aj/ad;->c()Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x1

    move v5, v3

    :goto_3
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->J()I

    move-result v3

    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    invoke-virtual {v3}, Lmaps/ao/b;->e()Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->K:Z

    if-eqz v3, :cond_f

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->K()Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v3, 0x1

    move v14, v3

    :goto_4
    if-nez v14, :cond_10

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->J()I

    move-result v3

    if-lez v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    sget-object v4, Lmaps/ao/b;->x:Lmaps/ao/b;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    sget-object v4, Lmaps/ao/b;->w:Lmaps/ao/b;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    :cond_b
    const/4 v3, 0x1

    move v13, v3

    :goto_5
    if-nez v14, :cond_c

    if-eqz v13, :cond_d

    :cond_c
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->x()V

    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->u:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v6, 0x0

    invoke-virtual/range {v16 .. v16}, Lmaps/aj/ad;->d()Z

    move-result v18

    if-eqz v13, :cond_12

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v7, 0x1e01

    const/16 v9, 0x1e01

    const/16 v10, 0x1e01

    invoke-interface {v3, v7, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v7, 0x7f

    invoke-interface {v3, v7}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move v7, v3

    :goto_6
    if-ltz v7, :cond_11

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    const/16 v10, 0x200

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v11

    invoke-virtual {v11}, Lmaps/ac/bt;->b()I

    move-result v11

    const/16 v12, 0x7f

    invoke-interface {v9, v10, v11, v12}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v9

    invoke-interface {v9}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v9

    invoke-virtual {v3}, Lmaps/ac/bd;->f()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v9, v3}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-static/range {p1 .. p1}, Lmaps/au/aq;->c(Lmaps/as/a;)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v3, v7, -0x1

    move v7, v3

    goto :goto_6

    :cond_e
    const/4 v3, 0x0

    move v5, v3

    goto/16 :goto_3

    :cond_f
    const/4 v3, 0x0

    move v14, v3

    goto/16 :goto_4

    :cond_10
    const/4 v3, 0x0

    move v13, v3

    goto/16 :goto_5

    :cond_11
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v7, 0x1e00

    const/16 v9, 0x1e00

    const/16 v10, 0x1e00

    invoke-interface {v3, v7, v9, v10}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    :cond_12
    move v9, v4

    :goto_7
    if-ltz v9, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->v:[I

    aget v3, v3, v9

    if-lez v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->v:[I

    aget v3, v3, v9

    sub-int v7, v8, v3

    if-eqz v5, :cond_14

    move v4, v7

    :goto_8
    if-ge v4, v8, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    invoke-interface {v3}, Lmaps/au/ap;->f()Z

    move-result v10

    if-nez v10, :cond_13

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v10

    invoke-virtual {v10}, Lmaps/ac/bt;->b()I

    move-result v10

    const/16 v11, 0xe

    if-lt v10, v11, :cond_13

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v10

    const/high16 v11, 0x40000000    # 2.0f

    invoke-virtual {v3}, Lmaps/ac/bt;->b()I

    move-result v12

    shr-int/2addr v11, v12

    invoke-interface {v10}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-virtual {v3}, Lmaps/ac/bt;->g()Lmaps/ac/av;

    move-result-object v3

    int-to-float v11, v11

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v3, v11}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    move-object/from16 v0, p1

    iget-object v3, v0, Lmaps/as/a;->d:Lmaps/at/n;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lmaps/at/n;->d(Lmaps/as/a;)V

    invoke-static {v15}, Lmaps/ap/f;->a(Lmaps/ap/b;)[I

    move-result-object v3

    const/4 v11, 0x0

    aget v11, v3, v11

    const/4 v12, 0x1

    aget v12, v3, v12

    const/16 v19, 0x2

    aget v19, v3, v19

    const/16 v20, 0x3

    aget v3, v3, v20

    move/from16 v0, v19

    invoke-interface {v10, v11, v12, v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    const/4 v3, 0x5

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-interface {v10, v3, v11, v12}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    invoke-interface {v10}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    :cond_13
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_8

    :cond_14
    if-eqz v14, :cond_16

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e01

    const/16 v10, 0x1e01

    const/16 v11, 0x1e01

    invoke-interface {v3, v4, v10, v11}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x7f

    invoke-interface {v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    move v4, v7

    :goto_9
    if-ge v4, v8, :cond_15

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v10

    const/16 v11, 0x200

    add-int/lit8 v12, v4, 0x1

    const/16 v19, 0x7f

    move/from16 v0, v19

    invoke-interface {v10, v11, v12, v0}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v10

    invoke-interface {v10}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v10

    invoke-virtual {v3}, Lmaps/ac/bd;->f()I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v10, v3}, Lmaps/ap/o;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ac/av;F)V

    invoke-static/range {p1 .. p1}, Lmaps/au/aq;->c(Lmaps/as/a;)V

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_9

    :cond_15
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    const/16 v4, 0x1e00

    const/16 v10, 0x1e00

    const/16 v11, 0x1e00

    invoke-interface {v3, v4, v10, v11}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    :cond_16
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v15, v7, v8}, Lmaps/ay/ap;->a(Lmaps/ar/a;Lmaps/ap/b;II)I

    move-result v4

    const/4 v3, 0x0

    move v11, v3

    move v12, v4

    :goto_a
    if-eqz v12, :cond_1f

    and-int/lit8 v3, v12, 0x1

    if-eqz v3, :cond_1e

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->B()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lmaps/ap/c;->a(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/au/ap;->b(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    const/4 v3, 0x1

    shl-int v19, v3, v11

    move v10, v7

    :goto_b
    if-ge v10, v8, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmaps/au/ap;

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v4

    invoke-virtual {v4}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v20

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    move-object/from16 v21, v0

    sget-object v22, Lmaps/ao/b;->n:Lmaps/ao/b;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_17

    if-eqz v20, :cond_17

    sget-object v4, Lmaps/ac/bx;->d:Lmaps/ac/bx;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lmaps/ac/cf;->a(Lmaps/ac/bx;)Lmaps/ac/bw;

    move-result-object v4

    check-cast v4, Lmaps/ac/ae;

    if-nez v4, :cond_1c

    const/4 v4, 0x0

    :goto_c
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lmaps/aj/ad;->a(Lmaps/ac/o;)Lmaps/aj/af;

    move-result-object v4

    if-eqz v4, :cond_25

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lmaps/ay/ap;->t:[I

    move-object/from16 v20, v0

    aget v20, v20, v10

    and-int v20, v20, v19

    if-eqz v20, :cond_25

    if-eqz v14, :cond_18

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    const/16 v20, 0x202

    add-int/lit8 v21, v10, 0x1

    const/16 v22, 0x7f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-interface {v6, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    :cond_18
    if-eqz v13, :cond_19

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    const/16 v20, 0x202

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lmaps/ac/bt;->b()I

    move-result v21

    const/16 v22, 0x7f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-interface {v6, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    invoke-interface {v3}, Lmaps/au/ap;->g()V

    :cond_19
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->B()V

    invoke-interface {v3}, Lmaps/au/ap;->a()Lmaps/ac/bt;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v6

    invoke-virtual {v6}, Lmaps/ac/bd;->c()Lmaps/ac/av;

    move-result-object v6

    if-eqz v4, :cond_1a

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v4, v0, v1, v2, v6}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;Lmaps/ac/av;)V

    :cond_1a
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Lmaps/au/ap;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V

    if-eqz v4, :cond_1b

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v4, v0, v1}, Lmaps/aj/af;->a(Lmaps/as/a;Lmaps/ap/c;)V

    :cond_1b
    const/4 v3, 0x1

    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->C()V

    :goto_d
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    move v6, v3

    goto/16 :goto_b

    :cond_1c
    invoke-virtual {v4}, Lmaps/ac/ae;->b()Lmaps/ac/r;

    move-result-object v4

    goto/16 :goto_c

    :cond_1d
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->C()V

    :cond_1e
    add-int/lit8 v3, v11, 0x1

    ushr-int/lit8 v4, v12, 0x1

    move v11, v3

    move v12, v4

    goto/16 :goto_a

    :cond_1f
    if-eqz v18, :cond_20

    if-nez v6, :cond_21

    :cond_20
    move v3, v7

    :goto_e
    add-int/lit8 v4, v9, -0x1

    move v9, v4

    move v8, v3

    goto/16 :goto_7

    :cond_21
    if-nez v14, :cond_22

    if-eqz v13, :cond_23

    :cond_22
    invoke-virtual/range {p1 .. p1}, Lmaps/as/a;->y()V

    :cond_23
    invoke-virtual/range {v16 .. v16}, Lmaps/aj/ad;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lmaps/ay/ap;->I:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lmaps/ah/d;->b(Ljava/util/List;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lmaps/ay/ap;->I:Z

    goto/16 :goto_0

    :cond_24
    move v3, v8

    goto :goto_e

    :cond_25
    move v3, v6

    goto :goto_d
.end method

.method public final a(Lmaps/ay/ar;)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->B:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected a(Lmaps/u/a;)V
    .locals 2

    iput-object p1, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lmaps/ay/ap;->G:J

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0, p1}, Lmaps/ah/d;->a(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ap;->A:Z

    return-void
.end method

.method public final a(Lmaps/ac/bw;)Z
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-virtual {v0, p1}, Lmaps/ao/a;->a(Lmaps/ac/bw;)Z

    move-result v0

    return v0
.end method

.method protected aq_()Lmaps/aj/ae;
    .locals 1

    sget-object v0, Lmaps/aj/ae;->a:Lmaps/aj/ae;

    return-object v0
.end method

.method public final ar_()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/ap;->c:Z

    return v0
.end method

.method protected b(Lmaps/ar/a;)Ljava/util/Set;
    .locals 1

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lmaps/ay/ap;->j:Z

    return-void
.end method

.method public b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 13

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ay/ap;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ap;->I:Z

    iget-object v0, p0, Lmaps/ay/ap;->D:Lmaps/ac/av;

    invoke-virtual {p1, v0}, Lmaps/ar/a;->a(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    invoke-interface {v0, p1}, Lmaps/u/a;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lmaps/ay/ap;->w:Lmaps/ay/as;

    invoke-virtual {p1}, Lmaps/ar/a;->g()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ay/as;->a(Lmaps/ac/av;)V

    iget-object v0, p0, Lmaps/ay/ap;->w:Lmaps/ay/as;

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    iget v0, p0, Lmaps/ay/ap;->k:I

    int-to-float v0, v0

    invoke-virtual {p1}, Lmaps/ar/a;->k()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v7, v0

    iget-object v0, p0, Lmaps/ay/ap;->P:Ljava/util/Set;

    iget-object v1, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmaps/ay/ap;->v:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lmaps/ay/ap;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->g()Z

    move-result v0

    iget-boolean v1, p0, Lmaps/ay/ap;->z:Z

    if-eqz v1, :cond_8

    iget-object v0, p0, Lmaps/ay/ap;->E:Lmaps/ar/b;

    const/4 v1, 0x0

    iput-object v1, p0, Lmaps/ay/ap;->E:Lmaps/ar/b;

    if-eqz v0, :cond_7

    new-instance v3, Lmaps/ar/a;

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v1

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v2

    invoke-virtual {p1}, Lmaps/ar/a;->k()F

    move-result v4

    invoke-direct {v3, v0, v1, v2, v4}, Lmaps/ar/a;-><init>(Lmaps/ar/b;IIF)V

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    iget-object v1, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    new-instance v2, Lmaps/ac/av;

    invoke-virtual {v3}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v4

    invoke-direct {v2, v4}, Lmaps/ac/av;-><init>(Lmaps/ac/av;)V

    iget-object v4, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    invoke-interface {v4, v3}, Lmaps/u/a;->b(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    iget-boolean v5, p0, Lmaps/ay/ap;->z:Z

    invoke-virtual/range {v0 .. v5}, Lmaps/ah/d;->a(Lmaps/u/a;Lmaps/ac/av;Ljava/util/List;Ljava/util/Set;Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->b()I

    move-result v4

    invoke-static {}, Lmaps/aq/a;->a()Lmaps/aq/a;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->c()V

    const/4 v0, 0x0

    move v2, v0

    move-object v3, v6

    :goto_1
    if-gt v2, v8, :cond_c

    if-ne v2, v8, :cond_a

    const/4 v0, 0x0

    move-object v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    iget-object v10, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v10, v0}, Lmaps/ah/d;->a(Lmaps/ac/bt;)Lmaps/au/ap;

    move-result-object v10

    if-eqz v10, :cond_4

    iget-object v11, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v11, p0, Lmaps/ay/ap;->R:Ljava/lang/ref/WeakReference;

    invoke-virtual {v11}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    iget-object v11, p0, Lmaps/ay/ap;->P:Ljava/util/Set;

    invoke-interface {v11, v10}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Lmaps/au/ap;->a(Z)V

    :cond_3
    iget-object v11, p0, Lmaps/ay/ap;->v:[I

    aget v12, v11, v2

    add-int/lit8 v12, v12, 0x1

    aput v12, v11, v2

    iget-object v11, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    iget v12, p0, Lmaps/ay/ap;->g:I

    if-eq v11, v12, :cond_b

    :cond_4
    if-eqz v10, :cond_5

    invoke-interface {v10}, Lmaps/au/ap;->f()Z

    move-result v10

    if-eqz v10, :cond_2

    :cond_5
    if-eqz v1, :cond_6

    iget-object v10, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    iget-object v11, p0, Lmaps/ay/ap;->D:Lmaps/ac/av;

    invoke-interface {v10, v0, v11}, Lmaps/u/a;->a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v10

    if-eqz v10, :cond_6

    invoke-interface {v1, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_6
    if-nez v2, :cond_2

    invoke-interface {v9, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_7
    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->e()V

    goto :goto_0

    :cond_8
    iget-wide v1, p0, Lmaps/ay/ap;->F:J

    invoke-virtual {p1}, Lmaps/ar/a;->e()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_9

    iget-wide v1, p0, Lmaps/ay/ap;->G:J

    iget-object v3, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    invoke-interface {v3}, Lmaps/u/a;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_9

    iget-wide v1, p0, Lmaps/ay/ap;->H:J

    iget-object v3, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-virtual {v3}, Lmaps/ao/a;->b()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lmaps/ay/ap;->A:Z

    if-nez v1, :cond_9

    if-eqz v0, :cond_1

    :cond_9
    invoke-virtual {p0, p1}, Lmaps/ay/ap;->b(Lmaps/ar/a;)Ljava/util/Set;

    move-result-object v4

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    iget-object v1, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    new-instance v2, Lmaps/ac/av;

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v3

    invoke-direct {v2, v3}, Lmaps/ac/av;-><init>(Lmaps/ac/av;)V

    iget-boolean v5, p0, Lmaps/ay/ap;->z:Z

    move-object v3, v6

    invoke-virtual/range {v0 .. v5}, Lmaps/ah/d;->a(Lmaps/u/a;Lmaps/ac/av;Ljava/util/List;Ljava/util/Set;Z)V

    invoke-virtual {p1}, Lmaps/ar/a;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ay/ap;->F:J

    iget-object v0, p0, Lmaps/ay/ap;->x:Lmaps/u/a;

    invoke-interface {v0}, Lmaps/u/a;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ay/ap;->G:J

    iget-object v0, p0, Lmaps/ay/ap;->Q:Lmaps/ao/a;

    invoke-virtual {v0}, Lmaps/ao/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lmaps/ay/ap;->H:J

    goto/16 :goto_0

    :cond_a
    :try_start_1
    iget-object v0, p0, Lmaps/ay/ap;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move-object v1, v0

    goto/16 :goto_2

    :cond_b
    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Lmaps/ay/ap;->g:I

    if-eq v0, v3, :cond_c

    if-eqz v1, :cond_c

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-eqz v0, :cond_c

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v3, v1

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->d()V

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lmaps/ay/ap;->v:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lmaps/ay/ap;->c:Z

    iget-boolean v0, p0, Lmaps/ay/ap;->j:Z

    if-eqz v0, :cond_f

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    new-instance v5, Lmaps/au/j;

    shl-int v2, v7, v2

    invoke-direct {v5, v0, v2}, Lmaps/au/j;-><init>(Lmaps/ac/bt;I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_d
    const/4 v0, 0x0

    goto :goto_4

    :cond_e
    iget-object v0, p0, Lmaps/ay/ap;->y:Lmaps/y/f;

    invoke-virtual {v0, p1}, Lmaps/y/f;->c(Lmaps/ar/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->p()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v0}, Lmaps/ac/bt;->b()I

    move-result v5

    sub-int/2addr v3, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lmaps/ay/ap;->s:Ljava/util/ArrayList;

    new-instance v5, Lmaps/au/j;

    shl-int v2, v7, v2

    invoke-direct {v5, v0, v2}, Lmaps/au/j;-><init>(Lmaps/ac/bt;I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_f
    iget-boolean v0, p0, Lmaps/ay/ap;->z:Z

    iput-boolean v0, p0, Lmaps/ay/ap;->A:Z

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->b()I

    move-result v0

    sub-int/2addr v0, v4

    iget-boolean v1, p0, Lmaps/ay/ap;->A:Z

    if-nez v1, :cond_10

    if-nez v0, :cond_10

    iget-object v1, p0, Lmaps/ay/ap;->B:Ljava/util/Set;

    monitor-enter v1

    :try_start_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lmaps/ay/ap;->B:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ay/ar;

    iget-object v2, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    invoke-interface {v0}, Lmaps/ay/ar;->a()Z

    goto :goto_7

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_10
    iget-object v0, p0, Lmaps/ay/ap;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/au/ap;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lmaps/au/ap;->a(Z)V

    goto :goto_8

    :cond_11
    iget-object v0, p0, Lmaps/ay/ap;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lmaps/as/a;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ay/ap;->R:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lmaps/ay/ap;->b:Lmaps/ap/n;

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ap;->A:Z

    return-void
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->i:Lmaps/ay/v;

    return-object v0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->r:Lmaps/ah/d;

    invoke-virtual {v0}, Lmaps/ah/d;->f()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ay/ap;->A:Z

    return-void
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/ap;->J:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ay/ap;->K:Z

    return v0
.end method

.method public final l()Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    return-object v0
.end method

.method public final n()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final p()Lmaps/u/c;
    .locals 1

    iget-object v0, p0, Lmaps/ay/ap;->L:Lmaps/u/c;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "tileType"

    iget-object v2, p0, Lmaps/ay/ap;->q:Lmaps/ao/b;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "isBase"

    iget-boolean v2, p0, Lmaps/ay/ap;->K:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "maxTilesPerView"

    iget v2, p0, Lmaps/ay/ap;->g:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    const-string v1, "maxTilesToFetch"

    iget v2, p0, Lmaps/ay/ap;->h:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    const-string v1, "drawOrder"

    iget-object v2, p0, Lmaps/ay/ap;->i:Lmaps/ay/v;

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    const-string v1, "fetchMissingAncestorTiles"

    iget-boolean v2, p0, Lmaps/ay/ap;->m:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lmaps/ay/ap;->p:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "prefetchAncestors"

    iget-boolean v2, p0, Lmaps/ay/ap;->o:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "tileSize"

    iget v2, p0, Lmaps/ay/ap;->k:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    const-string v1, "allowMultiZoom"

    iget-boolean v2, p0, Lmaps/ay/ap;->p:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "isContributingLabels"

    iget-boolean v2, p0, Lmaps/ay/ap;->J:Z

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Z)Lmaps/k/k;

    move-result-object v0

    const-string v1, "maxTileSize"

    iget v2, p0, Lmaps/ay/ap;->l:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
