.class public final Lmaps/bz/c;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/util/Enumeration;

.field private synthetic d:Lmaps/bz/b;


# direct methods
.method public constructor <init>(Lmaps/bz/b;)V
    .locals 1

    iput-object p1, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/bz/c;->a:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lmaps/bz/c;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/bz/c;->c:Ljava/util/Enumeration;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    const/4 v1, 0x1

    iget v0, p0, Lmaps/bz/c;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/bz/c;->a:I

    iget-object v2, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-static {v2}, Lmaps/bz/b;->a(Lmaps/bz/b;)I

    move-result v2

    if-gt v0, v2, :cond_2

    :goto_1
    iget v0, p0, Lmaps/bz/c;->a:I

    iget-object v2, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-static {v2}, Lmaps/bz/b;->a(Lmaps/bz/b;)I

    move-result v2

    if-gt v0, v2, :cond_2

    iget-object v0, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-static {v0}, Lmaps/bz/b;->b(Lmaps/bz/b;)[Ljava/lang/Object;

    move-result-object v0

    iget v2, p0, Lmaps/bz/c;->a:I

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/bz/c;->a:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lmaps/bz/c;->a:I

    iput v0, p0, Lmaps/bz/c;->b:I

    move v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lmaps/bz/c;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/bz/c;->a:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-static {v0}, Lmaps/bz/b;->c(Lmaps/bz/b;)Ljava/util/Hashtable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/bz/c;->c:Ljava/util/Enumeration;

    if-nez v0, :cond_3

    iget-object v0, p0, Lmaps/bz/c;->d:Lmaps/bz/b;

    invoke-static {v0}, Lmaps/bz/b;->c(Lmaps/bz/b;)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    iput-object v0, p0, Lmaps/bz/c;->c:Ljava/util/Enumeration;

    :cond_3
    iget-object v0, p0, Lmaps/bz/c;->c:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/bz/c;->c:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lmaps/bz/c;->b:I

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    invoke-virtual {p0}, Lmaps/bz/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lmaps/bz/c;->b:I

    const/high16 v1, -0x80000000

    iput v1, p0, Lmaps/bz/c;->b:I

    return v0
.end method
