.class public final Lmaps/ak/b;
.super Ljava/lang/Object;


# instance fields
.field private volatile a:F

.field private b:Lmaps/ak/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x42960000    # 75.0f

    iput v0, p0, Lmaps/ak/b;->a:F

    return-void
.end method

.method public static b(F)F
    .locals 4

    const/high16 v3, 0x41600000    # 14.0f

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v0, 0x41f00000    # 30.0f

    const/high16 v1, 0x41800000    # 16.0f

    cmpl-float v1, p0, v1

    if-ltz v1, :cond_1

    const/high16 v0, 0x42960000    # 75.0f

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpl-float v1, p0, v3

    if-lez v1, :cond_2

    const/high16 v1, 0x42340000    # 45.0f

    sub-float v2, p0, v3

    mul-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0

    :cond_2
    cmpl-float v1, p0, v2

    if-lez v1, :cond_0

    sub-float v1, p0, v2

    const/high16 v2, 0x41700000    # 15.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lmaps/ak/c;
    .locals 1

    iget-object v0, p0, Lmaps/ak/b;->b:Lmaps/ak/c;

    return-object v0
.end method

.method public final a(Lmaps/ar/b;)Lmaps/ar/b;
    .locals 7

    const/high16 v1, 0x41a80000    # 21.0f

    const/high16 v0, 0x40000000    # 2.0f

    const/4 v6, 0x0

    invoke-virtual {p1}, Lmaps/ar/b;->d()F

    move-result v4

    iget v2, p0, Lmaps/ak/b;->a:F

    invoke-virtual {p1}, Lmaps/ar/b;->b()F

    move-result v3

    invoke-static {v3}, Lmaps/ak/b;->b(F)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget-object v2, p0, Lmaps/ak/b;->b:Lmaps/ak/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lmaps/ak/b;->b:Lmaps/ak/c;

    invoke-virtual {p1}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v5

    invoke-interface {v2, v5}, Lmaps/ak/c;->a(Lmaps/ac/av;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lmaps/ak/b;->b:Lmaps/ak/c;

    invoke-interface {v2}, Lmaps/ak/c;->a()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_0
    invoke-virtual {p1}, Lmaps/ar/b;->b()F

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {p1}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v1

    cmpl-float v0, v4, v3

    if-lez v0, :cond_1

    invoke-virtual {v1, v1}, Lmaps/ac/av;->h(Lmaps/ac/av;)V

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p1}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    :goto_0
    return-object v0

    :cond_1
    cmpg-float v0, v4, v6

    if-gez v0, :cond_2

    invoke-virtual {v1, v1}, Lmaps/ac/av;->h(Lmaps/ac/av;)V

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p1}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/ar/b;->f()F

    move-result v5

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lmaps/ar/b;->b()F

    move-result v0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lmaps/ar/b;->c()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ac/av;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {v1, v1}, Lmaps/ac/av;->h(Lmaps/ac/av;)V

    new-instance v0, Lmaps/ar/b;

    invoke-virtual {p1}, Lmaps/ar/b;->d()F

    move-result v3

    invoke-virtual {p1}, Lmaps/ar/b;->e()F

    move-result v4

    invoke-virtual {p1}, Lmaps/ar/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lmaps/ar/b;-><init>(Lmaps/ac/av;FFFF)V

    goto :goto_0

    :cond_4
    move-object v0, p1

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    const/high16 v0, 0x42870000    # 67.5f

    iput v0, p0, Lmaps/ak/b;->a:F

    return-void
.end method

.method public final a(Lmaps/ak/c;)V
    .locals 0

    iput-object p1, p0, Lmaps/ak/b;->b:Lmaps/ak/c;

    return-void
.end method

.method public final b()F
    .locals 1

    iget v0, p0, Lmaps/ak/b;->a:F

    return v0
.end method
