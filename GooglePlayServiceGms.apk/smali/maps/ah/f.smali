.class final Lmaps/ah/f;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/af/d;


# instance fields
.field private synthetic a:Lmaps/ah/d;


# direct methods
.method private constructor <init>(Lmaps/ah/d;)V
    .locals 0

    iput-object p1, p0, Lmaps/ah/f;->a:Lmaps/ah/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmaps/ah/d;B)V
    .locals 0

    invoke-direct {p0, p1}, Lmaps/ah/f;-><init>(Lmaps/ah/d;)V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/bt;ILmaps/ac/bs;)V
    .locals 3

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lmaps/ah/f;->a:Lmaps/ah/d;

    invoke-static {v0, p1, p2, p3}, Lmaps/ah/d;->a(Lmaps/ah/d;Lmaps/ac/bt;ILmaps/ac/bs;)Lmaps/au/ap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lmaps/ah/f;->a:Lmaps/ah/d;

    invoke-static {v0}, Lmaps/ah/d;->c(Lmaps/ah/d;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_2

    const-string v0, "TileFetcher"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received an unknown tile "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    iget-object v0, p0, Lmaps/ah/f;->a:Lmaps/ah/d;

    invoke-static {v0, p1, v1}, Lmaps/ah/d;->a(Lmaps/ah/d;Lmaps/ac/bt;Lmaps/au/ap;)V

    goto :goto_0
.end method
