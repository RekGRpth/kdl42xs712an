.class final Lmaps/ah/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:I

.field private c:Lmaps/ac/bt;

.field private d:Ljava/util/LinkedHashSet;

.field private e:Ljava/util/LinkedHashSet;

.field private f:Ljava/util/Iterator;

.field private synthetic g:Lmaps/ah/a;


# direct methods
.method public constructor <init>(Lmaps/ah/a;Z)V
    .locals 1

    iput-object p1, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ah/b;->b:I

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    iput-boolean p2, p0, Lmaps/ah/b;->a:Z

    return-void
.end method


# virtual methods
.method public final a(Z)Lmaps/ah/c;
    .locals 5

    iget-object v0, p0, Lmaps/ah/b;->c:Lmaps/ac/bt;

    if-eqz v0, :cond_1

    iget v0, p0, Lmaps/ah/b;->b:I

    iget-object v1, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-static {v1}, Lmaps/ah/a;->c(Lmaps/ah/a;)I

    move-result v1

    if-ge v0, v1, :cond_1

    if-nez p1, :cond_0

    iget-object v0, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-static {v0}, Lmaps/ah/a;->e(Lmaps/ah/a;)Lmaps/u/a;

    move-result-object v0

    iget-object v1, p0, Lmaps/ah/b;->c:Lmaps/ac/bt;

    iget-object v2, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-static {v2}, Lmaps/ah/a;->d(Lmaps/ah/a;)Lmaps/ac/av;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lmaps/u/a;->a(Lmaps/ac/bt;Lmaps/ac/av;)Lmaps/ac/bt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lmaps/ah/b;->f:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lmaps/ah/b;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/ah/b;->b:I

    iget-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    iput-object v1, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    iput-object v0, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    iget-object v0, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/b;->f:Ljava/util/Iterator;

    :cond_1
    iget-object v0, p0, Lmaps/ah/b;->f:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lmaps/ah/b;->f:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    iput-object v0, p0, Lmaps/ah/b;->c:Lmaps/ac/bt;

    new-instance v1, Lmaps/ah/c;

    iget-object v2, p0, Lmaps/ah/b;->c:Lmaps/ac/bt;

    iget-object v0, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-static {v0}, Lmaps/ah/a;->b(Lmaps/ah/a;)J

    move-result-wide v3

    iget-boolean v0, p0, Lmaps/ah/b;->a:Z

    if-nez v0, :cond_3

    iget v0, p0, Lmaps/ah/b;->b:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, v2, v3, v4, v0}, Lmaps/ah/c;-><init>(Lmaps/ac/bt;JZ)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lmaps/ah/b;->b:I

    iget-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    iget-object v0, p0, Lmaps/ah/b;->e:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ah/b;->c:Lmaps/ac/bt;

    iget-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    iget-object v1, p0, Lmaps/ah/b;->g:Lmaps/ah/a;

    invoke-static {v1}, Lmaps/ah/a;->a(Lmaps/ah/a;)Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lmaps/ah/b;->d:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lmaps/ah/b;->f:Ljava/util/Iterator;

    return-void
.end method
