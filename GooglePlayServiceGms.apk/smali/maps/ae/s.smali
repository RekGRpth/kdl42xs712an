.class public final Lmaps/ae/s;
.super Lmaps/ac/bt;


# instance fields
.field private final h:Lmaps/ac/ai;


# direct methods
.method private constructor <init>(Lmaps/ac/bt;Lmaps/ac/ai;)V
    .locals 4

    iget v0, p1, Lmaps/ac/bt;->a:I

    iget v1, p1, Lmaps/ac/bt;->b:I

    iget v2, p1, Lmaps/ac/bt;->c:I

    iget-object v3, p1, Lmaps/ac/bt;->d:Lmaps/ac/cf;

    invoke-direct {p0, v0, v1, v2, v3}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    iput-object p2, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    return-void
.end method


# virtual methods
.method public final a(Lmaps/ac/cf;)Lmaps/ac/bt;
    .locals 3

    new-instance v0, Lmaps/ae/s;

    invoke-super {p0, p1}, Lmaps/ac/bt;->a(Lmaps/ac/cf;)Lmaps/ac/bt;

    move-result-object v1

    iget-object v2, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    invoke-direct {v0, v1, v2}, Lmaps/ae/s;-><init>(Lmaps/ac/bt;Lmaps/ac/ai;)V

    return-object v0
.end method

.method public final a(Lmaps/ae/s;)Z
    .locals 2

    iget-object v0, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    iget-object v1, p1, Lmaps/ae/s;->h:Lmaps/ac/ai;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p1, Lmaps/ae/s;

    if-eqz v1, :cond_2

    invoke-super {p0, p1}, Lmaps/ac/bt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Lmaps/ae/s;

    invoke-virtual {p0, p1}, Lmaps/ae/s;->a(Lmaps/ae/s;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    invoke-super {p0}, Lmaps/ac/bt;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final k()Lmaps/ac/ai;
    .locals 1

    iget-object v0, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[layer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    invoke-virtual {v1}, Lmaps/ac/ai;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, " params: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lmaps/ae/s;->h:Lmaps/ac/ai;

    invoke-virtual {v0}, Lmaps/ac/ai;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    const-string v0, " coords: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lmaps/ac/bt;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
