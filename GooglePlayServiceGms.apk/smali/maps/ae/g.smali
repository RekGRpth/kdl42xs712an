.class final Lmaps/ae/g;
.super Landroid/os/Handler;


# instance fields
.field private synthetic a:Lmaps/ae/d;


# direct methods
.method constructor <init>(Lmaps/ae/d;)V
    .locals 0

    iput-object p1, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmaps/ae/k;

    invoke-static {v1, v0}, Lmaps/ae/d;->a(Lmaps/ae/d;Lmaps/ae/k;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->a(Lmaps/ae/d;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lmaps/ae/h;

    invoke-static {v1, v0}, Lmaps/ae/d;->a(Lmaps/ae/d;Lmaps/ae/h;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->b(Lmaps/ae/d;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->c(Lmaps/ae/d;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v3, p0, Lmaps/ae/g;->a:Lmaps/ae/d;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmaps/ac/bu;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lmaps/ae/c;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmaps/af/d;

    invoke-static {v3, v1, v2, v0}, Lmaps/ae/d;->a(Lmaps/ae/d;Lmaps/ac/bu;Lmaps/ae/c;Lmaps/af/d;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
