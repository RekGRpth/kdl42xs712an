.class public abstract Lmaps/ae/b;
.super Lmaps/ae/h;


# instance fields
.field protected a:I

.field protected b:[[B

.field private synthetic f:Lmaps/ae/a;


# direct methods
.method protected constructor <init>(Lmaps/ae/a;)V
    .locals 1

    iput-object p1, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-direct {p0}, Lmaps/ae/h;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [[B

    iput-object v0, p0, Lmaps/ae/b;->b:[[B

    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 17

    move-object/from16 v0, p0

    iget v12, v0, Lmaps/ae/h;->d:I

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    new-instance v3, Lmaps/bv/a;

    const/4 v1, 0x0

    invoke-direct {v3, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    sget-object v1, Lmaps/cm/y;->d:Lmaps/bv/c;

    move-object/from16 v0, p1

    invoke-static {v1, v0, v3}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/InputStream;Lmaps/bv/a;)I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_a

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v6

    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Lmaps/bv/a;->d(I)I

    move-result v1

    :goto_1
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lmaps/bv/a;->c(I)[B

    move-result-object v4

    if-eqz v4, :cond_2

    array-length v3, v4

    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lmaps/ae/b;->a(II)[B

    move-result-object v13

    if-eqz v4, :cond_0

    const/4 v1, 0x0

    array-length v5, v13

    sub-int/2addr v5, v3

    invoke-static {v4, v1, v13, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    if-ge v2, v12, :cond_8

    const/16 v1, 0x1e

    invoke-virtual {v6, v1}, Lmaps/bv/a;->i(I)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x1e

    invoke-virtual {v6, v1}, Lmaps/bv/a;->e(I)J

    move-result-wide v9

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    move v11, v4

    move v4, v7

    move-wide v7, v9

    move/from16 v16, v5

    move v5, v1

    move/from16 v1, v16

    :goto_3
    const-wide/16 v14, 0x1

    cmp-long v14, v7, v14

    if-lez v14, :cond_3

    const-wide/16 v14, 0x3

    and-long/2addr v14, v7

    long-to-int v14, v14

    packed-switch v14, :pswitch_data_0

    :goto_4
    :pswitch_0
    const/4 v14, 0x2

    shr-long/2addr v7, v14

    shl-int/lit8 v5, v5, 0x1

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_1
    or-int/2addr v1, v5

    goto :goto_4

    :pswitch_2
    or-int/2addr v4, v5

    or-int/2addr v1, v5

    goto :goto_4

    :pswitch_3
    or-int/2addr v4, v5

    goto :goto_4

    :cond_3
    const-wide/16 v14, 0x1

    cmp-long v5, v7, v14

    if-eqz v5, :cond_4

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid TUVW "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9, v10}, Ljava/lang/Long;->toBinaryString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v5, Lmaps/bv/a;

    sget-object v7, Lmaps/cm/y;->e:Lmaps/bv/c;

    invoke-direct {v5, v7}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v7, 0x2

    invoke-virtual {v5, v7, v4}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v4, 0x3

    invoke-virtual {v5, v4, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v1, 0x4

    invoke-virtual {v5, v1, v11}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    move-object v1, v5

    :goto_5
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Lmaps/bv/a;->d(I)I

    move-result v4

    const/4 v5, 0x3

    invoke-virtual {v1, v5}, Lmaps/bv/a;->d(I)I

    move-result v5

    const/4 v7, 0x4

    invoke-virtual {v1, v7}, Lmaps/bv/a;->d(I)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v7, v0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v7}, Lmaps/ae/a;->e(Lmaps/ae/a;)I

    move-result v7

    sub-int v7, v1, v7

    new-instance v8, Lmaps/ac/cf;

    invoke-direct {v8}, Lmaps/ac/cf;-><init>()V

    invoke-static {}, Lmaps/ac/bx;->values()[Lmaps/ac/bx;

    move-result-object v9

    array-length v10, v9

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v10, :cond_7

    aget-object v11, v9, v1

    invoke-virtual {v11, v6}, Lmaps/ac/bx;->a(Lmaps/bv/a;)Lmaps/ac/bw;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v8, v11}, Lmaps/ac/cf;->a(Lmaps/ac/bw;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    move-object v1, v6

    goto :goto_5

    :cond_7
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Lmaps/bv/a;->d(I)I

    move-result v1

    invoke-static {v1}, Lmaps/ao/b;->a(I)Lmaps/ao/b;

    move-result-object v1

    new-instance v6, Lmaps/ac/bt;

    invoke-direct {v6, v7, v4, v5, v8}, Lmaps/ac/bt;-><init>(IIILmaps/ac/cf;)V

    invoke-static {v1, v6}, Lmaps/aw/a;->a(Lmaps/ao/b;Lmaps/ac/bt;)Landroid/util/Pair;

    move-result-object v4

    iget-object v1, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_b

    new-instance v1, Landroid/util/Pair;

    iget-object v4, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v5, ""

    invoke-direct {v1, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lmaps/ae/h;->e:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-virtual {v1}, Lmaps/ae/a;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Received wrong tile"

    invoke-static {v1, v3}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    :goto_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    :cond_9
    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ae/b;->b:[[B

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput-object v13, v3, v1

    goto :goto_8

    :cond_a
    return-void

    :cond_b
    move-object v1, v4

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private f()Lmaps/ae/c;
    .locals 5

    sget-object v1, Lmaps/ae/c;->a:Lmaps/ae/c;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lmaps/ae/h;->d:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v2, v2, v0

    iget-object v2, v2, Lmaps/ae/k;->d:Lmaps/ae/c;

    sget-object v3, Lmaps/ae/c;->a:Lmaps/ae/c;

    if-eq v1, v3, :cond_0

    invoke-virtual {v2}, Lmaps/ae/c;->a()I

    move-result v3

    invoke-virtual {v1}, Lmaps/ae/c;->a()I

    move-result v4

    if-ge v3, v4, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method protected a()I
    .locals 1

    iget v0, p0, Lmaps/ae/b;->a:I

    return v0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 11

    new-instance v1, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/y;->b:Lmaps/bv/c;

    invoke-direct {v1, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    new-instance v2, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/y;->c:Lmaps/bv/c;

    invoke-direct {v2, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    const/4 v0, 0x1

    iget-object v3, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v3}, Lmaps/ae/a;->a(Lmaps/ae/a;)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v0, 0x4

    iget-object v3, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v3}, Lmaps/ae/a;->b(Lmaps/ae/a;)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v0, 0x5

    invoke-direct {p0}, Lmaps/ae/b;->f()Lmaps/ae/c;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ae/c;->a()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    iget-object v0, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v0}, Lmaps/ae/a;->c(Lmaps/ae/a;)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v0}, Lmaps/ae/a;->c(Lmaps/ae/a;)F

    move-result v0

    invoke-virtual {v1, v0}, Lmaps/bv/a;->a(F)Lmaps/bv/a;

    :cond_0
    iget-object v0, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v0}, Lmaps/ae/a;->d(Lmaps/ae/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v4, 0x2

    invoke-virtual {v1, v4, v0}, Lmaps/bv/a;->a(II)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {}, Lmaps/ae/a;->c()Z

    invoke-static {}, Lmaps/ap/p;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->a(II)V

    :cond_2
    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    const/4 v3, 0x4

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->a(II)V

    :cond_3
    invoke-static {}, Lmaps/ax/m;->a()Lmaps/ax/k;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ax/k;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    const/4 v3, 0x3

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->a(II)V

    :cond_4
    invoke-direct {p0}, Lmaps/ae/b;->f()Lmaps/ae/c;

    move-result-object v0

    sget-object v3, Lmaps/ae/c;->a:Lmaps/ae/c;

    if-eq v0, v3, :cond_5

    const/4 v0, 0x5

    invoke-direct {p0}, Lmaps/ae/b;->f()Lmaps/ae/c;

    move-result-object v3

    invoke-virtual {v3}, Lmaps/ae/c;->a()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    :cond_5
    const/4 v0, 0x3

    const/4 v3, 0x6

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->a(II)V

    iget-object v0, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    iget-object v0, v0, Lmaps/ae/a;->e:Lmaps/ao/b;

    sget-object v3, Lmaps/ao/b;->x:Lmaps/ao/b;

    if-ne v0, v3, :cond_6

    const/4 v0, 0x3

    const/4 v3, 0x7

    invoke-virtual {v1, v0, v3}, Lmaps/bv/a;->a(II)V

    :cond_6
    iget v1, p0, Lmaps/ae/h;->d:I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_8

    iget-object v3, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v3, v3, v0

    iget-object v4, v3, Lmaps/ae/k;->a:Lmaps/ac/bt;

    new-instance v5, Lmaps/bv/a;

    sget-object v6, Lmaps/cm/y;->e:Lmaps/bv/c;

    invoke-direct {v5, v6}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/16 v6, 0x1e

    invoke-virtual {v4}, Lmaps/ac/bt;->c()I

    move-result v7

    invoke-virtual {v4}, Lmaps/ac/bt;->d()I

    move-result v8

    invoke-virtual {v4}, Lmaps/ac/bt;->b()I

    move-result v9

    iget-object v10, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-static {v10}, Lmaps/ae/a;->e(Lmaps/ae/a;)I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v7, v8, v9}, Lmaps/ac/ck;->a(III)J

    move-result-wide v7

    invoke-virtual {v5, v6, v7, v8}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v6, 0x1

    iget-object v7, v3, Lmaps/ae/k;->h:Lmaps/ao/b;

    iget v7, v7, Lmaps/ao/b;->y:I

    invoke-virtual {v5, v6, v7}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    const/4 v6, 0x7

    iget-object v7, v3, Lmaps/ae/k;->h:Lmaps/ao/b;

    iget v7, v7, Lmaps/ao/b;->z:I

    invoke-virtual {v5, v6, v7}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    iget-object v6, v3, Lmaps/ae/k;->h:Lmaps/ao/b;

    invoke-virtual {v4, v6, v5}, Lmaps/ac/bt;->a(Lmaps/ao/b;Lmaps/bv/a;)V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->f()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    iget v3, v3, Lmaps/ae/k;->i:I

    invoke-virtual {v5, v4, v3}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    :cond_7
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v5}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    invoke-static {p1, v2}, Lmaps/bv/e;->a(Ljava/io/DataOutput;Lmaps/bv/a;)V

    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 6

    const/4 v5, 0x1

    invoke-static {p1}, Lmaps/bv/e;->a(Ljava/io/DataInput;)Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    new-instance v0, Lmaps/bv/a;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    sget-object v2, Lmaps/cm/y;->d:Lmaps/bv/c;

    invoke-static {v2, v1, v0}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/InputStream;Lmaps/bv/a;)I

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lmaps/bv/a;->d(I)I

    move-result v2

    iput v2, p0, Lmaps/ae/b;->a:I

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lmaps/bv/a;->d(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lmaps/ae/b;->f:Lmaps/ae/a;

    invoke-virtual {v2}, Lmaps/ae/a;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received tile response code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, v1}, Lmaps/ae/b;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return v5

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method protected final a(Lmaps/ae/k;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lmaps/ae/h;->d:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v2, v2, v1

    iget-object v2, v2, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->b()I

    move-result v2

    iget-object v3, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v3}, Lmaps/ac/bt;->b()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lmaps/ae/h;->c:[Lmaps/ae/k;

    aget-object v2, v2, v1

    iget-object v2, v2, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v2}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v2

    iget-object v3, p1, Lmaps/ae/k;->a:Lmaps/ac/bt;

    invoke-virtual {v3}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected a(II)[B
    .locals 1

    new-array v0, p1, [B

    return-object v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x6c

    return v0
.end method
