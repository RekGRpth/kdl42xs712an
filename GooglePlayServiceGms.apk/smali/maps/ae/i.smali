.class final Lmaps/ae/i;
.super Lmaps/ax/b;


# instance fields
.field private volatile a:Z

.field private volatile b:Z

.field private synthetic c:Lmaps/ae/d;


# direct methods
.method constructor <init>(Lmaps/ae/d;)V
    .locals 2

    iput-object p1, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CacheCommitter:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lmaps/ae/d;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/ax/b;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lmaps/ae/d;->d(Lmaps/ae/d;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ae/i;->b:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lmaps/ae/i;->start()V

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ae/i;->a:Z

    return-void
.end method

.method final b()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/ae/i;->b:Z

    return v0
.end method

.method public final e()V
    .locals 4

    :try_start_0
    invoke-static {}, Lmaps/ap/p;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    iget-object v0, v0, Lmaps/ae/d;->c:Lmaps/ae/aa;

    invoke-virtual {v0}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lmaps/ae/i;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not set thread priority: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lmaps/ae/i;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->e(Lmaps/ae/d;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->f(Lmaps/ae/d;)Z

    invoke-interface {v1}, Lmaps/ag/f;->b()V

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ae/i;->b:Z

    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->g(Lmaps/ae/d;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/ae/i;->a:Z

    :try_start_1
    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->d(Lmaps/ae/d;)I

    move-result v0

    :goto_3
    if-lez v0, :cond_2

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Lmaps/ae/i;->sleep(J)V

    iget-object v2, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v2}, Lmaps/ae/d;->e(Lmaps/ae/d;)Z

    move-result v2

    if-nez v2, :cond_2

    add-int/lit16 v0, v0, -0x3e8

    goto :goto_3

    :cond_2
    iget-boolean v0, p0, Lmaps/ae/i;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ae/i;->c:Lmaps/ae/d;

    invoke-static {v0}, Lmaps/ae/d;->f(Lmaps/ae/d;)Z

    invoke-interface {v1}, Lmaps/ag/f;->b()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method
