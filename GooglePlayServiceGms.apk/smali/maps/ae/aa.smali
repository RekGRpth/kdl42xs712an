.class public final Lmaps/ae/aa;
.super Ljava/lang/Object;


# instance fields
.field a:Lmaps/ag/aj;

.field b:Lmaps/ag/f;

.field private volatile c:Z

.field private d:I

.field private final e:Z

.field private f:Ljava/util/Locale;

.field private final g:Ljava/lang/String;

.field private h:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lmaps/ag/aj;Lmaps/ag/f;ZLjava/util/Locale;Ljava/io/File;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ae/aa;->g:Ljava/lang/String;

    iput-object p2, p0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    iput-object p3, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    const/4 v0, -0x1

    iput v0, p0, Lmaps/ae/aa;->d:I

    iput-boolean p4, p0, Lmaps/ae/aa;->e:Z

    iput-object p5, p0, Lmaps/ae/aa;->f:Ljava/util/Locale;

    iput-object p6, p0, Lmaps/ae/aa;->h:Ljava/io/File;

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    iget-object v1, p0, Lmaps/ae/aa;->h:Ljava/io/File;

    invoke-interface {v0, v1}, Lmaps/ag/f;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    :cond_0
    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ae/aa;->f:Ljava/util/Locale;

    iget-object v1, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    invoke-interface {v1}, Lmaps/ag/f;->e()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    iget-object v1, p0, Lmaps/ae/aa;->f:Ljava/util/Locale;

    invoke-interface {v0, v1}, Lmaps/ag/f;->a(Ljava/util/Locale;)Z

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/ae/aa;->c:Z

    :cond_2
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(I)Z
    .locals 2

    invoke-virtual {p0}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmaps/ag/f;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput p1, p0, Lmaps/ae/aa;->d:I

    iget-boolean v1, p0, Lmaps/ae/aa;->e:Z

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/ag/f;->a()Z

    :cond_1
    iget-object v0, p0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v0}, Lmaps/ag/aj;->a()Z

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lmaps/ag/f;
    .locals 1

    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lmaps/ae/aa;->c:Z

    if-nez v0, :cond_1

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmaps/ae/aa;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    return-object v0

    :cond_0
    monitor-exit p0

    :cond_1
    iget-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c()V
    .locals 2

    iget-object v0, p0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/ae/aa;->a:Lmaps/ag/aj;

    invoke-interface {v0}, Lmaps/ag/aj;->a()Z

    :cond_0
    invoke-virtual {p0}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lmaps/ag/f;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Lmaps/ag/f;->f()V

    iget-object v0, p0, Lmaps/ae/aa;->g:Ljava/lang/String;

    const-string v1, "Unable to clear disk cache"

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/ae/aa;->b:Lmaps/ag/f;

    :cond_1
    return-void
.end method

.method final d()I
    .locals 1

    invoke-virtual {p0}, Lmaps/ae/aa;->b()Lmaps/ag/f;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmaps/ag/f;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/ae/aa;->d:I

    goto :goto_0
.end method
