.class public abstract Lmaps/ae/a;
.super Lmaps/ae/d;


# instance fields
.field private volatile a:Z

.field private final b:I

.field private final g:Ljava/util/List;

.field private final h:I

.field private final i:I

.field private final j:F


# direct methods
.method protected constructor <init>(Lmaps/bn/k;Ljava/lang/String;Lmaps/ao/b;Ljava/util/List;IIFZLjava/util/Locale;ZLjava/io/File;Lmaps/ag/g;)V
    .locals 15

    invoke-virtual/range {p3 .. p3}, Lmaps/ao/b;->b()Lmaps/ag/aj;

    move-result-object v8

    invoke-static {}, Lmaps/aw/a;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move/from16 v2, p10

    move-object/from16 v3, p12

    invoke-virtual {v0, v1, v2, v3}, Lmaps/ao/b;->a(Ljava/lang/String;ZLmaps/ag/g;)Lmaps/ag/f;

    move-result-object v9

    :goto_0
    sget-object v4, Lmaps/ao/b;->d:Lmaps/ao/b;

    move-object/from16 v0, p3

    if-ne v0, v4, :cond_2

    const/16 v10, 0x3e8

    :goto_1
    move-object v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move-object/from16 v7, p2

    move/from16 v11, p8

    move/from16 v12, p6

    move-object/from16 v13, p9

    move-object/from16 v14, p11

    invoke-direct/range {v4 .. v14}, Lmaps/ae/d;-><init>(Lmaps/bn/k;Lmaps/ao/b;Ljava/lang/String;Lmaps/ag/aj;Lmaps/ag/f;IZILjava/util/Locale;Ljava/io/File;)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lmaps/ae/a;->a:Z

    const/16 v4, 0x100

    iput v4, p0, Lmaps/ae/a;->b:I

    move-object/from16 v0, p4

    iput-object v0, p0, Lmaps/ae/a;->g:Ljava/util/List;

    move/from16 v0, p5

    iput v0, p0, Lmaps/ae/a;->i:I

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    const/4 v4, 0x0

    iput v4, p0, Lmaps/ae/a;->h:I

    :goto_2
    move/from16 v0, p7

    iput v0, p0, Lmaps/ae/a;->j:F

    return-void

    :cond_1
    const/4 v9, 0x0

    goto :goto_0

    :cond_2
    const/16 v10, 0xbb8

    goto :goto_1

    :cond_3
    const/4 v4, 0x1

    iput v4, p0, Lmaps/ae/a;->h:I

    goto :goto_2
.end method

.method static synthetic a(Lmaps/ae/a;)I
    .locals 1

    iget v0, p0, Lmaps/ae/a;->b:I

    return v0
.end method

.method static synthetic b(Lmaps/ae/a;)I
    .locals 1

    iget v0, p0, Lmaps/ae/a;->i:I

    return v0
.end method

.method static synthetic c(Lmaps/ae/a;)F
    .locals 1

    iget v0, p0, Lmaps/ae/a;->j:F

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static synthetic d(Lmaps/ae/a;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lmaps/ae/a;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lmaps/ae/a;)I
    .locals 1

    iget v0, p0, Lmaps/ae/a;->h:I

    return v0
.end method
