.class final Lmaps/ag/x;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ag/r;


# instance fields
.field private a:Lmaps/ao/b;

.field private b:Ljava/util/Queue;

.field private c:Ljava/util/Map;

.field private d:Lmaps/ag/g;


# direct methods
.method public constructor <init>(Lmaps/ao/b;Lmaps/ag/g;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ag/x;->a:Lmaps/ao/b;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    iput-object p2, p0, Lmaps/ag/x;->d:Lmaps/ag/g;

    return-void
.end method

.method static synthetic a(Lmaps/ag/x;)Lmaps/ao/b;
    .locals 1

    iget-object v0, p0, Lmaps/ag/x;->a:Lmaps/ao/b;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/z;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/ag/z;-><init>(Lmaps/ag/x;B)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/ab;

    invoke-direct {v1, p0, p1}, Lmaps/ag/ab;-><init>(Lmaps/ag/x;I)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(J)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/aw/a;->a(J)Lmaps/ac/bt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/ae;

    invoke-direct {v2, p0, v0}, Lmaps/ag/ae;-><init>(Lmaps/ag/x;Lmaps/ac/bt;)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(JI)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/aw/a;->a(J)Lmaps/ac/bt;

    move-result-object v1

    iget-object v0, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bs;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/ac;

    invoke-direct {v2, p0, v0, p3}, Lmaps/ag/ac;-><init>(Lmaps/ag/x;Lmaps/ac/bs;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(Lmaps/ac/bs;)V
    .locals 2

    iget-object v0, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    invoke-interface {p1}, Lmaps/ac/bs;->a()Lmaps/ac/bt;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b()V
    .locals 3

    :goto_0
    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ag/y;

    iget-object v1, p0, Lmaps/ag/x;->d:Lmaps/ag/g;

    invoke-interface {v0}, Lmaps/ag/y;->a()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, "SDCardTileCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tiles were not inserted into the disk cache."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/aw/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lmaps/ag/x;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_1
    return-void
.end method

.method public final b(JI)V
    .locals 3

    invoke-static {p1, p2}, Lmaps/aw/a;->a(J)Lmaps/ac/bt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v2, Lmaps/ag/ad;

    invoke-direct {v2, p0, v0, p3}, Lmaps/ag/ad;-><init>(Lmaps/ag/x;Lmaps/ac/bt;I)V

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    iget-object v0, p0, Lmaps/ag/x;->b:Ljava/util/Queue;

    new-instance v1, Lmaps/ag/aa;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmaps/ag/aa;-><init>(Lmaps/ag/x;B)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    return-void
.end method
