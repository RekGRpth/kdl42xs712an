.class public final Lmaps/g/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/widget/RelativeLayout;

.field private final b:Ljava/util/Map;

.field private final c:Landroid/content/res/Resources;

.field private final d:Landroid/content/Context;

.field private e:Lmaps/g/e;

.field private f:Lmaps/g/c;

.field private g:Lmaps/c/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 9

    const/16 v8, 0xb

    const/4 v7, 0x2

    const/4 v4, -0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/m/co;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lmaps/g/a;->b:Ljava/util/Map;

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p1, p0, Lmaps/g/a;->d:Landroid/content/Context;

    iput-object p2, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/b/d;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/b/d;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/b/d;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/b/d;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v5, v1, v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-direct {p0, v7, v0}, Lmaps/g/a;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/b/d;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/b/d;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v4, Lmaps/b/d;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/4 v1, 0x3

    invoke-direct {p0, v1, v0}, Lmaps/g/a;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v2, Lmaps/b/d;->e:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    sget v3, Lmaps/b/d;->f:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lmaps/g/a;->a(ILandroid/widget/RelativeLayout$LayoutParams;)V

    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lmaps/g/a;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->removeViewInLayout(Landroid/view/View;)V

    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    iget-object v2, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v2, p2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(ILandroid/widget/RelativeLayout$LayoutParams;)V
    .locals 3

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lmaps/g/a;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setWillNotDraw(Z)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lmaps/g/a;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public final a(IIII)V
    .locals 1

    iget-object v0, p0, Lmaps/g/a;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    return-void
.end method

.method public final b()Lmaps/g/e;
    .locals 2

    iget-object v0, p0, Lmaps/g/a;->e:Lmaps/g/e;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/g/a;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/g/e;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/g/e;

    move-result-object v0

    iput-object v0, p0, Lmaps/g/a;->e:Lmaps/g/e;

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/g/a;->e:Lmaps/g/e;

    invoke-virtual {v1}, Lmaps/g/e;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/g/a;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/g/a;->e:Lmaps/g/e;

    return-object v0
.end method

.method public final c()Lmaps/g/c;
    .locals 3

    iget-object v0, p0, Lmaps/g/a;->f:Lmaps/g/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/g/a;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    new-instance v2, Landroid/view/View;

    invoke-direct {v2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    sget v0, Lmaps/b/e;->a:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lmaps/g/c;

    invoke-direct {v0, v2}, Lmaps/g/c;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lmaps/g/a;->f:Lmaps/g/c;

    const/4 v0, 0x2

    iget-object v1, p0, Lmaps/g/a;->f:Lmaps/g/c;

    invoke-virtual {v1}, Lmaps/g/c;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/g/a;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/g/a;->f:Lmaps/g/c;

    return-object v0
.end method

.method public final d()Lmaps/c/a;
    .locals 2

    iget-object v0, p0, Lmaps/g/a;->g:Lmaps/c/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmaps/g/a;->d:Landroid/content/Context;

    iget-object v1, p0, Lmaps/g/a;->c:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lmaps/c/a;->a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/c/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/g/a;->g:Lmaps/c/a;

    const/4 v0, 0x3

    iget-object v1, p0, Lmaps/g/a;->g:Lmaps/c/a;

    invoke-virtual {v1}, Lmaps/c/a;->a()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/g/a;->a(ILandroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lmaps/g/a;->g:Lmaps/c/a;

    return-object v0
.end method
