.class public final Lmaps/y/d;
.super Lmaps/y/g;


# instance fields
.field private d:Lmaps/ac/cw;

.field private final e:Ljava/util/List;

.field private final f:Lmaps/ac/av;

.field private g:Lmaps/ar/a;

.field private h:Lmaps/ac/cw;

.field private i:F

.field private final j:F

.field private k:J


# direct methods
.method public constructor <init>(Lmaps/ao/b;ILmaps/ao/a;)V
    .locals 2

    invoke-direct {p0, p1, p3}, Lmaps/y/g;-><init>(Lmaps/ao/b;Lmaps/ao/a;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    iput-object v0, p0, Lmaps/y/d;->f:Lmaps/ac/av;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lmaps/y/d;->k:J

    mul-int v0, p2, p2

    int-to-float v0, v0

    iput v0, p0, Lmaps/y/d;->j:F

    return-void
.end method

.method private a(Lmaps/ac/bt;Lmaps/ac/av;Z)V
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lmaps/y/d;->h:Lmaps/ac/cw;

    invoke-virtual {p1}, Lmaps/ac/bt;->i()Lmaps/ac/bd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lmaps/ac/cw;->b(Lmaps/ac/be;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    const/high16 v2, 0x20000000

    shr-int/2addr v2, v0

    iget-object v3, p0, Lmaps/y/d;->f:Lmaps/ac/av;

    invoke-virtual {p1}, Lmaps/ac/bt;->e()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {p1}, Lmaps/ac/bt;->f()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v5}, Lmaps/ac/av;->d(II)V

    iget-object v3, p0, Lmaps/y/d;->g:Lmaps/ar/a;

    iget-object v4, p0, Lmaps/y/d;->f:Lmaps/ac/av;

    invoke-virtual {v3, v4, v9}, Lmaps/ar/a;->a(Lmaps/ac/av;Z)F

    move-result v3

    iget-object v4, p0, Lmaps/y/d;->g:Lmaps/ar/a;

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v4, v2, v3}, Lmaps/ar/a;->b(FF)F

    move-result v2

    iget v3, p0, Lmaps/y/d;->i:F

    mul-float/2addr v3, v2

    mul-float/2addr v2, v3

    iget v3, p0, Lmaps/y/d;->j:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    const/16 v2, 0x1e

    if-ge v0, v2, :cond_5

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-super {p0, p2}, Lmaps/y/g;->c(Lmaps/ac/av;)Lmaps/aj/aj;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v2

    invoke-virtual {v0, v2}, Lmaps/aj/aj;->b(I)I

    move-result v4

    if-ltz v4, :cond_3

    invoke-virtual {p1}, Lmaps/ac/bt;->b()I

    move-result v0

    sub-int v5, v4, v0

    shl-int v6, v9, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_3

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_2

    invoke-virtual {p1}, Lmaps/ac/bt;->c()I

    move-result v7

    shl-int/2addr v7, v5

    add-int/2addr v7, v0

    invoke-virtual {p1}, Lmaps/ac/bt;->d()I

    move-result v8

    shl-int/2addr v8, v5

    add-int/2addr v8, v2

    invoke-virtual {p1, v4, v7, v8}, Lmaps/ac/bt;->a(III)Lmaps/ac/bt;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-direct {p0, v0, p2, v9}, Lmaps/y/d;->a(Lmaps/ac/bt;Lmaps/ac/av;Z)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lmaps/y/d;->k:J

    return-wide v0
.end method

.method public final b(Lmaps/ar/a;)Ljava/util/List;
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v3

    iget-object v0, p0, Lmaps/y/d;->d:Lmaps/ac/cw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/d;->d:Lmaps/ac/cw;

    invoke-virtual {v3, v0}, Lmaps/ac/cw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {v0}, Lmaps/ac/bt;->j()Lmaps/ac/cf;

    move-result-object v0

    iget-object v1, p0, Lmaps/y/d;->b:Lmaps/ao/a;

    invoke-virtual {v1}, Lmaps/ao/a;->a()Lmaps/ac/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/ac/cf;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lmaps/y/d;->k:J

    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    iput-wide v0, p0, Lmaps/y/d;->k:J

    invoke-virtual {v3}, Lmaps/ac/cw;->c()Lmaps/ac/be;

    move-result-object v0

    check-cast v0, Lmaps/ac/t;

    invoke-virtual {v0}, Lmaps/ac/t;->d()Lmaps/ac/av;

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ac/t;->c()Lmaps/ac/av;

    move-result-object v0

    invoke-virtual {v1, v0}, Lmaps/ac/av;->c(Lmaps/ac/av;)F

    move-result v0

    invoke-virtual {p1}, Lmaps/ar/a;->i()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lmaps/ar/a;->c(FF)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lmaps/y/d;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lmaps/y/d;->g:Lmaps/ar/a;

    invoke-virtual {p1}, Lmaps/ar/a;->y()Lmaps/ac/cw;

    move-result-object v1

    iput-object v1, p0, Lmaps/y/d;->h:Lmaps/ac/cw;

    invoke-virtual {p1}, Lmaps/ar/a;->o()F

    move-result v1

    const v4, 0x3c8efa35

    mul-float/2addr v1, v4

    invoke-static {v1}, Landroid/util/FloatMath;->cos(F)F

    move-result v1

    iput v1, p0, Lmaps/y/d;->i:F

    invoke-virtual {v3}, Lmaps/ac/cw;->a()Lmaps/ac/cx;

    move-result-object v1

    iget-object v4, p0, Lmaps/y/d;->b:Lmaps/ao/a;

    invoke-virtual {v4}, Lmaps/ao/a;->a()Lmaps/ac/cf;

    move-result-object v4

    invoke-static {v1, v0, v4}, Lmaps/ac/bt;->a(Lmaps/ac/cx;ILmaps/ac/cf;)Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/ac/bt;

    invoke-virtual {p1}, Lmaps/ar/a;->f()Lmaps/ac/av;

    move-result-object v5

    invoke-direct {p0, v0, v5, v2}, Lmaps/y/d;->a(Lmaps/ac/bt;Lmaps/ac/av;Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    iput-object v3, p0, Lmaps/y/d;->d:Lmaps/ac/cw;

    iget-object v0, p0, Lmaps/y/d;->e:Ljava/util/List;

    goto :goto_1
.end method
