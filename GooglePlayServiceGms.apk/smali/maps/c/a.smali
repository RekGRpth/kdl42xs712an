.class public final Lmaps/c/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lmaps/bq/a;

.field private final b:Landroid/view/View;


# direct methods
.method private constructor <init>(Lmaps/bq/a;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/c/a;->a:Lmaps/bq/a;

    iput-object p2, p0, Lmaps/c/a;->b:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lmaps/c/a;)Lmaps/bq/a;
    .locals 1

    iget-object v0, p0, Lmaps/c/a;->a:Lmaps/bq/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/res/Resources;)Lmaps/c/a;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lmaps/bq/a;

    invoke-direct {v0, p0, p1}, Lmaps/bq/a;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    sget v2, Lmaps/b/d;->c:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Lmaps/bq/a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v1, Lmaps/b/e;->Q:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/bq/a;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Lmaps/bq/a;->setCacheColorHint(I)V

    invoke-virtual {v0, v5}, Lmaps/bq/a;->setChoiceMode(I)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lmaps/bq/a;->setDivider(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Lmaps/bq/a;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {v0, v5}, Lmaps/bq/a;->setScrollingCacheEnabled(Z)V

    invoke-virtual {v0, v5}, Lmaps/bq/a;->setSmoothScrollbarEnabled(Z)V

    invoke-virtual {v0, v6}, Lmaps/bq/a;->setVisibility(I)V

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    new-instance v2, Lmaps/c/a;

    invoke-direct {v2, v0, v1}, Lmaps/c/a;-><init>(Lmaps/bq/a;Landroid/view/View;)V

    iget-object v0, v2, Lmaps/c/a;->a:Lmaps/bq/a;

    new-instance v1, Lmaps/c/b;

    invoke-direct {v1, v2}, Lmaps/c/b;-><init>(Lmaps/c/a;)V

    invoke-virtual {v0, v1}, Lmaps/bq/a;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v2
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmaps/c/a;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lmaps/c/a;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final a(Lmaps/c/c;)V
    .locals 2

    iget-object v1, p0, Lmaps/c/a;->a:Lmaps/bq/a;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lmaps/bq/a;->b(Lmaps/ab/q;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lmaps/c/c;->e()Lmaps/ab/q;

    move-result-object v0

    goto :goto_0
.end method
