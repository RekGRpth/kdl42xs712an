.class public final Lmaps/al/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Lmaps/as/b;

.field private final b:Lmaps/at/i;

.field private final c:Lmaps/al/b;

.field private final d:Lmaps/al/a;


# direct methods
.method public constructor <init>(ILmaps/al/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lmaps/at/k;->a(I)Lmaps/at/k;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    new-instance v0, Lmaps/al/b;

    invoke-direct {v0}, Lmaps/al/b;-><init>()V

    iput-object v0, p0, Lmaps/al/c;->c:Lmaps/al/b;

    iput-object p2, p0, Lmaps/al/c;->d:Lmaps/al/a;

    return-void
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->i()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    invoke-virtual {v0}, Lmaps/at/i;->b()I

    move-result v0

    return v0
.end method

.method public final a(II)V
    .locals 3

    if-lez p2, :cond_0

    iget-object v0, p0, Lmaps/al/c;->d:Lmaps/al/a;

    iget-object v1, p0, Lmaps/al/c;->c:Lmaps/al/b;

    invoke-virtual {v0, p1, v1}, Lmaps/al/a;->a(ILmaps/al/b;)V

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    iget-object v1, p0, Lmaps/al/c;->c:Lmaps/al/b;

    iget v1, v1, Lmaps/al/b;->a:I

    iget-object v2, p0, Lmaps/al/c;->c:Lmaps/al/b;

    iget v2, v2, Lmaps/al/b;->b:I

    invoke-virtual {v0, v1, v2, p2}, Lmaps/at/i;->a(III)V

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->d()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lmaps/al/c;->c()V

    :cond_0
    iget-object v0, p0, Lmaps/al/c;->d:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->b(Lmaps/as/a;)V

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/al/c;->d:Lmaps/al/a;

    invoke-virtual {v0, p1}, Lmaps/al/a;->a(Lmaps/as/a;)Lmaps/as/b;

    move-result-object v0

    iput-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    invoke-virtual {v0}, Lmaps/as/b;->h()V

    :cond_1
    iget-object v0, p0, Lmaps/al/c;->a:Lmaps/as/b;

    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmaps/as/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->d(Lmaps/as/a;)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    invoke-virtual {v0}, Lmaps/at/i;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public final b(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->b(Lmaps/as/a;)V

    invoke-direct {p0}, Lmaps/al/c;->c()V

    return-void
.end method

.method public final c(Lmaps/as/a;)V
    .locals 1

    iget-object v0, p0, Lmaps/al/c;->b:Lmaps/at/i;

    invoke-virtual {v0, p1}, Lmaps/at/i;->c(Lmaps/as/a;)V

    invoke-direct {p0}, Lmaps/al/c;->c()V

    return-void
.end method
