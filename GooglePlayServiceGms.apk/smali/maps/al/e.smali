.class public final Lmaps/al/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/microedition/khronos/opengles/GL11;


# static fields
.field private static final g:Z


# instance fields
.field private a:Lmaps/al/g;

.field private b:Lmaps/al/g;

.field private c:Lmaps/al/g;

.field private d:Lmaps/al/g;

.field private e:Lmaps/al/f;

.field private f:I

.field private h:I

.field private i:I

.field private j:I

.field private k:[F

.field private l:Z

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/al/e;->g:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmaps/al/f;

    invoke-direct {v0}, Lmaps/al/f;-><init>()V

    iput-object v0, p0, Lmaps/al/e;->e:Lmaps/al/f;

    iput v1, p0, Lmaps/al/e;->f:I

    iput v1, p0, Lmaps/al/e;->h:I

    const/16 v0, 0x2100

    iput v0, p0, Lmaps/al/e;->i:I

    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lmaps/al/e;->k:[F

    iput-boolean v1, p0, Lmaps/al/e;->l:Z

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "GL20 class is not ready for production use"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b()V
    .locals 7

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lmaps/al/e;->m:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    invoke-static {}, Lmaps/an/m;->c()V

    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget v0, p0, Lmaps/al/e;->n:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {v3}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    invoke-static {v2}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    :goto_1
    invoke-static {}, Lmaps/an/m;->c()V

    iget v0, p0, Lmaps/al/e;->j:I

    iget v2, p0, Lmaps/al/e;->i:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    iget v0, p0, Lmaps/al/e;->p:I

    iget-object v2, p0, Lmaps/al/e;->c:Lmaps/al/g;

    invoke-virtual {v2}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v2

    invoke-static {v2}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v2

    invoke-static {v0, v6, v1, v2, v1}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lmaps/an/m;->c()V

    iget v2, p0, Lmaps/al/e;->q:I

    iget-boolean v0, p0, Lmaps/al/e;->l:Z

    if-eqz v0, :cond_2

    move v0, v6

    :goto_2
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glUniform1i(II)V

    invoke-static {}, Lmaps/an/m;->c()V

    iget-object v0, p0, Lmaps/al/e;->e:Lmaps/al/f;

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    iget-object v2, p0, Lmaps/al/e;->b:Lmaps/al/g;

    invoke-virtual {v2}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v2

    invoke-static {v2}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v2

    iget-object v3, p0, Lmaps/al/e;->a:Lmaps/al/g;

    invoke-virtual {v3}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v3

    invoke-static {v3}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lmaps/al/e;->o:I

    iget-object v2, p0, Lmaps/al/e;->e:Lmaps/al/f;

    invoke-static {v2}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v2

    invoke-static {v0, v6, v1, v2, v1}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {v3}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    goto :goto_0

    :cond_1
    invoke-static {v2}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final glActiveTexture(I)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glAlphaFunc(IF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glAlphaFuncx(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glBindBuffer(II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glBindBuffer(II)V

    goto :goto_0
.end method

.method public final glBindTexture(II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBindTexture(II)V

    goto :goto_0
.end method

.method public final glBlendFunc(II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glBlendFunc(II)V

    goto :goto_0
.end method

.method public final glBufferData(IILjava/nio/Buffer;I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferData(IILjava/nio/Buffer;I)V

    goto :goto_0
.end method

.method public final glBufferSubData(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glBufferSubData(IIILjava/nio/Buffer;)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glBufferSubData(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public final glClear(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glClear(I)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glClear(I)V

    goto :goto_0
.end method

.method public final glClearColor(FFFF)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColor(FFFF)V

    invoke-static {}, Lmaps/an/m;->c()V

    goto :goto_0
.end method

.method public final glClearColorx(IIII)V
    .locals 5

    const/high16 v4, 0x43800000    # 256.0f

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Draw Count "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lmaps/al/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lmaps/al/e;->f:I

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    int-to-float v0, p1

    div-float/2addr v0, v4

    int-to-float v1, p2

    div-float/2addr v1, v4

    int-to-float v2, p3

    div-float/2addr v2, v4

    int-to-float v3, p4

    div-float/2addr v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glClearColorx(IIII)V

    invoke-static {}, Lmaps/an/m;->c()V

    goto :goto_0
.end method

.method public final glClearDepthf(F)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClearDepthx(I)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClearStencil(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glClearStencil(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glClearStencil(I)V

    goto :goto_0
.end method

.method public final glClientActiveTexture(I)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClipPlanef(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glClipPlanex(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glColor4f(FFFF)V
    .locals 2

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4f(FFFF)V

    goto :goto_0
.end method

.method public final glColor4ub(BBBB)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glColor4x(IIII)V
    .locals 4

    const/high16 v3, 0x10000

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x0

    div-int v2, p1, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x1

    div-int v2, p2, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x2

    div-int v2, p3, v3

    int-to-float v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lmaps/al/e;->k:[F

    const/4 v1, 0x3

    div-int v2, p4, v3

    int-to-float v2, v2

    aput v2, v0, v1

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColor4x(IIII)V

    goto :goto_0
.end method

.method public final glColorMask(ZZZZ)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glColorPointer(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glColorPointer(IIII)V

    goto :goto_0
.end method

.method public final glColorPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glColorPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public final glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glCompressedTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glCopyTexImage2D(IIIIIIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glCopyTexSubImage2D(IIIIIIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glCullFace(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glCullFace(I)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glCullFace(I)V

    goto :goto_0
.end method

.method public final glDeleteBuffers(ILjava/nio/IntBuffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glDeleteBuffers(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public final glDeleteBuffers(I[II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glDeleteBuffers(I[II)V

    goto :goto_0
.end method

.method public final glDeleteTextures(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glDeleteTextures(I[II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDeleteTextures(I[II)V

    goto :goto_0
.end method

.method public final glDepthFunc(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glDepthFunc(I)V

    goto :goto_0
.end method

.method public final glDepthMask(Z)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glDepthRangef(FF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glDepthRangex(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glDisable(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xde1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmaps/al/e;->l:Z

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glDisable(I)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glDisable(I)V

    goto :goto_0
.end method

.method public final glDisableClientState(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :pswitch_1
    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glDisableClientState(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8074
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final glDrawArrays(III)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/al/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/e;->f:I

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lmaps/al/e;->b()V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->b:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->a:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glDrawArrays(III)V

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public final glDrawElements(IIII)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/al/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/e;->f:I

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/al/e;->b()V

    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    :cond_0
    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_1
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->b:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->a:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glDrawElements(IIII)V

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public final glDrawElements(IIILjava/nio/Buffer;)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lmaps/al/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/e;->f:I

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lmaps/al/e;->b()V

    iget v0, p0, Lmaps/al/e;->h:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    :cond_0
    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_1
    const/16 v0, 0x1701

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->b:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    const/16 v0, 0x1700

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    iget-object v0, p0, Lmaps/al/e;->a:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-static {v0}, Lmaps/al/f;->b(Lmaps/al/f;)[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glDrawElements(IIILjava/nio/Buffer;)V

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->d()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public final glEnable(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_1

    const/16 v0, 0xde1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/al/e;->l:Z

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES20;->glEnable(I)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/opengl/GLES10;->glEnable(I)V

    goto :goto_0
.end method

.method public final glEnableClientState(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :pswitch_1
    iget v0, p0, Lmaps/al/e;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lmaps/al/e;->h:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lmaps/al/e;->h:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lmaps/al/e;->h:I

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glEnableClientState(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x8074
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final glFinish()V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFlush()V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogf(IF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogfv(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogx(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFogxv(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFrontFace(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glFrontFace(I)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glFrontFace(I)V

    goto :goto_0
.end method

.method public final glFrustumf(FFFFFF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glFrustumx(IIIIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGenBuffers(ILjava/nio/IntBuffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGenBuffers(ILjava/nio/IntBuffer;)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES11;->glGenBuffers(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public final glGenBuffers(I[II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES11;->glGenBuffers(I[II)V

    goto :goto_0
.end method

.method public final glGenTextures(ILjava/nio/IntBuffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glGenTextures(ILjava/nio/IntBuffer;)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glGenTextures(ILjava/nio/IntBuffer;)V

    goto :goto_0
.end method

.method public final glGenTextures(I[II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGenTextures(I[II)V

    goto :goto_0
.end method

.method public final glGetBooleanv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetBooleanv(I[ZI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetBufferParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetBufferParameteriv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetClipPlanef(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetClipPlanef(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetClipPlanex(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetClipPlanex(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetError()I
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/opengl/GLES10;->glGetError()I

    move-result v0

    goto :goto_0
.end method

.method public final glGetFixedv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetFixedv(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetFloatv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetFloatv(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetIntegerv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetIntegerv(I[II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glGetIntegerv(I[II)V

    goto :goto_0
.end method

.method public final glGetLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetLightfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetLightxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetMaterialfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetMaterialxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetPointerv(I[Ljava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetString(I)Ljava/lang/String;
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final glGetTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexEnviv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexEnvxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameterfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameteriv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glGetTexParameterxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glHint(II)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glHint(II)V

    :cond_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void
.end method

.method public final glIsBuffer(I)Z
    .locals 1

    invoke-static {}, Lmaps/al/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public final glIsEnabled(I)Z
    .locals 1

    invoke-static {}, Lmaps/al/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public final glIsTexture(I)Z
    .locals 1

    invoke-static {}, Lmaps/al/e;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method public final glLightModelf(IF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightModelfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightModelfv(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightModelx(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightModelxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightModelxv(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightf(IIF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightx(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLightxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLineWidth(F)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidth(F)V

    :cond_0
    return-void
.end method

.method public final glLineWidthx(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glLineWidthx(I)V

    :cond_0
    return-void
.end method

.method public final glLoadIdentity()V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/al/f;->a()Lmaps/al/f;

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glLoadIdentity()V

    :cond_0
    return-void
.end method

.method public final glLoadMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLoadMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/al/f;->b([FI)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glLoadMatrixf([FI)V

    :cond_0
    return-void
.end method

.method public final glLoadMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLoadMatrixx([II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glLogicOp(I)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialf(IIF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialx(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMaterialxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMatrixMode(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unexpected value"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lmaps/al/e;->a:Lmaps/al/g;

    iput-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    :goto_0
    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glMatrixMode(I)V

    :cond_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lmaps/al/e;->b:Lmaps/al/g;

    iput-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lmaps/al/e;->c:Lmaps/al/g;

    iput-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1700
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final glMultMatrixf(Ljava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMultMatrixf([FI)V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmaps/al/f;->a([FI)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glMultMatrixf([FI)V

    :cond_0
    return-void
.end method

.method public final glMultMatrixx(Ljava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMultMatrixx([II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMultiTexCoord4f(IFFFF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glMultiTexCoord4x(IIIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glNormal3f(FFF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glNormal3x(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glNormalPointer(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glNormalPointer(IILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glOrthof(FFFFFF)V
    .locals 8

    const/4 v1, 0x0

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x10

    new-array v0, v0, [F

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    iget-object v2, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v2}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lmaps/al/f;->a([FI)V

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p6}, Landroid/opengl/GLES10;->glOrthof(FFFFFF)V

    goto :goto_0
.end method

.method public final glOrthox(IIIIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPixelStorei(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterf(IF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterfv(ILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterfv(I[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterx(II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterxv(ILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointParameterxv(I[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointSize(F)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glPointSize(F)V

    :cond_0
    return-void
.end method

.method public final glPointSizePointerOES(IILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPointSizex(I)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glPolygonOffset(FF)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    goto :goto_0
.end method

.method public final glPolygonOffsetx(II)V
    .locals 2

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    :goto_0
    return-void

    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    invoke-static {v0, v1}, Landroid/opengl/GLES10;->glPolygonOffset(FF)V

    goto :goto_0
.end method

.method public final glPopMatrix()V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->b()Lmaps/al/f;

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glPopMatrix()V

    :cond_0
    return-void
.end method

.method public final glPushMatrix()V
    .locals 2

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    iget-object v1, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v1}, Lmaps/al/g;->a()Lmaps/al/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmaps/al/f;->a(Lmaps/al/f;)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/opengl/GLES10;->glPushMatrix()V

    :cond_0
    return-void
.end method

.method public final glReadPixels(IIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glRotatef(FFFF)V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/al/f;->a(FFFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatef(FFFF)V

    :cond_0
    return-void
.end method

.method public final glRotatex(IIII)V
    .locals 5

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    int-to-float v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Lmaps/al/f;->a(FFFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glRotatex(IIII)V

    :cond_0
    return-void
.end method

.method public final glSampleCoverage(FZ)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glSampleCoveragex(IZ)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glScalef(FFF)V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmaps/al/f;->b(FFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalef(FFF)V

    :cond_0
    return-void
.end method

.method public final glScalex(III)V
    .locals 4

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/al/f;->b(FFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glScalex(III)V

    :cond_0
    return-void
.end method

.method public final glScissor(IIII)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glShadeModel(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES10;->glShadeModel(I)V

    :cond_0
    return-void
.end method

.method public final glStencilFunc(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glStencilMask(I)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/opengl/GLES20;->glStencilMask(I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/opengl/GLES10;->glStencilMask(I)V

    goto :goto_0
.end method

.method public final glStencilOp(III)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glStencilOp(III)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glStencilOp(III)V

    goto :goto_0
.end method

.method public final glTexCoordPointer(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glTexCoordPointer(IIII)V

    goto :goto_0
.end method

.method public final glTexCoordPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glTexCoordPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public final glTexEnvf(IIF)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnvfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnvfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnvi(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnviv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnviv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnvx(III)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x2300

    if-ne p1, v0, :cond_1

    const/16 v0, 0x2200

    if-ne p2, v0, :cond_1

    const/16 v0, 0x2100

    if-eq p3, v0, :cond_0

    const/16 v0, 0x1e01

    if-eq p3, v0, :cond_0

    const/4 v0, -0x2

    if-ne p3, v0, :cond_1

    :cond_0
    iput p3, p0, Lmaps/al/e;->i:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexEnvx(III)V

    goto :goto_0
.end method

.method public final glTexEnvxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexEnvxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameterf(IIF)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_1

    const v0, 0x8191

    if-eq p2, v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    :cond_0
    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_1
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterf(IIF)V

    goto :goto_0
.end method

.method public final glTexParameterfv(IILjava/nio/FloatBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameterfv(II[FI)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameteri(III)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameteriv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameteriv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameterx(III)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTexParameterx(III)V

    goto :goto_0
.end method

.method public final glTexParameterxv(IILjava/nio/IntBuffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexParameterxv(II[II)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTexSubImage2D(IIIIIIIILjava/nio/Buffer;)V
    .locals 0

    invoke-static {}, Lmaps/al/e;->a()V

    return-void
.end method

.method public final glTranslatef(FFF)V
    .locals 1

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lmaps/al/f;->a(FFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    :cond_0
    return-void
.end method

.method public final glTranslatex(III)V
    .locals 4

    iget-object v0, p0, Lmaps/al/e;->d:Lmaps/al/g;

    invoke-virtual {v0}, Lmaps/al/g;->c()Lmaps/al/f;

    move-result-object v0

    int-to-float v1, p1

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lmaps/al/f;->a(FFF)V

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-nez v0, :cond_0

    int-to-float v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES10;->glTranslatef(FFF)V

    :cond_0
    return-void
.end method

.method public final glVertexPointer(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES11;->glVertexPointer(IIII)V

    goto :goto_0
.end method

.method public final glVertexPointer(IIILjava/nio/Buffer;)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lmaps/al/e;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glVertexPointer(IIILjava/nio/Buffer;)V

    goto :goto_0
.end method

.method public final glViewport(IIII)V
    .locals 1

    sget-boolean v0, Lmaps/al/e;->g:Z

    if-eqz v0, :cond_0

    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    :goto_0
    invoke-static {}, Lmaps/an/m;->c()V

    return-void

    :cond_0
    invoke-static {p1, p2, p3, p4}, Landroid/opengl/GLES10;->glViewport(IIII)V

    goto :goto_0
.end method
