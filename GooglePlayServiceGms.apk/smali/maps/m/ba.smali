.class public final Lmaps/m/ba;
.super Lmaps/m/aq;


# instance fields
.field private a:[Ljava/lang/Object;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmaps/m/ba;-><init>(B)V

    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    invoke-direct {p0}, Lmaps/m/aq;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lmaps/m/ba;->b:I

    return-void
.end method

.method private a(I)Lmaps/m/ba;
    .locals 2

    iget-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    iget-object v1, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    array-length v1, v1

    invoke-static {v1, p1}, Lmaps/m/ba;->a(II)I

    move-result v1

    invoke-static {v0, v1}, Lmaps/m/cx;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Iterable;)Lmaps/m/aq;
    .locals 2

    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    iget v1, p0, Lmaps/m/ba;->b:I

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lmaps/m/ba;->a(I)Lmaps/m/ba;

    :cond_0
    invoke-super {p0, p1}, Lmaps/m/aq;->a(Ljava/lang/Iterable;)Lmaps/m/aq;

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;)Lmaps/m/aq;
    .locals 1

    invoke-virtual {p0, p1}, Lmaps/m/ba;->b(Ljava/lang/Object;)Lmaps/m/ba;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/util/Iterator;)Lmaps/m/aq;
    .locals 0

    invoke-super {p0, p1}, Lmaps/m/aq;->a(Ljava/util/Iterator;)Lmaps/m/aq;

    return-object p0
.end method

.method public final a()Lmaps/m/ay;
    .locals 3

    iget v0, p0, Lmaps/m/ba;->b:I

    packed-switch v0, :pswitch_data_0

    iget v0, p0, Lmaps/m/ba;->b:I

    iget-object v1, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    new-instance v0, Lmaps/m/da;

    iget-object v1, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    invoke-direct {v0, v1}, Lmaps/m/da;-><init>([Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Lmaps/m/ay;->a(Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v0, Lmaps/m/da;

    iget-object v1, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    iget v2, p0, Lmaps/m/ba;->b:I

    invoke-static {v1, v2}, Lmaps/m/cx;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lmaps/m/da;-><init>([Ljava/lang/Object;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;)Lmaps/m/ba;
    .locals 3

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lmaps/m/ba;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lmaps/m/ba;->a(I)Lmaps/m/ba;

    iget-object v0, p0, Lmaps/m/ba;->a:[Ljava/lang/Object;

    iget v1, p0, Lmaps/m/ba;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lmaps/m/ba;->b:I

    aput-object p1, v0, v1

    return-object p0
.end method
