.class final Lmaps/m/l;
.super Lmaps/m/f;

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field private c:Ljava/util/SortedSet;

.field private synthetic d:Lmaps/m/e;


# direct methods
.method constructor <init>(Lmaps/m/e;Ljava/util/SortedMap;)V
    .locals 0

    iput-object p1, p0, Lmaps/m/l;->d:Lmaps/m/e;

    invoke-direct {p0, p1, p2}, Lmaps/m/f;-><init>(Lmaps/m/e;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final firstKey()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    new-instance v1, Lmaps/m/l;

    iget-object v2, p0, Lmaps/m/l;->d:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/l;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final synthetic keySet()Ljava/util/Set;
    .locals 3

    iget-object v0, p0, Lmaps/m/l;->c:Ljava/util/SortedSet;

    if-nez v0, :cond_0

    new-instance v1, Lmaps/m/m;

    iget-object v2, p0, Lmaps/m/l;->d:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-direct {v1, v2, v0}, Lmaps/m/m;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    iput-object v1, p0, Lmaps/m/l;->c:Ljava/util/SortedSet;

    move-object v0, v1

    :cond_0
    return-object v0
.end method

.method public final lastKey()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    new-instance v1, Lmaps/m/l;

    iget-object v2, p0, Lmaps/m/l;->d:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/l;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .locals 3

    new-instance v1, Lmaps/m/l;

    iget-object v2, p0, Lmaps/m/l;->d:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/l;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/l;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method
