.class final Lmaps/m/at;
.super Lmaps/m/be;


# instance fields
.field private final transient a:Ljava/util/EnumMap;


# direct methods
.method private constructor <init>(Ljava/util/EnumMap;)V
    .locals 1

    invoke-direct {p0}, Lmaps/m/be;-><init>()V

    iput-object p1, p0, Lmaps/m/at;->a:Ljava/util/EnumMap;

    invoke-virtual {p1}, Ljava/util/EnumMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lmaps/m/at;)Ljava/util/EnumMap;
    .locals 1

    iget-object v0, p0, Lmaps/m/at;->a:Ljava/util/EnumMap;

    return-object v0
.end method

.method static a(Ljava/util/EnumMap;)Lmaps/m/be;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/util/EnumMap;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lmaps/m/at;

    invoke-direct {v0, p0}, Lmaps/m/at;-><init>(Ljava/util/EnumMap;)V

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lmaps/m/ab;->a:Lmaps/m/ab;

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Ljava/util/EnumMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bz;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    const-string v0, "null key in entry: null=%s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v1, v0, v3}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "null value in entry: %s=null"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-static {v2, v0, v3}, Lmaps/k/o;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lmaps/m/dn;

    invoke-direct {v0, v1, v2}, Lmaps/m/dn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method final a()Lmaps/m/bo;
    .locals 1

    new-instance v0, Lmaps/m/au;

    invoke-direct {v0, p0}, Lmaps/m/au;-><init>(Lmaps/m/at;)V

    return-object v0
.end method

.method final c()Lmaps/m/bo;
    .locals 1

    new-instance v0, Lmaps/m/av;

    invoke-direct {v0, p0}, Lmaps/m/av;-><init>(Lmaps/m/at;)V

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/m/at;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/at;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lmaps/m/at;->a:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    return v0
.end method
