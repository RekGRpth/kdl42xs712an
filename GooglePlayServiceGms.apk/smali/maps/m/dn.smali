.class final Lmaps/m/dn;
.super Lmaps/m/ao;


# instance fields
.field private transient a:Ljava/lang/Object;

.field private transient b:Ljava/lang/Object;

.field private transient c:Lmaps/m/ao;


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/ao;-><init>()V

    iput-object p1, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    iput-object p2, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/m/ao;)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/ao;-><init>()V

    iput-object p1, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    iput-object p2, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    iput-object p3, p0, Lmaps/m/dn;->c:Lmaps/m/ao;

    return-void
.end method

.method constructor <init>(Ljava/util/Map$Entry;)V
    .locals 2

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmaps/m/dn;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method final a()Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final av_()Lmaps/m/ao;
    .locals 3

    iget-object v0, p0, Lmaps/m/dn;->c:Lmaps/m/ao;

    if-nez v0, :cond_0

    new-instance v0, Lmaps/m/dn;

    iget-object v1, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    iget-object v2, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2, p0}, Lmaps/m/dn;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lmaps/m/ao;)V

    iput-object v0, p0, Lmaps/m/dn;->c:Lmaps/m/ao;

    :cond_0
    return-object v0
.end method

.method final c()Lmaps/m/bo;
    .locals 2

    iget-object v0, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    iget-object v1, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Lmaps/m/co;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final containsKey(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final containsValue(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/dn;->a:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/m/dn;->b:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
