.class final Lmaps/m/m;
.super Lmaps/m/i;

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field private synthetic c:Lmaps/m/e;


# direct methods
.method constructor <init>(Lmaps/m/e;Ljava/util/SortedMap;)V
    .locals 0

    iput-object p1, p0, Lmaps/m/m;->c:Lmaps/m/e;

    invoke-direct {p0, p1, p2}, Lmaps/m/i;-><init>(Lmaps/m/e;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final first()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v1, Lmaps/m/m;

    iget-object v2, p0, Lmaps/m/m;->c:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/m;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final last()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v1, Lmaps/m/m;

    iget-object v2, p0, Lmaps/m/m;->c:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/m;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method

.method public final tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 3

    new-instance v1, Lmaps/m/m;

    iget-object v2, p0, Lmaps/m/m;->c:Lmaps/m/e;

    iget-object v0, p0, Lmaps/m/m;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    invoke-interface {v0, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lmaps/m/m;-><init>(Lmaps/m/e;Ljava/util/SortedMap;)V

    return-object v1
.end method
