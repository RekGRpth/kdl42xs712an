.class public Lmaps/m/br;
.super Lmaps/m/bl;

# interfaces
.implements Lmaps/m/cu;


# instance fields
.field private final transient a:Lmaps/m/bx;


# direct methods
.method constructor <init>(Lmaps/m/be;ILjava/util/Comparator;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmaps/m/bl;-><init>(Lmaps/m/be;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/m/br;->a:Lmaps/m/bx;

    return-void
.end method

.method static synthetic a(Lmaps/m/cu;)Lmaps/m/br;
    .locals 6

    invoke-static {p0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lmaps/m/cu;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/m/ae;->a:Lmaps/m/ae;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v0, p0, Lmaps/m/br;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lmaps/m/br;

    iget-object v1, v0, Lmaps/m/bl;->b:Lmaps/m/be;

    invoke-virtual {v1}, Lmaps/m/be;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {}, Lmaps/m/be;->h()Lmaps/m/bf;

    move-result-object v2

    const/4 v0, 0x0

    invoke-interface {p0}, Lmaps/m/cu;->c()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0}, Lmaps/m/bo;->a(Ljava/util/Collection;)Lmaps/m/bo;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bo;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v2, v4, v0}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    invoke-virtual {v0}, Lmaps/m/bo;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_3
    new-instance v0, Lmaps/m/br;

    invoke-virtual {v2}, Lmaps/m/bf;->a()Lmaps/m/be;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lmaps/m/br;-><init>(Lmaps/m/be;ILjava/util/Comparator;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private c(Ljava/lang/Object;)Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/m/br;->b:Lmaps/m/be;

    invoke-virtual {v0, p1}, Lmaps/m/be;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/m/bo;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/m/br;->a:Lmaps/m/bx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmaps/m/br;->a:Lmaps/m/bx;

    goto :goto_0

    :cond_1
    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    goto :goto_0
.end method

.method public static f()Lmaps/m/br;
    .locals 1

    sget-object v0, Lmaps/m/ae;->a:Lmaps/m/ae;

    return-object v0
.end method

.method public static g()Lmaps/m/bs;
    .locals 1

    new-instance v0, Lmaps/m/bs;

    invoke-direct {v0}, Lmaps/m/bs;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Lmaps/m/ap;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/m/br;->c(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 1

    invoke-direct {p0, p1}, Lmaps/m/br;->c(Ljava/lang/Object;)Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method
