.class final Lmaps/m/af;
.super Lmaps/m/bv;


# instance fields
.field private final transient a:Lmaps/m/bx;


# direct methods
.method constructor <init>(Ljava/util/Comparator;)V
    .locals 1

    invoke-direct {p0}, Lmaps/m/bv;-><init>()V

    invoke-static {p1}, Lmaps/m/bx;->a(Ljava/util/Comparator;)Lmaps/m/bx;

    move-result-object v0

    iput-object v0, p0, Lmaps/m/af;->a:Lmaps/m/bx;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lmaps/m/bv;
    .locals 0

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final ax_()Lmaps/m/bx;
    .locals 1

    iget-object v0, p0, Lmaps/m/af;->a:Lmaps/m/bx;

    return-object v0
.end method

.method public final b()Lmaps/m/bo;
    .locals 1

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lmaps/m/bv;
    .locals 0

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method final c()Lmaps/m/bo;
    .locals 2

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should never be called"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public final bridge synthetic d()Lmaps/m/bo;
    .locals 1

    iget-object v0, p0, Lmaps/m/af;->a:Lmaps/m/bx;

    return-object v0
.end method

.method final e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic entrySet()Ljava/util/Set;
    .locals 1

    invoke-static {}, Lmaps/m/bo;->j()Lmaps/m/bo;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lmaps/m/ap;
    .locals 1

    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic keySet()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lmaps/m/af;->a:Lmaps/m/bx;

    return-object v0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    const-string v0, "{}"

    return-object v0
.end method

.method public final synthetic values()Ljava/util/Collection;
    .locals 1

    invoke-static {}, Lmaps/m/ay;->e()Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method
