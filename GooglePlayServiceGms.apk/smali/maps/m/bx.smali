.class public abstract Lmaps/m/bx;
.super Lmaps/m/by;

# interfaces
.implements Ljava/util/SortedSet;
.implements Lmaps/m/dq;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final c:Lmaps/m/bx;


# instance fields
.field final transient a:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lmaps/m/cy;->b()Lmaps/m/cy;

    move-result-object v0

    sput-object v0, Lmaps/m/bx;->b:Ljava/util/Comparator;

    new-instance v0, Lmaps/m/ag;

    sget-object v1, Lmaps/m/bx;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Lmaps/m/ag;-><init>(Ljava/util/Comparator;)V

    sput-object v0, Lmaps/m/bx;->c:Lmaps/m/bx;

    return-void
.end method

.method constructor <init>(Ljava/util/Comparator;)V
    .locals 0

    invoke-direct {p0}, Lmaps/m/by;-><init>()V

    iput-object p1, p0, Lmaps/m/bx;->a:Ljava/util/Comparator;

    return-void
.end method

.method static a(Ljava/util/Comparator;)Lmaps/m/bx;
    .locals 1

    sget-object v0, Lmaps/m/bx;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lmaps/m/bx;->c:Lmaps/m/bx;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lmaps/m/ag;

    invoke-direct {v0, p0}, Lmaps/m/ag;-><init>(Ljava/util/Comparator;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract b()Lmaps/m/ee;
.end method

.method public comparator()Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lmaps/m/bx;->a:Ljava/util/Comparator;

    return-object v0
.end method

.method public abstract e()Lmaps/m/ee;
.end method

.method abstract f()Lmaps/m/bx;
.end method

.method public first()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/bx;->b()Lmaps/m/ee;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ee;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method abstract g()Lmaps/m/bx;
.end method

.method abstract h()Lmaps/m/bx;
.end method

.method public synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lmaps/m/bx;->f()Lmaps/m/bx;

    move-result-object v0

    return-object v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/bx;->b()Lmaps/m/ee;

    move-result-object v0

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/m/bx;->e()Lmaps/m/ee;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/ee;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lmaps/m/bx;->a:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lmaps/k/o;->a(Z)V

    invoke-virtual {p0}, Lmaps/m/bx;->g()Lmaps/m/bx;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .locals 1

    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lmaps/m/bx;->h()Lmaps/m/bx;

    move-result-object v0

    return-object v0
.end method
