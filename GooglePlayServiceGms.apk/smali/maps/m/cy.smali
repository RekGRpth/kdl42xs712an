.class public abstract Lmaps/m/cy;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()Lmaps/m/cy;
    .locals 1

    sget-object v0, Lmaps/m/cw;->a:Lmaps/m/cw;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lmaps/m/ay;
    .locals 4

    invoke-static {p1}, Lmaps/m/bz;->b(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-static {v3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v0, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-static {v0}, Lmaps/m/ay;->b([Ljava/lang/Object;)Lmaps/m/ay;

    move-result-object v0

    return-object v0
.end method

.method public a()Lmaps/m/cy;
    .locals 1

    new-instance v0, Lmaps/m/dj;

    invoke-direct {v0, p0}, Lmaps/m/dj;-><init>(Lmaps/m/cy;)V

    return-object v0
.end method

.method public final a(Lmaps/k/e;)Lmaps/m/cy;
    .locals 1

    new-instance v0, Lmaps/m/y;

    invoke-direct {v0, p1, p0}, Lmaps/m/y;-><init>(Lmaps/k/e;Lmaps/m/cy;)V

    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
.end method
