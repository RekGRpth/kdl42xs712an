.class public final Lmaps/be/g;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Z

.field private static volatile b:Lmaps/bv/a;

.field private static volatile c:Lmaps/be/c;

.field private static volatile d:Lmaps/be/k;

.field private static volatile e:Lmaps/be/f;

.field private static volatile f:Lmaps/be/l;

.field private static volatile g:Lmaps/be/e;

.field private static volatile h:Lmaps/be/a;

.field private static volatile i:Lmaps/be/b;

.field private static volatile j:Lmaps/be/d;

.field private static volatile k:Lmaps/by/d;

.field private static volatile l:Z

.field private static volatile m:Z

.field private static n:Ljava/lang/Object;

.field private static o:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/be/g;->a:Z

    const/4 v0, 0x0

    sput-object v0, Lmaps/be/g;->k:Lmaps/by/d;

    sput-boolean v1, Lmaps/be/g;->l:Z

    sput-boolean v1, Lmaps/be/g;->m:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lmaps/be/g;->n:Ljava/lang/Object;

    invoke-static {}, Lmaps/m/be;->h()Lmaps/m/bf;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/m/bf;->a(Ljava/lang/Object;Ljava/lang/Object;)Lmaps/m/bf;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/m/bf;->a()Lmaps/m/be;

    move-result-object v0

    sput-object v0, Lmaps/be/g;->o:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()Lmaps/be/c;
    .locals 2

    const-class v0, Lmaps/be/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/be/g;->c:Lmaps/be/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(Lmaps/by/d;)Lmaps/by/d;
    .locals 0

    sput-object p0, Lmaps/be/g;->k:Lmaps/by/d;

    return-object p0
.end method

.method public static declared-synchronized a(Lmaps/bn/d;)V
    .locals 9

    const/4 v2, 0x0

    const-class v4, Lmaps/be/g;

    monitor-enter v4

    :try_start_0
    const-string v5, "ServerControlledParametersManager.data"

    sget-object v0, Lmaps/be/g;->b:Lmaps/bv/a;

    if-nez v0, :cond_4

    new-instance v6, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/f;->c:Lmaps/bv/c;

    invoke-direct {v6, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    sget-object v0, Lmaps/be/g;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v3, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-direct {v3, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v8, 0x1

    invoke-virtual {v3, v8, v1}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v6, v1, v0}, Lmaps/bv/a;->a(ILmaps/bv/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :pswitch_1
    :try_start_1
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->e:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_2
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->k:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_3
    invoke-static {}, Lmaps/be/f;->b()Lmaps/bv/a;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_4
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->m:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_5
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->n:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_6
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->f:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_7
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->g:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :pswitch_8
    new-instance v1, Lmaps/bv/a;

    sget-object v8, Lmaps/cm/f;->h:Lmaps/bv/c;

    invoke-direct {v1, v8}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v3, v0, v1}, Lmaps/bv/a;->b(ILmaps/bv/a;)Lmaps/bv/a;

    move-object v0, v3

    goto :goto_1

    :cond_1
    sput-object v6, Lmaps/be/g;->b:Lmaps/bv/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v0

    invoke-interface {v0, v5}, Lmaps/bt/k;->c(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v1, Lmaps/bv/a;

    sget-object v3, Lmaps/cm/f;->c:Lmaps/bv/c;

    invoke-direct {v1, v3}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-virtual {v1, v0}, Lmaps/bv/a;->a([B)Lmaps/bv/a;

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lmaps/bv/a;->j(I)I

    move-result v3

    move v0, v2

    :goto_2
    if-ge v0, v3, :cond_2

    const/4 v6, 0x1

    invoke-virtual {v1, v6, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v6

    invoke-static {v6}, Lmaps/be/g;->e(Lmaps/bv/a;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    :cond_2
    :try_start_3
    sget-object v0, Lmaps/be/g;->b:Lmaps/bv/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmaps/bv/a;->j(I)I

    move-result v1

    move v0, v2

    :goto_3
    if-ge v0, v1, :cond_3

    sget-object v2, Lmaps/be/g;->b:Lmaps/bv/a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v2

    invoke-static {v2}, Lmaps/be/g;->d(Lmaps/bv/a;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x1

    invoke-static {p0, v5, v0}, Lmaps/be/g;->b(Lmaps/bn/d;Ljava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    monitor-exit v4

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method static synthetic a(Lmaps/bn/d;Ljava/lang/String;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lmaps/be/g;->b(Lmaps/bn/d;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Lmaps/bv/a;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/f;->b:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->d()Z

    move-result v1

    invoke-virtual {v0, v2, v2}, Lmaps/bv/a;->a(IZ)Lmaps/bv/a;

    invoke-virtual {v0, v3, v1}, Lmaps/bv/a;->a(IZ)Lmaps/bv/a;

    invoke-virtual {p0, v3, v0}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    return-void
.end method

.method static a(Lmaps/bv/a;Ljava/lang/String;)Z
    .locals 2

    :try_start_0
    invoke-static {}, Lmaps/bf/a;->a()Lmaps/bf/a;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/bf/a;->q()Lmaps/bt/k;

    move-result-object v0

    invoke-virtual {p0}, Lmaps/bv/a;->c()[B

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmaps/bt/k;->a([BLjava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized b()Lmaps/be/f;
    .locals 2

    const-class v0, Lmaps/be/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/be/g;->e:Lmaps/be/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized b(Lmaps/bn/d;Ljava/lang/String;Z)V
    .locals 3

    const-class v1, Lmaps/be/g;

    monitor-enter v1

    if-nez p0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_0
    sget-object v2, Lmaps/be/g;->n:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lmaps/be/g;->k:Lmaps/by/d;

    if-eqz v0, :cond_1

    sget-object v0, Lmaps/be/g;->k:Lmaps/by/d;

    invoke-virtual {v0}, Lmaps/by/d;->b()I

    const/4 v0, 0x0

    sput-object v0, Lmaps/be/g;->k:Lmaps/by/d;

    :cond_1
    sget-boolean v0, Lmaps/be/g;->m:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    sput-boolean v0, Lmaps/be/g;->l:Z

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Lmaps/be/g;->m:Z

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/be/g;->l:Z

    new-instance v0, Lmaps/be/h;

    invoke-direct {v0, p1, p2}, Lmaps/be/h;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lmaps/bn/d;->c(Lmaps/bn/c;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method static synthetic b(Lmaps/bv/a;)Z
    .locals 1

    invoke-static {p0}, Lmaps/be/g;->d(Lmaps/bv/a;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized c()Lmaps/be/l;
    .locals 2

    const-class v0, Lmaps/be/g;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lmaps/be/g;->f:Lmaps/be/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic c(Lmaps/bv/a;)V
    .locals 0

    invoke-static {p0}, Lmaps/be/g;->e(Lmaps/bv/a;)V

    return-void
.end method

.method public static d()Lmaps/be/b;
    .locals 1

    sget-object v0, Lmaps/be/g;->i:Lmaps/be/b;

    return-object v0
.end method

.method private static d(Lmaps/bv/a;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    invoke-static {p0, v2, v0}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v3

    sget-object v0, Lmaps/be/g;->o:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lmaps/be/g;->o:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lmaps/bv/a;->i(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0, v0}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    sget-object v1, Lmaps/be/g;->c:Lmaps/be/c;

    if-eqz v1, :cond_1

    sget-object v1, Lmaps/be/g;->c:Lmaps/be/c;

    invoke-virtual {v1, v0}, Lmaps/be/c;->a(Lmaps/bv/a;)V

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_1
    new-instance v1, Lmaps/be/c;

    invoke-direct {v1, v0}, Lmaps/be/c;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->c:Lmaps/be/c;

    goto :goto_1

    :pswitch_2
    new-instance v1, Lmaps/be/k;

    invoke-direct {v1, v0}, Lmaps/be/k;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->d:Lmaps/be/k;

    invoke-static {}, Lmaps/ba/f;->a()Lmaps/ba/f;

    invoke-static {}, Lmaps/ba/f;->h()V

    move v0, v2

    goto :goto_0

    :pswitch_3
    new-instance v1, Lmaps/be/f;

    invoke-direct {v1, v0}, Lmaps/be/f;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->e:Lmaps/be/f;

    move v0, v2

    goto :goto_0

    :pswitch_4
    new-instance v1, Lmaps/be/l;

    invoke-direct {v1, v0}, Lmaps/be/l;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->f:Lmaps/be/l;

    move v0, v2

    goto :goto_0

    :pswitch_5
    new-instance v1, Lmaps/be/e;

    invoke-direct {v1, v0}, Lmaps/be/e;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->g:Lmaps/be/e;

    move v0, v2

    goto :goto_0

    :pswitch_6
    new-instance v1, Lmaps/be/a;

    invoke-direct {v1, v0}, Lmaps/be/a;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->h:Lmaps/be/a;

    move v0, v2

    goto :goto_0

    :pswitch_7
    new-instance v1, Lmaps/be/b;

    invoke-direct {v1, v0}, Lmaps/be/b;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->i:Lmaps/be/b;

    move v0, v2

    goto :goto_0

    :pswitch_8
    new-instance v1, Lmaps/be/d;

    invoke-direct {v1, v0}, Lmaps/be/d;-><init>(Lmaps/bv/a;)V

    sput-object v1, Lmaps/be/g;->j:Lmaps/be/d;

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method static synthetic e()Lmaps/bv/a;
    .locals 1

    sget-object v0, Lmaps/be/g;->b:Lmaps/bv/a;

    return-object v0
.end method

.method private static e(Lmaps/bv/a;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lmaps/bv/a;->d(I)I

    move-result v1

    sget-object v0, Lmaps/be/g;->o:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lmaps/be/g;->b:Lmaps/bv/a;

    invoke-virtual {v0, v4}, Lmaps/bv/a;->j(I)I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    sget-object v3, Lmaps/be/g;->b:Lmaps/bv/a;

    invoke-virtual {v3, v4, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v3

    invoke-virtual {v3, v4}, Lmaps/bv/a;->d(I)I

    move-result v3

    if-ne v1, v3, :cond_2

    sget-object v1, Lmaps/be/g;->b:Lmaps/bv/a;

    invoke-virtual {v1, v4, v0}, Lmaps/bv/a;->e(II)V

    :cond_1
    sget-object v0, Lmaps/be/g;->b:Lmaps/bv/a;

    invoke-virtual {v0, v4, p0}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic f()Lmaps/be/j;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic g()Z
    .locals 1

    sget-boolean v0, Lmaps/be/g;->a:Z

    return v0
.end method

.method static synthetic h()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lmaps/be/g;->n:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic i()Z
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lmaps/be/g;->m:Z

    return v0
.end method

.method static synthetic j()Z
    .locals 1

    sget-boolean v0, Lmaps/be/g;->l:Z

    return v0
.end method

.method static synthetic k()Lmaps/by/d;
    .locals 1

    sget-object v0, Lmaps/be/g;->k:Lmaps/by/d;

    return-object v0
.end method
