.class public final Lmaps/be/l;
.super Ljava/lang/Object;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:I

.field private final d:I

.field private final e:I

.field private f:I

.field private g:Z

.field private final h:Z

.field private final i:Z


# direct methods
.method public constructor <init>(Lmaps/bv/a;)V
    .locals 2

    const/16 v1, 0x9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/l;->a:I

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/l;->b:Z

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/l;->c:I

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/l;->d:I

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/l;->e:I

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/be/l;->f:I

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/l;->g:Z

    invoke-virtual {p1, v1}, Lmaps/bv/a;->h(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/l;->h:Z

    invoke-virtual {p1, v1}, Lmaps/bv/a;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Lmaps/be/l;->i:Z

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget v0, p0, Lmaps/be/l;->d:I

    int-to-long v0, v0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lmaps/be/l;->f:I

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/l;->g:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/l;->h:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/be/l;->i:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "personalizedSmartMapsTileDuration: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lmaps/be/l;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " onlyRequestPsmWhenPoiInBaseTile: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lmaps/be/l;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minPsmRequestZoom: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/l;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pertileDuration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/l;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pertileClientCoverage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/l;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " diskCacheServerSchemaVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lmaps/be/l;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " offlineBorderTiles:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lmaps/be/l;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
