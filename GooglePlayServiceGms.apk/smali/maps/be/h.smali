.class final Lmaps/be/h;
.super Lmaps/bn/c;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    iput-object p1, p0, Lmaps/be/h;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lmaps/be/h;->b:Z

    return-void
.end method

.method static synthetic a(Lmaps/be/h;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmaps/be/h;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/DataOutput;)V
    .locals 9

    const/4 v8, -0x1

    new-instance v1, Lmaps/bv/a;

    sget-object v0, Lmaps/cm/f;->a:Lmaps/bv/c;

    invoke-direct {v1, v0}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const-class v2, Lmaps/be/g;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lmaps/be/g;->e()Lmaps/bv/a;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lmaps/bv/a;->j(I)I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    invoke-static {}, Lmaps/be/g;->e()Lmaps/bv/a;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v0}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v4

    new-instance v5, Lmaps/bv/a;

    sget-object v6, Lmaps/cm/f;->d:Lmaps/bv/c;

    invoke-direct {v5, v6}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-static {v4, v6, v7}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v6

    if-eq v6, v8, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v5, v7, v6}, Lmaps/bv/a;->f(II)Lmaps/bv/a;

    :cond_0
    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lmaps/bv/a;->i(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lmaps/bv/a;->e(I)J

    move-result-wide v6

    const/4 v4, 0x2

    invoke-virtual {v5, v4, v6, v7}, Lmaps/bv/a;->a(IJ)Lmaps/bv/a;

    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v4, v5}, Lmaps/bv/a;->a(ILmaps/bv/a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lmaps/be/g;->a(Lmaps/bv/a;)V

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lmaps/bv/a;->a(Ljava/io/OutputStream;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-object v2, Lmaps/cm/f;->c:Lmaps/bv/c;

    invoke-static {v2, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v3

    invoke-virtual {v3, v1}, Lmaps/bv/a;->j(I)I

    move-result v4

    const-class v5, Lmaps/be/g;

    monitor-enter v5

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, v6, v2}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Lmaps/bv/a;->i(I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lmaps/be/g;->b(Lmaps/bv/a;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lmaps/be/g;->c(Lmaps/bv/a;)V

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {}, Lmaps/be/g;->e()Lmaps/bv/a;

    move-result-object v2

    iget-object v3, p0, Lmaps/be/h;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lmaps/be/g;->a(Lmaps/bv/a;Ljava/lang/String;)Z

    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lmaps/be/h;->b:Z

    if-nez v0, :cond_4

    :cond_3
    invoke-static {}, Lmaps/be/g;->f()Lmaps/be/j;

    :cond_4
    invoke-static {}, Lmaps/be/g;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lmaps/be/g;->h()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_1
    invoke-static {}, Lmaps/be/g;->i()Z

    invoke-static {}, Lmaps/be/g;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lmaps/bn/d;->a()Lmaps/bn/d;

    move-result-object v0

    iget-object v3, p0, Lmaps/be/h;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lmaps/be/g;->a(Lmaps/bn/d;Ljava/lang/String;Z)V

    :goto_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_5
    return v1

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_6
    :try_start_2
    new-instance v0, Lmaps/be/i;

    invoke-static {}, Lmaps/ba/i;->a()Lmaps/by/c;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Lmaps/be/i;-><init>(Lmaps/be/h;Lmaps/by/c;)V

    invoke-static {v0}, Lmaps/be/g;->a(Lmaps/by/d;)Lmaps/by/d;

    invoke-static {}, Lmaps/be/g;->k()Lmaps/by/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/by/d;->j()V

    invoke-static {}, Lmaps/be/g;->k()Lmaps/by/d;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/by/d;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final af_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final ag_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x4b

    return v0
.end method
