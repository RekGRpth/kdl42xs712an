.class public final Lmaps/ad/b;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/ac/m;


# instance fields
.field private final a:Lmaps/ac/bt;

.field private final b:I

.field private final c:J

.field private final d:[Lmaps/ad/c;


# direct methods
.method private constructor <init>(Lmaps/ac/bt;Lmaps/bv/a;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lmaps/bv/a;->d(I)I

    move-result v0

    iput v0, p0, Lmaps/ad/b;->b:I

    iput-wide p3, p0, Lmaps/ad/b;->c:J

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lmaps/bv/a;->j(I)I

    move-result v0

    new-array v0, v0, [Lmaps/ad/c;

    iput-object v0, p0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    invoke-direct {p0, p2}, Lmaps/ad/b;->a(Lmaps/bv/a;)[Lmaps/ac/av;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lmaps/ad/b;->a(Lmaps/bv/a;[Lmaps/ac/av;)V

    invoke-direct {p0, p2}, Lmaps/ad/b;->b(Lmaps/bv/a;)V

    return-void
.end method

.method public static a([BI)I
    .locals 4

    new-instance v0, Lmaps/bw/a;

    invoke-direct {v0, p0}, Lmaps/bw/a;-><init>([B)V

    invoke-virtual {v0, p1}, Lmaps/bw/a;->skipBytes(I)I

    invoke-virtual {v0}, Lmaps/bw/a;->readInt()I

    move-result v1

    const v2, 0x45504752

    if-eq v1, v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FORMAT_MAGIC expected. Found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lmaps/bw/a;->readUnsignedShort()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch: 1 expected, "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lmaps/bw/a;->readInt()I

    move-result v0

    return v0
.end method

.method public static a(Lmaps/ac/bt;[BIJ)Lmaps/ad/b;
    .locals 6

    invoke-static {p1, p2}, Lmaps/ad/b;->a([BI)I

    move-result v0

    add-int/lit8 v1, p2, 0xa

    const/16 v2, 0x20

    new-array v2, v2, [B

    invoke-virtual {p0}, Lmaps/ac/bt;->c()I

    move-result v3

    invoke-virtual {p0}, Lmaps/ac/bt;->d()I

    move-result v4

    invoke-virtual {p0}, Lmaps/ac/bt;->b()I

    move-result v5

    invoke-static {v3, v4, v5, v0, v2}, Lmaps/ae/x;->a(IIII[B)V

    new-instance v0, Lmaps/ae/x;

    invoke-direct {v0}, Lmaps/ae/x;-><init>()V

    invoke-virtual {v0, v2}, Lmaps/ae/x;->b([B)V

    array-length v2, p1

    sub-int/2addr v2, v1

    invoke-virtual {v0, p1, v1, v2}, Lmaps/ae/x;->a([BII)V

    array-length v0, p1

    sub-int/2addr v0, v1

    new-instance v2, Ljava/util/zip/Inflater;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/zip/Inflater;-><init>(Z)V

    :try_start_0
    invoke-static {p1, v1, v0}, Lmaps/ax/d;->a([BII)Lmaps/ax/e;

    move-result-object v0

    invoke-virtual {v0}, Lmaps/ax/e;->a()[B

    move-result-object v1

    invoke-virtual {v0}, Lmaps/ax/e;->b()I

    move-result v0

    new-instance v3, Lmaps/bv/a;

    sget-object v4, Lmaps/cd/b;->a:Lmaps/bv/c;

    invoke-direct {v3, v4}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v3, v4, v0}, Lmaps/bv/a;->a(Ljava/io/InputStream;I)I

    new-instance v0, Lmaps/ad/b;

    invoke-direct {v0, p0, v3, p3, p4}, Lmaps/ad/b;-><init>(Lmaps/ac/bt;Lmaps/bv/a;J)V
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/util/zip/DataFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->end()V

    throw v0
.end method

.method private a(Lmaps/bv/a;[Lmaps/ac/av;)V
    .locals 19

    const/4 v1, 0x0

    move v7, v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v7, v1, :cond_b

    mul-int/lit8 v10, v7, 0x2

    mul-int/lit8 v1, v7, 0x2

    add-int/lit8 v11, v1, 0x1

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v12

    const/4 v1, 0x2

    invoke-static {v12, v1}, Lmaps/bv/e;->a(Lmaps/bv/a;I)I

    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v13

    const/4 v1, 0x2

    invoke-static {v13, v1}, Lmaps/bv/e;->a(Lmaps/bv/a;I)I

    const/4 v1, 0x0

    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lmaps/bv/a;->i(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lmaps/bv/a;->c(I)[B

    move-result-object v1

    :cond_0
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v2

    const/4 v3, 0x3

    invoke-virtual {v12, v3}, Lmaps/bv/a;->j(I)I

    move-result v6

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    move v5, v2

    :goto_1
    if-lez v6, :cond_3

    new-array v4, v6, [Lmaps/ad/d;

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_4

    const/4 v2, 0x3

    invoke-virtual {v12, v2, v3}, Lmaps/bv/a;->b(II)I

    move-result v2

    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v2}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lmaps/bv/a;->i(I)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v8, v2}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    :cond_1
    new-instance v9, Lmaps/ad/d;

    const/4 v14, 0x1

    invoke-virtual {v8, v14}, Lmaps/bv/a;->g(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8, v2, v5}, Lmaps/ad/d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v9, v4, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    move v5, v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x1

    new-array v4, v2, [Lmaps/ad/d;

    const/4 v2, 0x0

    sget-object v3, Lmaps/ad/c;->a:Lmaps/ad/d;

    aput-object v3, v4, v2

    :cond_4
    const/4 v8, 0x0

    const/4 v6, 0x4

    aget-object v14, p2, v11

    aget-object v15, p2, v10

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-eqz v1, :cond_f

    new-instance v2, Ljava/io/DataInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v2}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v1

    move-object/from16 v18, v2

    move v2, v1

    move-object/from16 v1, v18

    :goto_3
    new-instance v16, Lmaps/ac/bb;

    add-int/lit8 v3, v2, 0x2

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lmaps/ac/bb;-><init>(I)V

    if-eqz v14, :cond_5

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_5
    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x0

    :goto_4
    if-ge v3, v2, :cond_6

    invoke-static {v1}, Lmaps/ac/cl;->b(Ljava/io/DataInput;)I

    move-result v17

    add-int v9, v9, v17

    invoke-static {v1}, Lmaps/ac/cl;->b(Ljava/io/DataInput;)I

    move-result v17

    add-int v5, v5, v17

    invoke-static {v9, v5}, Lmaps/ac/av;->c(II)Lmaps/ac/av;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_6
    if-eqz v15, :cond_7

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lmaps/ac/bb;->a(Lmaps/ac/av;)Z

    :cond_7
    invoke-virtual/range {v16 .. v16}, Lmaps/ac/bb;->d()Lmaps/ac/az;

    move-result-object v5

    if-nez v14, :cond_8

    if-nez v15, :cond_8

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Both polyline endpoints are missing for segment: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    if-nez v14, :cond_9

    const/4 v2, 0x2

    const/4 v1, 0x5

    :goto_5
    const/4 v3, 0x4

    const/4 v6, 0x0

    invoke-static {v12, v3, v6}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_d

    or-int/lit8 v6, v2, 0x8

    :goto_6
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v13, v2, v3}, Lmaps/bv/e;->a(Lmaps/bv/a;II)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_c

    or-int/lit8 v1, v1, 0x8

    move v8, v1

    :goto_7
    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    new-instance v1, Lmaps/ad/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    invoke-static {v2, v10}, Lmaps/ad/c;->a(Lmaps/ac/bt;I)J

    move-result-wide v2

    invoke-direct/range {v1 .. v6}, Lmaps/ad/c;-><init>(J[Lmaps/ad/d;Lmaps/ac/az;I)V

    aput-object v1, v9, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    new-instance v1, Lmaps/ad/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    invoke-static {v2, v11}, Lmaps/ad/c;->a(Lmaps/ac/bt;I)J

    move-result-wide v2

    move v6, v8

    invoke-direct/range {v1 .. v6}, Lmaps/ad/c;-><init>(J[Lmaps/ad/d;Lmaps/ac/az;I)V

    aput-object v1, v9, v11

    invoke-virtual {v5}, Lmaps/ac/az;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_a

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Segment polyline had fewer than two points for segment: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in tile: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    if-nez v15, :cond_e

    const/4 v2, 0x1

    const/4 v1, 0x6

    goto :goto_5

    :cond_a
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto/16 :goto_0

    :cond_b
    return-void

    :cond_c
    move v8, v1

    goto :goto_7

    :cond_d
    move v6, v2

    goto :goto_6

    :cond_e
    move v1, v6

    move v2, v8

    goto/16 :goto_5

    :cond_f
    move-object v1, v2

    move v2, v3

    goto/16 :goto_3
.end method

.method private static a([B)[I
    .locals 5

    new-instance v1, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v2

    new-array v3, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-static {v1}, Lmaps/ac/cl;->a(Ljava/io/DataInput;)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method private a(Lmaps/bv/a;)[Lmaps/ac/av;
    .locals 9

    const/4 v8, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    array-length v0, v0

    new-array v3, v0, [Lmaps/ac/av;

    invoke-virtual {p1, v8}, Lmaps/bv/a;->j(I)I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v8, v2}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lmaps/bv/a;->d(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lmaps/bv/a;->d(I)I

    move-result v6

    invoke-static {v5, v6}, Lmaps/ac/av;->a(II)Lmaps/ac/av;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v0, v6}, Lmaps/bv/a;->c(I)[B

    move-result-object v0

    invoke-static {v0}, Lmaps/ad/b;->a([B)[I

    move-result-object v6

    move v0, v1

    :goto_1
    array-length v7, v6

    if-ge v0, v7, :cond_0

    aget v7, v6, v0

    aput-object v5, v3, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private b(Lmaps/bv/a;)V
    .locals 14

    const/4 v13, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v13}, Lmaps/bv/a;->j(I)I

    move-result v7

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    new-instance v0, Lmaps/ac/av;

    invoke-direct {v0}, Lmaps/ac/av;-><init>()V

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_4

    invoke-virtual {p1, v13, v6}, Lmaps/bv/a;->c(II)Lmaps/bv/a;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lmaps/bv/a;->c(I)[B

    move-result-object v2

    invoke-static {v2}, Lmaps/ad/b;->a([B)[I

    move-result-object v8

    invoke-virtual {v0, v13}, Lmaps/bv/a;->c(I)[B

    move-result-object v0

    invoke-static {v0}, Lmaps/ad/b;->a([B)[I

    move-result-object v9

    array-length v0, v8

    new-array v10, v0, [Lmaps/ad/a;

    move v0, v1

    move v2, v1

    :goto_1
    array-length v3, v8

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    aget v4, v8, v0

    aget-object v11, v3, v4

    move v3, v1

    move v4, v0

    move v0, v1

    :goto_2
    array-length v5, v8

    if-ge v0, v5, :cond_2

    array-length v5, v9

    if-lt v2, v5, :cond_1

    array-length v0, v8

    move v4, v0

    :cond_0
    :goto_3
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    aget v5, v9, v2

    if-eqz v5, :cond_0

    iget-object v5, p0, Lmaps/ad/b;->d:[Lmaps/ad/c;

    add-int/lit8 v5, v3, 0x1

    new-instance v12, Lmaps/ad/a;

    invoke-direct {v12}, Lmaps/ad/a;-><init>()V

    aput-object v12, v10, v3

    move v3, v5

    goto :goto_3

    :cond_2
    new-array v0, v3, [Lmaps/ad/a;

    array-length v3, v0

    invoke-static {v10, v1, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v11, v0}, Lmaps/ad/c;->a([Lmaps/ad/a;)V

    add-int/lit8 v0, v4, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public final a()Lmaps/ac/bt;
    .locals 1

    iget-object v0, p0, Lmaps/ad/b;->a:Lmaps/ac/bt;

    return-object v0
.end method

.method public final b()Lmaps/ao/b;
    .locals 1

    sget-object v0, Lmaps/ao/b;->i:Lmaps/ao/b;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/ad/b;->b:I

    return v0
.end method

.method public final d()Z
    .locals 4

    iget-wide v0, p0, Lmaps/ad/b;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Lmaps/bs/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lmaps/ad/b;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final n()J
    .locals 2

    iget-wide v0, p0, Lmaps/ad/b;->c:J

    return-wide v0
.end method

.method public final q()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method
