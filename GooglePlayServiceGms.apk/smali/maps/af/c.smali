.class public final Lmaps/af/c;
.super Lmaps/bn/c;


# instance fields
.field private final a:Lmaps/ac/r;

.field private final b:Ljava/util/List;

.field private c:Lmaps/bv/a;

.field private d:Z


# direct methods
.method public constructor <init>(Lmaps/ac/r;)V
    .locals 1

    invoke-direct {p0}, Lmaps/bn/c;-><init>()V

    iput-object p1, p0, Lmaps/af/c;->a:Lmaps/ac/r;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lmaps/af/c;->b:Ljava/util/List;

    return-void
.end method

.method private j()I
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lmaps/af/c;->c:Lmaps/bv/a;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lmaps/af/c;->c:Lmaps/bv/a;

    invoke-virtual {v1, v0}, Lmaps/bv/a;->d(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()Lmaps/ac/r;
    .locals 1

    iget-object v0, p0, Lmaps/af/c;->a:Lmaps/ac/r;

    return-object v0
.end method

.method public final a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lmaps/bv/a;

    sget-object v1, Lmaps/cm/l;->a:Lmaps/bv/c;

    invoke-direct {v0, v1}, Lmaps/bv/a;-><init>(Lmaps/bv/c;)V

    const/4 v1, 0x1

    iget-object v2, p0, Lmaps/af/c;->a:Lmaps/ac/r;

    invoke-virtual {v2}, Lmaps/ac/r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/bv/a;->a(ILjava/lang/String;)Lmaps/bv/a;

    invoke-static {p1, v0}, Lmaps/bv/e;->a(Ljava/io/DataOutput;Lmaps/bv/a;)V

    return-void
.end method

.method public final a(Lmaps/ac/z;)V
    .locals 4

    invoke-direct {p0}, Lmaps/af/c;->j()I

    move-result v0

    if-nez p1, :cond_1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lmaps/af/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/af/b;

    iget-object v3, p0, Lmaps/af/c;->a:Lmaps/ac/r;

    invoke-interface {v0, v3, v1, p1}, Lmaps/af/b;->a(Lmaps/ac/r;ILmaps/ac/z;)V

    goto :goto_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final a(Lmaps/af/b;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmaps/af/c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(Ljava/io/DataInput;)Z
    .locals 1

    sget-object v0, Lmaps/cm/l;->b:Lmaps/bv/c;

    invoke-static {v0, p1}, Lmaps/bv/e;->a(Lmaps/bv/c;Ljava/io/DataInput;)Lmaps/bv/a;

    move-result-object v0

    iput-object v0, p0, Lmaps/af/c;->c:Lmaps/bv/a;

    const/4 v0, 0x1

    return v0
.end method

.method public final ah_()Z
    .locals 1

    iget-boolean v0, p0, Lmaps/af/c;->d:Z

    return v0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x76

    return v0
.end method

.method public final c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/af/c;->d:Z

    return-void
.end method

.method public final e()Lmaps/bv/a;
    .locals 2

    iget-object v0, p0, Lmaps/af/c;->c:Lmaps/bv/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lmaps/af/c;->c:Lmaps/bv/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmaps/bv/a;->f(I)Lmaps/bv/a;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    invoke-direct {p0}, Lmaps/af/c;->j()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
