.class Lmaps/l/be;
.super Ljava/lang/ref/WeakReference;

# interfaces
.implements Lmaps/l/am;


# instance fields
.field private a:I

.field private b:Lmaps/l/am;

.field private volatile c:Lmaps/l/ba;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILmaps/l/am;)V
    .locals 1

    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-static {}, Lmaps/l/m;->j()Lmaps/l/ba;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/be;->c:Lmaps/l/ba;

    iput p3, p0, Lmaps/l/be;->a:I

    iput-object p4, p0, Lmaps/l/be;->b:Lmaps/l/am;

    return-void
.end method


# virtual methods
.method public final a()Lmaps/l/ba;
    .locals 1

    iget-object v0, p0, Lmaps/l/be;->c:Lmaps/l/ba;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lmaps/l/am;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Lmaps/l/ba;)V
    .locals 0

    iput-object p1, p0, Lmaps/l/be;->c:Lmaps/l/ba;

    return-void
.end method

.method public final b()Lmaps/l/am;
    .locals 1

    iget-object v0, p0, Lmaps/l/be;->b:Lmaps/l/am;

    return-object v0
.end method

.method public b(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lmaps/l/am;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lmaps/l/be;->a:I

    return v0
.end method

.method public c(Lmaps/l/am;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lmaps/l/be;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lmaps/l/am;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lmaps/l/am;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lmaps/l/am;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()J
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lmaps/l/am;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public j()Lmaps/l/am;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
