.class public final Lmaps/l/d;
.super Ljava/lang/Object;


# static fields
.field private static a:Lmaps/k/ab;

.field private static b:Lmaps/k/ae;

.field private static final c:Ljava/util/logging/Logger;


# instance fields
.field private d:Z

.field private e:I

.field private f:I

.field private g:J

.field private h:J

.field private i:Lmaps/l/aq;

.field private j:J

.field private k:J

.field private l:J

.field private m:Lmaps/k/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmaps/l/e;

    invoke-direct {v0}, Lmaps/l/e;-><init>()V

    invoke-static {v0}, Lmaps/k/ac;->a(Ljava/lang/Object;)Lmaps/k/ab;

    move-result-object v0

    sput-object v0, Lmaps/l/d;->a:Lmaps/k/ab;

    new-instance v0, Lmaps/l/l;

    invoke-direct {v0}, Lmaps/l/l;-><init>()V

    new-instance v0, Lmaps/l/f;

    invoke-direct {v0}, Lmaps/l/f;-><init>()V

    new-instance v0, Lmaps/l/g;

    invoke-direct {v0}, Lmaps/l/g;-><init>()V

    sput-object v0, Lmaps/l/d;->b:Lmaps/k/ae;

    const-class v0, Lmaps/l/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lmaps/l/d;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmaps/l/d;->d:Z

    iput v3, p0, Lmaps/l/d;->e:I

    iput v3, p0, Lmaps/l/d;->f:I

    iput-wide v1, p0, Lmaps/l/d;->g:J

    iput-wide v1, p0, Lmaps/l/d;->h:J

    iput-wide v1, p0, Lmaps/l/d;->j:J

    iput-wide v1, p0, Lmaps/l/d;->k:J

    iput-wide v1, p0, Lmaps/l/d;->l:J

    sget-object v0, Lmaps/l/d;->a:Lmaps/k/ab;

    iput-object v0, p0, Lmaps/l/d;->m:Lmaps/k/ab;

    return-void
.end method

.method static a(Z)Lmaps/k/ae;
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {}, Lmaps/k/ae;->b()Lmaps/k/ae;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lmaps/l/d;->b:Lmaps/k/ae;

    goto :goto_0
.end method

.method public static a()Lmaps/l/d;
    .locals 1

    new-instance v0, Lmaps/l/d;

    invoke-direct {v0}, Lmaps/l/d;-><init>()V

    return-object v0
.end method

.method static c()Lmaps/k/b;
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lmaps/l/d;->j()Lmaps/l/aq;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/l/aq;->a()Lmaps/k/b;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/k/b;

    return-object v0
.end method

.method static g()Lmaps/l/ci;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lmaps/l/i;->a:Lmaps/l/i;

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ci;

    return-object v0
.end method

.method static j()Lmaps/l/aq;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lmaps/l/aq;->a:Lmaps/l/aq;

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/aq;

    return-object v0
.end method

.method static n()Lmaps/l/ca;
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lmaps/l/h;->a:Lmaps/l/h;

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/ca;

    return-object v0
.end method


# virtual methods
.method public final a(Lmaps/l/j;)Lmaps/k/e;
    .locals 4

    iget-wide v0, p0, Lmaps/l/d;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "maximumWeight requires weigher"

    invoke-static {v0, v1}, Lmaps/k/o;->b(ZLjava/lang/Object;)V

    new-instance v0, Lmaps/l/aj;

    invoke-direct {v0, p0, p1}, Lmaps/l/aj;-><init>(Lmaps/l/d;Lmaps/l/j;)V

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()Lmaps/k/b;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lmaps/l/d;->i()Lmaps/l/aq;

    move-result-object v1

    invoke-virtual {v1}, Lmaps/l/aq;->a()Lmaps/k/b;

    move-result-object v1

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/k/b;

    return-object v0
.end method

.method final d()I
    .locals 2

    iget v0, p0, Lmaps/l/d;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x10

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/l/d;->e:I

    goto :goto_0
.end method

.method final e()I
    .locals 2

    iget v0, p0, Lmaps/l/d;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x4

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lmaps/l/d;->f:I

    goto :goto_0
.end method

.method final f()J
    .locals 4

    const-wide/16 v0, 0x0

    iget-wide v2, p0, Lmaps/l/d;->j:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lmaps/l/d;->k:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Lmaps/l/d;->g:J

    goto :goto_0
.end method

.method public final h()Lmaps/l/d;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v3, Lmaps/l/aq;->b:Lmaps/l/aq;

    iget-object v0, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Key strength was already set to %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    aput-object v5, v1, v2

    invoke-static {v0, v4, v1}, Lmaps/k/o;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/aq;

    iput-object v0, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    return-object p0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method final i()Lmaps/l/aq;
    .locals 2

    iget-object v0, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    sget-object v1, Lmaps/l/aq;->a:Lmaps/l/aq;

    invoke-static {v0, v1}, Lmaps/k/j;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/l/aq;

    return-object v0
.end method

.method final k()J
    .locals 4

    iget-wide v0, p0, Lmaps/l/d;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/l/d;->j:J

    goto :goto_0
.end method

.method final l()J
    .locals 4

    iget-wide v0, p0, Lmaps/l/d;->k:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/l/d;->k:J

    goto :goto_0
.end method

.method final m()J
    .locals 4

    iget-wide v0, p0, Lmaps/l/d;->l:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lmaps/l/d;->l:J

    goto :goto_0
.end method

.method final o()Lmaps/k/ab;
    .locals 1

    iget-object v0, p0, Lmaps/l/d;->m:Lmaps/k/ab;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    const/4 v3, -0x1

    const-wide/16 v5, -0x1

    invoke-static {p0}, Lmaps/k/j;->a(Ljava/lang/Object;)Lmaps/k/k;

    move-result-object v0

    iget v1, p0, Lmaps/l/d;->e:I

    if-eq v1, v3, :cond_0

    const-string v1, "initialCapacity"

    iget v2, p0, Lmaps/l/d;->e:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    :cond_0
    iget v1, p0, Lmaps/l/d;->f:I

    if-eq v1, v3, :cond_1

    const-string v1, "concurrencyLevel"

    iget v2, p0, Lmaps/l/d;->f:I

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;I)Lmaps/k/k;

    :cond_1
    iget-wide v1, p0, Lmaps/l/d;->g:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_2

    const-string v1, "maximumSize"

    iget-wide v2, p0, Lmaps/l/d;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lmaps/k/k;->a(Ljava/lang/String;J)Lmaps/k/k;

    :cond_2
    iget-wide v1, p0, Lmaps/l/d;->h:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_3

    const-string v1, "maximumWeight"

    iget-wide v2, p0, Lmaps/l/d;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lmaps/k/k;->a(Ljava/lang/String;J)Lmaps/k/k;

    :cond_3
    iget-wide v1, p0, Lmaps/l/d;->j:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_4

    const-string v1, "expireAfterWrite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/l/d;->j:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    :cond_4
    iget-wide v1, p0, Lmaps/l/d;->k:J

    cmp-long v1, v1, v5

    if-eqz v1, :cond_5

    const-string v1, "expireAfterAccess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v3, p0, Lmaps/l/d;->k:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    :cond_5
    iget-object v1, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    if-eqz v1, :cond_6

    const-string v1, "keyStrength"

    iget-object v2, p0, Lmaps/l/d;->i:Lmaps/l/aq;

    invoke-virtual {v2}, Lmaps/l/aq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lmaps/k/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lmaps/k/k;->a(Ljava/lang/String;Ljava/lang/Object;)Lmaps/k/k;

    :cond_6
    invoke-virtual {v0}, Lmaps/k/k;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
