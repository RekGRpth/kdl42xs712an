.class final Lmaps/l/ai;
.super Ljava/lang/Object;

# interfaces
.implements Lmaps/l/ba;


# instance fields
.field volatile a:Lmaps/l/ba;

.field private b:Lmaps/s/q;

.field private c:Lmaps/k/y;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Lmaps/l/m;->j()Lmaps/l/ba;

    move-result-object v0

    invoke-direct {p0, v0}, Lmaps/l/ai;-><init>(Lmaps/l/ba;)V

    return-void
.end method

.method public constructor <init>(Lmaps/l/ba;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    new-instance v0, Lmaps/k/y;

    invoke-direct {v0}, Lmaps/k/y;-><init>()V

    iput-object v0, p0, Lmaps/l/ai;->c:Lmaps/k/y;

    iput-object p1, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    return-void
.end method

.method private static a(Lmaps/s/q;Ljava/lang/Throwable;)Z
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1}, Lmaps/s/q;->a(Ljava/lang/Throwable;)Z
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lmaps/l/am;)Lmaps/l/ba;
    .locals 0

    return-object p0
.end method

.method public final a(Ljava/lang/Object;Lmaps/l/j;)Lmaps/s/k;
    .locals 2

    iget-object v0, p0, Lmaps/l/ai;->c:Lmaps/k/y;

    invoke-virtual {v0}, Lmaps/k/y;->a()Lmaps/k/y;

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    invoke-interface {v0}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    invoke-virtual {p2, p1}, Lmaps/l/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmaps/l/ai;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Lmaps/s/f;->a(Ljava/lang/Object;)Lmaps/s/k;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lmaps/k/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, p1}, Lmaps/l/j;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lmaps/s/f;->a(Ljava/lang/Object;)Lmaps/s/k;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lmaps/s/f;->a(Ljava/lang/Object;)Lmaps/s/k;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    instance-of v0, v1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_3
    invoke-virtual {p0, v1}, Lmaps/l/ai;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    goto :goto_0

    :cond_4
    invoke-static {}, Lmaps/s/q;->a()Lmaps/s/q;

    move-result-object v0

    invoke-static {v0, v1}, Lmaps/l/ai;->a(Lmaps/s/q;Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    invoke-virtual {v0, p1}, Lmaps/s/q;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Throwable;)Z
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    invoke-static {v0, p1}, Lmaps/l/ai;->a(Lmaps/s/q;Ljava/lang/Throwable;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lmaps/l/ai;->a(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lmaps/l/m;->j()Lmaps/l/ba;

    move-result-object v0

    iput-object v0, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    invoke-interface {v0}, Lmaps/l/ba;->b()Z

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    invoke-interface {v0}, Lmaps/l/ba;->c()I

    move-result v0

    return v0
.end method

.method public final d()J
    .locals 2

    iget-object v0, p0, Lmaps/l/ai;->c:Lmaps/k/y;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lmaps/k/y;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->b:Lmaps/s/q;

    invoke-static {v0}, Lmaps/s/s;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lmaps/l/am;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmaps/l/ai;->a:Lmaps/l/ba;

    invoke-interface {v0}, Lmaps/l/ba;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
