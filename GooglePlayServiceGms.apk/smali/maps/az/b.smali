.class public final Lmaps/az/b;
.super Lmaps/ay/u;


# instance fields
.field private final a:Ljava/util/Vector;

.field private b:Lmaps/az/a;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmaps/ay/u;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static d(Lmaps/as/a;)V
    .locals 2

    invoke-virtual {p0}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1701

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    return-void
.end method


# virtual methods
.method public final a(Lmaps/as/a;Lmaps/ap/n;)V
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2}, Lmaps/az/a;->a(Lmaps/as/a;Lmaps/ap/n;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    .locals 8

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lmaps/as/a;->z()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    const/16 v1, 0x1701

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    const/4 v1, 0x0

    invoke-virtual {p2}, Lmaps/ar/a;->i()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p2}, Lmaps/ar/a;->j()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface/range {v0 .. v6}, Ljavax/microedition/khronos/opengles/GL10;->glOrthof(FFFFFF)V

    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/az/a;->a(Lmaps/as/a;Lmaps/ar/a;Lmaps/ap/c;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    :try_start_3
    invoke-static {p1}, Lmaps/az/b;->d(Lmaps/as/a;)V

    throw v0

    :cond_2
    invoke-static {p1}, Lmaps/az/b;->d(Lmaps/as/a;)V

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Lmaps/az/a;)V
    .locals 2

    iget-object v1, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lmaps/az/b;->b()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/az/a;->a(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(FFLmaps/ar/a;)Z
    .locals 3

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/az/a;->a_(FFLmaps/ar/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, Lmaps/az/b;->b:Lmaps/az/a;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final at_()V
    .locals 1

    iget-object v0, p0, Lmaps/az/b;->b:Lmaps/az/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmaps/az/b;->b:Lmaps/az/a;

    invoke-virtual {v0}, Lmaps/az/a;->at_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmaps/az/b;->b:Lmaps/az/a;

    :cond_0
    return-void
.end method

.method public final b(Lmaps/az/a;)V
    .locals 2

    iget-object v1, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lmaps/az/b;->b()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(FFLmaps/ac/av;Lmaps/ar/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lmaps/az/a;->b(FFLmaps/ac/av;Lmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lmaps/ar/a;Lmaps/as/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2}, Lmaps/az/a;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lmaps/ay/u;->b(Lmaps/ar/a;Lmaps/as/a;)Z

    move-result v0

    return v0
.end method

.method public final c(Lmaps/as/a;)V
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1}, Lmaps/az/a;->c(Lmaps/as/a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c(Lmaps/az/a;)Z
    .locals 1

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(FFLmaps/ar/a;)Z
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0, p1, p2, p3}, Lmaps/az/a;->d(FFLmaps/ar/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lmaps/ay/v;
    .locals 1

    sget-object v0, Lmaps/ay/v;->s:Lmaps/ay/v;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    iget-object v0, p0, Lmaps/az/b;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmaps/az/a;

    invoke-virtual {v0}, Lmaps/az/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
