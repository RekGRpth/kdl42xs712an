.class public final Lbdt;
.super Lbdq;
.source "SourceFile"

# interfaces
.implements Lbei;


# instance fields
.field private a:I

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 2

    new-instance v0, Lbdr;

    invoke-direct {v0}, Lbdr;-><init>()V

    invoke-direct {p0, v0}, Lbdq;-><init>(Lbdr;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lbdt;->a:I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lbdt;->a:I

    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbeh;

    invoke-interface {v0, p0}, Lbeh;->a(Lbei;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/common/api/Status;)Lbek;
    .locals 0

    return-object p1
.end method

.method protected final a(Lbdn;)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    iget-boolean v0, p0, Lbdt;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbdt;->b:Z

    iget v0, p0, Lbdt;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbdt;->a:I

    iget v0, p0, Lbdt;->a:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbdt;->c:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lbdt;->b:Z

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    :goto_1
    invoke-virtual {p0, v0}, Lbdt;->a(Lbek;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_1
.end method

.method public final e()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbdt;->c:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
