.class public interface abstract Lcfz;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lbsp;J)I
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract a(Lcfp;JLbsj;)Lbsj;
.end method

.method public abstract a(Lcfc;J)Lbsp;
.end method

.method public abstract a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;
.end method

.method public abstract a(J)Lcfc;
.end method

.method public abstract a(Ljava/lang/String;)Lcfc;
.end method

.method public abstract a(Lbsp;)Lcfg;
.end method

.method public abstract a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;
.end method

.method public abstract a(Lbsp;Ljava/lang/String;)Lcfp;
.end method

.method public abstract a(Lbsp;Ljava/lang/String;Ljava/lang/String;)Lcfp;
.end method

.method public abstract a(Lcfc;Ljava/lang/String;Ljava/lang/String;Lcgf;)Lcfp;
.end method

.method public abstract a(Lcfp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfv;
.end method

.method public abstract a(Lcfc;Lbuv;J)Lcgb;
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcge;
.end method

.method public abstract a()Lcgs;
.end method

.method public abstract a(Lbsp;Lcfp;)Lcgs;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcgs;
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;I)Lcgs;
.end method

.method public abstract a(Lbsp;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract a(Lcfc;Lbux;)Ljava/util/List;
.end method

.method public abstract a(Lcfc;Ljava/lang/Boolean;)Ljava/util/Set;
.end method

.method public abstract a(Lcfc;)V
.end method

.method public abstract a(Lcfn;)V
.end method

.method public abstract a(Lcfp;Ljava/util/Set;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/database/data/EntrySpec;Lcom/google/android/gms/drive/database/data/EntrySpec;)V
.end method

.method public abstract a(Lcov;)V
.end method

.method public abstract b(Ljava/lang/String;)Lcfe;
.end method

.method public abstract b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;
.end method

.method public abstract b(Lbsp;Ljava/lang/String;)Lcfp;
.end method

.method public abstract b(Lcfn;)Lcfp;
.end method

.method public abstract b(J)Lcge;
.end method

.method public abstract b()Lcgs;
.end method

.method public abstract b(Lbsp;Lcfp;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract b(Lbsp;)V
.end method

.method public abstract b(Lcfc;)V
.end method

.method public abstract b(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)V
.end method

.method public abstract b(Lcov;)Z
.end method

.method public abstract c(Lcfc;)I
.end method

.method public abstract c()V
.end method

.method public abstract c(Lcfn;)V
.end method

.method public abstract c(Ljava/lang/String;)Z
.end method

.method public abstract d(Ljava/lang/String;)Lcfx;
.end method

.method public abstract d(Lcfc;)Lcgs;
.end method

.method public abstract d()V
.end method

.method public abstract e(Lcfc;)Ljava/util/Set;
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract e()Z
.end method

.method public abstract f()V
.end method

.method public abstract f(Ljava/lang/String;)V
.end method

.method public abstract g()V
.end method

.method public abstract g(Ljava/lang/String;)Z
.end method

.method public abstract h()V
.end method

.method public abstract h(Ljava/lang/String;)V
.end method

.method public abstract i()V
.end method

.method public abstract j()J
.end method

.method public abstract k()Lcgs;
.end method

.method public abstract l()J
.end method

.method public abstract m()Lcgs;
.end method

.method public abstract n()J
.end method

.method public abstract o()Lcgs;
.end method

.method public abstract p()J
.end method

.method public abstract q()J
.end method

.method public abstract r()Z
.end method

.method public abstract s()Lcgf;
.end method

.method public abstract t()Lcgs;
.end method
