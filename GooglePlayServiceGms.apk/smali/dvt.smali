.class public final Ldvt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckedTextView;

.field final synthetic b:Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;Landroid/widget/CheckedTextView;)V
    .locals 0

    iput-object p1, p0, Ldvt;->b:Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;

    iput-object p2, p0, Ldvt;->a:Landroid/widget/CheckedTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.CLEAR_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ldvt;->b:Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Ldvt;->b:Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;

    iget-object v1, p0, Ldvt;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a(Ljava/lang/CharSequence;)Z

    return-void
.end method
