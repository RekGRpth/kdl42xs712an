.class public final Ldsq;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZZZZ)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsq;->b:Ldad;

    iput-object p3, p0, Ldsq;->c:Ljava/lang/String;

    iput p4, p0, Ldsq;->d:I

    iput-boolean p5, p0, Ldsq;->e:Z

    iput-boolean p6, p0, Ldsq;->f:Z

    iput-boolean p7, p0, Ldsq;->g:Z

    iput-boolean p8, p0, Ldsq;->h:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldsq;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->g(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    iget-object v2, p0, Ldsq;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsq;->c:Ljava/lang/String;

    iget v4, p0, Ldsq;->d:I

    iget-boolean v5, p0, Ldsq;->e:Z

    iget-boolean v6, p0, Ldsq;->f:Z

    iget-boolean v7, p0, Ldsq;->g:Z

    iget-boolean v8, p0, Ldsq;->h:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZZZZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
