.class public final Ldrp;
.super Ldaq;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Ldkj;
.implements Ldrt;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ldas;

.field private c:Ljava/lang/String;

.field private d:Ldkb;

.field private e:Ldkk;

.field private final f:Ljava/lang/Object;

.field private g:Landroid/os/IBinder;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private final k:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ldaq;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldrp;->f:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p1, p0, Ldrp;->a:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, Ldrp;->e:Ldkk;

    return-void
.end method

.method private a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)Ldkh;
    .locals 11

    const/4 v4, 0x0

    iget-object v0, p0, Ldrp;->c:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->b_(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v6, v4

    :goto_0
    if-ge v6, v10, :cond_0

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/games/multiplayer/Participant;

    new-instance v0, Ldki;

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v3

    invoke-interface {v5}, Lcom/google/android/gms/games/multiplayer/Participant;->c()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Ldki;-><init>(Ljava/lang/String;Ljava/lang/String;IZI)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ldkh;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->d()I

    move-result v2

    invoke-direct {v0, v1, v7, v2, v8}, Ldkh;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/ArrayList;)V

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ldrp;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    :goto_0
    new-instance v2, Ldjw;

    invoke-direct {v2, p2, v1, v0}, Ldjw;-><init>(Ljava/lang/String;II)V

    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p1, v2}, Ldkb;->a(ILjava/lang/Object;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/data/DataHolder;ZI)V
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ldgi;

    invoke-direct {v1, p1}, Ldgi;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_0
    invoke-virtual {v1}, Ldgi;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldgi;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Ldgi;->b()V

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ldrp;->g()V

    new-instance v1, Ldkg;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldrp;->c:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->b_(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0}, Ldrp;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)Ldkh;

    move-result-object v0

    invoke-direct {v1, v2, v3, p2, v0}, Ldkg;-><init>(Ljava/lang/String;Ljava/lang/String;ZLdkh;)V

    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p3, v1}, Ldkb;->a(ILjava/lang/Object;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ldgi;->b()V

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/internal/ConnectionInfo;
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Ldrp;->b()Z

    move-result v0

    const-string v1, "Must initialize RoomService before use!"

    invoke-static {v0, v1}, Lbiq;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Ldrp;->e:Ldkk;

    invoke-virtual {v0}, Ldkk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldrp;->e:Ldkk;

    invoke-virtual {v0}, Ldkk;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrp;->h:Ljava/lang/String;

    iget-object v0, p0, Ldrp;->d:Ldkb;

    iget-object v1, p0, Ldrp;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ldkb;->a(ILjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v1, p0, Ldrp;->h:Ljava/lang/String;

    iget v2, p0, Ldrp;->i:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/internal/ConnectionInfo;-><init>(Ljava/lang/String;I)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldrp;->e:Ldkk;

    iget-object v1, p0, Ldrp;->c:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Ldkk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Ldrp;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "RoomAndroidService"

    const-string v1, "Could not prepare network for the room."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Ldrp;->e:Ldkk;

    invoke-virtual {v0}, Ldkk;->d()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ldrp;->d:Ldkb;

    iget-object v1, p0, Ldrp;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ldkb;->a(ILjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v1, p0, Ldrp;->h:Ljava/lang/String;

    iget v2, p0, Ldrp;->i:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/internal/ConnectionInfo;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private declared-synchronized f()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Ldrp;->j:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Ldrp;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Ldrp;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0}, Ldkb;->d()V

    const/4 v0, 0x0

    iput-object v0, p0, Ldrp;->d:Ldkb;

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0}, Ldas;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0}, Ldrp;->e()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private i()Z
    .locals 2

    iget-object v0, p0, Ldrp;->b:Ldas;

    if-nez v0, :cond_0

    const-string v0, "RoomAndroidService"

    const-string v1, "Callbacks not set"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static l(Ljava/lang/String;)Ldpi;
    .locals 2

    new-instance v0, Ldpi;

    invoke-direct {v0}, Ldpi;-><init>()V

    new-instance v1, Lbnl;

    invoke-direct {v1}, Lbnl;-><init>()V

    invoke-virtual {v1, p0, v0}, Lbnl;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    return-object v0
.end method


# virtual methods
.method public final a([BLjava/lang/String;)I
    .locals 4

    :try_start_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p2}, Ldkb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to send message to self!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p2}, Ldkb;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad participant ID received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Ldrp;->f()I

    move-result v0

    new-instance v1, Ldka;

    invoke-direct {v1, v0, p1, p2}, Ldka;-><init>(I[BLjava/lang/String;)V

    iget-object v2, p0, Ldrp;->d:Ldkb;

    const/16 v3, 0xd

    invoke-virtual {v2, v3, v1}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    return v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/games/internal/ConnectionInfo;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Ldrp;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/internal/ConnectionInfo;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)Ldpi;
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    array-length v0, v3

    if-ge v2, v0, :cond_1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldpf;

    invoke-virtual {v0}, Ldpf;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, v3}, Ldas;->g(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ldrp;->l(Ljava/lang/String;)Ldpi;
    :try_end_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v2, "Room status could not be parsed."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v2, "Room client is not connected."

    invoke-static {v0, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldrp;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(IILjava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2, p3}, Ldas;->a(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 4

    if-eqz p3, :cond_2

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p0, Ldrp;->d:Ldkb;

    iget-object v2, p0, Ldrp;->d:Ldkb;

    iget-object v3, v2, Lbqi;->b:Lbql;

    if-nez v3, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v2, v1, Lbqi;->b:Lbql;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lbqi;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    iget-object v1, v1, Lbqi;->b:Lbql;

    invoke-virtual {v1, v0}, Lbql;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x1b59

    goto :goto_0

    :cond_3
    iget-object v2, v2, Lbqi;->b:Lbql;

    const/16 v3, 0xc

    invoke-static {v2, v3, p1, v0, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 3

    const/4 v0, 0x4

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Ldrp;->a(Lcom/google/android/gms/common/data/DataHolder;ZI)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(Ldas;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    if-eqz v0, :cond_0

    const-string v0, "RoomAndroidService"

    const-string v1, "Setting callbacks when already set"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Ldrp;->b:Ldas;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(Ldjx;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, p1}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mRegistrationLatency "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldrp;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mMessageToken "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ldrp;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "isInitialized "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ldrp;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Ldrp;->d:Ldkb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldrp;->d:Ldkb;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ldkb;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldrp;->e:Ldkk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldrp;->e:Ldkk;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ldkk;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x3

    :try_start_0
    invoke-direct {p0, v0, p1}, Ldrp;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Ldrp;->h:Ljava/lang/String;

    iput p2, p0, Ldrp;->i:I

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->g:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    const-string v0, "RoomAndroidService"

    const-string v1, "initalize: Deathbinder should be null"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    if-eqz v0, :cond_1

    const-string v0, "RoomAndroidService"

    const-string v1, "initalize: mRealTimeStateMachine should be null"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {p2, p0, v0}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    iget-object v1, p0, Ldrp;->f:Ljava/lang/Object;

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    iput-object p2, p0, Ldrp;->g:Landroid/os/IBinder;

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Ldrp;->a:Landroid/content/Context;

    invoke-static {}, Lddz;->b()I

    move-result v1

    invoke-static {v0, p0, v1}, Ldkk;->a(Landroid/content/Context;Ldkj;I)Ldkk;

    move-result-object v0

    iput-object v0, p0, Ldrp;->e:Ldkk;

    iput-object p1, p0, Ldrp;->c:Ljava/lang/String;

    new-instance v0, Ldkb;

    iget-object v1, p0, Ldrp;->c:Ljava/lang/String;

    iget-object v2, p0, Ldrp;->e:Ldkk;

    invoke-direct {v0, v1, v2, p0}, Ldkb;-><init>(Ljava/lang/String;Ldkk;Ldrt;)V

    iput-object v0, p0, Ldrp;->d:Ldkb;

    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0}, Ldkb;->b()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Failed to register for death binder. Exiting room."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1

    throw v0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    const-string v2, "STATUS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    if-eqz v2, :cond_1

    :try_start_1
    invoke-static {v1}, Ldrp;->l(Ljava/lang/String;)Ldpi;

    move-result-object v0

    const-string v1, "RoomAndroidService"

    invoke-virtual {v0}, Ldpi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Ldrp;->d:Ldkb;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, v0}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_1
    .catch Lbnu; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "RoomAndroidService"

    const-string v2, "MatchStatus parsing error for payload"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "RoomAndroidService"

    const-string v2, "Json parsing error for payload"

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :try_start_4
    const-string v1, "RoomAndroidService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid notification type :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldpc;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-virtual {p3}, Ldpc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Ldas;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 3

    :try_start_0
    new-instance v0, Ldjz;

    const/4 v1, 0x1

    invoke-direct {v0, p2, p1, v1}, Ldjz;-><init>([BLjava/lang/String;I)V

    iget-object v1, p0, Ldrp;->d:Ldkb;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;[BI)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2, p3}, Ldas;->a(Ljava/lang/String;[BI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->a(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a([B[Ljava/lang/String;)V
    .locals 4

    if-eqz p2, :cond_2

    :try_start_0
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    aget-object v2, p2, v0

    iget-object v3, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v3, v2}, Ldkb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to send message to self!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :try_start_1
    iget-object v3, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v3, v2}, Ldkb;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad participant ID received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ldkf;

    invoke-direct {v0, p1, p2}, Ldkf;-><init>([B[Ljava/lang/String;)V

    iget-object v1, p0, Ldrp;->d:Ldkb;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method

.method public final a(Ldpi;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Ldrp;->b:Ldas;

    invoke-virtual {p1}, Ldpi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ldas;->h(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "RoomAndroidService"

    const-string v2, "Room client is not connected."

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 3

    const/4 v0, 0x5

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Ldrp;->a(Lcom/google/android/gms/common/data/DataHolder;ZI)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final b(Ldjx;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    const/16 v1, 0x9

    invoke-virtual {v0, v1, p1}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;[B)V
    .locals 3

    :try_start_0
    new-instance v0, Ldjz;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p1, v1}, Ldjz;-><init>([BLjava/lang/String;I)V

    iget-object v1, p0, Ldrp;->d:Ldkb;

    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Ldkb;->a(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->b(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Ldrp;->e:Ldkk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 3

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p1}, Ldkb;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0}, Ldkb;->c()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final binderDied()V
    .locals 2

    const-string v0, "RoomAndroidService"

    const-string v1, "GamesService died while room was connected. Exiting room"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p1}, Ldkb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to create a socket connection to self."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v0, p1}, Ldkb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad participant ID received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Ldrp;->e:Ldkk;

    invoke-virtual {v1, v0}, Ldkk;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized c()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ldrp;->h()V

    :goto_0
    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string v0, "RoomAndroidService"

    const-string v1, "Failure in the connectToNetwork() operation."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->c(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    const/4 v0, 0x0

    const/16 v1, 0xd

    :try_start_0
    invoke-static {v1}, Lbpz;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "RoomAndroidService"

    const-string v2, "Native sockets are not supported at this API level"

    invoke-static {v1, v2}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v1, p1}, Ldkb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to create a socket connection to self."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Ldrp;->d:Ldkb;

    invoke-virtual {v1, p1}, Ldkb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad participant ID received."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, p0, Ldrp;->e:Ldkk;

    invoke-virtual {v2, v1}, Ldkk;->c(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    invoke-static {v1}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "RoomAndroidService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Native socket creation failed for participant: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrp;->k:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ldrp;->h()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldrp;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "RoomAndroidService"

    const-string v2, "Uncaught exception: "

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final d(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->d(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    iget-object v1, p0, Ldrp;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ldrp;->g:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldrp;->g:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v0, 0x0

    iput-object v0, p0, Ldrp;->g:Landroid/os/IBinder;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->e(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1, p2}, Ldas;->f(Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ldrp;->i()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Ldrp;->b:Ldas;

    invoke-interface {v0, p1}, Ldas;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "RoomAndroidService"

    const-string v1, "Room client is not connected."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
