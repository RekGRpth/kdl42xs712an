.class public final Lgzs;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V
    .locals 0

    iput-object p1, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->d(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    return-void
.end method

.method public final a(Lipg;)V
    .locals 5

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lipg;)Lipg;

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lipg;->a:[Lipj;

    array-length v0, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const-string v0, "AcceptLegalDocsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected number of LegalDocsForCountry returned by server: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lipg;->a:[Lipj;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->b(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lipg;->a:[Lipj;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    new-instance v2, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v3, v0, Lipj;->a:Ljava/lang/String;

    iget-object v4, v0, Lipj;->b:Ljava/lang/String;

    iget-object v0, v0, Lipj;->c:[Ljava/lang/String;

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->a(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->e(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->f(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lipg;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->g(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->h(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->i(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->j(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lgzs;->a:Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;->c(Lcom/google/android/gms/wallet/ia/AcceptLegalDocsActivity;)V

    return-void
.end method
