.class public final Lhdy;
.super Lhct;
.source "SourceFile"


# instance fields
.field private final a:Lhct;


# direct methods
.method public constructor <init>(Lhct;)V
    .locals 0

    invoke-direct {p0}, Lhct;-><init>()V

    iput-object p1, p0, Lhdy;->a:Lhct;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhdy;->a:Lhct;

    invoke-virtual {v0, p1, p2}, Lhct;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
