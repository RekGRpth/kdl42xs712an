.class public final Lbxw;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic c:Landroid/app/Activity;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

.field final synthetic g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;Ljava/lang/String;Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicReference;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;)V
    .locals 0

    iput-object p1, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    iput-object p3, p0, Lbxw;->a:Landroid/os/Handler;

    iput-object p4, p0, Lbxw;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p5, p0, Lbxw;->c:Landroid/app/Activity;

    iput-object p6, p0, Lbxw;->d:Ljava/lang/String;

    iput-object p7, p0, Lbxw;->e:Ljava/lang/String;

    iput-object p8, p0, Lbxw;->f:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lbxw;->a:Landroid/os/Handler;

    new-instance v1, Lbxx;

    invoke-direct {v1, p0}, Lbxx;-><init>(Lbxw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    new-instance v0, Lbqu;

    invoke-direct {v0}, Lbqu;-><init>()V

    iget-object v1, p0, Lbxw;->e:Ljava/lang/String;

    iget-object v2, v0, Lbqu;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcle;->b:Lcje;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    iget-object v1, p0, Lbxw;->f:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lbqu;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcle;->c:Lcje;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    new-instance v1, Lbqt;

    iget-object v0, v0, Lbqu;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lbqt;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;B)V

    iget-object v0, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    invoke-static {v0}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->a(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcfc;

    move-result-object v0

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v2, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    invoke-static {v2}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->c(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lbuc;

    move-result-object v2

    iget-object v1, v1, Lbqt;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    invoke-static {v3}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->b(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {v2, v0, v1, v3}, Lbuc;->a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "driveId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lbxw;->a:Landroid/os/Handler;

    new-instance v2, Lbxy;

    invoke-direct {v2, p0, v1}, Lbxy;-><init>(Lbxw;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Lbrm; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    iget-object v1, p0, Lbxw;->f:Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/gms/drive/data/ui/open/SimpleEntryCreator;->f:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbxw;->g:Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;

    invoke-static {v1}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->d(Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;)Lcbo;

    move-result-object v1

    invoke-interface {v1, v0}, Lcbo;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lbxw;->a:Landroid/os/Handler;

    new-instance v1, Lbxz;

    invoke-direct {v1, p0}, Lbxz;-><init>(Lbxw;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
