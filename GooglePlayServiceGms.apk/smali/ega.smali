.class public final Lega;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field a:Landroid/app/KeyguardManager;

.field b:Z

.field c:Landroid/content/Context;

.field d:Leeq;

.field public e:Z

.field f:Z

.field public g:J

.field h:Z

.field i:Z

.field private j:Leeu;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Leeu;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-boolean v2, p0, Lega;->e:Z

    iput-boolean v1, p0, Lega;->f:Z

    iput-boolean v1, p0, Lega;->k:Z

    iput-object p2, p0, Lega;->j:Leeu;

    iput-object p1, p0, Lega;->c:Landroid/content/Context;

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lega;->a:Landroid/app/KeyguardManager;

    new-instance v0, Leeq;

    iget-object v3, p0, Lega;->c:Landroid/content/Context;

    const-string v4, "GCM_IDLE_ALARM"

    invoke-direct {v0, v3, v4, p0}, Leeq;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v0, p0, Lega;->d:Leeq;

    iget-object v0, p0, Lega;->d:Leeq;

    const-string v3, "com.google.android.intent.action.SEND_IDLE"

    iput-object v3, v0, Leeq;->b:Ljava/lang/String;

    iget-object v0, p0, Lega;->d:Leeq;

    invoke-virtual {v0}, Leeq;->b()V

    iget-object v0, p0, Lega;->c:Landroid/content/Context;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.google.android.intent.action.SEND_IDLE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lega;->c:Landroid/content/Context;

    const/4 v3, 0x0

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v3, "plugged"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v2, :cond_0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lega;->h:Z

    iget-object v0, p0, Lega;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gtalk_idle_timeout_ms"

    const-wide/16 v2, 0x7530

    invoke-static {v0, v1, v2, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lega;->g:J

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lega;->b:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e()V
    .locals 1

    iget-boolean v0, p0, Lega;->e:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lega;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Line;)V
    .locals 4

    iget-object v0, p1, Line;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linc;

    iget-object v2, v0, Linc;->a:Ljava/lang/String;

    iget-object v0, v0, Linc;->b:Ljava/lang/String;

    const-string v3, "IdleNotification"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lega;->e:Z

    goto :goto_0

    :cond_1
    const-string v3, "PowerNotification"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lega;->f:Z

    goto :goto_0

    :cond_2
    const-string v3, "DataActiveNotification"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lega;->k:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lega;->c()V

    return-void
.end method

.method public final a()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lega;->b:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b()V
    .locals 1

    iget-object v0, p0, Lega;->d:Leeq;

    invoke-virtual {v0}, Leeq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lega;->d:Leeq;

    invoke-virtual {v0}, Leeq;->e()V

    :cond_0
    invoke-virtual {p0}, Lega;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lega;->a(Z)V

    invoke-direct {p0}, Lega;->e()V

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lega;->j:Leeu;

    iget-object v0, v0, Leeu;->g:Limv;

    invoke-virtual {v0}, Limv;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Line;

    invoke-direct {v2}, Line;-><init>()V

    const/4 v0, 0x0

    iget-boolean v3, p0, Lega;->f:Z

    if-eqz v3, :cond_2

    const-string v0, "PowerNotification"

    iget-boolean v3, p0, Lega;->h:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :cond_2
    iget-boolean v3, p0, Lega;->e:Z

    if-eqz v3, :cond_3

    const-string v0, "IdleNotification"

    iget-boolean v3, p0, Lega;->b:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    :cond_3
    iget-boolean v3, p0, Lega;->k:Z

    if-eqz v3, :cond_4

    const-string v0, "DataActiveNotification"

    iget-boolean v3, p0, Lega;->i:Z

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3}, Leeu;->a(Line;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    if-eqz v1, :cond_0

    const-string v0, "com.google.android.gsf.gtalkservice"

    invoke-virtual {v2, v0}, Line;->c(Ljava/lang/String;)Line;

    iget-object v0, p0, Lega;->j:Leeu;

    iget-object v0, v0, Leeu;->g:Limv;

    invoke-virtual {v0, v2}, Limv;->d(Lizk;)V

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    invoke-virtual {p0}, Lega;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lega;->d:Leeq;

    invoke-virtual {v0}, Leeq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lega;->d:Leeq;

    iget-wide v1, p0, Lega;->g:J

    invoke-virtual {v0, v1, v2}, Leeq;->a(J)V

    :cond_0
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lega;->a(Z)V

    invoke-direct {p0}, Lega;->e()V

    iget-object v0, p0, Lega;->d:Leeq;

    invoke-virtual {v0}, Leeq;->e()V

    return-void
.end method
