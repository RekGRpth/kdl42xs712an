.class public final enum Laro;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Laro;

.field public static final enum b:Laro;

.field public static final enum c:Laro;

.field private static final synthetic d:[Laro;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Laro;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Laro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laro;->a:Laro;

    new-instance v0, Laro;

    const-string v1, "GRANTED"

    invoke-direct {v0, v1, v3}, Laro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laro;->b:Laro;

    new-instance v0, Laro;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v4}, Laro;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laro;->c:Laro;

    const/4 v0, 0x3

    new-array v0, v0, [Laro;

    sget-object v1, Laro;->a:Laro;

    aput-object v1, v0, v2

    sget-object v1, Laro;->b:Laro;

    aput-object v1, v0, v3

    sget-object v1, Laro;->c:Laro;

    aput-object v1, v0, v4

    sput-object v0, Laro;->d:[Laro;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laro;
    .locals 1

    const-class v0, Laro;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laro;

    return-object v0
.end method

.method public static values()[Laro;
    .locals 1

    sget-object v0, Laro;->d:[Laro;

    invoke-virtual {v0}, [Laro;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laro;

    return-object v0
.end method
