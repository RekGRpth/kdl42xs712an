.class public final Lepv;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/identity/service/AddressService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/identity/service/AddressService;)V
    .locals 0

    iput-object p1, p0, Lepv;->a:Lcom/google/android/gms/identity/service/AddressService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/identity/service/AddressService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lepv;-><init>(Lcom/google/android/gms/identity/service/AddressService;)V

    return-void
.end method


# virtual methods
.method public final d(Lbjv;ILjava/lang/String;)V
    .locals 3

    if-gtz p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientVersion too old"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lepv;->a:Lcom/google/android/gms/identity/service/AddressService;

    invoke-static {v0, p3}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lept;

    iget-object v1, p0, Lepv;->a:Lcom/google/android/gms/identity/service/AddressService;

    invoke-direct {v0, v1, p3}, Lept;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lept;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v1, v0, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddressService"

    const-string v1, "Client died while brokering Address service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
