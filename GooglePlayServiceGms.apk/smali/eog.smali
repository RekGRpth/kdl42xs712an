.class public abstract Leog;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field private static final a:J


# instance fields
.field protected final b:Landroid/content/Context;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3c

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Leog;->a:J

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Leog;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected varargs abstract a(Lahj;)Ljava/lang/Object;
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected final varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    new-instance v1, Lahj;

    iget-object v0, p0, Leog;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, Lahj;-><init>(Landroid/content/Context;)V

    sget-wide v2, Leog;->a:J

    invoke-virtual {v1, v2, v3}, Lahj;->a(J)Lbbo;

    move-result-object v0

    invoke-virtual {v0}, Lbbo;->b()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t connect to Icing. Error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lbbo;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehe;->d(Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Leog;->c:Z

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Leog;->a(Lahj;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {v1}, Lahj;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lahj;->b()V

    throw v0
.end method

.method protected final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    iget-boolean v0, p0, Leog;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Leog;->b:Landroid/content/Context;

    const v1, 0x7f0b01ec    # com.google.android.gms.R.string.icing_storage_managment_connection_error

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Leog;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Leog;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
