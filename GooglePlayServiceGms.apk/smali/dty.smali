.class public final Ldty;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:I

.field private final g:[B

.field private final h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldty;->b:Ldad;

    iput-object p3, p0, Ldty;->c:Ljava/lang/String;

    iput-object p4, p0, Ldty;->d:Ljava/lang/String;

    iput p5, p0, Ldty;->e:I

    iput p6, p0, Ldty;->f:I

    iput-object p8, p0, Ldty;->h:[Ljava/lang/String;

    if-nez p7, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Ldty;->g:[B

    :goto_0
    return-void

    :cond_0
    array-length v0, p7

    new-array v0, v0, [B

    iput-object v0, p0, Ldty;->g:[B

    iget-object v0, p0, Ldty;->g:[B

    array-length v1, p7

    invoke-static {p7, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldty;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->D(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    iget-object v2, p0, Ldty;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldty;->c:Ljava/lang/String;

    iget-object v4, p0, Ldty;->d:Ljava/lang/String;

    iget v5, p0, Ldty;->e:I

    iget v6, p0, Ldty;->f:I

    iget-object v7, p0, Ldty;->g:[B

    iget-object v8, p0, Ldty;->h:[Ljava/lang/String;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;II[B[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
