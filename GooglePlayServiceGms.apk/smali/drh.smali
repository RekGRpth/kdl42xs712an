.class public final Ldrh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldri;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Z

.field private final c:Ldaj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;ZLdaj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldrh;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldrh;->b:Z

    iput-object p3, p0, Ldrh;->c:Ldaj;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcun;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Ldrh;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-boolean v1, p0, Ldrh;->b:Z

    invoke-virtual {p2, p1, v0, v1}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldqq; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    move-object v1, v0

    :goto_0
    :try_start_1
    iget-object v0, p0, Ldrh;->c:Ldaj;

    invoke-interface {v0, v1}, Ldaj;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    instance-of v0, v0, Lane;

    if-eqz v0, :cond_0

    const/16 v0, 0x3e9

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_0
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "SignInIntentService"

    invoke-virtual {v0}, Ldqq;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ldqq;->a()I

    move-result v1

    const/16 v2, 0x3ea

    if-ne v1, v2, :cond_1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    const/16 v2, 0x5dc

    if-ne v1, v2, :cond_2

    iget-object v0, p0, Ldrh;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0, p2}, Leep;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcun;)V

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_3

    invoke-virtual {p2, p1}, Lcun;->c(Landroid/content/Context;)V

    invoke-virtual {v0}, Ldqq;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ldqq;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method
