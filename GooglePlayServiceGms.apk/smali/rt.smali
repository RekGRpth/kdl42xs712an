.class public final Lrt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lsl;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lru;

    invoke-direct {v0, p0, p1}, Lru;-><init>(Lrt;Landroid/os/Handler;)V

    iput-object v0, p0, Lrt;->a:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final a(Lsc;Lsi;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lrt;->a(Lsc;Lsi;Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lsc;Lsi;Ljava/lang/Runnable;)V
    .locals 2

    invoke-virtual {p1}, Lsc;->r()V

    const-string v0, "post-response"

    invoke-virtual {p1, v0}, Lsc;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lrt;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lrv;

    invoke-direct {v1, p0, p1, p2, p3}, Lrv;-><init>(Lrt;Lsc;Lsi;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lsc;Lsp;)V
    .locals 4

    const-string v0, "post-error"

    invoke-virtual {p1, v0}, Lsc;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lsi;->a(Lsp;)Lsi;

    move-result-object v0

    iget-object v1, p0, Lrt;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lrv;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v0, v3}, Lrv;-><init>(Lrt;Lsc;Lsi;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
