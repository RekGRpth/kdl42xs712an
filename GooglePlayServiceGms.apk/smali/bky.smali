.class public final Lbky;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Lblk;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 4

    iget-object v0, p0, Lblk;->a:Lbln;

    iget-boolean v1, p0, Lblk;->d:Z

    if-eqz v1, :cond_0

    iget v0, p0, Lblk;->e:I

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown system group type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    const-string v0, "public"

    :goto_0
    invoke-static {v0, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    :goto_1
    return-object v0

    :pswitch_1
    const-string v0, "domain"

    goto :goto_0

    :pswitch_2
    const-string v0, "myCircles"

    goto :goto_0

    :pswitch_3
    const-string v0, "extendedCircles"

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lblk;->b:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lblk;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-boolean v1, v0, Lbln;->c:Z

    if-eqz v1, :cond_2

    iget-object v0, v0, Lbln;->d:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-boolean v1, v0, Lbln;->a:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lbln;->b:Ljava/lang/String;

    iget-object v0, v0, Lbln;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized sharing target ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/common/people/data/Audience;)Lgge;
    .locals 2

    new-instance v0, Lggf;

    invoke-direct {v0}, Lggf;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lbky;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lggf;->a(Ljava/util/List;)Lggf;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Lggf;->a()Lgge;

    move-result-object v0

    return-object v0

    :pswitch_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lggf;->a(Z)Lggf;

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lggf;->a(Z)Lggf;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lgge;)Ljava/util/ArrayList;
    .locals 7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Lgge;->g()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lgge;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-interface {p0}, Lgge;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgld;

    invoke-interface {v0}, Lgld;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lgld;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lgld;->b()Ljava/lang/String;

    move-result-object v0

    const-string v6, "circle"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v5, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    const-string v6, "person"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v4, 0x0

    invoke-static {v5, v0, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-static {v4, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    new-instance v4, Lgle;

    invoke-direct {v4}, Lgle;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0}, Lfdl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v0}, Lgle;->a(Ljava/lang/String;)Lgle;

    const-string v0, "person"

    invoke-virtual {v4, v0}, Lgle;->b(Ljava/lang/String;)Lgle;

    :goto_2
    invoke-virtual {v4}, Lgle;->a()Lgld;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->h()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lgle;->a(Ljava/lang/String;)Lgle;

    const-string v0, "circle"

    invoke-virtual {v4, v0}, Lgle;->b(Ljava/lang/String;)Lgle;

    goto :goto_2

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lgle;->b(Ljava/lang/String;)Lgle;

    goto :goto_2

    :cond_3
    return-object v2
.end method

.method private static a(Lblf;)Ljava/util/List;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lblf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lblf;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblj;

    iget-object v4, v0, Lblj;->a:Lblk;

    iget-object v0, v0, Lblj;->b:Ljava/lang/String;

    invoke-static {v4, v0}, Lbky;->a(Lblk;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public static a([B)Ljava/util/List;
    .locals 11

    const/4 v2, 0x0

    invoke-static {p0}, Lblf;->a([B)Lblf;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-static {v4}, Lbky;->a(Lblf;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v6, v0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Lblf;->c()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_3

    invoke-virtual {v4, v3}, Lblf;->a(I)Lblg;

    move-result-object v0

    iget-boolean v1, v0, Lblg;->a:Z

    if-eqz v1, :cond_2

    iget-object v8, v0, Lblg;->b:Lbli;

    invoke-virtual {v8}, Lbli;->c()I

    move-result v9

    move v1, v2

    :goto_2
    if-ge v1, v9, :cond_2

    invoke-virtual {v8, v1}, Lbli;->a(I)Lblk;

    move-result-object v0

    const/4 v10, 0x0

    invoke-static {v0, v10}, Lbky;->a(Lblk;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-nez v0, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No rendered information for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " available."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    return-object v5
.end method

.method public static a(Lbli;Ljava/util/List;)V
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    new-instance v2, Lblk;

    invoke-direct {v2}, Lblk;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown member type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown system group type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lblk;->a(Ljava/lang/String;)Lblk;

    :goto_1
    invoke-virtual {p0, v2}, Lbli;->a(Lblk;)Lbli;

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Lblk;->a(I)Lblk;

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_2

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_2

    :pswitch_5
    const/4 v0, 0x4

    goto :goto_2

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lbln;

    invoke-direct {v3}, Lbln;-><init>()V

    invoke-static {v0}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lfdl;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbln;->a(Ljava/lang/String;)Lbln;

    :goto_3
    invoke-virtual {v2, v3}, Lblk;->a(Lbln;)Lblk;

    goto :goto_1

    :cond_0
    invoke-static {v0}, Lfdl;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbln;->b(Ljava/lang/String;)Lbln;

    goto :goto_3

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static b(Lgge;)Lcom/google/android/gms/common/people/data/Audience;
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Lbkw;

    invoke-direct {v0}, Lbkw;-><init>()V

    invoke-static {p0}, Lbky;->a(Lgge;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lbkw;->a(Ljava/util/Collection;)Lbkw;

    move-result-object v2

    invoke-interface {p0}, Lgge;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Lgge;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lbkw;->a(Z)Lbkw;

    move-result-object v0

    invoke-interface {p0}, Lgge;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Lgge;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lbkw;->a(I)Lbkw;

    :cond_0
    invoke-virtual {v0}, Lbkw;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    goto :goto_1
.end method
