.class public final Lhzx;
.super Leqv;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Lhvb;

.field private final d:Lhyq;

.field private final e:Ligr;

.field private final f:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Leqv;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    iput-object p1, p0, Lhzx;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lhzx;->b:Landroid/content/pm/PackageManager;

    new-instance v0, Lhyq;

    invoke-direct {v0, p1}, Lhyq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhzx;->d:Lhyq;

    invoke-static {p1}, Lhvb;->a(Landroid/content/Context;)Lhvb;

    move-result-object v0

    iput-object v0, p0, Lhzx;->c:Lhvb;

    new-instance v0, Ligr;

    iget-object v1, p0, Lhzx;->c:Lhvb;

    invoke-direct {v0, p1, v1}, Ligr;-><init>(Landroid/content/Context;Lhvb;)V

    iput-object v0, p0, Lhzx;->e:Ligr;

    return-void
.end method

.method private a(Landroid/content/Intent;)I
    .locals 3

    iget-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a(I)V
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lilm;->a(Landroid/content/Context;)I

    move-result v0

    if-ge v0, p1, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_FINE_LOCATION permission to request PRIORITY_HIGH_ACCURACY locations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION permission to perform any location operations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private static a(Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid pending intent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "PendingIntent\'s target package can\'t be different to the request package."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public static synthetic a(Lhzx;Landroid/content/Intent;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {p1}, Lhon;->b(Landroid/content/Intent;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const-string v0, "GoogleLocationManagerSe"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown cache type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1}, Lhvb;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lhzx;->d:Lhyq;

    invoke-static {p1}, Lhon;->a(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lhon;->b(Landroid/content/Intent;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    :goto_1
    invoke-static {v0}, Lwe;->a(Z)V

    const-string v0, "GeofencerHelper"

    const-string v1, "Initializing geofence\'s system cache."

    invoke-static {v0, v1}, Lhyb;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v2, Lhyq;->a:Lhyt;

    invoke-virtual {v0, p1}, Lhyt;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lhzx;->e:Ligr;

    iget-object v2, v2, Ligr;->b:Ligp;

    invoke-static {p1}, Lhon;->a(Landroid/content/Intent;)Z

    move-result v3

    invoke-static {v3}, Lbkm;->b(Z)V

    invoke-static {p1}, Lhon;->b(Landroid/content/Intent;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    :goto_2
    invoke-static {v0}, Lbkm;->b(Z)V

    const-string v0, "PlaceSubscriptionManager"

    const-string v1, "Initializing PlaceSubscriptionManager\'s system cache."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v2, Ligp;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, v2, Ligp;->b:Z

    if-eqz v0, :cond_2

    const-string v0, "PlaceSubscriptionManager"

    const-string v2, "PlaceSubscriptionManager.initializeSystemCache called >1 times"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v2, Ligp;->b:Z

    iget-object v0, v2, Ligp;->c:Lhon;

    invoke-virtual {v0, p1}, Lhon;->c(Landroid/content/Intent;)V

    iget-object v0, v2, Ligp;->c:Lhon;

    invoke-virtual {v0}, Lhon;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {v2, v0}, Ligp;->a(Lcom/google/android/location/places/PlaceSubscription;)V

    goto :goto_3

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_MOCK_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must have ACCESS_MOCK_LOCATION permission to perform mock operations."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mock_location"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "The Settings.Secure.ALLOW_MOCK_LOCATION system setting is not enabled."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method public static synthetic b(Lhzx;Landroid/content/Intent;)V
    .locals 2

    iget-object v1, p0, Lhzx;->f:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0}, Lhvb;->a()V

    :cond_0
    invoke-direct {p0, p1}, Lhzx;->a(Landroid/content/Intent;)I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static synthetic c(Lhzx;Landroid/content/Intent;)V
    .locals 3

    iget-object v1, p0, Lhzx;->f:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lhzx;->a(Landroid/content/Intent;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v2, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lhzx;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0}, Lhvb;->b()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lilm;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Geofence usage requires ACCESS_FINE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lilm;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0}, Lhzx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhzx;->e:Ligr;

    iget-object v0, v0, Ligr;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client not authorized to use Places API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhzx;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    iget-object v2, p0, Lhzx;->b:Landroid/content/pm/PackageManager;

    aget-object v0, v1, v0

    invoke-static {v2, v0}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhzx;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/location/Location;
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-direct {p0}, Lhzx;->c()Z

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lhvb;->a(ILjava/lang/String;Z)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public final a(JZLandroid/app/PendingIntent;)V
    .locals 4

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Landroid/content/Context;)V

    invoke-direct {p0}, Lhzx;->d()Z

    move-result v0

    const-string v1, "GoogleLocationManagerSe"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GoogleLocationManagerSe"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestActivityUpdates: isFirstParty="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " detectionIntervalMillis="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " force="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " callback="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {p4}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lbqf;->a(ILjava/lang/String;)Landroid/os/WorkSource;

    move-result-object v1

    new-instance v2, Liac;

    invoke-direct {v2}, Liac;-><init>()V

    invoke-virtual {v2, p1, p2, p3, p4}, Liac;->a(JZLandroid/app/PendingIntent;)Liac;

    move-result-object v2

    invoke-virtual {v2, v0}, Liac;->c(Z)Liac;

    move-result-object v0

    invoke-virtual {v0, v1}, Liac;->a(Landroid/os/WorkSource;)Liac;

    move-result-object v0

    iget-object v1, p0, Lhzx;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    return-void
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 2

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/internal/GoogleLocationManagerService;->a(Landroid/content/Context;)V

    new-instance v0, Liac;

    invoke-direct {v0}, Liac;-><init>()V

    invoke-virtual {v0, p1}, Liac;->a(Landroid/app/PendingIntent;)Liac;

    move-result-object v0

    iget-object v1, p0, Lhzx;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Liac;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    return-void
.end method

.method public final a(Landroid/app/PendingIntent;Leqr;Ljava/lang/String;)V
    .locals 7

    :try_start_0
    invoke-static {p1, p3}, Lhzx;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-direct {p0, p3}, Lhzx;->c(Ljava/lang/String;)V

    iget-object v6, p0, Lhzx;->d:Lhyq;

    const-string v0, "PendingIntent not specified."

    invoke-static {p1, v0}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Package name not specified."

    invoke-static {p3, v0}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lhzv;

    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhzv;-><init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Leqr;)V

    iget-object v1, v6, Lhyq;->a:Lhyt;

    invoke-virtual {v1, v0}, Lhyt;->a(Lhzv;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "original removeGeofencesByPendingIntent() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    invoke-direct {p0}, Lhzx;->b()V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1}, Lhvb;->b(Landroid/location/Location;)V

    return-void
.end method

.method public final a(Landroid/location/Location;I)V
    .locals 2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lhzx;->a(I)V

    invoke-direct {p0}, Lhzx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must be signed by Google to use injection API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1, p2}, Lhvb;->a(Landroid/location/Location;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-direct {p0}, Lhzx;->c()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lhvb;->b(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqc;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-direct {p0}, Lhzx;->c()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqc;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/location/LocationRequest;->b()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-direct {p0}, Lhzx;->c()Z

    move-result v1

    invoke-virtual {v0, p1, p2, v1, p3}, Lhvb;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;ZLjava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 6

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0}, Lhvb;->a(Landroid/content/Context;)Lhvb;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    iget-object v2, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0}, Lhzx;->c()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lhvb;->a(ILjava/lang/String;Z)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "getLastPlace: doing a nearby search"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {p0, v1, p1, p2, p3}, Lhzx;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/location/places/internal/PlacesParams;)V
    .locals 4

    const/4 v3, 0x3

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    const-string v0, "PlacesHelper"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlacesHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reporting place: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    iget v1, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:I

    invoke-static {v3, v0, v1}, Ligu;->a(ILjava/lang/String;I)Liuh;

    move-result-object v0

    new-instance v1, Liuf;

    invoke-direct {v1}, Liuf;-><init>()V

    iput-object v1, v0, Liuh;->g:Liuf;

    iget-object v1, v0, Liuh;->f:Liuf;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceReport;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Liuf;->a:Ljava/lang/String;

    iget-object v0, v0, Liuh;->f:Liuf;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/PlaceReport;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Liuf;->b:Ljava/lang/String;

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 4

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    invoke-static {p1}, Ligt;->a(Lcom/google/android/gms/location/places/PlaceRequest;)I

    move-result v0

    iget-object v1, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    iget-object v2, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v2}, Lilm;->a(Landroid/content/Context;)I

    move-result v2

    if-ge v2, v0, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requested place updates requires ACCESS_FINE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requested place updates requires ACCESS_COARSE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lhzx;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-static {p3, v0}, Lhzx;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    invoke-static {p1, p2, p3}, Lcom/google/android/location/places/PlaceSubscription;->a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;

    move-result-object v1

    iget-object v0, v0, Ligr;->b:Ligp;

    iget-object v2, v0, Ligp;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Ligp;->c:Lhon;

    invoke-virtual {v3, v1}, Lhon;->a(Landroid/os/Parcelable;)V

    iget-boolean v3, v0, Ligp;->b:Z

    if-nez v3, :cond_2

    monitor-exit v2

    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0, v1}, Ligp;->a(Lcom/google/android/location/places/PlaceSubscription;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-static {p2, v0}, Lhzx;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    iget-object v0, v0, Ligr;->b:Ligp;

    invoke-static {v1, v1, p2}, Lcom/google/android/location/places/PlaceSubscription;->a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/location/places/internal/PlacesParams;Landroid/app/PendingIntent;)Lcom/google/android/location/places/PlaceSubscription;

    move-result-object v1

    invoke-virtual {v0, v1}, Ligp;->b(Lcom/google/android/location/places/PlaceSubscription;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 5

    iget-object v0, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    iget-object v1, v0, Ligr;->a:Lifw;

    invoke-virtual {v1, p1, p2, p3, p4}, Lifw;->a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    const/4 v1, 0x1

    iget-object v2, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    iget v3, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:I

    invoke-static {v1, v2, v3}, Ligu;->a(ILjava/lang/String;I)Liuh;

    move-result-object v1

    const/4 v2, 0x2

    iget-object v3, p3, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, p2}, Ligu;->a(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)Liuj;

    move-result-object v2

    iput-object v2, v1, Liuh;->e:Liuj;

    iget-object v2, v1, Liuh;->e:Liuj;

    new-instance v3, Liui;

    invoke-direct {v3}, Liui;-><init>()V

    iput-object v3, v2, Liuj;->f:Liui;

    iget-object v1, v1, Liuh;->e:Liuj;

    iget-object v1, v1, Liuj;->f:Liui;

    invoke-static {p1}, Ligu;->a(Lcom/google/android/gms/maps/model/LatLng;)Liuc;

    move-result-object v2

    iput-object v2, v1, Liui;->a:Liuc;

    invoke-virtual {v0}, Ligr;->a()Z

    return-void
.end method

.method public final a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v6, p0, Lhzx;->e:Ligr;

    iget-object v0, v6, Ligr;->a:Lifw;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lifw;->a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    iget-object v0, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    iget v1, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->a:I

    invoke-static {v7, v0, v1}, Ligu;->a(ILjava/lang/String;I)Liuh;

    move-result-object v0

    iget-object v1, p4, Lcom/google/android/gms/location/places/internal/PlacesParams;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v1, v2, p3}, Ligu;->a(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;)Liuj;

    move-result-object v1

    iput-object v1, v0, Liuh;->e:Liuj;

    iget-object v1, v0, Liuh;->e:Liuj;

    new-instance v2, Liuk;

    invoke-direct {v2}, Liuk;-><init>()V

    iput-object v2, v1, Liuj;->e:Liuk;

    iget-object v1, v0, Liuh;->e:Liuj;

    iget-object v1, v1, Liuj;->e:Liuk;

    new-instance v2, Liud;

    invoke-direct {v2}, Liud;-><init>()V

    iput-object v2, v1, Liuk;->a:Liud;

    iget-object v1, v0, Liuh;->e:Liuj;

    iget-object v1, v1, Liuj;->e:Liuk;

    iget-object v1, v1, Liuk;->a:Liud;

    iget-object v2, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v2}, Ligu;->a(Lcom/google/android/gms/maps/model/LatLng;)Liuc;

    move-result-object v2

    iput-object v2, v1, Liud;->b:Liuc;

    iget-object v0, v0, Liuh;->e:Liuj;

    iget-object v0, v0, Liuj;->e:Liuk;

    iget-object v0, v0, Liuk;->a:Liud;

    iget-object v1, p1, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Ligu;->a(Lcom/google/android/gms/maps/model/LatLng;)Liuc;

    move-result-object v1

    iput-object v1, v0, Liud;->a:Liuc;

    invoke-virtual {v6}, Ligr;->a()Z

    return-void
.end method

.method public final a(Leqc;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1}, Lhvb;->a(Leqc;)V

    return-void
.end method

.method public final a(Leqr;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-direct {p0, p2}, Lhzx;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->d:Lhyq;

    const-string v1, "Package name not specified."

    invoke-static {p2, v1}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p1}, Lhzv;->a(Ljava/lang/String;Leqr;)Lhzv;

    move-result-object v1

    iget-object v0, v0, Lhyq;->a:Lhyt;

    invoke-virtual {v0, v1}, Lhyt;->a(Lhzv;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "original removeAllGeofences() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 4

    const-string v0, "\nGeofencer State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->d:Lhyq;

    iget-object v0, v0, Lhyq;->a:Lhyt;

    invoke-virtual {v0, p2}, Lhyt;->b(Ljava/io/PrintWriter;)V

    const-string v0, "\nFused Location Provider State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1, p2, p3}, Lhvb;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const-string v0, "\nPlaces State:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    iget-object v0, v0, Ligr;->b:Ligp;

    iget-object v1, v0, Ligp;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Ligp;->b:Z

    if-nez v2, :cond_0

    const-string v0, "PlaceSubscriptionManager not yet initialized from cache"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const-string v2, "\nActive Place subscriptions:"

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Ligp;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ligi;

    const-string v3, "  "

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v0, Ligi;->f:Lcom/google/android/location/places/PlaceSubscription;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 1

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhzx;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    aget-object v0, v2, v1

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lhzx;->e:Ligr;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceImpl$LogEntry;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl$LogEntry;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl$LogEntry;->d()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl$LogEntry;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/internal/PlaceImpl$LogEntry;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x2

    invoke-static {v6, v3, v4}, Ligu;->a(ILjava/lang/String;I)Liuh;

    move-result-object v3

    new-instance v4, Liuf;

    invoke-direct {v4}, Liuf;-><init>()V

    iput-object v4, v3, Liuh;->f:Liuf;

    iget-object v4, v3, Liuh;->f:Liuf;

    iput-object v5, v4, Liuf;->a:Ljava/lang/String;

    iget-object v3, v3, Liuh;->f:Liuf;

    iput-object v0, v3, Liuf;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ligr;->a()Z

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;Landroid/app/PendingIntent;Leqr;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    invoke-static {p2, p4}, Lhzx;->a(Landroid/app/PendingIntent;Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lhzx;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lhzx;->d:Lhyq;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Geofence list can not be null or empty."

    invoke-static {v0, v2}, Lwe;->a(ZLjava/lang/Object;)V

    const-string v0, "PendingIntent not specified."

    invoke-static {p2, v0}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Package name not specified."

    invoke-static {p4, v0}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v1, Lhyq;->a:Lhyt;

    invoke-virtual {v0, p1, p3, p2}, Lhyt;->a(Ljava/util/List;Leqr;Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "original addGeofence() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    invoke-direct {p0}, Lhzx;->b()V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1}, Lhvb;->a(Z)V

    return-void
.end method

.method public final a([Ljava/lang/String;Leqr;Ljava/lang/String;)V
    .locals 7

    :try_start_0
    invoke-direct {p0, p3}, Lhzx;->c(Ljava/lang/String;)V

    iget-object v6, p0, Lhzx;->d:Lhyq;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "geofenceRequestIds can not be null or empty."

    invoke-static {v0, v1}, Lwe;->a(ZLjava/lang/Object;)V

    const-string v0, "Package name not specified."

    invoke-static {p3, v0}, Lwe;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lhzv;

    const/4 v1, 0x2

    const/4 v2, 0x0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lhzv;-><init>(ILandroid/app/PendingIntent;[Ljava/lang/String;Ljava/lang/String;Leqr;)V

    iget-object v1, v6, Lhyq;->a:Lhyt;

    invoke-virtual {v1, v0}, Lhyt;->a(Lhzv;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GoogleLocationManagerSe"

    const-string v2, "original removeGeofencesByRequestIds() exception (before parcelling)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    throw v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhzx;->a(I)V

    invoke-direct {p0}, Lhzx;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Client must be signed by Google to use status API"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0}, Lhvb;->c()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/app/PendingIntent;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lhzx;->a(I)V

    iget-object v0, p0, Lhzx;->c:Lhvb;

    invoke-virtual {v0, p1}, Lhvb;->b(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V
    .locals 1

    iget-object v0, p2, Lcom/google/android/gms/location/places/internal/PlacesParams;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lhzx;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lhzx;->e:Ligr;

    iget-object v0, v0, Ligr;->a:Lifw;

    invoke-virtual {v0, p1, p2, p3}, Lifw;->a(Ljava/lang/String;Lcom/google/android/gms/location/places/internal/PlacesParams;Lerq;)V

    return-void
.end method
