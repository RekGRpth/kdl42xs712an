.class public abstract Latw;
.super Laua;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# static fields
.field public static v:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Landroid/view/View$OnClickListener;

.field private C:Landroid/widget/TextView$OnEditorActionListener;

.field protected n:Landroid/widget/FrameLayout;

.field protected o:Lauc;

.field protected p:Landroid/view/View;

.field protected q:Z

.field protected r:Landroid/widget/TextView;

.field protected s:I

.field protected t:I

.field protected u:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "SetupWizardAccountInfoSharedPrefs"

    sput-object v0, Latw;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Laua;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Latw;->q:Z

    const/16 v0, -0x65

    iput v0, p0, Latw;->t:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Latw;->A:Z

    new-instance v0, Latx;

    invoke-direct {v0, p0}, Latx;-><init>(Latw;)V

    iput-object v0, p0, Latw;->u:Landroid/view/View$OnClickListener;

    new-instance v0, Laty;

    invoke-direct {v0, p0}, Laty;-><init>(Latw;)V

    iput-object v0, p0, Latw;->B:Landroid/view/View$OnClickListener;

    new-instance v0, Latz;

    invoke-direct {v0, p0}, Latz;-><init>(Latw;)V

    iput-object v0, p0, Latw;->C:Landroid/widget/TextView$OnEditorActionListener;

    return-void
.end method

.method static synthetic a(Latw;)Z
    .locals 1

    iget-boolean v0, p0, Latw;->A:Z

    return v0
.end method

.method static synthetic b(Latw;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Latw;->B:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 3

    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/16 v0, 0x40

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Latw;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Latw;->q:Z

    :cond_0
    return-void
.end method

.method protected final a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p2, :cond_0

    iput-object p1, p0, Latw;->p:Landroid/view/View;

    :cond_0
    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    check-cast p1, Landroid/widget/EditText;

    iget-object v0, p0, Latw;->C:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Latw;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    invoke-virtual {p0}, Latw;->h()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0x52

    if-ne v3, v4, :cond_3

    move v3, v1

    :goto_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_4

    move v4, v1

    :goto_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    const/16 v6, 0x18

    if-ne v5, v6, :cond_5

    move v5, v1

    :goto_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v6

    const/16 v7, 0x19

    if-ne v6, v7, :cond_0

    move v2, v1

    :cond_0
    if-eqz v0, :cond_6

    iget-boolean v0, p0, Latw;->q:Z

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Laua;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_4
    return v1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v4, v2

    goto :goto_2

    :cond_5
    move v5, v2

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_7

    if-nez v3, :cond_7

    if-nez v4, :cond_7

    if-nez v5, :cond_7

    if-eqz v2, :cond_1

    :cond_7
    invoke-super {p0, p1}, Laua;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_4
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method protected f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final g()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Latw;->n:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latw;->n:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const v2, 0x7f020063    # com.google.android.gms.R.drawable.auth_btn_dir_prev_holo_dark

    const/4 v1, 0x1

    invoke-super {p0, p1}, Laua;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Latw;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Laox;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-static {v0}, Laox;->a(Landroid/view/ViewConfiguration;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Latw;->f()Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Latw;->requestWindowFeature(I)Z

    invoke-static {p0}, Laox;->a(Landroid/app/Activity;)V

    invoke-static {p0}, Laox;->b(Landroid/app/Activity;)V

    :goto_2
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Latw;->n:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_1

    const-string v0, "nextRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Latw;->s:I

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-le v0, v1, :cond_4

    const v0, 0x7f0a00a5    # com.google.android.gms.R.id.next_button

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    const v1, 0x7f02005f    # com.google.android.gms.R.drawable.auth_btn_dir_next_holo_dark

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    const v0, 0x7f0a00aa    # com.google.android.gms.R.id.back_button

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_3
    const v0, 0x7f0a007b    # com.google.android.gms.R.id.cancel_button

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_4
    iget-object v0, p0, Latw;->n:Landroid/widget/FrameLayout;

    invoke-super {p0, v0}, Laua;->setContentView(Landroid/view/View;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v1}, Latw;->requestWindowFeature(I)Z

    goto :goto_2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, -0x1

    invoke-super {p0, p1}, Laua;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "currentFocus"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Laua;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Latw;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    const-string v1, "currentFocus"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "nextRequest"

    iget v1, p0, Latw;->s:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public setContentView(I)V
    .locals 2

    invoke-virtual {p0}, Latw;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Latw;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Latw;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latw;->r:Landroid/widget/TextView;

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    invoke-virtual {p0, p1}, Latw;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Latw;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {p0, v0}, Latw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Latw;->r:Landroid/widget/TextView;

    iget-object v0, p0, Latw;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Latw;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Laua;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
