.class public final Lfou;
.super Lk;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private Y:Lfov;

.field private Z:Landroid/accounts/Account;

.field private aa:Lfxh;

.field private ab:Z

.field private ac:Lfoa;

.field private ad:Ljava/lang/String;

.field private ae:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lk;-><init>()V

    return-void
.end method

.method public static a(Landroid/accounts/Account;Lfxh;Ljava/lang/String;Z)Lfou;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "application"

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "signed_up"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lfou;

    invoke-direct {v1}, Lfou;-><init>()V

    invoke-virtual {v1, v0}, Lfou;->g(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Lk;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfou;->Y:Lfov;

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfov;

    if-eqz v0, :cond_0

    check-cast p1, Lfov;

    iput-object p1, p0, Lfou;->Y:Lfov;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DisconnectSourceDialog has to be hosted by an Activity that implements OnDisconnectSourceAcceptedListener."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lk;->a_(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lfou;->Z:Landroid/accounts/Account;

    const-string v0, "application"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfxh;

    iput-object v0, p0, Lfou;->aa:Lfxh;

    if-eqz p1, :cond_0

    const-string v0, "delete_all_frames"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfou;->ab:Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfnz;->a(Landroid/content/Context;)Lfnz;

    move-result-object v0

    iget-object v2, p0, Lfou;->aa:Lfxh;

    invoke-virtual {v0, v2}, Lfnz;->a(Lfxh;)Lfoa;

    move-result-object v0

    iput-object v0, p0, Lfou;->ac:Lfoa;

    const-string v0, "calling_package_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfou;->ad:Ljava/lang/String;

    const-string v0, "signed_up"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfou;->ae:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    const/4 v5, 0x0

    const v4, 0x7f0a0071    # com.google.android.gms.R.id.message

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b037d    # com.google.android.gms.R.string.plus_disconnect_source_dialog_positive_button

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f0b037e    # com.google.android.gms.R.string.plus_disconnect_source_dialog_negative_button

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    iget-boolean v0, p0, Lfou;->ae:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfou;->aa:Lfxh;

    invoke-interface {v0}, Lfxh;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b0379    # com.google.android.gms.R.string.plus_disconnect_source_dialog_title

    invoke-virtual {p0, v0}, Lfou;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lfou;->ac:Lfoa;

    iget-object v2, v0, Lfoa;->a:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f0400d3    # com.google.android.gms.R.layout.plus_disconnect_source_dialog_contents

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, 0x7f0b037a    # com.google.android.gms.R.string.plus_disconnect_source_dialog_message

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    aput-object v2, v5, v7

    invoke-virtual {p0, v4, v5}, Lfou;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v4, 0x7f0b037c    # com.google.android.gms.R.string.plus_disconnect_source_dialog_checkbox

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p0, v4, v5}, Lfou;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v2, p0, Lfou;->ab:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    :goto_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400d4    # com.google.android.gms.R.layout.plus_disconnect_source_non_aspen_dialog_contents

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0b037b    # com.google.android.gms.R.string.plus_disconnect_source_non_aspen_dialog_message

    invoke-virtual {p0, v3}, Lfou;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lk;->e(Landroid/os/Bundle;)V

    const-string v0, "delete_all_frames"

    iget-boolean v1, p0, Lfou;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 5

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfou;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfou;->ad:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    const v1, 0x7f0a0060    # com.google.android.gms.R.id.checkbox

    if-ne v0, v1, :cond_0

    iput-boolean p2, p0, Lfou;->ab:Z

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lfou;->Y:Lfov;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfou;->Y:Lfov;

    iget-object v1, p0, Lfou;->Z:Landroid/accounts/Account;

    iget-object v1, p0, Lfou;->aa:Lfxh;

    iget-boolean v2, p0, Lfou;->ab:Z

    invoke-interface {v0, v1, v2}, Lfov;->a(Lfxh;Z)V

    iget-boolean v0, p0, Lfou;->ab:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfou;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfou;->ad:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lk;->a(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfou;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfou;->ad:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfou;->Z:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v2, Lbcj;->u:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbck;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfou;->ad:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1
.end method
