.class public Lfyw;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbog;
.implements Lboh;


# instance fields
.field private Y:Z

.field private Z:Ljava/lang/String;

.field protected a:Lfyf;

.field private aa:I

.field private ab:Z

.field protected b:Landroid/widget/LinearLayout;

.field protected c:Landroid/widget/Button;

.field protected d:Landroid/widget/Button;

.field private e:Lfyy;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

.field private h:Landroid/view/View;

.field private i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lfyw;)Lcom/google/android/gms/common/ui/ScrollViewWithEvents;
    .locals 1

    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    return-object v0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfyw;
    .locals 4

    new-instance v0, Lfyw;

    invoke-direct {v0}, Lfyw;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "promo_app_package"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "promo_app_text"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "back_button_name"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "upgrade_account"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lfyw;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAppName can\'t find a package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/widget/LinearLayout;)Ljava/util/List;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v4, v0, Lfye;

    if-eqz v4, :cond_1

    check-cast v0, Lfye;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    instance-of v4, v0, Landroid/widget/LinearLayout;

    if-eqz v4, :cond_0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lfyw;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    return-object v2
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 7

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfm;

    :try_start_0
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-boolean v4, p0, Lfyw;->Y:Z

    invoke-static {v2, v1, v0, v4}, Lfye;->a(Landroid/content/Context;ILgfm;Z)Lfye;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, p0, Lfyw;->a:Lfyf;

    invoke-virtual {v2, v0, v4}, Lfye;->a(Lgfm;Lfyf;)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v4, "UpgradeAccount"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "UpgradeAccount"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to add field: type="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lgfm;->u()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lgfm;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    return-object v3
.end method

.method private a(Lfye;)V
    .locals 2

    invoke-virtual {p1}, Lfye;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lfye;->setVisibility(I)V

    :cond_0
    invoke-virtual {p1}, Lfye;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfyw;->a:Lfyf;

    invoke-virtual {p1}, Lfye;->g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v1

    invoke-interface {v0, v1}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lfyw;)I
    .locals 1

    iget v0, p0, Lfyw;->aa:I

    return v0
.end method


# virtual methods
.method public final J()V
    .locals 5

    const/4 v1, 0x1

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "Next button update requested but form view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v2, p0, Lfyw;->c:Landroid/widget/Button;

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lfyw;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfye;

    invoke-virtual {v0}, Lfye;->b()Z

    move-result v4

    if-nez v4, :cond_2

    const-string v1, "UpgradeAccount"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "UpgradeAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid field: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_2
    invoke-virtual {p0}, Lfyw;->a()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2
.end method

.method public final M_()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    iput-object v0, p0, Lfyw;->e:Lfyy;

    iput-object v0, p0, Lfyw;->a:Lfyf;

    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "is_setup_wizard_theme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfyw;->Y:Z

    invoke-virtual {p0}, Lfyw;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01a7    # com.google.android.gms.R.dimen.plus_oob_wide_ux_max_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v1}, Lo;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {v1}, Lo;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    if-lez v0, :cond_4

    const/4 v4, -0x2

    if-ne v2, v4, :cond_4

    iget-boolean v2, p0, Lfyw;->Y:Z

    if-nez v2, :cond_4

    iput-boolean v5, p0, Lfyw;->ab:Z

    invoke-virtual {v1}, Lo;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v2, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    invoke-virtual {v1}, Lo;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget v2, v4, Landroid/graphics/Point;->x:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/view/Window;->setLayout(II)V

    :goto_0
    const v0, 0x7f0400f9    # com.google.android.gms.R.layout.plus_oob_upgrade_account_form_fragment

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a02b0    # com.google.android.gms.R.id.form_layout

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    const v0, 0x7f0a00be    # com.google.android.gms.R.id.scroll_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    iput-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lboh;)V

    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a(Lbog;)V

    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lfyx;

    invoke-direct {v2, p0}, Lfyx;-><init>(Lfyw;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-boolean v0, p0, Lfyw;->Y:Z

    invoke-static {p1, v1, v0}, Lfzc;->a(Landroid/view/LayoutInflater;Landroid/view/View;Z)V

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfyw;->f:Landroid/widget/TextView;

    iget-boolean v0, p0, Lfyw;->Y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "promo_app_package"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v2}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v0, 0x7f0a02ad    # com.google.android.gms.R.id.title_icon

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const v0, 0x7f0a02ab    # com.google.android.gms.R.id.promo_layout

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfyw;->h:Landroid/view/View;

    iget-boolean v0, p0, Lfyw;->Y:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfyw;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_2
    invoke-virtual {p0, p1, v1}, Lfyw;->a(Landroid/view/LayoutInflater;Landroid/view/View;)V

    const v0, 0x7f0a00a5    # com.google.android.gms.R.id.next_button

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f0a00aa    # com.google.android.gms.R.id.back_button

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    iget-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "back_button_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyw;->Z:Ljava/lang/String;

    iget-object v0, p0, Lfyw;->Z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfyw;->j()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyw;->Z:Ljava/lang/String;

    :cond_2
    iget-boolean v0, p0, Lfyw;->Y:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lfyw;->Z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    iget-object v2, p0, Lfyw;->Z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    :cond_3
    :goto_3
    return-object v1

    :cond_4
    iput-boolean v6, p0, Lfyw;->ab:Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v2, "promo_app_text"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0a02ac    # com.google.android.gms.R.id.promo_text_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0, v3}, Lfyw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lfyw;->j()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b03e9    # com.google.android.gms.R.string.plus_oob_3p_promo

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3

    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method protected a()V
    .locals 2

    iget-boolean v0, p0, Lfyw;->Y:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-static {v0}, Lfzd;->c(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    const v1, 0x7f0b03f2    # com.google.android.gms.R.string.plus_oob_tos_accept

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    const v1, 0x7f0b03f0    # com.google.android.gms.R.string.plus_oob_join

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    const v1, 0x7f0b03f1    # com.google.android.gms.R.string.plus_oob_next

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lfyy;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lfyy;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lfyy;

    iput-object v0, p0, Lfyw;->e:Lfyy;

    instance-of v0, p1, Lfyf;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lfyf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast p1, Lfyf;

    iput-object p1, p0, Lfyw;->a:Lfyf;

    return-void
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "is_setup_wizard_theme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0400e0    # com.google.android.gms.R.layout.plus_oob_buttons_setup_wizard

    move v1, v0

    :goto_0
    const v0, 0x7f0a02b1    # com.google.android.gms.R.id.buttons_layout

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    return-void

    :cond_0
    const v0, 0x7f0400df    # com.google.android.gms.R.layout.plus_oob_buttons

    move v1, v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 12

    const/4 v4, 0x0

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "Form build requested but view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lfyw;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    move v1, v4

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfye;

    iget-boolean v2, p0, Lfyw;->ab:Z

    if-eqz v2, :cond_2

    instance-of v2, v0, Lcom/google/android/gms/plus/oob/FieldViewName;

    if-eqz v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/plus/oob/FieldViewGender;

    if-eqz v2, :cond_2

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfye;

    iget-object v7, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/plus/oob/FieldViewName;

    move-object v3, v1

    check-cast v3, Lcom/google/android/gms/plus/oob/FieldViewGender;

    new-instance v8, Landroid/widget/LinearLayout;

    iget-object v9, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v8, v9}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/view/ViewGroup$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x2

    invoke-direct {v9, v10, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v9, 0x50

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/oob/FieldViewName;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v9, 0x3f800000    # 1.0f

    iput v9, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v8, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/oob/FieldViewGender;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x0

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lfyw;->a(Lfye;)V

    invoke-direct {p0, v1}, Lfyw;->a(Lfye;)V

    move v1, v5

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lfyw;->a(Lfye;)V

    goto :goto_2

    :cond_3
    invoke-static {p1}, Lfzd;->c(Lgll;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfyw;->f:Landroid/widget/TextView;

    const v1, 0x7f0b03ef    # com.google.android.gms.R.string.plus_oob_title_tos

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfyw;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    invoke-virtual {p0}, Lfyw;->J()V

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 0

    invoke-virtual {p0}, Lfyw;->b()V

    invoke-virtual {p0}, Lfyw;->J()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "upgrade_account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object v0, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    const/4 v0, 0x0

    iput v0, p0, Lfyw;->aa:I

    :goto_0
    return-void

    :cond_0
    const-string v0, "state_upgrade_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iput-object v0, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    const-string v0, "state_scroll_y"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lfyw;->aa:I

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    iget-boolean v0, p0, Lfyw;->Y:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfyw;->d:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c_(I)V
    .locals 0

    iput p1, p0, Lfyw;->aa:I

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-virtual {p0, v0}, Lfyw;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "state_upgrade_account"

    iget-object v1, p0, Lfyw;->i:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "state_scroll_y"

    iget v1, p0, Lfyw;->aa:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, p1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfyw;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    const-string v0, "UpgradeAccount"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UpgradeAccount"

    const-string v1, "FieldView values requested but form view has not yet been created"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-static {v0}, Lfzd;->c(Lgll;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfyw;->a:Lfyf;

    sget-object v2, Lbcq;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lbcr;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v1, v2, v3}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    iget-object v1, p0, Lfyw;->e:Lfyy;

    invoke-interface {v1, v0}, Lfyy;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lfyw;->b:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lfyw;->a(Landroid/widget/LinearLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfye;

    invoke-virtual {v0}, Lfye;->c()Lgfm;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    new-instance v2, Lglm;

    invoke-direct {v2}, Lglm;-><init>()V

    const-string v0, "upgrade"

    iput-object v0, v2, Lglm;->c:Ljava/lang/String;

    iget-object v0, v2, Lglm;->d:Ljava/util/Set;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v3, Lglq;

    invoke-direct {v3}, Lglq;-><init>()V

    iput-object v1, v3, Lglq;->a:Ljava/util/List;

    iget-object v0, v3, Lglq;->b:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v1, v3, Lglq;->b:Ljava/util/Set;

    iget-object v3, v3, Lglq;->a:Ljava/util/List;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;-><init>(Ljava/util/Set;Ljava/util/List;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iput-object v0, v2, Lglm;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v0, v2, Lglm;->d:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    iget-object v1, v2, Lglm;->d:Ljava/util/Set;

    iget-object v3, v2, Lglm;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    iget-object v4, v2, Lglm;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;

    iget-object v2, v2, Lglm;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v3, v4, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$FormEntity;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lfyw;->g:Lcom/google/android/gms/common/ui/ScrollViewWithEvents;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/ScrollViewWithEvents;->pageScroll(I)Z

    iget-object v0, p0, Lfyw;->a:Lfyf;

    sget-object v1, Lbcq;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lfyw;->a:Lfyf;

    sget-object v1, Lbcq;->w:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1}, Lfyf;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lfyw;->e:Lfyy;

    invoke-interface {v0}, Lfyy;->e()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a00a5 -> :sswitch_0    # com.google.android.gms.R.id.next_button
        0x7f0a00aa -> :sswitch_1    # com.google.android.gms.R.id.back_button
    .end sparse-switch
.end method
