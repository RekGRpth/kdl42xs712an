.class public abstract Lizk;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Lizg;)Lizk;
.end method

.method public a([BI)Lizk;
    .locals 2

    :try_start_0
    new-instance v0, Lizg;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lizg;-><init>([BII)V

    invoke-virtual {p0, v0}, Lizk;->a(Lizg;)Lizk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lizg;->a(I)V
    :try_end_0
    .catch Lizj; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a(Lizh;)V
.end method

.method public abstract b()I
.end method

.method public final d()[B
    .locals 4

    invoke-virtual {p0}, Lizk;->b()I

    move-result v0

    new-array v0, v0, [B

    array-length v1, v0

    :try_start_0
    new-instance v2, Lizh;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3, v1}, Lizh;-><init>([BII)V

    invoke-virtual {p0, v2}, Lizk;->a(Lizh;)V

    iget-object v1, v2, Lizh;->c:Ljava/io/OutputStream;

    if-nez v1, :cond_0

    iget v1, v2, Lizh;->a:I

    iget v2, v2, Lizh;->b:I

    sub-int/2addr v1, v2

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    return-object v0
.end method
