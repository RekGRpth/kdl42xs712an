.class public abstract Lgar;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lfrx;

.field private final b:Lgat;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lgat;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lgar;->b:Lgat;

    return-void
.end method

.method private static a(Lgas;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "OperationIntentService"

    const-string v1, "Failed operation."

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :try_start_0
    invoke-interface {p0, p1}, Lgas;->a(Ljava/lang/Exception;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lgas;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lgar;->a:Lfrx;

    invoke-interface {p1, p0, v0}, Lgas;->a(Landroid/content/Context;Lfrx;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {p1, v0}, Lgar;->a(Lgas;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {p1, v0}, Lgar;->a(Lgas;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {p1, v0}, Lgar;->a(Lgas;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lfrx;->a(Landroid/content/Context;)Lfrx;

    move-result-object v0

    iput-object v0, p0, Lgar;->a:Lfrx;

    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    iget-object v0, p0, Lgar;->b:Lgat;

    invoke-virtual {v0}, Lgat;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgas;

    invoke-virtual {p0, v0}, Lgar;->a(Lgas;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method
