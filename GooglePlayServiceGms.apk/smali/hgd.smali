.class public final Lhgd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V
    .locals 0

    iput-object p1, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;B)V
    .locals 0

    invoke-direct {p0, p1}, Lhgd;-><init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lhgd;
    .locals 1

    iget-object v0, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b:I

    return-object p0
.end method

.method public final a(Landroid/accounts/Account;)Lhgd;
    .locals 1

    iget-object v0, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c:Landroid/accounts/Account;

    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lhgd;
    .locals 1

    iget-object v0, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d:Landroid/os/Bundle;

    return-object p0
.end method

.method public final a(Z)Lhgd;
    .locals 1

    iget-object v0, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e:Z

    return-object p0
.end method

.method public final b(I)Lhgd;
    .locals 1

    iget-object v0, p0, Lhgd;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f:I

    return-object p0
.end method
