.class public final Ldax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/os/IBinder;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method private constructor <init>(ILandroid/os/IBinder;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Ldax;->c:I

    iput v1, p0, Ldax;->d:I

    iput v1, p0, Ldax;->e:I

    iput v1, p0, Ldax;->f:I

    iput v1, p0, Ldax;->g:I

    iput p1, p0, Ldax;->b:I

    iput-object p2, p0, Ldax;->a:Landroid/os/IBinder;

    return-void
.end method

.method synthetic constructor <init>(ILandroid/os/IBinder;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ldax;-><init>(ILandroid/os/IBinder;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;Landroid/os/IBinder;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Ldax;->c:I

    iput v1, p0, Ldax;->d:I

    iput v1, p0, Ldax;->e:I

    iput v1, p0, Ldax;->f:I

    iput v1, p0, Ldax;->g:I

    iput-object p2, p0, Ldax;->a:Landroid/os/IBinder;

    const-string v0, "popupLocationInfo.gravity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->b:I

    const-string v0, "popupLocationInfo.displayId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->c:I

    const-string v0, "popupLocationInfo.left"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->d:I

    const-string v0, "popupLocationInfo.top"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->e:I

    const-string v0, "popupLocationInfo.right"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->f:I

    const-string v0, "popupLocationInfo.bottom"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldax;->g:I

    return-void
.end method
