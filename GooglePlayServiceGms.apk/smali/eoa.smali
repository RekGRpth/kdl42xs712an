.class public final Leoa;
.super Leog;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)V
    .locals 0

    iput-object p1, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-direct {p0, p1}, Leog;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Leoa;-><init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)V

    return-void
.end method

.method private static varargs b(Lahj;)Ljava/lang/Void;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lahj;->c:Lajd;

    invoke-virtual {v0}, Lajd;->e()Laje;

    move-result-object v0

    invoke-interface {v0}, Laje;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "AppDataSearchClient"

    const-string v2, "TriggerCompaction failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(Lahj;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Leoa;->b(Lahj;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 2

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b01e8    # com.google.android.gms.R.string.icing_storage_managment_reclaim_button_label

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->c(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0b01eb    # com.google.android.gms.R.string.icing_storage_managment_empty_list

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Lenw;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lenw;->cancel(Z)Z

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    new-instance v1, Lenw;

    iget-object v2, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-direct {v1, v2, v3}, Lenw;-><init>(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;Z)V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;Lenw;)Lenw;

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->b(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Lenw;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lenw;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Leoa;->a:Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;->a(Lcom/google/android/gms/icing/ui/IcingManageSpaceActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f0b01e9    # com.google.android.gms.R.string.icing_storage_managment_reclaiming

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    return-void
.end method
