.class public Lbme;
.super Ltf;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/Class;

.field private final g:Ljava/lang/Object;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 11

    if-nez p3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lbme;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-void

    :cond_0
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 7

    if-nez p3, :cond_1

    const/4 v4, 0x0

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v1 .. v6}, Ltf;-><init>(ILjava/lang/String;Ljava/lang/String;Lsk;Lsj;)V

    iput-object p4, p0, Lbme;->f:Ljava/lang/Class;

    iput-object p5, p0, Lbme;->g:Ljava/lang/Object;

    iput-object p8, p0, Lbme;->h:Ljava/lang/String;

    move-object/from16 v0, p10

    iput-object v0, p0, Lbme;->i:Ljava/util/HashMap;

    iget-object v1, p0, Lbme;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbme;->i:Ljava/util/HashMap;

    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OAuth "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lbme;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lbme;->i:Ljava/util/HashMap;

    const-string v2, "Accept-Encoding"

    const-string v3, "gzip"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v0, p9

    iput-boolean v0, p0, Lsc;->c:Z

    return-void

    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 11

    const/4 v1, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lbme;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method protected final a(Lrz;)Lsi;
    .locals 5

    :try_start_0
    iget-boolean v0, p0, Lsc;->c:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Ltb;->a(Lrz;)Lrp;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lbme;->g:Ljava/lang/Object;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbme;->f:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    :goto_1
    iget v2, p1, Lrz;->a:I

    iget-object v3, p1, Lrz;->b:[B

    iput v2, v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;->b:I

    iput-object v3, v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;->c:[B

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;->d:Z

    invoke-static {v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->a([B)Ljava/io/InputStream;
    :try_end_0
    .catch Lbnu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v2

    :try_start_1
    new-instance v3, Lbnl;

    invoke-direct {v3}, Lbnl;-><init>()V

    invoke-virtual {v3, v2, v0}, Lbnl;->a(Ljava/io/InputStream;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Lbnu; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    invoke-static {v0, v1}, Lsi;->a(Ljava/lang/Object;Lrp;)Lsi;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbme;->f:Ljava/lang/Class;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v4, p0, Lbme;->g:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbme;->g:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_3
    .catch Lbnu; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Lbnu; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_4
    :try_start_5
    throw v0
    :try_end_5
    .catch Lbnu; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_0
    move-exception v0

    new-instance v1, Lsb;

    invoke-direct {v1, v0}, Lsb;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lsi;->a(Lsp;)Lsi;

    move-result-object v0

    goto :goto_3

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_5
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_6
    move-exception v2

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_4
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 0

    invoke-super {p0, p1}, Ltf;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    invoke-super {p0, p1}, Ltf;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lbme;->i:Ljava/util/HashMap;

    return-object v0
.end method
