.class public final Lbew;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbeu;
.implements Lbev;
.implements Lbey;
.implements Lbez;
.implements Lbfb;
.implements Lbfc;


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lbew;->a:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lbew;-><init>(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Ljava/util/List;
    .locals 1

    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lbew;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_1
    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method

.method private static f(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 1

    instance-of v0, p0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/util/ArrayList;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object p0, v0

    goto :goto_0
.end method

.method private m(Ljava/lang/String;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "DESCRIPTION_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/people/data/Audience;)Lbeu;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbew;->c(Ljava/util/List;)Lbew;

    return-object p0
.end method

.method public final synthetic a(Ljava/lang/String;)Lbeu;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->e(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Ljava/util/List;)Lbeu;
    .locals 0

    invoke-virtual {p0, p1}, Lbew;->c(Ljava/util/List;)Lbew;

    return-object p0
.end method

.method public final a(I)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Ljava/util/ArrayList;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public final a(Z)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "EVERYONE_CHECKED"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic b(Ljava/lang/String;)Lbeu;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->f(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/util/List;)Lbeu;
    .locals 3

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "INITIAL_ACL"

    invoke-static {p1}, Lbew;->f(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public final b(Ljava/util/ArrayList;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic b(Z)Lbfb;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->a(Z)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    invoke-static {v0}, Lbew;->b(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/String;)Lbeu;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->g(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/util/List;)Lbew;
    .locals 3

    if-nez p1, :cond_0

    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_INITIAL_AUDIENCE"

    invoke-static {p1}, Lbew;->f(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    return-object p0
.end method

.method public final c()Z
    .locals 3

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "EVERYONE_CHECKED"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final synthetic d(Ljava/lang/String;)Lbeu;
    .locals 1

    invoke-direct {p0, p1}, Lbew;->m(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d(Ljava/util/List;)Lbez;
    .locals 0

    invoke-virtual {p0, p1}, Lbew;->c(Ljava/util/List;)Lbew;

    return-object p0
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ADDED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic e(Ljava/util/List;)Lbfb;
    .locals 0

    invoke-virtual {p0, p1}, Lbew;->c(Ljava/util/List;)Lbew;

    return-object p0
.end method

.method public final e()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_REMOVED_AUDIENCE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final f()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    invoke-static {v0}, Lbew;->b(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lbew;
    .locals 2

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic h(Ljava/lang/String;)Lbez;
    .locals 2

    const-string v0, "People qualified ID"

    invoke-static {p1, v0}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbew;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final synthetic i(Ljava/lang/String;)Lbez;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->g(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic j(Ljava/lang/String;)Lbez;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->e(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k(Ljava/lang/String;)Lbfb;
    .locals 1

    invoke-direct {p0, p1}, Lbew;->m(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l(Ljava/lang/String;)Lbfb;
    .locals 1

    invoke-virtual {p0, p1}, Lbew;->e(Ljava/lang/String;)Lbew;

    move-result-object v0

    return-object v0
.end method
