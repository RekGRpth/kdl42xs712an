.class public final Ldrm;
.super Ldan;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ldan;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbfy;->a(Landroid/content/Context;)V

    iput-object p1, p0, Ldrm;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    :try_start_0
    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p1, p2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "GamesSignInService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a()V
    .locals 2

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lduj;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lduj;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lbov;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ldaj;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ldaj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "game package name is missing"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "package name is missing"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "scopes are missing"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2, p3, p4}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1, v0, v1}, Ldaj;->a(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "player ID is missing"

    invoke-static {p5, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2, p3, p4}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p1, v0}, Ldaj;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p5}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "account name is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "game ID is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldrm;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ldaj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "ACL data is missing"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcum;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Ldrm;->a()V

    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "package name is missing"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "scopes are missing"

    invoke-static {p4, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p2, p3, p4}, Ldrm;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Ldaj;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldrm;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ldaj;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
