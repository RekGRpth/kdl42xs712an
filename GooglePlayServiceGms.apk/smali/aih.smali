.class public final Laih;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public final c:Ljava/util/List;

.field public d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

.field public e:Z

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laih;->f:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laih;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 7

    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v1, p0, Laih;->f:Ljava/lang/String;

    iget-object v2, p0, Laih;->a:Ljava/lang/String;

    iget-object v3, p0, Laih;->b:Landroid/net/Uri;

    iget-object v4, p0, Laih;->c:Ljava/util/List;

    iget-object v5, p0, Laih;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v5, p0, Laih;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v6, p0, Laih;->e:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;Z)V

    return-object v0
.end method
