.class public final Ldjs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldjt;

.field public b:Ldju;

.field public volatile c:Ljava/util/concurrent/CountDownLatch;

.field public volatile d:Ljava/util/concurrent/CountDownLatch;

.field public e:I

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldjs;->f:Ljava/lang/String;

    new-instance v0, Ldjt;

    iget-object v1, p0, Ldjs;->f:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Ldjt;-><init>(Ldjs;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ldjs;->a:Ldjt;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "games.data_store_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldjs;->g:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    const/4 v0, 0x0

    iput v0, p0, Ldjs;->e:I

    invoke-static {p1, p2}, Lcto;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/util/concurrent/CountDownLatch;)V
    .locals 1

    if-nez p0, :cond_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    iget-object v0, p0, Ldjs;->g:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    if-nez v2, :cond_0

    iget-object v2, p0, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Ldjs;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Ldju;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    iget v0, p0, Ldjs;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldjs;->e:I

    move v0, v1

    goto :goto_1

    :cond_2
    new-instance v3, Ldju;

    iget-object v4, p0, Ldjs;->a:Ldjt;

    invoke-direct {v3, v2, v4}, Ldju;-><init>(Ljava/io/File;Ldjt;)V

    iput-object v3, p0, Ldjs;->b:Ldju;

    iget-object v2, p0, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iput-object v5, p0, Ldjs;->c:Ljava/util/concurrent/CountDownLatch;

    iget-object v2, p0, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iput-object v5, p0, Ldjs;->d:Ljava/util/concurrent/CountDownLatch;

    iput v1, p0, Ldjs;->e:I

    goto :goto_1
.end method
