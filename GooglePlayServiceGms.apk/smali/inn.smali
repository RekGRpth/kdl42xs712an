.class public final Linn;
.super Lizk;
.source "SourceFile"


# instance fields
.field public a:Linf;

.field public b:Lini;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/util/List;

.field private i:Z

.field private j:I

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Z

.field private o:J

.field private p:Z

.field private q:J

.field private r:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Linn;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Linn;->f:Ljava/lang/String;

    iput-object v2, p0, Linn;->a:Linf;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Linn;->h:Ljava/util/List;

    iput v1, p0, Linn;->j:I

    iput v1, p0, Linn;->l:I

    iput-object v2, p0, Linn;->b:Lini;

    iput-wide v3, p0, Linn;->o:J

    iput-wide v3, p0, Linn;->q:J

    const/4 v0, -0x1

    iput v0, p0, Linn;->r:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Linn;->r:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Linn;->b()I

    :cond_0
    iget v0, p0, Linn;->r:I

    return v0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 3

    const/4 v2, 0x1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Linn;->c:Z

    iput-object v0, p0, Linn;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Linn;->e:Z

    iput-object v0, p0, Linn;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    new-instance v0, Linf;

    invoke-direct {v0}, Linf;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Linn;->g:Z

    iput-object v0, p0, Linn;->a:Linf;

    goto :goto_0

    :sswitch_4
    new-instance v0, Linp;

    invoke-direct {v0}, Linp;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iget-object v1, p0, Linn;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Linn;->h:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Linn;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Linn;->i:Z

    iput v0, p0, Linn;->j:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->c()I

    move-result v0

    iput-boolean v2, p0, Linn;->k:Z

    iput v0, p0, Linn;->l:I

    goto :goto_0

    :sswitch_7
    new-instance v0, Lini;

    invoke-direct {v0}, Lini;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    iput-boolean v2, p0, Linn;->m:Z

    iput-object v0, p0, Linn;->b:Lini;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Linn;->n:Z

    iput-wide v0, p0, Linn;->o:J

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    iput-boolean v2, p0, Linn;->p:Z

    iput-wide v0, p0, Linn;->q:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Linn;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Linn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Linn;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Linn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Linn;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Linn;->a:Linf;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_2
    iget-object v0, p0, Linn;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linp;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Linn;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Linn;->j:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_4
    iget-boolean v0, p0, Linn;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Linn;->l:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_5
    iget-boolean v0, p0, Linn;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Linn;->b:Lini;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_6
    iget-boolean v0, p0, Linn;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-wide v1, p0, Linn;->o:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_7
    iget-boolean v0, p0, Linn;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-wide v1, p0, Linn;->q:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_8
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Linn;->c:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Linn;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lizh;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Linn;->e:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Linn;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Linn;->g:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Linn;->a:Linf;

    invoke-static {v1, v2}, Lizh;->b(ILizk;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Linn;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linp;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Linn;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v2, p0, Linn;->j:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_4
    iget-boolean v0, p0, Linn;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v2, p0, Linn;->l:I

    invoke-static {v0, v2}, Lizh;->c(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_5
    iget-boolean v0, p0, Linn;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v2, p0, Linn;->b:Lini;

    invoke-static {v0, v2}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_6
    iget-boolean v0, p0, Linn;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-wide v2, p0, Linn;->o:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_7
    iget-boolean v0, p0, Linn;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-wide v2, p0, Linn;->q:J

    invoke-static {v0, v2, v3}, Lizh;->c(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_8
    iput v1, p0, Linn;->r:I

    return v1
.end method
