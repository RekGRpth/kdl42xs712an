.class public final Ldxv;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Ldfz;
.implements Ldgp;
.implements Ldws;
.implements Ledt;


# instance fields
.field private ad:Leae;

.field private ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

.field private af:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 3

    invoke-virtual {p0}, Ldxv;->J()Lbdu;

    move-result-object v1

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    iget-object v2, p0, Ldxv;->Y:Ldvn;

    invoke-virtual {v2}, Ldvn;->l()Z

    move-result v2

    invoke-static {v1, v0, v2}, Leee;->a(Lbdu;Landroid/app/Activity;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    iget-object v0, p0, Ldxv;->ad:Leae;

    invoke-virtual {v0, v1}, Leae;->a(Lbdu;)V

    goto :goto_0
.end method


# virtual methods
.method public final A_()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method public final B_()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method public final C_()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method public final M_()V
    .locals 2

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Ldxv;->T()V

    invoke-virtual {p0}, Ldxv;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ClientInboxListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Ldxv;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->i:Ldfx;

    invoke-interface {v1, v0}, Ldfx;->a(Lbdu;)V

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0}, Ldgs;->a(Lbdu;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method protected final U()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Ldxe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0163    # com.google.android.gms.R.id.quick_access_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iput-object v0, p0, Ldxv;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Ldxv;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a()V

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Ldxv;->af:Landroid/widget/ListView;

    iget-object v0, p0, Ldxv;->Z:Leds;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Leds;->a(I)V

    return-object v1
.end method

.method public final a(Lbdu;)V
    .locals 1

    iget-object v0, p0, Ldxv;->ad:Leae;

    invoke-virtual {v0, p1}, Leae;->a(Lbdu;)V

    invoke-virtual {p0}, Ldxv;->S()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Ldxv;->ad:Leae;

    invoke-virtual {v0, p1, p2, p3}, Leae;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;ZLjava/util/ArrayList;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/matches/ClientInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Ldwt;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Ldzy;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    check-cast v0, Ldzy;

    invoke-interface {v0}, Ldzy;->D_()Ldzx;

    move-result-object v2

    new-instance v0, Leae;

    iget-object v1, p0, Ldxv;->Y:Ldvn;

    iget-object v4, p0, Ldxv;->Y:Ldvn;

    check-cast v4, Ldwt;

    const/4 v6, 0x0

    move-object v3, v2

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Leae;-><init>(Ldvn;Leac;Leag;Ldwt;Ldws;Lebk;)V

    iput-object v0, p0, Ldxv;->ad:Leae;

    iget-object v0, p0, Ldxv;->ad:Leae;

    iget-object v1, p0, Ldxv;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v0, v1}, Leae;->a(Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;)V

    iget-object v1, p0, Ldxv;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    check-cast v0, Ldwt;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Ldwt;)V

    iget-object v0, p0, Ldxv;->ad:Leae;

    invoke-virtual {p0, v0}, Ldxv;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Ldxv;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final e()V
    .locals 0

    invoke-direct {p0}, Ldxv;->V()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Ldxv;->ad:Leae;

    invoke-virtual {v0}, Leae;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method

.method public final y_()V
    .locals 2

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, p0, Ldxv;->ae:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v1, p0, Ldxv;->af:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/ListView;)V

    iget-object v0, p0, Ldxv;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    invoke-virtual {p0}, Ldxv;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->i:Ldfx;

    invoke-interface {v1, v0, p0}, Ldfx;->a(Lbdu;Ldfz;)V

    sget-object v1, Lcte;->j:Ldgs;

    invoke-interface {v1, v0, p0}, Ldgs;->a(Lbdu;Ldgp;)V

    :cond_0
    return-void
.end method

.method public final z_()V
    .locals 2

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldxv;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, p0, Ldxv;->Z:Leds;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Leds;->a(I)V

    return-void
.end method
