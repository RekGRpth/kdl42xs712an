.class public final enum Ldhw;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldhw;

.field public static final enum b:Ldhw;

.field public static final enum c:Ldhw;

.field public static final enum d:Ldhw;

.field public static final enum e:Ldhw;

.field private static final synthetic f:[Ldhw;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ldhw;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2}, Ldhw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldhw;->a:Ldhw;

    new-instance v0, Ldhw;

    const-string v1, "BOOLEAN"

    invoke-direct {v0, v1, v3}, Ldhw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldhw;->b:Ldhw;

    new-instance v0, Ldhw;

    const-string v1, "INTEGER"

    invoke-direct {v0, v1, v4}, Ldhw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldhw;->c:Ldhw;

    new-instance v0, Ldhw;

    const-string v1, "LONG"

    invoke-direct {v0, v1, v5}, Ldhw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldhw;->d:Ldhw;

    new-instance v0, Ldhw;

    const-string v1, "BLOB"

    invoke-direct {v0, v1, v6}, Ldhw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldhw;->e:Ldhw;

    const/4 v0, 0x5

    new-array v0, v0, [Ldhw;

    sget-object v1, Ldhw;->a:Ldhw;

    aput-object v1, v0, v2

    sget-object v1, Ldhw;->b:Ldhw;

    aput-object v1, v0, v3

    sget-object v1, Ldhw;->c:Ldhw;

    aput-object v1, v0, v4

    sget-object v1, Ldhw;->d:Ldhw;

    aput-object v1, v0, v5

    sget-object v1, Ldhw;->e:Ldhw;

    aput-object v1, v0, v6

    sput-object v0, Ldhw;->f:[Ldhw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldhw;
    .locals 1

    const-class v0, Ldhw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldhw;

    return-object v0
.end method

.method public static values()[Ldhw;
    .locals 1

    sget-object v0, Ldhw;->f:[Ldhw;

    invoke-virtual {v0}, [Ldhw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldhw;

    return-object v0
.end method
