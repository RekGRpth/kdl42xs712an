.class public final Lezx;
.super Landroid/opengl/GLSurfaceView;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:F

.field private c:F

.field private d:Z

.field private e:Landroid/graphics/PointF;

.field private f:J

.field private g:Z

.field private h:Lfaa;

.field private i:Lfac;

.field private j:Z

.field private k:Landroid/graphics/PointF;

.field private l:Lezl;

.field private m:Leyj;

.field private n:Leyj;

.field private o:Leyj;

.field private p:Leyj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lezw;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lezx;-><init>(Landroid/content/Context;Lezw;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lezw;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lezx;->g:Z

    new-instance v0, Lfac;

    invoke-direct {v0}, Lfac;-><init>()V

    iput-object v0, p0, Lezx;->i:Lfac;

    iput-boolean v2, p0, Lezx;->j:Z

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lezx;->k:Landroid/graphics/PointF;

    new-instance v0, Leyj;

    invoke-direct {v0}, Leyj;-><init>()V

    iput-object v0, p0, Lezx;->m:Leyj;

    new-instance v0, Leyj;

    invoke-direct {v0}, Leyj;-><init>()V

    iput-object v0, p0, Lezx;->n:Leyj;

    new-instance v0, Leyj;

    invoke-direct {v0}, Leyj;-><init>()V

    iput-object v0, p0, Lezx;->o:Leyj;

    new-instance v0, Leyj;

    invoke-direct {v0}, Leyj;-><init>()V

    iput-object v0, p0, Lezx;->p:Leyj;

    :try_start_0
    new-instance v0, Lfaa;

    const v1, 0x7f0201e5    # com.google.android.gms.R.drawable.panorama_cubemap

    invoke-direct {v0, p0, p2, p1, v1}, Lfaa;-><init>(Lezx;Lezw;Landroid/content/Context;I)V

    iput-object v0, p0, Lezx;->h:Lfaa;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lezx;->h:Lfaa;

    new-instance v1, Lezy;

    invoke-direct {v1, p0}, Lezy;-><init>(Lezx;)V

    invoke-virtual {v0, v1}, Lfaa;->b(Lezl;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lezx;->setEGLContextClientVersion(I)V

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {p0, v0}, Lezx;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    invoke-virtual {p0, v2}, Lezx;->setRenderMode(I)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, "PanoramaView"

    const-string v1, "Error creating Panorama view renderer."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lezx;)Lfaa;
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    return-object v0
.end method

.method private static b(Landroid/view/MotionEvent;)F
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v2, :cond_0

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lezx;->i:Lfac;

    iget-object v1, p0, Lezx;->k:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Lfac;->a(Landroid/graphics/PointF;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lezx;->h:Lfaa;

    iget-object v1, p0, Lezx;->k:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lezx;->k:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lezx;->o:Leyj;

    invoke-virtual {v0, v1, v2, v3}, Lfaa;->a(FFLeyj;)V

    iget-object v0, p0, Lezx;->n:Leyj;

    iget v0, v0, Leyj;->a:F

    iget-object v1, p0, Lezx;->o:Leyj;

    iget v1, v1, Leyj;->a:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lezx;->n:Leyj;

    iget v1, v1, Leyj;->b:F

    iget-object v2, p0, Lezx;->o:Leyj;

    iget v2, v2, Leyj;->b:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lezx;->h:Lfaa;

    iget-object v3, p0, Lezx;->h:Lfaa;

    invoke-virtual {v3}, Lfaa;->e()F

    move-result v3

    sub-float v0, v3, v0

    iget-object v3, p0, Lezx;->h:Lfaa;

    invoke-virtual {v3}, Lfaa;->d()F

    move-result v3

    sub-float v1, v3, v1

    invoke-virtual {v2, v0, v1}, Lfaa;->a(FF)V

    invoke-virtual {p0}, Lezx;->requestRender()V

    goto :goto_0
.end method

.method public final a(Lezd;Lezf;)V
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {v0, p1, p2}, Lfaa;->a(Lezd;Lezf;)V

    new-instance v0, Lezz;

    invoke-direct {v0, p0}, Lezz;-><init>(Lezx;)V

    iput-object v0, p2, Lezf;->n:Lezl;

    return-void
.end method

.method public final a(Lezl;)V
    .locals 0

    iput-object p1, p0, Lezx;->l:Lezl;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {v0, p1}, Lfaa;->a(Z)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    const/high16 v12, 0x447a0000    # 1000.0f

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    const/4 v9, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :cond_0
    :goto_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lezx;->l:Lezl;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lezx;->l:Lezl;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lezl;->a(Ljava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lezx;->i:Lfac;

    invoke-virtual {v1}, Lfac;->a()V

    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, p0, Lezx;->e:Landroid/graphics/PointF;

    iget-object v1, p0, Lezx;->h:Lfaa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lezx;->m:Leyj;

    invoke-virtual {v1, v2, v3, v4}, Lfaa;->a(FFLeyj;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    iput-wide v1, p0, Lezx;->f:J

    iget-object v1, p0, Lezx;->i:Lfac;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, v1, Lfac;->a:J

    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v4, v1, Lfac;->b:Landroid/graphics/PointF;

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lezx;->b(Landroid/view/MotionEvent;)F

    move-result v1

    iput v1, p0, Lezx;->b:F

    iput-boolean v0, p0, Lezx;->a:Z

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v2, v0, :cond_2

    iget-object v2, p0, Lezx;->i:Lfac;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-wide v7, v2, Lfac;->a:J

    sub-long v7, v5, v7

    const-wide/16 v9, 0x32

    cmp-long v9, v7, v9

    if-ltz v9, :cond_2

    iget-object v9, v2, Lfac;->c:Landroid/graphics/PointF;

    iget-object v10, v2, Lfac;->b:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float v10, v3, v10

    long-to-float v11, v7

    div-float v11, v12, v11

    mul-float/2addr v10, v11

    iput v10, v9, Landroid/graphics/PointF;->x:F

    iget-object v9, v2, Lfac;->c:Landroid/graphics/PointF;

    iget-object v10, v2, Lfac;->b:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    sub-float v10, v4, v10

    long-to-float v7, v7

    div-float v7, v12, v7

    mul-float/2addr v7, v10

    iput v7, v9, Landroid/graphics/PointF;->y:F

    iput-wide v5, v2, Lfac;->a:J

    iget-object v5, v2, Lfac;->b:Landroid/graphics/PointF;

    iput v3, v5, Landroid/graphics/PointF;->x:F

    iget-object v2, v2, Lfac;->b:Landroid/graphics/PointF;

    iput v4, v2, Landroid/graphics/PointF;->y:F

    :cond_2
    iget-boolean v2, p0, Lezx;->d:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lezx;->h:Lfaa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lezx;->m:Leyj;

    invoke-virtual {v2, v3, v4, v5}, Lfaa;->a(FFLeyj;)V

    iput-boolean v1, p0, Lezx;->d:Z

    :cond_3
    iget-boolean v1, p0, Lezx;->a:Z

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v0, :cond_4

    invoke-static {p1}, Lezx;->b(Landroid/view/MotionEvent;)F

    move-result v1

    iput v1, p0, Lezx;->c:F

    iget v1, p0, Lezx;->c:F

    iget v2, p0, Lezx;->b:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lezx;->h:Lfaa;

    invoke-virtual {v2, v1}, Lfaa;->a(F)V

    invoke-virtual {p0}, Lezx;->requestRender()V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lezx;->h:Lfaa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lezx;->p:Leyj;

    invoke-virtual {v1, v2, v3, v4}, Lfaa;->a(FFLeyj;)V

    iget-object v1, p0, Lezx;->m:Leyj;

    iget v1, v1, Leyj;->a:F

    iget-object v2, p0, Lezx;->p:Leyj;

    iget v2, v2, Leyj;->a:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lezx;->m:Leyj;

    iget v2, v2, Leyj;->b:F

    iget-object v3, p0, Lezx;->p:Leyj;

    iget v3, v3, Leyj;->b:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lezx;->e:Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float/2addr v4, v5

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v3, v5

    float-to-double v4, v4

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v3

    const-wide/high16 v5, 0x4010000000000000L    # 4.0

    cmpl-double v3, v3, v5

    if-ltz v3, :cond_0

    iget-object v3, p0, Lezx;->h:Lfaa;

    iget-object v4, p0, Lezx;->h:Lfaa;

    invoke-virtual {v4}, Lfaa;->e()F

    move-result v4

    sub-float v1, v4, v1

    iget-object v4, p0, Lezx;->h:Lfaa;

    invoke-virtual {v4}, Lfaa;->d()F

    move-result v4

    sub-float v2, v4, v2

    invoke-virtual {v3, v1, v2}, Lfaa;->a(FF)V

    invoke-virtual {p0}, Lezx;->requestRender()V

    goto/16 :goto_0

    :pswitch_4
    iput-boolean v1, p0, Lezx;->a:Z

    iget v1, p0, Lezx;->c:F

    iget v2, p0, Lezx;->b:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lezx;->h:Lfaa;

    invoke-virtual {v2, v1}, Lfaa;->b(F)V

    iput-boolean v0, p0, Lezx;->d:Z

    iput-boolean v0, p0, Lezx;->j:Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v2, p0, Lezx;->e:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lezx;->e:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-double v4, v2

    float-to-double v2, v3

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lezx;->f:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x190

    cmp-long v4, v4, v6

    if-gez v4, :cond_5

    cmpg-double v4, v2, v10

    if-gez v4, :cond_5

    iget-boolean v4, p0, Lezx;->g:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lezx;->h:Lfaa;

    invoke-virtual {v4}, Lfaa;->c()V

    :cond_5
    iput-boolean v1, p0, Lezx;->d:Z

    iget-boolean v4, p0, Lezx;->j:Z

    if-nez v4, :cond_7

    cmpl-double v2, v2, v10

    if-lez v2, :cond_0

    iget-object v2, p0, Lezx;->h:Lfaa;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget-object v5, p0, Lezx;->n:Leyj;

    invoke-virtual {v2, v3, v4, v5}, Lfaa;->a(FFLeyj;)V

    iget-object v2, p0, Lezx;->i:Lfac;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-wide v7, v2, Lfac;->a:J

    sub-long/2addr v5, v7

    const-wide/16 v7, 0xc8

    cmp-long v5, v5, v7

    if-gez v5, :cond_6

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v2, v1}, Lfac;->b(Landroid/graphics/PointF;)Z

    move-result v1

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lezx;->requestRender()V

    goto/16 :goto_0

    :cond_6
    const-wide/16 v3, 0x0

    iput-wide v3, v2, Lfac;->a:J

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v9, v9}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, v2, Lfac;->b:Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v9, v9}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, v2, Lfac;->c:Landroid/graphics/PointF;

    goto :goto_1

    :cond_7
    iput-boolean v1, p0, Lezx;->j:Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {v0}, Lfaa;->b()V

    return-void
.end method

.method public final b(Lezl;)V
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {v0, p1}, Lfaa;->a(Lezl;)V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lezx;->g:Z

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lezx;->h:Lfaa;

    invoke-virtual {v0}, Lfaa;->f()V

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lezx;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
