.class public final Lahf;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V
    .locals 0

    iput-object p1, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lahf;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    new-instance v0, Lcbm;

    iget-object v1, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v1}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v1

    invoke-direct {v0, v1}, Lcbm;-><init>(Lcoy;)V

    invoke-virtual {v0}, Lcbm;->a()Lcbn;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Lcbn;

    iget-object v0, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->d(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    if-nez p1, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-wide v0, p1, Lcbn;->d:J

    goto :goto_0
.end method

.method protected final onPreExecute()V
    .locals 2

    iget-object v0, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v0}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->d(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lahf;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v1}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->a(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
