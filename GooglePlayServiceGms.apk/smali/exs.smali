.class public final Lexs;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/mdm/services/RingService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/mdm/services/RingService;)V
    .locals 0

    iput-object p1, p0, Lexs;->a:Lcom/google/android/gms/mdm/services/RingService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[RingService] Received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lexs;->a:Lcom/google/android/gms/mdm/services/RingService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/RingService;->stopSelf()V

    return-void
.end method
