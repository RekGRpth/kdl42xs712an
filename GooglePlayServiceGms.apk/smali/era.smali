.class public final Lera;
.super Lbje;
.source "SourceFile"


# instance fields
.field private final f:Lerd;

.field private final g:Leqx;

.field private final h:Lesa;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Ljava/lang/String;)V
    .locals 7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lera;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Ljava/lang/String;Ljava/util/Locale;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 7

    const/4 v6, 0x0

    new-array v5, v6, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbje;-><init>(Landroid/content/Context;Landroid/os/Looper;Lbdx;Lbdy;[Ljava/lang/String;)V

    new-instance v0, Lerb;

    invoke-direct {v0, p0, v6}, Lerb;-><init>(Lera;B)V

    iput-object v0, p0, Lera;->f:Lerd;

    new-instance v0, Leqx;

    iget-object v1, p0, Lera;->f:Lerd;

    invoke-direct {v0, p1, v1}, Leqx;-><init>(Landroid/content/Context;Lerd;)V

    iput-object v0, p0, Lera;->g:Leqx;

    iput-object p5, p0, Lera;->i:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lera;->j:Ljava/lang/String;

    new-instance v0, Lesa;

    iget-object v1, p0, Lbje;->b_:Landroid/content/Context;

    iget-object v2, p0, Lera;->f:Lerd;

    invoke-direct {v0, v1, p6, v2}, Lesa;-><init>(Landroid/content/Context;Ljava/util/Locale;Lerd;)V

    iput-object v0, p0, Lera;->h:Lesa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)V
    .locals 4

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lbje;-><init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V

    new-instance v0, Lerb;

    invoke-direct {v0, p0, v1}, Lerb;-><init>(Lera;B)V

    iput-object v0, p0, Lera;->f:Lerd;

    new-instance v0, Leqx;

    iget-object v1, p0, Lera;->f:Lerd;

    invoke-direct {v0, p1, v1}, Leqx;-><init>(Landroid/content/Context;Lerd;)V

    iput-object v0, p0, Lera;->g:Leqx;

    iput-object p4, p0, Lera;->i:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lera;->j:Ljava/lang/String;

    new-instance v0, Lesa;

    iget-object v1, p0, Lbje;->b_:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    iget-object v3, p0, Lera;->f:Lerd;

    invoke-direct {v0, v1, v2, v3}, Lesa;-><init>(Landroid/content/Context;Ljava/util/Locale;Lerd;)V

    iput-object v0, p0, Lera;->h:Lesa;

    return-void
.end method

.method static synthetic a(Lera;)V
    .locals 0

    invoke-virtual {p0}, Lera;->h()V

    return-void
.end method

.method static synthetic b(Lera;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lera;->i()Landroid/os/IInterface;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Leqv;->a(Landroid/os/IBinder;)Lequ;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    iget-object v2, p0, Lera;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x41fe88

    iget-object v2, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjy;->f(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqg;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lera;->a(Lcom/google/android/gms/location/LocationRequest;Leqg;Landroid/os/Looper;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/location/LocationRequest;Leqg;Landroid/os/Looper;)V
    .locals 5

    iget-object v2, p0, Lera;->g:Leqx;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lera;->g:Leqx;

    iget-object v0, v3, Leqx;->a:Lerd;

    invoke-interface {v0}, Lerd;->a()V

    if-nez p3, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    const-string v1, "Can\'t create handler inside thread that has not called Looper.prepare()"

    invoke-static {v0, v1}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v4, v3, Leqx;->e:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v3, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqz;

    if-nez v0, :cond_1

    new-instance v0, Leqz;

    invoke-direct {v0, p2, p3}, Leqz;-><init>(Leqg;Landroid/os/Looper;)V

    move-object v1, v0

    :goto_0
    iget-object v0, v3, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, v3, Leqx;->a:Lerd;

    invoke-interface {v0}, Lerd;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lequ;

    iget-object v3, v3, Leqx;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, p1, v1, v3}, Lequ;->a(Lcom/google/android/gms/location/LocationRequest;Leqc;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v4

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Leqg;)V
    .locals 4

    iget-object v1, p0, Lera;->g:Leqx;

    iget-object v0, v1, Leqx;->a:Lerd;

    invoke-interface {v0}, Lerd;->a()V

    const-string v0, "Invalid null listener"

    invoke-static {p1, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Leqx;->e:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqz;

    iget-object v3, v1, Leqx;->c:Landroid/content/ContentProviderClient;

    if-eqz v3, :cond_0

    iget-object v3, v1, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v1, Leqx;->c:Landroid/content/ContentProviderClient;

    invoke-virtual {v3}, Landroid/content/ContentProviderClient;->release()Z

    const/4 v3, 0x0

    iput-object v3, v1, Leqx;->c:Landroid/content/ContentProviderClient;

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Leqz;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, v1, Leqx;->a:Lerd;

    invoke-interface {v1}, Lerd;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lequ;

    invoke-interface {v1, v0}, Lequ;->a(Leqc;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :try_start_2
    monitor-exit v2

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b()V
    .locals 6

    iget-object v2, p0, Lera;->g:Leqx;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lera;->d_()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v3, p0, Lera;->g:Leqx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v4, v3, Leqx;->e:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v3, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leqz;

    if-eqz v0, :cond_0

    iget-object v1, v3, Leqx;->a:Lerd;

    invoke-interface {v1}, Lerd;->b()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lequ;

    invoke-interface {v1, v0}, Lequ;->a(Leqc;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_5
    iget-object v0, v3, Leqx;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v1, p0, Lera;->g:Leqx;

    iget-boolean v0, v1, Leqx;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Leqx;->a:Lerd;

    invoke-interface {v0}, Lerd;->a()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    iget-object v0, v1, Leqx;->a:Lerd;

    invoke-interface {v0}, Lerd;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lequ;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lequ;->a(Z)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v0, 0x0

    :try_start_8
    iput-boolean v0, v1, Leqx;->d:Z

    :cond_2
    invoke-super {p0}, Lbje;->b()V

    monitor-exit v2

    return-void

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method public final e()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lera;->g:Leqx;

    invoke-virtual {v0}, Leqx;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
