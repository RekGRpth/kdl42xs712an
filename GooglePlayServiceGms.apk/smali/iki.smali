.class public abstract Liki;
.super Ljp;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field protected o:Landroid/widget/CompoundButton;

.field protected p:Liky;

.field protected q:Liiw;

.field protected r:Ljava/lang/String;

.field protected s:Landroid/accounts/Account;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljp;-><init>()V

    return-void
.end method

.method private f()V
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v2, 0x0

    iget-object v0, p0, Liki;->s:Landroid/accounts/Account;

    if-nez v0, :cond_1

    iget-object v0, p0, Liki;->p:Liky;

    invoke-interface {v0}, Liky;->a()[Landroid/accounts/Account;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_6

    aget-object v0, v4, v3

    iget-object v6, p0, Liki;->r:Ljava/lang/String;

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GCoreLocationSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    iput-object v0, p0, Liki;->s:Landroid/accounts/Account;

    :cond_1
    iget-object v0, p0, Liki;->q:Liiw;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Liki;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Liki;->s:Landroid/accounts/Account;

    if-eqz v0, :cond_8

    :try_start_0
    iget-object v0, p0, Liki;->q:Liiw;

    iget-object v1, p0, Liki;->s:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Liiw;->a(Landroid/accounts/Account;)Lcom/google/android/location/reporting/service/AccountConfig;

    move-result-object v1

    const-string v0, "GCoreLocationSettings"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "GCoreLocationSettings"

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v1}, Liki;->a(Lcom/google/android/location/reporting/service/AccountConfig;)I

    move-result v3

    if-lez v3, :cond_7

    const/4 v0, 0x1

    :goto_2
    iget-object v2, p0, Liki;->o:Landroid/widget/CompoundButton;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v2, p0, Liki;->o:Landroid/widget/CompoundButton;

    invoke-virtual {v2, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Liki;->o:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->s()Z

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    iget-object v2, p0, Liki;->o:Landroid/widget/CompoundButton;

    invoke-virtual {v2, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {v1}, Lcom/google/android/location/reporting/service/AccountConfig;->s()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v3, :cond_3

    const/4 v1, -0x2

    if-ne v3, v1, :cond_4

    :cond_3
    invoke-virtual {p0, v0}, Liki;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_3
    return-void

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Liki;->finish()V

    move-object v0, v1

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "GCoreLocationSettings"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_8
    const-string v0, "GCoreLocationSettings"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BaseUserLocationSettingsActivity skipping UI update, svc="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Liki;->q:Liiw;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", finishing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Liki;->isFinishing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", account="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Liki;->s:Landroid/accounts/Account;

    invoke-static {v2}, Lesq;->a(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/location/reporting/service/AccountConfig;)I
.end method

.method protected abstract b(Z)V
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    iget-object v0, p0, Liki;->q:Liiw;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p2}, Liki;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "GCoreLocationSettings"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No service, setting change ignored"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Likf;->a(Ljava/lang/Throwable;)V

    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const/16 v3, 0x10

    const/4 v5, 0x0

    invoke-super {p0, p1}, Ljp;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Liki;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.location.settings.extra.account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liki;->r:Ljava/lang/String;

    iget-object v0, p0, Liki;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Finishing activity: no account name found in the intent. This shouldn\'t happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const-string v1, "GCoreLocationSettings"

    invoke-static {v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Liki;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Likz;

    invoke-direct {v0, p0}, Likz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Liki;->p:Liky;

    const/16 v0, 0xe

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/widget/Switch;

    invoke-direct {v0, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Liki;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d01e0    # com.google.android.gms.R.dimen.location_action_bar_switch_padding

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/Switch;->setPadding(IIII)V

    :goto_1
    iput-object v0, p0, Liki;->o:Landroid/widget/CompoundButton;

    iget-object v0, p0, Ljp;->n:Ljq;

    invoke-virtual {v0}, Ljq;->b()Ljj;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljj;->a(Z)V

    invoke-virtual {v0, v3, v3}, Ljj;->a(II)V

    iget-object v1, p0, Liki;->o:Landroid/widget/CompoundButton;

    new-instance v2, Ljl;

    const/4 v3, -0x2

    const v4, 0x800015

    invoke-direct {v2, v3, v4}, Ljl;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Ljj;->a(Landroid/view/View;Ljl;)V

    iget-object v0, p0, Liki;->o:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v5}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c    # android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Liki;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0}, Ljp;->onResume()V

    invoke-direct {p0}, Liki;->f()V

    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "BaseUserLocationSettingsActivity.onServiceConnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p2}, Liix;->a(Landroid/os/IBinder;)Liiw;

    move-result-object v0

    iput-object v0, p0, Liki;->q:Liiw;

    invoke-direct {p0}, Liki;->f()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    const-string v0, "GCoreLocationSettings"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GCoreLocationSettings"

    const-string v1, "BaseUserLocationSettingsActivity.onServiceDisconnected()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Liki;->q:Liiw;

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Ljp;->onStart()V

    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ljp;->onStop()V

    iget-object v0, p0, Liki;->q:Liiw;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p0}, Liki;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Liki;->q:Liiw;

    :cond_0
    return-void
.end method
