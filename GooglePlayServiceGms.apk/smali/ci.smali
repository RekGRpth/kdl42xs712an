.class abstract Lci;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ThreadFactory;

.field private static final b:Ljava/util/concurrent/BlockingQueue;

.field private static final c:Lco;

.field public static final d:Ljava/util/concurrent/Executor;

.field private static volatile e:Ljava/util/concurrent/Executor;


# instance fields
.field private final f:Lcq;

.field private final g:Ljava/util/concurrent/FutureTask;

.field private volatile h:Lcp;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lcj;

    invoke-direct {v0}, Lcj;-><init>()V

    sput-object v0, Lci;->a:Ljava/util/concurrent/ThreadFactory;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, Lci;->b:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x5

    const/16 v2, 0x80

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v6, Lci;->b:Ljava/util/concurrent/BlockingQueue;

    sget-object v7, Lci;->a:Ljava/util/concurrent/ThreadFactory;

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lci;->d:Ljava/util/concurrent/Executor;

    new-instance v0, Lco;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco;-><init>(B)V

    sput-object v0, Lci;->c:Lco;

    sget-object v0, Lci;->d:Ljava/util/concurrent/Executor;

    sput-object v0, Lci;->e:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcp;->a:Lcp;

    iput-object v0, p0, Lci;->h:Lcp;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lci;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lck;

    invoke-direct {v0, p0}, Lck;-><init>(Lci;)V

    iput-object v0, p0, Lci;->f:Lcq;

    new-instance v0, Lcl;

    iget-object v1, p0, Lci;->f:Lcq;

    invoke-direct {v0, p0, v1}, Lcl;-><init>(Lci;Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lci;->g:Ljava/util/concurrent/FutureTask;

    return-void
.end method

.method static synthetic a(Lci;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lci;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lci;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lci;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    const/4 v4, 0x1

    sget-object v0, Lci;->c:Lco;

    new-instance v1, Lcn;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {v1, p0, v2}, Lcn;-><init>(Lci;[Ljava/lang/Object;)V

    invoke-virtual {v0, v4, v1}, Lco;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-object p1
.end method

.method static synthetic b(Lci;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lci;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lci;->b(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method protected static varargs c()V
    .locals 0

    return-void
.end method

.method static synthetic c(Lci;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lci;->g:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lci;->a()V

    :goto_0
    sget-object v0, Lcp;->c:Lcp;

    iput-object v0, p0, Lci;->h:Lcp;

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lci;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final varargs a(Ljava/util/concurrent/Executor;)Lci;
    .locals 2

    iget-object v0, p0, Lci;->h:Lcp;

    sget-object v1, Lcp;->a:Lcp;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcm;->a:[I

    iget-object v1, p0, Lci;->h:Lcp;

    invoke-virtual {v1}, Lcp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    sget-object v0, Lcp;->b:Lcp;

    iput-object v0, p0, Lci;->h:Lcp;

    iget-object v0, p0, Lci;->f:Lcq;

    const/4 v1, 0x0

    iput-object v1, v0, Lcq;->b:[Ljava/lang/Object;

    iget-object v0, p0, Lci;->g:Ljava/util/concurrent/FutureTask;

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-object p0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task is already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot execute task: the task has already been executed (a task can be executed only once)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected varargs abstract b()Ljava/lang/Object;
.end method

.method public final d()Z
    .locals 2

    iget-object v0, p0, Lci;->g:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method
