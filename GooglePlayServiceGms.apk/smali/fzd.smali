.class public final Lfzd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/lang/String;Ljava/util/List;)Lgfr;
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfr;

    invoke-interface {v0}, Lgfr;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lgfr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lfyf;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lfzd;->a(Lfyf;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static a(Lfyf;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 10

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-instance v3, Landroid/text/SpannableStringBuilder;

    const-string v0, "<p>|</p>"

    const-string v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    invoke-virtual {v3, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    if-nez v0, :cond_3

    move v1, v2

    :goto_1
    move v5, v2

    :goto_2
    if-ge v5, v1, :cond_c

    aget-object v6, v0, v5

    invoke-virtual {v6}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "#"

    invoke-virtual {v7, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_7

    if-eqz p3, :cond_5

    invoke-static {v7, p3}, Lfzd;->a(Ljava/lang/String;Ljava/util/List;)Lgfr;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lfyf;->a(Ljava/lang/String;Lgfr;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    :goto_4
    if-eqz v4, :cond_1

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v9

    invoke-virtual {v3, v4, v7, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    invoke-virtual {v3, v6}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_3
    array-length v1, v0

    goto :goto_1

    :cond_4
    move v4, v2

    goto :goto_3

    :cond_5
    if-eqz p4, :cond_6

    invoke-static {v7, p4}, Lfzd;->b(Ljava/lang/String;Ljava/util/List;)Lgfp;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lfyf;->a(Ljava/lang/String;Lgfp;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_6
    if-eqz p5, :cond_a

    invoke-static {v7, p5}, Lfzd;->c(Ljava/lang/String;Ljava/util/List;)Lglo;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-interface {p0, p2, v4}, Lfyf;->a(Ljava/lang/String;Lglo;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_7
    if-eqz p3, :cond_8

    invoke-static {v7, p3}, Lfzd;->a(Ljava/lang/String;Ljava/util/List;)Lgfr;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lfyf;->a(Ljava/lang/String;Lgfr;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_8
    if-eqz p4, :cond_9

    invoke-static {v7, p4}, Lfzd;->b(Ljava/lang/String;Ljava/util/List;)Lgfp;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lfyf;->a(Ljava/lang/String;Lgfp;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_9
    if-eqz p5, :cond_a

    invoke-static {v7, p5}, Lfzd;->c(Ljava/lang/String;Ljava/util/List;)Lglo;

    move-result-object v4

    invoke-interface {p0, p2, v4, v7}, Lfyf;->a(Ljava/lang/String;Lglo;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v4

    goto :goto_4

    :cond_a
    const-string v4, "UpgradeAccount"

    const/4 v8, 0x5

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "UpgradeAccount"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failed to create ClickableSpan for url: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const/4 v4, 0x0

    goto :goto_4

    :cond_c
    move-object p1, v3

    goto/16 :goto_0
.end method

.method public static a()Z
    .locals 3

    sget-object v0, Lfsr;->Q:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "*"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static a(Lgfm;)Z
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "name"

    invoke-interface {p0}, Lgfm;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "name"

    invoke-interface {p0}, Lgfm;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lgll;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lgll;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ok"

    invoke-interface {p0}, Lgll;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Ljava/util/List;)Lgfp;
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfp;

    invoke-interface {v0}, Lgfp;->g()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lgfp;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static b(Lgll;)Z
    .locals 2

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lgll;->g()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v0, "error"

    invoke-interface {p0}, Lgll;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;Ljava/util/List;)Lglo;
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglo;

    invoke-interface {v0}, Lglo;->c()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Lglo;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Lgll;)Z
    .locals 10

    const/4 v2, 0x1

    const/4 v4, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lgll;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lgll;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->c()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v4

    :cond_1
    :goto_0
    return v2

    :cond_2
    invoke-interface {p0}, Lgll;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v4

    move v1, v4

    move v3, v4

    :goto_1
    if-ge v5, v7, :cond_4

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfm;

    const-string v8, "termsOfService"

    invoke-interface {v0}, Lgfm;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v1, v2

    :cond_3
    const-string v8, "button"

    invoke-interface {v0}, Lgfm;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    add-int/lit8 v0, v3, 0x1

    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v0

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    if-eq v3, v2, :cond_1

    :cond_5
    move v2, v4

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_2
.end method

.method public static d(Lgll;)Lgfm;
    .locals 6

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lgll;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lgll;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->c()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-interface {p0}, Lgll;->b()Lglp;

    move-result-object v0

    invoke-interface {v0}, Lglp;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfm;

    invoke-interface {v0}, Lgfm;->h()Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method
