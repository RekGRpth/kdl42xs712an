.class public final Ldcy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbdu;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcwm;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;I)Lbeh;
    .locals 3

    new-instance v0, Ldda;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p2, v2}, Ldda;-><init>(Ldcy;III)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;)Lbeh;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Ldcz;

    invoke-direct {v1, p0, v0}, Ldcz;-><init>(Ldcy;[Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;I)Lbeh;
    .locals 7

    new-instance v0, Lddd;

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lddd;-><init>(Ldcy;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;I[BI)Lbeh;
    .locals 7

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p3, v3, v0

    new-instance v0, Lddb;

    move-object v1, p0

    move-object v2, p2

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lddb;-><init>(Ldcy;Ljava/lang/String;[Ljava/lang/String;I[BI)V

    invoke-interface {p1, v0}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbeh;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Lddc;

    invoke-direct {v1, p0, p2, p3, v0}, Lddc;-><init>(Ldcy;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lbdu;->b(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lbdu;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->o()V

    return-void
.end method

.method public final a(Lbdu;Ldle;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->a(Ldle;)V

    return-void
.end method

.method public final a(Lbdu;Ldle;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcwm;->a(Ldle;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Lbdu;)I
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0}, Lcwm;->s()I

    move-result v0

    return v0
.end method

.method public final b(Lbdu;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcte;->a(Lbdu;)Lcwm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcwm;->e(Ljava/lang/String;)V

    return-void
.end method

.method public final c(Lbdu;Ljava/lang/String;)Lbeh;
    .locals 2

    new-instance v0, Ldde;

    const v1, 0xffff

    invoke-direct {v0, p0, p2, v1}, Ldde;-><init>(Ldcy;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lbdu;->a(Lbdq;)Lbdq;

    move-result-object v0

    return-object v0
.end method
