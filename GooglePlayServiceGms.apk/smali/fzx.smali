.class public final Lfzx;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbbr;
.implements Lbbs;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private Y:Lfst;

.field private Z:Lfsv;

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Lbbo;

.field private ah:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final ai:Lfuk;

.field private final aj:Lfue;

.field private final ak:Lfuk;

.field private final al:Lfuo;

.field b:Landroid/widget/ProgressBar;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private final h:Lftz;

.field private i:Lftx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/pos"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    sput-object v0, Lfzx;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lfzx;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lfzy;

    invoke-direct {v0, p0}, Lfzy;-><init>(Lfzx;)V

    iput-object v0, p0, Lfzx;->ai:Lfuk;

    new-instance v0, Lfzz;

    invoke-direct {v0, p0}, Lfzz;-><init>(Lfzx;)V

    iput-object v0, p0, Lfzx;->aj:Lfue;

    new-instance v0, Lgaa;

    invoke-direct {v0, p0}, Lgaa;-><init>(Lfzx;)V

    iput-object v0, p0, Lfzx;->ak:Lfuk;

    new-instance v0, Lgab;

    invoke-direct {v0, p0}, Lgab;-><init>(Lfzx;)V

    iput-object v0, p0, Lfzx;->al:Lfuo;

    iput-object p1, p0, Lfzx;->h:Lftz;

    return-void
.end method

.method static synthetic a(Lfzx;Lfst;)Lfst;
    .locals 0

    iput-object p1, p0, Lfzx;->Y:Lfst;

    return-object p1
.end method

.method static synthetic a(Lfzx;Lfsv;)Lfsv;
    .locals 0

    iput-object p1, p0, Lfzx;->Z:Lfsv;

    return-object p1
.end method

.method static synthetic a(Lfzx;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lfzx;->g:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lfst;)V
    .locals 7

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v2, 0x7f0a029d    # com.google.android.gms.R.id.plus_one_preview_container

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v2, Lgac;

    invoke-direct {v2, v1}, Lgac;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lfzx;->i:Lftx;

    invoke-virtual {v2, v3}, Lgac;->a(Lftx;)V

    invoke-virtual {v2, p1}, Lgac;->a(Lfst;)Z

    move-result v3

    iget-boolean v4, p0, Lfzx;->ab:Z

    if-nez v4, :cond_1

    if-eqz p1, :cond_1

    const/4 v4, 0x1

    iput-boolean v4, p0, Lfzx;->ab:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v4, Lfnu;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v1, v3, v4, v5, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_1
    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v4, Lfnu;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v1, v3, v4, v5, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Lfsv;)V
    .locals 4

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a0296    # com.google.android.gms.R.id.plus_one_user_avatar

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v1, p0, Lfzx;->i:Lftx;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lftx;)V

    iget-object v1, p0, Lfzx;->i:Lftx;

    invoke-interface {v1}, Lftx;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "com.google.android.gms.plus"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "avatars"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    invoke-virtual {p1}, Lfsv;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v2, 0x7f0a0298    # com.google.android.gms.R.id.plus_one_user_name

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a0294    # com.google.android.gms.R.id.plus_one_content

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method static synthetic a(Lfzx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzx;->aa:Z

    return v0
.end method

.method static synthetic b(Lfzx;)Lfuk;
    .locals 1

    iget-object v0, p0, Lfzx;->ak:Lfuk;

    return-object v0
.end method

.method static synthetic b(Lfzx;Lfst;)V
    .locals 0

    invoke-direct {p0, p1}, Lfzx;->a(Lfst;)V

    return-void
.end method

.method static synthetic b(Lfzx;Lfsv;)V
    .locals 0

    invoke-direct {p0, p1}, Lfzx;->a(Lfsv;)V

    return-void
.end method

.method static synthetic c(Lfzx;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfzx;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lfzx;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfzx;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lfzx;)Lftx;
    .locals 1

    iget-object v0, p0, Lfzx;->i:Lftx;

    return-object v0
.end method

.method static synthetic f(Lfzx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzx;->ae:Z

    return v0
.end method

.method static synthetic g(Lfzx;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lfzx;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfzx;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lfzx;)Lfst;
    .locals 1

    iget-object v0, p0, Lfzx;->Y:Lfst;

    return-object v0
.end method

.method static synthetic j(Lfzx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzx;->ad:Z

    return v0
.end method

.method static synthetic k(Lfzx;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzx;->af:Z

    return v0
.end method

.method static synthetic l(Lfzx;)Lfsv;
    .locals 1

    iget-object v0, p0, Lfzx;->Z:Lfsv;

    return-object v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfzx;->ag:Lbbo;

    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    iget-object v0, p0, Lfzx;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lfzx;->b(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method public final P_()V
    .locals 0

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0400de    # com.google.android.gms.R.layout.plus_one_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 5

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v2, Lfnu;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v2, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfnv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, v4}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x1

    :goto_2
    invoke-virtual {v1, v0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v2, Lfnu;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/widget/ProgressBar;)V
    .locals 1

    iput-object p1, p0, Lfzx;->b:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lfzx;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lfzx;->b(Landroid/widget/ProgressBar;)V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_1
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->w:Z

    if-eqz v1, :cond_2

    :try_start_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lbbo;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lfzx;->ag:Lbbo;

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "PlusOneFragment#mAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "PlusOneFragment#mToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfzx;->g:Ljava/lang/String;

    const-string v2, "PlusOneFragment#mApplyPlusOne"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lfzx;->d:Z

    const-string v2, "PlusOneFragment#mUrl"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfzx;->c:Ljava/lang/String;

    const-string v2, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzx;->e:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    new-instance v2, Lfwa;

    invoke-direct {v2, v0}, Lfwa;-><init>(Landroid/content/Context;)V

    iput-object v1, v2, Lfwa;->a:Ljava/lang/String;

    sget-object v1, Lfzx;->a:[Ljava/lang/String;

    invoke-virtual {v2, v1}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v1

    iget-object v2, p0, Lfzx;->e:Ljava/lang/String;

    iput-object v2, v1, Lfwa;->c:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lfwa;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v1

    iget-object v2, p0, Lfzx;->h:Lftz;

    invoke-interface {v2, v0, v1, p0, p0}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v1

    iput-object v1, p0, Lfzx;->i:Lftx;

    iput-boolean v3, p0, Lfzx;->ad:Z

    iput-boolean v3, p0, Lfzx;->ae:Z

    iput-boolean v3, p0, Lfzx;->af:Z

    if-nez p1, :cond_1

    iget-boolean v1, p0, Lfzx;->d:Z

    iput-boolean v1, p0, Lfzx;->aa:Z

    iput-boolean v3, p0, Lfzx;->ab:Z

    iput-boolean v3, p0, Lfzx;->ac:Z

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "pendingInsert"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfzx;->aa:Z

    const-string v0, "loggedPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfzx;->ab:Z

    const-string v0, "loggedPlusOne"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lfzx;->ac:Z

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzx;->f:Ljava/lang/String;

    :cond_2
    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzx;->g:Ljava/lang/String;

    :cond_3
    const-string v0, "linkPreview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lfst;

    const-string v1, "linkPreview"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lfst;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lfzx;->Y:Lfst;

    :cond_4
    const-string v0, "signUpState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lfsv;

    const-string v1, "signUpState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lfsv;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lfzx;->Z:Lfsv;

    goto :goto_0
.end method

.method protected final b(Landroid/widget/ProgressBar;)V
    .locals 1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lfzx;->ad:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfzx;->ae:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfzx;->af:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfzx;->f:Ljava/lang/String;

    :cond_0
    iget-boolean v0, p0, Lfzx;->ac:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_1

    iput-boolean v5, p0, Lfzx;->ac:Z

    iget-object v1, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v2, Lfnu;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfnv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-object v1, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v2, Lfnv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lfzx;->aa:Z

    if-eqz v0, :cond_2

    iput-boolean v5, p0, Lfzx;->ad:Z

    iget-object v0, p0, Lfzx;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lfzx;->i:Lftx;

    iget-object v1, p0, Lfzx;->ai:Lfuk;

    iget-object v2, p0, Lfzx;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lftx;->a(Lfuk;Ljava/lang/String;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lfzx;->Y:Lfst;

    if-nez v0, :cond_3

    iput-boolean v5, p0, Lfzx;->ae:Z

    iget-object v0, p0, Lfzx;->i:Lftx;

    iget-object v1, p0, Lfzx;->aj:Lfue;

    iget-object v2, p0, Lfzx;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lftx;->a(Lfue;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lfzx;->Z:Lfsv;

    if-nez v0, :cond_4

    iput-boolean v5, p0, Lfzx;->af:Z

    iget-object v0, p0, Lfzx;->i:Lftx;

    iget-object v1, p0, Lfzx;->al:Lfuo;

    invoke-interface {v0, v1}, Lftx;->a(Lfuo;)V

    :cond_4
    invoke-direct {p0, v5}, Lfzx;->a(Z)V

    iget-object v0, p0, Lfzx;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lfzx;->b(Landroid/widget/ProgressBar;)V

    :cond_5
    :goto_1
    return-void

    :cond_6
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    iget-object v1, p0, Lfzx;->i:Lftx;

    invoke-interface {v1}, Lftx;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_5

    invoke-virtual {v0, v5}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzx;->aa:Z

    iget-object v0, p0, Lfzx;->i:Lftx;

    iget-object v1, p0, Lfzx;->ak:Lfuk;

    iget-object v2, p0, Lfzx;->c:Ljava/lang/String;

    iget-object v3, p0, Lfzx;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lftx;->a(Lfuk;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lfzx;->ag:Lbbo;

    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a029a    # com.google.android.gms.R.id.plus_one_app_icon

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v3, 0x7f0a029b    # com.google.android.gms.R.id.plus_one_app_name

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :try_start_0
    iget-object v3, p0, Lfzx;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lfzx;->e:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lfzx;->j()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b034a    # com.google.android.gms.R.string.plus_one_app

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a029e    # com.google.android.gms.R.id.plus_one_undo_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a029f    # com.google.android.gms.R.id.plus_one_share_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a02a0    # com.google.android.gms.R.id.plus_one_confirm_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    invoke-direct {p0, v0}, Lfzx;->a(Z)V

    iget-object v0, p0, Lfzx;->Z:Lfsv;

    invoke-direct {p0, v0}, Lfzx;->a(Lfsv;)V

    iget-object v0, p0, Lfzx;->Y:Lfst;

    invoke-direct {p0, v0}, Lfzx;->a(Lfst;)V

    iget-object v0, p0, Lfzx;->b:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lfzx;->b(Landroid/widget/ProgressBar;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lfzx;->ah:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lfzx;->ah:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {v0, v1}, Lfmr;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "pendingInsert"

    iget-boolean v1, p0, Lfzx;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "loggedPreview"

    iget-boolean v1, p0, Lfzx;->ab:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "loggedPlusOne"

    iget-boolean v1, p0, Lfzx;->ac:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "account"

    iget-object v1, p0, Lfzx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfzx;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "token"

    iget-object v1, p0, Lfzx;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lfzx;->Y:Lfst;

    if-eqz v0, :cond_2

    const-string v0, "linkPreview"

    iget-object v1, p0, Lfzx;->Y:Lfst;

    invoke-virtual {v1}, Lfst;->f()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    iget-object v0, p0, Lfzx;->Z:Lfsv;

    if-eqz v0, :cond_3

    const-string v0, "signUpState"

    iget-object v1, p0, Lfzx;->Z:Lfsv;

    iget-object v1, v1, Lfsv;->a:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_3
    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->g_()V

    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 11

    const/4 v10, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v5, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfzx;->i:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnu;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-object v0, p0, Lfzx;->i:Lftx;

    iget-object v3, p0, Lfzx;->c:Ljava/lang/String;

    invoke-interface {v0, v10, v3}, Lftx;->b(Lfuk;Ljava/lang/String;)V

    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnu;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnu;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v6, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v7, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v6, v7}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v6, Lfnv;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v7, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v6, v7}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    new-instance v6, Lfmw;

    invoke-direct {v6, v5}, Lfmw;-><init>(Landroid/app/Activity;)V

    iget-object v0, p0, Lfzx;->c:Ljava/lang/String;

    iget-object v3, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v7, "android.intent.extra.TEXT"

    invoke-virtual {v3, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    const-string v0, "text/plain"

    iget-object v3, v6, Lfmw;->a:Landroid/content/Intent;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v4, :cond_a

    move v3, v4

    :goto_3
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    iget-object v7, v6, Lfmw;->a:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {v0, v8, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v3, :cond_4

    if-nez v8, :cond_b

    :cond_4
    move v0, v4

    :goto_4
    const-string v9, "Call-to-action buttons are only available for URLs."

    invoke-static {v0, v9}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz v8, :cond_5

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v9, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v0, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_5
    move v0, v4

    :goto_5
    const-string v9, "The content URL is required for interactive posts."

    invoke-static {v0, v9}, Lbkm;->a(ZLjava/lang/Object;)V

    if-eqz v8, :cond_6

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_6
    move v0, v4

    :goto_6
    const-string v8, "Must set content URL or content deep-link ID to use a call-to-action button."

    invoke-static {v0, v8}, Lbkm;->a(ZLjava/lang/Object;)V

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    const-string v0, "GooglePlusPlatform"

    const-string v8, "The provided deep-link ID is empty."

    invoke-static {v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_7
    const-string v8, "The specified deep-link ID was malformed."

    invoke-static {v0, v8}, Lbkm;->a(ZLjava/lang/Object;)V

    :cond_7
    if-nez v3, :cond_8

    if-eqz v7, :cond_8

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_10

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v8, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v9, "android.intent.extra.STREAM"

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v8, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_8
    iput-object v10, v6, Lfmw;->b:Ljava/util/ArrayList;

    :cond_8
    if-eqz v3, :cond_9

    if-nez v7, :cond_9

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    iget-object v0, v6, Lfmw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    iget-object v7, v6, Lfmw;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_9
    :goto_9
    const-string v0, "com.google.android.gms.plus.action.SHARE_INTERNAL_GOOGLE"

    iget-object v3, v6, Lfmw;->a:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    :goto_a
    const-string v3, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.apps.plus.IS_FROM_PLUSONE"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lfzx;->ah:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->e()V

    invoke-virtual {p0, v0}, Lfzx;->a(Landroid/content/Intent;)V

    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_14

    :goto_b
    invoke-virtual {v5, v2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_a
    move v3, v1

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_4

    :cond_c
    move v0, v1

    goto/16 :goto_5

    :cond_d
    move v0, v1

    goto/16 :goto_6

    :cond_e
    const-string v8, " "

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "GooglePlusPlatform"

    const-string v8, "Spaces are not allowed in deep-link IDs."

    invoke-static {v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_7

    :cond_f
    move v0, v4

    goto/16 :goto_7

    :cond_10
    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_11
    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    goto :goto_9

    :cond_12
    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms.plus.action.SHARE_GOOGLE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.gms"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    goto :goto_a

    :cond_13
    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    const-string v3, "com.google.android.apps.plus"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v6, Lfmw;->a:Landroid/content/Intent;

    goto :goto_a

    :cond_14
    move v2, v1

    goto :goto_b

    :pswitch_2
    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnu;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    :goto_c
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-boolean v0, p0, Lfzx;->d:Z

    if-eqz v0, :cond_16

    :goto_d
    invoke-virtual {v5, v2}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v5}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_15
    iget-object v0, p0, Lfzx;->f:Ljava/lang/String;

    sget-object v3, Lfnu;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lfnv;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, p0, Lfzx;->e:Ljava/lang/String;

    invoke-static {v5, v0, v3, v4, v6}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_c

    :cond_16
    move v2, v1

    goto :goto_d

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a029e
        :pswitch_0    # com.google.android.gms.R.id.plus_one_undo_button
        :pswitch_1    # com.google.android.gms.R.id.plus_one_share_button
        :pswitch_2    # com.google.android.gms.R.id.plus_one_confirm_button
    .end packed-switch
.end method

.method public final w()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    iget-object v0, p0, Lfzx;->ag:Lbbo;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lfzx;->ag:Lbbo;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lbbo;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lfzx;->ag:Lbbo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0, v3}, Lo;->showDialog(I)V

    goto :goto_0
.end method
