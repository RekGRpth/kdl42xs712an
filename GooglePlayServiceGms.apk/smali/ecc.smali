.class public final Lecc;
.super Ldzx;
.source "SourceFile"

# interfaces
.implements Lebr;
.implements Lebu;
.implements Lebx;


# instance fields
.field private final a:Ldwr;

.field private final b:Ljava/lang/String;

.field private final c:Leci;


# direct methods
.method public constructor <init>(Ldwr;)V
    .locals 2

    invoke-direct {p0}, Ldzx;-><init>()V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lecc;->a:Ldwr;

    invoke-virtual {p1}, Ldwr;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lecc;->b:Ljava/lang/String;

    new-instance v0, Leci;

    invoke-direct {v0, p1}, Leci;-><init>(Ldwr;)V

    iput-object v0, p0, Lecc;->c:Leci;

    iget-object v0, p0, Lecc;->a:Ldwr;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v0, v1}, Ledd;->a(Lo;Ljava/lang/String;)V

    return-void
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 6

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "acceptInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->q_()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "acceptInvitation: invitation not valid anymore..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lecc;->a:Ldwr;

    const v2, 0x7f0b02a6    # com.google.android.gms.R.string.games_progress_dialog_accepting_invitation

    invoke-virtual {v1, v2}, Ldwr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    iget-object v2, p0, Lecc;->a:Ldwr;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Ldgs;->b(Lbdu;Ljava/lang/String;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Lecd;

    invoke-direct {v1, p0}, Lecd;-><init>(Lecc;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lecc;->a:Ldwr;

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v2}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "invitation"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v2}, Ldab;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Leee;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    iget-object v0, p0, Lecc;->a:Ldwr;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Ldwr;->setResult(I)V

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->finish()V

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 6

    iget-object v1, p0, Lecc;->a:Ldwr;

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v3}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "turn_based_match"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-static {v4, v2}, Ldab;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Leee;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    iget-object v0, p0, Lecc;->a:Ldwr;

    const/16 v1, 0x4e21

    invoke-virtual {v0, v1}, Ldwr;->setResult(I)V

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->finish()V

    return-void
.end method

.method private g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "launchGameForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lecc;->a:Ldwr;

    const v2, 0x7f0b02a7    # com.google.android.gms.R.string.games_progress_dialog_starting_rematch

    invoke-virtual {v1, v2}, Ldwr;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lebo;->a(Ljava/lang/String;)Lebo;

    move-result-object v1

    iget-object v2, p0, Lecc;->a:Ldwr;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Ldgs;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)Lbeh;

    move-result-object v0

    new-instance v1, Lece;

    invoke-direct {v1, p0}, Lece;-><init>(Lecc;)V

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lecc;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcte;->i:Ldfx;

    invoke-interface {v1, v0, p1, p2, p3}, Ldfx;->a(Lbdu;Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const/16 v2, 0x384

    invoke-virtual {v1, v0, v2}, Ldwr;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationAccepted: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcte;->a(Lbdu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lecc;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lecc;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {v1, v2, v0}, Lebt;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/Invitation;)Lebt;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Leee;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-static {v0, p1, p0}, Leee;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Leei;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Leee;->a(Landroid/content/Context;Lbdu;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lecc;->a:Ldwr;

    invoke-virtual {v1}, Ldwr;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onMatchClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcte;->a(Lbdu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lecc;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lecc;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lebw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lebw;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lecc;->a_(Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onMatchParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Leee;->a(Landroid/content/Context;Lbdu;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Ldgt;)V
    .locals 4

    invoke-interface {p1}, Ldgt;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    invoke-interface {p1}, Ldgt;->g()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v1

    iget-object v2, p0, Lecc;->a:Ldwr;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v3}, Ledd;->a(Lo;Ljava/lang/String;)V

    iget-object v2, p0, Lecc;->a:Ldwr;

    invoke-virtual {v2}, Ldwr;->j()Lbdu;

    move-result-object v2

    invoke-interface {v2}, Lbdu;->d()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onTurnBasedMatchInitiated: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Leee;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const v0, 0x7f0b0254    # com.google.android.gms.R.string.games_inbox_network_error_dialog_message

    invoke-static {v0}, Lebn;->c(I)Lebn;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-nez v1, :cond_2

    const-string v1, "HeadlessInboxHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No turn-based match received after accepting invite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final a(Ldlb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_REQUEST_INBOX_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-interface {p1}, Ldlb;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lecc;->a:Ldwr;

    invoke-virtual {v1, v0}, Ldwr;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/Game;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationGameInfoClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lecc;->a:Ldwr;

    invoke-static {v1}, Leee;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "com.google.android.gms"

    invoke-static {v0, v1}, Lcte;->a(Lbdu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lecc;->d(Lcom/google/android/gms/games/Game;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lecc;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    invoke-static {v1, v2, v0}, Lebq;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)Lebq;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lecc;->a:Ldwr;

    iget-object v1, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Leee;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Leee;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecc;->c:Leci;

    invoke-virtual {v0, p1}, Leci;->a(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lecc;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0

    invoke-direct {p0, p1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onMatchDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Ldgs;->e(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecc;->c:Leci;

    invoke-virtual {v0, p1}, Leci;->a(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationDeclined: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {v2, v0, v1, v3}, Ldgs;->c(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcte;->k:Ldgh;

    invoke-interface {v2, v0, v1, v3}, Ldgh;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onMatchRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcte;->a(Lbdu;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0, p1}, Lecc;->e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lecc;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    const/4 v3, 0x1

    invoke-static {v1, v2, v0, v3}, Lebw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Z)Lebw;

    move-result-object v0

    iget-object v1, p0, Lecc;->a:Ldwr;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Ledd;->a(Lo;Lebm;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lecc;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/Game;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "switchAccountForGameDetail: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "com.google.android.gms"

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcte;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lecc;->a:Ldwr;

    iget-object v1, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Leee;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "onInvitationDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->d()Ljava/lang/String;

    move-result-object v3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {v2, v0, v1, v3}, Ldgs;->d(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcte;->k:Ldgh;

    invoke-interface {v2, v0, v1, v3}, Ldgh;->b(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "switchAccountForTurnBasedMatch: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcte;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "switchAccountForInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcte;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Leee;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-static {v0, p1, p0}, Leee;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Leei;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lecc;->f(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    iget-object v0, p0, Lecc;->a:Ldwr;

    invoke-virtual {v0}, Ldwr;->j()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "HeadlessInboxHelper"

    const-string v1, "switchAccountForRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lecc;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcte;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lecc;->g(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method
