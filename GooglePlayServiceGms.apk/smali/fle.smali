.class public final Lfle;
.super Lizp;
.source "SourceFile"


# static fields
.field private static volatile k:[Lfle;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Z

.field public f:[Lflf;

.field public g:Lfkz;

.field public h:[B

.field public i:[B

.field public j:Lfkx;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lizp;-><init>()V

    invoke-virtual {p0}, Lfle;->d()Lfle;

    return-void
.end method

.method public static c()[Lfle;
    .locals 2

    sget-object v0, Lfle;->k:[Lfle;

    if-nez v0, :cond_1

    sget-object v1, Lizq;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lfle;->k:[Lfle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lfle;

    sput-object v0, Lfle;->k:[Lfle;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lfle;->k:[Lfle;

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 6

    invoke-super {p0}, Lizp;->a()I

    move-result v0

    iget-wide v1, p0, Lfle;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-wide v2, p0, Lfle;->a:J

    invoke-static {v1, v2, v3}, Lizn;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lfle;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lfle;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lfle;->f:[Lflf;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lfle;->f:[Lflf;

    array-length v1, v1

    if-lez v1, :cond_4

    const/4 v1, 0x0

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    iget-object v2, p0, Lfle;->f:[Lflf;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lfle;->f:[Lflf;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v1, v2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    :cond_4
    iget-object v1, p0, Lfle;->h:[B

    sget-object v2, Lizv;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lfle;->h:[B

    invoke-static {v1, v2}, Lizn;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lfle;->j:Lfkx;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lfle;->j:Lfkx;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lfle;->i:[B

    sget-object v2, Lizv;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lfle;->i:[B

    invoke-static {v1, v2}, Lizn;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lfle;->g:Lfkz;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lfle;->g:Lfkz;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lfle;->e:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-boolean v2, p0, Lfle;->e:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lfle;->c:I

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget v2, p0, Lfle;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lfle;->d:I

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget v2, p0, Lfle;->d:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iput v0, p0, Lfle;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lfle;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lfle;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfle;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lfle;->f:[Lflf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lflf;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lfle;->f:[Lflf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lflf;

    invoke-direct {v3}, Lflf;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lfle;->f:[Lflf;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lflf;

    invoke-direct {v3}, Lflf;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lfle;->f:[Lflf;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v0

    iput-object v0, p0, Lfle;->h:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lfle;->j:Lfkx;

    if-nez v0, :cond_4

    new-instance v0, Lfkx;

    invoke-direct {v0}, Lfkx;-><init>()V

    iput-object v0, p0, Lfle;->j:Lfkx;

    :cond_4
    iget-object v0, p0, Lfle;->j:Lfkx;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v0

    iput-object v0, p0, Lfle;->i:[B

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lfle;->g:Lfkz;

    if-nez v0, :cond_5

    new-instance v0, Lfkz;

    invoke-direct {v0}, Lfkz;-><init>()V

    iput-object v0, p0, Lfle;->g:Lfkz;

    :cond_5
    iget-object v0, p0, Lfle;->g:Lfkz;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Lfle;->e:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfle;->c:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Lfle;->d:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-wide v0, p0, Lfle;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-wide v1, p0, Lfle;->a:J

    invoke-virtual {p1, v0, v1, v2}, Lizn;->b(IJ)V

    :cond_0
    iget-object v0, p0, Lfle;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lfle;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lfle;->f:[Lflf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfle;->f:[Lflf;

    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lfle;->f:[Lflf;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lfle;->f:[Lflf;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lfle;->h:[B

    sget-object v1, Lizv;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lfle;->h:[B

    invoke-virtual {p1, v0, v1}, Lizn;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lfle;->j:Lfkx;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lfle;->j:Lfkx;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_5
    iget-object v0, p0, Lfle;->i:[B

    sget-object v1, Lizv;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lfle;->i:[B

    invoke-virtual {p1, v0, v1}, Lizn;->a(I[B)V

    :cond_6
    iget-object v0, p0, Lfle;->g:Lfkz;

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Lfle;->g:Lfkz;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_7
    iget-boolean v0, p0, Lfle;->e:Z

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-boolean v1, p0, Lfle;->e:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_8
    iget v0, p0, Lfle;->c:I

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget v1, p0, Lfle;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_9
    iget v0, p0, Lfle;->d:I

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget v1, p0, Lfle;->d:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_a
    invoke-super {p0, p1}, Lizp;->a(Lizn;)V

    return-void
.end method

.method public final d()Lfle;
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfle;->a:J

    const-string v0, ""

    iput-object v0, p0, Lfle;->b:Ljava/lang/String;

    iput v2, p0, Lfle;->c:I

    iput v2, p0, Lfle;->d:I

    iput-boolean v2, p0, Lfle;->e:Z

    invoke-static {}, Lflf;->c()[Lflf;

    move-result-object v0

    iput-object v0, p0, Lfle;->f:[Lflf;

    iput-object v3, p0, Lfle;->g:Lfkz;

    sget-object v0, Lizv;->h:[B

    iput-object v0, p0, Lfle;->h:[B

    sget-object v0, Lizv;->h:[B

    iput-object v0, p0, Lfle;->i:[B

    iput-object v3, p0, Lfle;->j:Lfkx;

    iput-object v3, p0, Lfle;->q:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lfle;->C:I

    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lfle;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lfle;

    iget-wide v2, p0, Lfle;->a:J

    iget-wide v4, p1, Lfle;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lfle;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lfle;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lfle;->b:Ljava/lang/String;

    iget-object v3, p1, Lfle;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lfle;->c:I

    iget v3, p1, Lfle;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lfle;->d:I

    iget v3, p1, Lfle;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-boolean v2, p0, Lfle;->e:Z

    iget-boolean v3, p1, Lfle;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lfle;->f:[Lflf;

    iget-object v3, p1, Lfle;->f:[Lflf;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lfle;->g:Lfkz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lfle;->g:Lfkz;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lfle;->g:Lfkz;

    iget-object v3, p1, Lfle;->g:Lfkz;

    invoke-virtual {v2, v3}, Lfkz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lfle;->h:[B

    iget-object v3, p1, Lfle;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Lfle;->i:[B

    iget-object v3, p1, Lfle;->i:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Lfle;->j:Lfkx;

    if-nez v2, :cond_e

    iget-object v2, p1, Lfle;->j:Lfkx;

    if-eqz v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lfle;->j:Lfkx;

    iget-object v3, p1, Lfle;->j:Lfkx;

    invoke-virtual {v2, v3}, Lfkx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Lfle;->q:Ljava/util/List;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lfle;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_10
    iget-object v2, p1, Lfle;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lfle;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lfle;->q:Ljava/util/List;

    iget-object v1, p1, Lfle;->q:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Lfle;->a:J

    iget-wide v4, p0, Lfle;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfle;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfle;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lfle;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lfle;->e:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfle;->f:[Lflf;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfle;->g:Lfkz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfle;->h:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfle;->i:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lfle;->j:Lfkx;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lfle;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfle;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_0
    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lfle;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x4d5

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lfle;->g:Lfkz;

    invoke-virtual {v0}, Lfkz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lfle;->j:Lfkx;

    invoke-virtual {v0}, Lfkx;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lfle;->q:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4
.end method
