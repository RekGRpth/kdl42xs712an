.class public final Lbmc;
.super Lbme;
.source "SourceFile"


# instance fields
.field private final f:[Lbmd;


# direct methods
.method public constructor <init>(Ljava/lang/String;[Lbmd;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 11

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lbme;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lsk;Lsj;Ljava/lang/String;ZLjava/util/HashMap;)V

    iput-object p2, p0, Lbmc;->f:[Lbmd;

    return-void
.end method


# virtual methods
.method public final l()Ljava/lang/String;
    .locals 1

    const-string v0, "multipart/related; boundary=END_OF_PART"

    return-object v0
.end method

.method public final m()[B
    .locals 9

    const/4 v3, 0x1

    const/4 v4, 0x0

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lbmc;->f:[Lbmd;

    array-length v6, v0

    move v2, v4

    :goto_0
    if-ge v2, v6, :cond_5

    iget-object v0, p0, Lbmc;->f:[Lbmd;

    aget-object v7, v0, v2

    const-string v0, "--END_OF_PART\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "Content-Type: "

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v7, Lbmd;->a:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v8, "\n"

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    iget-object v0, v7, Lbmd;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, v7, Lbmd;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    :goto_2
    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v4

    goto :goto_1

    :cond_1
    iget-object v0, v7, Lbmd;->c:[B

    if-eqz v0, :cond_3

    iget-object v0, v7, Lbmd;->c:[B

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v3

    :goto_3
    if-eqz v0, :cond_4

    iget-object v0, p0, Lbmc;->f:[Lbmd;

    aget-object v0, v0, v2

    iget-object v0, v0, Lbmd;->c:[B

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_4
    :try_start_2
    const-string v2, "PeopleService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "ApiaryMRR"

    const-string v3, "Failed to write parts to output stream"

    invoke-static {v2, v3, v0}, Lfdk;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    :goto_5
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v4

    goto :goto_3

    :cond_4
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Multipart/related part has no content"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :goto_6
    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_5
    :try_start_4
    const-string v0, "--END_OF_PART--\n"

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v1}, Lbpm;->a(Ljava/io/Closeable;)V

    goto :goto_5

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_4
.end method
