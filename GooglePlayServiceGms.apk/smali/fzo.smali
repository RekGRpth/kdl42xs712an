.class final Lfzo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfuk;


# instance fields
.field final synthetic a:Lfzm;


# direct methods
.method constructor <init>(Lfzm;)V
    .locals 0

    iput-object p1, p0, Lfzo;->a:Lfzm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbbo;Lfsu;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "PlusOneButtonView"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPlusOneLoaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lfzo;->a:Lfzm;

    iget-boolean v0, v0, Lfzm;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfzo;->a:Lfzm;

    iput-boolean v3, v0, Lfzm;->a:Z

    iget-object v0, p0, Lfzo;->a:Lfzm;

    iget-object v0, v0, Lfzm;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_1
    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, p0, Lfzo;->a:Lfzm;

    iput-object p2, v0, Lfzm;->k:Lfsu;

    iget-object v0, p0, Lfzo;->a:Lfzm;

    iget-object v1, p0, Lfzo;->a:Lfzm;

    iget-object v1, v1, Lfzm;->k:Lfsu;

    invoke-virtual {v0, v1}, Lfzm;->a(Lfsu;)V

    iget-object v0, p0, Lfzo;->a:Lfzm;

    invoke-virtual {v0}, Lfzm;->a()V

    :goto_0
    iget-object v0, p0, Lfzo;->a:Lfzm;

    iput-boolean v3, v0, Lfzm;->m:Z

    return-void

    :cond_2
    const-string v0, "PlusOneButtonView"

    const-string v1, "PlusOne failed to load"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfzo;->a:Lfzm;

    invoke-virtual {v0}, Lfzm;->c()V

    goto :goto_0
.end method
