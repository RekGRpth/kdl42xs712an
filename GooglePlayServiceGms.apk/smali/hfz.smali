.class public final Lhfz;
.super Lhfb;
.source "SourceFile"


# instance fields
.field private final a:Lhfb;


# direct methods
.method public constructor <init>(Lhfb;)V
    .locals 0

    invoke-direct {p0}, Lhfb;-><init>()V

    iput-object p1, p0, Lhfz;->a:Lhfb;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    invoke-static {}, Lhgo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lhfz;->a:Lhfb;

    invoke-virtual {v0, p1, p2}, Lhfb;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
