.class public final enum Liae;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Liae;

.field public static final enum b:Liae;

.field public static final enum c:Liae;

.field public static final enum d:Liae;

.field public static final enum e:Liae;

.field public static final enum f:Liae;

.field private static final synthetic i:[Liae;


# instance fields
.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Liae;

    const-string v1, "ANDROID"

    const-string v2, "com.google.android.location"

    const-string v3, "com.google.android.location.internal.server.NetworkLocationService"

    invoke-direct {v0, v1, v5, v2, v3}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->a:Liae;

    new-instance v0, Liae;

    const-string v1, "DEMO"

    const-string v2, "com.google.nlpdemoapp"

    const-string v3, "com.google.android.location.internal.server.GoogleLocationService"

    invoke-direct {v0, v1, v6, v2, v3}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->b:Liae;

    new-instance v0, Liae;

    const-string v1, "GMM"

    const-string v2, "com.google.android.apps.maps"

    const-string v3, "com.google.android.location.internal.server.GoogleLocationService"

    invoke-direct {v0, v1, v7, v2, v3}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->c:Liae;

    new-instance v0, Liae;

    const-string v1, "OLD_GMM"

    const-string v2, "com.google.android.apps.maps"

    const-string v3, "com.google.android.location.internal.server.NetworkLocationService"

    invoke-direct {v0, v1, v8, v2, v3}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->d:Liae;

    new-instance v0, Liae;

    const-string v1, "GMS"

    const-string v2, "com.google.android.gms"

    const-string v3, "com.google.android.location.internal.server.GoogleLocationService"

    invoke-direct {v0, v1, v9, v2, v3}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->e:Liae;

    new-instance v0, Liae;

    const-string v1, "NONE"

    const/4 v2, 0x5

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Liae;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Liae;->f:Liae;

    const/4 v0, 0x6

    new-array v0, v0, [Liae;

    sget-object v1, Liae;->a:Liae;

    aput-object v1, v0, v5

    sget-object v1, Liae;->b:Liae;

    aput-object v1, v0, v6

    sget-object v1, Liae;->c:Liae;

    aput-object v1, v0, v7

    sget-object v1, Liae;->d:Liae;

    aput-object v1, v0, v8

    sget-object v1, Liae;->e:Liae;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Liae;->f:Liae;

    aput-object v2, v0, v1

    sput-object v0, Liae;->i:[Liae;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Liae;->g:Ljava/lang/String;

    iput-object p4, p0, Liae;->h:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Liae;
    .locals 1

    const-class v0, Liae;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Liae;

    return-object v0
.end method

.method public static values()[Liae;
    .locals 1

    sget-object v0, Liae;->i:[Liae;

    invoke-virtual {v0}, [Liae;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Liae;

    return-object v0
.end method
