.class public final Lhon;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/Class;

.field private final d:Landroid/content/Intent;

.field private final e:Ljava/util/List;

.field private final f:Lhor;

.field private g:Z

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/lang/String;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 2

    new-instance v0, Lhoq;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lhoq;-><init>(B)V

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, v0, v1}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;Lhor;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lhor;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lhon;-><init>(Landroid/content/Context;Ljava/lang/Class;Lhor;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lhor;I)V
    .locals 7

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhon;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhon;->e:Ljava/util/List;

    iput-boolean v3, p0, Lhon;->g:Z

    iput-object v1, p0, Lhon;->h:Ljava/util/ArrayList;

    if-gez p4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cache type must be a >= 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lhon;->b:Landroid/content/Context;

    iput-object p2, p0, Lhon;->c:Ljava/lang/Class;

    iput-object v1, p0, Lhon;->d:Landroid/content/Intent;

    iput-object p3, p0, Lhon;->f:Lhor;

    iput p4, p0, Lhon;->j:I

    invoke-direct {p0}, Lhon;->e()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lhon;->d:Landroid/content/Intent;

    if-nez v1, :cond_2

    iget-object v1, p0, Lhon;->b:Landroid/content/Context;

    iget v2, p0, Lhon;->j:I

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lhon;->b:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v2, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const-wide/32 v5, 0x6cebb800

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :try_start_0
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lhon;->b:Landroid/content/Context;

    iget v2, p0, Lhon;->j:I

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-boolean v1, Licj;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "SystemMemoryCache"

    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "com.google.android.location.cache.is_cache"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Intent;)I
    .locals 2

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "com.google.android.location.cache.cache_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "SystemMemoryCache"

    const-string v1, "No cache type found"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_2
    const-string v1, "com.google.android.location.cache.cache_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    const/high16 v3, 0x8000000

    invoke-direct {p0}, Lhon;->e()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.location.cache.cache_data"

    iget-object v2, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v1, "com.google.android.location.cache.cache_id"

    iget-object v2, p0, Lhon;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lhon;->d:Landroid/content/Intent;

    if-nez v1, :cond_0

    iget-object v1, p0, Lhon;->b:Landroid/content/Context;

    iget v2, p0, Lhon;->j:I

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lhon;->b:Landroid/content/Context;

    iget v2, p0, Lhon;->j:I

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method private e()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lhon;->d:Landroid/content/Intent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lhon;->b:Landroid/content/Context;

    iget-object v2, p0, Lhon;->c:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    const-string v1, "com.google.android.location.cache.is_cache"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.location.cache.cache_type"

    iget v2, p0, Lhon;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v0

    :cond_0
    iget-object v0, p0, Lhon;->d:Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Parcelable;)V
    .locals 4

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lhon;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhon;->c(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lhon;->d()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lhon;->e:Ljava/util/List;

    new-instance v2, Lhoo;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lhoo;-><init>(Lhon;Landroid/os/Parcelable;B)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 4

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lhon;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhon;->b(Ljava/util/Collection;)V

    invoke-direct {p0}, Lhon;->d()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lhon;->e:Ljava/util/List;

    new-instance v2, Lhot;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lhot;-><init>(Lhon;Ljava/util/Collection;B)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a()Z
    .locals 2

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lhon;->g:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhon;->i:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Landroid/os/Parcelable;)V
    .locals 4

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lhon;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lhon;->d(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lhon;->d()V

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    iget-object v0, p0, Lhon;->e:Ljava/util/List;

    new-instance v2, Lhos;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v3}, Lhos;-><init>(Lhon;Landroid/os/Parcelable;B)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 3

    iget-object v1, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lhon;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 5

    const/4 v0, 0x1

    iget-object v2, p0, Lhon;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lhon;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_0

    const-string v0, "SystemMemoryCache"

    const-string v1, "Cache initialize called when already initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v2

    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Lhon;->a(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-boolean v0, Licj;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "SystemMemoryCache"

    const-string v1, "Cache initialize called with wrong intent."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_3
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "com.google.android.location.cache.cache_data"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lhon;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lhon;->h:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_4

    const-string v1, "SystemMemoryCache"

    const-string v3, "No existing cache data found. Initializing."

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhon;->h:Ljava/util/ArrayList;

    :cond_5
    iget-object v1, p0, Lhon;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8

    move v1, v0

    :goto_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.android.location.cache.cache_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lhon;->i:Ljava/lang/String;

    iget-object v3, p0, Lhon;->i:Ljava/lang/String;

    if-nez v3, :cond_7

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhon;->i:Ljava/lang/String;

    sget-boolean v1, Licj;->c:Z

    if-eqz v1, :cond_6

    const-string v1, "SystemMemoryCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Created new cached id:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lhon;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v1, v0

    :cond_7
    iget-object v0, p0, Lhon;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhop;

    invoke-virtual {v0}, Lhop;->a()V

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lhon;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lhon;->d()V

    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhon;->g:Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method final c(Landroid/os/Parcelable;)V
    .locals 1

    invoke-virtual {p0, p1}, Lhon;->d(Landroid/os/Parcelable;)V

    iget-object v0, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method final d(Landroid/os/Parcelable;)V
    .locals 3

    iget-object v0, p0, Lhon;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    iget-object v2, p0, Lhon;->f:Lhor;

    invoke-interface {v2, v0, p1}, Lhor;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method
