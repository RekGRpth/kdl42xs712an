.class public final Lflo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;


# instance fields
.field private final a:Lfkr;

.field private b:Lflq;

.field private c:Z


# direct methods
.method public constructor <init>(Lfkr;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lflo;->a:Lfkr;

    const/4 v0, 0x0

    iput-object v0, p0, Lflo;->b:Lflq;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflo;->c:Z

    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 2

    iget-object v0, p0, Lflo;->b:Lflq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflq;->a(Z)V

    return-void
.end method

.method public final a(Lbbo;)V
    .locals 2

    iget-object v0, p0, Lflo;->b:Lflq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflq;->a(Z)V

    iget-boolean v0, p0, Lflo;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lfkr;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lbbo;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflo;->a:Lfkr;

    invoke-virtual {p1}, Lbbo;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-interface {v0, v1}, Lfkr;->a(Landroid/app/PendingIntent;)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflo;->c:Z

    return-void

    :cond_1
    iget-object v0, p0, Lflo;->a:Lfkr;

    invoke-interface {v0}, Lfkr;->c()V

    goto :goto_0
.end method

.method public final a(Lflq;)V
    .locals 0

    iput-object p1, p0, Lflo;->b:Lflq;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lflo;->c:Z

    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lflo;->b:Lflq;

    invoke-virtual {v0, v1}, Lflq;->a(Z)V

    iget-boolean v0, p0, Lflo;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lfkr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lfkr;

    invoke-interface {v0}, Lfkr;->b()V

    :cond_0
    iput-boolean v1, p0, Lflo;->c:Z

    return-void
.end method
