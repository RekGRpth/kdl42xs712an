.class public Lbxe;
.super Lo;
.source "SourceFile"


# instance fields
.field protected n:Lbtd;

.field protected o:Lcoy;

.field private p:Lbtg;

.field private volatile q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lo;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxe;->q:Z

    return-void
.end method


# virtual methods
.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lbxe;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lo;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lbtb;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lbxe;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    iput-object v0, p0, Lbxe;->o:Lcoy;

    iget-object v0, p0, Lbxe;->o:Lcoy;

    invoke-virtual {v0}, Lcoy;->m()Lbtd;

    move-result-object v0

    iput-object v0, p0, Lbxe;->n:Lbtd;

    iget-object v0, p0, Lbxe;->n:Lbtd;

    invoke-virtual {v0}, Lbtd;->a()V

    if-nez p1, :cond_0

    iget-object v0, p0, Lbxe;->n:Lbtd;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbtd;->a(Ljava/lang/String;Landroid/content/Intent;)V

    :cond_0
    new-instance v0, Lbtg;

    iget-object v1, p0, Lbxe;->o:Lcoy;

    invoke-direct {v0, v1}, Lbtg;-><init>(Lcoy;)V

    iput-object v0, p0, Lbxe;->p:Lbtg;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxe;->q:Z

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lbxe;->n:Lbtd;

    invoke-virtual {v0}, Lbtd;->b()V

    invoke-super {p0}, Lo;->onDestroy()V

    return-void
.end method

.method protected onPostResume()V
    .locals 1

    invoke-super {p0}, Lo;->onPostResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxe;->q:Z

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lo;->onResume()V

    invoke-virtual {p0}, Lbxe;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbxe;->p:Lbtg;

    iget-object v1, v1, Lbtg;->a:Lbwd;

    invoke-virtual {v1}, Lbwd;->a()[Landroid/accounts/Account;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v1, v0}, Lbwd;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-ne v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbxe;->finish()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const-string v0, "BaseIsRestart"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lo;->onSaveInstanceState(Landroid/os/Bundle;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iput-boolean v2, p0, Lbxe;->q:Z

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lo;->onStart()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxe;->q:Z

    return-void
.end method

.method protected onStop()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbxe;->q:Z

    invoke-super {p0}, Lo;->onStop()V

    return-void
.end method
