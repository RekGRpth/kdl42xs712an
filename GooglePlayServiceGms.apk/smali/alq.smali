.class public final Lalq;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/appstate/service/AppStateAndroidService;)V
    .locals 0

    iput-object p1, p0, Lalq;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6

    const-string v0, "AppStateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "client connected with version: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid package name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lalq;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    invoke-static {v0, p3}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/appstate/service/AppStateAndroidService;Ljava/lang/String;)Z

    iget-object v0, p0, Lalq;->a:Lcom/google/android/gms/appstate/service/AppStateAndroidService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method
