.class public final Ljav;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljah;

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lipv;

.field public h:Lipv;

.field public i:[I

.field public j:Ljbb;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    sget-object v0, Lizv;->f:[Ljava/lang/String;

    iput-object v0, p0, Ljav;->a:[Ljava/lang/String;

    invoke-static {}, Ljah;->c()[Ljah;

    move-result-object v0

    iput-object v0, p0, Ljav;->b:[Ljah;

    iput v2, p0, Ljav;->c:I

    iput v2, p0, Ljav;->d:I

    const-string v0, ""

    iput-object v0, p0, Ljav;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljav;->f:Ljava/lang/String;

    iput-object v1, p0, Ljav;->g:Lipv;

    iput-object v1, p0, Ljav;->h:Lipv;

    sget-object v0, Lizv;->a:[I

    iput-object v0, p0, Ljav;->i:[I

    iput-object v1, p0, Ljav;->j:Ljbb;

    const/4 v0, -0x1

    iput v0, p0, Ljav;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lizs;->a()I

    move-result v4

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_d

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    iget-object v5, p0, Ljav;->a:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Ljav;->a:[Ljava/lang/String;

    aget-object v5, v5, v0

    if-eqz v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    invoke-static {v5}, Lizn;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int v0, v4, v2

    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget v2, p0, Ljav;->c:I

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    iget v3, p0, Ljav;->c:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Ljav;->d:I

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    iget v3, p0, Ljav;->d:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Ljav;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x4

    iget-object v3, p0, Ljav;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Ljav;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v2, 0x5

    iget-object v3, p0, Ljav;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lizn;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Ljav;->g:Lipv;

    if-eqz v2, :cond_6

    const/4 v2, 0x6

    iget-object v3, p0, Ljav;->g:Lipv;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Ljav;->h:Lipv;

    if-eqz v2, :cond_7

    const/4 v2, 0x7

    iget-object v3, p0, Ljav;->h:Lipv;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Ljav;->i:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Ljav;->i:[I

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    :goto_2
    iget-object v4, p0, Ljav;->i:[I

    array-length v4, v4

    if-ge v2, v4, :cond_8

    iget-object v4, p0, Ljav;->i:[I

    aget v4, v4, v2

    invoke-static {v4}, Lizn;->a(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    add-int/2addr v0, v3

    iget-object v2, p0, Ljav;->i:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Ljav;->j:Ljbb;

    if-eqz v2, :cond_a

    const/16 v2, 0x9

    iget-object v3, p0, Ljav;->j:Ljbb;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Ljav;->b:[Ljah;

    if-eqz v2, :cond_c

    iget-object v2, p0, Ljav;->b:[Ljah;

    array-length v2, v2

    if-lez v2, :cond_c

    :goto_3
    iget-object v2, p0, Ljav;->b:[Ljah;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    iget-object v2, p0, Ljav;->b:[Ljah;

    aget-object v2, v2, v1

    if-eqz v2, :cond_b

    const/16 v3, 0xa

    invoke-static {v3, v2}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_c
    iput v0, p0, Ljav;->C:I

    return v0

    :cond_d
    move v0, v4

    goto/16 :goto_1
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 7

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Ljav;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Ljav;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljav;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljav;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljav;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljav;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Ljav;->g:Lipv;

    if-nez v0, :cond_4

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    iput-object v0, p0, Ljav;->g:Lipv;

    :cond_4
    iget-object v0, p0, Ljav;->g:Lipv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Ljav;->h:Lipv;

    if-nez v0, :cond_5

    new-instance v0, Lipv;

    invoke-direct {v0}, Lipv;-><init>()V

    iput-object v0, p0, Ljav;->h:Lipv;

    :cond_5
    iget-object v0, p0, Ljav;->h:Lipv;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_7

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lizm;->a()I

    :cond_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_1
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_7
    if-eqz v2, :cond_0

    iget-object v0, p0, Ljav;->i:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    if-nez v0, :cond_9

    array-length v3, v5

    if-ne v2, v3, :cond_9

    iput-object v5, p0, Ljav;->i:[I

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Ljav;->i:[I

    array-length v0, v0

    goto :goto_5

    :cond_9
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_a

    iget-object v4, p0, Ljav;->i:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Ljav;->i:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lizm;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lizm;->l()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Lizm;->k()I

    move-result v4

    if-lez v4, :cond_b

    invoke-virtual {p1}, Lizm;->g()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_6

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    if-eqz v0, :cond_f

    invoke-virtual {p1, v2}, Lizm;->e(I)V

    iget-object v2, p0, Ljav;->i:[I

    if-nez v2, :cond_d

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_c

    iget-object v0, p0, Ljav;->i:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    invoke-virtual {p1}, Lizm;->k()I

    move-result v0

    if-lez v0, :cond_e

    invoke-virtual {p1}, Lizm;->g()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_8

    :pswitch_5
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_d
    iget-object v2, p0, Ljav;->i:[I

    array-length v2, v2

    goto :goto_7

    :cond_e
    iput-object v4, p0, Ljav;->i:[I

    :cond_f
    invoke-virtual {p1, v3}, Lizm;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Ljav;->j:Ljbb;

    if-nez v0, :cond_10

    new-instance v0, Ljbb;

    invoke-direct {v0}, Ljbb;-><init>()V

    iput-object v0, p0, Ljav;->j:Ljbb;

    :cond_10
    iget-object v0, p0, Ljav;->j:Ljbb;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Ljav;->b:[Ljah;

    if-nez v0, :cond_12

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Ljah;

    if-eqz v0, :cond_11

    iget-object v3, p0, Ljav;->b:[Ljah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_13

    new-instance v3, Ljah;

    invoke-direct {v3}, Ljah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_12
    iget-object v0, p0, Ljav;->b:[Ljah;

    array-length v0, v0

    goto :goto_9

    :cond_13
    new-instance v3, Ljah;

    invoke-direct {v3}, Ljah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Ljav;->b:[Ljah;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v2, p0, Ljav;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Ljav;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Ljav;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iget v2, p0, Ljav;->c:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_2
    iget v0, p0, Ljav;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget v2, p0, Ljav;->d:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_3
    iget-object v0, p0, Ljav;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x4

    iget-object v2, p0, Ljav;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Ljav;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x5

    iget-object v2, p0, Ljav;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILjava/lang/String;)V

    :cond_5
    iget-object v0, p0, Ljav;->g:Lipv;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v2, p0, Ljav;->g:Lipv;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_6
    iget-object v0, p0, Ljav;->h:Lipv;

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-object v2, p0, Ljav;->h:Lipv;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_7
    iget-object v0, p0, Ljav;->i:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljav;->i:[I

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    :goto_1
    iget-object v2, p0, Ljav;->i:[I

    array-length v2, v2

    if-ge v0, v2, :cond_8

    const/16 v2, 0x8

    iget-object v3, p0, Ljav;->i:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lizn;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget-object v0, p0, Ljav;->j:Ljbb;

    if-eqz v0, :cond_9

    const/16 v0, 0x9

    iget-object v2, p0, Ljav;->j:Ljbb;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_9
    iget-object v0, p0, Ljav;->b:[Ljah;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ljav;->b:[Ljah;

    array-length v0, v0

    if-lez v0, :cond_b

    :goto_2
    iget-object v0, p0, Ljav;->b:[Ljah;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    iget-object v0, p0, Ljav;->b:[Ljah;

    aget-object v0, v0, v1

    if-eqz v0, :cond_a

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lizn;->a(ILizs;)V

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_b
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljav;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljav;

    iget-object v2, p0, Ljav;->a:[Ljava/lang/String;

    iget-object v3, p1, Ljav;->a:[Ljava/lang/String;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljav;->b:[Ljah;

    iget-object v3, p1, Ljav;->b:[Ljah;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Ljav;->c:I

    iget v3, p1, Ljav;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Ljav;->d:I

    iget v3, p1, Ljav;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljav;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Ljav;->e:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljav;->e:Ljava/lang/String;

    iget-object v3, p1, Ljav;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljav;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Ljav;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljav;->f:Ljava/lang/String;

    iget-object v3, p1, Ljav;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljav;->g:Lipv;

    if-nez v2, :cond_b

    iget-object v2, p1, Ljav;->g:Lipv;

    if-eqz v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Ljav;->g:Lipv;

    iget-object v3, p1, Ljav;->g:Lipv;

    invoke-virtual {v2, v3}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p0, Ljav;->h:Lipv;

    if-nez v2, :cond_d

    iget-object v2, p1, Ljav;->h:Lipv;

    if-eqz v2, :cond_e

    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p0, Ljav;->h:Lipv;

    iget-object v3, p1, Ljav;->h:Lipv;

    invoke-virtual {v2, v3}, Lipv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Ljav;->i:[I

    iget-object v3, p1, Ljav;->i:[I

    invoke-static {v2, v3}, Lizq;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Ljav;->j:Ljbb;

    if-nez v2, :cond_10

    iget-object v2, p1, Ljav;->j:Ljbb;

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Ljav;->j:Ljbb;

    iget-object v3, p1, Ljav;->j:Ljbb;

    invoke-virtual {v2, v3}, Ljbb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Ljav;->a:[Ljava/lang/String;

    invoke-static {v0}, Lizq;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljav;->b:[Ljah;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ljav;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ljav;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljav;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljav;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljav;->g:Lipv;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljav;->h:Lipv;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljav;->i:[I

    invoke-static {v2}, Lizq;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljav;->j:Ljbb;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljav;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljav;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljav;->g:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Ljav;->h:Lipv;

    invoke-virtual {v0}, Lipv;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Ljav;->j:Ljbb;

    invoke-virtual {v1}, Ljbb;->hashCode()I

    move-result v1

    goto :goto_4
.end method
