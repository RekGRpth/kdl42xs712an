.class final Lbiw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Lbjd;


# direct methods
.method private constructor <init>(Lbjd;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbiw;-><init>(Lbjd;Z)V

    return-void
.end method

.method private constructor <init>(Lbjd;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbiw;->b:Lbjd;

    iput-boolean p2, p0, Lbiw;->a:Z

    return-void
.end method

.method public static a(Ljava/lang/String;)Lbiw;
    .locals 2

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "separator may not be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lbiw;

    new-instance v1, Lbix;

    invoke-direct {v1, p0}, Lbix;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbiw;-><init>(Lbjd;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lbiw;
    .locals 3

    new-instance v0, Lbiw;

    iget-object v1, p0, Lbiw;->b:Lbjd;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbiw;-><init>(Lbjd;Z)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1

    new-instance v0, Lbiz;

    invoke-direct {v0, p0, p1}, Lbiz;-><init>(Lbiw;Ljava/lang/CharSequence;)V

    return-object v0
.end method
