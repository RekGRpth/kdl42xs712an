.class public final Lfnc;
.super Lfmx;
.source "SourceFile"

# interfaces
.implements Lbpj;


# instance fields
.field final synthetic b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    iput-object p1, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfmx;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lfnc;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lfni;

    move-result-object v0

    iget-object v1, p0, Lfnc;->b:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lfni;->a(Ljava/lang/String;Lbpj;)V

    return-void
.end method
