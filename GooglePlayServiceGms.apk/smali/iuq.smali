.class public abstract Liuq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liut;


# instance fields
.field protected final a:Liva;

.field private b:Z

.field private c:Z

.field private d:Z

.field private final e:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Liuq;->b:Z

    iput-boolean v0, p0, Liuq;->c:Z

    iput-boolean v0, p0, Liuq;->d:Z

    iput-object p1, p0, Liuq;->e:Ljava/lang/String;

    invoke-static {}, Liun;->a()Liun;

    move-result-object v0

    invoke-virtual {v0}, Liun;->c()Liva;

    move-result-object v0

    iput-object v0, p0, Liuq;->a:Liva;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Liuq;->b:Z

    iget-boolean v2, p0, Liuq;->d:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Liuq;->a:Liva;

    iget-object v3, p0, Liuq;->e:Ljava/lang/String;

    invoke-interface {v2, v3}, Liva;->a(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Liuq;->d:Z

    iput-boolean v0, p0, Liuq;->c:Z

    :cond_1
    iget-boolean v0, p0, Liuq;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Liuq;->c:Z

    iget-object v0, p0, Liuq;->a:Liva;

    iget-object v2, p0, Liuq;->e:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [B

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-byte v5, v3, v4

    invoke-interface {v0, v2, v3}, Liva;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
