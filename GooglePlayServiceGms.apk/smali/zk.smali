.class public final Lzk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzn;


# instance fields
.field final a:Lzg;

.field final b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field final c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field final d:Landroid/content/Context;

.field public final e:Ljava/lang/Object;

.field final f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field public g:Lzs;

.field public h:I

.field private final i:Ljava/lang/String;

.field private final j:Lzp;

.field private final k:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lzp;Lzh;Lzg;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lzk;->e:Ljava/lang/Object;

    const/4 v0, -0x2

    iput v0, p0, Lzk;->h:I

    iput-object p1, p0, Lzk;->d:Landroid/content/Context;

    iput-object p2, p0, Lzk;->i:Ljava/lang/String;

    iput-object p3, p0, Lzk;->j:Lzp;

    iget-wide v0, p4, Lzh;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p4, Lzh;->b:J

    :goto_0
    iput-wide v0, p0, Lzk;->k:J

    iput-object p5, p0, Lzk;->a:Lzg;

    iput-object p6, p0, Lzk;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iput-object p7, p0, Lzk;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iput-object p8, p0, Lzk;->f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    return-void

    :cond_0
    const-wide/16 v0, 0x2710

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Lzm;
    .locals 13

    iget-object v6, p0, Lzk;->e:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    new-instance v4, Lzj;

    invoke-direct {v4}, Lzj;-><init>()V

    sget-object v2, Laci;->a:Landroid/os/Handler;

    new-instance v3, Lzl;

    invoke-direct {v3, p0, v4}, Lzl;-><init>(Lzk;Lzj;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-wide v2, p0, Lzk;->k:J

    :goto_0
    iget v5, p0, Lzk;->h:I

    const/4 v7, -0x2

    if-ne v5, v7, :cond_2

    const-wide/32 v7, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    sub-long v11, v9, v0

    sub-long v11, v2, v11

    sub-long/2addr v9, p1

    sub-long/2addr v7, v9

    const-wide/16 v9, 0x0

    cmp-long v5, v11, v9

    if-lez v5, :cond_0

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-gtz v5, :cond_1

    :cond_0
    const-string v5, "Timed out waiting for adapter."

    invoke-static {v5}, Lacj;->b(Ljava/lang/String;)V

    const/4 v5, 0x3

    iput v5, p0, Lzk;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_1
    :try_start_1
    iget-object v5, p0, Lzk;->e:Ljava/lang/Object;

    invoke-static {v11, v12, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v7

    invoke-virtual {v5, v7, v8}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v5

    const/4 v5, -0x1

    :try_start_2
    iput v5, p0, Lzk;->h:I

    goto :goto_0

    :cond_2
    new-instance v0, Lzm;

    iget-object v1, p0, Lzk;->a:Lzg;

    iget-object v2, p0, Lzk;->g:Lzs;

    iget-object v3, p0, Lzk;->i:Ljava/lang/String;

    iget v5, p0, Lzk;->h:I

    invoke-direct/range {v0 .. v5}, Lzm;-><init>(Lzg;Lzs;Ljava/lang/String;Lzj;I)V

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0
.end method

.method final a()Lzs;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Instantiating mediation adapter: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lzk;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lacj;->b(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lzk;->j:Lzp;

    iget-object v1, p0, Lzk;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Lzp;->a(Ljava/lang/String;)Lzs;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not instantiate mediation adapter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lzk;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Lacj;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Ads"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    iget-object v1, p0, Lzk;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput p1, p0, Lzk;->h:I

    iget-object v0, p0, Lzk;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
