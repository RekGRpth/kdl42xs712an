.class public final Lfur;
.super Lbje;
.source "SourceFile"

# interfaces
.implements Lftx;


# instance fields
.field private final f:Lcom/google/android/gms/plus/internal/PlusSession;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)V
    .locals 1

    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/PlusSession;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p3, p4, v0}, Lbje;-><init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V

    iput-object p2, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lftf;->a(Landroid/os/IBinder;)Lfte;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 8

    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->k()Landroid/os/Bundle;

    move-result-object v7

    const-string v0, "skip_oob"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->e()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "required_features"

    iget-object v1, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusSession;->e()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "application_name"

    iget-object v1, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusSession;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const v2, 0x41fe88

    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbje;->c:[Ljava/lang/String;

    iget-object v0, p0, Lfur;->f:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->b()Ljava/lang/String;

    move-result-object v6

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lbjy;->a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lfua;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfuu;

    invoke-direct {v1, p0, p1}, Lfuu;-><init>(Lfur;Lfua;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3}, Lfte;->a(Lftb;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfuu;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfua;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 3

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfuu;

    invoke-direct {v1, p0, p1}, Lfuu;-><init>(Lfur;Lfua;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3, p4}, Lfte;->a(Lftb;Ljava/lang/String;Ljava/util/List;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfuu;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfub;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfuw;

    invoke-direct {v1, p0, p1}, Lfuw;-><init>(Lfur;Lfub;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->e(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, p2, v2}, Lfuw;->a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    goto :goto_0
.end method

.method public final a(Lfuc;ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfuy;

    invoke-direct {v1, p0, p1}, Lfuy;-><init>(Lfur;Lfuc;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3}, Lfte;->a(Lftb;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfuy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lfud;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfus;

    invoke-direct {v1, p0, p1}, Lfus;-><init>(Lfur;Lfud;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->a(Lftb;Lcom/google/android/gms/plus/model/posts/Comment;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfus;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0
.end method

.method public final a(Lfue;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfva;

    invoke-direct {v1, p0, p1}, Lfva;-><init>(Lfur;Lfue;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->c(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfva;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfuf;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvc;

    invoke-direct {v1, p0, p1}, Lfvc;-><init>(Lfur;Lfuf;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->g(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvc;->a(ILandroid/os/Bundle;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Lfug;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfve;

    invoke-direct {v1, p0, p1}, Lfve;-><init>(Lfur;Lfug;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->f(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p2}, Lfve;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lfuh;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvg;

    invoke-direct {v1, p0, p1}, Lfvg;-><init>(Lfur;Lfuh;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v6}, Lfte;->a(Lftb;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lfvg;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final a(Lfui;Landroid/net/Uri;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "bounding_box"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v2, Lfvi;

    invoke-direct {v2, p0, p1}, Lfvi;-><init>(Lfur;Lfui;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v2, p2, v1}, Lfte;->a(Lftb;Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v3, v3}, Lfvi;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lfuj;IILjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvk;

    invoke-direct {v1, p0, p1}, Lfvk;-><init>(Lfur;Lfuj;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3, p4}, Lfte;->b(Lftb;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvk;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0
.end method

.method public final a(Lfuk;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvm;

    invoke-direct {v1, p0, p1}, Lfvm;-><init>(Lfur;Lfuk;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->a(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvm;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfuk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvm;

    invoke-direct {v1, p0, p1}, Lfvm;-><init>(Lfur;Lfuk;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3}, Lfte;->a(Lftb;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvm;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lful;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvo;

    invoke-direct {v1, p0, p1}, Lfvo;-><init>(Lfur;Lful;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->c(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvo;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfum;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvq;

    invoke-direct {v1, p0, p1}, Lfvq;-><init>(Lfur;Lfum;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->b(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvq;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Settings;)V

    goto :goto_0
.end method

.method public final a(Lfun;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvs;

    invoke-direct {v1, p0, p1}, Lfvs;-><init>(Lfur;Lfun;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->a(Lftb;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvs;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_0
.end method

.method public final a(Lfuo;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvu;

    invoke-direct {v1, p0, p1}, Lfvu;-><init>(Lfur;Lfuo;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1}, Lfte;->a(Lftb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvu;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfup;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvw;

    invoke-direct {v1, p0, p1}, Lfvw;-><init>(Lfur;Lfup;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lfte;->a(Lftb;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lfvw;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lfuq;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvy;

    invoke-direct {v1, p0, p1}, Lfvy;-><init>(Lfur;Lfuq;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3}, Lfte;->a(Lftb;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvy;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method public final b(Lfuc;ILjava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfuy;

    invoke-direct {v1, p0, p1}, Lfuy;-><init>(Lfur;Lfuc;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p2, p3}, Lfte;->a(Lftb;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lfuy;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lfuh;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvg;

    invoke-direct {v1, p0, p1}, Lfvg;-><init>(Lfur;Lfuh;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2, p3, p4}, Lfte;->a(Lftb;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvg;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final b(Lfuk;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfur;->h()V

    new-instance v1, Lfvm;

    invoke-direct {v1, p0, p1}, Lfvm;-><init>(Lfur;Lfuk;)V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0, v1, p2}, Lfte;->b(Lftb;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lfvm;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.internal.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lfur;->h()V

    :try_start_0
    invoke-virtual {p0}, Lfur;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfte;

    invoke-interface {v0}, Lfte;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
