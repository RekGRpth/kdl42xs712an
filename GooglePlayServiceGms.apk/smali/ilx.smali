.class public Lilx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lilx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lilx;

    invoke-direct {v0}, Lilx;-><init>()V

    sput-object v0, Lilx;->a:Lilx;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lilx;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    new-instance v0, Lily;

    invoke-direct {v0}, Lily;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lilx;->a:Lilx;

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;)Lilx;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    invoke-static {p0, p1}, Lily;->b(Landroid/content/Intent;Ljava/lang/String;)Lily;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcelable;)Lilx;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    new-instance v0, Lily;

    invoke-direct {v0, p0}, Lily;-><init>(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lilx;->a:Lilx;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/wifi/WifiManager$WifiLock;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/PowerManager$WakeLock;)V
    .locals 0

    return-void
.end method

.method public a(Lilx;)V
    .locals 0

    return-void
.end method

.method public b()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
