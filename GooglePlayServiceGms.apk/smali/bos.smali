.class public abstract Lbos;
.super Lbol;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private e:[Lbot;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lbol;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    new-array v0, v0, [Lbot;

    iput-object v0, p0, Lbos;->e:[Lbot;

    return-void
.end method


# virtual methods
.method protected final b()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lbol;->d:Lbon;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lbon;->a(Ljava/util/ArrayList;I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lbot;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbot;

    iput-object v0, p0, Lbos;->e:[Lbot;

    return-void
.end method

.method public getPositionForSection(I)I
    .locals 1

    iget-object v0, p0, Lbos;->e:[Lbot;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lbos;->e:[Lbot;

    aget-object v0, v0, p1

    iget v0, v0, Lbot;->a:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbos;->e:[Lbot;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lbos;->e:[Lbot;

    aget-object v1, v1, v0

    iget v1, v1, Lbot;->a:I

    if-le v1, p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbos;->e:[Lbot;

    return-object v0
.end method
