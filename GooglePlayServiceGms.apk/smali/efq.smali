.class public final Lefq;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field public a:Leeq;

.field b:I

.field private c:Landroid/content/Context;

.field private d:Limv;

.field private e:Lefs;

.field private f:Z

.field private g:J

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:J

.field private m:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lefs;)V
    .locals 4

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lefq;->c:Landroid/content/Context;

    iput-object p2, p0, Lefq;->e:Lefs;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lefq;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gtalk_wifi_max_heartbeat_ping_interval_ms"

    const v3, 0xdbba0

    invoke-static {v1, v2, v3}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lefq;->j:I

    const-string v1, "gtalk_heartbeat_ack_timeout_ms"

    const v2, 0xea60

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefq;->b:I

    iget-object v0, p0, Lefq;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "gtalk_nosync_heartbeat_ping_interval_ms"

    const v2, 0x19a280

    invoke-static {v0, v1, v2}, Lhhw;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lefq;->h:I

    new-instance v0, Leeq;

    const-string v1, "GCM_HB_ALARM"

    invoke-direct {v0, p1, v1, p0}, Leeq;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/BroadcastReceiver;)V

    iput-object v0, p0, Lefq;->a:Leeq;

    iget-object v0, p0, Lefq;->a:Leeq;

    const-string v1, "com.google.android.intent.action.MCS_HEARTBEAT"

    iput-object v1, v0, Leeq;->b:Ljava/lang/String;

    iget-object v0, p0, Lefq;->a:Leeq;

    invoke-virtual {v0}, Leeq;->b()V

    return-void
.end method

.method private d()V
    .locals 8

    const/4 v7, 0x1

    iget-boolean v0, p0, Lefq;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefq;->a:Leeq;

    iget v1, p0, Lefq;->b:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Leeq;->a(J)V

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lefq;->a:Leeq;

    iget v0, p0, Lefq;->h:I

    int-to-long v2, v0

    iget v0, p0, Lefq;->i:I

    int-to-long v0, v0

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-lez v5, :cond_2

    cmp-long v5, v2, v0

    if-lez v5, :cond_2

    :goto_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lefq;->k:Z

    iget-object v2, p0, Lefq;->e:Lefs;

    invoke-virtual {v2}, Lefs;->e()I

    move-result v2

    if-ne v2, v7, :cond_1

    iget v2, p0, Lefq;->j:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget v0, p0, Lefq;->j:I

    int-to-long v0, v0

    iput-boolean v7, p0, Lefq;->k:Z

    :cond_1
    invoke-virtual {v4, v0, v1}, Leeq;->a(J)V

    goto :goto_0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefq;->f:Z

    iget-object v0, p0, Lefq;->a:Leeq;

    invoke-virtual {v0}, Leeq;->e()V

    return-void
.end method

.method public final a(Limv;)V
    .locals 0

    iput-object p1, p0, Lefq;->d:Limv;

    return-void
.end method

.method public final a(Linn;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p1, Linn;->b:Lini;

    if-eqz v0, :cond_0

    iget v1, v0, Lini;->a:I

    if-lez v1, :cond_0

    iget v0, v0, Lini;->a:I

    iput v0, p0, Lefq;->i:I

    :cond_0
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, Lefq;->g:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefq;->f:Z

    invoke-direct {p0}, Lefq;->d()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lefq;->a:Leeq;

    iget-wide v0, v0, Leeq;->c:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    iget-object v1, p0, Lefq;->e:Lefs;

    invoke-virtual {v1}, Lefs;->e()I

    move-result v1

    iget-object v2, p0, Lefq;->d:Limv;

    invoke-virtual {v2}, Limv;->o()Ljava/lang/String;

    move-result-object v2

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    const v1, 0x31ce4

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-static {v1, v3}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 9

    const-wide/16 v7, 0x3e8

    const-wide/16 v5, 0x0

    iget-boolean v0, p0, Lefq;->f:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Heartbeat waiting ack "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lefq;->a:Leeq;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lefq;->g:J

    cmp-long v2, v2, v5

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last heartbeat reset connection "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lefq;->g:J

    sub-long v3, v0, v3

    div-long/2addr v3, v7

    invoke-static {v3, v4}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ago"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget-wide v2, p0, Lefq;->m:J

    cmp-long v2, v2, v5

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Last ping: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lefq;->m:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lefq;->g:J

    sub-long/2addr v0, v3

    div-long/2addr v0, v7

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Heartbeat: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lefq;->k:Z

    if-eqz v0, :cond_3

    const-string v0, " wifi "

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lefq;->a:Leeq;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, " cell "

    goto :goto_1
.end method

.method public final b()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-boolean v0, p0, Lefq;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefq;->f:Z

    :cond_0
    iget-wide v0, p0, Lefq;->l:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lefq;->l:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lefq;->m:J

    iput-wide v4, p0, Lefq;->l:J

    :cond_1
    invoke-direct {p0}, Lefq;->d()V

    return-void
.end method

.method final c()V
    .locals 1

    iget-object v0, p0, Lefq;->a:Leeq;

    iget-object v0, v0, Leeq;->a:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    iget-object v0, p0, Lefq;->d:Limv;

    invoke-virtual {v0}, Limv;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GCM"

    const-string v1, "Ignoring attempt to send heartbeat on dead connection."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lefq;->f:Z

    if-eqz v0, :cond_1

    const-string v0, "GCM"

    const-string v1, "Heartbeat timeout, GCM connection reset"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lefq;->a()V

    iget-object v0, p0, Lefq;->d:Limv;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Limv;->b(I)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefq;->g:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lefq;->f:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lefq;->a:Leeq;

    iget-object v0, v0, Leeq;->a:Landroid/os/PowerManager$WakeLock;

    iget-object v1, p0, Lefq;->d:Limv;

    invoke-virtual {v1}, Limv;->h()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v0, p0, Lefq;->d:Limv;

    invoke-virtual {v0}, Limv;->i()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lefq;->l:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lefq;->f:Z

    invoke-direct {p0}, Lefq;->d()V

    goto :goto_0
.end method
