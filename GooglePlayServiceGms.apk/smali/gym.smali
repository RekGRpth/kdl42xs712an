.class public final Lgym;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 0

    iput-object p1, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v2, 0x1

    iget-object v1, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v1, v1

    if-lez v1, :cond_3

    iget-object v5, p1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v6, v5

    move v4, v0

    move v1, v0

    move v3, v0

    :goto_0
    if-ge v4, v6, :cond_4

    aget v0, v5, v4

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "UpdateInstrumentActivit"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected error code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    :goto_2
    return-void

    :pswitch_1
    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    instance-of v0, v0, Lgyn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    check-cast v0, Lgyn;

    invoke-virtual {v0}, Lgyn;->b()V

    :cond_0
    if-nez v3, :cond_5

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const v7, 0x7f0b0113    # com.google.android.gms.R.string.wallet_error_creditcard_invalid

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V

    if-nez v3, :cond_5

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const v7, 0x7f0b014d    # com.google.android.gms.R.string.wallet_error_creditcard_expiry_date_invalid

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    instance-of v0, v0, Lgyn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    check-cast v0, Lgyn;

    invoke-virtual {v0}, Lgyn;->J()V

    :cond_1
    if-nez v3, :cond_5

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->t:Lgxf;

    invoke-interface {v0}, Lgxf;->i()Z

    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_3
    const-string v0, "UpdateInstrumentActivit"

    const-string v1, "Unexpected update instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    goto :goto_2

    :cond_5
    move v0, v1

    move v1, v3

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final b(Lioj;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lizs;)V

    const-string v1, "com.google.android.gms.wallet.priorInstrumentId"

    iget-object v2, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Lioj;

    move-result-object v2

    iget-object v2, v2, Lioj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lgym;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    return-void
.end method
