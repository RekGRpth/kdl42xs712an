.class public final Lgzv;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V
    .locals 0

    iput-object p1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->y(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    return-void
.end method

.method public final a(Liot;)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Liot;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Liot;)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->t(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o:Lioj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    :cond_0
    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v0

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->v(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->u(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->w(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->x(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->z(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->A(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->B(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->C(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n:Liot;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->D(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->E(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "settings_update_failure"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->F(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    return-void
.end method

.method public final f()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->a(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;Z)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->l(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Z

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->m(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->n(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lgsm;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lgsm;

    move-result-object v0

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->o(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "settings_update_success"

    invoke-static {v0, v1, v2}, Lgsl;->a(Lgsm;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v0

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->q(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "click_to_activity_result"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Luu;->a(Lut;[Ljava/lang/String;)Z

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    iget-object v1, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->p(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    move-result-object v1

    invoke-static {v0, v1}, Lgsp;->a(Landroid/content/Context;Luu;)V

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->r(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Luu;

    iget-object v0, p0, Lgzv;->a:Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;->s(Lcom/google/android/gms/wallet/ia/BillingInstrumentManagementActivity;)Lut;

    :cond_0
    return-void
.end method
