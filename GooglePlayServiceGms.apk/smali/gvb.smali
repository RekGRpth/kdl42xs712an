.class public final Lgvb;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# instance fields
.field a:Lgwj;

.field b:Lgvc;

.field private c:Z

.field private d:Lgxt;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    iput-boolean v0, p0, Lgvb;->c:Z

    iput-boolean v0, p0, Lgvb;->e:Z

    return-void
.end method

.method private N()V
    .locals 2

    iget-object v0, p0, Lgvb;->a:Lgwj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvb;->a:Lgwj;

    iget-boolean v1, p0, Lgvb;->c:Z

    invoke-virtual {v0, v1}, Lgwj;->a(Z)V

    iget-object v0, p0, Lgvb;->b:Lgvc;

    iget-boolean v1, p0, Lgvb;->c:Z

    invoke-virtual {v0, v1}, Lgvc;->a(Z)V

    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;)Lgvb;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "buyFlowConfig must not be null"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    if-eqz p1, :cond_1

    :goto_1
    const-string v0, "account must not be null"

    invoke-static {v1, v0}, Lbkm;->b(ZLjava/lang/Object;)V

    new-instance v0, Lgvb;

    invoke-direct {v0}, Lgvb;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "cardEntryContext"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "allowedCountryCodes"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "defaultCountryCode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "requiresFullAddress"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "phoneNumberRequired"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "addressHints"

    invoke-static {v1, v2, p9}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lgvb;->g(Landroid/os/Bundle;)V

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgvb;->a:Lgwj;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgvb;->b:Lgvc;

    aput-object v0, v4, v1

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final J()V
    .locals 1

    iget-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0}, Lgvc;->K()V

    return-void
.end method

.method public final K()V
    .locals 1

    iget-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0}, Lgvc;->L()V

    return-void
.end method

.method public final L()V
    .locals 1

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0}, Lgwj;->J()V

    return-void
.end method

.method public final M()V
    .locals 1

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0}, Lgwj;->K()V

    return-void
.end method

.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgvb;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgvb;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    const v11, 0x7f0a0315    # com.google.android.gms.R.id.address_fragment_holder

    const v10, 0x7f0a0314    # com.google.android.gms.R.id.instrument_entry_fragment_holder

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v0, 0x7f04012a    # com.google.android.gms.R.layout.wallet_fragment_add_instrument

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "defaultCountryCode"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhgq;->b(Ljava/lang/String;)I

    move-result v5

    const-string v0, "allowedCountryCodes"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lgty;->a(Ljava/util/List;)[I

    move-result-object v6

    const-string v0, "phoneNumberRequired"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgvb;->e:Z

    invoke-virtual {p0}, Lgvb;->k()Lu;

    move-result-object v0

    invoke-virtual {v0, v11}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgvc;

    iput-object v0, p0, Lgvb;->b:Lgvc;

    iget-object v0, p0, Lgvb;->b:Lgvc;

    if-nez v0, :cond_0

    const-string v0, "requiresFullAddress"

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v7, "addressHints"

    const-class v8, Lipv;

    invoke-static {v4, v7, v8}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lgty;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v8

    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lgvq;

    move-result-object v9

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v9, v0}, Lgvq;->a(Z)Lgvq;

    move-result-object v0

    iget-object v1, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object v6, v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    iget-object v1, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput v5, v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    invoke-virtual {v0}, Lgvq;->a()Lgvq;

    move-result-object v1

    iget-object v0, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lgvq;->a(Ljava/util/ArrayList;)Lgvq;

    move-result-object v0

    iget-object v0, v0, Lgvq;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iget-boolean v1, p0, Lgvb;->e:Z

    invoke-static {v0, v7, v1, v2}, Lgvc;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;Ljava/util/Collection;ZZ)Lgvc;

    move-result-object v0

    iput-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {p0}, Lgvb;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0, v11, v1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    iget-object v0, p0, Lgvb;->b:Lgvc;

    iget-object v1, p0, Lgvb;->d:Lgxt;

    invoke-virtual {v0, v1}, Lgvc;->a(Lgxt;)V

    invoke-virtual {p0}, Lgvb;->k()Lu;

    move-result-object v0

    invoke-virtual {v0, v10}, Lu;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwj;

    iput-object v0, p0, Lgvb;->a:Lgwj;

    iget-object v0, p0, Lgvb;->a:Lgwj;

    if-nez v0, :cond_1

    const-string v0, "buyFlowConfig"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v1, "cardEntryContext"

    invoke-virtual {v4, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string v1, "account"

    invoke-virtual {v4, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    const-string v5, "disallowedCreditCardTypes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    const-string v6, "disallowedCardCategories"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    invoke-static {v0, v1, v2, v5, v4}, Lgwj;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[I)Lgwj;

    move-result-object v0

    iput-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {p0}, Lgvb;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0, v10, v1}, Lag;->b(ILandroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_1
    invoke-direct {p0}, Lgvb;->N()V

    return-object v3

    :cond_2
    move v0, v2

    goto/16 :goto_0
.end method

.method public final a()Linv;
    .locals 3

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0}, Lgwj;->a()Linv;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lizs;)Lizs;

    move-result-object v0

    check-cast v0, Linv;

    iget-object v1, v0, Linv;->b:Lint;

    iget-object v2, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v2}, Lgvc;->a()Lixo;

    move-result-object v2

    iput-object v2, v1, Lint;->d:Lixo;

    iget-boolean v1, p0, Lgvb;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v1}, Lgvc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Linv;->b:Lint;

    iget-object v2, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v2}, Lgvc;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lint;->g:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public final a(Lgxt;)V
    .locals 1

    iput-object p1, p0, Lgvb;->d:Lgxt;

    iget-object v0, p0, Lgvb;->b:Lgvc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0, p1}, Lgvc;->a(Lgxt;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0, p1}, Lgwj;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgvb;->c:Z

    invoke-direct {p0}, Lgvb;->N()V

    return-void
.end method

.method public final a([I)V
    .locals 1

    iget-object v0, p0, Lgvb;->a:Lgwj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0, p1}, Lgwj;->a([I)V

    :cond_0
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgvb;->c:Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0}, Lgvc;->J()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgvb;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lgvb;->a:Lgwj;

    invoke-virtual {v0}, Lgwj;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgvb;->b:Lgvc;

    invoke-virtual {v0}, Lgvc;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
