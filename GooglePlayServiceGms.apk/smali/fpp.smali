.class public abstract Lfpp;
.super Lah;
.source "SourceFile"

# interfaces
.implements Lfoc;


# instance fields
.field private Y:Ljj;

.field private Z:Z

.field protected i:Lfob;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lah;-><init>()V

    return-void
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Lah;->E_()V

    iget-object v0, p0, Lfpp;->i:Lfob;

    iget-object v0, v0, Lfob;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lfpp;->c()V

    return-void
.end method

.method public abstract J()I
.end method

.method public abstract K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
.end method

.method protected final P()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, ""

    invoke-virtual {p0, v0}, Lfpp;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lfpp;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {p0}, Lfpp;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0171    # com.google.android.gms.R.dimen.plus_empty_text_padding

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 1

    invoke-super {p0, p1}, Lah;->a(Landroid/view/Menu;)V

    iget-object v0, p0, Lfpp;->i:Lfob;

    invoke-virtual {v0, p1}, Lfob;->a(Landroid/view/Menu;)V

    return-void
.end method

.method abstract a(Ljj;)V
.end method

.method public a_(Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lfpp;->i:Lfob;

    invoke-virtual {v0, p1}, Lfob;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lah;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljj;)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lfpp;->a(Ljj;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpp;->Z:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpp;->Z:Z

    iput-object p1, p0, Lfpp;->Y:Ljj;

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lah;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->B:Lw;

    const-string v1, "apps_util"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lfob;

    iput-object v0, p0, Lfpp;->i:Lfob;

    return-void
.end method

.method public final g_()V
    .locals 1

    invoke-super {p0}, Lah;->g_()V

    iget-object v0, p0, Lfpp;->i:Lfob;

    iget-object v0, v0, Lfob;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final w()V
    .locals 1

    invoke-super {p0}, Lah;->w()V

    iget-boolean v0, p0, Lfpp;->Z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpp;->Y:Ljj;

    invoke-virtual {p0, v0}, Lfpp;->a(Ljj;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lfpp;->Y:Ljj;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpp;->Z:Z

    :cond_0
    return-void
.end method
