.class final Labk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field final synthetic c:Labm;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Labm;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Labk;->a:Landroid/content/Context;

    iput-object p2, p0, Labk;->b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iput-object p3, p0, Labk;->c:Labm;

    iput-object p4, p0, Labk;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Labk;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>()V

    iget-object v3, p0, Labk;->b:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v5, v3, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lacl;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLuf;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lacl;

    move-result-object v5

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lacl;->setWillNotDraw(Z)V

    iget-object v0, p0, Labk;->c:Labm;

    iget-object v1, v0, Labm;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object v5, v0, Labm;->b:Lacl;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v5}, Lacl;->e()Lacn;

    move-result-object v0

    const-string v1, "/invalidRequest"

    iget-object v2, p0, Labk;->c:Labm;

    iget-object v2, v2, Labm;->f:Lyz;

    invoke-virtual {v0, v1, v2}, Lacn;->a(Ljava/lang/String;Lyz;)V

    const-string v1, "/loadAdURL"

    iget-object v2, p0, Labk;->c:Labm;

    iget-object v2, v2, Labm;->g:Lyz;

    invoke-virtual {v0, v1, v2}, Lacn;->a(Ljava/lang/String;Lyz;)V

    const-string v1, "/log"

    sget-object v2, Lyr;->f:Lyz;

    invoke-virtual {v0, v1, v2}, Lacn;->a(Ljava/lang/String;Lyz;)V

    const-string v0, "Getting the ad request URL."

    invoke-static {v0}, Lacj;->a(Ljava/lang/String;)V

    const-string v6, "http://googleads.g.doubleclick.net"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<!DOCTYPE html><html><head><script src=\"http://googleads.g.doubleclick.net/mads/static/sdk/native/sdk-core-v40.js\"></script><script>AFMA_buildAdURL("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Labk;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");</script></head><body></body></html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "text/html"

    const-string v9, "UTF-8"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lacl;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
