.class public final Lihq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lihy;


# direct methods
.method public constructor <init>(Lihy;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lihq;->a:Lihy;

    return-void
.end method

.method private a(I)Ljavax/crypto/Cipher;
    .locals 3

    const-string v0, "AES/CBC/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    const/16 v1, 0x10

    new-array v1, v1, [B

    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    iget-object v1, p0, Lihq;->a:Lihy;

    invoke-virtual {v1}, Lihy;->b()Ljavax/crypto/spec/SecretKeySpec;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    return-object v0
.end method


# virtual methods
.method public final a([B)[B
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lihq;->a(I)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method

.method public final b([B)[B
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lihq;->a(I)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    return-object v0
.end method
