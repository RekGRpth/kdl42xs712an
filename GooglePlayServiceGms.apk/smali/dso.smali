.class public final Ldso;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldso;->b:Ldad;

    iput-object p3, p0, Ldso;->c:Ljava/lang/String;

    iput-boolean p4, p0, Ldso;->d:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-boolean v0, p0, Ldso;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldso;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->g(Lcom/google/android/gms/common/data/DataHolder;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldso;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->f(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    iget-object v2, p0, Ldso;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldso;->c:Ljava/lang/String;

    iget-boolean v4, p0, Ldso;->d:Z

    const/4 v5, 0x0

    iget-boolean v6, p0, Ldso;->d:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLjava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
