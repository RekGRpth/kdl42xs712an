.class public abstract Lnz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Loc;

.field public final c:Lob;

.field d:Loa;

.field public e:Lny;

.field f:Z

.field public g:Loe;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lnz;-><init>(Landroid/content/Context;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lob;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lob;-><init>(Lnz;B)V

    iput-object v0, p0, Lnz;->c:Lob;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lnz;->a:Landroid/content/Context;

    new-instance v0, Loc;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Loc;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, Lnz;->b:Loc;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lod;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lny;)V
    .locals 0

    return-void
.end method
