.class public final Lecg;
.super Ldxe;
.source "SourceFile"

# interfaces
.implements Lbel;
.implements Ldfz;
.implements Leac;


# instance fields
.field private ad:Leaa;

.field private ae:Ldxi;

.field private af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

.field private ag:Landroid/widget/ListView;

.field private ah:Leac;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ldxe;-><init>()V

    return-void
.end method

.method private V()V
    .locals 4

    iget-object v0, p0, Lecg;->ad:Leaa;

    invoke-virtual {v0}, Leaa;->d()I

    move-result v1

    iget-object v2, p0, Lecg;->ae:Ldxi;

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ldxi;->b(Z)V

    iget-object v0, p0, Lecg;->Z:Leds;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Leds;->a(I)V

    iget-object v0, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v2, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->c()I

    move-result v2

    iget-object v3, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->d()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()V
    .locals 4

    invoke-virtual {p0}, Lecg;->J()Lbdu;

    move-result-object v1

    invoke-interface {v1}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "InvitationListFragment"

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lecg;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecg;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    invoke-virtual {p0}, Lecg;->M()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    sget-object v3, Lcte;->j:Ldgs;

    invoke-interface {v3, v1, v0, v2}, Ldgs;->a(Lbdu;Ljava/lang/String;[I)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method public final A_()V
    .locals 0

    invoke-direct {p0}, Lecg;->W()V

    return-void
.end method

.method public final B_()V
    .locals 0

    invoke-direct {p0}, Lecg;->W()V

    return-void
.end method

.method public final M_()V
    .locals 3

    invoke-super {p0}, Ldxe;->M_()V

    invoke-virtual {p0}, Lecg;->T()V

    invoke-virtual {p0}, Lecg;->K()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "InvitationListFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lecg;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcte;->i:Ldfx;

    invoke-virtual {p0}, Lecg;->M()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ldfx;->a(Lbdu;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final R()V
    .locals 0

    invoke-direct {p0}, Lecg;->W()V

    return-void
.end method

.method protected final U()V
    .locals 0

    invoke-direct {p0}, Lecg;->W()V

    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const v0, 0x7f040066    # com.google.android.gms.R.layout.games_inbox_list_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    invoke-super {p0, p1, p2, p3}, Ldxe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0163    # com.google.android.gms.R.id.quick_access_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iput-object v0, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a()V

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lecg;->ag:Landroid/widget/ListView;

    return-object v1
.end method

.method public final a(Lbdu;)V
    .locals 3

    iget-object v0, p0, Lecg;->ad:Leaa;

    invoke-virtual {p0}, Lecg;->N()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lecg;->O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Leaa;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lecg;->M()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    sget-object v2, Lcte;->j:Ldgs;

    invoke-interface {v2, p1, v0, v1}, Ldgs;->a(Lbdu;Ljava/lang/String;[I)Lbeh;

    move-result-object v1

    invoke-interface {v1, p0}, Lbeh;->a(Lbel;)V

    sget-object v1, Lcte;->n:Lctp;

    const/4 v2, 0x1

    invoke-interface {v1, p1, v0, v2}, Lctp;->a(Lbdu;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lecg;->S()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data
.end method

.method public final synthetic a(Lbek;)V
    .locals 7

    check-cast p1, Ldgu;

    invoke-interface {p1}, Ldgu;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-interface {p1}, Ldgu;->g()Ldgo;

    move-result-object v2

    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->J:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v2}, Ldgo;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lecg;->Y:Ldvn;

    invoke-virtual {v0, v1}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Ldgo;->a()V

    goto :goto_0

    :cond_3
    :try_start_2
    iget-object v0, p0, Lecg;->Y:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecg;->Y:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-object v0, v2, Ldgo;->a:Ldfu;

    invoke-virtual {v0}, Ldfu;->a()I

    move-result v3

    iget-object v4, v2, Ldgo;->b:Ldgq;

    invoke-virtual {v4}, Ldgq;->a()I

    move-result v4

    iget-object v5, v2, Ldgo;->c:Ldgq;

    invoke-virtual {v5}, Ldgq;->a()I

    move-result v5

    iget-object v6, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-virtual {v6, v3, v4, v5}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(III)V

    iget-object v4, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    invoke-static {v4}, Leeh;->a(Landroid/view/View;)V

    iget-object v4, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v5, p0, Lecg;->ag:Landroid/widget/ListView;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Landroid/widget/ListView;)V

    iget-object v4, v2, Ldgo;->b:Ldgq;

    invoke-virtual {v4}, Ldgq;->b()V

    iget-object v4, v2, Ldgo;->c:Ldgq;

    invoke-virtual {v4}, Ldgq;->b()V

    new-instance v4, Ldzz;

    invoke-direct {v4, v0}, Ldzz;-><init>(Lbgo;)V

    iget-object v0, p0, Lecg;->ad:Leaa;

    invoke-virtual {v0, v4}, Leaa;->a(Ldzz;)V

    iget-object v0, p0, Lecg;->ae:Ldxi;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ldxi;->b(Z)V

    if-lez v3, :cond_4

    iget-object v0, p0, Lecg;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    invoke-virtual {v2}, Ldgo;->a()V

    invoke-virtual {p0}, Lecg;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcte;->i:Ldfx;

    invoke-virtual {p0}, Lecg;->M()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, p0, v2}, Ldfx;->a(Lbdu;Ldfz;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-eqz v1, :cond_5

    :try_start_3
    iget-object v0, p0, Lecg;->Z:Leds;

    const v3, 0x7f0b02b2    # com.google.android.gms.R.string.games_invitation_list_generic_error

    const v4, 0x7f0b02b1    # com.google.android.gms.R.string.games_invitation_list_null_state

    invoke-virtual {v0, v1, v3, v4}, Leds;->a(III)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ldgo;->a()V

    throw v0

    :cond_5
    :try_start_4
    iget-object v0, p0, Lecg;->ae:Ldxi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldxi;->b(Z)V

    iget-object v0, p0, Lecg;->Z:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    invoke-direct {p0}, Lecg;->V()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1, p2, p3}, Leac;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1, p2, p3}, Leac;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a_(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->a_(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->b(Lcom/google/android/gms/games/Game;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    invoke-direct {p0}, Lecg;->V()V

    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Lecg;->V()V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 8

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Ldxe;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lecg;->Y:Ldvn;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessInboxListActivity;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecg;->Y:Ldvn;

    instance-of v0, v0, Ldzy;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lecg;->Y:Ldvn;

    check-cast v0, Ldzy;

    invoke-interface {v0}, Ldzy;->D_()Ldzx;

    move-result-object v0

    iput-object v0, p0, Lecg;->ah:Leac;

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-static {v0}, Lbiq;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lecg;->af:Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;

    iget-object v0, p0, Lecg;->Y:Ldvn;

    check-cast v0, Ldwt;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MatchQuickAccessView;->a(Ldwt;)V

    new-instance v0, Leaa;

    iget-object v1, p0, Lecg;->Y:Ldvn;

    invoke-direct {v0, v1, p0}, Leaa;-><init>(Ldvn;Leac;)V

    iput-object v0, p0, Lecg;->ad:Leaa;

    new-instance v0, Ldxi;

    iget-object v1, p0, Lecg;->Y:Ldvn;

    const v2, 0x7f02017d    # com.google.android.gms.R.drawable.illo_null_match_inbox

    const v3, 0x7f0b02b3    # com.google.android.gms.R.string.games_invitation_null_state_text

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Ldxi;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;)V

    iput-object v0, p0, Lecg;->ae:Ldxi;

    iget-object v0, p0, Lecg;->ae:Ldxi;

    invoke-virtual {v0, v4}, Ldxi;->b(Z)V

    new-instance v0, Ldwu;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lecg;->ad:Leaa;

    aput-object v2, v1, v4

    iget-object v2, p0, Lecg;->ae:Ldxi;

    aput-object v2, v1, v7

    invoke-direct {v0, v1}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Lecg;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lecg;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    iget-object v0, p0, Lecg;->ah:Leac;

    invoke-interface {v0, p1}, Leac;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    invoke-direct {p0}, Lecg;->V()V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lecg;->ad:Leaa;

    invoke-virtual {v0}, Leaa;->a()V

    invoke-super {p0}, Ldxe;->f()V

    return-void
.end method
