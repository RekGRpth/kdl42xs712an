.class public final Lcgb;
.super Lcfl;
.source "SourceFile"


# instance fields
.field public a:Lbuw;

.field public b:Ljava/lang/Long;

.field private final c:J


# direct methods
.method public constructor <init>(Lcdu;JLbuv;Ljava/lang/String;Ljava/lang/Long;J)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {p0, p1, v0, v3}, Lcfl;-><init>(Lcdu;Lcdt;Landroid/net/Uri;)V

    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    cmp-long v0, p7, v4

    if-ltz v0, :cond_1

    :goto_1
    invoke-static {v1}, Lbkm;->b(Z)V

    invoke-static {p5, p6, p7, p8}, Lcgb;->a(Ljava/lang/String;Ljava/lang/Long;J)V

    iput-wide p2, p0, Lcgb;->c:J

    invoke-static {p4, p5, p7, p8}, Lbuw;->a(Lbuv;Ljava/lang/String;J)Lbuw;

    move-result-object v0

    iput-object v0, p0, Lcgb;->a:Lbuw;

    iput-object p6, p0, Lcgb;->b:Ljava/lang/Long;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static a(Lcdu;Landroid/database/Cursor;)Lcgb;
    .locals 9

    sget-object v0, Lceo;->a:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lceo;->d:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lceo;->e:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    sget-object v0, Lceo;->f:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->c(Landroid/database/Cursor;)J

    move-result-wide v7

    sget-object v0, Lceo;->b:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lceo;->c:Lceo;

    invoke-virtual {v1}, Lceo;->a()Lcdp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcdp;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lbux;->b(Ljava/lang/String;)Lbux;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbux;->a(Ljava/lang/String;)Lbuv;

    move-result-object v4

    new-instance v0, Lcgb;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcgb;-><init>(Lcdu;JLbuv;Ljava/lang/String;Ljava/lang/Long;J)V

    invoke-static {}, Lcen;->a()Lcen;

    move-result-object v1

    invoke-virtual {v1}, Lcen;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcdp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcgb;->d(J)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Long;J)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    move v3, v0

    :goto_0
    if-nez p1, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid nextUri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", clipTime="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    return-void

    :cond_0
    move v3, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 3

    sget-object v0, Lceo;->a:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcgb;->c:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceo;->b:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgb;->a:Lbuw;

    iget-object v1, v1, Lbuw;->a:Lbuv;

    iget-object v1, v1, Lbuv;->c:Lbux;

    invoke-virtual {v1}, Lbux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceo;->c:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgb;->a:Lbuw;

    iget-object v1, v1, Lbuw;->a:Lbuv;

    invoke-virtual {v1}, Lbuv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceo;->d:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgb;->a:Lbuw;

    invoke-virtual {v1}, Lbuw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lceo;->e:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgb;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    sget-object v0, Lceo;->f:Lceo;

    invoke-virtual {v0}, Lceo;->a()Lcdp;

    move-result-object v0

    invoke-virtual {v0}, Lcdp;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcgb;->a:Lbuw;

    iget-wide v1, v1, Lbuw;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    iget-object v0, p0, Lcgb;->a:Lbuw;

    iget-wide v0, v0, Lbuw;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Lcgb;->a(Ljava/lang/String;Ljava/lang/Long;J)V

    iget-object v0, p0, Lcgb;->a:Lbuw;

    invoke-static {v0, p1}, Lbuw;->a(Lbuw;Ljava/lang/String;)Lbuw;

    move-result-object v0

    iput-object v0, p0, Lcgb;->a:Lbuw;

    iput-object p2, p0, Lcgb;->b:Ljava/lang/Long;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    const-string v0, "PartialFeed[accountSqlId=%s, clipTime=%s, sqlId=%s, feedState=%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v3, p0, Lcgb;->c:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcgb;->b:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v3, p0, Lcfl;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcgb;->a:Lbuw;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
