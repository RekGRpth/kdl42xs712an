.class public final Legf;
.super Legy;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/gcm/http/GoogleHttpService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/gcm/http/GoogleHttpService;)V
    .locals 0

    iput-object p1, p0, Legf;->a:Lcom/google/android/gms/gcm/http/GoogleHttpService;

    invoke-direct {p0}, Legy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Legf;->a:Lcom/google/android/gms/gcm/http/GoogleHttpService;

    invoke-static {v0, p1}, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a(Lcom/google/android/gms/gcm/http/GoogleHttpService;Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Legf;->a:Lcom/google/android/gms/gcm/http/GoogleHttpService;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/gcm/http/GoogleHttpService;->a(Lcom/google/android/gms/gcm/http/GoogleHttpService;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method
