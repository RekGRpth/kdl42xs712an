.class public final Lfzg;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Lfzh;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lfzh;

    invoke-direct {v0}, Lfzh;-><init>()V

    iput-object v0, p0, Lfzg;->a:Lfzh;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Lfzg;
    .locals 2

    new-instance v0, Lfzg;

    invoke-direct {v0}, Lfzg;-><init>()V

    invoke-static {p0, p1}, Lfzh;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfzg;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lfzg;->a:Lfzh;

    invoke-virtual {v0}, Lfzh;->b()V

    return-void
.end method

.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    iget-object v0, p0, Lfzg;->a:Lfzh;

    invoke-virtual {v0}, Lfzh;->a()V

    return-void
.end method

.method public final a()Lfze;
    .locals 1

    iget-object v0, p0, Lfzg;->a:Lfzh;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget-object v0, p0, Lfzg;->a:Lfzh;

    invoke-virtual {v0, p1}, Lfzh;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfzg;->q()V

    iget-object v0, p0, Lfzg;->a:Lfzh;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lfzh;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public final y()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lfzg;->a:Lfzh;

    invoke-virtual {v0}, Lfzh;->c()V

    return-void
.end method
