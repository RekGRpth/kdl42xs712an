.class public final Ldsv;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Z

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsv;->b:Ldad;

    iput-object p3, p0, Ldsv;->c:Ljava/lang/String;

    iput p4, p0, Ldsv;->d:I

    iput-boolean p5, p0, Ldsv;->e:Z

    iput-object p6, p0, Ldsv;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldsv;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->j(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v0, p0, Ldsv;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldsv;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Ldsv;->f:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcun;->f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Ldsv;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldsv;->c:Ljava/lang/String;

    iget v4, p0, Ldsv;->d:I

    iget-boolean v5, p0, Ldsv;->e:Z

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
