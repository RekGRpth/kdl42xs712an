.class public abstract Lbfy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lbgf;

.field private static final d:Ljava/lang/Object;


# instance fields
.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbfy;->d:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lbfy;->a:Lbgf;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbfy;->e:Ljava/lang/Object;

    iput-object p1, p0, Lbfy;->b:Ljava/lang/String;

    iput-object p2, p0, Lbfy;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Double;)Lbfy;
    .locals 1

    new-instance v0, Lbgc;

    invoke-direct {v0, p0, p1}, Lbgc;-><init>(Ljava/lang/String;Ljava/lang/Double;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Float;)Lbfy;
    .locals 1

    new-instance v0, Lbgd;

    invoke-direct {v0, p0, p1}, Lbgd;-><init>(Ljava/lang/String;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;
    .locals 1

    new-instance v0, Lbgb;

    invoke-direct {v0, p0, p1}, Lbgb;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;
    .locals 1

    new-instance v0, Lbga;

    invoke-direct {v0, p0, p1}, Lbga;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lbfy;
    .locals 1

    new-instance v0, Lbge;

    invoke-direct {v0, p0, p1}, Lbge;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lbfy;
    .locals 2

    new-instance v0, Lbfz;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbfz;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    sget-object v1, Lbfy;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lbfy;->a:Lbgf;

    if-nez v0, :cond_0

    new-instance v0, Lbgg;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v2}, Lbgg;-><init>(Landroid/content/ContentResolver;)V

    sput-object v0, Lbfy;->a:Lbgf;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Z
    .locals 1

    sget-object v0, Lbfy;->a:Lbgf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract b()Ljava/lang/Object;
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lbfy;->e:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbfy;->e:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbfy;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lbfy;->b()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Object;
    .locals 3

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    :try_start_0
    invoke-virtual {p0}, Lbfy;->c()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbfy;->b:Ljava/lang/String;

    return-object v0
.end method
