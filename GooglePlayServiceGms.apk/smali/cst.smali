.class public final Lcst;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Runnable;

.field private static b:Landroid/os/Handler;

.field private static c:Lcsw;

.field private static d:Lcsx;

.field private static volatile e:Ljava/lang/Process;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcsu;

    invoke-direct {v0}, Lcsu;-><init>()V

    sput-object v0, Lcst;->a:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcst;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Ljava/lang/Process;)Ljava/lang/Process;
    .locals 0

    sput-object p0, Lcst;->e:Ljava/lang/Process;

    return-object p0
.end method

.method public static declared-synchronized a()V
    .locals 5

    const-class v1, Lcst;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcst;->c:Lcsw;

    if-nez v0, :cond_0

    new-instance v0, Lcsw;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcsw;-><init>(B)V

    sput-object v0, Lcst;->c:Lcsw;

    invoke-virtual {v0}, Lcsw;->start()V

    sget-object v0, Lcst;->b:Landroid/os/Handler;

    sget-object v2, Lcst;->a:Ljava/lang/Runnable;

    const-wide/16 v3, 0x3a98

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcst;->c:Lcsw;

    invoke-virtual {v0}, Lcsw;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcsx;)V
    .locals 0

    sput-object p0, Lcst;->d:Lcsx;

    return-void
.end method

.method static synthetic a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcst;->b:Landroid/os/Handler;

    sget-object v1, Lcst;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v0, Lcst;->b:Landroid/os/Handler;

    new-instance v1, Lcsv;

    invoke-direct {v1, p0, p1}, Lcsv;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcst;->c:Lcsw;

    return-void
.end method

.method static synthetic c()V
    .locals 1

    sget-object v0, Lcst;->e:Ljava/lang/Process;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    :cond_0
    return-void
.end method

.method static synthetic d()Lcsx;
    .locals 1

    sget-object v0, Lcst;->d:Lcsx;

    return-object v0
.end method
