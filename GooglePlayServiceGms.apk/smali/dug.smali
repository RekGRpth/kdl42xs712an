.class public final Ldug;
.super Ldrv;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:[I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I[I)V
    .locals 1

    array-length v0, p5

    invoke-direct {p0, p1, v0}, Ldrv;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    iput-object p2, p0, Ldug;->b:Ldad;

    iput-object p3, p0, Ldug;->c:Ljava/lang/String;

    iput p4, p0, Ldug;->d:I

    iput-object p5, p0, Ldug;->e:[I

    const/4 v0, 0x1

    iput v0, p0, Ldug;->f:I

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcun;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const/4 v4, 0x0

    if-ltz p3, :cond_1

    iget-object v0, p0, Ldug;->e:[I

    array-length v0, v0

    if-ge p3, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    if-nez p3, :cond_0

    iget-object v2, p0, Ldug;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldug;->c:Ljava/lang/String;

    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I

    move-result v0

    iput v0, p0, Ldug;->f:I

    :cond_0
    iget-object v0, p0, Ldug;->e:[I

    aget v4, v0, p3

    if-nez v4, :cond_2

    iget-object v2, p0, Ldug;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldug;->c:Ljava/lang/String;

    iget v4, p0, Ldug;->d:I

    iget v5, p0, Ldug;->f:I

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    move v0, v4

    goto :goto_0

    :cond_2
    iget-object v2, p0, Ldug;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldug;->c:Ljava/lang/String;

    iget v5, p0, Ldug;->f:I

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Ldug;->e:[I

    array-length v0, v0

    array-length v2, p1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    move v0, v1

    :goto_1
    iget-object v3, p0, Ldug;->e:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Ldug;->e:[I

    aget v3, v3, v1

    invoke-static {v3}, Ldeh;->a(I)Ljava/lang/String;

    move-result-object v3

    aget-object v4, p1, v1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    add-int/2addr v0, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ldug;->b:Ldad;

    iget v3, p0, Ldug;->f:I

    invoke-static {v0, v3}, Lcum;->a(II)I

    move-result v0

    invoke-interface {v1, v0, v2}, Ldad;->a(ILandroid/os/Bundle;)V

    return-void
.end method
