.class public final Lflg;
.super Lizp;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Lfla;

.field public c:I

.field public d:[Lfle;

.field public e:[[B


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    invoke-direct {p0}, Lizp;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lflg;->a:J

    iput-object v3, p0, Lflg;->b:Lfla;

    iput v2, p0, Lflg;->c:I

    invoke-static {}, Lfle;->c()[Lfle;

    move-result-object v0

    iput-object v0, p0, Lflg;->d:[Lfle;

    sget-object v0, Lizv;->g:[[B

    iput-object v0, p0, Lflg;->e:[[B

    iput-object v3, p0, Lflg;->q:Ljava/util/List;

    iput v2, p0, Lflg;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Lizp;->a()I

    move-result v0

    iget-object v2, p0, Lflg;->b:Lfla;

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Lflg;->b:Lfla;

    invoke-static {v2, v3}, Lizn;->b(ILizs;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lflg;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const/4 v2, 0x2

    iget v3, p0, Lflg;->c:I

    invoke-static {v2, v3}, Lizn;->e(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lflg;->d:[Lfle;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lflg;->d:[Lfle;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lflg;->d:[Lfle;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lflg;->d:[Lfle;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-static {v4, v3}, Lizn;->b(ILizs;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    :cond_4
    iget-wide v2, p0, Lflg;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    iget-wide v3, p0, Lflg;->a:J

    invoke-static {v2, v3, v4}, Lizn;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lflg;->e:[[B

    if-eqz v2, :cond_8

    iget-object v2, p0, Lflg;->e:[[B

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    move v3, v1

    :goto_1
    iget-object v4, p0, Lflg;->e:[[B

    array-length v4, v4

    if-ge v1, v4, :cond_7

    iget-object v4, p0, Lflg;->e:[[B

    aget-object v4, v4, v1

    if-eqz v4, :cond_6

    add-int/lit8 v3, v3, 0x1

    invoke-static {v4}, Lizn;->a([B)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    add-int/2addr v0, v2

    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    :cond_8
    iput v0, p0, Lflg;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lflg;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lflg;->b:Lfla;

    if-nez v0, :cond_1

    new-instance v0, Lfla;

    invoke-direct {v0}, Lfla;-><init>()V

    iput-object v0, p0, Lflg;->b:Lfla;

    :cond_1
    iget-object v0, p0, Lflg;->b:Lfla;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lflg;->c:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lflg;->d:[Lfle;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lfle;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lflg;->d:[Lfle;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lfle;

    invoke-direct {v3}, Lfle;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lizm;->a(Lizs;)V

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lflg;->d:[Lfle;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lfle;

    invoke-direct {v3}, Lfle;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    iput-object v2, p0, Lflg;->d:[Lfle;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lflg;->a:J

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lizv;->b(Lizm;I)I

    move-result v2

    iget-object v0, p0, Lflg;->e:[[B

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_5

    iget-object v3, p0, Lflg;->e:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lizm;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lflg;->e:[[B

    array-length v0, v0

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lflg;->e:[[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lflg;->b:Lfla;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v2, p0, Lflg;->b:Lfla;

    invoke-virtual {p1, v0, v2}, Lizn;->a(ILizs;)V

    :cond_0
    iget v0, p0, Lflg;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    const/4 v0, 0x2

    iget v2, p0, Lflg;->c:I

    invoke-virtual {p1, v0, v2}, Lizn;->a(II)V

    :cond_1
    iget-object v0, p0, Lflg;->d:[Lfle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflg;->d:[Lfle;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    iget-object v2, p0, Lflg;->d:[Lfle;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lflg;->d:[Lfle;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lizn;->a(ILizs;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lflg;->a:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget-wide v2, p0, Lflg;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lizn;->b(IJ)V

    :cond_4
    iget-object v0, p0, Lflg;->e:[[B

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflg;->e:[[B

    array-length v0, v0

    if-lez v0, :cond_6

    :goto_1
    iget-object v0, p0, Lflg;->e:[[B

    array-length v0, v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lflg;->e:[[B

    aget-object v0, v0, v1

    if-eqz v0, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lizn;->a(I[B)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    invoke-super {p0, p1}, Lizp;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lflg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lflg;

    iget-wide v2, p0, Lflg;->a:J

    iget-wide v4, p1, Lflg;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lflg;->b:Lfla;

    if-nez v2, :cond_4

    iget-object v2, p1, Lflg;->b:Lfla;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lflg;->b:Lfla;

    iget-object v3, p1, Lflg;->b:Lfla;

    invoke-virtual {v2, v3}, Lfla;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lflg;->c:I

    iget v3, p1, Lflg;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lflg;->d:[Lfle;

    iget-object v3, p1, Lflg;->d:[Lfle;

    invoke-static {v2, v3}, Lizq;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lflg;->e:[[B

    iget-object v3, p1, Lflg;->e:[[B

    invoke-static {v2, v3}, Lizq;->a([[B[[B)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lflg;->q:Ljava/util/List;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lflg;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    iget-object v2, p1, Lflg;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lflg;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v0, p0, Lflg;->q:Ljava/util/List;

    iget-object v1, p1, Lflg;->q:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Lflg;->a:J

    iget-wide v4, p0, Lflg;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lflg;->b:Lfla;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lflg;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lflg;->d:[Lfle;

    invoke-static {v2}, Lizq;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lflg;->e:[[B

    invoke-static {v2}, Lizq;->a([[B)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lflg;->q:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lflg;->q:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lflg;->b:Lfla;

    invoke-virtual {v0}, Lfla;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lflg;->q:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1
.end method
