.class public abstract Livq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[Livq;


# instance fields
.field protected a:Livs;

.field protected b:Ljava/lang/Runnable;

.field protected c:Ljava/util/Vector;

.field private e:I

.field private f:I

.field private g:Ljava/lang/Object;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Livq;

    sput-object v0, Livq;->d:[Livq;

    return-void
.end method

.method public constructor <init>(Livs;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Livq;->g:Ljava/lang/Object;

    iput-object p1, p0, Livq;->a:Livs;

    iput-object p2, p0, Livq;->b:Ljava/lang/Runnable;

    iput-object p3, p0, Livq;->h:Ljava/lang/String;

    return-void
.end method

.method private h()[Livq;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Livq;->c:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Livq;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v0, v0, [Livq;

    iget-object v1, p0, Livq;->c:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    sget-object v0, Livq;->d:[Livq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Livq;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->b(Livq;)I

    move-result v0

    return v0
.end method

.method protected final a(I)V
    .locals 0

    iput p1, p0, Livq;->e:I

    return-void
.end method

.method abstract b()I
.end method

.method protected final c()I
    .locals 1

    iget v0, p0, Livq;->e:I

    return v0
.end method

.method public d()V
    .locals 2

    iget-object v1, p0, Livq;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Livq;->f:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Livq;->a:Livs;

    invoke-virtual {v0, p0}, Livs;->a(Livq;)V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method abstract e()V
.end method

.method protected f()V
    .locals 1

    iget-object v0, p0, Livq;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Livq;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method g()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Livq;->f()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Livq;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget v0, p0, Livq;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Livq;->f:I

    iget-object v0, p0, Livq;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0}, Livq;->h()[Livq;

    move-result-object v1

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Livq;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    return-void
.end method
