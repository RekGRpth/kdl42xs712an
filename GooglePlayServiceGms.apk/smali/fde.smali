.class final Lfde;
.super Lfbx;
.source "SourceFile"


# instance fields
.field final synthetic a:Lfch;

.field private final b:Lfaz;


# direct methods
.method public constructor <init>(Lfch;Lfaz;)V
    .locals 0

    iput-object p1, p0, Lfde;->a:Lfch;

    invoke-direct {p0}, Lfbx;-><init>()V

    iput-object p2, p0, Lfde;->b:Lfaz;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 7

    const/4 v5, 0x0

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bundle callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbundle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, p2}, Lfch;->a(ILandroid/os/Bundle;)Lbbo;

    move-result-object v3

    invoke-virtual {v3}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "added_circles"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    const-string v0, "removed_circles"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    :goto_0
    iget-object v6, p0, Lfde;->a:Lfch;

    new-instance v0, Lfdf;

    iget-object v1, p0, Lfde;->a:Lfch;

    iget-object v2, p0, Lfde;->b:Lfaz;

    invoke-direct/range {v0 .. v5}, Lfdf;-><init>(Lfch;Lfaz;Lbbo;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v6, v0}, Lfch;->b(Lbjg;)V

    return-void

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method
