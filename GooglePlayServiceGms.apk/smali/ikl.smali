.class public final Likl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/accounts/Account;

.field public final b:Landroid/view/ViewGroup;

.field public final c:Landroid/view/ViewGroup;

.field public final d:Landroid/view/ViewGroup;

.field final synthetic e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;Landroid/accounts/Account;)V
    .locals 3

    iput-object p1, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Likl;->a:Landroid/accounts/Account;

    invoke-virtual {p1}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a3    # com.google.android.gms.R.layout.location_account_settings

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Likl;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Likl;->b:Landroid/view/ViewGroup;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const v2, 0x7f0a0187    # com.google.android.gms.R.id.header

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Likl;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0a020d    # com.google.android.gms.R.id.location_reporting_pref

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Likl;->c:Landroid/view/ViewGroup;

    iget-object v0, p0, Likl;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0a020e    # com.google.android.gms.R.id.location_history_pref

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Likl;->d:Landroid/view/ViewGroup;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;ZILjava/lang/String;Ljava/lang/Class;)Landroid/view/ViewGroup;
    .locals 4

    iget-object v1, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1, p5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.location.settings.extra.account"

    iget-object v3, p0, Likl;->a:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p1, p3}, Likt;->a(Landroid/view/ViewGroup;I)V

    if-eqz p4, :cond_0

    const v0, 0x1020010    # android.R.id.summary

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    new-instance v0, Liku;

    invoke-direct {v0, v1, v2}, Liku;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p1, p2}, Lbqc;->a(Landroid/view/View;Z)V

    return-object p1
.end method

.method public final a(ILcom/google/android/location/reporting/service/AccountConfig;Z)Ljava/lang/String;
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Lcom/google/android/location/reporting/service/AccountConfig;->l()Lcom/google/android/location/reporting/service/Conditions;

    move-result-object v0

    iget-object v3, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/location/reporting/service/AccountConfig;->p()Z

    move-result v4

    if-eqz v4, :cond_0

    const v0, 0x7f0b043f    # com.google.android.gms.R.string.location_settings_ulr_summary_restricted

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->isIneligibleDueToGeoOnly()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v0, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v3, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const v3, 0x7f0b0440    # com.google.android.gms.R.string.location_settings_ulr_summary_unsupported_country

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v3}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->b(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v1, 0x7f0b0441    # com.google.android.gms.R.string.location_settings_ulr_summary_unsupported_region

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Likm;

    invoke-direct {v1, p0}, Likm;-><init>(Likl;)V

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Likm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->f()Z

    move-result v4

    if-nez v4, :cond_3

    const v0, 0x7f0b042b    # com.google.android.gms.R.string.location_ulr_setting_location_disabled

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/location/reporting/service/Conditions;->e()Z

    move-result v0

    if-nez v0, :cond_4

    const v0, 0x7f0b042c    # com.google.android.gms.R.string.location_ulr_setting_google_location_disabled

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/location/reporting/service/AccountConfig;->j()Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f0b043b    # com.google.android.gms.R.string.location_settings_ulr_summary_auth_error

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, -0x2

    if-ne p1, v0, :cond_6

    const v0, 0x7f0b043d    # com.google.android.gms.R.string.location_settings_ulr_summary_ambiguous

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    if-lez p1, :cond_7

    move v0, v1

    :goto_1
    if-eqz p3, :cond_a

    iget-object v2, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    if-ne v2, v1, :cond_9

    if-eqz v0, :cond_8

    const v0, 0x7f0b0434    # com.google.android.gms.R.string.location_settings_reporting_summary_on

    :goto_2
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    const v0, 0x7f0b0435    # com.google.android.gms.R.string.location_settings_reporting_summary_off

    goto :goto_2

    :cond_9
    invoke-static {v2}, Lija;->a(I)I

    move-result v0

    goto :goto_2

    :cond_a
    iget-object v2, p0, Likl;->e:Lcom/google/android/location/settings/GoogleLocationSettingsActivity;

    invoke-static {v2}, Lcom/google/android/location/settings/GoogleLocationSettingsActivity;->a(Lcom/google/android/location/settings/GoogleLocationSettingsActivity;)I

    move-result v2

    if-ne v2, v1, :cond_c

    if-eqz v0, :cond_b

    const v0, 0x7f0b0436    # com.google.android.gms.R.string.location_settings_ulr_summary_on

    goto :goto_2

    :cond_b
    const v0, 0x7f0b0437    # com.google.android.gms.R.string.location_settings_ulr_summary_off

    goto :goto_2

    :cond_c
    invoke-static {v2}, Lija;->a(I)I

    move-result v0

    goto :goto_2
.end method
