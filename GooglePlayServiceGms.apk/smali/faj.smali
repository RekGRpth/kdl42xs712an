.class public final Lfaj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbq;


# instance fields
.field public final a:Lfch;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbbr;Lbbs;)V
    .locals 6

    const/16 v4, 0x50

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V
    .locals 6

    new-instance v0, Lfch;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lfch;-><init>(Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lfaj;-><init>(Lfch;)V

    return-void
.end method

.method private constructor <init>(Lfch;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lfaj;->a:Lfch;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->a()V

    return-void
.end method

.method public final a(Lbbr;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1}, Lfch;->a(Lbbr;)V

    return-void
.end method

.method public final a(Lbbs;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1}, Lfch;->a(Lbbs;)V

    return-void
.end method

.method public final a(Lfam;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lfaj;->a:Lfch;

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lfch;->a(Lfam;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfao;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2, p3}, Lfch;->a(Lfao;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfaq;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8

    iget-object v0, p0, Lfaj;->a:Lfch;

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lfch;->a(Lfaq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method public final a(Lfar;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1}, Lfch;->a(Lfar;)V

    return-void
.end method

.method public final a(Lfas;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2}, Lfch;->a(Lfas;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfas;Ljava/lang/String;II)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2, p3, p4}, Lfch;->a(Lfas;Ljava/lang/String;II)V

    return-void
.end method

.method public final a(Lfat;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2}, Lfch;->a(Lfat;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lfaw;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x1

    iget-object v0, p0, Lfaj;->a:Lfch;

    move-object v1, p1

    move v3, v2

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lfch;->a(Lfaw;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfaw;Z)V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lfaj;->a:Lfch;

    const/4 v2, 0x0

    move-object v1, p1

    move v3, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lfch;->a(Lfaw;ZZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lfax;Ljava/lang/String;Ljava/lang/String;Lfal;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    if-nez p4, :cond_0

    sget-object p4, Lfal;->a:Lfal;

    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lfch;->a(Lfax;Ljava/lang/String;Ljava/lang/String;Lfal;)V

    return-void
.end method

.method public final a(Lfar;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 2

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "account must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2, p3, p4}, Lfch;->a(Lfar;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1, p2, p3, p4}, Lfch;->a(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->b()V

    return-void
.end method

.method public final b(Lbbr;)Z
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1}, Lfch;->b(Lbbr;)Z

    move-result v0

    return v0
.end method

.method public final c(Lbbr;)V
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0, p1}, Lfch;->c(Lbbr;)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    return v0
.end method

.method public final d_()Z
    .locals 1

    iget-object v0, p0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    return v0
.end method
