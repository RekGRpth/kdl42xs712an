.class public abstract Ldve;
.super Ldvj;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:Landroid/content/Context;

.field protected b:I

.field protected c:Lbgo;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private h:Z

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private final k:Landroid/view/View;

.field private l:Z

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Ldvf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ldvj;-><init>()V

    iput-boolean v0, p0, Ldve;->h:Z

    iput-boolean v0, p0, Ldve;->l:Z

    iput-boolean v1, p0, Ldve;->o:Z

    iput-boolean v0, p0, Ldve;->p:Z

    iput-boolean v0, p0, Ldve;->q:Z

    iput-boolean v0, p0, Ldve;->r:Z

    iput-boolean v1, p0, Ldve;->s:Z

    iput v0, p0, Ldve;->t:I

    iput v0, p0, Ldve;->b:I

    iput-object p1, p0, Ldve;->a:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-static {v0}, Ldve;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->i:Landroid/view/View;

    iget-object v1, p0, Ldve;->i:Landroid/view/View;

    sget v2, Lxa;->aJ:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->j:Landroid/view/View;

    iget-object v1, p0, Ldve;->i:Landroid/view/View;

    sget v2, Lxa;->aR:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->k:Landroid/view/View;

    iget-object v1, p0, Ldve;->k:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v0}, Ldve;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->e:Landroid/view/View;

    iget-object v1, p0, Ldve;->i:Landroid/view/View;

    sget v2, Lxa;->aJ:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->f:Landroid/view/View;

    iget-object v1, p0, Ldve;->i:Landroid/view/View;

    sget v2, Lxa;->aR:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Ldve;->g:Landroid/view/View;

    iget-object v1, p0, Ldve;->g:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lxc;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldve;->m:Landroid/view/View;

    return-void
.end method

.method private static a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    sget v0, Lxc;->i:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Z
    .locals 1

    iget-boolean v0, p0, Ldve;->q:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    iget-boolean v0, p0, Ldve;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldve;->t:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Ldve;->u:Ldvf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldve;->u:Ldvf;

    invoke-interface {v0, p1}, Ldvf;->a_(I)V

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pageDirection needs to be NEXT or PREV"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Ldve;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldve;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Ldve;->h:Z

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Ldve;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldve;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Ldve;->l:Z

    goto :goto_0

    :cond_0
    const-string v0, "DataBufferAdapter"

    const-string v1, "Reached the end of a paginated DataBuffer, but no OnEndOfWindowReachedListener registered!"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private l()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ldve;->c:Lbgo;

    if-nez v0, :cond_1

    iput-boolean v2, p0, Ldve;->q:Z

    iput-boolean v2, p0, Ldve;->p:Z

    iput v2, p0, Ldve;->t:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Ldve;->s:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldve;->c:Lbgo;

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "prev_page_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Ldve;->q:Z

    iget-boolean v0, p0, Ldve;->s:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Ldve;->c:Lbgo;

    invoke-virtual {v0}, Lbgo;->c()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v3, "next_page_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    :goto_4
    iput-boolean v1, p0, Ldve;->p:Z

    invoke-virtual {p0}, Ldve;->c()I

    move-result v0

    iput v0, p0, Ldve;->t:I

    iget-boolean v0, p0, Ldve;->q:Z

    if-eqz v0, :cond_2

    iget v0, p0, Ldve;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldve;->t:I

    :cond_2
    iget-boolean v0, p0, Ldve;->p:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldve;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldve;->t:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method private m()Z
    .locals 1

    iget-boolean v0, p0, Ldve;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldve;->n:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Ldve;->t:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Ldve;->c:Lbgo;

    iget v1, p0, Ldve;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    if-nez p2, :cond_0

    iget-object v1, p0, Ldve;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p3}, Ldve;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v1, p0, Ldve;->a:Landroid/content/Context;

    invoke-virtual {p0, p2, v1, v0}, Ldve;->a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V

    return-object p2
.end method

.method public abstract a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldve;->a(Lbgo;)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iput p1, p0, Ldve;->b:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Ldve;->n:Landroid/view/View;

    iget v0, p0, Ldve;->t:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldve;->r:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ldve;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public abstract a(Landroid/view/View;Landroid/content/Context;Ljava/lang/Object;)V
.end method

.method public a(Lbgo;)V
    .locals 1

    iget-object v0, p0, Ldve;->c:Lbgo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldve;->c:Lbgo;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Ldve;->c:Lbgo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldve;->c:Lbgo;

    invoke-virtual {v0}, Lbgo;->b()V

    :cond_1
    iput-object p1, p0, Ldve;->c:Lbgo;

    invoke-direct {p0}, Ldve;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldve;->r:Z

    invoke-virtual {p0}, Ldve;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Ldvf;)V
    .locals 0

    iput-object p1, p0, Ldve;->u:Ldvf;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-boolean v0, p0, Ldve;->o:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Ldve;->o:Z

    invoke-virtual {p0}, Ldve;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Ldve;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Ldve;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldve;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-super {p0}, Ldvj;->areAllItemsEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldve;->s:Z

    invoke-virtual {p0}, Ldve;->notifyDataSetChanged()V

    return-void
.end method

.method public c()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Ldve;->c:Lbgo;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Ldve;->c:Lbgo;

    invoke-virtual {v1}, Lbgo;->a()I

    move-result v1

    iget v2, p0, Ldve;->b:I

    sub-int/2addr v1, v2

    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Ldve;->c:Lbgo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldve;->c:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lbgo;
    .locals 1

    iget-object v0, p0, Ldve;->c:Lbgo;

    return-object v0
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Ldve;->r:Z

    if-eq v1, v0, :cond_0

    iput-boolean v1, p0, Ldve;->r:Z

    invoke-virtual {p0}, Ldve;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Ldve;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldve;->g:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldve;->h:Z

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Ldve;->o:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Ldve;->r:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Ldve;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget v0, p0, Ldve;->t:I

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Ldve;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Ldve;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldve;->c:Lbgo;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Ldve;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Ldve;->q:Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_2
    iget-object v0, p0, Ldve;->c:Lbgo;

    iget v1, p0, Ldve;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Ldve;->r:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Ldve;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Ldve;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Ldve;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget-boolean v0, p0, Ldve;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldve;->m:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Ldve;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldve;->n:Landroid/view/View;

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Ldve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Ldve;->l:Z

    if-nez v0, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldve;->d(I)V

    :cond_2
    iget-object v0, p0, Ldve;->i:Landroid/view/View;

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Ldve;->q:Z

    if-eqz v0, :cond_6

    if-nez p1, :cond_5

    iget-boolean v0, p0, Ldve;->h:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldve;->d(I)V

    :cond_4
    iget-object v0, p0, Ldve;->e:Landroid/view/View;

    goto :goto_0

    :cond_5
    add-int/lit8 p1, p1, -0x1

    :cond_6
    iget-object v0, p0, Ldve;->i:Landroid/view/View;

    if-eq p2, v0, :cond_7

    iget-object v0, p0, Ldve;->e:Landroid/view/View;

    if-ne p2, v0, :cond_8

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "trying to convert header/footer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-virtual {p0, p1, p2, p3}, Ldve;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Ldve;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ldve;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldve;->l:Z

    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Ldve;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Ldve;->m()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Ldve;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Ldve;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Ldve;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Ldvj;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    invoke-direct {p0}, Ldve;->l()V

    invoke-super {p0}, Ldvj;->notifyDataSetChanged()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Ldve;->k:Landroid/view/View;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldve;->d(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ldve;->k:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldve;->d(I)V

    goto :goto_0
.end method
