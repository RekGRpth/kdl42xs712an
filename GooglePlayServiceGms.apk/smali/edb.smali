.class public final Ledb;
.super Lecz;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lecz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3}, Lecz;->a(IILandroid/content/Intent;)V

    if-ne p1, v3, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Ledb;->f(I)V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ledb;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ledb;->f(I)V

    const-string v0, "PlusCheckFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User failed to complete G+ OOB - result code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ledb;->c(I)V

    goto :goto_0
.end method

.method public final a(Lduk;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v4, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "PlusCheckFragment"

    const-string v1, "Couldn\'t locate selected account!"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Ledb;->e(I)V

    :goto_1
    return-void

    :cond_1
    sget-object v2, Lbje;->e:[Ljava/lang/String;

    invoke-virtual {v3, v0, v2, p0, v1}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lecz;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Ledb;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-void
.end method

.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 5

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0}, Ledb;->J()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ledb;->c(I)V
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PlusCheckFragment"

    const-string v2, "Error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Ledb;->e(I)V

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    iget-object v2, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->i()[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    new-instance v0, Lfmu;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v2, v1}, Lfmu;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x1

    iput v1, v0, Lfmu;->b:I

    invoke-virtual {v0}, Lfmu;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Ledb;->b:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->e()V

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ledb;->f(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ledb;->a(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "PlusCheckFragment"

    const-string v2, "Error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "PlusCheckFragment"

    const-string v2, "Error checking G+ sign-up status"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
