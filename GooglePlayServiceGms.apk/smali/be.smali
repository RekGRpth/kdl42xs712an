.class final Lbe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laz;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lax;)Landroid/app/Notification;
    .locals 18

    new-instance v1, Lbg;

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lax;->r:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Lax;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Lax;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Lax;->h:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v7, v0, Lax;->f:Landroid/widget/RemoteViews;

    move-object/from16 v0, p1

    iget v8, v0, Lax;->i:I

    move-object/from16 v0, p1

    iget-object v9, v0, Lax;->d:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v10, v0, Lax;->e:Landroid/app/PendingIntent;

    move-object/from16 v0, p1

    iget-object v11, v0, Lax;->g:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, Lax;->n:I

    move-object/from16 v0, p1

    iget v13, v0, Lax;->o:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lax;->p:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Lax;->k:Z

    move-object/from16 v0, p1

    iget v0, v0, Lax;->j:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lax;->m:Ljava/lang/CharSequence;

    move-object/from16 v17, v0

    invoke-direct/range {v1 .. v17}, Lbg;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZILjava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->q:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lau;

    iget v4, v2, Lau;->a:I

    iget-object v5, v2, Lau;->b:Ljava/lang/CharSequence;

    iget-object v2, v2, Lau;->c:Landroid/app/PendingIntent;

    iget-object v6, v1, Lbg;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v6, v4, v5, v2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_0

    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    instance-of v2, v2, Law;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    check-cast v2, Law;

    iget-object v3, v2, Law;->e:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Law;->g:Z

    iget-object v5, v2, Law;->f:Ljava/lang/CharSequence;

    iget-object v2, v2, Law;->a:Ljava/lang/CharSequence;

    new-instance v6, Landroid/app/Notification$BigTextStyle;

    iget-object v7, v1, Lbg;->a:Landroid/app/Notification$Builder;

    invoke-direct {v6, v7}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v6, v3}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v2

    if-eqz v4, :cond_1

    invoke-virtual {v2, v5}, Landroid/app/Notification$BigTextStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    :cond_1
    :goto_1
    iget-object v1, v1, Lbg;->a:Landroid/app/Notification$Builder;

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    return-object v1

    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    instance-of v2, v2, Lay;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    check-cast v2, Lay;

    iget-object v3, v2, Lay;->e:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Lay;->g:Z

    iget-object v5, v2, Lay;->f:Ljava/lang/CharSequence;

    iget-object v2, v2, Lay;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v4, v5, v2}, Lbg;->a(Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    instance-of v2, v2, Lav;

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lax;->l:Lbf;

    check-cast v2, Lav;

    iget-object v3, v2, Lav;->e:Ljava/lang/CharSequence;

    iget-boolean v4, v2, Lav;->g:Z

    iget-object v5, v2, Lav;->f:Ljava/lang/CharSequence;

    iget-object v6, v2, Lav;->a:Landroid/graphics/Bitmap;

    iget-object v7, v2, Lav;->b:Landroid/graphics/Bitmap;

    iget-boolean v2, v2, Lav;->c:Z

    new-instance v8, Landroid/app/Notification$BigPictureStyle;

    iget-object v9, v1, Lbg;->a:Landroid/app/Notification$Builder;

    invoke-direct {v8, v9}, Landroid/app/Notification$BigPictureStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v8, v3}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v3

    if-eqz v2, :cond_4

    invoke-virtual {v3, v7}, Landroid/app/Notification$BigPictureStyle;->bigLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    :cond_4
    if-eqz v4, :cond_1

    invoke-virtual {v3, v5}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    goto :goto_1
.end method
