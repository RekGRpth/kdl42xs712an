.class Ljr;
.super Ljq;
.source "SourceFile"

# interfaces
.implements Lln;
.implements Lmc;


# static fields
.field private static final d:[I


# instance fields
.field private e:Landroid/support/v7/internal/widget/ActionBarView;

.field private f:Llk;

.field private g:Llm;

.field private h:Lot;

.field private i:Z

.field private j:Ljava/lang/CharSequence;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lki;->i:I

    aput v2, v0, v1

    sput-object v0, Ljr;->d:[I

    return-void
.end method

.method constructor <init>(Ljp;)V
    .locals 0

    invoke-direct {p0, p1}, Ljq;-><init>(Ljp;)V

    return-void
.end method

.method private m()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-boolean v2, p0, Ljr;->n:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Ljr;->g:Llm;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Ljr;->o:Z

    if-eqz v2, :cond_6

    :cond_1
    iget-object v2, p0, Ljr;->g:Llm;

    if-nez v2, :cond_2

    new-instance v2, Llm;

    invoke-virtual {p0}, Ljr;->k()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Llm;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Ljr;->g:Llm;

    iget-object v2, p0, Ljr;->g:Llm;

    invoke-virtual {v2, p0}, Llm;->a(Lln;)V

    iget-object v2, p0, Ljr;->g:Llm;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v3, p0, Ljr;->g:Llm;

    invoke-virtual {v2, v3, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Lcs;Lmc;)V

    :cond_3
    iget-object v2, p0, Ljr;->g:Llm;

    invoke-virtual {v2}, Llm;->g()V

    iget-object v2, p0, Ljr;->a:Ljp;

    iget-object v3, p0, Ljr;->g:Llm;

    invoke-virtual {v2, v1, v3}, Ljp;->a(ILandroid/view/Menu;)Z

    move-result v2

    if-nez v2, :cond_5

    iput-object v4, p0, Ljr;->g:Llm;

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v4, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Lcs;Lmc;)V

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iput-boolean v1, p0, Ljr;->o:Z

    :cond_6
    iget-object v2, p0, Ljr;->g:Llm;

    invoke-virtual {v2}, Llm;->g()V

    iget-object v2, p0, Ljr;->p:Landroid/os/Bundle;

    if-eqz v2, :cond_7

    iget-object v2, p0, Ljr;->g:Llm;

    iget-object v3, p0, Ljr;->p:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Llm;->b(Landroid/os/Bundle;)V

    iput-object v4, p0, Ljr;->p:Landroid/os/Bundle;

    :cond_7
    iget-object v2, p0, Ljr;->a:Ljp;

    iget-object v3, p0, Ljr;->g:Llm;

    invoke-virtual {v2, v1, v4, v3}, Ljp;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, v4, p0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Lcs;Lmc;)V

    :cond_8
    iget-object v0, p0, Ljr;->g:Llm;

    invoke-virtual {v0}, Llm;->h()V

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v1, p0, Ljr;->g:Llm;

    invoke-virtual {v1}, Llm;->h()V

    iput-boolean v0, p0, Ljr;->n:Z

    goto :goto_0
.end method


# virtual methods
.method public a()Ljj;
    .locals 3

    invoke-virtual {p0}, Ljr;->l()V

    new-instance v0, Ljy;

    iget-object v1, p0, Ljr;->a:Ljp;

    iget-object v2, p0, Ljr;->a:Ljp;

    invoke-direct {v0, v1, v2}, Ljy;-><init>(Ljp;Ljk;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    invoke-virtual {p0}, Ljr;->l()V

    iget-object v0, p0, Ljr;->a:Ljp;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Ljr;->a:Ljp;

    invoke-virtual {v1}, Ljp;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v0, p0, Ljr;->a:Ljp;

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Ljr;->l()V

    iget-object v0, p0, Ljr;->a:Ljp;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Ljr;->a:Ljp;

    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Ljr;->l()V

    iget-object v0, p0, Ljr;->a:Ljp;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ljr;->a:Ljp;

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Ljr;->j:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(Llm;)V
    .locals 1

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->b()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->d()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Llm;->close()V

    goto :goto_0
.end method

.method public final a(Llm;Z)V
    .locals 1

    iget-boolean v0, p0, Ljr;->m:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljr;->m:Z

    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0}, Ljp;->closeOptionsMenu()V

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->g()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljr;->m:Z

    goto :goto_0
.end method

.method public final a(ILandroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->a(ILandroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/MenuItem;)Z
    .locals 1

    if-nez p1, :cond_0

    invoke-static {p2}, Lmf;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0, p1, p2}, Ljp;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0, p1, p2, p3}, Ljp;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    iget-object v0, p0, Ljr;->a:Ljp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljp;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-direct {p0}, Ljr;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljr;->a:Ljp;

    iget-object v2, p0, Ljr;->g:Llm;

    if-nez v2, :cond_1

    :goto_0
    check-cast v0, Landroid/view/View;

    :cond_0
    return-object v0

    :cond_1
    iget-object v0, p0, Ljr;->f:Llk;

    if-nez v0, :cond_2

    sget-object v0, Lkq;->C:[I

    invoke-virtual {v1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x4

    sget v3, Lkp;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Llk;

    sget v3, Lkn;->m:I

    invoke-direct {v0, v3, v2}, Llk;-><init>(II)V

    iput-object v0, p0, Ljr;->f:Llk;

    iget-object v0, p0, Ljr;->f:Llk;

    invoke-virtual {v0, p0}, Llk;->a(Lmc;)V

    iget-object v0, p0, Ljr;->g:Llm;

    iget-object v2, p0, Ljr;->f:Llk;

    invoke-virtual {v0, v2}, Llm;->a(Lmb;)V

    :goto_1
    iget-object v0, p0, Ljr;->f:Llk;

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Llk;->a(Landroid/view/ViewGroup;)Lmd;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljr;->f:Llk;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Llk;->a(Z)V

    goto :goto_1
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Ljr;->l()V

    iget-object v0, p0, Ljr;->a:Ljp;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Ljr;->a:Ljp;

    return-void
.end method

.method public final b(Llm;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 2

    iget-boolean v0, p0, Ljr;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ljr;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljr;->b()Ljj;

    move-result-object v0

    check-cast v0, Ljy;

    iget-object v1, v0, Ljy;->a:Landroid/content/Context;

    invoke-static {v1}, Lkr;->a(Landroid/content/Context;)Lkr;

    move-result-object v1

    invoke-virtual {v1}, Lkr;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljy;->c(Z)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    invoke-virtual {p0}, Ljr;->b()Ljj;

    move-result-object v0

    check-cast v0, Ljy;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljy;->d(Z)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    invoke-virtual {p0}, Ljr;->b()Ljj;

    move-result-object v0

    check-cast v0, Ljy;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljy;->d(Z)V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Ljr;->g:Llm;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Ljr;->g:Llm;

    invoke-virtual {v1, v0}, Llm;->a(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_0

    iput-object v0, p0, Ljr;->p:Landroid/os/Bundle;

    :cond_0
    iget-object v0, p0, Ljr;->g:Llm;

    invoke-virtual {v0}, Llm;->g()V

    iget-object v0, p0, Ljr;->g:Llm;

    invoke-virtual {v0}, Llm;->clear()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljr;->o:Z

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljr;->n:Z

    invoke-direct {p0}, Ljr;->m()Z

    :cond_2
    return-void
.end method

.method public final h()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Ljr;->h:Lot;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljr;->h:Lot;

    invoke-virtual {v1}, Lot;->a()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->l()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 0

    return-void
.end method

.method final l()V
    .locals 12

    const/4 v5, 0x0

    const/4 v11, 0x6

    const/4 v10, 0x5

    const/4 v3, 0x0

    const/4 v4, -0x1

    iget-boolean v0, p0, Ljr;->i:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Ljr;->b:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Ljr;->c:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkn;->b:I

    invoke-virtual {v0, v1}, Ljp;->b(I)V

    :goto_0
    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkl;->a:I

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarView;

    iput-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Ljr;->a:Ljp;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/view/Window$Callback;)V

    iget-boolean v0, p0, Ljr;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->h()V

    :cond_0
    iget-boolean v0, p0, Ljr;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->i()V

    :cond_1
    const-string v0, "splitActionBarWhenNarrow"

    invoke-virtual {p0}, Ljr;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0}, Ljp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lkj;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    move v2, v0

    :goto_1
    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkl;->w:I

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_2

    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->a(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarView;->a(Z)V

    iget-object v1, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v6}, Landroid/support/v7/internal/widget/ActionBarView;->b(Z)V

    iget-object v1, p0, Ljr;->a:Ljp;

    sget v7, Lkl;->h:I

    invoke-virtual {v1, v7}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Z)V

    invoke-virtual {v1, v6}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(Z)V

    :cond_2
    :goto_2
    iget-object v0, p0, Ljr;->a:Ljp;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkl;->b:I

    invoke-virtual {v0, v1}, Ljp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Ljr;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ljr;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Ljr;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->b(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Ljr;->j:Ljava/lang/CharSequence;

    :cond_3
    iget-object v0, p0, Ljr;->a:Ljp;

    sget-object v1, Lkq;->c:[I

    invoke-virtual {v0, v1}, Ljp;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v6

    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v6, v1, v0}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_3
    invoke-virtual {v6, v10}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_13

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v6, v10, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_4
    invoke-virtual {v6, v11}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v6, v11, v2}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :goto_5
    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v7

    if-eqz v7, :cond_4

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    const/4 v7, 0x4

    invoke-virtual {v6, v7, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_4
    iget-object v7, p0, Ljr;->a:Ljp;

    invoke-virtual {v7}, Ljp;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v9, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v8, v9, :cond_5

    const/4 v5, 0x1

    :cond_5
    if-eqz v5, :cond_c

    :goto_6
    if-eqz v1, :cond_11

    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_11

    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_d

    invoke-virtual {v1, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    :goto_7
    if-eqz v5, :cond_e

    :goto_8
    if-eqz v2, :cond_10

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_10

    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v10, :cond_f

    invoke-virtual {v2, v7}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_9
    if-ne v1, v4, :cond_6

    if-eq v0, v4, :cond_7

    :cond_6
    iget-object v2, p0, Ljr;->a:Ljp;

    invoke-virtual {v2}, Ljp;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/view/Window;->setLayout(II)V

    :cond_7
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljr;->i:Z

    iget-object v0, p0, Ljr;->a:Ljp;

    invoke-virtual {v0}, Ljp;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljs;

    invoke-direct {v1, p0}, Ljs;-><init>(Ljr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_8
    return-void

    :cond_9
    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkn;->a:I

    invoke-virtual {v0, v1}, Ljp;->b(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Ljr;->a:Ljp;

    sget-object v1, Lkq;->c:[I

    invoke-virtual {v0, v1}, Ljp;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v0

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Ljr;->a:Ljp;

    sget v1, Lkn;->r:I

    invoke-virtual {v0, v1}, Ljp;->b(I)V

    goto/16 :goto_2

    :cond_c
    move-object v1, v0

    goto :goto_6

    :cond_d
    iget v0, v1, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_11

    iget v0, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v8, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v8, v8

    invoke-virtual {v1, v0, v8}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    goto :goto_7

    :cond_e
    move-object v2, v3

    goto :goto_8

    :cond_f
    iget v0, v2, Landroid/util/TypedValue;->type:I

    if-ne v0, v11, :cond_10

    iget v0, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v3, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    invoke-virtual {v2, v0, v3}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result v0

    float-to-int v0, v0

    goto :goto_9

    :cond_10
    move v0, v4

    goto :goto_9

    :cond_11
    move v1, v4

    goto/16 :goto_7

    :cond_12
    move-object v2, v3

    goto/16 :goto_5

    :cond_13
    move-object v1, v3

    goto/16 :goto_4

    :cond_14
    move-object v0, v3

    goto/16 :goto_3
.end method
