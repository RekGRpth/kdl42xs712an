.class public final enum Lhif;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhif;

.field public static final enum b:Lhif;

.field public static final enum c:Lhif;

.field public static final enum d:Lhif;

.field public static final enum e:Lhif;

.field public static final enum f:Lhif;

.field public static final enum g:Lhif;

.field public static final enum h:Lhif;

.field public static final enum i:Lhif;

.field public static final enum j:Lhif;

.field public static final enum k:Lhif;

.field public static final enum l:Lhif;

.field public static final enum m:Lhif;

.field public static final enum n:Lhif;

.field private static final synthetic o:[Lhif;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lhif;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->a:Lhif;

    new-instance v0, Lhif;

    const-string v1, "ACCOUNT_DISABLED"

    invoke-direct {v0, v1, v4}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->b:Lhif;

    new-instance v0, Lhif;

    const-string v1, "BAD_USERNAME"

    invoke-direct {v0, v1, v5}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->c:Lhif;

    new-instance v0, Lhif;

    const-string v1, "BAD_REQUEST"

    invoke-direct {v0, v1, v6}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->d:Lhif;

    new-instance v0, Lhif;

    const-string v1, "LOGIN_FAIL"

    invoke-direct {v0, v1, v7}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->e:Lhif;

    new-instance v0, Lhif;

    const-string v1, "SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->f:Lhif;

    new-instance v0, Lhif;

    const-string v1, "MISSING_APPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->g:Lhif;

    new-instance v0, Lhif;

    const-string v1, "NO_GMAIL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->h:Lhif;

    new-instance v0, Lhif;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->i:Lhif;

    new-instance v0, Lhif;

    const-string v1, "CAPTCHA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->j:Lhif;

    new-instance v0, Lhif;

    const-string v1, "CANCELLED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->k:Lhif;

    new-instance v0, Lhif;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->l:Lhif;

    new-instance v0, Lhif;

    const-string v1, "OAUTH_MIGRATION_REQUIRED"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->m:Lhif;

    new-instance v0, Lhif;

    const-string v1, "DMAGENT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lhif;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhif;->n:Lhif;

    const/16 v0, 0xe

    new-array v0, v0, [Lhif;

    sget-object v1, Lhif;->a:Lhif;

    aput-object v1, v0, v3

    sget-object v1, Lhif;->b:Lhif;

    aput-object v1, v0, v4

    sget-object v1, Lhif;->c:Lhif;

    aput-object v1, v0, v5

    sget-object v1, Lhif;->d:Lhif;

    aput-object v1, v0, v6

    sget-object v1, Lhif;->e:Lhif;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lhif;->f:Lhif;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lhif;->g:Lhif;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lhif;->h:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lhif;->i:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lhif;->j:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lhif;->k:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lhif;->l:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lhif;->m:Lhif;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lhif;->n:Lhif;

    aput-object v2, v0, v1

    sput-object v0, Lhif;->o:[Lhif;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhif;
    .locals 1

    const-class v0, Lhif;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhif;

    return-object v0
.end method

.method public static values()[Lhif;
    .locals 1

    sget-object v0, Lhif;->o:[Lhif;

    invoke-virtual {v0}, [Lhif;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhif;

    return-object v0
.end method
