.class public final Ldzu;
.super Ldvp;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Lbel;
.implements Ldvf;
.implements Ledh;
.implements Ledt;
.implements Lje;


# instance fields
.field protected Z:Ldzo;

.field protected aa:Leds;

.field private ab:I

.field private ac:I

.field private ad:I

.field private ae:Landroid/view/View;

.field private af:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private ag:Landroid/view/View;

.field private ah:Ledo;

.field private ai:I

.field private aj:Landroid/view/View;

.field private ak:Lcom/google/android/gms/games/Game;

.field private al:Z

.field private am:Ldzq;

.field private an:Ldzv;

.field private ao:Ldzs;

.field private ap:Ldxa;

.field private aq:Ldzl;

.field private ar:Ledq;

.field private as:Ledq;

.field protected i:Ldvn;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ldvp;-><init>()V

    iput v0, p0, Ldzu;->ab:I

    iput v0, p0, Ldzu;->ac:I

    return-void
.end method

.method private S()V
    .locals 2

    iget-object v0, p0, Ldzu;->am:Ldzq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldzq;->b(Z)V

    return-void
.end method

.method private a(Landroid/widget/ListView;Ldfg;)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "LScoreListFragment"

    const-string v1, "scrollToCurrentPlayer: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcte;->m:Lctw;

    invoke-interface {v2, v0}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "LScoreListFragment"

    const-string v1, "scrollToCurrentPlayer: couldn\'t get current player ID"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Leee;->a(Landroid/widget/ListView;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Ldfg;->a()I

    move-result v4

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_4

    invoke-virtual {p2, v0}, Ldfg;->b(I)Ldff;

    move-result-object v2

    invoke-interface {v2}, Ldff;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    if-eqz v2, :cond_3

    :goto_3
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    invoke-virtual {p1, v0, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method private a(Lbdu;ZZ)V
    .locals 14

    if-eqz p3, :cond_0

    invoke-direct {p0}, Ldzu;->S()V

    :cond_0
    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-static {v0}, Ledx;->a(Landroid/content/Context;)I

    move-result v5

    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->u()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->v()I

    move-result v3

    iget v0, p0, Ldzu;->ab:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    sget-object v0, Lcte;->h:Ldfo;

    iget v4, p0, Ldzu;->ad:I

    move-object v1, p1

    move/from16 v6, p2

    invoke-interface/range {v0 .. v6}, Ldfo;->b(Lbdu;Ljava/lang/String;IIIZ)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    const/4 v0, 0x2

    iput v0, p0, Ldzu;->ac:I

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcte;->h:Ldfo;

    iget v4, p0, Ldzu;->ad:I

    move-object v1, p1

    move/from16 v6, p2

    invoke-interface/range {v0 .. v6}, Ldfo;->a(Lbdu;Ljava/lang/String;IIIZ)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    const/4 v0, 0x1

    iput v0, p0, Ldzu;->ac:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->u()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->v()I

    move-result v10

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    iget v0, p0, Ldzu;->ab:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    sget-object v6, Lcte;->h:Ldfo;

    const/4 v8, 0x0

    iget v11, p0, Ldzu;->ad:I

    move-object v7, p1

    move v12, v5

    move/from16 v13, p2

    invoke-interface/range {v6 .. v13}, Ldfo;->b(Lbdu;Ljava/lang/String;Ljava/lang/String;IIIZ)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    const/4 v0, 0x2

    iput v0, p0, Ldzu;->ac:I

    goto :goto_0

    :cond_3
    sget-object v6, Lcte;->h:Ldfo;

    const/4 v8, 0x0

    iget v11, p0, Ldzu;->ad:I

    move-object v7, p1

    move v12, v5

    move/from16 v13, p2

    invoke-interface/range {v6 .. v13}, Ldfo;->a(Lbdu;Ljava/lang/String;Ljava/lang/String;IIIZ)Lbeh;

    move-result-object v0

    invoke-interface {v0, p0}, Lbeh;->a(Lbel;)V

    const/4 v0, 0x1

    iput v0, p0, Ldzu;->ac:I

    goto :goto_0
.end method

.method private a(Ldfb;)V
    .locals 11

    invoke-interface {p1}, Ldfb;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v1, v0}, Ldzo;->b(Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->v()I

    move-result v3

    const/4 v1, 0x0

    invoke-interface {p1}, Ldfb;->h()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_c

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfl;

    invoke-interface {v0}, Ldfl;->b()I

    move-result v6

    iget v7, p0, Ldzu;->ad:I

    if-ne v6, v7, :cond_0

    invoke-interface {v0}, Ldfl;->a()I

    move-result v6

    if-ne v3, v6, :cond_0

    move-object v2, v0

    :goto_1
    if-nez v2, :cond_1

    const-string v0, "LScoreListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No variant found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ldfb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ldzu;->ad:I

    invoke-static {v2}, Ldea;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ldef;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Ldzu;->S()V

    :goto_2
    return-void

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ldfl;->h()J

    move-result-wide v3

    invoke-interface {v2}, Ldfl;->k()J

    move-result-wide v5

    invoke-static {v3, v4}, Ledm;->a(J)Z

    move-result v7

    invoke-interface {v2}, Ldfl;->d()J

    move-result-wide v0

    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    iget v1, p0, Ldzu;->ad:I

    if-nez v1, :cond_2

    iget-object v1, p0, Ldzu;->aq:Ldzl;

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Ldzl;->a(I)V

    iget-object v1, p0, Ldzu;->aq:Ldzl;

    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Ldzl;->a(I)V

    :cond_2
    const/4 v1, 0x0

    iget-object v8, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v8}, Ldvn;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const-wide/16 v9, -0x1

    cmp-long v9, v3, v9

    if-eqz v9, :cond_5

    if-eqz v7, :cond_4

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-interface {v2}, Ldfl;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sget v1, Lxf;->am:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    const-wide/16 v1, -0x1

    cmp-long v1, v5, v1

    if-nez v1, :cond_9

    iget-object v0, p0, Ldzu;->am:Ldzq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldzq;->b(Z)V

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    const-wide/16 v9, 0x0

    cmp-long v0, v5, v9

    if-lez v0, :cond_8

    const-wide/16 v0, 0x64

    mul-long/2addr v0, v3

    div-long/2addr v0, v5

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lxf;->an:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v8, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    if-eqz v0, :cond_8

    iget v0, p0, Ldzu;->ad:I

    if-nez v0, :cond_8

    iget-object v0, p0, Ldzu;->ak:Lcom/google/android/gms/games/Game;

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_6

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_7

    iget-object v0, p0, Ldzu;->aq:Ldzl;

    invoke-virtual {v0}, Ldzl;->d()V

    move-object v0, v1

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_7
    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Ldzu;->aq:Ldzl;

    invoke-virtual {v0, p0}, Ldzl;->a(Landroid/content/DialogInterface$OnClickListener;)V

    :cond_8
    move-object v0, v1

    goto :goto_4

    :cond_9
    iget-object v1, p0, Ldzu;->am:Ldzq;

    invoke-virtual {v1, v0}, Ldzq;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->an:Ldzv;

    invoke-virtual {v0, v5, v6}, Ldzv;->a(J)V

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0, v5, v6}, Ldzs;->a(J)V

    iget-object v2, p0, Ldzu;->i:Ldvn;

    long-to-int v0, v5

    const-wide/16 v3, 0x1f4

    cmp-long v1, v5, v3

    if-ltz v1, :cond_a

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v5, v6}, Ljava/math/BigDecimal;-><init>(J)V

    new-instance v1, Ljava/math/MathContext;

    const/4 v3, 0x1

    sget-object v4, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-direct {v1, v3, v4}, Ljava/math/MathContext;-><init>(ILjava/math/RoundingMode;)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->round(Ljava/math/MathContext;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    :cond_a
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v1

    int-to-long v3, v0

    invoke-virtual {v1, v3, v4}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/16 v3, 0x1f4

    if-lt v0, v3, :cond_b

    sget v3, Lxf;->ah:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_b
    sget v3, Lxe;->e:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldzu;->am:Ldzq;

    invoke-virtual {v1, v0}, Ldzq;->b(Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->am:Ldzq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldzq;->b(Z)V

    goto/16 :goto_2

    :cond_c
    move-object v2, v1

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Ldzu;->ap:Ldxa;

    invoke-virtual {v0, p1}, Ldxa;->a(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Ldzu;->ap:Ldxa;

    sget v1, Lwy;->k:I

    invoke-virtual {v0, v1}, Ldxa;->a(I)V

    :goto_0
    iget-object v0, p0, Ldzu;->ap:Ldxa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldxa;->b(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Ldzu;->ap:Ldxa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldxa;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public final H_()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Ldzu;->al:Z

    invoke-direct {p0, v0, v3, v2}, Ldzu;->a(Lbdu;ZZ)V

    iget-object v0, p0, Ldzu;->i:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    invoke-virtual {p0}, Ldzu;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/widget/ListView;->scrollTo(II)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "LScoreListFragment"

    const-string v1, "refresh(): client not connected yet..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final P()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Ldzu;->a(Lbdu;ZZ)V

    iget-object v0, p0, Ldzu;->aa:Leds;

    invoke-virtual {v0, v2}, Leds;->a(I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "LScoreListFragment"

    const-string v1, "onTimeSpanChanged: client not connected yet..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final Q()V
    .locals 2

    iget-object v0, p0, Ldzu;->ag:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final R()V
    .locals 1

    iget-object v0, p0, Ldzu;->i:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->b()V

    invoke-virtual {p0}, Ldzu;->H_()V

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    sget v0, Lxc;->d:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    new-instance v0, Leds;

    sget v2, Lxa;->bh:I

    sget v3, Lxa;->aa:I

    sget v4, Lxa;->r:I

    sget v5, Lxa;->an:I

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Leds;-><init>(Landroid/view/View;IIIILedt;)V

    iput-object v0, p0, Ldzu;->aa:Leds;

    iget-object v0, p0, Ldzu;->aa:Leds;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Leds;->a(I)V

    sget v0, Lxa;->bi:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzu;->ae:Landroid/view/View;

    sget v0, Lxa;->aU:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Ldzu;->af:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v0, p0, Ldzu;->af:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Lje;)V

    iget-object v0, p0, Ldzu;->af:Landroid/support/v4/widget/SwipeRefreshLayout;

    sget v2, Lwx;->f:I

    sget v3, Lwx;->h:I

    sget v4, Lwx;->i:I

    sget v5, Lwx;->g:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(IIII)V

    sget v0, Lxa;->be:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzu;->ag:Landroid/view/View;

    iget-object v0, p0, Ldzu;->ag:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lxa;->bh:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldzu;->aj:Landroid/view/View;

    new-instance v0, Ldvk;

    sget v2, Lxa;->J:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v0, v2}, Ldvk;-><init>(Landroid/view/View;)V

    new-instance v2, Ldzl;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v2, v3, v0}, Ldzl;-><init>(Landroid/content/Context;Ldvk;)V

    iput-object v2, p0, Ldzu;->aq:Ldzl;

    if-eqz p3, :cond_1

    iget-object v0, p0, Ldzu;->aq:Ldzl;

    const-string v2, "BUTTERBAR_STATES"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "BUTTERBAR_STATES"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iput-object v2, v0, Ldzl;->a:[I

    :cond_0
    invoke-virtual {v0}, Ldzl;->b()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    return-object v1

    :pswitch_0
    invoke-virtual {v0}, Ldzl;->c()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, p0}, Ldzl;->a(Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Ldzl;->d()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(IF)V
    .locals 3

    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v0, 0xb

    invoke-static {v0}, Lbpz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Ldzu;->ai:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldzu;->ag:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget v0, p0, Ldzu;->ai:I

    if-ne v0, p1, :cond_1

    sub-float p2, v2, p2

    :cond_1
    const/high16 v0, 0x3e800000    # 0.25f

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    cmpl-float v1, v0, v2

    if-nez v1, :cond_2

    iget-object v1, p0, Ldzu;->ag:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v1, p0, Ldzu;->aj:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_3
    return-void
.end method

.method public final a(Lbdu;)V
    .locals 2

    sget-object v0, Lcte;->m:Lctw;

    invoke-interface {v0, p1}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "LScoreListFragment"

    const-string v1, "couldn\'t get current player ID; bailing out..."

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Ldzu;->an:Ldzv;

    invoke-virtual {v1, v0}, Ldzv;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v1, v0}, Ldzs;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0, p0}, Ldzo;->a(Ldvp;)V

    goto :goto_0
.end method

.method public final synthetic a(Lbek;)V
    .locals 9

    check-cast p1, Ldfq;

    iget-object v0, p0, Ldzu;->af:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    iget-object v0, p0, Ldzu;->i:Ldvn;

    instance-of v0, v0, Ldxc;

    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Ldzu;->i:Ldvn;

    check-cast v0, Ldxc;

    invoke-interface {v0}, Ldxc;->x_()V

    iget-boolean v0, p0, Ldzu;->al:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldzu;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Leee;->b(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldzu;->al:Z

    invoke-interface {p1}, Ldfq;->L_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v2

    invoke-interface {p1}, Ldfq;->g()Ldfb;

    move-result-object v3

    invoke-interface {p1}, Ldfq;->h()Ldfg;

    move-result-object v4

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ldfb;->i()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    iput-object v0, p0, Ldzu;->ak:Lcom/google/android/gms/games/Game;

    :cond_1
    if-eqz v2, :cond_2

    const-string v0, "LScoreListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "onLeaderboardScoresLoaded: got non-SUCCESS statusCode: "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", data = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :try_start_0
    invoke-virtual {p0}, Ldzu;->L()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v4}, Ldfg;->b()V

    :goto_0
    return-void

    :cond_3
    :try_start_1
    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0, v2}, Ldvn;->d(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v4}, Ldfg;->b()V

    goto :goto_0

    :cond_4
    :try_start_2
    invoke-static {v2}, Leee;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldzu;->aq:Ldzl;

    invoke-virtual {v0}, Ldzl;->c()V

    :goto_1
    invoke-virtual {v4}, Ldfg;->a()I

    move-result v5

    if-nez v5, :cond_8

    iget-object v0, p0, Ldzu;->an:Ldzv;

    invoke-virtual {v0}, Ldzv;->a()V

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldzs;->a(I)V

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0, v4}, Ldzs;->a(Lbgo;)V

    :goto_2
    invoke-virtual {v4}, Ldfg;->a()I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    move v1, v0

    :goto_3
    const/4 v0, 0x0

    iget v5, p0, Ldzu;->ad:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    invoke-virtual {v4}, Ldfg;->a()I

    move-result v5

    if-nez v5, :cond_d

    const/4 v0, 0x1

    :cond_5
    :goto_4
    if-nez v0, :cond_6

    if-eqz v1, :cond_13

    :cond_6
    if-eqz v2, :cond_11

    iget-object v0, p0, Ldzu;->aa:Leds;

    sget v1, Lxf;->ao:I

    sget v5, Lxf;->ao:I

    invoke-virtual {v0, v2, v1, v5}, Leds;->a(III)V

    :goto_5
    :pswitch_0
    if-nez v3, :cond_14

    const-string v0, "LScoreListFragment"

    const-string v1, "Could not load leaderboard metadata"

    invoke-static {v0, v1}, Ldac;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldzu;->aa:Leds;

    sget v1, Lxf;->ao:I

    sget v3, Lxf;->ao:I

    invoke-virtual {v0, v2, v1, v3}, Leds;->a(III)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {v4}, Ldfg;->b()V

    goto :goto_0

    :cond_7
    :try_start_3
    iget-object v0, p0, Ldzu;->aq:Ldzl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldzl;->a(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ldfg;->b()V

    throw v0

    :cond_8
    const/4 v0, 0x0

    :try_start_4
    invoke-virtual {v4, v0}, Ldfg;->b(I)Ldff;

    move-result-object v6

    invoke-static {v6}, Lbiq;->a(Ljava/lang/Object;)V

    invoke-interface {v6}, Ldff;->a()J

    move-result-wide v0

    const-wide/16 v7, 0x1

    cmp-long v0, v0, v7

    if-nez v0, :cond_b

    const/4 v0, 0x1

    if-le v5, v0, :cond_9

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ldfg;->b(I)Ldff;

    move-result-object v0

    move-object v1, v0

    :goto_6
    const/4 v0, 0x2

    if-le v5, v0, :cond_a

    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Ldfg;->b(I)Ldff;

    move-result-object v0

    :goto_7
    iget-object v7, p0, Ldzu;->an:Ldzv;

    invoke-virtual {v7, v6, v1, v0}, Ldzv;->a(Ldff;Ldff;Ldff;)V

    const/4 v0, 0x3

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_8
    iget-object v1, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v1, v0}, Ldzs;->a(I)V

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0, v4}, Ldzs;->a(Lbgo;)V

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_6

    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    :cond_b
    iget-object v0, p0, Ldzu;->an:Ldzv;

    invoke-virtual {v0}, Ldzv;->a()V

    const/4 v0, 0x0

    goto :goto_8

    :cond_c
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v4}, Ldfg;->a()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ldfg;->b(I)Ldff;

    move-result-object v0

    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v5

    invoke-interface {v5}, Lbdu;->d()Z

    move-result v6

    if-nez v6, :cond_f

    const-string v0, "LScoreListFragment"

    const-string v5, "isCurrentPlayerScore: not connected; ignoring..."

    invoke-static {v0, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    :goto_9
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_f
    sget-object v6, Lcte;->m:Lctw;

    invoke-interface {v6, v5}, Lctw;->a(Lbdu;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_10

    const-string v0, "LScoreListFragment"

    const-string v5, "scoreBelongsToCurrentPlayer: couldn\'t get current player ID"

    invoke-static {v0, v5}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    :cond_10
    invoke-interface {v0}, Ldff;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_11
    iget v0, p0, Ldzu;->ad:I

    if-nez v0, :cond_12

    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid TimeSpan "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    sget v0, Lxf;->aj:I

    :goto_a
    invoke-virtual {p0, v0}, Ldzu;->b(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldzu;->a(Ljava/lang/String;Z)V

    :goto_b
    iget-object v0, p0, Ldzu;->aa:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    goto/16 :goto_5

    :pswitch_2
    sget v0, Lxf;->al:I

    goto :goto_a

    :pswitch_3
    sget v0, Lxf;->ak:I

    goto :goto_a

    :cond_12
    iget-object v0, p0, Ldzu;->Z:Ldzo;

    invoke-interface {v0}, Ldzo;->v()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid TimeSpan "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_4
    sget v0, Lxf;->aq:I

    :goto_c
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Ldzu;->ak:Lcom/google/android/gms/games/Game;

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    invoke-virtual {p0, v0, v1}, Ldzu;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Ldzu;->a(Ljava/lang/String;Z)V

    goto :goto_b

    :pswitch_5
    sget v0, Lxf;->as:I

    goto :goto_c

    :pswitch_6
    sget v0, Lxf;->ar:I

    goto :goto_c

    :cond_13
    iget-object v0, p0, Ldzu;->ap:Ldxa;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldxa;->b(Z)V

    iget-object v0, p0, Ldzu;->aa:Leds;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Leds;->a(I)V

    invoke-virtual {p0}, Ldzu;->a()Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Ldzu;->ac:I

    packed-switch v1, :pswitch_data_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unexpected mOnScoresLoadedScrollBehavior: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Ldzu;->ac:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbiq;->b(Ljava/lang/Object;)V

    goto/16 :goto_5

    :pswitch_7
    invoke-static {v0}, Leee;->a(Landroid/widget/ListView;)V

    goto/16 :goto_5

    :pswitch_8
    invoke-direct {p0, v0, v4}, Ldzu;->a(Landroid/widget/ListView;Ldfg;)V

    goto/16 :goto_5

    :cond_14
    invoke-direct {p0, v3}, Ldzu;->a(Ldfb;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 3

    iput-object p1, p0, Ldzu;->ak:Lcom/google/android/gms/games/Game;

    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldzu;->am:Ldzq;

    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldzq;->a(Landroid/net/Uri;)V

    :cond_0
    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v0

    invoke-interface {v0}, Lbdu;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "LScoreListFragment"

    const-string v1, "onGameLoaded: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Ldzu;->a(Lbdu;ZZ)V

    goto :goto_0
.end method

.method public final a_(I)V
    .locals 5

    if-nez p1, :cond_0

    iget-object v0, p0, Ldzu;->ar:Ledq;

    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Ldzu;->J()Lbdu;

    move-result-object v2

    invoke-interface {v2}, Lbdu;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "LScoreListFragment"

    const-string v1, "expandData: not connected; ignoring..."

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Ldzu;->as:Ledq;

    move-object v1, v0

    goto :goto_0

    :cond_1
    sget-object v3, Lcte;->h:Ldfo;

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0}, Ldzs;->e()Lbgo;

    move-result-object v0

    check-cast v0, Ldfg;

    iget-object v4, p0, Ldzu;->i:Ldvn;

    invoke-static {v4}, Ledx;->a(Landroid/content/Context;)I

    move-result v4

    invoke-interface {v3, v2, v0, v4, p1}, Ldfo;->a(Lbdu;Ldfg;II)Lbeh;

    move-result-object v0

    invoke-interface {v0, v1}, Lbeh;->a(Lbel;)V

    const/4 v0, 0x3

    iput v0, p0, Ldzu;->ac:I

    goto :goto_1
.end method

.method public final d(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iput p1, p0, Ldzu;->ab:I

    invoke-virtual {p0}, Ldzu;->H_()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Ldvp;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldvn;

    iput-object v0, p0, Ldzu;->i:Ldvn;

    iget-object v0, p0, Ldzu;->i:Ldvn;

    instance-of v0, v0, Ldzo;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Parent activity did not implement LeaderboardMetaDataProvider"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ldzu;->i:Ldvn;

    instance-of v0, v0, Ledo;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Parent activity did not implement LeaderboardOverlayListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ledo;

    iput-object v0, p0, Ldzu;->ah:Ledo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    check-cast v0, Ldzo;

    iput-object v0, p0, Ldzu;->Z:Ldzo;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    const-string v3, "leaderboard_collection_arg"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "leaderboard_tab_index"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldzu;->ai:I

    iget v0, p0, Ldzu;->ai:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Ldzu;->ag:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iput v3, p0, Ldzu;->ad:I

    if-nez v3, :cond_4

    iget-object v0, p0, Ldzu;->ae:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_0
    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->l()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    new-instance v3, Ldzq;

    iget-object v4, p0, Ldzu;->i:Ldvn;

    invoke-direct {v3, v4, v0}, Ldzq;-><init>(Landroid/content/Context;Z)V

    iput-object v3, p0, Ldzu;->am:Ldzq;

    iget v3, p0, Ldzu;->ad:I

    if-nez v3, :cond_6

    sget v3, Lxf;->ai:I

    :goto_2
    iget-object v4, p0, Ldzu;->am:Ldzq;

    invoke-virtual {v4, v3}, Ldzq;->a(I)V

    iget-object v3, p0, Ldzu;->am:Ldzq;

    invoke-virtual {v3, v2}, Ldzq;->b(Z)V

    new-instance v4, Ldzv;

    iget-object v5, p0, Ldzu;->i:Ldvn;

    iget v3, p0, Ldzu;->ad:I

    if-ne v3, v1, :cond_7

    move v3, v1

    :goto_3
    invoke-direct {v4, v5, p0, v0, v3}, Ldzv;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;ZZ)V

    iput-object v4, p0, Ldzu;->an:Ldzv;

    new-instance v3, Ldzs;

    iget-object v4, p0, Ldzu;->i:Ldvn;

    invoke-direct {v3, v4, v0, p0}, Ldzs;-><init>(Ldvn;ZLandroid/view/View$OnClickListener;)V

    iput-object v3, p0, Ldzu;->ao:Ldzs;

    new-instance v0, Ldxa;

    iget-object v3, p0, Ldzu;->i:Ldvn;

    invoke-direct {v0, v3}, Ldxa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldzu;->ap:Ldxa;

    iget-object v0, p0, Ldzu;->ap:Ldxa;

    sget v3, Lwy;->j:I

    invoke-virtual {v0, v3}, Ldxa;->c(I)V

    iget-object v0, p0, Ldzu;->ap:Ldxa;

    invoke-virtual {v0, v2}, Ldxa;->b(Z)V

    new-instance v0, Ldwu;

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    iget-object v4, p0, Ldzu;->am:Ldzq;

    aput-object v4, v3, v2

    iget-object v4, p0, Ldzu;->an:Ldzv;

    aput-object v4, v3, v1

    const/4 v4, 0x2

    iget-object v5, p0, Ldzu;->ao:Ldzs;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p0, Ldzu;->ap:Ldxa;

    aput-object v5, v3, v4

    invoke-direct {v0, v3}, Ldwu;-><init>([Landroid/widget/BaseAdapter;)V

    invoke-virtual {p0, v0}, Ldzu;->a(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Ldzu;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0, p0}, Ldzs;->a(Ldvf;)V

    new-instance v0, Ledq;

    iget-object v3, p0, Ldzu;->ao:Ldzs;

    invoke-direct {v0, v3, p0, v2}, Ledq;-><init>(Ldve;Lbel;I)V

    iput-object v0, p0, Ldzu;->ar:Ledq;

    new-instance v0, Ledq;

    iget-object v2, p0, Ldzu;->ao:Ldzs;

    invoke-direct {v0, v2, p0, v1}, Ledq;-><init>(Ldve;Lbel;I)V

    iput-object v0, p0, Ldzu;->as:Ledq;

    invoke-direct {p0}, Ldzu;->S()V

    return-void

    :cond_4
    iget-object v0, p0, Ldzu;->ae:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    sget v3, Lxf;->ap:I

    goto/16 :goto_2

    :cond_7
    move v3, v2

    goto :goto_3
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Ldvp;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Ldzu;->aq:Ldzl;

    const-string v1, "BUTTERBAR_STATES"

    iget-object v0, v0, Ldzl;->a:[I

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Ldzu;->ao:Ldzs;

    invoke-virtual {v0}, Ldzs;->a()V

    invoke-super {p0}, Ldvp;->f()V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-virtual {v0}, Ldvn;->h()V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    const-string v2, "ShareView"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Ldzu;->i:Ldvn;

    iget-object v3, p0, Ldzu;->ak:Lcom/google/android/gms/games/Game;

    if-nez v3, :cond_1

    const-string v0, "UiUtils"

    const-string v1, "shareGame: null game"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->r()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "UiUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getShareUri: no instance package name for game: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    if-nez v0, :cond_3

    const-string v0, "UiUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "shareGame: couldn\'t get shareGame for game: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lbjt;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_3
    sget v1, Lxf;->ag:I

    new-array v4, v6, [Ljava/lang/Object;

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v1, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget v4, Lxf;->ae:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->n_()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    aput-object v0, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lbjt;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v1, "FindPeople"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldzu;->i:Ldvn;

    invoke-static {v0}, Lbjt;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "UiUtils"

    const-string v2, "Unable to launch find people intent"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    instance-of v2, v0, Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_8

    check-cast v0, Lcom/google/android/gms/games/Player;

    iget-object v2, p0, Ldzu;->i:Ldvn;

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2}, Leee;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.google.android.gms.games.PLAYER"

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-static {v3, v5, v0, v4, v1}, Lbis;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "UiUtils"

    const-string v1, "Failed to downgrade game safely! Aborting."

    invoke-static {v0, v1}, Ldac;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.VIEW_PLAYER_DETAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v3, "com.google.android.play.games"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    invoke-static {v2}, Leee;->b(Landroid/content/Context;)Z

    move-result v1

    invoke-static {v1}, Lbiq;->a(Z)V

    invoke-static {v2}, Leee;->c(Landroid/content/Context;)Lbo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbo;->a(Landroid/content/Intent;)Lbo;

    invoke-virtual {v1}, Lbo;->a()V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lxa;->be:I

    if-ne v0, v1, :cond_9

    iget v0, p0, Ldzu;->ai:I

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Ldzu;->ah:Ledo;

    invoke-interface {v0}, Ledo;->n()V

    goto/16 :goto_0

    :cond_9
    const-string v0, "LScoreListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
