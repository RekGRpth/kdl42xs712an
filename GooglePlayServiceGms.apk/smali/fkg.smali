.class public final Lfkg;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)V
    .locals 0

    iput-object p1, p0, Lfkg;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-direct {p0}, Lbip;-><init>()V

    return-void
.end method


# virtual methods
.method public final n(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lfkg;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-static {v0, p3}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    const-string v0, "AutoBackupService"

    const-string v1, "Verified Package Name."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lfkg;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lbbv;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    const-string v0, "AutoBackupService"

    const-string v1, "Verified Package is Google Signed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lfkg;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-static {v0}, Lfkh;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-instance v1, Lfkc;

    iget-object v2, p0, Lfkg;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)Lfkh;

    move-result-object v2

    invoke-direct {v1, v2}, Lfkc;-><init>(Lfkh;)V

    invoke-virtual {v1}, Lfkc;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Client died while brokering service."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
