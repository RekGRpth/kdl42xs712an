.class public final Lamp;
.super Lamf;
.source "SourceFile"


# static fields
.field private static transient c:Lamp;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lamf;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static a()Lamp;
    .locals 2

    sget-object v0, Lamp;->c:Lamp;

    if-nez v0, :cond_0

    new-instance v0, Lamp;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lamp;-><init>(Landroid/content/Context;)V

    sput-object v0, Lamp;->c:Lamp;

    :cond_0
    sget-object v0, Lamp;->c:Lamp;

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 5

    const/4 v1, 0x1

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "@"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10

    const/16 v9, 0x8

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-nez p5, :cond_0

    new-instance p5, Landroid/os/Bundle;

    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    :cond_0
    const-string v0, "setupWizard"

    const-string v2, "firstRun"

    invoke-virtual {p5, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p5, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    :try_start_0
    iget-object v0, p0, Lamp;->a:Laoy;

    iget-object v0, v0, Laoy;->a:Landroid/content/Context;

    new-instance v6, Lapa;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "auth.authenticator.config.properties"

    invoke-direct {v6, v0, v2}, Lapa;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz v5, :cond_1

    const-string v0, "domain"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v6, v0}, Lapa;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v0, v6, Lapa;->a:Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v0, v6, Lapa;->b:Ljava/util/Properties;

    const/4 v7, 0x0

    invoke-virtual {v0, v2, v7}, Ljava/util/Properties;->store(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    :cond_1
    iget-object v0, v6, Lapa;->b:Ljava/util/Properties;

    const-string v2, "allowed_domains"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_0
    const-string v2, "useBrowser"

    invoke-virtual {p5, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    const-string v2, "created"

    invoke-virtual {p5, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string v2, "password"

    invoke-virtual {p5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lapp;

    iget-object v2, p0, Lamp;->a:Laoy;

    invoke-direct {v8, v2}, Lapp;-><init>(Laoy;)V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "code:"

    invoke-virtual {v7, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v0, 0x5

    invoke-virtual {v7, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v4}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Z)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    invoke-virtual {v8, v0, v6, v5, v1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    :goto_1
    sget-object v1, Laso;->a:Laso;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v3

    if-ne v1, v3, :cond_7

    const-string v1, "authAccount"

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "accountType"

    const-string v1, "com.google"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    move-object v1, v2

    :cond_2
    :goto_3
    return-object v1

    :catchall_0
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_4
    if-eqz v2, :cond_b

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    :goto_5
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_3
    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "GLSActivity"

    const-string v2, "Eror while trying to get PersistentAuthConfiguration"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    :try_start_6
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const-string v4, "username"

    invoke-virtual {p5, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v0}, Lamp;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "errorCode"

    invoke-virtual {v2, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v1, v2

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lamp;->a:Laoy;

    iget-object v0, v0, Laoy;->c:Ljava/lang/String;

    invoke-static {v4, v0}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Z)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    invoke-virtual {v8, v0, v6, v5, v1}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    goto :goto_1

    :cond_7
    const-string v0, "errorCode"

    invoke-virtual {v2, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    :cond_8
    if-eqz p4, :cond_a

    array-length v2, p4

    move v1, v4

    :goto_6
    if-ge v1, v2, :cond_a

    aget-object v3, p4, v1

    const-string v6, "hosted_or_google"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "google"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "youtubelinked"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "saml"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_a
    const-string v1, "hasMultipleUsers"

    invoke-virtual {p5, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lamp;->b:Lamg;

    new-instance v3, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    iget-object v6, p0, Lamp;->a:Laoy;

    invoke-virtual {v6}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;-><init>(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)V

    invoke-virtual {v3, v1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->a(Z)Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->b(Z)Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->a(Ljava/util/Collection;)Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;

    move-result-object v0

    invoke-virtual {v2, v0}, Lamg;->a(Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lamp;->a:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "accountType"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "authAccount"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "booleanResult"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "retry"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v0, v2, p1}, Lcom/google/android/gms/auth/RedirectActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/List;Landroid/accounts/AccountAuthenticatorResponse;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4

    :catchall_2
    move-exception v0

    goto/16 :goto_4

    :cond_b
    move-object v1, v3

    goto/16 :goto_5
.end method

.method public final addAccountFromCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7

    const-string v0, "GLSActivity"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSActivity"

    const-string v1, "Inside addAccountFromCredentials"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lamp;->a:Laoy;

    iget-object v2, v2, Laoy;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v0

    const-string v2, "booleanResult"

    const-string v3, "access_secret"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "parent_aid"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Laoz;->c:Laoy;

    iget-object v5, v5, Laoy;->a:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    if-eqz v3, :cond_1

    if-nez v4, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1

    :cond_2
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v6, "parent_aid"

    invoke-static {p3, v4, v6}, Laoz;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v6, "oauthAccessToken"

    invoke-static {p3, v4, v6}, Laoz;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v6, "sha1hash"

    invoke-static {p3, v4, v6}, Laoz;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v6, "services"

    invoke-static {p3, v4, v6}, Laoz;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    const-string v6, "flags"

    invoke-static {p3, v4, v6}, Laoz;->a(Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object v0, v0, Laoz;->a:Landroid/accounts/Account;

    invoke-virtual {v5, v0, v3, v4}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_0
.end method

.method public final getAccountCredentialsForCloning(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v1, "GLSActivity"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "GLSActivity"

    const-string v2, "Inside getAccountCredentialsForCloning"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Lapp;

    iget-object v2, p0, Lamp;->a:Laoy;

    invoke-direct {v1, v2}, Lapp;-><init>(Laoy;)V

    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v4, "oauth2:https://www.googleapis.com/auth/userinfo.email"

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->b(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    move-result-object v2

    iget-object v3, p0, Lamp;->a:Laoy;

    invoke-virtual {v3}, Laoy;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v3

    invoke-virtual {v1, v3, v2, v0}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "booleanResult"

    invoke-virtual {v0, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    sget-object v2, Laso;->u:Laso;

    invoke-virtual {v2, v1}, Laso;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "errorCode"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_0
    return-object v0

    :cond_1
    sget-object v2, Laso;->b:Laso;

    invoke-virtual {v2, v1}, Laso;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "errorCode"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    sget-object v2, Laso;->m:Laso;

    invoke-virtual {v2, v1}, Laso;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "errorCode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    const-string v1, "errorCode"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :cond_4
    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lamp;->a:Laoy;

    iget-object v2, v2, Laoy;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Laoz;->a(Ljava/lang/String;Ljava/lang/String;)Laoz;

    move-result-object v2

    iget-object v1, v2, Laoz;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v2, Laoz;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_1
    if-nez v0, :cond_8

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "booleanResult"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    iget-object v0, v2, Laoz;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v2, Laoz;->d:Ljava/lang/String;

    :goto_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "access_secret"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "parent_aid"

    iget-object v0, v2, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v4, v2, Laoz;->a:Landroid/accounts/Account;

    const-string v5, "parent_aid"

    invoke-virtual {v0, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    :goto_3
    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "oauthAccessToken"

    iget-object v3, v2, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v4, v2, Laoz;->a:Landroid/accounts/Account;

    const-string v5, "oauthAccessToken"

    invoke-virtual {v3, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sha1hash"

    iget-object v3, v2, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v4, v2, Laoz;->a:Landroid/accounts/Account;

    const-string v5, "sha1hash"

    invoke-virtual {v3, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "services"

    iget-object v3, v2, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v4, v2, Laoz;->a:Landroid/accounts/Account;

    const-string v5, "services"

    invoke-virtual {v3, v4, v5}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "flags"

    iget-object v3, v2, Laoz;->b:Landroid/accounts/AccountManager;

    iget-object v2, v2, Laoz;->a:Landroid/accounts/Account;

    const-string v4, "flags"

    invoke-virtual {v3, v2, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    :cond_6
    iget-object v0, v2, Laoz;->e:Ljava/lang/String;

    goto :goto_2

    :cond_7
    new-instance v0, Lbhv;

    iget-object v4, v2, Laoz;->c:Laoy;

    iget-object v4, v4, Laoy;->a:Landroid/content/Context;

    invoke-direct {v0, v4}, Lbhv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lbhv;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_8
    const-string v1, "booleanResult"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
