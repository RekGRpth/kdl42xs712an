.class public final Lbml;
.super Lrs;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lrs;-><init>()V

    iput-object p1, p0, Lbml;->a:Landroid/content/Context;

    iput-object p2, p0, Lbml;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIF)V
    .locals 0

    invoke-direct {p0, p3, p4, p5}, Lrs;-><init>(IIF)V

    iput-object p1, p0, Lbml;->a:Landroid/content/Context;

    iput-object p2, p0, Lbml;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lsp;)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p1, Lsp;->a:Lrz;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lsp;->a:Lrz;

    iget v1, v1, Lrz;->a:I

    const/16 v2, 0x190

    if-lt v1, v2, :cond_2

    iget-object v1, p1, Lsp;->a:Lrz;

    iget v1, v1, Lrz;->a:I

    const/16 v2, 0x1f4

    if-ge v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    invoke-static {p1, v0}, Lbng;->b(Lsp;Ljava/lang/String;)Lbne;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lbne;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v1, "userRateLimitExceeded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbml;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lbmi;->a(Lsp;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbml;->a:Landroid/content/Context;

    iget-object v1, p0, Lbml;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lamr;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    throw p1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lrs;->a(Lsp;)V

    return-void
.end method
