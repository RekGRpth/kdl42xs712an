.class public final Lgzy;
.super Lhcb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V
    .locals 0

    iput-object p1, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-direct {p0}, Lhcb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->g(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    iget-object v1, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;)V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->b(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v1, v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v1, v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v1, v1

    if-lez v1, :cond_2

    :cond_1
    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->c(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    :goto_0
    return-void

    :cond_2
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget v4, v2, v1

    packed-switch v4, :pswitch_data_0

    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup address error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->d(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v4, v4, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v4}, Lgvc;->J()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget v3, v1, v0

    packed-switch v3, :pswitch_data_1

    :pswitch_1
    const-string v0, "CreateProfileActivity"

    const-string v1, "Unexpected signup instrument error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->e(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v3}, Lgvc;->v()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->A:Lgvc;

    invoke-virtual {v3}, Lgvc;->J()V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->u:Lgvc;

    invoke-virtual {v3}, Lgvc;->J()V

    goto :goto_3

    :pswitch_3
    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const v4, 0x7f0b0113    # com.google.android.gms.R.string.wallet_error_creditcard_invalid

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V

    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    invoke-virtual {v3}, Lgwj;->b()V

    goto :goto_3

    :pswitch_4
    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    const v4, 0x7f0b014d    # com.google.android.gms.R.string.wallet_error_creditcard_expiry_date_invalid

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;I)V

    iget-object v3, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-object v3, v3, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->v:Lgwj;

    invoke-virtual {v3}, Lgwj;->J()V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i()Z

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->f(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lipg;)V
    .locals 8

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Lipg;)Lipg;

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->o:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lipg;->a:[Lipj;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p1, Lipg;->a:[Lipj;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    new-instance v5, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v6, v4, Lipj;->a:Ljava/lang/String;

    iget-object v7, v4, Lipj;->b:Ljava/lang/String;

    iget-object v4, v4, Lipj;->c:[Ljava/lang/String;

    invoke-direct {v5, v6, v7, v4}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->a(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->h(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->i(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Lipg;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->j(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->k(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->l(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lgzy;->a:Lcom/google/android/gms/wallet/ia/CreateProfileActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CreateProfileActivity;->m(Lcom/google/android/gms/wallet/ia/CreateProfileActivity;)V

    return-void
.end method
