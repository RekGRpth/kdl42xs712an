.class public final Liif;
.super Lizk;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:I

.field public a:Z

.field public b:Liig;

.field public c:Z

.field public d:I

.field public e:Z

.field public f:J

.field public g:Z

.field public h:F

.field public i:Z

.field public j:F

.field public k:Z

.field public l:D

.field public m:Z

.field public n:F

.field public o:I

.field public p:Z

.field public q:I

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Ljava/lang/String;

.field public x:Z

.field public y:I

.field public z:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lizk;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Liif;->b:Liig;

    iput v2, p0, Liif;->d:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liif;->f:J

    iput v3, p0, Liif;->h:F

    iput v3, p0, Liif;->j:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Liif;->l:D

    iput v3, p0, Liif;->n:F

    iput v2, p0, Liif;->o:I

    iput v4, p0, Liif;->q:I

    iput-boolean v2, p0, Liif;->s:Z

    iput-boolean v2, p0, Liif;->u:Z

    const-string v0, ""

    iput-object v0, p0, Liif;->w:Ljava/lang/String;

    iput v2, p0, Liif;->y:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Liif;->z:Ljava/util/List;

    iput v4, p0, Liif;->B:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Liif;->B:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Liif;->b()I

    :cond_0
    iget v0, p0, Liif;->B:I

    return v0
.end method

.method public final a(D)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->k:Z

    iput-wide p1, p0, Liif;->l:D

    return-object p0
.end method

.method public final a(F)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->g:Z

    iput p1, p0, Liif;->h:F

    return-object p0
.end method

.method public final a(I)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->c:Z

    iput p1, p0, Liif;->d:I

    return-object p0
.end method

.method public final a(J)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->e:Z

    iput-wide p1, p0, Liif;->f:J

    return-object p0
.end method

.method public final a(Liig;)Liif;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->a:Z

    iput-object p1, p0, Liif;->b:Liig;

    return-object p0
.end method

.method public final a(Liih;)Liif;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Liif;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liif;->z:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Liif;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->v:Z

    iput-object p1, p0, Liif;->w:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->r:Z

    iput-boolean p1, p0, Liif;->s:Z

    return-object p0
.end method

.method public final synthetic a(Lizg;)Lizk;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizg;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lizg;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Liig;

    invoke-direct {v0}, Liig;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Liif;->a(Liig;)Liif;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Liif;->a(I)Liif;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizg;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Liif;->a(J)Liif;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Liif;->a(F)Liif;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Liif;->b(F)Liif;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizg;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Liif;->a(D)Liif;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizg;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-virtual {p0, v0}, Liif;->c(F)Liif;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Liif;->b(I)Liif;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Liif;->c(I)Liif;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Liif;->a(Z)Liif;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lizg;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Liif;->b(Z)Liif;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lizg;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Liif;->a(Ljava/lang/String;)Liif;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lizg;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Liif;->d(I)Liif;

    goto/16 :goto_0

    :sswitch_e
    new-instance v0, Liih;

    invoke-direct {v0}, Liih;-><init>()V

    invoke-virtual {p1, v0}, Lizg;->a(Lizk;)V

    invoke-virtual {p0, v0}, Liif;->a(Liih;)Liif;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x31 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lizh;)V
    .locals 3

    iget-boolean v0, p0, Liif;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liif;->b:Liig;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILizk;)V

    :cond_0
    iget-boolean v0, p0, Liif;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Liif;->d:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_1
    iget-boolean v0, p0, Liif;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Liif;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(IJ)V

    :cond_2
    iget-boolean v0, p0, Liif;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Liif;->h:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_3
    iget-boolean v0, p0, Liif;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Liif;->j:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_4
    iget-boolean v0, p0, Liif;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Liif;->l:D

    invoke-virtual {p1, v0, v1, v2}, Lizh;->a(ID)V

    :cond_5
    iget-boolean v0, p0, Liif;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Liif;->n:F

    invoke-virtual {p1, v0, v1}, Lizh;->a(IF)V

    :cond_6
    iget-boolean v0, p0, Liif;->A:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Liif;->o:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_7
    iget-boolean v0, p0, Liif;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v1, p0, Liif;->q:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_8
    iget-boolean v0, p0, Liif;->r:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-boolean v1, p0, Liif;->s:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_9
    iget-boolean v0, p0, Liif;->t:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-boolean v1, p0, Liif;->u:Z

    invoke-virtual {p1, v0, v1}, Lizh;->a(IZ)V

    :cond_a
    iget-boolean v0, p0, Liif;->v:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Liif;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizh;->a(ILjava/lang/String;)V

    :cond_b
    iget-boolean v0, p0, Liif;->x:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget v1, p0, Liif;->y:I

    invoke-virtual {p1, v0, v1}, Lizh;->a(II)V

    :cond_c
    iget-object v0, p0, Liif;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liih;

    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lizh;->a(ILizk;)V

    goto :goto_0

    :cond_d
    return-void
.end method

.method public final b()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Liif;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liif;->b:Liig;

    invoke-static {v0, v1}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Liif;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Liif;->d:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Liif;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-wide v2, p0, Liif;->f:J

    invoke-static {v1, v2, v3}, Lizh;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Liif;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Liif;->h:F

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Liif;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Liif;->j:F

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Liif;->k:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-wide v2, p0, Liif;->l:D

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Liif;->m:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Liif;->n:F

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Liif;->A:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Liif;->o:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Liif;->p:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget v2, p0, Liif;->q:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Liif;->r:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-boolean v2, p0, Liif;->s:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Liif;->t:Z

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-boolean v2, p0, Liif;->u:Z

    invoke-static {v1}, Lizh;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Liif;->v:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Liif;->w:Ljava/lang/String;

    invoke-static {v1, v2}, Lizh;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Liif;->x:Z

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget v2, p0, Liif;->y:I

    invoke-static {v1, v2}, Lizh;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Liif;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liih;

    const/16 v3, 0xe

    invoke-static {v3, v0}, Lizh;->b(ILizk;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_d
    iput v1, p0, Liif;->B:I

    return v1
.end method

.method public final b(F)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->i:Z

    iput p1, p0, Liif;->j:F

    return-object p0
.end method

.method public final b(I)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->A:Z

    iput p1, p0, Liif;->o:I

    return-object p0
.end method

.method public final b(Z)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->t:Z

    iput-boolean p1, p0, Liif;->u:Z

    return-object p0
.end method

.method public final c(F)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->m:Z

    iput p1, p0, Liif;->n:F

    return-object p0
.end method

.method public final c(I)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->p:Z

    iput p1, p0, Liif;->q:I

    return-object p0
.end method

.method public final d(I)Liif;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Liif;->x:Z

    iput p1, p0, Liif;->y:I

    return-object p0
.end method
