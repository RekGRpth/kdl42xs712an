.class public Lapb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lapb;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lapb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Laup;)Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;
    .locals 13

    const/4 v12, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    iget-boolean v0, p0, Laup;->f:Z

    if-eqz v0, :cond_7

    iget-object v1, p0, Laup;->g:Laun;

    iget-boolean v2, v1, Laun;->e:Z

    iget-object v3, v1, Laun;->a:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;-><init>(ZLjava/lang/String;)V

    :goto_0
    iget-object v2, v1, Laun;->b:Ljava/lang/String;

    iget-object v3, v1, Laun;->d:Ljava/lang/String;

    iget-boolean v1, v1, Laun;->c:Z

    new-instance v6, Lcom/google/android/gms/auth/firstparty/shared/FACLData;

    invoke-direct {v6, v0, v2, v3, v1}, Lcom/google/android/gms/auth/firstparty/shared/FACLData;-><init>(Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_1
    iget-object v0, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    if-lez v0, :cond_1

    iget-object v0, p0, Laup;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    iget-object v0, v0, Lauq;->a:Ljava/lang/String;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    iget-object v2, p0, Laup;->a:Ljava/lang/String;

    iget-object v3, p0, Laup;->b:Ljava/lang/String;

    iget-object v4, p0, Laup;->c:Ljava/lang/String;

    iget-object v1, p0, Laup;->h:Ljava/lang/String;

    iget-boolean v0, p0, Laup;->d:Z

    if-eqz v0, :cond_2

    iget-object v5, p0, Laup;->e:Ljava/lang/String;

    :cond_2
    const-string v0, "GLSUser"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lapb;->a:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, " Converting ScopeDescription to ScopeDetail for service %s. With pacl? %s. With facl? %s"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v0, 0x3

    new-array v11, v0, [Ljava/lang/Object;

    aput-object v1, v11, v9

    if-nez v5, :cond_4

    move v0, v8

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v11, v8

    if-nez v6, :cond_5

    :goto_4
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "GLSUser"

    invoke-static {v8, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/FACLData;Ljava/util/List;)V

    return-object v0

    :cond_4
    move v0, v9

    goto :goto_3

    :cond_5
    move v8, v9

    goto :goto_4

    :cond_6
    move-object v0, v5

    goto/16 :goto_0

    :cond_7
    move-object v6, v5

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;)Lcom/google/android/gms/common/acl/ScopeData;
    .locals 11

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Lauq;

    invoke-direct {v4}, Lauq;-><init>()V

    invoke-virtual {v4, v0}, Lauq;->a(Ljava/lang/String;)Lauq;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->h:Lcom/google/android/gms/auth/firstparty/shared/FACLData;

    if-eqz v0, :cond_3

    const/4 v5, 0x1

    :goto_1
    if-eqz v5, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->h:Lcom/google/android/gms/auth/firstparty/shared/FACLData;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/FACLData;->a()Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;->a()Z

    move-result v0

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->h:Lcom/google/android/gms/auth/firstparty/shared/FACLData;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/FACLData;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v2, p0, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->h:Lcom/google/android/gms/auth/firstparty/shared/FACLData;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/shared/FACLData;->b()Z

    move-result v8

    move v9, v0

    move-object v6, v1

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->f()Ljava/lang/String;

    move-result-object v3

    :cond_2
    new-instance v0, Lcom/google/android/gms/common/acl/ScopeData;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/firstparty/shared/ScopeDetail;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/acl/ScopeData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;)V

    return-object v0

    :cond_3
    move v5, v1

    goto :goto_1

    :cond_4
    move v0, v1

    move-object v1, v2

    goto :goto_2

    :cond_5
    move v9, v1

    move v8, v1

    move-object v7, v2

    move-object v6, v2

    goto :goto_3
.end method
