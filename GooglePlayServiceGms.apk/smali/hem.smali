.class public final Lhem;
.super Lhak;
.source "SourceFile"


# instance fields
.field private final a:Lhak;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhak;)V
    .locals 1

    invoke-direct {p0}, Lhak;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhem;->b:Landroid/content/Context;

    iput-object p2, p0, Lhem;->a:Lhak;

    return-void
.end method

.method private a(Lhal;Ljava/lang/String;)Lhal;
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhem;->b:Landroid/content/Context;

    invoke-static {v0}, Lgsp;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lhen;

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lhen;-><init>(Landroid/content/Context;Lhal;Ljava/lang/String;B)V

    move-object p1, v0

    :cond_0
    return-object p1
.end method

.method private a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v0, "androidPackageName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lhem;->b:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lgth;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Lhal;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    const-string v1, "check_preauth"

    invoke-direct {p0, p2, v1}, Lhem;->a(Lhal;Ljava/lang/String;)Lhal;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lhak;->a(Landroid/os/Bundle;Lhal;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "checkForPreAuthorization: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p1}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x8

    const/4 v1, 0x0

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p2, v0, v1, v2}, Lhal;->a(IZLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lhal;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    const-string v1, "create_wallet_objects"

    invoke-direct {p0, p3, v1}, Lhem;->a(Lhal;Ljava/lang/String;)Lhal;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lhak;->a(Lcom/google/android/gms/wallet/CreateWalletObjectsRequest;Landroid/os/Bundle;Lhal;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "createWalletObjects: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x8

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1}, Lhal;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lhal;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    const-string v1, "load_full_wallet"

    invoke-direct {p0, p3, v1}, Lhem;->a(Lhal;Ljava/lang/String;)Lhal;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lhak;->a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Lhal;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "getFullWallet: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lgrc;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrc;->b(Ljava/lang/String;)Lgrc;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/FullWalletRequest;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrc;->a(Ljava/lang/String;)Lgrc;

    move-result-object v1

    iget-object v1, v1, Lgrc;->a:Lcom/google/android/gms/wallet/FullWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lhal;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    const-string v1, "load_masked_wallet"

    invoke-direct {p0, p3, v1}, Lhem;->a(Lhal;Ljava/lang/String;)Lhal;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lhak;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Lhal;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "getMaskedWalletForPreauthorizedBuyer: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    move-result-object v1

    iget-object v1, v1, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p3, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    invoke-virtual {v0, p1, p2}, Lhak;->a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lhal;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lhem;->a:Lhak;

    const-string v1, "change_masked_wallet"

    invoke-direct {p0, p4, v1}, Lhem;->a(Lhal;Ljava/lang/String;)Lhal;

    move-result-object v1

    invoke-virtual {v0, p1, p2, p3, v1}, Lhak;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lhal;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "ExceptionHandlingOwServi"

    const-string v1, "changeMaskedWallet: DeadObjectException"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lhem;->b:Landroid/content/Context;

    invoke-direct {p0, p3}, Lhem;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lhhe;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 v0, 0x8

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lgrl;

    move-result-object v1

    invoke-virtual {v1, p2}, Lgrl;->b(Ljava/lang/String;)Lgrl;

    move-result-object v1

    invoke-virtual {v1, p1}, Lgrl;->a(Ljava/lang/String;)Lgrl;

    move-result-object v1

    iget-object v1, v1, Lgrl;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p4, v0, v1, v2}, Lhal;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method
