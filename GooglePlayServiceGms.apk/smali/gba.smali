.class public final Lgba;
.super Lbip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/service/PlusService;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/service/PlusService;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-direct {p0}, Lbip;-><init>()V

    iput-object p2, p0, Lgba;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lbjv;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 21

    if-gtz p2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "clientVersion too old"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v6, 0x0

    if-eqz p7, :cond_1

    const-class v1, Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "request_visible_actions"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    :cond_1
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid callingPackage"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lbox;->c(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/PlusService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    :cond_3
    const/4 v1, 0x0

    if-eqz p7, :cond_4

    const-string v1, "skip_oob"

    const/4 v2, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :cond_4
    if-eqz v1, :cond_6

    invoke-static {}, Lcom/google/android/gms/plus/service/PlusService;->a()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Lgax;

    invoke-direct {v5}, Lgax;-><init>()V

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lgax;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v1, v3}, Lbjv;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_5
    invoke-virtual/range {p3 .. p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "invalid authPackage"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v14

    const/4 v15, 0x0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    if-ne v1, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/PlusService;->b(Ljava/lang/String;)I

    move-result v14

    const-string v1, "required_features"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    :cond_7
    if-nez v15, :cond_8

    sget-object v15, Lbje;->e:[Ljava/lang/String;

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/PlusService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "application_name"

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    :goto_1
    invoke-static/range {p7 .. p7}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v20

    new-instance v7, Lgcy;

    move-object/from16 v0, p0

    iget-object v0, v0, Lgba;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    sget-object v18, Lgau;->a:Lgau;

    sget-object v19, Lgbk;->a:Lgbk;

    move-object/from16 v8, p6

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v12, p5

    move-object v13, v6

    move-object/from16 v16, p1

    invoke-direct/range {v7 .. v20}, Lgcy;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lbjv;Ljava/lang/String;Lgau;Lgbk;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lgba;->a:Lcom/google/android/gms/plus/service/PlusService;

    invoke-static {v1, v7}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lgas;)V

    goto :goto_0

    :cond_9
    sget-object v11, Lbch;->c:Ljava/lang/String;

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method
