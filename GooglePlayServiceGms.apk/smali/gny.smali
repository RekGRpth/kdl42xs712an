.class public final Lgny;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)V
    .locals 0

    iput-object p1, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lgny;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)V

    return-void
.end method

.method private a(I)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 1

    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lgny;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-direct {p0, p1}, Lgny;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p2, :cond_2

    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bd    # com.google.android.gms.R.layout.plus_add_to_circle_list_item

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lgob;

    invoke-direct {v0, p2}, Lgob;-><init>(Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lgny;->a(I)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    iput p1, v1, Lgob;->a:I

    iput-object v2, v1, Lgob;->b:Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v1, Lgob;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    invoke-static {v0}, Lfdl;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lgob;->c:Landroid/widget/ImageView;

    const v3, 0x7f020094    # com.google.android.gms.R.drawable.common_ic_email_black_24

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    iget-object v0, v1, Lgob;->d:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lgob;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "checked"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    return-object p2

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgob;

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, v1, Lgob;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lgob;->c:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lbkt;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbkt;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    iget-object v0, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lbfs;

    move-result-object v0

    new-instance v5, Lgoa;

    iget-object v6, p0, Lgny;->a:Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {v5, v6, v3, v4}, Lgoa;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-virtual {v0, v5, v3, v8, v8}, Lbfs;->a(Lbfq;Ljava/lang/String;II)V

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
