.class public abstract Lamf;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field protected final a:Laoy;

.field protected final b:Lamg;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Lamf;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lamf;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    new-instance v0, Lamg;

    invoke-direct {v0, p1}, Lamg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lamf;->b:Lamg;

    new-instance v0, Laoy;

    invoke-direct {v0, p1}, Laoy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lamf;->a:Laoy;

    return-void
.end method


# virtual methods
.method public confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_2

    const-string v2, "password"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "password"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Laso;->a:Laso;

    iget-object v4, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v5, Lapp;

    iget-object v6, p0, Lamf;->a:Laoy;

    invoke-direct {v5, v6}, Lapp;-><init>(Laoy;)V

    new-instance v6, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v6}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    invoke-virtual {v6, v4}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v5, v2, v4}, Lapp;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v2

    sget-object v4, Laso;->c:Laso;

    if-ne v2, v4, :cond_0

    sget-object v2, Laso;->a:Laso;

    :cond_0
    if-ne v3, v2, :cond_1

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "booleanResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iget-object v4, p0, Lamf;->a:Laoy;

    iget-object v4, v4, Laoy;->c:Ljava/lang/String;

    iget-object v5, p0, Lamf;->a:Laoy;

    iget v5, v5, Laoy;->e:I

    invoke-direct {v3, v4, v5, v2, v2}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;

    move-result-object v2

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;

    move-result-object v2

    iget-object v3, p0, Lamf;->b:Lamg;

    invoke-virtual {v3, v2}, Lamg;->a(Lcom/google/android/gms/auth/firstparty/delegate/ConfirmCredentialsWorkflowRequest;)Landroid/app/PendingIntent;

    move-result-object v2

    iget-object v3, p0, Lamf;->a:Laoy;

    iget-object v3, v3, Laoy;->a:Landroid/content/Context;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "booleanResult"

    aput-object v5, v4, v1

    const-string v1, "accountType"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "retry"

    aput-object v1, v4, v0

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v3, v2, v0, p1}, Lcom/google/android/gms/auth/RedirectActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/List;Landroid/accounts/AccountAuthenticatorResponse;)Landroid/content/Intent;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1
.end method

.method public editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "editProperties hasn\'t been implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 10

    const/16 v6, 0xb

    const/4 v9, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    const-string v0, "setupWizard"

    invoke-virtual {p4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_3

    sget-object v0, Lamr;->a:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    move v1, v0

    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_4

    sget-object v0, Lamr;->b:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    :goto_1
    const-string v0, "pendingIntent"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v6, :cond_0

    iget-object v5, p0, Lamf;->a:Laoy;

    iget-object v6, p0, Lamf;->a:Laoy;

    invoke-virtual {v6, v0}, Laoy;->a(Landroid/app/PendingIntent;)I

    move-result v6

    invoke-virtual {v5, v1, v6}, Laoy;->a(II)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v1, p0, Lamf;->a:Laoy;

    invoke-virtual {v1, v0}, Laoy;->a(Landroid/app/PendingIntent;)I

    move-result v1

    iget-object v3, p0, Lamf;->a:Laoy;

    invoke-static {v0}, Laoy;->b(Landroid/app/PendingIntent;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-direct {v5, v3, v1, v0, v0}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;->a(Z)Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    new-instance v1, Land;

    iget-object v3, p0, Lamf;->a:Laoy;

    invoke-direct {v1, v3}, Land;-><init>(Laoy;)V

    const-string v3, "GLSActivity"

    invoke-static {v3, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lamf;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Getting token via AccountManager [%s, %s, %s]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v4, v2

    aput-object p3, v4, v8

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "GLSActivity"

    invoke-static {v4, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v0, v3, p3, p4}, Land;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "userRecoveryIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "userRecoveryIntent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "response"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lamf;->a:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lamf;->a:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "authAccount"

    aput-object v4, v3, v2

    const-string v2, "booleanResult"

    aput-object v2, v3, v8

    const-string v2, "accountType"

    aput-object v2, v3, v7

    const-string v2, "authtoken"

    aput-object v2, v3, v9

    const/4 v2, 0x4

    const-string v4, "retry"

    aput-object v4, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v0, v2, p1}, Lcom/google/android/gms/auth/RedirectActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/List;Landroid/accounts/AccountAuthenticatorResponse;)Landroid/content/Intent;

    move-result-object v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    return-object v0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lamf;->a:Laoy;

    invoke-virtual {v0, v1}, Laoy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_1
.end method

.method public getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const-string v1, "oauth:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "com.google.android.googleapps.permission.GOOGLE_AUTH."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lamf;->a:Laoy;

    iget-object v2, v2, Laoy;->b:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v1

    iget v3, v1, Landroid/content/pm/PermissionInfo;->labelRes:I

    if-eqz v3, :cond_0

    iget-object v3, v1, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    iget v4, v1, Landroid/content/pm/PermissionInfo;->labelRes:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v2, v1, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    iget-object v2, v1, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v2, v1, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v0, v1, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    if-nez v0, :cond_3

    :goto_1
    return-object p1

    :cond_3
    move-object p1, v0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 10

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lamf;->a:Laoy;

    iget-object v2, v2, Laoy;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    array-length v5, p3

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v2, p3, v3

    const-string v6, "youtubelinked"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "booleanResult"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v1

    :cond_1
    const-string v6, "hosted_or_google"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "google"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "legacy_google"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "legacy_hosted_or_google"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "service_"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    const-string v2, "services"

    invoke-virtual {v4, p2, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v7, ","

    invoke-virtual {v2, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    move v2, v1

    :goto_2
    if-ge v2, v8, :cond_5

    aget-object v9, v7, v2

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move v2, v0

    :goto_3
    if-nez v2, :cond_4

    move v0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_3
.end method

.method public updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iget-object v2, p0, Lamf;->a:Laoy;

    iget-object v2, v2, Laoy;->c:Ljava/lang/String;

    iget-object v3, p0, Lamf;->a:Laoy;

    iget v3, v3, Laoy;->e:I

    invoke-direct {v1, v2, v3, v0, v0}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;-><init>()V

    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;

    move-result-object v0

    iget-object v1, p0, Lamf;->b:Lamg;

    invoke-virtual {v1, v0}, Lamg;->a(Lcom/google/android/gms/auth/firstparty/delegate/UpdateCredentialsWorkflowRequest;)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v1, p0, Lamf;->a:Laoy;

    iget-object v1, v1, Laoy;->a:Landroid/content/Context;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "booleanResult"

    aput-object v4, v2, v3

    const-string v3, "accountType"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "retry"

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v0, v2, p1}, Lcom/google/android/gms/auth/RedirectActivity;->a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/util/List;Landroid/accounts/AccountAuthenticatorResponse;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "intent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "isTop"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v1
.end method
