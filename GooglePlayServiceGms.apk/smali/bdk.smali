.class public final Lbdk;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Lbdj;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput-object v0, p0, Lbdk;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lbdk;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lbdk;->c:Lbdj;

    const/4 v0, -0x1

    iput v0, p0, Lbdk;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-object v1, p0, Lbdk;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lbdk;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lbdk;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lbdk;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lbdk;->c:Lbdj;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lbdk;->c:Lbdj;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lbdk;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbdk;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbdk;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lbdk;->c:Lbdj;

    if-nez v0, :cond_1

    new-instance v0, Lbdj;

    invoke-direct {v0}, Lbdj;-><init>()V

    iput-object v0, p0, Lbdk;->c:Lbdj;

    :cond_1
    iget-object v0, p0, Lbdk;->c:Lbdj;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lizn;)V
    .locals 2

    iget-object v0, p0, Lbdk;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lbdk;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_0
    iget-object v0, p0, Lbdk;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lbdk;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_1
    iget-object v0, p0, Lbdk;->c:Lbdj;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lbdk;->c:Lbdj;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method
