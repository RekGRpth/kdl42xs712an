.class public final Lbhf;
.super Lbgy;
.source "SourceFile"


# static fields
.field public static final c:Lbhj;

.field public static final d:Lbhj;

.field public static final e:Lbhj;


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/ArrayList;

.field private h:Ljava/lang/String;

.field private i:Lbhj;

.field private j:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lbhg;

    invoke-direct {v0}, Lbhg;-><init>()V

    sput-object v0, Lbhf;->c:Lbhj;

    new-instance v0, Lbhh;

    invoke-direct {v0}, Lbhh;-><init>()V

    sput-object v0, Lbhf;->d:Lbhj;

    new-instance v0, Lbhi;

    invoke-direct {v0}, Lbhi;-><init>()V

    sput-object v0, Lbhf;->e:Lbhj;

    return-void
.end method

.method public constructor <init>(Lbgo;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lbgy;-><init>(Lbgo;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    iput-object p2, p0, Lbhf;->f:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lbhf;->j:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    :goto_0
    if-ge v0, v3, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isIdentifierIgnorable(C)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lbhf;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhf;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lbhj;Ljava/lang/String;)V
    .locals 8

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p3, p0, Lbhf;->h:Ljava/lang/String;

    iput-object p2, p0, Lbhf;->i:Lbhj;

    iget-object v0, p0, Lbhf;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v0, p0, Lbhf;->j:Ljava/util/Locale;

    iget-object v0, p0, Lbhf;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbhf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbhf;->h:Ljava/lang/String;

    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lbhf;->b:Lbgo;

    iget-object v2, v0, Lbgo;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v3, p0, Lbhf;->f:Ljava/lang/String;

    iget-object v0, p0, Lbhf;->b:Lbgo;

    instance-of v4, v0, Lbgw;

    const/4 v1, 0x0

    iget-object v0, p0, Lbhf;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_3

    if-eqz v4, :cond_1

    iget-object v0, p0, Lbhf;->b:Lbgo;

    check-cast v0, Lbgw;

    invoke-virtual {v0, v1}, Lbgw;->b(I)I

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v6

    invoke-virtual {v2, v3, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lbhf;->i:Lbhj;

    invoke-direct {p0, v0}, Lbhf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v7, p0, Lbhf;->h:Ljava/lang/String;

    invoke-interface {v6, v0, v7}, Lbhj;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_3
    return-void
.end method

.method public final b(I)I
    .locals 3

    iget-object v0, p0, Lbhf;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p1

    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lbhf;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0
.end method
