.class public abstract Lbro;
.super Lbqz;
.source "SourceFile"


# instance fields
.field protected final b:Lbrc;


# direct methods
.method protected constructor <init>(Lbrc;Lchq;)V
    .locals 0

    invoke-direct {p0, p2}, Lbqz;-><init>(Landroid/os/IInterface;)V

    iput-object p1, p0, Lbro;->b:Lbrc;

    return-void
.end method


# virtual methods
.method public abstract a(Lbsp;)V
.end method

.method public final a(Lcom/google/android/gms/drive/api/DriveAsyncService;)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lbro;->b:Lbrc;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lbrc;->a(Z)Lbsp;
    :try_end_0
    .catch Lbsl; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {p0, v0}, Lbro;->a(Lbsp;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Authorization has been revoked by the user. Reconnect the Drive API client to reauthorize."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
.end method
