.class public abstract Laut;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Laqw;

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v1, Laut;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laut;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Laoy;

    invoke-direct {v0, p1}, Laoy;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Laut;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <init> tokenRequest cannot be null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lbkm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iput-object v0, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    iput-object p3, p0, Laut;->d:Ljava/lang/String;

    iput-object p4, p0, Laut;->c:Ljava/lang/String;

    new-instance v0, Laqw;

    invoke-direct {v0, p1}, Laqw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laut;->e:Laqw;

    iput-boolean p5, p0, Laut;->f:Z

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    const-string v0, "token_response"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    return-object v0
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Laut;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Laut;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;-><init>()V

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    iget-object v2, p0, Laut;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    iget-object v2, p0, Laut;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v0

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;

    invoke-direct {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;-><init>()V

    iget-object v3, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->i()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;

    move-result-object v0

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->j()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;

    move-result-object v0

    iget-boolean v2, p0, Laut;->f:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a(Z)Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;

    move-result-object v0

    iget-object v2, p0, Laut;->e:Laqw;

    invoke-virtual {v2, v0}, Laqw;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    sget-object v2, Lapc;->b:Lapc;

    iget-object v2, v2, Lapc;->e:Laso;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Laso;

    move-result-object v3

    if-ne v2, v3, :cond_3

    :cond_2
    iget-object v0, p0, Laut;->e:Laqw;

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v0, v2}, Laqw;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    iget-object v2, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    :cond_3
    return-object v0

    :cond_4
    iget-object v2, p0, Laut;->e:Laqw;

    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;

    invoke-direct {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;

    move-result-object v0

    iget-object v3, p0, Laut;->b:Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->j()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->a(Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;

    move-result-object v0

    invoke-virtual {v2, v0}, Laqw;->a(Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method
