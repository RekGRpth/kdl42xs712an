.class public final Lhbb;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lgwv;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private Y:Ljava/lang/String;

.field private Z:Ljava/lang/String;

.field private aa:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private ab:Landroid/accounts/Account;

.field private ac:Lgyi;

.field private ad:Z

.field private ae:Lhbp;

.field b:Lgwr;

.field c:I

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/TextView;

.field f:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

.field g:Landroid/widget/ProgressBar;

.field protected final h:Lhcb;

.field private i:Ljas;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "createWalletObjects"

    invoke-static {v0}, Lgyi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lhbb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lhbb;->c:I

    new-instance v0, Lhbc;

    invoke-direct {v0, p0}, Lhbc;-><init>(Lhbb;)V

    iput-object v0, p0, Lhbb;->h:Lhcb;

    return-void
.end method

.method private J()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbb;->ad:Z

    invoke-direct {p0}, Lhbb;->K()V

    return-void
.end method

.method private K()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lhbb;->f:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-boolean v0, p0, Lhbb;->ad:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Z)V

    iget-object v3, p0, Lhbb;->g:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lhbb;->ad:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lhbb;->ae:Lhbp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbb;->ae:Lhbp;

    iget-boolean v3, p0, Lhbb;->ad:Z

    if-nez v3, :cond_3

    :goto_2
    invoke-interface {v0, v1}, Lhbp;->b(Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljas;Ljava/lang/String;Ljava/lang/String;)Lhbb;
    .locals 3

    new-instance v0, Lhbb;

    invoke-direct {v0}, Lhbb;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "request"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    const-string v2, "issuerName"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "programName"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhbb;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private a()V
    .locals 2

    iget v0, p0, Lhbb;->c:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget-object v1, p0, Lhbb;->h:Lhcb;

    invoke-interface {v0, v1}, Lhca;->c(Lhcb;)I

    move-result v0

    iput v0, p0, Lhbb;->c:I

    :cond_0
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    invoke-direct {p0}, Lhbb;->J()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0, p1, p2}, Lo;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->finish()V

    return-void
.end method

.method static synthetic a(Lhbb;)V
    .locals 3

    iget-object v0, p0, Lhbb;->b:Lgwr;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhbb;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhbb;->b:Lgwr;

    invoke-virtual {v0, v1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lgwr;->c(I)Lgwr;

    move-result-object v0

    iput-object v0, p0, Lhbb;->b:Lgwr;

    iget-object v0, p0, Lhbb;->b:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    iget-object v0, p0, Lhbb;->b:Lgwr;

    invoke-virtual {p0}, Lhbb;->k()Lu;

    move-result-object v1

    const-string v2, "CreateWalletObjectsFragment.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lgwr;->a(Lu;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lhbb;I)V
    .locals 0

    invoke-direct {p0, p1}, Lhbb;->c(I)V

    return-void
.end method

.method static synthetic a(Lhbb;Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lhbb;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method private b()Lgyi;
    .locals 2

    iget-object v0, p0, Lhbb;->ac:Lgyi;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->K_()Lu;

    move-result-object v0

    sget-object v1, Lhbb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgyi;

    iput-object v0, p0, Lhbb;->ac:Lgyi;

    :cond_0
    iget-object v0, p0, Lhbb;->ac:Lgyi;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    invoke-direct {p0}, Lhbb;->J()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lhbb;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhbb;->ae:Lhbp;

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    const v0, 0x7f04012f    # com.google.android.gms.R.layout.wallet_fragment_create_wallet_objects

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a032b    # com.google.android.gms.R.id.issuer_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhbb;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lhbb;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lhbb;->Y:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a032c    # com.google.android.gms.R.id.program_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lhbb;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lhbb;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lhbb;->Z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a02e4    # com.google.android.gms.R.id.button_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iput-object v0, p0, Lhbb;->f:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    iget-object v0, p0, Lhbb;->f:Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/DialogButtonBar;->a(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a02e3    # com.google.android.gms.R.id.prog_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lhbb;->g:Landroid/widget/ProgressBar;

    if-eqz p3, :cond_0

    const-string v0, "serviceConnectionSavePoint"

    const/4 v2, -0x1

    invoke-virtual {p3, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhbb;->c:I

    const-string v0, "remoteOperationInProgress"

    invoke-virtual {p3, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhbb;->ad:Z

    :cond_0
    invoke-direct {p0}, Lhbb;->K()V

    return-object v1
.end method

.method public final a(II)V
    .locals 1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lhbb;->c(I)V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lhbb;->aa:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lhbb;->ab:Landroid/accounts/Account;

    const-string v0, "request"

    const-class v2, Ljas;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Ljas;

    iput-object v0, p0, Lhbb;->i:Ljas;

    const-string v0, "issuerName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbb;->Y:Ljava/lang/String;

    const-string v0, "programName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbb;->Z:Ljava/lang/String;

    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lhbb;->aa:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lhbb;->ab:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lgyi;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lgyi;

    move-result-object v0

    iput-object v0, p0, Lhbb;->ac:Lgyi;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->K_()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lhbb;->ac:Lgyi;

    sget-object v2, Lhbb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lag;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->c()I

    iget-object v0, p0, Lhbb;->ac:Lgyi;

    invoke-virtual {v0, p1}, Lgyi;->a(Landroid/app/Activity;)V

    :cond_0
    instance-of v0, p1, Lhbp;

    if-eqz v0, :cond_1

    check-cast p1, Lhbp;

    iput-object p1, p0, Lhbb;->ae:Lhbp;

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lhbb;->ae:Lhbp;

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lhbb;->a()V

    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lhbb;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "remoteOperationInProgress"

    iget-boolean v1, p0, Lhbb;->ad:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbb;->ad:Z

    invoke-direct {p0}, Lhbb;->K()V

    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget-object v1, p0, Lhbb;->i:Ljas;

    invoke-interface {v0, v1}, Lhca;->a(Ljas;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a036c
        :pswitch_0    # com.google.android.gms.R.id.continue_btn
    .end packed-switch
.end method

.method public final w()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->w()V

    invoke-virtual {p0}, Lhbb;->k()Lu;

    move-result-object v0

    const-string v1, "CreateWalletObjectsFragment.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgwr;

    iput-object v0, p0, Lhbb;->b:Lgwr;

    iget-object v0, p0, Lhbb;->b:Lgwr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhbb;->b:Lgwr;

    invoke-virtual {v0, p0}, Lgwr;->a(Lgwv;)V

    :cond_0
    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lhbb;->b()Lgyi;

    move-result-object v0

    iget-object v0, v0, Lgyi;->a:Lhca;

    iget-object v1, p0, Lhbb;->h:Lhcb;

    iget v2, p0, Lhbb;->c:I

    invoke-interface {v0, v1, v2}, Lhca;->b(Lhcb;I)V

    :cond_1
    return-void
.end method

.method public final x()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->x()V

    invoke-direct {p0}, Lhbb;->a()V

    return-void
.end method
