.class public final Lgxm;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgxf;
.implements Lgyq;


# instance fields
.field private Y:Lgwy;

.field a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ImageView;

.field e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private f:Lioj;

.field private g:Lhgu;

.field private h:Z

.field private i:Lgwx;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgxm;->h:Z

    return-void
.end method

.method public static a(Lioj;)Lgxm;
    .locals 3

    new-instance v0, Lgxm;

    invoke-direct {v0}, Lgxm;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.google.android.gms.wallet.instrument"

    invoke-static {v1, v2, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lizs;)V

    invoke-virtual {v0, v1}, Lgxm;->g(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxm;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxm;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    iget-object v0, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lgxm;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    return-void
.end method

.method private b(Z)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    new-array v4, v0, [Lgyo;

    iget-object v0, p0, Lgxm;->i:Lgwx;

    aput-object v0, v4, v2

    iget-object v0, p0, Lgxm;->Y:Lgwy;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v3, v4, v0

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    if-eqz v6, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {v6}, Lgyo;->R_()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-interface {v6}, Lgyo;->S_()Z

    move-result v6

    if-nez v6, :cond_0

    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final R_()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgxm;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final S_()Z
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgxm;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const v5, 0x7f0a0333    # com.google.android.gms.R.id.exp_year

    const/4 v4, 0x0

    const v0, 0x7f040133    # com.google.android.gms.R.layout.wallet_fragment_local_update_instrument

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a0331    # com.google.android.gms.R.id.card_holder_name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v0, p0, Lgxm;->f:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxm;->f:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgxm;->f:Lioj;

    iget-object v0, v0, Lioj;->e:Lipv;

    iget-object v0, v0, Lipv;->a:Lixo;

    iget-object v0, v0, Lixo;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->f:Lioj;

    iget-object v2, v2, Lioj;->e:Lipv;

    iget-object v2, v2, Lipv;->a:Lixo;

    iget-object v2, v2, Lixo;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f0a031c    # com.google.android.gms.R.id.card_description

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgxm;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lgxm;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lgxm;->f:Lioj;

    invoke-static {v2}, Lgth;->b(Lioj;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a031b    # com.google.android.gms.R.id.card_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgxm;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lgxm;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lgxm;->f:Lioj;

    iget-object v3, p0, Lgxm;->g:Lhgu;

    invoke-static {v0, v2, v3}, Lgth;->a(Landroid/widget/ImageView;Lioj;Lhgu;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgxm;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f0a0332    # com.google.android.gms.R.id.exp_month

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v0, Lgww;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v2, v3, v4}, Lgww;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    new-instance v2, Lgwx;

    iget-object v3, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v4, v0}, Lgwx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgxm;->i:Lgwx;

    new-instance v2, Lgwy;

    iget-object v3, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v2, v3, v0}, Lgwy;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lgyo;)V

    iput-object v2, p0, Lgxm;->Y:Lgwy;

    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->i:Lgwx;

    iget-object v3, p0, Lgxm;->i:Lgwx;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->Y:Lgwy;

    iget-object v3, p0, Lgxm;->Y:Lgwy;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgwi;Lgyo;)V

    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->i:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->Y:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lgyo;)V

    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->i:Lgwx;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->Y:Lgwy;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    invoke-direct {p0}, Lgxm;->b()V

    return-object v1

    :cond_1
    iget-object v0, p0, Lgxm;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a()Linv;
    .locals 4

    const/4 v1, 0x0

    new-instance v2, Lint;

    invoke-direct {v2}, Lint;-><init>()V

    iget-object v0, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lixo;

    invoke-direct {v0}, Lixo;-><init>()V

    iput-object v0, v2, Lint;->d:Lixo;

    iget-object v0, v2, Lint;->d:Lixo;

    iget-object v3, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lixo;->s:Ljava/lang/String;

    :cond_0
    :try_start_0
    iget-object v0, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    :try_start_1
    iget-object v3, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/lit16 v3, v3, 0x7d0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lint;->b:I

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Lint;->c:I

    :cond_2
    new-instance v0, Linv;

    invoke-direct {v0}, Linv;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Linv;->a:I

    iput-object v2, v0, Linv;->b:Lint;

    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lgxm;->h:Z

    invoke-direct {p0}, Lgxm;->b()V

    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "Fragment requires instrument extra!"

    invoke-static {v1, v2}, Lbkm;->b(ZLjava/lang/Object;)V

    const-string v1, "com.google.android.gms.wallet.instrument"

    const-class v2, Lioj;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lizs;

    move-result-object v0

    check-cast v0, Lioj;

    iput-object v0, p0, Lgxm;->f:Lioj;

    if-eqz p1, :cond_0

    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgxm;->h:Z

    :cond_0
    new-instance v0, Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Lhgu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgxm;->g:Lhgu;

    iget-object v0, p0, Lgxm;->g:Lhgu;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1}, Lhgs;->a(Landroid/content/Context;)Lhgs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhgu;->a(Lhgs;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "enabled"

    iget-boolean v1, p0, Lgxm;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final i()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lgxm;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lgxm;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lgxm;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
