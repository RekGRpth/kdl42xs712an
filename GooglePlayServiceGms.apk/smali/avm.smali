.class public final Lavm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lavh;


# instance fields
.field private final a:Lavj;

.field private final b:Lauz;

.field private final c:Ljcf;


# direct methods
.method public constructor <init>(Lavj;Lauz;)V
    .locals 1

    new-instance v0, Ljcd;

    invoke-direct {v0}, Ljcd;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lavm;-><init>(Lavj;Lauz;Ljcf;)V

    return-void
.end method

.method private constructor <init>(Lavj;Lauz;Ljcf;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lavm;->a:Lavj;

    iput-object p2, p0, Lavm;->b:Lauz;

    iput-object p3, p0, Lavm;->c:Ljcf;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lavm;->a:Lavj;

    invoke-interface {v1, p1}, Lavj;->a(Ljava/lang/String;)Lavi;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lavm;->b:Lauz;

    invoke-interface {v2, v1}, Lauz;->a(Lavi;)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v3, "HMACSHA256"

    invoke-static {v3}, Lbox;->b(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v1, v1, Lavi;->b:[B

    const-string v5, "HMACSHA256"

    invoke-direct {v4, v1, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v3, v4}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Ljca;

    iget-object v1, p0, Lavm;->c:Ljcf;

    invoke-direct {v0, v3, v1}, Ljca;-><init>(Ljavax/crypto/Mac;Ljcf;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljca;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string v3, "%02d%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x64

    rem-long/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
