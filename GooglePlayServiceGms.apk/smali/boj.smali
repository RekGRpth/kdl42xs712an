.class final Lboj;
.super Landroid/app/AlertDialog;
.source "SourceFile"


# instance fields
.field private a:Lvf;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-boolean v6, p0, Lboj;->b:Z

    new-instance v2, Lvf;

    invoke-direct {v2, p1}, Lvf;-><init>(Landroid/content/Context;)V

    iget-object v0, v2, Lvf;->a:Landroid/content/Context;

    invoke-static {p2}, Lvf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lvf;->b:Ljava/lang/String;

    iget-object v0, v2, Lvf;->a:Landroid/content/Context;

    invoke-static {p3}, Lvf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lvf;->c:Ljava/lang/String;

    invoke-static {}, Lboi;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0b0477    # com.google.android.gms.R.string.common_url_load_unsuccessful_message_wifi_only

    :goto_0
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lvf;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v2, Lvf;->c:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lvf;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lvf;->a()V

    iget-object v0, v2, Lvf;->f:Landroid/webkit/WebView;

    iget-object v3, v2, Lvf;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iput-object v2, p0, Lboj;->a:Lvf;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p4}, Lboj;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f0b03ec    # com.google.android.gms.R.string.plus_close_button_label

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v2, Lbok;

    invoke-direct {v2, p0}, Lbok;-><init>(Lboj;)V

    invoke-virtual {p0, v1, v0, v2}, Lboj;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lboj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-lez v3, :cond_2

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v3, v0

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v3, v5

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    add-double/2addr v3, v5

    double-to-int v0, v3

    :goto_1
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lboj;->a:Lvf;

    invoke-virtual {v0}, Lvf;->a()V

    iget-object v0, v0, Lvf;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v2}, Lboj;->setView(Landroid/view/View;)V

    return-void

    :cond_1
    const v0, 0x7f0b0476    # com.google.android.gms.R.string.common_url_load_unsuccessful_message

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lboj;->a:Lvf;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lvf;->f:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lvf;->f:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public final onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    const/4 v1, -0x1

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    iget-boolean v0, p0, Lboj;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lboj;->b:Z

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    if-eq v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lboj;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lboj;->b:Z

    goto :goto_0
.end method
