.class public final Lexi;
.super Lk;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Y:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lk;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lexi;->Y:Z

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0475    # com.google.android.gms.R.string.common_mdm_feature_name

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b01dc    # com.google.android.gms.R.string.mdm_activate_device_admin_dialog_message

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b01dd    # com.google.android.gms.R.string.mdm_activate_device_admin_dialog_positive

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b01de    # com.google.android.gms.R.string.mdm_activate_device_admin_dialog_negative

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lexi;->Y:Z

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Lexi;->Y:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lexv;->a(Landroid/content/Context;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lk;->onDismiss(Landroid/content/DialogInterface;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/mdm/services/SitrepService;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_2
    sget-boolean v0, Lexw;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "Null activity when trying to perform device admin requests."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
