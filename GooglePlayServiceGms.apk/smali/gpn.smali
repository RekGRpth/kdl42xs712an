.class public final Lgpn;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lfrc;


# instance fields
.field private Y:Z

.field private Z:Z

.field protected a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

.field private aa:Lgpp;

.field private ab:Lfqr;

.field private ac:Lgod;

.field private ad:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private ae:Landroid/view/View;

.field private af:Landroid/widget/ScrollView;

.field private ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

.field private ah:Landroid/widget/ImageView;

.field private ai:Lfst;

.field protected b:Landroid/view/ViewGroup;

.field protected c:Landroid/widget/CheckBox;

.field protected d:Z

.field protected e:Lcom/google/android/gms/plus/model/posts/Settings;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lgpn;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lgpn;)Landroid/widget/ScrollView;
    .locals 1

    iget-object v0, p0, Lgpn;->af:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic c(Lgpn;)Lgpp;
    .locals 1

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    return-object v0
.end method

.method static synthetic d(Lgpn;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpn;->i:Z

    return v0
.end method

.method static synthetic e(Lgpn;)Z
    .locals 1

    iget-boolean v0, p0, Lgpn;->i:Z

    return v0
.end method

.method static synthetic f(Lgpn;)Z
    .locals 1

    iget-boolean v0, p0, Lgpn;->g:Z

    return v0
.end method

.method static synthetic g(Lgpn;)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpn;->g:Z

    return v0
.end method


# virtual methods
.method public final E_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->E_()V

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->M()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgpn;->a(Lcom/google/android/gms/plus/model/posts/Settings;)V

    :cond_0
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->N()Lfst;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgpn;->a(Lfst;)V

    :cond_1
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->T()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->S()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgpn;->a(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-boolean v0, v0, Lgpx;->l:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lgpn;->L()V

    :cond_3
    return-void
.end method

.method public final J()Lcom/google/android/gms/plus/model/posts/Post;
    .locals 14

    const/4 v13, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lgpn;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v2, Lbcz;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v2}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_1
    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lgpq;->a(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->p:Lgqa;

    iget-object v7, v0, Lgqa;->a:Landroid/os/Bundle;

    :goto_2
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->o:Lgqb;

    iget-object v8, v0, Lgqb;->a:Landroid/os/Bundle;

    :goto_3
    iget-boolean v0, p0, Lgpn;->d:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v2, Lbcz;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v5}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v5

    invoke-static {v5}, Lbcl;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v5

    invoke-virtual {v0, v2, v1, v5, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    iget-object v0, p0, Lgpn;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    move v2, v0

    :goto_4
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Post;

    iget-object v5, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v5}, Lgpp;->j()Lgpx;

    move-result-object v5

    iget-object v9, v5, Lgpx;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-object v2, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v2}, Lgpp;->j()Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v11

    iget-object v2, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v2}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v12

    move-object v2, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->l()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_3
    iget-boolean v1, p0, Lgpn;->Y:Z

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    if-ne v1, v13, :cond_4

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->E:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_4
    iget-boolean v1, p0, Lgpn;->Y:Z

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->F:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_5
    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->j()Lgpx;

    move-result-object v1

    invoke-virtual {v1}, Lgpx;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->o:Lgqb;

    invoke-virtual {v1}, Lgqb;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    sget-object v2, Lbcz;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_6
    move-object v1, v0

    goto/16 :goto_0

    :cond_7
    move-object v3, v1

    goto/16 :goto_1

    :cond_8
    move-object v7, v1

    goto/16 :goto_2

    :cond_9
    move-object v8, v1

    goto/16 :goto_3

    :cond_a
    move v2, v13

    goto/16 :goto_4
.end method

.method public final K()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    invoke-virtual {v0}, Lgpr;->V()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->o:Lgqb;

    invoke-virtual {v0}, Lgqb;->b()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    :goto_1
    iget-object v3, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v3}, Lgpp;->j()Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v3}, Lgpp;->j()Lgpx;

    move-result-object v3

    iget-object v3, v3, Lgpx;->o:Lgqb;

    invoke-virtual {v3}, Lgqb;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v2

    :goto_2
    iget-object v4, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-lez v4, :cond_7

    move v4, v2

    :goto_3
    if-nez v0, :cond_4

    if-nez v3, :cond_4

    if-eqz v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    move v4, v1

    goto :goto_3
.end method

.method public final L()V
    .locals 2

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0}, Lgod;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgpn;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0, v1}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    :cond_0
    return-void
.end method

.method public final M()V
    .locals 1

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0}, Lgod;->a()V

    :cond_0
    return-void
.end method

.method public final N()V
    .locals 2

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgod;->a(Z)V

    :cond_0
    return-void
.end method

.method public final O()V
    .locals 1

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0}, Lgod;->J()V

    :cond_0
    return-void
.end method

.method public final P()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0}, Lgod;->b()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Q()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lgpn;->ac:Lgod;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v1}, Lgod;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v1}, Lgod;->K()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final R()V
    .locals 2

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->h()Lgpy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->h()Lgpy;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgpy;->a(Z)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const/4 v1, 0x0

    const v0, 0x7f04010e    # com.google.android.gms.R.layout.plus_sharebox_fragment

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpn;->ae:Landroid/view/View;

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v2, 0x7f0a02b6    # com.google.android.gms.R.id.mention_scroll_view

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lgpn;->af:Landroid/widget/ScrollView;

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v2, 0x7f0a00b8    # com.google.android.gms.R.id.audience_view

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AudienceView;

    iput-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    if-eqz p3, :cond_0

    const-string v0, "audience_view_enabled"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setEnabled(Z)V

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v1, 0x7f0a024f    # com.google.android.gms.R.id.avatar

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgpn;->ah:Landroid/widget/ImageView;

    iget-object v0, p0, Lgpn;->ah:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lgpn;->j()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200bf    # com.google.android.gms.R.drawable.default_avatar

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lgoo;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v1, 0x7f0a02b8    # com.google.android.gms.R.id.compose_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    new-instance v1, Lgpo;

    invoke-virtual {p0}, Lgpn;->j()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lgpo;-><init>(Lgpn;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v1, 0x7f0a02d2    # com.google.android.gms.R.id.link_preview_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lgpn;->b:Landroid/view/ViewGroup;

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lgpn;->Y:Z

    iget-object v0, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgpn;->Z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0}, Lbla;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->o()V

    iput-boolean v1, p0, Lgpn;->Z:Z

    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpp;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpp;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lgpp;

    iput-object v0, p0, Lgpn;->aa:Lgpp;

    instance-of v0, p1, Lfqr;

    if-eqz v0, :cond_1

    check-cast p1, Lfqr;

    iput-object p1, p0, Lgpn;->ab:Lfqr;

    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lgpn;->ah:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->b()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0, p1}, Lbkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lgpn;->i:Z

    :cond_0
    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setEnabled(Z)V

    :cond_1
    iget-boolean v0, p0, Lgpn;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-boolean v0, v0, Lgpx;->l:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0, p1}, Lgpp;->b(Lcom/google/android/gms/common/people/data/Audience;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Lgpn;->L()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 8

    const/4 v7, 0x1

    iput-object p1, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    iget-object v1, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-interface {v0, v1}, Lgpp;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    :goto_0
    iget-object v0, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgpn;->Z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0}, Lbla;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->o()V

    iput-boolean v7, p0, Lgpn;->Z:Z

    :cond_0
    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lgpn;->s()Lak;

    move-result-object v1

    iget-object v2, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v2}, Lgpp;->g()Lgpr;

    move-result-object v2

    invoke-virtual {v2}, Lgpr;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v3}, Lgpp;->j()Lgpx;

    move-result-object v3

    invoke-virtual {v3}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lbch;->b:Ljava/lang/String;

    iget-object v5, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v5}, Lgpp;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v6}, Lgpp;->k()Lfrb;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Lak;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lfrb;)V

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->p()V

    iget-boolean v0, p0, Lgpn;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v1, Lbcz;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iput-boolean v7, p0, Lgpn;->f:Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    iget-object v1, p0, Lgpn;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-interface {v0, v1}, Lgpp;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    sget-object v1, Lbkw;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-interface {v0, v1}, Lgpp;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V
    .locals 5

    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "ShareBox"

    const-string v2, "Loaded add to circle data: %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lgpn;->L()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lgpn;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    iget-object v1, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0, v1}, Lag;->c(Landroid/support/v4/app/Fragment;)Lag;

    move-result-object v0

    invoke-virtual {v0}, Lag;->d()I

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0, p1, p2}, Lgod;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 1

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v0, p1}, Lgod;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    :cond_0
    return-void
.end method

.method public final a(Lfst;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p1, p0, Lgpn;->ai:Lfst;

    iget-boolean v0, p0, Lgpn;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpn;->ai:Lfst;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    invoke-virtual {v0}, Lgpx;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->o:Lgqb;

    invoke-virtual {v0}, Lgqb;->e()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p0, Lgpn;->ai:Lfst;

    if-nez v0, :cond_4

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->j()Lgpx;

    move-result-object v0

    iget-object v0, v0, Lgpx;->n:Ljava/lang/String;

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->o:Lgqb;

    invoke-static {v0, v1}, Lgpq;->a(Ljava/lang/String;Lgqb;)Lfst;

    move-result-object v0

    iput-object v0, p0, Lgpn;->ai:Lfst;

    iget-boolean v0, p0, Lgpn;->h:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v1, Lbcz;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iput-boolean v3, p0, Lgpn;->h:Z

    :cond_4
    iget-object v0, p0, Lgpn;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v0, Lgoy;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v1}, Lgoy;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->g()Lgpr;

    move-result-object v1

    invoke-virtual {v1}, Lgpr;->a()Lftx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgoy;->a(Lftx;)V

    iget-object v1, p0, Lgpn;->ai:Lfst;

    iget-object v2, p0, Lgpn;->ai:Lfst;

    invoke-virtual {v2}, Lfst;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgoy;->a(Lfst;Ljava/lang/String;)Z

    iget-object v1, p0, Lgpn;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-boolean v0, p0, Lgpn;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v1, Lbcz;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iput-boolean v3, p0, Lgpn;->h:Z

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    if-eq p1, p0, :cond_0

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    iget-object v0, v0, Lfrb;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lgpn;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    :cond_0
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "logged_expand_sharebox"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->f:Z

    const-string v0, "logged_comment_added"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->g:Z

    const-string v0, "logged_preview_shown"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->h:Z

    const-string v0, "user_edited"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->i:Z

    const-string v0, "saw_domain_restriction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->Y:Z

    const-string v0, "saw_underage_warning"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->Z:Z

    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lgpn;->ad:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lgpn;->i:Z

    return v0
.end method

.method public final c(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->c(Z)V

    if-nez p1, :cond_1

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->h()Lgpy;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgpy;->c(I)V

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->requestFocusFromTouch()Z

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v1}, Lbpo;->a(Landroid/content/Context;Landroid/view/View;)Z

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpn;->ab:Lfqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->b(Lfrc;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lgpn;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v1}, Lgpp;->j()Lgpx;

    move-result-object v1

    iget-object v1, v1, Lgpx;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lgpn;->d:Z

    iget-boolean v0, p0, Lgpn;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.SHARE_ON_PLUS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v2, 0x7f0a02d1    # com.google.android.gms.R.id.share_gplus_checkbox

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lgpn;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lgpn;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lgpn;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lgpn;->ae:Landroid/view/View;

    const v1, 0x7f0a02d0    # com.google.android.gms.R.id.share_gplus_checkbox_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    const-string v0, "logged_expand_sharebox"

    iget-boolean v1, p0, Lgpn;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "logged_comment_added"

    iget-boolean v1, p0, Lgpn;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "logged_preview_shown"

    iget-boolean v1, p0, Lgpn;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "user_edited"

    iget-boolean v1, p0, Lgpn;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "saw_domain_restriction"

    iget-boolean v1, p0, Lgpn;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "saw_underage_warning"

    iget-boolean v1, p0, Lgpn;->Z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    if-eqz v0, :cond_0

    const-string v0, "audience_view_enabled"

    iget-object v1, p0, Lgpn;->ag:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "add_to_circle_data"

    iget-object v1, p0, Lgpn;->ad:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public final h(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->h(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgpn;->k()Lu;

    move-result-object v0

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    const-string v2, "add_to_circle_fragment"

    invoke-virtual {v0, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lgod;

    iput-object v0, p0, Lgpn;->ac:Lgod;

    iget-object v0, p0, Lgpn;->ac:Lgod;

    if-nez v0, :cond_0

    new-instance v0, Lgod;

    invoke-direct {v0}, Lgod;-><init>()V

    iput-object v0, p0, Lgpn;->ac:Lgod;

    const v0, 0x7f0a02cf    # com.google.android.gms.R.id.add_to_circle_container

    iget-object v2, p0, Lgpn;->ac:Lgod;

    const-string v3, "add_to_circle_fragment"

    invoke-virtual {v1, v0, v2, v3}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_0
    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v1, v0}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :cond_2
    iget-boolean v0, p0, Lgpn;->d:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgpn;->ac:Lgod;

    invoke-virtual {v1, v0}, Lag;->b(Landroid/support/v4/app/Fragment;)Lag;

    :cond_3
    invoke-virtual {v1}, Lag;->f()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v1}, Lag;->d()I

    :cond_4
    iget-object v0, p0, Lgpn;->ab:Lfqr;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgpn;->ab:Lfqr;

    invoke-interface {v0}, Lfqr;->k()Lfrb;

    move-result-object v0

    invoke-virtual {v0, p0}, Lfrb;->a(Lfrc;)V

    :cond_5
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a00b8    # com.google.android.gms.R.id.audience_view

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->e()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->g()Lgpr;

    move-result-object v0

    sget-object v1, Lbcz;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lgpn;->aa:Lgpp;

    invoke-interface {v0}, Lgpp;->n()V

    :cond_0
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f0a02b8    # com.google.android.gms.R.id.compose_text

    if-ne v0, v1, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, p1}, Lbpo;->b(Landroid/content/Context;Landroid/view/View;)Z

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
