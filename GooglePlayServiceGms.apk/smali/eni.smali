.class public final Leni;
.super Lenf;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/gms/icing/service/IndexWorkerService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/service/IndexWorkerService;Landroid/content/Intent;)V
    .locals 1

    iput-object p1, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    iput-object p2, p0, Leni;->a:Landroid/content/Intent;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lenf;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    iget-object v0, p0, Leni;->a:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Leni;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    new-instance v0, Lenj;

    iget-object v1, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v0, v1}, Lenj;-><init>(Landroid/app/Service;)V

    iget-boolean v1, v0, Lenj;->a:Z

    if-eqz v1, :cond_3

    iget-boolean v0, v0, Lenj;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b()V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_2
    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    move-result-object v0

    invoke-virtual {v0}, Lejm;->n()Lenh;

    goto :goto_0

    :cond_4
    const-string v1, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    goto :goto_0

    :cond_5
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Z)V

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b()V

    goto :goto_0

    :cond_6
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    move-result-object v0

    iget-object v1, p0, Leni;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lejm;->a(Landroid/content/Intent;)V

    goto :goto_0

    :cond_8
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b(Z)V

    goto :goto_0

    :cond_a
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b(Z)V

    goto/16 :goto_0

    :cond_b
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    move-result-object v0

    invoke-virtual {v0}, Lejm;->v()V

    goto/16 :goto_0

    :cond_c
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_d
    iget-object v0, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    move-result-object v0

    iget-object v1, p0, Leni;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lejm;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_e
    const-string v1, "com.google.android.gms.icing.INDEX_CONTENT_PROVIDER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v0, p0, Leni;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_11

    const-string v1, "delay"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_f

    const-string v0, "Received invalid intent: %s"

    iget-object v1, p0, Leni;->a:Landroid/content/Intent;

    invoke-static {v0, v1}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_f
    iget-object v2, p0, Leni;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lejm;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lejm;->a(J)V

    goto/16 :goto_0

    :cond_10
    const-string v1, "Received unexpected intent: %s"

    invoke-static {v1, v0}, Lehe;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_11
    move-wide v0, v2

    goto :goto_1
.end method
