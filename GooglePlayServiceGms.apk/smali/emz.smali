.class public abstract Lemz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Landroid/os/ConditionVariable;

.field public c:I

.field final d:Ljava/lang/ThreadLocal;

.field private e:Ljava/util/Set;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lemz;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lemz;->e:Ljava/util/Set;

    const/4 v0, 0x0

    iput v0, p0, Lemz;->f:I

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lemz;->b:Landroid/os/ConditionVariable;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lemz;->d:Ljava/lang/ThreadLocal;

    return-void
.end method

.method static synthetic a(Lemz;)V
    .locals 5

    iget-object v1, p0, Lemz;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lemz;->c()V

    iget v0, p0, Lemz;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lemz;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lemz;->e:Ljava/util/Set;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lemz;->e:Ljava/util/Set;

    new-instance v2, Lena;

    invoke-direct {v2, p0, v0}, Lena;-><init>(Lemz;Ljava/util/Set;)V

    const-wide/16 v3, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lemz;->a(Lenf;J)Lenf;

    :cond_0
    iget v0, p0, Lemz;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lemz;->f:I

    iget v0, p0, Lemz;->f:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lemz;->b()V

    iget-object v0, p0, Lemz;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    :cond_1
    invoke-virtual {p0}, Lemz;->c()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lenf;J)Lenf;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-wide/16 v3, 0x0

    cmp-long v0, p2, v3

    if-ltz v0, :cond_0

    const-wide/16 v3, 0x1388

    cmp-long v0, p2, v3

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Delay out of range: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lbkm;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget v0, p1, Lenf;->h:I

    invoke-static {v0}, Leng;->a(I)V

    invoke-virtual {p1}, Lenf;->c()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lemz;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    invoke-virtual {p0}, Lemz;->c()V

    iget v0, p0, Lemz;->c:I

    if-eqz v0, :cond_2

    instance-of v0, p1, Lena;

    if-nez v0, :cond_2

    const-string v0, "Scheduling new tasks while awaiting pending to complete: %s."

    invoke-static {v0, p1}, Lehe;->e(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_2
    iget-object v0, p0, Lemz;->e:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget v0, p0, Lemz;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lemz;->f:I

    iget-object v0, p0, Lemz;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    iget v0, p0, Lemz;->f:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, p1}, Lemz;->a(Lenf;)V

    :cond_3
    invoke-virtual {p0}, Lemz;->c()V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v2, v1}, Lenf;->a(II)V

    new-instance v0, Lenb;

    invoke-direct {v0, p0, p1}, Lenb;-><init>(Lemz;Lenf;)V

    invoke-virtual {p0, v0, p2, p3}, Lemz;->a(Lenb;J)V

    return-object p1

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method protected abstract a(Lenb;J)V
.end method

.method protected abstract a(Lenf;)V
.end method

.method public final a()Z
    .locals 2

    const/4 v1, 0x2

    invoke-static {v1}, Leng;->a(I)V

    iget-object v0, p0, Lemz;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lemz;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()V
.end method

.method public final c()V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lemz;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget v2, p0, Lemz;->f:I

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lbkm;->a(Z)V

    iget v2, p0, Lemz;->c:I

    if-ltz v2, :cond_1

    :goto_1
    invoke-static {v0}, Lbkm;->a(Z)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method
