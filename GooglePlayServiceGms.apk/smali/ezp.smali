.class public final Lezp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/util/Calendar;

.field public final d:Ljava/util/Calendar;

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:I

.field public final i:I

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:F

.field public final o:F

.field public final p:F

.field public final q:Z


# direct methods
.method private constructor <init>(II)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lezp;->a:I

    iput p2, p0, Lezp;->b:I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lezp;->c:Ljava/util/Calendar;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lezp;->d:Ljava/util/Calendar;

    const/4 v0, -0x1

    iput v0, p0, Lezp;->e:I

    const-string v0, "equirectangular"

    iput-object v0, p0, Lezp;->f:Ljava/lang/String;

    iput-boolean v3, p0, Lezp;->g:Z

    iput p1, p0, Lezp;->h:I

    iput p2, p0, Lezp;->i:I

    iput v2, p0, Lezp;->l:I

    iput v2, p0, Lezp;->m:I

    iput p1, p0, Lezp;->j:I

    iput p2, p0, Lezp;->k:I

    iput v1, p0, Lezp;->n:F

    iput v1, p0, Lezp;->o:F

    iput v1, p0, Lezp;->p:F

    iput-boolean v3, p0, Lezp;->q:Z

    return-void
.end method

.method private constructor <init>(IILjava/util/Calendar;Ljava/util/Calendar;ILjava/lang/String;ZIIIIIIFFF)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lezp;->a:I

    iput p2, p0, Lezp;->b:I

    iput-object p3, p0, Lezp;->c:Ljava/util/Calendar;

    iput-object p4, p0, Lezp;->d:Ljava/util/Calendar;

    iput p5, p0, Lezp;->e:I

    iput-object p6, p0, Lezp;->f:Ljava/lang/String;

    iput-boolean p7, p0, Lezp;->g:Z

    iput p8, p0, Lezp;->h:I

    iput p9, p0, Lezp;->i:I

    iput p10, p0, Lezp;->j:I

    iput p11, p0, Lezp;->k:I

    iput p12, p0, Lezp;->l:I

    iput p13, p0, Lezp;->m:I

    move/from16 v0, p14

    iput v0, p0, Lezp;->n:F

    move/from16 v0, p15

    iput v0, p0, Lezp;->o:F

    move/from16 v0, p16

    iput v0, p0, Lezp;->p:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lezp;->q:Z

    return-void
.end method

.method private static a(Lps;Ljava/lang/String;)D
    .locals 2

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lezo;)Lezp;
    .locals 25

    invoke-interface/range {p0 .. p0}, Lezo;->a()Ljava/io/InputStream;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v2, 0x0

    invoke-static {v6}, Lfah;->a(Ljava/io/InputStream;)Lps;

    move-result-object v6

    if-eqz v6, :cond_c

    :try_start_0
    const-string v7, "FirstPhotoDate"

    invoke-static {v6, v7}, Lezp;->c(Lps;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v5

    const-string v7, "LastPhotoDate"

    invoke-static {v6, v7}, Lezp;->c(Lps;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v4

    const-string v7, "SourcePhotosCount"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v3

    const-string v7, "ProjectionType"

    const-string v19, "http://ns.google.com/photos/1.0/panorama/"

    move-object/from16 v0, v19

    invoke-interface {v6, v0, v7}, Lps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    const-string v19, "http://ns.google.com/photos/1.0/panorama/"

    move-object/from16 v0, v19

    invoke-interface {v6, v0, v7}, Lps;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    :goto_1
    const-string v7, "UsePanoramaViewer"

    const-string v19, "http://ns.google.com/photos/1.0/panorama/"

    move-object/from16 v0, v19

    invoke-interface {v6, v0, v7}, Lps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_3

    const-string v19, "http://ns.google.com/photos/1.0/panorama/"

    move-object/from16 v0, v19

    invoke-interface {v6, v0, v7}, Lps;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    :goto_2
    const-string v7, "CroppedAreaImageWidthPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v10

    const-string v7, "CroppedAreaImageHeightPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v11

    const-string v7, "FullPanoWidthPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v12

    const-string v7, "FullPanoHeightPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v13

    const-string v7, "CroppedAreaLeftPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v14

    const-string v7, "CroppedAreaTopPixels"

    invoke-static {v6, v7}, Lezp;->b(Lps;Ljava/lang/String;)I

    move-result v15

    const-string v7, "PoseHeadingDegrees"

    invoke-static {v6, v7}, Lezp;->a(Lps;Ljava/lang/String;)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-float v0, v0

    move/from16 v16, v0

    const-string v7, "PosePitchDegrees"

    invoke-static {v6, v7}, Lezp;->a(Lps;Ljava/lang/String;)D

    move-result-wide v19

    move-wide/from16 v0, v19

    double-to-float v0, v0

    move/from16 v17, v0

    const-string v7, "PoseRollDegrees"

    invoke-static {v6, v7}, Lezp;->a(Lps;Ljava/lang/String;)D
    :try_end_0
    .catch Lpr; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    double-to-float v0, v6

    move/from16 v18, v0

    if-lez v10, :cond_4

    if-lez v11, :cond_4

    if-lez v12, :cond_4

    if-lez v13, :cond_4

    const/4 v2, 0x1

    :goto_3
    move/from16 v19, v3

    move-object/from16 v20, v4

    move-object/from16 v21, v5

    :goto_4
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-interface/range {p0 .. p0}, Lezo;->a()Ljava/io/InputStream;

    move-result-object v4

    const/4 v5, 0x0

    :try_start_1
    invoke-static {v4, v5, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_5
    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v23, v0

    iget v0, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v24, v0

    mul-int/lit8 v3, v24, 0x2

    move/from16 v0, v23

    if-ne v3, v0, :cond_6

    const/4 v3, 0x1

    move v4, v3

    :goto_6
    const/4 v3, 0x0

    if-nez v2, :cond_b

    if-eqz v4, :cond_7

    const/4 v2, 0x1

    move/from16 v22, v2

    :goto_7
    move/from16 v0, v23

    int-to-double v2, v0

    move/from16 v0, v24

    int-to-double v4, v0

    div-double/2addr v2, v4

    int-to-double v4, v10

    int-to-double v6, v11

    div-double/2addr v4, v6

    if-nez v22, :cond_8

    const-wide v6, 0x3fa999999999999aL    # 0.05

    invoke-static/range {v2 .. v7}, Lezp;->a(DDD)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "PanoMetadata"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Pano metadata does not match file dimensions. Image aspect ratio: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Metadata aspect ratio: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :catch_0
    move-exception v6

    move/from16 v19, v3

    move-object/from16 v20, v4

    move-object/from16 v21, v5

    goto :goto_4

    :catchall_0
    move-exception v2

    if-eqz v4, :cond_5

    :try_start_3
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_5
    :goto_8
    throw v2

    :cond_6
    const/4 v3, 0x0

    move v4, v3

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_8
    if-nez v22, :cond_9

    int-to-double v2, v12

    int-to-double v4, v13

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    invoke-static/range {v2 .. v7}, Lezp;->a(DDD)Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "PanoMetadata"

    const-string v3, "Pano metadata invalid: Full pano dimension not 2:1."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_9
    if-eqz v22, :cond_a

    new-instance v2, Lezp;

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v2, v0, v1}, Lezp;-><init>(II)V

    goto/16 :goto_0

    :cond_a
    new-instance v2, Lezp;

    move/from16 v3, v23

    move/from16 v4, v24

    move-object/from16 v5, v21

    move-object/from16 v6, v20

    move/from16 v7, v19

    invoke-direct/range {v2 .. v18}, Lezp;-><init>(IILjava/util/Calendar;Ljava/util/Calendar;ILjava/lang/String;ZIIIIIIFFF)V

    goto/16 :goto_0

    :catch_1
    move-exception v4

    goto/16 :goto_5

    :catch_2
    move-exception v3

    goto :goto_8

    :cond_b
    move/from16 v22, v3

    goto/16 :goto_7

    :cond_c
    move/from16 v19, v3

    move-object/from16 v20, v4

    move-object/from16 v21, v5

    goto/16 :goto_4
.end method

.method private static a(DDD)Z
    .locals 2

    sub-double v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    cmpg-double v0, v0, p4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lps;Ljava/lang/String;)I
    .locals 1

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lps;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 1

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "http://ns.google.com/photos/1.0/panorama/"

    invoke-interface {p0, v0, p1}, Lps;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
