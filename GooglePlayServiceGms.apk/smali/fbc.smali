.class public Lfbc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Lfbc;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Lfbe;

.field private e:Lfbv;

.field private f:Lfbh;

.field private g:Lfbo;

.field private h:Lffe;

.field private i:Lffh;

.field private j:Lfft;

.field private k:Lfhz;

.field private l:Lfhn;

.field private m:Lfjl;

.field private n:Lfjn;

.field private o:Lfhp;

.field private p:Lfhq;

.field private q:Lfjq;

.field private r:Lfjj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lfbc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfbc;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfbc;->c:Landroid/content/Context;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lfbc;
    .locals 3

    const-class v1, Lfbc;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lfbc;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lfbc;

    if-eqz v2, :cond_0

    check-cast v0, Lfbc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lfbc;->b:Lfbc;

    if-nez v0, :cond_1

    new-instance v0, Lfbc;

    invoke-direct {v0, p0}, Lfbc;-><init>(Landroid/content/Context;)V

    sput-object v0, Lfbc;->b:Lfbc;

    :cond_1
    sget-object v0, Lfbc;->b:Lfbc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Lfbe;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->d:Lfbe;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfbe;

    const-string v2, "gms.people"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lfbe;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lfbc;->d:Lfbe;

    :cond_0
    iget-object v0, p0, Lfbc;->d:Lfbe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Lfbv;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->e:Lfbv;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfbv;

    invoke-direct {v1, v0}, Lfbv;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfbc;->e:Lfbv;

    :cond_0
    iget-object v0, p0, Lfbc;->e:Lfbv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lfbh;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->f:Lfbh;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfbh;

    invoke-direct {v1, v0}, Lfbh;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfbc;->f:Lfbh;

    :cond_0
    iget-object v0, p0, Lfbc;->f:Lfbh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lfbo;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->g:Lfbo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {v0}, Lfbo;->b(Landroid/content/Context;)Lfbo;

    move-result-object v0

    iput-object v0, p0, Lfbc;->g:Lfbo;

    :cond_0
    iget-object v0, p0, Lfbc;->g:Lfbo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Lffe;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->h:Lffe;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {v0}, Lffe;->b(Landroid/content/Context;)Lffe;

    move-result-object v0

    iput-object v0, p0, Lfbc;->h:Lffe;

    :cond_0
    iget-object v0, p0, Lfbc;->h:Lffe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Lffh;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->i:Lffh;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {v0}, Lffh;->b(Landroid/content/Context;)Lffh;

    move-result-object v0

    iput-object v0, p0, Lfbc;->i:Lffh;

    :cond_0
    iget-object v0, p0, Lfbc;->i:Lffh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Lfft;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->j:Lfft;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {v0}, Lfft;->b(Landroid/content/Context;)Lfft;

    move-result-object v0

    iput-object v0, p0, Lfbc;->j:Lfft;

    :cond_0
    iget-object v0, p0, Lfbc;->j:Lfft;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Lfhz;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->k:Lfhz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfhz;

    invoke-direct {v1, v0}, Lfhz;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfbc;->k:Lfhz;

    :cond_0
    iget-object v0, p0, Lfbc;->k:Lfhz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()Lfjl;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->m:Lfjl;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {v0}, Lfjl;->b(Landroid/content/Context;)Lfjl;

    move-result-object v0

    iput-object v0, p0, Lfbc;->m:Lfjl;

    :cond_0
    iget-object v0, p0, Lfbc;->m:Lfjl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final j()Lfjn;
    .locals 3

    iget-object v0, p0, Lfbc;->n:Lfjn;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfjn;

    const-string v2, "images/people-cover-photos"

    invoke-direct {v1, v0, v2}, Lfjn;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lfbc;->n:Lfjn;

    :cond_0
    iget-object v0, p0, Lfbc;->n:Lfjn;

    return-object v0
.end method

.method public final declared-synchronized k()Lfjj;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->r:Lfjj;

    if-nez v0, :cond_0

    new-instance v0, Lfjj;

    new-instance v1, Lfjk;

    invoke-direct {v1}, Lfjk;-><init>()V

    invoke-direct {v0, v1}, Lfjj;-><init>(Lfjk;)V

    iput-object v0, p0, Lfbc;->r:Lfjj;

    :cond_0
    iget-object v0, p0, Lfbc;->r:Lfjj;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()Lfhn;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->l:Lfhn;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    invoke-static {}, Lfhn;->a()Lfhn;

    move-result-object v0

    iput-object v0, p0, Lfbc;->l:Lfhn;

    :cond_0
    iget-object v0, p0, Lfbc;->l:Lfhn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized m()Lfhp;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->o:Lfhp;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfhp;

    invoke-direct {v1, v0}, Lfhp;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfbc;->o:Lfhp;

    :cond_0
    iget-object v0, p0, Lfbc;->o:Lfhp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()Lfhq;
    .locals 3

    iget-object v0, p0, Lfbc;->p:Lfhq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfhq;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v0, v2}, Lfhq;-><init>(Landroid/content/Context;F)V

    iput-object v1, p0, Lfbc;->p:Lfhq;

    :cond_0
    iget-object v0, p0, Lfbc;->p:Lfhq;

    return-object v0
.end method

.method public final declared-synchronized o()Lfjq;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lfbc;->q:Lfjq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfbc;->c:Landroid/content/Context;

    new-instance v1, Lfjq;

    invoke-direct {v1, v0}, Lfjq;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfbc;->q:Lfjq;

    :cond_0
    iget-object v0, p0, Lfbc;->q:Lfjq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
