.class public abstract Lanz;
.super Laoa;
.source "SourceFile"

# interfaces
.implements Lbbr;
.implements Lbbs;
.implements Lfas;
.implements Lfaw;


# instance fields
.field protected n:Lfaj;

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Laoa;-><init>()V

    return-void
.end method

.method protected static a(Ljby;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    invoke-static {p0}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "tx_request"

    invoke-virtual {p0}, Ljby;->d()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "encryption_key_handle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x800000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x40000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method

.method private i()V
    .locals 2

    sget-object v0, Laoi;->a:Ljava/lang/String;

    iget-object v1, p0, Lanz;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Laoe;->a:Ljava/lang/String;

    iget-object v1, p0, Lanz;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lanz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lanz;->a(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lanz;->setResult(I)V

    invoke-virtual {p0}, Lanz;->finish()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final P_()V
    .locals 1

    iget-object v0, p0, Lanz;->n:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    return-void
.end method

.method protected final a(Laoj;Laoj;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lag;->a(Landroid/support/v4/app/Fragment;)Lag;

    invoke-virtual {v0}, Lag;->c()I

    :cond_0
    invoke-virtual {p2}, Laoj;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanz;->t:Ljava/lang/String;

    iget-object v0, p0, Lo;->b:Lw;

    invoke-virtual {v0}, Lu;->a()Lag;

    move-result-object v1

    invoke-virtual {p2}, Laoj;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lu;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1

    const v0, 0x7f0a008d    # com.google.android.gms.R.id.fragments_layout

    invoke-virtual {p2}, Laoj;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, p2, v2}, Lag;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lag;

    :cond_1
    invoke-virtual {v1}, Lag;->f()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lag;->c()I

    :cond_2
    return-void
.end method

.method public final a(Lbbo;)V
    .locals 3

    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load Image due to connection failure : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lbbo;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public final a(Lbbo;Landroid/os/ParcelFileDescriptor;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v8, 0x1

    const/16 v5, 0xc8

    const/4 v7, 0x0

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    invoke-static {p2}, Lfba;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    const v0, 0x7f0a008f    # com.google.android.gms.R.id.profile_image

    invoke-virtual {p0, v0}, Lanz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-nez v4, :cond_2

    const-string v2, "AuthZen"

    const-string v3, "Invalid profile image dimensions: %d x %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v0

    goto :goto_0

    :cond_2
    if-eq v2, v3, :cond_4

    sub-int v0, v2, v4

    div-int/lit8 v0, v0, 0x2

    sub-int v2, v3, v4

    div-int/lit8 v2, v2, 0x2

    invoke-static {v1, v0, v2, v4, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    if-le v4, v5, :cond_3

    invoke-static {v0, v5, v5, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v4, v7, v7, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    invoke-virtual {v2, v7, v7, v7, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    const v5, -0xbdbdbe

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual {v2, v5, v6, v7, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {v2, v0, v4, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Lbbo;Lfed;)V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lfed;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v2

    invoke-interface {v2}, Lfec;->b()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v2}, Lfec;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lanz;->o:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Lfec;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lanz;->n:Lfaj;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, p0, v0, v2, v3}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lanz;->n:Lfaj;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lfaj;->a(Lfaw;Z)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v6, -0x1

    invoke-super {p0, p1}, Laoa;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lanz;->requestWindowFeature(I)Z

    const v0, 0x7f040021    # com.google.android.gms.R.layout.auth_authzen_fragment_layout_activity

    invoke-virtual {p0, v0}, Lanz;->setContentView(I)V

    new-instance v0, Lfaj;

    invoke-virtual {p0}, Lanz;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lfaj;-><init>(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)V

    iput-object v0, p0, Lanz;->n:Lfaj;

    iget-object v0, p0, Lanz;->n:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-gt v0, v1, :cond_0

    const v0, 0x7f0a008a    # com.google.android.gms.R.id.main_workflow_layout

    invoke-virtual {p0, v0}, Lanz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    const v0, 0x7f0a008e    # com.google.android.gms.R.id.profile_layout

    invoke-virtual {p0, v0}, Lanz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v6, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    :cond_0
    const v0, 0x7f0a0090    # com.google.android.gms.R.id.account

    invoke-virtual {p0, v0}, Lanz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lanz;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    const-string v0, "current_fragment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lanz;->t:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Laoa;->onDestroy()V

    iget-object v0, p0, Lanz;->n:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    invoke-direct {p0}, Lanz;->i()V

    return-void
.end method

.method public onResume()V
    .locals 9

    invoke-super {p0}, Laoa;->onResume()V

    const v0, 0x7f0a0091    # com.google.android.gms.R.id.request_description

    invoke-virtual {p0, v0}, Lanz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lanz;->h()I

    move-result v1

    invoke-virtual {p0}, Lanz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0025    # com.google.android.gms.R.plurals.auth_authzen_request_description_no_useragent

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lanz;->p:Ljby;

    iget-object v6, v6, Ljby;->d:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "<b>"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</b>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Laoa;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "current_fragment"

    iget-object v1, p0, Lanz;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "creation_time"

    iget-wide v1, p0, Lanz;->r:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method

.method public onUserLeaveHint()V
    .locals 0

    invoke-super {p0}, Laoa;->onUserLeaveHint()V

    invoke-direct {p0}, Lanz;->i()V

    return-void
.end method
