.class public final Ljdr;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field public d:Ljdh;

.field public e:Ljava/lang/String;

.field public f:Ljdp;

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljdr;->a:J

    iput v2, p0, Ljdr;->b:I

    iput v2, p0, Ljdr;->c:I

    iput-object v3, p0, Ljdr;->d:Ljdh;

    const-string v0, ""

    iput-object v0, p0, Ljdr;->e:Ljava/lang/String;

    iput-object v3, p0, Ljdr;->f:Ljdp;

    iput v2, p0, Ljdr;->g:I

    iput v2, p0, Ljdr;->h:I

    const/4 v0, -0x1

    iput v0, p0, Ljdr;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget-wide v1, p0, Ljdr;->a:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-wide v1, p0, Ljdr;->a:J

    const/4 v1, 0x1

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    :cond_0
    iget v1, p0, Ljdr;->b:I

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Ljdr;->b:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljdr;->d:Ljdh;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdr;->d:Ljdh;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Ljdr;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Ljdr;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljdr;->f:Ljdp;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdr;->f:Ljdp;

    invoke-static {v1, v2}, Lizn;->b(ILizs;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Ljdr;->g:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Ljdr;->g:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Ljdr;->h:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Ljdr;->h:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Ljdr;->c:I

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Ljdr;->c:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Ljdr;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->j()J

    move-result-wide v0

    iput-wide v0, p0, Ljdr;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljdr;->b:I

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Ljdr;->d:Ljdh;

    if-nez v0, :cond_1

    new-instance v0, Ljdh;

    invoke-direct {v0}, Ljdh;-><init>()V

    iput-object v0, p0, Ljdr;->d:Ljdh;

    :cond_1
    iget-object v0, p0, Ljdr;->d:Ljdh;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdr;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Ljdr;->f:Ljdp;

    if-nez v0, :cond_2

    new-instance v0, Ljdp;

    invoke-direct {v0}, Ljdp;-><init>()V

    iput-object v0, p0, Ljdr;->f:Ljdp;

    :cond_2
    iget-object v0, p0, Ljdr;->f:Ljdp;

    invoke-virtual {p1, v0}, Lizm;->a(Lizs;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdr;->g:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Ljdr;->h:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    iput v0, p0, Ljdr;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lizn;)V
    .locals 4

    iget-wide v0, p0, Ljdr;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Ljdr;->a:J

    invoke-virtual {p1, v0, v1}, Lizn;->a(J)V

    :cond_0
    iget v0, p0, Ljdr;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Ljdr;->b:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_1
    iget-object v0, p0, Ljdr;->d:Ljdh;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdr;->d:Ljdh;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_2
    iget-object v0, p0, Ljdr;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Ljdr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Ljdr;->f:Ljdp;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljdr;->f:Ljdp;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILizs;)V

    :cond_4
    iget v0, p0, Ljdr;->g:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Ljdr;->g:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_5
    iget v0, p0, Ljdr;->h:I

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Ljdr;->h:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_6
    iget v0, p0, Ljdr;->c:I

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Ljdr;->c:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_7
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljdr;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljdr;

    iget-wide v2, p0, Ljdr;->a:J

    iget-wide v4, p1, Ljdr;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Ljdr;->b:I

    iget v3, p1, Ljdr;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Ljdr;->c:I

    iget v3, p1, Ljdr;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljdr;->d:Ljdh;

    if-nez v2, :cond_6

    iget-object v2, p1, Ljdr;->d:Ljdh;

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Ljdr;->d:Ljdh;

    iget-object v3, p1, Ljdr;->d:Ljdh;

    invoke-virtual {v2, v3}, Ljdh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljdr;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Ljdr;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljdr;->e:Ljava/lang/String;

    iget-object v3, p1, Ljdr;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljdr;->f:Ljdp;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljdr;->f:Ljdp;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljdr;->f:Ljdp;

    iget-object v3, p1, Ljdr;->f:Ljdp;

    invoke-virtual {v2, v3}, Ljdp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Ljdr;->g:I

    iget v3, p1, Ljdr;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Ljdr;->h:I

    iget v3, p1, Ljdr;->h:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Ljdr;->a:J

    iget-wide v4, p0, Ljdr;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ljdr;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Ljdr;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljdr;->d:Ljdh;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljdr;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljdr;->f:Ljdp;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Ljdr;->g:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Ljdr;->h:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Ljdr;->d:Ljdh;

    invoke-virtual {v0}, Ljdh;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ljdr;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ljdr;->f:Ljdp;

    invoke-virtual {v1}, Ljdp;->hashCode()I

    move-result v1

    goto :goto_2
.end method
