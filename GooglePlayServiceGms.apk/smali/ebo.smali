.class public final Lebo;
.super Lebm;
.source "SourceFile"


# instance fields
.field private Y:I

.field private Z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lebm;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lebo;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lebo;

    invoke-direct {v0}, Lebo;-><init>()V

    iput v3, v0, Lebo;->Y:I

    iput v3, v0, Lebo;->Z:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "isCancelable"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "isIndeterminate"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "max"

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lebo;->g(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    if-eqz p1, :cond_0

    const-string v0, "progress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebo;->Y:I

    const-string v0, "secondaryProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebo;->Z:I

    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    sget v2, Lxf;->aO:I

    invoke-static {v1, v2}, Leee;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setCustomTitle(Landroid/view/View;)V

    const-string v1, "isIndeterminate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    const-string v1, "max"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    iget v1, p0, Lebo;->Y:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    iget v1, p0, Lebo;->Z:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setSecondaryProgress(I)V

    const-string v1, "isCancelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lebo;->b(Z)V

    return-object v2
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "progress"

    iget v1, p0, Lebo;->Y:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "secondaryProgress"

    iget v1, p0, Lebo;->Z:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
