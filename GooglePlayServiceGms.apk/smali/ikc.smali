.class public final Likc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Liif;


# direct methods
.method public constructor <init>(Liif;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Likc;->a:Liif;

    return-void
.end method

.method private static a(IF)I
    .locals 2

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_0
    mul-int/lit8 v1, p0, 0x1f

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(IJ)I
    .locals 3

    mul-int/lit8 v0, p0, 0x1f

    const/16 v1, 0x20

    ushr-long v1, p1, v1

    xor-long/2addr v1, p1

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static wrap(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 1
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    new-instance v0, Likd;

    invoke-direct {v0}, Likd;-><init>()V

    invoke-static {p0, v0}, Lisb;->a(Ljava/lang/Iterable;Lirc;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v0, 0x0

    instance-of v1, p1, Likc;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Likc;

    iget-object v1, p1, Likc;->a:Liif;

    iget-object v2, p0, Likc;->a:Liif;

    iget-object v2, v2, Liif;->b:Liig;

    iget-object v3, v1, Liif;->b:Liig;

    iget-object v4, p0, Likc;->a:Liif;

    iget-wide v4, v4, Liif;->f:J

    iget-wide v6, v1, Liif;->f:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iget v4, v2, Liig;->b:I

    iget v5, v3, Liig;->b:I

    if-ne v4, v5, :cond_0

    iget v2, v2, Liig;->d:I

    iget v3, v3, Liig;->d:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->d:I

    iget v3, v1, Liif;->d:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->h:F

    iget v3, v1, Liif;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->j:F

    iget v3, v1, Liif;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget-wide v2, v2, Liif;->l:D

    iget-wide v4, v1, Liif;->l:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->n:F

    iget v3, v1, Liif;->n:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->o:I

    iget v3, v1, Liif;->o:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->q:I

    iget v3, v1, Liif;->q:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget-boolean v2, v2, Liif;->s:Z

    iget-boolean v3, v1, Liif;->s:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget-boolean v2, v2, Liif;->u:Z

    iget-boolean v3, v1, Liif;->u:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget-object v2, v2, Liif;->w:Ljava/lang/String;

    iget-object v3, v1, Liif;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->y:I

    iget v1, v1, Liif;->y:I

    if-ne v2, v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Likc;->a:Liif;

    iget-wide v0, v0, Liif;->f:J

    invoke-static {v3, v0, v1}, Likc;->a(IJ)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget-object v1, v1, Liif;->b:Liig;

    iget v1, v1, Liig;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget-object v1, v1, Liif;->b:Liig;

    iget v1, v1, Liig;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->d:I

    add-int/2addr v0, v1

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->h:F

    invoke-static {v0, v1}, Likc;->a(IF)I

    move-result v0

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->j:F

    invoke-static {v0, v1}, Likc;->a(IF)I

    move-result v4

    iget-object v0, p0, Likc;->a:Liif;

    iget-wide v0, v0, Liif;->l:D

    const-wide/16 v5, 0x0

    cmpl-double v5, v0, v5

    if-eqz v5, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    :goto_0
    invoke-static {v4, v0, v1}, Likc;->a(IJ)I

    move-result v0

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->n:F

    invoke-static {v0, v1}, Likc;->a(IF)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->o:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->q:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Likc;->a:Liif;

    iget-boolean v0, v0, Liif;->s:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget-boolean v1, v1, Liif;->u:Z

    if-eqz v1, :cond_2

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget-object v1, v1, Liif;->w:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Likc;->a:Liif;

    iget v1, v1, Liif;->y:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Likc;->a:Liif;

    iget-object v0, v0, Liif;->b:Liig;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "time: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Likc;->a:Liif;

    iget-wide v3, v3, Liif;->f:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", latE7: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Liig;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", lngE7: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Liig;->d:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", source:  "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", speed: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->h:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", heading: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->j:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", altitude: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget-wide v2, v2, Liif;->l:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", accuracy: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->n:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ", gmmNlpVersion: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->o:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", batteryLevel: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->q:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", batteryCharging: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget-boolean v2, v2, Liif;->s:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", stationary: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget-boolean v2, v2, Liif;->u:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", levelId: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget-object v2, v2, Liif;->w:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", levenNumberE3: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Likc;->a:Liif;

    iget v2, v2, Liif;->y:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "}"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
