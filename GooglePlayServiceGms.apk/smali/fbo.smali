.class public final Lfbo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Landroid/content/Context;

.field private final c:Lbpe;

.field private final d:Lfbp;

.field private final e:Lfbf;

.field private f:Lfbn;

.field private g:Lfbn;

.field private final h:Lfbq;

.field private final i:Z

.field private final j:Lfbk;

.field private volatile k:Ljava/util/concurrent/CountDownLatch;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "value"

    aput-object v2, v0, v1

    sput-object v0, Lfbo;->l:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lfbo;->a:Ljava/lang/Object;

    new-instance v0, Lfbf;

    invoke-direct {v0}, Lfbf;-><init>()V

    iput-object v0, p0, Lfbo;->e:Lfbf;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lfbo;->k:Ljava/util/concurrent/CountDownLatch;

    iput v3, p0, Lfbo;->m:I

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lfbo;->b:Landroid/content/Context;

    new-instance v0, Lfbp;

    iget-object v1, p0, Lfbo;->b:Landroid/content/Context;

    const-string v2, "pluscontacts.db"

    invoke-direct {v0, p0, v1, v2}, Lfbp;-><init>(Lfbo;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lfbo;->d:Lfbp;

    iput-boolean v3, p0, Lfbo;->i:Z

    new-instance v0, Lfbk;

    invoke-direct {v0, p0}, Lfbk;-><init>(Lfbo;)V

    iput-object v0, p0, Lfbo;->j:Lfbk;

    new-instance v0, Lfbq;

    invoke-direct {v0, p0, p1}, Lfbq;-><init>(Lfbo;Landroid/content/Context;)V

    iput-object v0, p0, Lfbo;->h:Lfbq;

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lfdl;->a(Landroid/content/Context;)Lbpe;

    move-result-object v0

    iput-object v0, p0, Lfbo;->c:Lbpe;

    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 2

    :try_start_0
    const-string v0, "SELECT last_sync_status FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    invoke-static {p1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic a(Lfbo;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lfbo;
    .locals 1

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->d()Lfbo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM sync_tokens;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-static {p0}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v0

    invoke-virtual {v0}, Lfbc;->h()Lfhz;

    move-result-object v0

    invoke-virtual {v0}, Lfhz;->a()V

    return-void
.end method

.method static a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM people WHERE in_contacts=0 AND NOT EXISTS (SELECT * FROM circle_members AS cm WHERE cm.owner_id = people.owner_id AND cm.qualified_id = people.qualified_id)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 12

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "Fixing sync"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lfgy;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v0}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    const-string v0, "com.android.contacts"

    invoke-static {v4, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    const-string v0, "com.google.android.gms.people"

    invoke-static {v4, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v7

    const-string v0, "PeopleDatabaseHelper"

    const-string v8, "Last status=%d  people sync=%s  focus sync=%s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v0, v4, v8}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v0, Lfbd;->V:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-ne v5, v0, :cond_0

    if-eqz v7, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Last=failure"

    invoke-static {p1, v0, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.people"

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-static {v5, v8}, Lfhz;->a(ZZ)Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v4, v0, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    sget-object v0, Lfbd;->U:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    if-nez v7, :cond_1

    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Re-enabling"

    invoke-static {p1, v0, v4, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.people"

    const/4 v5, 0x1

    invoke-static {v4, v0, v5}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "value"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "properties"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method static synthetic a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V
    .locals 0

    invoke-static {p0, p1}, Lfbo;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    return-void
.end method

.method static synthetic a(Lfbo;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    iget-object v0, p0, Lfbo;->d:Lfbp;

    invoke-static {p1}, Lfbp;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v0, p0, Lfbo;->d:Lfbp;

    invoke-virtual {v0, p1}, Lfbp;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v0, p0, Lfbo;->d:Lfbp;

    invoke-static {p1}, Lfbp;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public static a()Z
    .locals 3

    const-string v0, "1"

    const-string v1, "gms.people.read_only"

    const-string v2, ""

    invoke-static {v1, v2}, Lfjw;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "gms.people.read_only is set.  Some features are disabled."

    invoke-static {v1, v2}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Context;)Lfbo;
    .locals 3

    new-instance v0, Lfbo;

    invoke-direct {v0, p0}, Lfbo;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lfbo;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lfbo;->d()Lfbn;

    move-result-object v1

    iget-object v1, v1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p0, v1}, Lfbo;->b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z

    const-string v1, "gcoreVersion"

    invoke-static {p0}, Lbox;->e(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;)V

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    const-string v0, "pluscontacts.db"

    return-object v0
.end method

.method static b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    const-string v0, "SELECT sql FROM sqlite_master WHERE name=\'owners\' AND type=\'table\'"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cover_photo_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ALTER TABLE owners ADD COLUMN cover_photo_url TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN  cover_photo_height INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN  cover_photo_width INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN cover_photo_id TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V
    .locals 2

    const-string v0, "dbLocale"

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 8

    const/4 v1, 0x0

    const-string v0, "PeopleDatabaseHelper"

    const-string v2, "addNewAccounts"

    invoke-static {v0, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lfgy;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v2, "SELECT DISTINCT account_name FROM owners"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    const/4 v3, -0x1

    :try_start_1
    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_0
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    const-string v5, "account_name"

    invoke-virtual {v3, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "owners"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    add-int/lit8 v2, v2, 0x1

    const-string v5, "PeopleDatabaseHelper"

    const/4 v6, 0x0

    const-string v7, "Account added"

    invoke-static {p0, v5, v0, v6, v7}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PeopleDatabaseHelper"

    const-string v6, "Account added"

    const/4 v7, 0x0

    invoke-static {v5, v6, v0, v7}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "addNewAccounts: done"

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-lez v2, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "circleId"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "qualifiedId"

    invoke-static {p4, v0}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM circle_members WHERE owner_id=? AND circle_id=? AND qualified_id=?"

    invoke-static {v0, p3, p4}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE owners ADD COLUMN last_full_people_sync_time INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "UPDATE owners SET gaia_id=(SELECT gaia_id FROM owners AS i WHERE owners.account_name = i.account_name ) WHERE page_gaia_id IS NOT NULL"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DROP TABLE gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE gaia_id_map (owner_id INTEGER NOT NULL,contact_id TEXT NOT NULL,value TEXT NOT NULL,gaia_id TEXT NOT NULL,UNIQUE (owner_id, contact_id, value),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string p3, ""

    move v0, v1

    :goto_0
    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p3, :cond_0

    move v3, v1

    :goto_1
    invoke-static {v3}, Lbkm;->a(Z)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v4

    invoke-virtual {v4}, Lfbn;->b()V

    :try_start_0
    iget-object v3, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v3, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "UPDATE people SET in_circle=(  CASE WHEN EXISTS( SELECT 1 FROM circle_members cm WHERE cm.qualified_id=people.qualified_id AND cm.owner_id=people.owner_id ) THEN 1 ELSE 0 END)  WHERE (?2 OR qualified_id=?3) AND owner_id=?1"

    if-eqz v0, :cond_1

    const-string v3, "1"

    :goto_2
    invoke-static {v5, v3, p3}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v4}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4}, Lfbn;->e()V

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    if-eqz p3, :cond_2

    :goto_3
    invoke-static {v1}, Lbkm;->a(Z)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V

    :try_start_1
    iget-object v2, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v2, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "DELETE FROM people WHERE in_circle=0  AND in_contacts=0 AND (?2 OR qualified_id=?3) AND owner_id=?1"

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_4
    invoke-static {v2, v0, p3}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v1}, Lfbn;->e()V

    return-void

    :cond_0
    move v3, v2

    goto :goto_1

    :cond_1
    :try_start_2
    const-string v3, "0"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lfbn;->e()V

    throw v0

    :cond_2
    move v1, v2

    goto :goto_3

    :cond_3
    :try_start_3
    const-string v0, "0"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_4

    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method static synthetic f(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_circles;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,UNIQUE (package_name));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id, qualified_id),FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,everyone INTEGER NOT NULL,UNIQUE (owner_id, dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "circleId"

    invoke-static {p3, v0}, Lbkm;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM circles WHERE owner_id=? AND circle_id=?"

    invoke-static {v0, p3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DROP TABLE IF EXISTS applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS application_packages;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_applications;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE IF EXISTS facl_people;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE applications (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id),FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE application_packages (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,package_name TEXT NOT NULL,certificate_hash TEXT NOT NULL,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE facl_people (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,dev_console_id TEXT NOT NULL,qualified_id TEXT NOT NULL,UNIQUE (owner_id, dev_console_id, qualified_id) ON CONFLICT IGNORE,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE,FOREIGN KEY (owner_id, dev_console_id) REFERENCES applications(owner_id, dev_console_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE people ADD COLUMN invisible_3p INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic i(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'people\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'gaiamap\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic j(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE owners ADD COLUMN is_dasher INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN dasher_domain TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_viewer_domain INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic k(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DROP TABLE IF EXISTS postal_address;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,qualified_id TEXT NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id, qualified_id) REFERENCES people(owner_id, qualified_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX IF NOT EXISTS postal_address_person ON postal_address (owner_id, qualified_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic l(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM people WHERE name IS NULL;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic m(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE people ADD COLUMN name_verified INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic n(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE owners ADD COLUMN etag TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_circle INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE people SET in_circle=1;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE people ADD COLUMN in_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_emails (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,email TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_phones (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,phone TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE owner_postal_address (_id INTEGER PRIMARY KEY AUTOINCREMENT,owner_id INTEGER NOT NULL,postal_address TEXT NOT NULL,type INTEGER NOT NULL,custom_label TEXT,FOREIGN KEY (owner_id) REFERENCES owners(_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic o(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE owner_sync_requests (_id INTEGER PRIMARY KEY AUTOINCREMENT,account_name TEXT NOT NULL,page_gaia_id TEXT,sync_requested_time INTEGER NOT NULL DEFAULT 0,UNIQUE (account_name, page_gaia_id));"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic p(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE owners ADD COLUMN sync_circles_to_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE owners ADD COLUMN sync_evergreen_to_contacts INTEGER NOT NULL DEFAULT 0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lfbo;->j:Lfbk;

    iget-object v1, v0, Lfbk;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, v0, Lfbk;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    invoke-virtual {v0}, Lffh;->b()V

    return-void
.end method

.method static synthetic q(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE from people WHERE in_circle=0 AND in_contacts=0;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic r(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "ALTER TABLE gaia_id_map ADD COLUMN type INTEGER;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE gaia_id_map SET type = (CASE WHEN value LIKE \'%@%\' then 1 ELSE 2 END);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE new_table ( owner_id INTEGER NOT NULL, contact_id TEXT NOT NULL, value TEXT NOT NULL, gaia_id TEXT NOT NULL, type INTEGER NOT NULL, UNIQUE (owner_id, contact_id, value), FOREIGN KEY (owner_id) REFERENCES owners (_id) ON DELETE CASCADE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO new_table SELECT * FROM gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE new_table RENAME TO gaia_id_map;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DELETE FROM sync_tokens WHERE name = \'gaiamap\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private r()Z
    .locals 13

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "removeStaleOwners"

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lfgy;->a(Landroid/content/Context;)Ljava/util/Set;

    move-result-object v0

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iget-object v6, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v6}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v6

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    const-string v7, "SELECT _id,account_name,page_gaia_id FROM owners"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    const/4 v8, -0x1

    :try_start_1
    invoke-interface {v7, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    invoke-interface {v7, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    invoke-interface {v0, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "PeopleService"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v5, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :cond_1
    :try_start_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lfbo;->b:Landroid/content/Context;

    const-string v8, "PeopleDatabaseHelper"

    const/4 v9, 0x0

    const-string v10, "Account removed"

    invoke-static {v7, v8, v0, v9, v10}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "PeopleDatabaseHelper"

    const-string v8, "Account removed"

    const/4 v9, 0x0

    invoke-static {v7, v8, v0, v9}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v7, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v7}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v7

    invoke-virtual {v7}, Lfbc;->a()Lfbe;

    move-result-object v7

    invoke-virtual {v7, v0}, Lfbe;->c(Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v0, v7, v8}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lfdl;->c()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v10, "Removing owner: "

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v10, "PeopleDatabaseHelper"

    invoke-static {v10, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, p0, Lfbo;->b:Landroid/content/Context;

    const-string v11, "PeopleDatabaseHelper"

    const/4 v12, 0x0

    invoke-static {v10, v11, v0, v12}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v0

    const-string v0, "DELETE FROM owners WHERE _id=?"

    invoke-virtual {v3, v0, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lfbn;->c()Z

    goto :goto_2

    :cond_3
    const-string v0, "contactsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {p0, v0, v5}, Lfbo;->b(Ljava/lang/String;Z)V

    :cond_4
    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v3}, Lfbn;->e()V

    invoke-virtual {v6}, Lffh;->b()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "PeopleDatabaseHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeStaleOwners: done: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return v0

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method static synthetic s(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "DELETE FROM owners WHERE account_name=\'\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method private s()Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanupEvergreenPeople"

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lfbd;->v:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    const-string v0, "people"

    const-string v4, "in_circle=0"

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3}, Lfbn;->e()V

    if-lez v0, :cond_1

    const-string v3, "contactsCleanupPending"

    invoke-virtual {p0, v3, v2}, Lfbo;->b(Ljava/lang/String;Z)V

    :cond_1
    const-string v3, "PeopleService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "PeopleDatabaseHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cleanupEvergreenPeople: evergreen people deleted: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-lez v0, :cond_3

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private t()Z
    .locals 12

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanUpNonGplusAccounts"

    invoke-static {v0, v3}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    const-string v0, "SELECT DISTINCT account_name FROM owners WHERE has_people!=0 AND page_gaia_id IS NULL"

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    const/4 v0, -0x1

    :try_start_1
    invoke-interface {v5, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfbo;->j:Lfbk;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lfbk;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    iget-object v6, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v6, v0}, Lfgy;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :cond_1
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "PeopleDatabaseHelper"

    const-string v5, "Detected G+ -> non-G+ owner(s).  Cleanining up..."

    invoke-static {v0, v5}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lfdl;->c()[Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Scrubbing account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "PeopleDatabaseHelper"

    invoke-static {v9, v8}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v9, p0, Lfbo;->b:Landroid/content/Context;

    const-string v10, "PeopleDatabaseHelper"

    const/4 v11, 0x0

    invoke-static {v9, v10, v0, v11, v8}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v8, 0x0

    aput-object v0, v5, v8

    invoke-virtual {v3}, Lfbn;->c()Z

    const-string v8, "DELETE FROM sync_tokens WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL)  AND name != \'gaiamap\'"

    invoke-virtual {v3, v8, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lfbn;->c()Z

    const-string v8, "DELETE FROM people WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL) "

    invoke-virtual {v3, v8, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lfbn;->c()Z

    const-string v8, "DELETE FROM circles WHERE owner_id IN(SELECT _id FROM owners WHERE account_name=?  AND page_gaia_id IS NULL) "

    invoke-virtual {v3, v8, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v3}, Lfbn;->c()Z

    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    const-string v8, "page_gaia_id"

    invoke-virtual {v6, v8}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v8, "sync_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "sync_circles_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "sync_evergreen_to_contacts"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "has_people"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "owners"

    const-string v9, "account_name=? AND page_gaia_id IS NULL"

    invoke-virtual {v3, v8, v6, v9, v5}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v8, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v8}, Lfbc;->a(Landroid/content/Context;)Lfbc;

    move-result-object v8

    invoke-virtual {v8}, Lfbc;->h()Lfhz;

    move-result-object v8

    invoke-virtual {v8, v0}, Lfhz;->c(Ljava/lang/String;)V

    invoke-virtual {v3}, Lfbn;->c()Z

    goto/16 :goto_1

    :cond_2
    const-string v0, "contactsCleanupPending"

    const/4 v5, 0x1

    invoke-virtual {p0, v0, v5}, Lfbo;->b(Ljava/lang/String;Z)V

    :cond_3
    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v3}, Lfbn;->e()V

    const-string v0, "PeopleDatabaseHelper"

    const-string v3, "cleanUpNonGplusAccounts done."

    invoke-static {v0, v3}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    return v0

    :cond_4
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v2, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lfbo;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "circle_id"

    invoke-virtual {v3, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "name"

    invoke-virtual {v3, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "type"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sort_key"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "p"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "for_sharing"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v2

    const-string v4, "circles"

    invoke-virtual {v2, v4, v3}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, p1, p2, v2}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lffh;->b()V

    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)I
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_0

    if-nez p5, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v2, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v4

    invoke-virtual {v4}, Lfbn;->b()V

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    if-eqz p4, :cond_1

    const-string v2, "name"

    invoke-virtual {v5, v2, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p5, :cond_2

    const-string v6, "for_sharing"

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    const-string v2, "circles"

    const-string v6, "owner_id=? AND circle_id=? AND type=-1"

    invoke-static {v3, p3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v5, v6, v3}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v3}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v3

    const/4 v5, 0x2

    invoke-virtual {v3, p1, p2, v5}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v4}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v2, :cond_4

    :goto_2
    invoke-virtual {v4}, Lfbn;->e()V

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lfbn;->e()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 11

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lfbo;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v4

    invoke-virtual {v4}, Lfbn;->b()V

    :try_start_0
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "owner_id"

    invoke-virtual {v5, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_id"

    invoke-virtual {v5, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v1

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-direct {p0, p1, p2, p3, v0}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "qualified_id"

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "circle_members"

    invoke-virtual {v4, v7, v5}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-direct {p0, p1, p2, v0}, Lfbo;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lfbn;->e()V

    throw v0

    :cond_1
    :try_start_1
    const-string v7, "PeopleDatabaseHelper"

    const-string v8, "Person %s is already a a member of circle %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    const/4 v0, 0x1

    aput-object p3, v9, v0

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v3, "PeopleDatabaseHelper"

    const-string v7, "Failed to add non-existant person %s to circle %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    aput-object p3, v8, v0

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lfdk;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1, p2}, Lfbo;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    const/4 v5, 0x6

    invoke-virtual {v0, p1, p2, v5}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v4}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v4}, Lfbn;->e()V

    if-eqz v3, :cond_4

    move v0, v1

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)I
    .locals 8

    const/4 v1, 0x1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "qualifiedId"

    invoke-static {p3, v0}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    const-string v0, "owner_id"

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "qualified_id"

    invoke-virtual {v5, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0}, Lfbo;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, p1, p2, v0, p3}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "circle_id"

    invoke-virtual {v5, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "circle_members"

    invoke-virtual {v3, v0, v5}, Lfbn;->b(Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, p3}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "circle_members"

    const-string v7, "owner_id=? AND circle_id=? AND qualified_id=?"

    invoke-static {v4, v0, p3}, Lfdl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v6, v7, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lfbo;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_2
    invoke-virtual {p0, p1, p2}, Lfbo;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v2}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v2

    const/4 v4, 0x6

    invoke-virtual {v2, p1, p2, v4}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v3}, Lfbn;->e()V

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_3
    return v0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-static {}, Lfdl;->c()[Ljava/lang/String;

    move-result-object v0

    aput-object p1, v0, v1

    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v1

    const-string v2, "properties"

    sget-object v3, Lfbo;->l:[Ljava/lang/String;

    const-string v4, "name=?"

    invoke-virtual {v1, v2, v3, v4, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method

.method final a(Lfbn;)V
    .locals 2

    const-string v0, "DROP TABLE IF EXISTS search_index;"

    invoke-virtual {p1}, Lfbn;->a()V

    iget-object v1, p1, Lfbn;->a:Lfbf;

    iget-object v1, v1, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v1, p1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p1, Lfbn;->a:Lfbf;

    iget-object v0, v0, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    iget-object v0, p0, Lfbo;->d:Lfbp;

    iget-object v0, p1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lfbp;->d(Landroid/database/sqlite/SQLiteDatabase;)V

    iget-object v0, p0, Lfbo;->d:Lfbp;

    iget-object v0, p1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lfbp;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p1, Lfbn;->a:Lfbf;

    iget-object v1, v1, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lfbo;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "people"

    const-string v3, "qualified_id=? AND owner_id=?"

    invoke-static {p3, v0}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lfbn;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0, p1, p2}, Lfbo;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, p1, p2, v2}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v1}, Lfbn;->e()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Failed to remove a person"

    invoke-static {v1, v2, v0}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v0

    iget-object v0, v0, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v0}, Lfbo;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lfbo;->j:Lfbk;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v2

    const-string v3, "SELECT has_people FROM owners WHERE _id=?"

    invoke-static {v1}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "qualifiedId"

    invoke-static {p3, v0}, Lfdl;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v1

    const-string v2, "SELECT count(1) FROM people WHERE owner_id=? AND qualified_id=?"

    invoke-static {v0, p3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->b(Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lfbn;
    .locals 7

    iget-object v6, p0, Lfbo;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lfbo;->f:Lfbn;

    if-nez v0, :cond_0

    new-instance v0, Lfbn;

    iget-object v1, p0, Lfbo;->b:Landroid/content/Context;

    iget-object v3, p0, Lfbo;->e:Lfbf;

    iget-object v2, p0, Lfbo;->d:Lfbp;

    invoke-virtual {v2}, Lfbp;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lfbn;-><init>(Landroid/content/Context;Lfbo;Lfbf;Landroid/database/sqlite/SQLiteDatabase;Z)V

    iput-object v0, p0, Lfbo;->f:Lfbn;

    :cond_0
    iget-object v0, p0, Lfbo;->f:Lfbn;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V

    :try_start_0
    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "UPDATE circles SET people_count=( SELECT count(1) FROM circle_members AS m WHERE m.owner_id=circles.owner_id AND m.circle_id=circles.circle_id), last_modified=?2 WHERE owner_id=?1 AND type=-1"

    iget-object v3, p0, Lfbo;->c:Lbpe;

    invoke-interface {v3}, Lbpe;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lfbn;->e()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v0, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V

    :try_start_0
    const-string v2, "DELETE FROM circles WHERE owner_id=? AND circle_id=? AND type=-1"

    invoke-static {v0, p3}, Lfdl;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfbo;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, p1, p2, v2}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lfbn;->e()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
.end method

.method public final d()Lfbn;
    .locals 7

    iget-object v6, p0, Lfbo;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iget-object v0, p0, Lfbo;->g:Lfbn;

    if-nez v0, :cond_0

    new-instance v0, Lfbn;

    iget-object v1, p0, Lfbo;->b:Landroid/content/Context;

    iget-object v3, p0, Lfbo;->e:Lfbf;

    iget-object v2, p0, Lfbo;->d:Lfbp;

    invoke-virtual {v2}, Lfbp;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lfbn;-><init>(Landroid/content/Context;Lfbo;Lfbf;Landroid/database/sqlite/SQLiteDatabase;Z)V

    iput-object v0, p0, Lfbo;->g:Lfbn;

    :cond_0
    iget-object v0, p0, Lfbo;->g:Lfbn;

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lfbo;->c()Lfbn;

    move-result-object v1

    const-string v2, "SELECT 1 FROM owners WHERE _id=? AND sync_evergreen_to_contacts !=0 AND sync_to_contacts !=0"

    iget-object v3, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v3, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v5}, Lfbn;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    const/4 v0, 0x1

    invoke-static {p1}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "avatar"

    invoke-static {p3}, Lfjp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "etag"

    invoke-static {}, Lfdl;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_0
    iget-object v4, p0, Lfbo;->j:Lfbk;

    invoke-virtual {v4, p1, p2}, Lfbk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lfdl;->j(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string v5, "owners"

    const-string v6, "_id=?"

    invoke-virtual {v3, v5, v2, v6, v4}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v1, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v1}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p2, v2}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {v3}, Lfbn;->e()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final e()Lfbk;
    .locals 1

    iget-object v0, p0, Lfbo;->j:Lfbk;

    return-object v0
.end method

.method public final f()V
    .locals 3

    iget-object v0, p0, Lfbo;->k:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Opening sync latch"

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfbo;->k:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method public final g()V
    .locals 3

    iget-object v0, p0, Lfbo;->k:Ljava/util/concurrent/CountDownLatch;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "Waiting for sync latch..."

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    goto :goto_0
.end method

.method public final h()Lfbq;
    .locals 1

    iget-object v0, p0, Lfbo;->h:Lfbq;

    return-object v0
.end method

.method public final i()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "dbLocale"

    const-string v4, ""

    invoke-virtual {p0, v3, v4}, Lfbo;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string v4, "Updating DB locale: %s -> %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v0

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "PeopleDatabaseHelper"

    invoke-static {v3, v0}, Lfdk;->c(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfbo;->b:Landroid/content/Context;

    const-string v4, "PeopleDatabaseHelper"

    const/4 v5, 0x0

    invoke-static {v3, v4, v0, v5}, Lfbu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lfbo;->h:Lfbq;

    invoke-virtual {v0, v2}, Lfbq;->a(Ljava/util/Locale;)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v3

    iget-object v0, v3, Lfbn;->a:Lfbf;

    iget-object v0, v0, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, v3, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v3, Lfbn;->a:Lfbf;

    iget-object v0, v0, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    invoke-virtual {v3}, Lfbn;->b()V

    :try_start_1
    iget-object v0, p0, Lfbo;->h:Lfbq;

    invoke-virtual {v0}, Lfbq;->a()V

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->c(Landroid/content/Context;)V

    iget-object v0, v3, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, v2}, Lfbo;->b(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    invoke-virtual {v3}, Lfbn;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3}, Lfbn;->e()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, v3, Lfbn;->a:Lfbf;

    iget-object v1, v1, Lfbf;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Lfbn;->e()V

    throw v0
.end method

.method final j()V
    .locals 2

    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onBeginTransaction"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lfbo;->m:I

    return-void
.end method

.method final k()V
    .locals 2

    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onYieldTransaction"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lfbo;->m:I

    invoke-direct {p0}, Lfbo;->q()V

    return-void
.end method

.method final l()V
    .locals 3

    const/4 v2, 0x3

    const-string v0, "PeopleTx"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onCommitTransaction"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput v2, p0, Lfbo;->m:I

    invoke-direct {p0}, Lfbo;->q()V

    return-void
.end method

.method final m()V
    .locals 2

    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleDatabaseHelper"

    const-string v1, "onRollbackTransaction"

    invoke-static {v0, v1}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x4

    iput v0, p0, Lfbo;->m:I

    invoke-direct {p0}, Lfbo;->q()V

    return-void
.end method

.method public final n()Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lfbo;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    iget-object v1, v1, Lfbn;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, v1}, Lfbo;->b(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfbo;->b:Landroid/content/Context;

    invoke-static {v1}, Lffh;->a(Landroid/content/Context;)Lffh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v3, v3, v2}, Lffh;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1}, Lffh;->b()V

    :cond_0
    return v0
.end method

.method public final o()Z
    .locals 2

    invoke-direct {p0}, Lfbo;->r()Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    invoke-direct {p0}, Lfbo;->s()Z

    move-result v1

    or-int/2addr v0, v1

    invoke-direct {p0}, Lfbo;->t()Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public final p()Z
    .locals 6

    const/4 v0, 0x0

    const-string v1, "PeopleDatabaseHelper"

    const-string v2, "clearSyncToContactsFlagsForAllOwners"

    invoke-static {v1, v2}, Lfdk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lfbo;->d()Lfbn;

    move-result-object v1

    invoke-virtual {v1}, Lfbn;->b()V

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "sync_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "sync_circles_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "sync_evergreen_to_contacts"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "owners"

    const-string v4, "page_gaia_id IS NULL AND sync_to_contacts!=0"

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v2, v4, v5}, Lfbn;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1}, Lfbn;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lfbn;->e()V

    if-lez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfbn;->e()V

    throw v0
.end method
