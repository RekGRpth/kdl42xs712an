.class public final Lcrz;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcrz;->b:Landroid/content/Context;

    invoke-direct {p0, p2}, Lcrz;->a(Lcom/google/android/gms/feedback/ErrorReport;)V

    return-void
.end method

.method private a(D)Ljava/lang/String;
    .locals 12

    const/4 v11, 0x3

    const v4, 0x15180

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    if-le v2, v4, :cond_5

    div-int v0, v2, v4

    mul-int v3, v0, v4

    sub-int/2addr v2, v3

    move v4, v0

    :goto_0
    const/16 v0, 0xe10

    if-le v2, v0, :cond_4

    div-int/lit16 v0, v2, 0xe10

    mul-int/lit16 v3, v0, 0xe10

    sub-int/2addr v2, v3

    move v3, v0

    :goto_1
    const/16 v0, 0x3c

    if-le v2, v0, :cond_3

    div-int/lit8 v0, v2, 0x3c

    mul-int/lit8 v6, v0, 0x3c

    sub-int/2addr v2, v6

    :goto_2
    if-lez v4, :cond_0

    iget-object v6, p0, Lcrz;->b:Landroid/content/Context;

    const v7, 0x7f0b00dc    # com.google.android.gms.R.string.gf_battery_history_days

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v11

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    if-lez v3, :cond_1

    iget-object v4, p0, Lcrz;->b:Landroid/content/Context;

    const v6, 0x7f0b00dd    # com.google.android.gms.R.string.gf_battery_history_hours

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_1
    if-lez v0, :cond_2

    iget-object v3, p0, Lcrz;->b:Landroid/content/Context;

    const v4, 0x7f0b00de    # com.google.android.gms.R.string.gf_battery_history_minutes

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcrz;->b:Landroid/content/Context;

    const v3, 0x7f0b00df    # com.google.android.gms.R.string.gf_battery_history_seconds

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v3, v1

    goto/16 :goto_1

    :cond_5
    move v4, v1

    goto/16 :goto_0
.end method

.method private static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x0

    const-class v1, Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    :try_start_0
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :cond_0
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(ILjava/lang/Object;I)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method private a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V
    .locals 8

    iget-object v7, p0, Lcrz;->a:Ljava/util/List;

    new-instance v0, Lcsa;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-object v4, p4

    move v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcsa;-><init>(Lcrz;Ljava/lang/Integer;Ljava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v3, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcrz;->a:Ljava/util/List;

    const v0, 0x7f0b00ba    # com.google.android.gms.R.string.gf_error_report_type

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v1, v1, Landroid/app/ApplicationErrorReport;->type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    const v1, 0x7f0b00c8    # com.google.android.gms.R.string.gf_error_report_description

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    const v1, 0x7f0b00bb    # com.google.android.gms.R.string.gf_error_report_package_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0b00b8    # com.google.android.gms.R.string.gf_error_report_package_version

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;

    const v1, 0x7f0b00b9    # com.google.android.gms.R.string.gf_error_report_package_version_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    const v1, 0x7f0b00bc    # com.google.android.gms.R.string.gf_error_report_installer_package_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;

    const v1, 0x7f0b00bd    # com.google.android.gms.R.string.gf_error_report_process_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v0, 0x7f0b00be    # com.google.android.gms.R.string.gf_error_report_time

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-wide v1, v1, Landroid/app/ApplicationErrorReport;->time:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-boolean v0, v0, Landroid/app/ApplicationErrorReport;->systemApp:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const v1, 0x7f0b00d5    # com.google.android.gms.R.string.gf_error_report_system_app

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    const v1, 0x7f0b00ad    # com.google.android.gms.R.string.gf_locale

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v0, 0x7f0b00b7    # com.google.android.gms.R.string.gf_error_report_system

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    const v1, 0x7f0b00c9    # com.google.android.gms.R.string.gf_error_report_device

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    const v1, 0x7f0b00ca    # com.google.android.gms.R.string.gf_error_report_build_id

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    const v1, 0x7f0b00cb    # com.google.android.gms.R.string.gf_error_report_build_type

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    const v1, 0x7f0b00e3    # com.google.android.gms.R.string.gf_error_report_build_fingerprint

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    const v1, 0x7f0b00cc    # com.google.android.gms.R.string.gf_error_report_model

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    const v1, 0x7f0b00cd    # com.google.android.gms.R.string.gf_error_report_product

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0b00ce    # com.google.android.gms.R.string.gf_error_report_sdk_version

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    const v1, 0x7f0b00cf    # com.google.android.gms.R.string.gf_error_report_release

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    const v1, 0x7f0b00d0    # com.google.android.gms.R.string.gf_error_report_incremental

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    const v1, 0x7f0b00d1    # com.google.android.gms.R.string.gf_error_report_codename

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    const v1, 0x7f0b00d2    # com.google.android.gms.R.string.gf_error_report_board

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    const v1, 0x7f0b00d3    # com.google.android.gms.R.string.gf_error_report_brand

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    const v1, 0x7f0b00ab    # com.google.android.gms.R.string.gf_user_account

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_2

    const v1, 0x7f0b00d4    # com.google.android.gms.R.string.gf_error_report_running_apps

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "running applications"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    if-eqz v0, :cond_3

    const v1, 0x7f0b00a5    # com.google.android.gms.R.string.gf_system_log

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "system logs"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    if-eqz v0, :cond_4

    const v1, 0x7f0b00a6    # com.google.android.gms.R.string.gf_event_log

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "event logs"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    if-eqz v0, :cond_5

    const v0, 0x7f0b00b4    # com.google.android.gms.R.string.gf_error_report_crash

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    const v1, 0x7f0b00bf    # com.google.android.gms.R.string.gf_error_report_exception_class_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    const v1, 0x7f0b00c0    # com.google.android.gms.R.string.gf_error_report_exception_throw_file_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    const v1, 0x7f0b00c1    # com.google.android.gms.R.string.gf_error_report_exception_throw_class_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    const v1, 0x7f0b00c2    # com.google.android.gms.R.string.gf_error_report_exception_throw_method_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0b00c3    # com.google.android.gms.R.string.gf_error_report_exception_throw_line_number

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v1, 0x7f0b00c4    # com.google.android.gms.R.string.gf_error_report_exception_stack_trace

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "stack trace"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    if-eqz v0, :cond_7

    const v0, 0x7f0b00b6    # com.google.android.gms.R.string.gf_error_report_anr

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    const v1, 0x7f0b00c5    # com.google.android.gms.R.string.gf_error_report_anr_activity

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->cause:Ljava/lang/String;

    const v1, 0x7f0b00c6    # com.google.android.gms.R.string.gf_error_report_anr_cause

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v1, 0x7f0b00c7    # com.google.android.gms.R.string.gf_error_report_anr_info

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "anr info"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    if-eqz v0, :cond_7

    const v1, 0x7f0b00d6    # com.google.android.gms.R.string.gf_error_report_anr_stack_traces

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "anr stack trace"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    if-eqz v0, :cond_8

    const v0, 0x7f0b00d7    # com.google.android.gms.R.string.gf_error_report_battery

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget v0, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->usagePercent:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f0b00d8    # com.google.android.gms.R.string.gf_error_report_battery_usage_percent

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v0, 0x7f0b00d9    # com.google.android.gms.R.string.gf_error_report_battery_usage_duration

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-wide v1, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->durationMicros:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    const v1, 0x7f0b00da    # com.google.android.gms.R.string.gf_error_report_battery_usage_details

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "battery usage details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    const v1, 0x7f0b00db    # com.google.android.gms.R.string.gf_error_report_battery_checkin_details

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "battery checkin details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_8
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    if-eqz v0, :cond_9

    const v0, 0x7f0b00e0    # com.google.android.gms.R.string.gf_error_report_running_service

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    const v0, 0x7f0b00e1    # com.google.android.gms.R.string.gf_error_report_running_service_duration

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-wide v1, v1, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    const v1, 0x7f0b00e2    # com.google.android.gms.R.string.gf_error_report_running_service_details

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "running service details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcrz;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    :cond_9
    const v0, 0x7f0b00af    # com.google.android.gms.R.string.gf_network_data

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    const v1, 0x7f0b00b0    # com.google.android.gms.R.string.gf_network_name

    invoke-direct {p0, v1, v0, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    :cond_a
    const v0, 0x7f0b00b1    # com.google.android.gms.R.string.gf_phone_type

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    const v0, 0x7f0b00b2    # com.google.android.gms.R.string.gf_network_type

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    invoke-direct {p0, v0, v1, v2}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_b

    const v0, 0x7f0b0093    # com.google.android.gms.R.string.gf_product_specific_data

    invoke-direct {p0, v0, v6, v3}, Lcrz;->a(ILjava/lang/Object;I)V

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcrz;->a:Ljava/util/List;

    new-instance v4, Lcsa;

    invoke-direct {v4, p0, v0, v2}, Lcsa;-><init>(Lcrz;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_b
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcrz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsa;

    invoke-virtual {v0}, Lcsa;->a()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcrz;->b:Landroid/content/Context;

    iget-object v3, v0, Lcsa;->b:Ljava/lang/Class;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "feedback.FIELD_NAME"

    iget-object v3, v0, Lcsa;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "feedback.FIELD_VALUE"

    iget-object v0, v0, Lcsa;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcrz;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcrz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcrz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcrz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsa;

    iget-object v1, p0, Lcrz;->b:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iget-object v4, v0, Lcsa;->e:Ljava/lang/Object;

    if-nez v4, :cond_1

    move v4, v2

    :goto_0
    if-eqz v4, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v2

    const v4, 0x7f0a02dd    # com.google.android.gms.R.id.section_header_row

    if-eq v2, v4, :cond_b

    :cond_0
    const v2, 0x7f040117    # com.google.android.gms.R.layout.section_header_row

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_1
    move-object v1, v2

    check-cast v1, Landroid/widget/TextView;

    iget-object v0, v0, Lcsa;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    return-object v2

    :cond_1
    move v4, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcsa;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0a0111    # com.google.android.gms.R.id.expandable_row

    if-eq v4, v5, :cond_4

    :cond_3
    const v4, 0x7f040054    # com.google.android.gms.R.layout.expandable_row

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_4
    :goto_3
    iget v1, v0, Lcsa;->c:I

    const/4 v4, 0x7

    if-ne v1, v4, :cond_9

    move v1, v2

    :goto_4
    if-nez v1, :cond_5

    const v1, 0x7f0a0112    # com.google.android.gms.R.id.label

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, v0, Lcsa;->a:Ljava/lang/Integer;

    if-nez v2, :cond_a

    iget-object v2, v0, Lcsa;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_5
    iget-object v1, v0, Lcsa;->b:Ljava/lang/Class;

    if-nez v1, :cond_6

    const v1, 0x7f0a020c    # com.google.android.gms.R.id.value

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, v0, Lcsa;->c:I

    packed-switch v2, :pswitch_data_0

    :cond_6
    :goto_6
    move-object v2, p2

    goto :goto_2

    :cond_7
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0a020b    # com.google.android.gms.R.id.label_value_row

    if-eq v4, v5, :cond_4

    :cond_8
    const v4, 0x7f0400a2    # com.google.android.gms.R.layout.label_value_row

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_3

    :cond_9
    move v1, v3

    goto :goto_4

    :cond_a
    iget-object v2, v0, Lcsa;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    :pswitch_0
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    :pswitch_1
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v3}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    :pswitch_2
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_3
    goto :goto_6

    :pswitch_4
    const v0, 0x7f0b00b4    # com.google.android.gms.R.string.gf_error_report_crash

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_5
    const v0, 0x7f0b00b6    # com.google.android.gms.R.string.gf_error_report_anr

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_6
    const v0, 0x7f0b00d7    # com.google.android.gms.R.string.gf_error_report_battery

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_7
    const v0, 0x7f0b00e0    # com.google.android.gms.R.string.gf_error_report_running_service

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_8
    const v0, 0x7f0b00b5    # com.google.android.gms.R.string.gf_error_report_user_initiated

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    :pswitch_9
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    invoke-direct {p0, v2, v3}, Lcrz;->a(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :pswitch_a
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-direct {p0, v2, v3}, Lcrz;->a(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :pswitch_b
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "NETWORK_TYPE_"

    invoke-static {v0, v2}, Lcrz;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :pswitch_c
    iget-object v0, v0, Lcsa;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "PHONE_TYPE_"

    invoke-static {v0, v2}, Lcrz;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_b
    move-object v2, p2

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcrz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsa;

    invoke-virtual {v0}, Lcsa;->a()Z

    move-result v0

    return v0
.end method
