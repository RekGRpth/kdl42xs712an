.class public final Ldtw;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldtw;->b:Ldad;

    iput-object p3, p0, Ldtw;->c:Ljava/lang/String;

    iput p4, p0, Ldtw;->d:I

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    iget-object v0, p0, Ldtw;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->E(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    iget-object v2, p0, Ldtw;->a:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLandroid/content/SyncResult;)I

    move-result v5

    :try_start_0
    iget-object v2, p0, Ldtw;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldtw;->c:Ljava/lang/String;

    iget v4, p0, Ldtw;->d:I

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcun;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lamq; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "LoadRequestSummariesOperation"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Ldac;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
