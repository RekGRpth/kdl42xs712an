.class public final Lbwx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbwz;


# instance fields
.field private final a:Lcfz;

.field private final b:Lbvr;

.field private final c:I

.field private final d:Z

.field private final e:I

.field private final f:Lcfc;

.field private final g:Lbsp;

.field private final h:Lbvu;

.field private final i:Z

.field private j:Lbvj;

.field private k:I


# direct methods
.method private constructor <init>(Lcoy;Lbvu;Lcfc;IZILbvr;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lbwx;->k:I

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvu;

    iput-object v0, p0, Lbwx;->h:Lbvu;

    invoke-static {p3}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfc;

    iput-object v0, p0, Lbwx;->f:Lcfc;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbwx;->a:Lcfz;

    invoke-static {p7}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvr;

    iput-object v0, p0, Lbwx;->b:Lbvr;

    iput p4, p0, Lbwx;->c:I

    iput-boolean p5, p0, Lbwx;->d:Z

    iput p6, p0, Lbwx;->e:I

    invoke-static {p3}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iput-object v0, p0, Lbwx;->g:Lbsp;

    iget-object v0, p0, Lbwx;->a:Lcfz;

    iget-object v1, p0, Lbwx;->f:Lcfc;

    invoke-static {v0, v1}, Lbwx;->a(Lcfz;Lcfc;)Lbvj;

    move-result-object v0

    iput-object v0, p0, Lbwx;->j:Lbvj;

    iput-boolean p8, p0, Lbwx;->i:Z

    return-void
.end method

.method public constructor <init>(Lcoy;Lbvu;Lcfc;IZIZ)V
    .locals 9

    new-instance v7, Lbvr;

    invoke-direct {v7, p1}, Lbvr;-><init>(Lcoy;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lbwx;-><init>(Lcoy;Lbvu;Lcfc;IZILbvr;Z)V

    return-void
.end method

.method public static a(Lcfz;Lcfc;)Lbvj;
    .locals 6

    const-wide v4, 0x7fffffffffffffffL

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lbux;->b:Lbux;

    invoke-interface {p0, p1, v0}, Lcfz;->a(Lcfc;Lbux;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    iget-object v0, v0, Lbuw;->a:Lbuv;

    check-cast v0, Lbvc;

    iget-boolean v0, v0, Lbvc;->a:Z

    if-eqz v0, :cond_1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgb;

    :goto_1
    new-instance v2, Lbvj;

    invoke-direct {v2, v1, v0}, Lbvj;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgb;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcgb;

    goto :goto_1

    :cond_2
    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lcfz;->a(Lcfc;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v0

    :goto_2
    new-instance v3, Lbvc;

    invoke-direct {v3, v2, v0}, Lbvc;-><init>(ZLjava/util/Set;)V

    invoke-interface {p0, p1, v3, v4, v5}, Lcfz;->a(Lcfc;Lbuv;J)Lcgb;

    move-result-object v2

    new-instance v3, Lbvc;

    invoke-direct {v3, v1, v0}, Lbvc;-><init>(ZLjava/util/Set;)V

    invoke-interface {p0, p1, v3, v4, v5}, Lcfz;->a(Lcfc;Lbuv;J)Lcgb;

    move-result-object v0

    move-object v1, v2

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_2
.end method

.method private a(Landroid/content/SyncResult;)V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    :try_start_0
    iget-object v0, p0, Lbwx;->a:Lcfz;

    iget-object v2, p0, Lbwx;->f:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v3

    iget-object v0, p0, Lbwx;->b:Lbvr;

    iget-object v2, p0, Lbwx;->g:Lbsp;

    invoke-virtual {v3}, Lcfe;->e()J

    move-result-wide v4

    iget-object v0, v0, Lbvr;->a:Lcfz;

    invoke-interface {v0, v2, v4, v5}, Lcfz;->a(Lbsp;J)I

    move-result v0

    if-eqz p1, :cond_0

    iget-object v2, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numDeletes:J

    :cond_0
    iget v0, p0, Lbwx;->c:I

    const v2, 0x7fffffff

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->c:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    new-instance v2, Ljava/util/Date;

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->c:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    :goto_1
    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->d:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_3

    new-instance v1, Ljava/util/Date;

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->d:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    move-object v0, v1

    :goto_2
    invoke-virtual {v3, v2}, Lcfe;->c(Ljava/util/Date;)V

    invoke-virtual {v3, v0}, Lcfe;->b(Ljava/util/Date;)V

    :goto_3
    invoke-virtual {v3}, Lcfe;->k()V

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v3, v0}, Lcfe;->c(Ljava/util/Date;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcfe;->b(Ljava/util/Date;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbwx;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method

.method private a(Lbvt;Lcgb;Ljava/lang/String;Lbvs;)V
    .locals 6

    iget-object v0, p2, Lcgb;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    iget-object v2, p0, Lbwx;->h:Lbvu;

    invoke-static {p4, v0, v1}, Lbvu;->a(Lbvs;J)Lbwi;

    move-result-object v0

    new-instance v1, Lbwy;

    invoke-direct {v1, p2, v0}, Lbwy;-><init>(Lcgb;Lbwi;)V

    iget v0, p0, Lbwx;->c:I

    int-to-long v2, v0

    iget-object v0, p2, Lcgb;->a:Lbuw;

    iget-wide v4, v0, Lbuw;->b:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    if-lez v0, :cond_0

    iget-object v2, p2, Lcgb;->a:Lbuw;

    invoke-virtual {v2}, Lbuw;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p2, Lcgb;->a:Lbuw;

    invoke-virtual {p1, v2, p3, v0, v1}, Lbvt;->a(Lbuw;Ljava/lang/String;ILbvs;)V

    iget v0, p0, Lbwx;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbwx;->k:I

    :cond_0
    return-void

    :cond_1
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 6

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lbwx;->a(Landroid/content/SyncResult;)V

    sget-object v0, Lbth;->a:Lbth;

    invoke-static {v0}, Lbti;->a(Lbth;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->c:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    iget-object v0, v0, Lbuw;->a:Lbuv;

    check-cast v0, Lbvc;

    iget-object v0, v0, Lbvc;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, Lbwx;->a:Lcfz;

    iget-object v3, p0, Lbwx;->f:Lcfc;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Lcfz;->a(Lcfc;J)Lbsp;

    move-result-object v0

    iget-object v2, p0, Lbwx;->a:Lcfz;

    invoke-interface {v2, v0}, Lcfz;->a(Lbsp;)Lcfg;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcfg;->c:Z

    invoke-virtual {v0}, Lcfg;->k()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lbvt;Landroid/content/SyncResult;)V
    .locals 6

    const-wide/16 v4, 0x0

    iget-object v0, p0, Lbwx;->h:Lbvu;

    iget-object v1, p0, Lbwx;->f:Lcfc;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-boolean v3, p0, Lbwx;->i:Z

    invoke-virtual {v0, p2, v1, v2, v3}, Lbvu;->a(Landroid/content/SyncResult;Lcfc;Ljava/lang/Boolean;Z)Lbvs;

    move-result-object v1

    iget-object v0, p0, Lbwx;->f:Lcfc;

    iget-object v2, v0, Lcfc;->a:Ljava/lang/String;

    iget-boolean v0, p0, Lbwx;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwx;->a:Lcfz;

    iget-object v3, p0, Lbwx;->f:Lcfc;

    invoke-interface {v0, v3}, Lcfz;->c(Lcfc;)I

    iget-object v0, p0, Lbwx;->a:Lcfz;

    iget-object v3, p0, Lbwx;->f:Lcfc;

    invoke-static {v0, v3}, Lbwx;->a(Lcfz;Lcfc;)Lbvj;

    move-result-object v0

    iput-object v0, p0, Lbwx;->j:Lbvj;

    :cond_0
    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->c:Ljava/lang/Object;

    check-cast v0, Lcgb;

    invoke-direct {p0, p1, v0, v2, v1}, Lbwx;->a(Lbvt;Lcgb;Ljava/lang/String;Lbvs;)V

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->d:Ljava/lang/Object;

    check-cast v0, Lcgb;

    invoke-direct {p0, p1, v0, v2, v1}, Lbwx;->a(Lbvt;Lcgb;Ljava/lang/String;Lbvs;)V

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->c:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    iget-wide v0, v0, Lbuw;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lbwx;->j:Lbvj;

    iget-object v0, v0, Lbvj;->d:Ljava/lang/Object;

    check-cast v0, Lcgb;

    iget-object v0, v0, Lcgb;->a:Lbuw;

    iget-wide v0, v0, Lbuw;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lbwx;->d:Z

    if-eqz v0, :cond_3

    :cond_2
    new-instance v0, Ljava/util/Date;

    const-wide v1, 0x7fffffffffffffffL

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iget-object v1, p0, Lbwx;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->c()V

    :try_start_0
    iget-object v1, p0, Lbwx;->a:Lcfz;

    iget-object v2, p0, Lbwx;->f:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcfz;->b(Ljava/lang/String;)Lcfe;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcfe;->c(Ljava/util/Date;)V

    invoke-virtual {v1, v0}, Lcfe;->b(Ljava/util/Date;)V

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcfe;->b(J)V

    invoke-virtual {v1}, Lcfe;->b()V

    iget v0, p0, Lbwx;->e:I

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lcfe;->c(J)V

    invoke-virtual {v1}, Lcfe;->k()V

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lbwx;->a:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    :cond_3
    iget v0, p0, Lbwx;->k:I

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbwx;->a(Landroid/content/SyncResult;)V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbwx;->a:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0
.end method
