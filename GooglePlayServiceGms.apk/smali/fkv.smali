.class public final Lfkv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lbfy;

.field public static b:Lbfy;

.field public static c:Lbfy;

.field public static d:Lbfy;

.field public static e:Lbfy;

.field public static f:Lbfy;

.field public static g:Lbfy;

.field public static h:Lbfy;

.field public static i:Lbfy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x0

    const-string v0, "gms:playlog:uploader:debug"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->a:Lbfy;

    const-string v0, "gms:playlog:uploader:debug_ignore_response"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->b:Lbfy;

    const-string v0, "gms:playlog:uploader:server_url"

    const-string v1, "https://play.googleapis.com/log"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->c:Lbfy;

    const-string v0, "gms:playlog:uploader:auth_token_service"

    const-string v1, "androidmarket"

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/String;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->d:Lbfy;

    const-string v0, "gms:playlog:uploader:dynamic_delay"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Z)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->e:Lbfy;

    const-string v0, "gms:playlog:uploader:delay_between_uploads_millis"

    const-wide/32 v1, 0x493e0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->f:Lbfy;

    const-string v0, "gms:playlog:uploader:min_delay_between_uploads_millis"

    const-wide/32 v1, 0xea60

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->g:Lbfy;

    const-string v0, "gms:playlog:uploader:min_delay_between_uploads_on_mobile_network_millis"

    const-wide/32 v1, 0x36ee80

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Long;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->h:Lbfy;

    const-string v0, "gms:playlog:uploader:max_redirects"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lbfy;->a(Ljava/lang/String;Ljava/lang/Integer;)Lbfy;

    move-result-object v0

    sput-object v0, Lfkv;->i:Lbfy;

    return-void
.end method
