.class public final Leqf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbq;


# instance fields
.field public final a:Lera;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lbbr;Lbbs;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lera;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lera;-><init>(Landroid/content/Context;Lbbr;Lbbs;Ljava/lang/String;)V

    iput-object v0, p0, Leqf;->a:Lera;

    return-void
.end method

.method public static a(Landroid/content/Intent;Ljava/util/ArrayList;)V
    .locals 3

    if-eqz p1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/ParcelableGeofence;

    invoke-virtual {v0}, Lcom/google/android/gms/location/internal/ParcelableGeofence;->k()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v0, "com.google.android.location.intent.extra.geofence_list"

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0}, Lera;->a()V

    return-void
.end method

.method public final a(Lbbr;)V
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0, p1}, Lera;->a(Lbbr;)V

    return-void
.end method

.method public final a(Lbbs;)V
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0, p1}, Lera;->a(Lbbs;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0}, Lera;->b()V

    return-void
.end method

.method public final b(Lbbr;)Z
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0, p1}, Lera;->b(Lbbr;)Z

    move-result v0

    return v0
.end method

.method public final c(Lbbr;)V
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0, p1}, Lera;->c(Lbbr;)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0}, Lera;->d()Z

    move-result v0

    return v0
.end method

.method public final d_()Z
    .locals 1

    iget-object v0, p0, Leqf;->a:Lera;

    invoke-virtual {v0}, Lera;->d_()Z

    move-result v0

    return v0
.end method
