.class final Lzl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lzj;

.field final synthetic b:Lzk;


# direct methods
.method constructor <init>(Lzk;Lzj;)V
    .locals 0

    iput-object p1, p0, Lzl;->b:Lzk;

    iput-object p2, p0, Lzl;->a:Lzj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    iget-object v0, p0, Lzl;->b:Lzk;

    iget-object v13, v0, Lzk;->e:Ljava/lang/Object;

    monitor-enter v13

    :try_start_0
    iget-object v0, p0, Lzl;->b:Lzk;

    iget v0, v0, Lzk;->h:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    monitor-exit v13

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lzl;->b:Lzk;

    iget-object v1, p0, Lzl;->b:Lzk;

    invoke-virtual {v1}, Lzk;->a()Lzs;

    move-result-object v1

    iput-object v1, v0, Lzk;->g:Lzs;

    iget-object v0, p0, Lzl;->b:Lzk;

    iget-object v0, v0, Lzk;->g:Lzs;

    if-nez v0, :cond_1

    iget-object v0, p0, Lzl;->b:Lzk;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lzk;->a(I)V

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v13

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lzl;->a:Lzj;

    iget-object v1, p0, Lzl;->b:Lzk;

    invoke-virtual {v0, v1}, Lzj;->a(Lzn;)V

    iget-object v14, p0, Lzl;->b:Lzk;

    iget-object v5, p0, Lzl;->a:Lzj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, v14, Lzk;->f:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->d:I

    const v1, 0x3e8fa0

    if-ge v0, v1, :cond_3

    iget-object v0, v14, Lzk;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, v14, Lzk;->g:Lzs;

    iget-object v1, v14, Lzk;->d:Landroid/content/Context;

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v1

    iget-object v2, v14, Lzk;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v3, v14, Lzk;->a:Lzg;

    iget-object v3, v3, Lzg;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v5}, Lzs;->a(Lcrv;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Lzv;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v0, v14, Lzk;->g:Lzs;

    iget-object v1, v14, Lzk;->d:Landroid/content/Context;

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v1

    iget-object v2, v14, Lzk;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v3, v14, Lzk;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v4, v14, Lzk;->a:Lzg;

    iget-object v4, v4, Lzg;->f:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lzs;->a(Lcrv;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Lzv;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "Could not request ad from mediation adapter."

    invoke-static {v1, v0}, Lacj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x5

    invoke-virtual {v14, v0}, Lzk;->a(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_3
    :try_start_6
    iget-object v0, v14, Lzk;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, v14, Lzk;->g:Lzs;

    iget-object v1, v14, Lzk;->d:Landroid/content/Context;

    invoke-static {v1}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v1

    iget-object v2, v14, Lzk;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v3, v14, Lzk;->a:Lzg;

    iget-object v3, v3, Lzg;->f:Ljava/lang/String;

    iget-object v4, v14, Lzk;->a:Lzg;

    iget-object v4, v4, Lzg;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lzs;->a(Lcrv;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Ljava/lang/String;Lzv;)V

    goto :goto_1

    :cond_4
    iget-object v6, v14, Lzk;->g:Lzs;

    iget-object v0, v14, Lzk;->d:Landroid/content/Context;

    invoke-static {v0}, Lcry;->a(Ljava/lang/Object;)Lcrv;

    move-result-object v7

    iget-object v8, v14, Lzk;->c:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v9, v14, Lzk;->b:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v14, Lzk;->a:Lzg;

    iget-object v10, v0, Lzg;->f:Ljava/lang/String;

    iget-object v0, v14, Lzk;->a:Lzg;

    iget-object v11, v0, Lzg;->a:Ljava/lang/String;

    move-object v12, v5

    invoke-interface/range {v6 .. v12}, Lzs;->a(Lcrv;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;Ljava/lang/String;Lzv;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1
.end method
