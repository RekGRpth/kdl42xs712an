.class public final Ldsd;
.super Ldrw;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Z

.field private final d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;ZLandroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Ldrw;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldsd;->b:Ldad;

    iput-boolean p3, p0, Ldsd;->c:Z

    iput-object p4, p0, Ldsd;->d:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    iget-object v0, p0, Ldsd;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->b(I)V

    return-void
.end method

.method protected final b(Landroid/content/Context;Lcun;)I
    .locals 3

    iget-object v0, p0, Ldsd;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-boolean v1, p0, Ldsd;->c:Z

    iget-object v2, p0, Ldsd;->d:Landroid/os/Bundle;

    invoke-virtual {p2, v0, v1, v2}, Lcun;->a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I

    move-result v0

    return v0
.end method
