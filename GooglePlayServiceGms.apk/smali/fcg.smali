.class public final Lfcg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lfcg;


# instance fields
.field private b:[Ljava/util/regex/Pattern;

.field private c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lfcg;

    invoke-direct {v0}, Lfcg;-><init>()V

    sput-object v0, Lfcg;->a:Lfcg;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Ljava/util/regex/Pattern;

    iput-object v0, p0, Lfcg;->b:[Ljava/util/regex/Pattern;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lfcg;->c:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/os/Bundle;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    const-string v0, "config.url_uncompress.patterns"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string v0, "config.url_uncompress.replacements"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    :cond_0
    return-object p1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lfcg;->b:[Ljava/util/regex/Pattern;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lfcg;->b:[Ljava/util/regex/Pattern;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    iget-object v2, p0, Lfcg;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    const/4 v1, 0x0

    const-string v0, "config.url_uncompress.patterns"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-string v0, "config.url_uncompress.replacements"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v0, v2

    array-length v4, v3

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    array-length v0, v2

    new-array v0, v0, [Ljava/util/regex/Pattern;

    iput-object v0, p0, Lfcg;->b:[Ljava/util/regex/Pattern;

    iput-object v3, p0, Lfcg;->c:[Ljava/lang/String;

    :goto_1
    array-length v0, v2

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lfcg;->b:[Ljava/util/regex/Pattern;

    aget-object v3, v2, v1

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    aput-object v3, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    return-void
.end method
