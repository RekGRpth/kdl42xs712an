.class public final Lfpn;
.super Lfpp;
.source "SourceFile"

# interfaces
.implements Lal;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lfpi;
.implements Lfpm;
.implements Lfps;


# instance fields
.field private Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private Z:Ljava/lang/String;

.field private aa:Ljava/lang/CharSequence;

.field private ab:Ljava/lang/String;

.field private ac:Ljava/lang/String;

.field private ad:Landroid/widget/TextView;

.field private final ae:Ljava/util/ArrayList;

.field private af:Lfpk;

.field private ag:Lfpj;

.field private ah:Lbce;

.field private ai:Lfpq;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lfpp;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpn;->ae:Ljava/util/ArrayList;

    return-void
.end method

.method private L()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {v0}, Lfph;->c()V

    :cond_0
    iget-object v0, p0, Lfpn;->i:Lfob;

    iget-object v0, v0, Lfob;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lfpn;->ab:Ljava/lang/String;

    iput-object v2, p0, Lfpn;->ac:Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lah;->a(ZZ)V

    invoke-virtual {p0}, Lfpn;->s()Lak;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, v2, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    :cond_1
    return-void
.end method

.method private M()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {v0}, Lfph;->d()V

    :cond_0
    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v2}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v2}, Lfot;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfpn;->a(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-super {p0, v1, v1}, Lah;->a(ZZ)V

    return-void

    :cond_1
    const v0, 0x7f0b0387    # com.google.android.gms.R.string.plus_list_moments_error

    invoke-virtual {p0, v0}, Lfpn;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfpn;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/lang/String;)Lfpn;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p0, :cond_1

    const-string v1, "app_filter"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :goto_0
    new-instance v1, Lfpn;

    invoke-direct {v1}, Lfpn;-><init>()V

    invoke-virtual {v1, v0}, Lfpn;->g(Landroid/os/Bundle;)V

    return-object v1

    :cond_1
    if-eqz p1, :cond_0

    const-string v1, "collection_filter"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v0, p0, Lfpn;->i:Lfob;

    iget-object v0, v0, Lfob;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iget-object v4, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lfxh;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v1

    iput-object v1, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-interface {v0}, Lfxh;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_1

    move-object p1, v2

    :cond_1
    iput-object p1, p0, Lfpn;->ad:Landroid/widget/TextView;

    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lfpn;->Z:Ljava/lang/String;

    if-eqz v0, :cond_3

    iput-object p1, p0, Lfpn;->ad:Landroid/widget/TextView;

    goto :goto_1

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iput-object v2, p0, Lfpn;->ad:Landroid/widget/TextView;

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final J()I
    .locals 1

    const v0, 0x7f0b0375    # com.google.android.gms.R.string.plus_app_settings_activity_log_page_label

    return v0
.end method

.method protected final K()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    sget-object v0, Lbck;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final N_()V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lfpn;->ag:Lfpj;

    invoke-virtual {v0, p1}, Lfpj;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lfpn;->af:Lfpk;

    invoke-virtual {v1, p1}, Lfpk;->a(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final a(ILandroid/os/Bundle;)Lcb;
    .locals 7

    const/16 v0, 0xc8

    if-ne p1, v0, :cond_1

    new-instance v0, Lfpo;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v2, p0, Lfpn;->i:Lfob;

    iget-object v2, v2, Lfob;->a:Landroid/accounts/Account;

    iget-object v3, p0, Lfpn;->Z:Ljava/lang/String;

    iget-object v4, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v4

    :goto_0
    sget-object v5, Lfsr;->F:Lbfy;

    invoke-virtual {v5}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lfpn;->ac:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lfpo;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/Menu;)V
    .locals 4

    invoke-super {p0, p1}, Lfpp;->a(Landroid/view/Menu;)V

    const/4 v0, 0x0

    const/16 v1, 0xc8

    const/16 v2, 0x64

    const v3, 0x7f0b0473    # com.google.android.gms.R.string.common_list_apps_menu_help

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return-void
.end method

.method public final synthetic a(Lcb;Ljava/lang/Object;)V
    .locals 6

    const/16 v5, 0xc8

    const/4 v2, 0x0

    const/4 v1, 0x1

    check-cast p2, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    iget v0, p1, Lcb;->m:I

    if-ne v0, v5, :cond_2

    invoke-super {p0, v1, v1}, Lah;->a(ZZ)V

    check-cast p1, Lfpo;

    invoke-virtual {p1}, Lfpo;->b()Lbbo;

    move-result-object v3

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    if-nez v0, :cond_0

    new-instance v0, Lfph;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-direct {v0, v4, p0}, Lfph;-><init>(Landroid/content/Context;Lfpi;)V

    invoke-virtual {p0, v0}, Lfpn;->a(Landroid/widget/ListAdapter;)V

    :cond_0
    if-eqz p2, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lbbo;->b()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-virtual {p0}, Lfpn;->s()Lak;

    move-result-object v0

    invoke-virtual {v0, v5}, Lak;->a(I)V

    invoke-direct {p0}, Lfpn;->M()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    :cond_4
    iget-object v0, p0, Lfpn;->aa:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lfpn;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpn;->ac:Ljava/lang/String;

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->d()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lfpn;->ac:Ljava/lang/String;

    if-eqz v4, :cond_5

    :goto_1
    invoke-virtual {v0, v3, v1}, Lfph;->a(Ljava/util/List;Z)V

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method

.method public final a(Lfpr;)V
    .locals 9

    const/4 v4, 0x0

    iget-object v0, p1, Lfpr;->a:Ljava/lang/String;

    iget-object v1, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v1, p1, Lfpr;->b:Ljava/lang/String;

    iget-object v2, p1, Lfpr;->c:Ljava/lang/String;

    iget-object v3, p1, Lfpr;->a:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;B)V

    iput-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lfpn;->a(Landroid/widget/TextView;)V

    :cond_0
    return-void
.end method

.method public final a(Lgkf;)V
    .locals 3

    invoke-interface {p1}, Lgkf;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lgkf;->j()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0, v1, p0}, Lfoy;->a(Ljava/lang/String;Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lbcj;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    iget-object v1, p0, Lfpn;->i:Lfob;

    sget-object v2, Lbck;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v0, v2}, Lfob;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lbcj;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lfpn;->ag:Lfpj;

    invoke-virtual {v0, p1, p2}, Lfpj;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {v0}, Lfph;->a()V

    :cond_0
    return-void
.end method

.method public final a(Ljj;)V
    .locals 6

    const v5, 0x7f0b0375    # com.google.android.gms.R.string.plus_app_settings_activity_log_page_label

    const/4 v4, 0x0

    const v0, 0x7f020207    # com.google.android.gms.R.drawable.plus_icon_red_32

    invoke-virtual {p1, v0}, Ljj;->b(I)V

    iget-object v0, p0, Lfpn;->i:Lfob;

    iget-object v0, v0, Lfob;->a:Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpn;->Z:Ljava/lang/String;

    if-eqz v0, :cond_3

    :cond_0
    const v0, 0x7f040105    # com.google.android.gms.R.layout.plus_settings_action_bar_title_sub_title

    invoke-virtual {p1, v0}, Ljj;->a(I)V

    invoke-virtual {p1}, Ljj;->a()Landroid/view/View;

    move-result-object v2

    const v0, 0x7f0a005e    # com.google.android.gms.R.id.title

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0a02bb    # com.google.android.gms.R.id.sub_title

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lfpn;->a(Landroid/widget/TextView;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lfpn;->ab:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lfpn;->L()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfob;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lfpn;->ah:Lbce;

    if-nez v0, :cond_4

    new-instance v0, Lbce;

    invoke-virtual {p1}, Ljj;->d()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3, v2}, Lbce;-><init>(Landroid/content/Context;[Ljava/lang/Object;)V

    iput-object v0, p0, Lfpn;->ah:Lbce;

    iget-object v0, p0, Lfpn;->ah:Lbce;

    invoke-virtual {v0, v5}, Lbce;->a(I)V

    :cond_4
    const v0, 0x7f040104    # com.google.android.gms.R.layout.plus_settings_action_bar_spinner

    invoke-virtual {p1, v0}, Ljj;->a(I)V

    invoke-virtual {p1}, Ljj;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lfpn;->ah:Lbce;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    iget-object v3, p0, Lfpn;->ah:Lbce;

    invoke-virtual {v3, v1}, Lbce;->getPosition(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setClickable(Z)V

    goto :goto_0
.end method

.method public final a_(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lfpp;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lfsr;->C:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v1, v0}, Lbox;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-class v2, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lfpn;->a(Landroid/content/Intent;I)V

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lfpn;->a(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lfpn;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {v0}, Lfph;->b()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpn;->ad:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lfpn;->a(Landroid/widget/TextView;)V

    :cond_0
    return-void
.end method

.method public final d()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lfpn;->ae:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lfpp;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lfpn;->P()V

    invoke-virtual {p0}, Lfpn;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    if-eqz p1, :cond_3

    const-string v0, "moment_list_app_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "moment_list_app_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "collection_filter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfpn;->Z:Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfpk;->a(Landroid/content/Context;)Lfpk;

    move-result-object v0

    iput-object v0, p0, Lfpn;->af:Lfpk;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfpj;->a(Landroid/content/Context;)Lfpj;

    move-result-object v0

    iput-object v0, p0, Lfpn;->ag:Lfpj;

    iget-object v0, p0, Lfpn;->af:Lfpk;

    invoke-virtual {v0, p0}, Lfpk;->a(Lfpm;)V

    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpn;->Z:Ljava/lang/String;

    if-eqz v0, :cond_4

    :cond_0
    const v0, 0x7f0b0371    # com.google.android.gms.R.string.plus_list_moments_filter_empty_message

    :goto_1
    invoke-virtual {p0, v0}, Lfpn;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lfpn;->aa:Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    const-string v0, "moment_list_deleted_moments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfpn;->ae:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lfpn;->ae:Ljava/util/ArrayList;

    const-string v1, "moment_list_deleted_moments"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-static {}, Lfot;->a()Lfot;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lfot;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lfpn;->M()V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "app_filter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    goto :goto_0

    :cond_4
    const v0, 0x7f0b0370    # com.google.android.gms.R.string.plus_list_moments_empty_message

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v0}, Lfpq;->a(Landroid/content/Context;)Lfpq;

    move-result-object v0

    iput-object v0, p0, Lfpn;->ai:Lfpq;

    iget-object v0, p0, Lfpn;->ai:Lfpq;

    iget-object v1, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-static {v2}, Lfob;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lfpq;->a(Lfps;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final e()V
    .locals 3

    invoke-virtual {p0}, Lfpn;->s()Lak;

    move-result-object v0

    const/16 v1, 0xc8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lak;->a(ILandroid/os/Bundle;Lal;)Lcb;

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lfpp;->e(Landroid/os/Bundle;)V

    const-string v0, "moment_list_app_filter"

    iget-object v1, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "moment_list_deleted_moments"

    iget-object v1, p0, Lfpn;->ae:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    invoke-super {p0}, Lfpp;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lfph;

    invoke-virtual {v0, p3}, Lfph;->a(I)Lgkf;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const-class v1, Lcom/google/android/gms/plus/apps/ManageMomentActivity;

    invoke-direct {v3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "account"

    iget-object v1, p0, Lfpn;->i:Lfob;

    iget-object v1, v1, Lfob;->a:Landroid/accounts/Account;

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "app_activity"

    invoke-interface {v2}, Lgkf;->f()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lfpn;->Y:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v1, :cond_2

    iget-object v0, p0, Lfpn;->i:Lfob;

    iget-object v0, v0, Lfob;->c:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    invoke-interface {v0}, Lfxh;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2}, Lgkf;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lfxh;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v0

    :goto_0
    const-string v1, "application"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v1, 0x3

    invoke-virtual {v0, v3, v1}, Lo;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lfpn;->i:Lfob;

    sget-object v1, Lbck;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lbck;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lfob;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lfpn;->ah:Lbce;

    invoke-virtual {v0, p3}, Lbce;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lfpn;->i:Lfob;

    iget-object v1, v1, Lfob;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lfpn;->i:Lfob;

    invoke-virtual {v1, v0}, Lfob;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lfpn;->ab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfpn;->L()V

    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method

.method public final y()V
    .locals 1

    invoke-super {p0}, Lfpp;->y()V

    iget-object v0, p0, Lfpn;->af:Lfpk;

    invoke-virtual {v0, p0}, Lfpk;->b(Lfpm;)V

    iget-object v0, p0, Lfpn;->ai:Lfpq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpn;->ai:Lfpq;

    invoke-virtual {v0, p0}, Lfpq;->a(Lfps;)V

    :cond_0
    return-void
.end method
