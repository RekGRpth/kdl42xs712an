.class public abstract Lhhz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Landroid/content/ContentResolver;


# instance fields
.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lhhz;->a:Landroid/content/ContentResolver;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhhz;->b:Ljava/lang/String;

    iput-object p2, p0, Lhhz;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lhhz;
    .locals 1

    new-instance v0, Lhib;

    invoke-direct {v0, p0, p1}, Lhib;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lhhz;
    .locals 1

    new-instance v0, Lhic;

    invoke-direct {v0, p0, p1}, Lhic;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lhhz;
    .locals 2

    new-instance v0, Lhia;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhia;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lhhz;->a:Landroid/content/ContentResolver;

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lhhz;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lhhz;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
