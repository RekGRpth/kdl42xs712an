.class public final Ldta;
.super Ldru;
.source "SourceFile"


# instance fields
.field private final b:Ldad;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:J

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ldad;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ldru;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p2, p0, Ldta;->b:Ldad;

    iput-object p3, p0, Ldta;->c:Ljava/lang/String;

    iput-object p4, p0, Ldta;->d:Ljava/lang/String;

    iput-object p5, p0, Ldta;->e:Ljava/lang/String;

    iput-wide p6, p0, Ldta;->f:J

    iput-wide p8, p0, Ldta;->g:J

    iput-object p10, p0, Ldta;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    iget-object v0, p0, Ldta;->b:Ldad;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Ldta;->b:Ldad;

    invoke-interface {v0, p1}, Ldad;->c(Lcom/google/android/gms/common/data/DataHolder;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ldfr;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    iget-object v2, p0, Ldta;->e:Ljava/lang/String;

    iget-object v3, p0, Ldta;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Ldfr;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ldfr;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Ldta;->b:Ldad;

    invoke-interface {v0, v1}, Ldad;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method protected final b(Landroid/content/Context;Lcun;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    iget-object v0, p0, Ldta;->b:Ldad;

    if-eqz v0, :cond_0

    const/4 v11, 0x1

    :goto_0
    iget-object v2, p0, Ldta;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Ldta;->c:Ljava/lang/String;

    iget-object v4, p0, Ldta;->d:Ljava/lang/String;

    iget-object v5, p0, Ldta;->e:Ljava/lang/String;

    iget-wide v6, p0, Ldta;->f:J

    iget-wide v8, p0, Ldta;->g:J

    iget-object v10, p0, Ldta;->h:Ljava/lang/String;

    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v11}, Lcun;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Z)Ldfr;

    move-result-object v0

    invoke-virtual {v0}, Ldfr;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v11, 0x0

    goto :goto_0
.end method
