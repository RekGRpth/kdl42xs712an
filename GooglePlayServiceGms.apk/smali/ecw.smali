.class public final Lecw;
.super Lecz;
.source "SourceFile"

# interfaces
.implements Lduv;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lecz;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Lecz;->a(IILandroid/content/Intent;)V

    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lecw;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lecw;->c(I)V

    goto :goto_0
.end method

.method public final a(ILandroid/content/Intent;)V
    .locals 1

    invoke-virtual {p0}, Lecw;->J()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lecw;->c(I)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lecw;->a(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lecw;->d(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lecw;->e(I)V

    goto :goto_0
.end method

.method public final a(Lduk;)V
    .locals 5

    iget-object v0, p0, Lecz;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lduk;->c()V

    :try_start_0
    iget-object v1, p1, Lduk;->f:Ldam;

    new-instance v2, Ldun;

    invoke-direct {v2, p1, p0}, Ldun;-><init>(Lduk;Lduv;)V

    iget-object v3, p1, Lduk;->d:Ljava/lang/String;

    iget-object v4, p1, Lduk;->e:[Ljava/lang/String;

    invoke-interface {v1, v2, v0, v3, v4}, Ldam;->a(Ldaj;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Ldac;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method
