.class public final Lbyg;
.super Lbxg;
.source "SourceFile"

# interfaces
.implements Lbzq;
.implements Lbzr;


# instance fields
.field private Y:Lcom/google/android/gms/drive/DriveId;

.field private Z:Lcom/google/android/gms/drive/data/view/DocListView;

.field private a:Lcfz;

.field private aa:Landroid/widget/ProgressBar;

.field private ab:Lcom/google/android/gms/drive/DriveId;

.field private ac:Lcfc;

.field private ad:Lbsp;

.field private ae:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private af:Lcat;

.field private ag:Ljava/lang/Runnable;

.field private ah:Landroid/database/ContentObserver;

.field private ai:Ljava/util/Set;

.field private aj:Landroid/view/View;

.field private ak:Landroid/widget/Button;

.field private al:Landroid/widget/Button;

.field private am:Lbyp;

.field private b:Lbtq;

.field private c:Lbwd;

.field private d:Lcmn;

.field private e:Lcdu;

.field private f:Lcom/google/android/gms/drive/DriveId;

.field private g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

.field private h:Lcom/google/android/gms/drive/DriveId;

.field private final i:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lbxg;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbyg;->i:Ljava/util/HashMap;

    sget-object v0, Lcat;->a:Lcat;

    iput-object v0, p0, Lbyg;->af:Lcat;

    sget-object v0, Lbyp;->a:Lbyp;

    iput-object v0, p0, Lbyg;->am:Lbyp;

    return-void
.end method

.method private J()V
    .locals 2

    iget-object v1, p0, Lbyg;->ak:Landroid/widget/Button;

    iget-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-interface {v0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private L()V
    .locals 4

    iget-object v0, p0, Lbyg;->ac:Lcfc;

    iget-object v0, v0, Lcfc;->a:Ljava/lang/String;

    iget-object v1, p0, Lbyg;->c:Lbwd;

    invoke-virtual {v1, v0}, Lbwd;->a(Ljava/lang/String;)Lccw;

    move-result-object v1

    iget-object v2, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lccw;)V

    iget-object v1, p0, Lbyg;->c:Lbwd;

    invoke-virtual {v1, v0}, Lbwd;->b(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lbyg;->aa:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    if-eqz v1, :cond_0

    iget-object v0, p0, Lbyg;->ag:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    if-eqz v0, :cond_0

    new-instance v0, Lbyo;

    invoke-direct {v0, p0}, Lbyo;-><init>(Lbyg;)V

    iput-object v0, p0, Lbyg;->ag:Ljava/lang/Runnable;

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v1, p0, Lbyg;->ag:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/data/view/DocListView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Lbyg;Lbyp;)Lbyp;
    .locals 0

    iput-object p1, p0, Lbyg;->am:Lbyp;

    return-object p1
.end method

.method static synthetic a(Lbyg;)V
    .locals 0

    invoke-direct {p0}, Lbyg;->L()V

    return-void
.end method

.method static synthetic a(Lbyg;Lcom/google/android/gms/drive/DriveId;)V
    .locals 0

    invoke-direct {p0, p1}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/drive/DriveId;)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    const-string v0, "PickEntryDialogFragment"

    const-string v1, "Activity is null in onAttemptEntrySelection()"

    invoke-static {v0, v1}, Lcbv;->c(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-nez v0, :cond_1

    iput-object v2, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    iput-object v2, p0, Lbyg;->Y:Lcom/google/android/gms/drive/DriveId;

    :goto_1
    invoke-direct {p0}, Lbyg;->J()V

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_e

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v1, p0, Lbyg;->ac:Lcfc;

    iget-object v1, v1, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v1

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v3, p0, Lbyg;->ac:Lcfc;

    iget-object v3, v3, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v3, p0, Lbyg;->a:Lcfz;

    iget-object v4, p0, Lbyg;->ac:Lcfc;

    iget-object v4, v4, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v1

    if-nez v1, :cond_c

    invoke-direct {p0, v0}, Lbyg;->b(Lcfp;)Lcfp;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v4, "disablePreselectedEntry"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "driveId"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcfp;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lbyg;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    invoke-direct {p0, v1}, Lbyg;->a(Lcfp;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v1}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v1}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/DriveId;)V

    :goto_3
    if-eqz v3, :cond_7

    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v3}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, v3}, Lbyg;->b(Lcfp;)Lcfp;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    :goto_5
    new-instance v0, Lbtm;

    invoke-direct {v0}, Lbtm;-><init>()V

    new-instance v1, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;

    iget-object v4, p0, Lbyg;->ac:Lcfc;

    iget-object v4, v4, Lcfc;->a:Ljava/lang/String;

    invoke-direct {v1, v4}, Lcom/google/android/gms/drive/data/app/model/navigation/AccountCriterion;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbtm;->a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Lbtm;

    const-string v1, "notInTrash"

    invoke-static {v1}, Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/data/app/model/navigation/SimpleCriterion;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbtm;->a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Lbtm;

    if-eqz v3, :cond_8

    iget-object v1, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v3}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    new-instance v1, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;

    iget-object v4, p0, Lbyg;->ac:Lcfc;

    iget-object v4, v4, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/drive/data/app/model/navigation/ChildrenOfCollectionCriterion;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)V

    invoke-virtual {v0, v1}, Lbtm;->a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Lbtm;

    :goto_6
    new-instance v1, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;

    iget-object v0, v0, Lbtm;->a:Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSetImpl;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iput-object v1, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    :try_start_0
    iget-object v0, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    iget-object v1, p0, Lbyg;->a:Lcfz;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;->a(Lcfz;)V
    :try_end_0
    .catch Lbtl; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lbyg;->b:Lbtq;

    new-instance v1, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    iget-object v4, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-direct {v1, v4}, Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;-><init>(Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;)V

    invoke-interface {v0, v1}, Lbtq;->a(Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->a()Z

    move-result v0

    if-nez v0, :cond_9

    :cond_3
    :goto_7
    invoke-direct {p0}, Lbyg;->b()V

    :cond_4
    if-nez v3, :cond_b

    :goto_8
    iput-object v2, p0, Lbyg;->Y:Lcom/google/android/gms/drive/DriveId;

    goto/16 :goto_1

    :cond_5
    iput-object v2, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    goto :goto_4

    :cond_7
    iput-object v2, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    goto :goto_5

    :cond_8
    new-instance v1, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;

    iget-object v4, p0, Lbyg;->am:Lbyp;

    iget-object v4, v4, Lbyp;->d:Lbzk;

    iget-object v5, p0, Lbyg;->ac:Lcfc;

    iget-object v5, v5, Lcfc;->a:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lcom/google/android/gms/drive/data/app/model/navigation/EntriesFilterCriterion;-><init>(Lbzk;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbtm;->a(Lcom/google/android/gms/drive/data/app/model/navigation/Criterion;)Lbtm;

    goto :goto_6

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_9
    iget-object v0, p0, Lbyg;->af:Lcat;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v1, p0, Lbyg;->af:Lcat;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcat;)V

    :cond_a
    iget-object v0, p0, Lbyg;->b:Lbtq;

    invoke-interface {v0}, Lbtq;->a()Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v1, p0, Lbyg;->b:Lbtq;

    invoke-interface {v1}, Lbtq;->b()Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcom/google/android/gms/drive/data/app/model/navigation/NavigationPathElement;)V

    goto :goto_7

    :cond_b
    invoke-virtual {v3}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    goto :goto_8

    :cond_c
    move-object v3, v1

    move-object v1, v0

    goto/16 :goto_2

    :cond_d
    move-object v1, v0

    move-object v3, v2

    goto/16 :goto_2

    :cond_e
    move-object v1, v2

    move-object v3, v2

    goto/16 :goto_2
.end method

.method private a(Lcfp;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v2, p0, Lbyg;->ad:Lbsp;

    invoke-interface {v0, v2, p1}, Lcfz;->a(Lbsp;Lcfp;)Lcgs;

    move-result-object v2

    :try_start_0
    invoke-interface {v2}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    invoke-direct {p0, v0}, Lbyg;->a(Lcfp;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v2}, Lcgs;->close()V

    iget-object v1, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Lcgs;->close()V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private b(Lcfp;)Lcfp;
    .locals 5

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v1, p0, Lbyg;->ad:Lbsp;

    invoke-interface {v0, v1, p1}, Lcfz;->a(Lbsp;Lcfp;)Lcgs;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcgs;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcgs;->close()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lbyg;->K()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-interface {v1}, Lcgs;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;

    invoke-virtual {v0}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Lcgs;->close()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v1, v0}, Lcgs;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfp;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v1}, Lcgs;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcgs;->close()V

    throw v0
.end method

.method private b()V
    .locals 9

    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbyg;->aj:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-nez v0, :cond_5

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/data/view/DocListView;->setVisibility(I)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->R:Landroid/view/View;

    const v1, 0x7f0a00ff    # com.google.android.gms.R.id.drive_top_collections_list

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lbyg;->am:Lbyp;

    if-eqz v1, :cond_6

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lbyg;->aj:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbyg;->aj:Landroid/view/View;

    const v1, 0x7f0a00fd    # com.google.android.gms.R.id.parent_title

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lbyg;->K()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    iget-object v1, p0, Lbyg;->aj:Landroid/view/View;

    const v5, 0x7f0a005d    # com.google.android.gms.R.id.icon

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    if-nez v4, :cond_2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lbyg;->am:Lbyp;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbyg;->am:Lbyp;

    iget v1, v1, Lbyp;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    if-nez v4, :cond_3

    const/4 v4, 0x0

    :goto_3
    if-nez v4, :cond_4

    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lbyg;->ac:Lcfc;

    invoke-static {v5}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v5

    iget-object v6, p0, Lbyg;->a:Lcfz;

    iget-object v7, p0, Lbyg;->ac:Lcfc;

    iget-object v7, v7, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v4

    invoke-interface {v6, v5, v4}, Lcfz;->a(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v4

    goto :goto_3

    :cond_4
    invoke-virtual {v4}, Lcfp;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Lcfp;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcfp;->s()Z

    move-result v4

    invoke-static {v0, v4}, Lcfp;->a(Ljava/lang/String;Z)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto/16 :goto_1

    :cond_6
    move v2, v3

    goto :goto_2
.end method

.method static synthetic b(Lbyg;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lbyg;->am:Lbyp;

    iput-object v1, p0, Lbyg;->g:Lcom/google/android/gms/drive/data/app/model/navigation/CriterionSet;

    invoke-direct {p0}, Lbyg;->b()V

    invoke-direct {p0, v1}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lbyg;->ai:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lbyg;)Lbyp;
    .locals 1

    iget-object v0, p0, Lbyg;->am:Lbyp;

    return-object v0
.end method

.method static synthetic d(Lbyg;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method static synthetic e(Lbyg;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lbyg;->ak:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lbyg;)V
    .locals 10

    new-instance v8, Landroid/content/Intent;

    const-string v0, "android.intent.action.PICK"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v1, p0, Lbyg;->ad:Lbsp;

    iget-object v2, p0, Lbyg;->ac:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    iget-object v2, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v9

    if-nez v9, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v1, p0, Lbyg;->ac:Lcfc;

    iget-object v2, p0, Lbyg;->ae:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v0, v1, v2}, Lcfz;->a(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;)Lbsp;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v0, Lcmt;

    iget-object v1, p0, Lbyg;->ac:Lcfc;

    iget-object v2, v4, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v3, p0, Lbyg;->ac:Lcfc;

    iget-object v3, v3, Lcfc;->a:Ljava/lang/String;

    iget-object v3, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v3

    iget-wide v4, v4, Lbsp;->b:J

    sget-object v6, Lbsj;->a:Lbsj;

    sget-object v7, Lcms;->b:Lcms;

    invoke-direct/range {v0 .. v7}, Lcmt;-><init>(Lcfc;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/data/EntrySpec;JLbsj;Lcms;)V

    iget-object v1, p0, Lbyg;->d:Lcmn;

    invoke-virtual {v1, v0}, Lcmn;->a(Lcml;)Z

    :goto_1
    const-string v0, "response_drive_id"

    iget-object v1, p0, Lbyg;->e:Lcdu;

    invoke-static {v9, v1}, Lcfu;->a(Lcfp;Lcdu;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v8}, Lo;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v0, "PickEntryDialogFragment"

    const-string v1, "Cannot grant app %s access to driveId %s because the app is no longer authorized."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbyg;->ae:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcbv;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method static synthetic g(Lbyg;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lbyg;->al:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic h(Lbyg;)Lcom/google/android/gms/drive/data/view/DocListView;
    .locals 1

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    return-object v0
.end method

.method static synthetic i(Lbyg;)Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbyg;->ag:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f04004f    # com.google.android.gms.R.layout.drive_file_picker

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0a00f5    # com.google.android.gms.R.id.doc_list_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/data/view/DocListView;

    iput-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Landroid/support/v4/app/Fragment;)V

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lbzr;)V

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "accountName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbyg;->a:Lcfz;

    invoke-interface {v3, v2}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lcfc;)V

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/data/view/DocListView;->a(Lbzq;)V

    const v0, 0x7f0a0101    # com.google.android.gms.R.id.drive_progress_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbyg;->aa:Landroid/widget/ProgressBar;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-ne v0, v2, :cond_0

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/4 v2, -0x4

    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lbyg;->aa:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const v0, 0x7f0a00fc    # com.google.android.gms.R.id.drive_navigate_up_bar

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbyg;->aj:Landroid/view/View;

    iget-object v0, p0, Lbyg;->aj:Landroid/view/View;

    new-instance v2, Lbyj;

    invoke-direct {v2, p0}, Lbyj;-><init>(Lbyg;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v0, 0x7f0a00ff    # com.google.android.gms.R.id.drive_top_collections_list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Lbyk;

    invoke-static {}, Lbyp;->b()[Lbyp;

    move-result-object v4

    invoke-direct {v3, p0, v2, v4}, Lbyk;-><init>(Lbyg;Landroid/content/Context;[Lbyp;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lbyl;

    invoke-direct {v2, p0}, Lbyl;-><init>(Lbyg;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f0a0102    # com.google.android.gms.R.id.drive_file_picker_button_left

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbyg;->ak:Landroid/widget/Button;

    const v0, 0x7f0a0103    # com.google.android.gms.R.id.drive_file_picker_button_right

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbyg;->al:Landroid/widget/Button;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_1

    iget-object v0, p0, Lbyg;->ak:Landroid/widget/Button;

    iget-object v2, p0, Lbyg;->al:Landroid/widget/Button;

    iput-object v2, p0, Lbyg;->ak:Landroid/widget/Button;

    iput-object v0, p0, Lbyg;->al:Landroid/widget/Button;

    :cond_1
    new-instance v2, Lbym;

    invoke-direct {v2, p0}, Lbym;-><init>(Lbyg;)V

    iget-object v0, p0, Lbyg;->ak:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lbyg;->ak:Landroid/widget/Button;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v4, "selectButtonText"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {p0, v4}, Lbyg;->b(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbyg;->al:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbyg;->al:Landroid/widget/Button;

    const/high16 v2, 0x1040000    # android.R.string.cancel

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    return-object v1

    :cond_3
    const-string v4, "selectButtonText"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const v0, 0x7f0b0028    # com.google.android.gms.R.string.drive_dialog_select

    invoke-virtual {p0, v0}, Lbyg;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->c()Lcom/google/android/gms/drive/data/ui/open/doclist/DocListQuery;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    new-instance v1, Lbyn;

    invoke-direct {v1, p0}, Lbyn;-><init>(Lbyg;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/data/view/DocListView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    if-nez p1, :cond_2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, "driveId"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    :goto_0
    invoke-direct {p0, v0}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lbxg;->a(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lbxg;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    const/high16 v0, 0x7f120000    # com.google.android.gms.R.menu.drive_file_picker

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v1, "showNewFolder"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0a037c    # com.google.android.gms.R.id.drive_menu_create_folder

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V
    .locals 4

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v1, p0, Lbyg;->ad:Lbsp;

    iget-object v2, p0, Lbyg;->ac:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/data/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/data/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcfz;->b(Lbsp;Lcom/google/android/gms/drive/database/data/EntrySpec;)Lcfp;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcfp;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbyg;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "application/vnd.google-apps.folder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lbyg;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcoy;->a(Landroid/content/Context;)Lcoy;

    move-result-object v0

    invoke-virtual {v0}, Lcoy;->f()Lcfz;

    move-result-object v2

    iput-object v2, p0, Lbyg;->a:Lcfz;

    new-instance v2, Lbtr;

    invoke-direct {v2}, Lbtr;-><init>()V

    iput-object v2, p0, Lbyg;->b:Lbtq;

    invoke-virtual {v0}, Lcoy;->k()Lbwd;

    move-result-object v2

    iput-object v2, p0, Lbyg;->c:Lbwd;

    invoke-virtual {v0}, Lcoy;->h()Lcmn;

    move-result-object v2

    iput-object v2, p0, Lbyg;->d:Lcmn;

    invoke-virtual {v0}, Lcoy;->d()Lcdu;

    move-result-object v0

    iput-object v0, p0, Lbyg;->e:Lcdu;

    invoke-super {p0, p1}, Lbxg;->a_(Landroid/os/Bundle;)V

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    iget-object v0, p0, Lbyg;->a:Lcfz;

    const-string v3, "accountName"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v0

    iput-object v0, p0, Lbyg;->ac:Lcfc;

    const-string v0, "callerIdentity"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lbyg;->ae:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v0, p0, Lbyg;->ac:Lcfc;

    invoke-static {v0}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iput-object v0, p0, Lbyg;->ad:Lbsp;

    iget-object v0, p0, Lbyg;->a:Lcfz;

    iget-object v3, p0, Lbyg;->ad:Lbsp;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v4}, Lo;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b004b    # com.google.android.gms.R.string.drive_menu_my_drive

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Lcfz;->b(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    const-string v0, "mimeTypes"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v0, v2

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lbkm;->b(Z)V

    invoke-static {v2}, Lcbu;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbyg;->ai:Ljava/util/Set;

    if-eqz p1, :cond_2

    const-string v0, "driveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    iget-object v0, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const-string v0, "disabledAncestors"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v0, "parentDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    const-string v0, "sortKind"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcat;

    iput-object v0, p0, Lbyg;->af:Lcat;

    const-string v0, "topCollection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Lbyp;->a(Ljava/lang/String;)Lbyp;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lbyg;->am:Lbyp;

    const-string v0, "currentFolderDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lbyg;->Y:Lcom/google/android/gms/drive/DriveId;

    :cond_2
    iget-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    if-nez v0, :cond_3

    sget-object v0, Lbyp;->a:Lbyp;

    iget-object v1, p0, Lbyg;->am:Lbyp;

    invoke-virtual {v0, v1}, Lbyp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "application/vnd.google-apps.folder"

    invoke-direct {p0, v0}, Lbyg;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbyg;->ab:Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    :cond_3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lbyh;

    invoke-direct {v1, p0, v0, v0}, Lbyh;-><init>(Lbyg;Landroid/os/Handler;Landroid/os/Handler;)V

    iput-object v1, p0, Lbyg;->ah:Landroid/database/ContentObserver;

    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a_(Landroid/view/MenuItem;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a037c    # com.google.android.gms.R.id.drive_menu_create_folder

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lbyg;->K()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v3, p0, Lbyg;->ac:Lcfc;

    iget-object v3, v3, Lcfc;->a:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/drive/data/ui/open/CreateFolderActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbyg;->a(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f0a037b    # com.google.android.gms.R.id.drive_menu_refresh

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lbyg;->c:Lbwd;

    iget-object v2, p0, Lbyg;->ac:Lcfc;

    iget-object v2, v2, Lcfc;->a:Ljava/lang/String;

    const/16 v3, 0x66

    invoke-virtual {v1, v2, v3}, Lbwd;->a(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lbyg;->L()V

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v2, 0x7f0b0050    # com.google.android.gms.R.string.drive_menu_sync_fail_no_connection

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    const v2, 0x7f0b0051    # com.google.android.gms.R.string.drive_menu_sync_fail_generic

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lbxg;->a_(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lbxg;->e(Landroid/os/Bundle;)V

    const-string v0, "driveId"

    iget-object v1, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "parentDriveId"

    iget-object v1, p0, Lbyg;->h:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lbyg;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, "disabledAncestors"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "sortKind"

    iget-object v1, p0, Lbyg;->af:Lcat;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "topCollection"

    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbyg;->am:Lbyp;

    iget-object v0, v0, Lbyp;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "currentFolderDriveId"

    iget-object v1, p0, Lbyg;->Y:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/data/view/DocListView;->h()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbyg;->Z:Lcom/google/android/gms/drive/data/view/DocListView;

    invoke-super {p0}, Lbxg;->f()V

    return-void
.end method

.method public final w()V
    .locals 4

    invoke-super {p0}, Lbxg;->w()V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lbwd;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lbyg;->ah:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lbyg;->am:Lbyp;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lbyg;->b()V

    invoke-direct {p0}, Lbyg;->J()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbyg;->f:Lcom/google/android/gms/drive/DriveId;

    :goto_1
    invoke-direct {p0, v0}, Lbyg;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbyg;->Y:Lcom/google/android/gms/drive/DriveId;

    goto :goto_1
.end method

.method public final x()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbyg;->ah:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    invoke-super {p0}, Lbxg;->x()V

    return-void
.end method
