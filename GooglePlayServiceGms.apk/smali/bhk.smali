.class public final Lbhk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lbgt;


# direct methods
.method public constructor <init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lbhk;->a:Ljava/lang/String;

    iput-object p4, p0, Lbhk;->b:Ljava/lang/String;

    if-eqz p2, :cond_0

    invoke-static {p1, p2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;Ljava/lang/String;)Lbgt;

    move-result-object v0

    iput-object v0, p0, Lbhk;->c:Lbgt;

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lbgt;

    move-result-object v0

    iput-object v0, p0, Lbhk;->c:Lbgt;

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lbhk;->c:Lbgt;

    iget-object v0, v0, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    const-string v0, "next_page_token"

    iget-object v1, p0, Lbhk;->b:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "prev_page_token"

    iget-object v1, p0, Lbhk;->a:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbhk;->c:Lbgt;

    new-instance v0, Lcom/google/android/gms/common/data/DataHolder;

    const/4 v5, 0x0

    move v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Lbgt;ILandroid/os/Bundle;IB)V

    return-object v0
.end method

.method public final a(Landroid/content/ContentValues;)V
    .locals 1

    iget-object v0, p0, Lbhk;->c:Lbgt;

    invoke-virtual {v0, p1}, Lbgt;->a(Landroid/content/ContentValues;)Lbgt;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    iget-object v2, p0, Lbhk;->c:Lbgt;

    invoke-static {p1}, Lbiq;->a(Ljava/lang/Object;)V

    iget-boolean v0, v2, Lbgt;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, v2, Lbgt;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, v2, Lbgt;->b:Ljava/util/ArrayList;

    new-instance v1, Lbgu;

    invoke-direct {v1, p1}, Lbgu;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, v2, Lbgt;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lbgt;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    iget-object v1, v2, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, v2, Lbgt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iget-object v4, v2, Lbgt;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v4, v2, Lbgt;->d:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, v2, Lbgt;->e:Z

    iput-object p1, v2, Lbgt;->f:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public final b()Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lbhk;->a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
