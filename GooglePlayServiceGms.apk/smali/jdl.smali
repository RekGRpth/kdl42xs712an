.class public final Ljdl;
.super Lizs;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:[B

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lizs;-><init>()V

    iput v1, p0, Ljdl;->a:I

    const-string v0, ""

    iput-object v0, p0, Ljdl;->b:Ljava/lang/String;

    sget-object v0, Lizv;->h:[B

    iput-object v0, p0, Ljdl;->c:[B

    iput-boolean v1, p0, Ljdl;->d:Z

    const-string v0, ""

    iput-object v0, p0, Ljdl;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Ljdl;->f:Ljava/lang/String;

    iput-boolean v1, p0, Ljdl;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Ljdl;->C:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-super {p0}, Lizs;->a()I

    move-result v0

    iget v1, p0, Ljdl;->a:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Ljdl;->a:I

    invoke-static {v1, v2}, Lizn;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Ljdl;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Ljdl;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Ljdl;->c:[B

    sget-object v2, Lizv;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Ljdl;->c:[B

    invoke-static {v1, v2}, Lizn;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Ljdl;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Ljdl;->d:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Ljdl;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Ljdl;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Ljdl;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Ljdl;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lizn;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Ljdl;->g:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Ljdl;->g:Z

    invoke-static {v1}, Lizn;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Ljdl;->C:I

    return v0
.end method

.method public final synthetic a(Lizm;)Lizs;
    .locals 1

    invoke-virtual {p0, p1}, Ljdl;->b(Lizm;)Ljdl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lizn;)V
    .locals 2

    iget v0, p0, Ljdl;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Ljdl;->a:I

    invoke-virtual {p1, v0, v1}, Lizn;->a(II)V

    :cond_0
    iget-object v0, p0, Ljdl;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Ljdl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ljdl;->c:[B

    sget-object v1, Lizv;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Ljdl;->c:[B

    invoke-virtual {p1, v0, v1}, Lizn;->a(I[B)V

    :cond_2
    iget-boolean v0, p0, Ljdl;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Ljdl;->d:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_3
    iget-object v0, p0, Ljdl;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Ljdl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Ljdl;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Ljdl;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lizn;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Ljdl;->g:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Ljdl;->g:Z

    invoke-virtual {p1, v0, v1}, Lizn;->a(IZ)V

    :cond_6
    invoke-super {p0, p1}, Lizs;->a(Lizn;)V

    return-void
.end method

.method public final b(Lizm;)Ljdl;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lizm;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lizv;->a(Lizm;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lizm;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Ljdl;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lizm;->d()[B

    move-result-object v0

    iput-object v0, p0, Ljdl;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljdl;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdl;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lizm;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljdl;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lizm;->b()Z

    move-result v0

    iput-boolean v0, p0, Ljdl;->g:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Ljdl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Ljdl;

    iget v2, p0, Ljdl;->a:I

    iget v3, p1, Ljdl;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Ljdl;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Ljdl;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Ljdl;->b:Ljava/lang/String;

    iget-object v3, p1, Ljdl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Ljdl;->c:[B

    iget-object v3, p1, Ljdl;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Ljdl;->d:Z

    iget-boolean v3, p1, Ljdl;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Ljdl;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Ljdl;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Ljdl;->e:Ljava/lang/String;

    iget-object v3, p1, Ljdl;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p0, Ljdl;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Ljdl;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Ljdl;->f:Ljava/lang/String;

    iget-object v3, p1, Ljdl;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-boolean v2, p0, Ljdl;->g:Z

    iget-boolean v3, p1, Ljdl;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    iget v0, p0, Ljdl;->a:I

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljdl;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljdl;->c:[B

    invoke-static {v4}, Ljava/util/Arrays;->hashCode([B)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ljdl;->d:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ljdl;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ljdl;->f:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Ljdl;->g:Z

    if-eqz v1, :cond_4

    :goto_4
    add-int/2addr v0, v2

    return v0

    :cond_0
    iget-object v0, p0, Ljdl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljdl;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Ljdl;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_4
.end method
