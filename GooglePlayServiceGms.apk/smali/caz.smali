.class public final Lcaz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Handler;

.field public b:Landroid/database/ContentObserver;

.field final c:Lcbc;

.field public final d:Lcbe;

.field public final e:Lcce;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcbc;Lcbe;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcaz;->a:Landroid/os/Handler;

    iput-object p1, p0, Lcaz;->c:Lcbc;

    iput-object p2, p0, Lcaz;->d:Lcbe;

    new-instance v0, Lcba;

    invoke-direct {v0, p0}, Lcba;-><init>(Lcaz;)V

    iput-object v0, p0, Lcaz;->f:Ljava/lang/Runnable;

    sget-object v0, Lccg;->a:Lccf;

    iget-object v1, p0, Lcaz;->f:Ljava/lang/Runnable;

    const/16 v2, 0x7d0

    new-instance v3, Lcbs;

    iget-object v4, p0, Lcaz;->a:Landroid/os/Handler;

    invoke-direct {v3, v4}, Lcbs;-><init>(Landroid/os/Handler;)V

    const-string v4, "ActivityUpdater"

    invoke-interface {v0, v1, v2, v3, v4}, Lccf;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lcce;

    move-result-object v0

    iput-object v0, p0, Lcaz;->e:Lcce;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ActivityUpdater[rateLimiter=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcaz;->e:Lcce;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
