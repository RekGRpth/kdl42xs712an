.class public abstract Lbop;
.super Lbon;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbol;

.field private final c:Lbgo;

.field private final d:Ljava/lang/Integer;

.field private final e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lbol;Lbgo;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lbop;-><init>(Lbol;Lbgo;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Lbol;Lbgo;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-object p1, p0, Lbop;->a:Lbol;

    invoke-direct {p0, p1}, Lbon;-><init>(Lbol;)V

    if-nez p3, :cond_0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    :cond_0
    if-nez p4, :cond_1

    if-nez p2, :cond_2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    :cond_1
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "rangeStart"

    invoke-static {v0, v3}, Lbkm;->b(ZLjava/lang/Object;)V

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Lbgo;->a()I

    move-result v3

    if-gt v0, v3, :cond_4

    :goto_2
    const-string v0, "rangeEnd"

    invoke-static {v1, v0}, Lbkm;->b(ZLjava/lang/Object;)V

    iput-object p2, p0, Lbop;->c:Lbgo;

    iput-object p3, p0, Lbop;->d:Ljava/lang/Integer;

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lbop;->e:Ljava/lang/Integer;

    return-void

    :cond_2
    invoke-virtual {p2}, Lbgo;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lbop;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1}, Lbop;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lbop;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lbop;->c:Lbgo;

    iget-object v1, p0, Lbop;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lbgo;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
