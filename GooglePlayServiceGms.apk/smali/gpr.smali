.class public final Lgpr;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lfam;
.implements Lfan;
.implements Lfao;
.implements Lfaq;
.implements Lfas;
.implements Lfaw;
.implements Lful;
.implements Lfum;
.implements Lfun;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Lbbo;


# instance fields
.field private Y:Ljava/lang/String;

.field private Z:[Ljava/lang/String;

.field private aa:Ljava/lang/String;

.field private ab:I

.field private ac:Z

.field private ad:Lcom/google/android/gms/plus/model/posts/Post;

.field private ae:Lcom/google/android/gms/plus/model/posts/Settings;

.field private af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private ag:Lfst;

.field private ah:Ljava/lang/String;

.field private ai:Landroid/graphics/Bitmap;

.field private aj:Z

.field private ak:Ljava/lang/String;

.field private al:Z

.field private am:Ljava/lang/String;

.field private an:Lcom/google/android/gms/common/people/data/Audience;

.field private final ao:Ljava/util/ArrayList;

.field private ap:J

.field private final c:Lftz;

.field private d:Lftx;

.field private e:Lgpv;

.field private f:Lfaj;

.field private g:Lgpu;

.field private h:Lgpw;

.field private i:Lgpx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    sput-object v0, Lgpr;->a:[Ljava/lang/String;

    new-instance v0, Lbbo;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    sput-object v0, Lgpr;->b:Lbbo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lftx;->a:Lftz;

    invoke-direct {p0, v0}, Lgpr;-><init>(Lftz;)V

    return-void
.end method

.method private constructor <init>(Lftz;)V
    .locals 2

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    sget-object v0, Lfsr;->ac:Lbfy;

    invoke-virtual {v0}, Lbfy;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lgpr;->ap:J

    iput-object p1, p0, Lgpr;->c:Lftz;

    return-void
.end method

.method private W()V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lgpr;->an:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lgof;->a(Lcom/google/android/gms/common/people/data/Audience;)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const-string v0, "ShareBox"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ShareBox"

    const-string v1, "No people to add to circle"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v0, Lgpr;->b:Lbbo;

    invoke-virtual {p0, v0, v2, v2}, Lgpr;->a(Lbbo;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v2, p0, Lgpr;->aa:Ljava/lang/String;

    iget-object v1, p0, Lgpr;->i:Lgpx;

    invoke-virtual {v1}, Lgpx;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgpr;->am:Ljava/lang/String;

    iget-object v0, v0, Lfaj;->a:Lfch;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lfch;->a(Lfan;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpr;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    goto :goto_0
.end method

.method static synthetic a(Lgpr;I)I
    .locals 0

    iput p1, p0, Lgpr;->ab:I

    return p1
.end method

.method static synthetic a(Lgpr;J)J
    .locals 0

    iput-wide p1, p0, Lgpr;->ap:J

    return-wide p1
.end method

.method public static a(Ljava/lang/String;)Lgpr;
    .locals 3

    sget-object v0, Lftx;->a:Lftz;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "specified_account_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lgpr;

    invoke-direct {v2, v0}, Lgpr;-><init>(Lftz;)V

    invoke-virtual {v2, v1}, Lgpr;->g(Landroid/os/Bundle;)V

    return-object v2
.end method

.method static synthetic a(Lgpr;)Lgpw;
    .locals 1

    iget-object v0, p0, Lgpr;->h:Lgpw;

    return-object v0
.end method

.method static synthetic a(Lgpr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lgpr;->aa:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lbbo;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V
    .locals 2

    iput-object p2, p0, Lgpr;->af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    iget-object v1, p0, Lgpr;->af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {v0, p1}, Lgpw;->b(Lbbo;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lgpr;Lbbo;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lgpr;->a(Lbbo;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V

    return-void
.end method

.method static synthetic b(Lgpr;)Lftx;
    .locals 1

    iget-object v0, p0, Lgpr;->d:Lftx;

    return-object v0
.end method

.method static synthetic b(Lgpr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lgpr;->ah:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lgpr;)Lgpx;
    .locals 1

    iget-object v0, p0, Lgpr;->i:Lgpx;

    return-object v0
.end method

.method static synthetic d(Lgpr;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->aa:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lgpr;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->Z:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lgpr;)I
    .locals 1

    iget v0, p0, Lgpr;->ab:I

    return v0
.end method

.method static synthetic g(Lgpr;)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v1, p0, Lgpr;->ao:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_7

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgps;

    invoke-virtual {v0}, Lgps;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lgps;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lgps;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lgps;->b()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued navigation log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lgps;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, v0, Lgps;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v1, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v0}, Lgps;->c()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Lgps;->c()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued action log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    iget-object v1, v0, Lgps;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, v0, Lgps;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v7, v0, Lgps;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-object v0, v0, Lgps;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {p0, v1, v6, v7, v0}, Lgpr;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    goto :goto_3

    :cond_7
    return-void
.end method

.method static synthetic h(Lgpr;)Lfaj;
    .locals 1

    iget-object v0, p0, Lgpr;->f:Lfaj;

    return-object v0
.end method

.method static synthetic i(Lgpr;)Z
    .locals 1

    iget-boolean v0, p0, Lgpr;->ac:Z

    return v0
.end method

.method static synthetic j(Lgpr;)Lcom/google/android/gms/plus/model/posts/Post;
    .locals 1

    iget-object v0, p0, Lgpr;->ad:Lcom/google/android/gms/plus/model/posts/Post;

    return-object v0
.end method

.method static synthetic k(Lgpr;)Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 1

    iget-object v0, p0, Lgpr;->ae:Lcom/google/android/gms/plus/model/posts/Settings;

    return-object v0
.end method

.method static synthetic l(Lgpr;)Lfst;
    .locals 1

    iget-object v0, p0, Lgpr;->ag:Lfst;

    return-object v0
.end method

.method static synthetic m(Lgpr;)J
    .locals 2

    iget-wide v0, p0, Lgpr;->ap:J

    return-wide v0
.end method

.method static synthetic n(Lgpr;)Z
    .locals 1

    iget-boolean v0, p0, Lgpr;->aj:Z

    return v0
.end method

.method static synthetic o(Lgpr;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgpr;->aj:Z

    return v0
.end method

.method static synthetic p(Lgpr;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->ak:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic q(Lgpr;)Z
    .locals 1

    iget-boolean v0, p0, Lgpr;->al:Z

    return v0
.end method

.method static synthetic r(Lgpr;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lgpr;->al:Z

    return v0
.end method

.method static synthetic s(Lgpr;)V
    .locals 0

    invoke-direct {p0}, Lgpr;->W()V

    return-void
.end method

.method static synthetic t(Lgpr;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;
    .locals 1

    iget-object v0, p0, Lgpr;->af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    return-object v0
.end method

.method static synthetic u(Lgpr;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->ah:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v(Lgpr;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lgpr;->ai:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public final J()Z
    .locals 1

    iget-object v0, p0, Lgpr;->aa:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->Z:[Ljava/lang/String;

    return-object v0
.end method

.method public final L()I
    .locals 1

    iget v0, p0, Lgpr;->ab:I

    return v0
.end method

.method public final M()Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 1

    iget-object v0, p0, Lgpr;->ae:Lcom/google/android/gms/plus/model/posts/Settings;

    return-object v0
.end method

.method public final M_()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->M_()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgpr;->h:Lgpw;

    return-void
.end method

.method public final N()Lfst;
    .locals 1

    iget-object v0, p0, Lgpr;->ag:Lfst;

    return-object v0
.end method

.method public final O()Lcom/google/android/gms/plus/circles/AddToCircleConsentData;
    .locals 1

    iget-object v0, p0, Lgpr;->af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    return-object v0
.end method

.method public final P()Z
    .locals 1

    iget-object v0, p0, Lgpr;->af:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Q()Z
    .locals 1

    iget-object v0, p0, Lgpr;->ae:Lcom/google/android/gms/plus/model/posts/Settings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final R()Z
    .locals 1

    iget-object v0, p0, Lgpr;->ag:Lfst;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final S()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lgpr;->ai:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final T()Z
    .locals 1

    iget-object v0, p0, Lgpr;->ai:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final U()V
    .locals 2

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    iget-object v1, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-static {v0, v1}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbov;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpr;->Z:[Ljava/lang/String;

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    return-void
.end method

.method public final V()Z
    .locals 1

    iget-object v0, p0, Lgpr;->ae:Lcom/google/android/gms/plus/model/posts/Settings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lftx;
    .locals 1

    iget-object v0, p0, Lgpr;->d:Lftx;

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    instance-of v0, p1, Lgpw;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lgpw;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast p1, Lgpw;

    iput-object p1, p0, Lgpr;->h:Lgpw;

    return-void
.end method

.method public final a(Lbbo;Landroid/os/ParcelFileDescriptor;)V
    .locals 2

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    :try_start_0
    invoke-static {p2}, Lfba;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lgoo;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lgpr;->ai:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    iget-object v1, p0, Lgpr;->ai:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lgpw;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    invoke-static {p2}, Lbpm;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 1

    iget-boolean v0, p0, Lgpr;->ac:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0, p1, p2}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/model/posts/Post;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgpr;->ac:Z

    return-void
.end method

.method public final a(Lbbo;Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 1

    iput-object p2, p0, Lgpr;->ae:Lcom/google/android/gms/plus/model/posts/Settings;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0, p1, p2}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/model/posts/Settings;)V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;Lfeb;)V
    .locals 5

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    :cond_0
    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpr;->h:Lgpw;

    sget-object v1, Lgpr;->b:Lbbo;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {p2}, Lfeb;->a()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    new-instance v3, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {p2, v0}, Lfeb;->b(I)Lfea;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(Lfea;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Lfeb;->b()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgpr;->h:Lgpw;

    sget-object v1, Lbbo;->a:Lbbo;

    iget-object v2, p0, Lgpr;->h:Lgpw;

    invoke-interface {v2}, Lgpw;->f()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p2}, Lfeb;->b()V

    throw v0
.end method

.method public final a(Lbbo;Lfed;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lfed;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lfed;->b(I)Lfec;

    move-result-object v0

    invoke-interface {v0}, Lfec;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpr;->ah:Ljava/lang/String;

    iget-object v0, p0, Lgpr;->ah:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v1, p0, Lgpr;->ah:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2, v2}, Lfaj;->a(Lfas;Ljava/lang/String;II)V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;Lfst;)V
    .locals 1

    iput-object p2, p0, Lgpr;->ag:Lfst;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0, p1, p2}, Lgpw;->a(Lbbo;Lfst;)V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lgpr;->aj:Z

    iput-object v0, p0, Lgpr;->ak:Ljava/lang/String;

    invoke-virtual {p1}, Lbbo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-direct {v0, p2, p3}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lgpr;->h:Lgpw;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgpr;->h:Lgpw;

    invoke-interface {v1, p1, v0}, Lgpw;->a(Lbbo;Lcom/google/android/gms/plus/sharebox/Circle;)V

    :cond_1
    return-void
.end method

.method public final a(Lbbo;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lgpr;->am:Ljava/lang/String;

    iput-object v0, p0, Lgpr;->an:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0, p1, p2, p3}, Lgpw;->a(Lbbo;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Lbbo;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lgpr;->a(Lbbo;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    sget-object v0, Lbda;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, p1, v0}, Lgpr;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lgpr;->J()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgpr;->aa:Ljava/lang/String;

    iget-object v2, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lbms;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    new-instance v1, Lgpt;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgpt;-><init>(B)V

    iput-object p1, v1, Lgpt;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p2, v1, Lgpt;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1}, Lgpt;->a()Lgps;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lgpr;->J()Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lbmt;

    invoke-direct {v1, v0}, Lbmt;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lgpr;->aa:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbmt;->b(Ljava/lang/String;)Lbmt;

    move-result-object v1

    invoke-virtual {v1, p1}, Lbmt;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    if-nez p2, :cond_1

    sget-object p2, Lbda;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :cond_1
    invoke-virtual {v1, p2}, Lbmt;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lbmt;

    move-result-object v1

    iget-object v2, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbmt;->a(Ljava/lang/String;)Lbmt;

    move-result-object v1

    if-eqz p3, :cond_2

    invoke-virtual {v1, p3}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lbmt;

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {v1, p4}, Lbmt;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lbmt;

    :cond_3
    invoke-static {v0, v1}, Lbms;->a(Landroid/content/Context;Lbmt;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    new-instance v1, Lgpt;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgpt;-><init>(B)V

    iput-object p2, v1, Lgpt;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p1, v1, Lgpt;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p3, v1, Lgpt;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iput-object p4, v1, Lgpt;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v1}, Lgpt;->a()Lgps;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 2

    iget-boolean v0, p0, Lgpr;->ac:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "One post at a time please"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpr;->ac:Z

    iput-object p1, p0, Lgpr;->ad:Lcom/google/android/gms/plus/model/posts/Post;

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpr;->e:Lgpv;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lgpv;->b_(Landroid/os/Bundle;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    iget-boolean v0, p0, Lgpr;->al:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Add people to circle request already initiated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v1, p0, Lgpr;->g:Lgpu;

    invoke-virtual {v0, v1}, Lfaj;->a(Lfar;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpr;->al:Z

    iput-object p1, p0, Lgpr;->am:Ljava/lang/String;

    iput-object p2, p0, Lgpr;->an:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lgpr;->W()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a_(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgpr;->q()V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgpr;->aa:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lgpr;->J()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgpr;->aa:Ljava/lang/String;

    iget-object v2, p0, Lgpr;->h:Lgpw;

    invoke-interface {v2}, Lgpw;->j()Lgpx;

    move-result-object v2

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lgpr;->Y:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lbms;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgpr;->ao:Ljava/util/ArrayList;

    new-instance v1, Lgpt;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lgpt;-><init>(B)V

    iput-object p1, v1, Lgpt;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p2, v1, Lgpt;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1}, Lgpt;->a()Lgps;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lgpr;->aj:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Create circle request already initiated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v1, p0, Lgpr;->g:Lgpu;

    invoke-virtual {v0, v1}, Lfaj;->a(Lfar;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgpr;->aj:Z

    iput-object p1, p0, Lgpr;->ak:Ljava/lang/String;

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v1, p0, Lgpr;->aa:Ljava/lang/String;

    iget-object v2, p0, Lgpr;->i:Lgpx;

    invoke-virtual {v2}, Lgpx;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lgpr;->ak:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2, v3}, Lfaj;->a(Lfam;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgpr;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->a()V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 7

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0}, Lgpw;->j()Lgpx;

    move-result-object v0

    iput-object v0, p0, Lgpr;->i:Lgpx;

    iget-object v0, p0, Lgpr;->h:Lgpw;

    invoke-interface {v0}, Lgpw;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpr;->Y:Ljava/lang/String;

    iget-object v0, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-static {v1, v0}, Lbov;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbov;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgpr;->Z:[Ljava/lang/String;

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->q:Landroid/os/Bundle;

    const-string v3, "specified_account_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lgpr;->Y:Ljava/lang/String;

    iget-object v4, p0, Lgpr;->Z:[Ljava/lang/String;

    invoke-static {v1, v0, v3, v4}, Lgpq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lfwa;

    invoke-direct {v3, v1}, Lfwa;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lgpr;->Y:Ljava/lang/String;

    iput-object v4, v3, Lfwa;->c:Ljava/lang/String;

    iget-object v4, p0, Lgpr;->h:Lgpw;

    invoke-interface {v4}, Lgpw;->i()Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v4

    iput-object v4, v3, Lfwa;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iput-object v0, v3, Lfwa;->a:Ljava/lang/String;

    iget-object v0, p0, Lgpr;->i:Lgpx;

    iget-object v0, v0, Lgpx;->m:Ljava/lang/String;

    iput-object v0, v3, Lfwa;->e:Ljava/lang/String;

    sget-object v0, Lgpr;->a:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Lfwa;->a([Ljava/lang/String;)Lfwa;

    move-result-object v0

    iget-object v3, p0, Lgpr;->i:Lgpx;

    iget-object v3, v3, Lgpx;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lgpq;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-array v3, v2, [Ljava/lang/String;

    iput-object v3, v0, Lfwa;->d:[Ljava/lang/String;

    :cond_2
    iget-object v3, p0, Lgpr;->d:Lftx;

    if-nez v3, :cond_3

    new-instance v3, Lgpv;

    invoke-direct {v3, p0, v2}, Lgpv;-><init>(Lgpr;B)V

    iput-object v3, p0, Lgpr;->e:Lgpv;

    iget-object v3, p0, Lgpr;->c:Lftz;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lfwa;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v5, p0, Lgpr;->e:Lgpv;

    iget-object v6, p0, Lgpr;->e:Lgpv;

    invoke-interface {v3, v4, v0, v5, v6}, Lftz;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lbbr;Lbbs;)Lftx;

    move-result-object v0

    iput-object v0, p0, Lgpr;->d:Lftx;

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->a()V

    :cond_3
    iget-object v0, p0, Lgpr;->f:Lfaj;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->C:Lo;

    invoke-virtual {v0}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-static {v0, v3}, Lbbv;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iget-object v3, p0, Lgpr;->i:Lgpx;

    iget-object v3, v3, Lgpx;->m:Ljava/lang/String;

    if-eqz v3, :cond_5

    :try_start_0
    iget-object v3, p0, Lgpr;->i:Lgpx;

    iget-object v3, v3, Lgpx;->m:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v4, v0

    :goto_2
    new-instance v0, Lgpu;

    invoke-direct {v0, p0, v2}, Lgpu;-><init>(Lgpr;B)V

    iput-object v0, p0, Lgpr;->g:Lgpu;

    iget-object v0, p0, Lgpr;->c:Lftz;

    iget-object v2, p0, Lgpr;->g:Lgpu;

    iget-object v3, p0, Lgpr;->g:Lgpu;

    iget-object v5, p0, Lgpr;->Y:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lftz;->a(Landroid/content/Context;Lbbr;Lbbs;ILjava/lang/String;)Lfaj;

    move-result-object v0

    iput-object v0, p0, Lgpr;->f:Lfaj;

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0x64

    goto :goto_1

    :catch_0
    move-exception v3

    :cond_5
    move v4, v0

    goto :goto_2
.end method

.method public final y()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->y()V

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgpr;->d:Lftx;

    invoke-interface {v0}, Lftx;->b()V

    :cond_1
    iput-object v1, p0, Lgpr;->d:Lftx;

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d_()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lgpr;->f:Lfaj;

    iget-object v0, v0, Lfaj;->a:Lfch;

    invoke-virtual {v0}, Lfch;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lgpr;->f:Lfaj;

    invoke-virtual {v0}, Lfaj;->b()V

    :cond_3
    iput-object v1, p0, Lgpr;->f:Lfaj;

    iput-object v1, p0, Lgpr;->aa:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lgpr;->ab:I

    return-void
.end method
