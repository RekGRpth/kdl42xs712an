.class public final Lflq;
.super Lbje;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Lflo;

.field private final h:Lfll;

.field private final i:Ljava/lang/Object;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lflo;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lbje;-><init>(Landroid/content/Context;Lbbr;Lbbs;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflq;->f:Ljava/lang/String;

    invoke-static {p2}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflo;

    iput-object v0, p0, Lflq;->g:Lflo;

    iget-object v0, p0, Lflq;->g:Lflo;

    invoke-virtual {v0, p0}, Lflo;->a(Lflq;)V

    new-instance v0, Lfll;

    invoke-direct {v0}, Lfll;-><init>()V

    iput-object v0, p0, Lflq;->h:Lfll;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lflq;->i:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflq;->j:Z

    return-void
.end method

.method private b(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lflq;->h:Lfll;

    iget-object v1, v0, Lfll;->a:Ljava/util/ArrayList;

    new-instance v2, Lflm;

    invoke-direct {v2, p1, p2, v3}, Lflm;-><init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;B)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, v0, Lfll;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, v0, Lfll;->b:I

    if-le v1, v2, :cond_0

    iget-object v1, v0, Lfll;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private k()V
    .locals 7

    iget-boolean v0, p0, Lflq;->j:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lbiq;->a(Z)V

    iget-object v0, p0, Lflq;->h:Lfll;

    iget-object v0, v0, Lfll;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lflq;->h:Lfll;

    iget-object v1, v1, Lfll;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflm;

    iget-object v1, v0, Lflm;->c:Lfle;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lflq;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lfli;

    iget-object v5, p0, Lflq;->f:Ljava/lang/String;

    iget-object v6, v0, Lflm;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v0, v0, Lflm;->c:Lfle;

    invoke-static {v0}, Lizs;->a(Lizs;)[B

    move-result-object v0

    invoke-interface {v1, v5, v6, v0}, Lfli;->a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlayLoggerImpl"

    const-string v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, v0, Lflm;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lflm;->b:Lcom/google/android/gms/playlog/internal/LogEvent;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, Lflq;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lfli;

    iget-object v5, p0, Lflq;->f:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lfli;->a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_4
    iget-object v1, v0, Lflm;->a:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v0, v0, Lflm;->b:Lcom/google/android/gms/playlog/internal/LogEvent;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lflq;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfli;

    iget-object v1, p0, Lflq;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lfli;->a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Ljava/util/List;)V

    :cond_6
    iget-object v0, p0, Lflq;->h:Lfll;

    iget-object v0, v0, Lfll;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lflj;->a(Landroid/os/IBinder;)Lfli;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lbjy;Lbjj;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x41fe88

    iget-object v2, p0, Lbje;->b_:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lbjy;->g(Lbjv;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    .locals 3

    iget-object v1, p0, Lflq;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lflq;->j:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lflq;->b(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lflq;->k()V

    invoke-virtual {p0}, Lflq;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lfli;

    iget-object v2, p0, Lflq;->f:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lfli;->a(Ljava/lang/String;Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lflq;->b(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lflq;->b(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/playlog/internal/LogEvent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final a(Z)V
    .locals 2

    iget-object v1, p0, Lflq;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lflq;->j:Z

    iput-boolean p1, p0, Lflq;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflq;->j:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lflq;->k()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected final c_()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method

.method public final e()V
    .locals 3

    iget-object v1, p0, Lflq;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflq;->g:Lflo;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lflo;->a(Z)V

    invoke-virtual {p0}, Lflq;->a()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()V
    .locals 3

    iget-object v1, p0, Lflq;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lflq;->g:Lflo;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflo;->a(Z)V

    invoke-virtual {p0}, Lflq;->b()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
