.class public final Lbgx;
.super Lbgy;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lbgo;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lbgy;-><init>(Lbgo;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbgx;->d:Ljava/util/HashSet;

    iput-object p2, p0, Lbgx;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget-object v0, p0, Lbgx;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v0

    iget-object v1, p0, Lbgx;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lbgx;->d:Ljava/util/HashSet;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lbgx;->b:Lbgo;

    iget-object v4, v0, Lbgo;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v5, p0, Lbgx;->c:Ljava/lang/String;

    iget-object v0, p0, Lbgx;->b:Lbgo;

    instance-of v6, v0, Lbgw;

    const/4 v1, 0x0

    iget-object v0, p0, Lbgx;->b:Lbgo;

    invoke-virtual {v0}, Lbgo;->a()I

    move-result v7

    :goto_1
    if-ge v1, v7, :cond_3

    if-eqz v6, :cond_2

    iget-object v0, p0, Lbgx;->b:Lbgo;

    check-cast v0, Lbgw;

    invoke-virtual {v0, v1}, Lbgw;->b(I)I

    move-result v0

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v8

    invoke-virtual {v4, v5, v0, v8}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final b(I)I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbgx;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return p1

    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {p0}, Lbgx;->a()I

    move-result v1

    if-lt p1, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v0

    :goto_1
    if-ge v1, p1, :cond_5

    iget-object v2, p0, Lbgx;->d:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lbgx;->b:Lbgo;

    invoke-virtual {v1}, Lbgo;->a()I

    move-result v1

    :goto_2
    if-ge p1, v1, :cond_7

    iget-object v2, p0, Lbgx;->d:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    if-lez v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_6
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_7
    const/4 p1, -0x1

    goto :goto_0
.end method
