.class public final Lezs;
.super Landroid/widget/ImageButton;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/animation/AlphaAnimation;

.field private final b:Landroid/view/animation/AlphaAnimation;

.field private c:Landroid/os/Handler;

.field private d:Ljava/lang/Runnable;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x2

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lezs;->c:Landroid/os/Handler;

    new-instance v0, Lezt;

    invoke-direct {v0, p0}, Lezt;-><init>(Lezs;)V

    iput-object v0, p0, Lezs;->d:Ljava/lang/Runnable;

    iput-boolean v3, p0, Lezs;->e:Z

    const v0, 0x7f0200fe    # com.google.android.gms.R.drawable.ic_compass

    invoke-virtual {p0, v0}, Lezs;->setImageResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0, v0}, Lezs;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f02023d    # com.google.android.gms.R.drawable.transparent_button_background

    invoke-virtual {p0, v0}, Lezs;->setBackgroundResource(I)V

    iget-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    iget-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    iget-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v5}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    iget-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Lezu;

    invoke-direct {v1, p0}, Lezu;-><init>(Lezs;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v5}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    return-void
.end method

.method static synthetic a(Lezs;)Landroid/view/animation/AlphaAnimation;
    .locals 1

    iget-object v0, p0, Lezs;->a:Landroid/view/animation/AlphaAnimation;

    return-object v0
.end method

.method static synthetic b(Lezs;)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lezs;->e:Z

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lezs;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lezs;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lezs;->setClickable(Z)V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->reset()V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setStartTime(J)V

    iget-object v0, p0, Lezs;->b:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p0, v0}, Lezs;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lezs;->e:Z

    :cond_0
    iget-object v0, p0, Lezs;->c:Landroid/os/Handler;

    iget-object v1, p0, Lezs;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lezs;->c:Landroid/os/Handler;

    iget-object v1, p0, Lezs;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x76c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
