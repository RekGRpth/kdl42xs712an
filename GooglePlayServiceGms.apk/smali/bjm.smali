.class public final Lbjm;
.super Lbjg;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final c:Landroid/os/Bundle;

.field public final d:Landroid/os/IBinder;

.field final synthetic e:Lbje;


# direct methods
.method public constructor <init>(Lbje;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    iput-object p1, p0, Lbjm;->e:Lbje;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbjg;-><init>(Lbje;Ljava/lang/Object;)V

    iput p2, p0, Lbjm;->a:I

    iput-object p3, p0, Lbjm;->d:Landroid/os/IBinder;

    iput-object p4, p0, Lbjm;->c:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0, v5}, Lbje;->a(Lbje;I)I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lbjm;->a:I

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lbjm;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbjm;->c:Landroid/os/Bundle;

    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :goto_1
    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-static {v2}, Lbje;->d(Lbje;)Lbjk;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-static {v2}, Lbje;->e(Lbje;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lbjq;->a(Landroid/content/Context;)Lbjq;

    move-result-object v2

    iget-object v3, p0, Lbjm;->e:Lbje;

    invoke-virtual {v3}, Lbje;->b_()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lbjm;->e:Lbje;

    invoke-static {v4}, Lbje;->d(Lbje;)Lbjk;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lbjq;->b(Ljava/lang/String;Lbjk;)V

    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-static {v2}, Lbje;->f(Lbje;)Lbjk;

    :cond_1
    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-static {v2, v5}, Lbje;->a(Lbje;I)I

    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-static {v2, v1}, Lbje;->a(Lbje;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v1, p0, Lbjm;->e:Lbje;

    invoke-static {v1}, Lbje;->a(Lbje;)Lbjn;

    move-result-object v1

    new-instance v2, Lbbo;

    iget v3, p0, Lbjm;->a:I

    invoke-direct {v2, v3, v0}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lbjn;->a(Lbbo;)V

    goto :goto_0

    :sswitch_0
    :try_start_0
    iget-object v0, p0, Lbjm;->d:Landroid/os/IBinder;

    invoke-interface {v0}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-virtual {v2}, Lbje;->c_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbjm;->e:Lbje;

    iget-object v2, p0, Lbjm;->e:Lbje;

    iget-object v3, p0, Lbjm;->d:Landroid/os/IBinder;

    invoke-virtual {v2, v3}, Lbje;->a(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v2

    invoke-static {v0, v2}, Lbje;->a(Lbje;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0}, Lbje;->c(Lbje;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbjm;->e:Lbje;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbje;->a(Lbje;I)I

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0}, Lbje;->a(Lbje;)Lbjn;

    move-result-object v0

    iget-object v2, v0, Lbjn;->b:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v3, v0, Lbjn;->a:Lbjp;

    invoke-interface {v3}, Lbjp;->e_()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Lbjn;->a(Landroid/os/Bundle;)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    :cond_2
    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0}, Lbje;->e(Lbje;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbjq;->a(Landroid/content/Context;)Lbjq;

    move-result-object v0

    iget-object v2, p0, Lbjm;->e:Lbje;

    invoke-virtual {v2}, Lbje;->b_()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbjm;->e:Lbje;

    invoke-static {v3}, Lbje;->d(Lbje;)Lbjk;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbjq;->b(Ljava/lang/String;Lbjk;)V

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0}, Lbje;->f(Lbje;)Lbjk;

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0, v5}, Lbje;->a(Lbje;I)I

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0, v1}, Lbje;->a(Lbje;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0}, Lbje;->a(Lbje;)Lbjn;

    move-result-object v0

    new-instance v2, Lbbo;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v1}, Lbbo;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v2}, Lbjn;->a(Lbbo;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lbjm;->e:Lbje;

    invoke-static {v0, v5}, Lbje;->a(Lbje;I)I

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A fatal developer error has occurred. Check the logs for further information."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method protected final r_()V
    .locals 0

    return-void
.end method
