.class public final Ldfe;
.super Lbgq;
.source "SourceFile"

# interfaces
.implements Ldfb;


# instance fields
.field private final c:I

.field private final d:Lcom/google/android/gms/games/Game;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;II)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lbgq;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput p3, p0, Ldfe;->c:I

    new-instance v0, Lcom/google/android/gms/games/GameRef;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/GameRef;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput-object v0, p0, Ldfe;->d:Lcom/google/android/gms/games/Game;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "external_leaderboard_id"

    invoke-virtual {p0, v0}, Ldfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0, p1}, Ldfe;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0}, Ldfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    const-string v0, "board_icon_image_uri"

    invoke-virtual {p0, v0}, Ldfe;->g(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "board_icon_image_url"

    invoke-virtual {p0, v0}, Ldfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Ldfd;->a(Ldfb;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final synthetic f()Ljava/lang/Object;
    .locals 1

    new-instance v0, Ldfd;

    invoke-direct {v0, p0}, Ldfd;-><init>(Ldfb;)V

    return-object v0
.end method

.method public final g()I
    .locals 1

    const-string v0, "score_order"

    invoke-virtual {p0, v0}, Ldfe;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    iget v0, p0, Ldfe;->c:I

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Ldfe;->c:I

    if-ge v0, v2, :cond_0

    new-instance v2, Ldfn;

    iget-object v3, p0, Ldfe;->a_:Lcom/google/android/gms/common/data/DataHolder;

    iget v4, p0, Ldfe;->b:I

    add-int/2addr v4, v0

    invoke-direct {v2, v3, v4}, Ldfn;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final hashCode()I
    .locals 1

    invoke-static {p0}, Ldfd;->a(Ldfb;)I

    move-result v0

    return v0
.end method

.method public final i()Lcom/google/android/gms/games/Game;
    .locals 1

    iget-object v0, p0, Ldfe;->d:Lcom/google/android/gms/games/Game;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Ldfd;->b(Ldfb;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
