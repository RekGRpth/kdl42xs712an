.class public final Lavt;
.super Lary;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;)V
    .locals 0

    iput-object p1, p0, Lavt;->a:Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;

    invoke-direct {p0}, Lary;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;B)V
    .locals 0

    invoke-direct {p0, p1}, Lavt;-><init>(Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;)Landroid/app/PendingIntent;
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lavu;

    iget-object v1, p0, Lavt;->a:Lcom/google/android/gms/auth/setup/workflow/AccountSetupWorkflowService;

    invoke-direct {v0, v1}, Lavu;-><init>(Landroid/content/Context;)V

    const-string v1, "Setup a Google Account"

    new-instance v2, Laoy;

    iget-object v3, v0, Lavu;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Laoy;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    iget-object v5, v2, Laoy;->c:Ljava/lang/String;

    iget v2, v2, Laoy;->e:I

    invoke-direct {v4, v5, v2, v3, v3}, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    new-instance v2, Laqw;

    iget-object v3, v0, Lavu;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Laqw;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;

    invoke-direct {v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;-><init>(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)V

    invoke-virtual {v2, v3}, Laqw;->a(Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Lauj;

    iget-object v4, v0, Lavu;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lauj;-><init>(Landroid/content/Context;)V

    const-string v4, "SID"

    invoke-virtual {v3, v4}, Lauj;->b(Ljava/lang/String;)Lauj;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->f:Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {v3, v4}, Lauj;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lauj;

    move-result-object v3

    sget-object v4, Laso;->b:Laso;

    invoke-virtual {v3, v4}, Lauj;->a(Laso;)Lauj;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->d()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lauj;->a(Landroid/os/Bundle;)Lauj;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;->b:Ljava/lang/String;

    iget-object v4, v3, Lauj;->a:Landroid/content/Intent;

    const-string v5, "url"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/delegate/SetupAccountWorkflowRequest;->b()Z

    move-result v2

    iget-object v4, v3, Lauj;->a:Landroid/content/Intent;

    const-string v5, "backup"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v2, v3, Lauj;->a:Landroid/content/Intent;

    const-string v4, "title"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, v3, Lauj;->a:Landroid/content/Intent;

    iget-object v2, v0, Lavu;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v6}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v0, Lavu;->a:Landroid/content/Context;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v6, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method
