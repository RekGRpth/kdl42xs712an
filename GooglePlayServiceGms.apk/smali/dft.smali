.class public final Ldft;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Ldft;->a:Z

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ldhr;
    .locals 2

    new-instance v0, Ldhr;

    invoke-direct {v0}, Ldhr;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Ldhr;->a:Z

    iput-object p1, v0, Ldhr;->b:Ljava/lang/String;

    iput-object p0, v0, Ldhr;->d:Ljava/lang/String;

    const-string v1, "4325000"

    iput-object v1, v0, Ldhr;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {p0, p1, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 2

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v1

    iput-object v1, v0, Ldho;->a:Ldhr;

    new-instance v1, Ldhp;

    invoke-direct {v1}, Ldhp;-><init>()V

    iput p3, v1, Ldhp;->a:I

    iput-boolean p4, v1, Ldhp;->b:Z

    iput-object v1, v0, Ldho;->h:Ldhp;

    invoke-static {v0}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p2, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    invoke-static {p1, p2}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v1

    iput-object v1, v0, Ldho;->a:Ldhr;

    new-instance v1, Ldhi;

    invoke-direct {v1}, Ldhi;-><init>()V

    iput p4, v1, Ldhi;->a:I

    iput-object v1, v0, Ldho;->c:Ldhi;

    invoke-static {v0}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p3, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    invoke-static {p1, p2}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v1

    iput-object v1, v0, Ldho;->a:Ldhr;

    iget-object v1, v0, Ldho;->a:Ldhr;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ldhr;->e:Ljava/lang/String;

    new-instance v1, Ldht;

    invoke-direct {v1}, Ldht;-><init>()V

    iput p5, v1, Ldht;->c:I

    iput-object v1, v0, Ldho;->b:Ldht;

    invoke-static {v0}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p3, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 3

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    invoke-static {p1, p2}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v1

    iput-object v1, v0, Ldho;->a:Ldhr;

    new-instance v1, Ldht;

    invoke-direct {v1}, Ldht;-><init>()V

    iput-object v1, v0, Ldho;->b:Ldht;

    iget-object v1, v0, Ldho;->b:Ldht;

    new-instance v2, Ldgx;

    invoke-direct {v2}, Ldgx;-><init>()V

    iput p6, v2, Ldgx;->a:I

    iput-wide p4, v2, Ldgx;->b:J

    iput-object v2, v1, Ldht;->d:Ldgx;

    invoke-static {v0}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p3, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZI)V
    .locals 3

    new-instance v0, Ldho;

    invoke-direct {v0}, Ldho;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v1

    iput-object v1, v0, Ldho;->a:Ldhr;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    iput-object v1, v0, Ldho;->i:Ldgy;

    iget-object v1, v0, Ldho;->i:Ldgy;

    iput-object p2, v1, Ldgy;->a:Ljava/lang/String;

    iget-object v1, v0, Ldho;->i:Ldgy;

    iput-object p3, v1, Ldgy;->b:Ljava/lang/String;

    iget-object v1, v0, Ldho;->i:Ldgy;

    iput-wide p4, v1, Ldgy;->c:J

    iget-object v1, v0, Ldho;->i:Ldgy;

    iput-boolean p6, v1, Ldgy;->d:Z

    iget-object v1, v0, Ldho;->i:Ldgy;

    iput p7, v1, Ldgy;->e:I

    invoke-static {v0}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p1, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6

    new-instance v2, Ldho;

    invoke-direct {v2}, Ldho;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Ldft;->a(Ljava/lang/String;Ljava/lang/String;)Ldhr;

    move-result-object v0

    iput-object v0, v2, Ldho;->a:Ldhr;

    if-nez p2, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v3, Ldhh;

    invoke-direct {v3}, Ldhh;-><init>()V

    const/4 v4, 0x1

    iput v4, v3, Ldhh;->a:I

    new-instance v4, Ldgw;

    invoke-direct {v4}, Ldgw;-><init>()V

    if-eqz v0, :cond_0

    array-length v5, v0

    if-lez v5, :cond_0

    iput-object v0, v4, Ldgw;->b:[Ljava/lang/String;

    :cond_0
    if-ltz v1, :cond_1

    iput v1, v4, Ldgw;->c:I

    :cond_1
    iput-object v4, v3, Ldhh;->b:Ldgw;

    iput-object v3, v2, Ldho;->f:Ldhh;

    invoke-static {v2}, Ldho;->a(Lizs;)[B

    move-result-object v0

    invoke-static {p0, p1, v0}, Ldft;->a(Landroid/content/Context;Ljava/lang/String;[B)V

    return-void

    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;[B)V
    .locals 5

    sget-boolean v0, Ldft;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    new-instance v0, Lfko;

    const/4 v3, 0x5

    invoke-direct {v0, p0, v3, p1}, Lfko;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v3, p2, v4}, Lfko;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0}, Lfko;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method
