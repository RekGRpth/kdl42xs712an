.class public final Lbuc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lbvr;

.field private final b:Lcll;

.field private final c:Lcfz;

.field private final d:Lbrl;

.field private final e:Landroid/content/Context;

.field private final f:Lbvp;


# direct methods
.method public constructor <init>(Lcoy;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcoy;->j()Lcll;

    move-result-object v0

    iput-object v0, p0, Lbuc;->b:Lcll;

    invoke-virtual {p1}, Lcoy;->f()Lcfz;

    move-result-object v0

    iput-object v0, p0, Lbuc;->c:Lcfz;

    invoke-virtual {p1}, Lcoy;->g()Lbrl;

    move-result-object v0

    iput-object v0, p0, Lbuc;->d:Lbrl;

    invoke-virtual {p1}, Lcoy;->c()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lbuc;->e:Landroid/content/Context;

    new-instance v0, Lbvr;

    invoke-direct {v0, p1}, Lbvr;-><init>(Lcoy;)V

    iput-object v0, p0, Lbuc;->a:Lbvr;

    new-instance v0, Lbvp;

    invoke-direct {v0, p1}, Lbvp;-><init>(Lcoy;)V

    iput-object v0, p0, Lbuc;->f:Lbvp;

    return-void
.end method


# virtual methods
.method public final a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 3

    iget-object v0, p0, Lbuc;->d:Lbrl;

    iget-object v1, p1, Lbsp;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v0, v1, p3}, Lbrl;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    const-string v0, "application/vnd.google-apps.folder"

    sget-object v1, Lcle;->c:Lcje;

    invoke-virtual {p2, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method may not be used to create folders."

    invoke-static {v0, v1}, Lbkm;->b(ZLjava/lang/Object;)V

    sget-object v0, Lcle;->b:Lcje;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcle;->b:Lcje;

    iget-object v1, p0, Lbuc;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b001d    # com.google.android.gms.R.string.drive_create_file_default_title

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lcle;->c:Lcje;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcle;->c:Lcje;

    const-string v1, "application/octet-stream"

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcje;Ljava/lang/Object;)V

    :cond_1
    iget-object v0, p0, Lbuc;->d:Lbrl;

    invoke-interface {v0, p1, p3, p2, p4}, Lbrl;->a(Lbsp;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lbsp;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 8

    const/4 v6, 0x7

    const/4 v1, 0x0

    const/4 v5, 0x0

    iget-object v2, p1, Lbsp;->a:Lcfc;

    if-nez p3, :cond_3

    move-object v0, v1

    :goto_0
    invoke-virtual {p1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    const-string v4, "https://www.googleapis.com/auth/drive.appdata"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lbth;->a:Lbth;

    invoke-static {v3}, Lbti;->a(Lbth;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lbuc;->f:Lbvp;

    invoke-virtual {v3, p1}, Lbvp;->a(Lbsp;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :try_start_1
    iget-object v3, p0, Lbuc;->b:Lcll;

    invoke-virtual {p1}, Lbsp;->a()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    invoke-interface {v3, v4, p2, v0, p1}, Lcll;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Ljava/lang/String;Lbsp;)Lclk;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    iget-object v0, p0, Lbuc;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->c()V

    :try_start_2
    iget-object v0, p0, Lbuc;->a:Lbvr;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lbvr;->a(Lcfc;Lclk;Ljava/lang/Boolean;Z)V
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v2}, Lbsp;->a(Lcfc;)Lbsp;

    move-result-object v0

    iget-object v4, p0, Lbuc;->c:Lcfz;

    invoke-interface {v3}, Lclk;->g()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    iget-object v4, p0, Lbuc;->c:Lcfz;

    iget-wide v5, p1, Lbsp;->b:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Lcfz;->a(Lcfp;Ljava/util/Set;)V

    iget-object v0, p0, Lbuc;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->f()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lbuc;->c:Lcfz;

    invoke-interface {v0}, Lcfz;->d()V

    iget-object v0, v2, Lcfc;->a:Ljava/lang/String;

    invoke-interface {v3}, Lclk;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3}, Lclk;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbkm;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "application/vnd.google-apps.folder"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lbuc;->c:Lcfz;

    invoke-interface {v2, v0}, Lcfz;->a(Ljava/lang/String;)Lcfc;

    move-result-object v2

    invoke-static {v4}, Lbvi;->a(Ljava/lang/String;)Lbvi;

    move-result-object v0

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0}, Lbvi;->a()Lbvh;

    move-result-object v5

    invoke-static {}, Lbvh;->b()Lbvh;

    move-result-object v6

    invoke-static {v5, v6}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v0}, Lbvi;->b()Lbvh;

    move-result-object v0

    invoke-static {}, Lbvh;->c()Lbvh;

    move-result-object v5

    invoke-static {v0, v5}, Lbvh;->a(Lbvh;Lbvh;)Lbvh;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvh;

    new-instance v5, Lbvd;

    invoke-virtual {v0}, Lbvh;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Lbvd;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbuc;->c:Lcfz;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v0, v2, v5, v6, v7}, Lcfz;->a(Lcfc;Lbuv;J)Lcgb;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Lcgb;->a(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0}, Lcgb;->k()V

    goto :goto_1

    :cond_3
    invoke-virtual {p3}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbkm;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lbrm;

    const-string v1, "Unable to create folder. Note that an active network connection is required to create folders."

    invoke-direct {v0, v6, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Lbrm;

    const-string v1, "Unable to create folder. Note that an active network connection is required to create folders."

    invoke-direct {v0, v6, v1, v5}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_2
    move-exception v0

    :try_start_4
    new-instance v0, Lbrm;

    const/16 v1, 0x8

    const-string v2, "Error creating folder."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbrm;-><init>(ILjava/lang/String;B)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbuc;->c:Lcfz;

    invoke-interface {v1}, Lcfz;->d()V

    throw v0

    :cond_4
    iget-object v0, p0, Lbuc;->c:Lcfz;

    invoke-interface {v3}, Lclk;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcfz;->a(Lbsp;Ljava/lang/String;)Lcfp;

    move-result-object v0

    invoke-virtual {v0}, Lcfp;->ac()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method
