.class public final Lfxv;
.super Lbgo;
.source "SourceFile"


# instance fields
.field private final b:Lbgr;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    invoke-direct {p0, p1}, Lbgo;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.IsSafeParcelable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbgr;

    sget-object v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->CREATOR:Lfwr;

    invoke-direct {v0, p1, v1}, Lbgr;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/os/Parcelable$Creator;)V

    iput-object v0, p0, Lfxv;->b:Lbgr;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lfxv;->b:Lbgr;

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lfxv;->b(I)Lfxl;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lfxl;
    .locals 2

    iget-object v0, p0, Lfxv;->b:Lbgr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxv;->b:Lbgr;

    invoke-virtual {v0, p1}, Lbgr;->b(I)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lfxl;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lfxb;

    iget-object v1, p0, Lfxv;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1}, Lfxb;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    goto :goto_0
.end method
