.class public final enum Lhim;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lhim;

.field public static final enum b:Lhim;

.field public static final enum c:Lhim;

.field public static final enum d:Lhim;

.field public static final enum e:Lhim;

.field private static final synthetic f:[Lhim;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lhim;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lhim;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhim;->a:Lhim;

    new-instance v0, Lhim;

    const-string v1, "AWAY"

    invoke-direct {v0, v1, v3}, Lhim;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhim;->b:Lhim;

    new-instance v0, Lhim;

    const-string v1, "EXTENDED_AWAY"

    invoke-direct {v0, v1, v4}, Lhim;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhim;->c:Lhim;

    new-instance v0, Lhim;

    const-string v1, "DND"

    invoke-direct {v0, v1, v5}, Lhim;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhim;->d:Lhim;

    new-instance v0, Lhim;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v6}, Lhim;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lhim;->e:Lhim;

    const/4 v0, 0x5

    new-array v0, v0, [Lhim;

    sget-object v1, Lhim;->a:Lhim;

    aput-object v1, v0, v2

    sget-object v1, Lhim;->b:Lhim;

    aput-object v1, v0, v3

    sget-object v1, Lhim;->c:Lhim;

    aput-object v1, v0, v4

    sget-object v1, Lhim;->d:Lhim;

    aput-object v1, v0, v5

    sget-object v1, Lhim;->e:Lhim;

    aput-object v1, v0, v6

    sput-object v0, Lhim;->f:[Lhim;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lhim;
    .locals 1

    const-class v0, Lhim;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lhim;

    return-object v0
.end method

.method public static values()[Lhim;
    .locals 1

    sget-object v0, Lhim;->f:[Lhim;

    invoke-virtual {v0}, [Lhim;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhim;

    return-object v0
.end method
