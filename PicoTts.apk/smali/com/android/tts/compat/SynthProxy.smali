.class public Lcom/android/tts/compat/SynthProxy;
.super Ljava/lang/Object;
.source "SynthProxy.java"


# static fields
.field private static final PICO_FILTER_GAIN:F = 5.0f

.field private static final PICO_FILTER_LOWSHELF_ATTENUATION:F = -18.0f

.field private static final PICO_FILTER_SHELF_SLOPE:F = 1.0f

.field private static final PICO_FILTER_TRANSITION_FREQ:F = 1100.0f

.field private static final TAG:Ljava/lang/String; = "SynthProxy"


# instance fields
.field private mJniData:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "ttscompat"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, p1}, Lcom/android/tts/compat/SynthProxy;->shouldApplyAudioFilter(Ljava/lang/String;)Z

    move-result v1

    const-string v0, "SynthProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "About to load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", applyFilter="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/android/tts/compat/SynthProxy;->native_setup(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/high16 v2, 0x40a00000    # 5.0f

    const/high16 v3, -0x3e700000    # -18.0f

    const v4, 0x44898000    # 1100.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/tts/compat/SynthProxy;->native_setLowShelf(ZFFFF)I

    return-void
.end method

.method private final native native_finalize(I)V
.end method

.method private final native native_getLanguage(I)[Ljava/lang/String;
.end method

.method private final native native_isLanguageAvailable(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private final native native_loadLanguage(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private final native native_setLanguage(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private final native native_setLowShelf(ZFFFF)I
.end method

.method private final native native_setProperty(ILjava/lang/String;Ljava/lang/String;)I
.end method

.method private final native native_setup(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private final native native_shutdown(I)V
.end method

.method private final native native_speak(ILjava/lang/String;Landroid/speech/tts/SynthesisCallback;)I
.end method

.method private final native native_stop(I)I
.end method

.method private final native native_stopSync(I)I
.end method

.method private shouldApplyAudioFilter(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pico"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected finalize()V
    .locals 2

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    if-eqz v0, :cond_0

    const-string v0, "SynthProxy"

    const-string v1, "SynthProxy finalized without being shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0}, Lcom/android/tts/compat/SynthProxy;->native_finalize(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    :cond_0
    return-void
.end method

.method public getLanguage()[Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0}, Lcom/android/tts/compat/SynthProxy;->native_getLanguage(I)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isLanguageAvailable(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/tts/compat/SynthProxy;->native_isLanguageAvailable(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public loadLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/tts/compat/SynthProxy;->native_loadLanguage(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public setConfig(Ljava/lang/String;)I
    .locals 2
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    const-string v1, "engineConfig"

    invoke-direct {p0, v0, v1, p1}, Lcom/android/tts/compat/SynthProxy;->native_setProperty(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public setLanguage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/tts/compat/SynthProxy;->native_setLanguage(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final setPitch(I)I
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    const-string v1, "pitch"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/tts/compat/SynthProxy;->native_setProperty(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final setSpeechRate(I)I
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    const-string v1, "rate"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/android/tts/compat/SynthProxy;->native_setProperty(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public shutdown()V
    .locals 1

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0}, Lcom/android/tts/compat/SynthProxy;->native_shutdown(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    return-void
.end method

.method public speak(Landroid/speech/tts/SynthesisRequest;Landroid/speech/tts/SynthesisCallback;)I
    .locals 2
    .param p1    # Landroid/speech/tts/SynthesisRequest;
    .param p2    # Landroid/speech/tts/SynthesisCallback;

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-virtual {p1}, Landroid/speech/tts/SynthesisRequest;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/android/tts/compat/SynthProxy;->native_speak(ILjava/lang/String;Landroid/speech/tts/SynthesisCallback;)I

    move-result v0

    return v0
.end method

.method public stop()I
    .locals 1

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0}, Lcom/android/tts/compat/SynthProxy;->native_stop(I)I

    move-result v0

    return v0
.end method

.method public stopSync()I
    .locals 1

    iget v0, p0, Lcom/android/tts/compat/SynthProxy;->mJniData:I

    invoke-direct {p0, v0}, Lcom/android/tts/compat/SynthProxy;->native_stopSync(I)I

    move-result v0

    return v0
.end method
