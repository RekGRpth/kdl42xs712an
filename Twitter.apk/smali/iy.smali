.class public Liy;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Liy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Z)V
    .locals 19

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/provider/az;->b()[Lcom/twitter/library/api/o;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v3, 0x0

    move/from16 v16, v3

    :goto_0
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    aget-object v3, v17, v16

    iget v4, v3, Lcom/twitter/library/api/o;->c:I

    invoke-static {v4}, Lcom/twitter/library/api/PromotedEvent;->a(I)Lcom/twitter/library/api/PromotedEvent;

    move-result-object v8

    iget-object v9, v3, Lcom/twitter/library/api/o;->b:Ljava/lang/String;

    iget-wide v10, v3, Lcom/twitter/library/api/o;->d:J

    iget-object v12, v3, Lcom/twitter/library/api/o;->e:Ljava/lang/String;

    iget-boolean v13, v3, Lcom/twitter/library/api/o;->f:Z

    const/4 v14, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-wide/from16 v5, p2

    move-object/from16 v7, p4

    move/from16 v15, p5

    invoke-static/range {v3 .. v15}, Lix;->a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;JLjava/lang/String;ZZZ)V

    add-int/lit8 v3, v16, 0x1

    move/from16 v16, v3

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p0}, Liy;->s()Lcom/twitter/library/service/p;

    move-result-object v4

    iget-object v0, p0, Liy;->l:Landroid/content/Context;

    iget-object v1, p0, Liy;->m:Lcom/twitter/library/network/aa;

    iget-wide v2, v4, Lcom/twitter/library/service/p;->c:J

    iget-object v4, v4, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {p0}, Liy;->t()Z

    move-result v5

    invoke-static/range {v0 .. v5}, Liy;->a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Z)V

    return-void
.end method
