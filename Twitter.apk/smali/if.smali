.class public final Lif;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final amplify_scrubber_control:I = 0x7f020003

.field public static final amplify_scrubber_control_normal:I = 0x7f020004

.field public static final common_signin_btn_icon_dark:I = 0x7f0200ab

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f0200ac

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f0200ad

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f0200ae

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f0200af

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f0200b0

.field public static final common_signin_btn_icon_focus_light:I = 0x7f0200b1

.field public static final common_signin_btn_icon_light:I = 0x7f0200b2

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f0200b3

.field public static final common_signin_btn_icon_normal_light:I = 0x7f0200b4

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f0200b5

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f0200b6

.field public static final common_signin_btn_text_dark:I = 0x7f0200b7

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f0200b8

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f0200b9

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f0200ba

.field public static final common_signin_btn_text_disabled_light:I = 0x7f0200bb

.field public static final common_signin_btn_text_focus_dark:I = 0x7f0200bc

.field public static final common_signin_btn_text_focus_light:I = 0x7f0200bd

.field public static final common_signin_btn_text_light:I = 0x7f0200be

.field public static final common_signin_btn_text_normal_dark:I = 0x7f0200bf

.field public static final common_signin_btn_text_normal_light:I = 0x7f0200c0

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f0200c1

.field public static final common_signin_btn_text_pressed_light:I = 0x7f0200c2

.field public static final fullscreen:I = 0x7f0200dd

.field public static final ic_badge_gov_default:I = 0x7f020124

.field public static final ic_badge_promoted_default:I = 0x7f020125

.field public static final ic_filters_crop_corner:I = 0x7f020187

.field public static final ic_launcher:I = 0x7f0201ce

.field public static final ic_media_pause:I = 0x7f0201d1

.field public static final ic_media_play:I = 0x7f0201d2

.field public static final ic_pending_default:I = 0x7f0201e4

.field public static final ic_plusone_medium_off_client:I = 0x7f0201f8

.field public static final ic_plusone_small_off_client:I = 0x7f0201f9

.field public static final ic_plusone_standard_off_client:I = 0x7f0201fa

.field public static final ic_plusone_tall_off_client:I = 0x7f0201fb

.field public static final ic_replay:I = 0x7f02021b

.field public static final progress_bg_holo_dark:I = 0x7f0202cf

.field public static final progress_horizontal:I = 0x7f0202d1

.field public static final progress_primary_holo_dark:I = 0x7f0202d2

.field public static final progress_secondary_holo_dark:I = 0x7f0202d3

.field public static final replay:I = 0x7f0202d4

.field public static final smallscreen:I = 0x7f0202e3
