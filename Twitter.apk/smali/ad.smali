.class public final Lad;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adUnitId:I = 0x1

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lad;->AdsAttrs:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lad;->MapAttrs:[I

    return-void

    :array_0
    .array-data 4
        0x7f010063    # com.twitter.android.R.attr.adSize
        0x7f010064    # com.twitter.android.R.attr.adUnitId
    .end array-data

    :array_1
    .array-data 4
        0x7f0100c1    # com.twitter.android.R.attr.mapType
        0x7f0100c2    # com.twitter.android.R.attr.cameraBearing
        0x7f0100c3    # com.twitter.android.R.attr.cameraTargetLat
        0x7f0100c4    # com.twitter.android.R.attr.cameraTargetLng
        0x7f0100c5    # com.twitter.android.R.attr.cameraTilt
        0x7f0100c6    # com.twitter.android.R.attr.cameraZoom
        0x7f0100c7    # com.twitter.android.R.attr.uiCompass
        0x7f0100c8    # com.twitter.android.R.attr.uiRotateGestures
        0x7f0100c9    # com.twitter.android.R.attr.uiScrollGestures
        0x7f0100ca    # com.twitter.android.R.attr.uiTiltGestures
        0x7f0100cb    # com.twitter.android.R.attr.uiZoomControls
        0x7f0100cc    # com.twitter.android.R.attr.uiZoomGestures
        0x7f0100cd    # com.twitter.android.R.attr.useViewLifecycle
        0x7f0100ce    # com.twitter.android.R.attr.zOrderOnTop
    .end array-data
.end method
