.class public Lgf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:D

.field private final b:D

.field private final c:D

.field private final d:D


# direct methods
.method public constructor <init>(FF)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v3, p2, p2

    const/high16 v4, 0x40800000    # 4.0f

    mul-float/2addr v4, p1

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lgf;->a:D

    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p2, v0

    float-to-double v0, v0

    iput-wide v0, p0, Lgf;->b:D

    iget-wide v0, p0, Lgf;->b:D

    iget-wide v2, p0, Lgf;->a:D

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgf;->c:D

    const-wide/high16 v0, -0x4000000000000000L    # -2.0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    float-to-double v2, p2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lgf;->d:D

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 6

    iget-wide v0, p0, Lgf;->a:D

    float-to-double v2, p1

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lgf;->d:D

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    iget-wide v4, p0, Lgf;->c:D

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lgf;->b:D

    neg-double v2, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    iget-wide v4, p0, Lgf;->d:D

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    const/high16 v4, 0x3f800000    # 1.0f

    mul-double/2addr v0, v2

    double-to-float v0, v0

    sub-float v0, v4, v0

    return v0
.end method
