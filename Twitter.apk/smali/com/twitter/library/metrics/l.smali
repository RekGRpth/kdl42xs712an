.class public Lcom/twitter/library/metrics/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/metrics/f;


# instance fields
.field a:Lcom/twitter/library/metrics/d;

.field private b:Landroid/os/Handler;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/metrics/m;

    invoke-direct {v0, p0}, Lcom/twitter/library/metrics/m;-><init>(Lcom/twitter/library/metrics/l;)V

    iput-object v0, p0, Lcom/twitter/library/metrics/l;->c:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/metrics/l;->b:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/metrics/l;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/metrics/l;->c:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/twitter/library/metrics/l;->d()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a(Lcom/twitter/library/metrics/d;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/metrics/l;->a:Lcom/twitter/library/metrics/d;

    return-void
.end method

.method public a(J)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/metrics/l;->a:Lcom/twitter/library/metrics/d;

    invoke-virtual {v2, v1}, Lcom/twitter/library/metrics/d;->a(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-virtual {p0}, Lcom/twitter/library/metrics/l;->d()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    iget-object v1, p0, Lcom/twitter/library/metrics/l;->a:Lcom/twitter/library/metrics/d;

    invoke-virtual {v1}, Lcom/twitter/library/metrics/d;->q()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/metrics/l;->b:Landroid/os/Handler;

    iget-object v4, p0, Lcom/twitter/library/metrics/l;->c:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/twitter/library/metrics/l;->d()J

    move-result-wide v5

    sub-long v2, v5, v2

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/metrics/l;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/metrics/l;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/metrics/l;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/library/metrics/l;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected d()J
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0xea60

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x36ee80

    goto :goto_0

    :cond_1
    const-wide/32 v0, 0x5265c00

    goto :goto_0
.end method
