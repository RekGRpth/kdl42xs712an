.class public Lcom/twitter/library/metrics/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/metrics/g;


# instance fields
.field private final a:Ljava/util/concurrent/ConcurrentHashMap;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Comparator;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/metrics/i;

    invoke-direct {v0, p0}, Lcom/twitter/library/metrics/i;-><init>(Lcom/twitter/library/metrics/h;)V

    iput-object v0, p0, Lcom/twitter/library/metrics/h;->c:Ljava/util/Comparator;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/metrics/h;->e:Ljava/util/HashSet;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/metrics/h;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iput-object p1, p0, Lcom/twitter/library/metrics/h;->b:Landroid/content/Context;

    const-string/jumbo v0, "metrics"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/metrics/h;->d:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/twitter/library/metrics/j;

    invoke-direct {v0, p0}, Lcom/twitter/library/metrics/j;-><init>(Lcom/twitter/library/metrics/h;)V

    invoke-static {v0}, Lcom/twitter/library/client/l;->a(Lcom/twitter/library/client/k;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/metrics/h;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->a:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/metrics/d;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/metrics/d;

    return-object v0
.end method

.method public a(Lcom/twitter/library/metrics/d;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/library/metrics/h;->a(Lcom/twitter/library/metrics/e;)V

    return-void
.end method

.method public a(Lcom/twitter/library/metrics/e;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/metrics/k;

    invoke-interface {v0, p1}, Lcom/twitter/library/metrics/k;->a(Lcom/twitter/library/metrics/e;)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/twitter/library/metrics/e;->c()V

    return-void
.end method

.method public a(Lcom/twitter/library/metrics/k;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->b:Landroid/content/Context;

    return-object v0
.end method

.method public b(Lcom/twitter/library/metrics/d;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/twitter/library/metrics/d;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/twitter/library/metrics/d;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/metrics/d;->b(Landroid/content/SharedPreferences$Editor;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/library/metrics/d;)V
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/metrics/d;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/metrics/d;->a(Landroid/content/SharedPreferences$Editor;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method

.method public d(Lcom/twitter/library/metrics/d;)Lcom/twitter/library/metrics/d;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/metrics/h;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/twitter/library/metrics/d;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/metrics/d;

    if-nez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method
