.class public Lcom/twitter/library/metrics/o;
.super Lcom/twitter/library/metrics/d;
.source "Twttr"


# instance fields
.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JI)V
    .locals 7

    invoke-static {p1}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/metrics/d;-><init>(Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/metrics/o;->e:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JILcom/twitter/library/metrics/g;)V
    .locals 7

    invoke-static {p1}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/metrics/d;-><init>(Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;)V

    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/twitter/library/metrics/o;->e:J

    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/metrics/h;)Lcom/twitter/library/metrics/o;
    .locals 3

    const-wide/16 v0, 0x0

    const/4 v2, 0x2

    invoke-static {p0, p1, v0, v1, v2}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;JI)Lcom/twitter/library/metrics/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/metrics/h;J)Lcom/twitter/library/metrics/o;
    .locals 1

    const/4 v0, 0x2

    invoke-static {p0, p1, p2, p3, v0}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;JI)Lcom/twitter/library/metrics/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/metrics/h;JI)Lcom/twitter/library/metrics/o;
    .locals 6

    invoke-static {p0}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/metrics/h;->a(Ljava/lang/String;)Lcom/twitter/library/metrics/d;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/metrics/o;

    move-object v1, p0

    move-wide v2, p2

    move v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/metrics/o;-><init>(Ljava/lang/String;JILcom/twitter/library/metrics/g;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/metrics/h;->d(Lcom/twitter/library/metrics/d;)Lcom/twitter/library/metrics/d;

    move-result-object v0

    :cond_0
    check-cast v0, Lcom/twitter/library/metrics/o;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "TimingMetric"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/scribe/ScribeLog;
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v1, p0, Lcom/twitter/library/metrics/o;->a:J

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iget-wide v1, p0, Lcom/twitter/library/metrics/o;->e:J

    iget-object v3, p0, Lcom/twitter/library/metrics/o;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Recorded perf event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/metrics/o;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/library/metrics/o;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "TimingMetric"

    return-object v0
.end method

.method protected f()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/metrics/o;->p()V

    return-void
.end method

.method protected g()V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/metrics/o;->f:J

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/metrics/o;->a(Z)V

    return-void
.end method

.method protected h()V
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/metrics/o;->f:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/metrics/o;->e:J

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/metrics/o;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/metrics/o;->q()V

    invoke-virtual {p0}, Lcom/twitter/library/metrics/o;->p()V

    return-void
.end method
