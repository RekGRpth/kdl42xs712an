.class public Lcom/twitter/library/amplify/o;
.super Lcom/twitter/library/card/element/h;
.source "Twttr"


# instance fields
.field private final c:Lcom/twitter/library/amplify/AmplifyPlayer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/h;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    new-instance v0, Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {v0, p2}, Lcom/twitter/library/amplify/AmplifyPlayer;-><init>(Lcom/twitter/library/card/element/Player;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/o;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x1000000

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p1, p3, p2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/h;->a(Landroid/view/View;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/h;->a(Z)V

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->h(Z)V

    return-void
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/o;->a:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/library/util/Util;->d()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/content/Context;Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->g()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->j()V

    return-void
.end method

.method public d()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v1, p0, Lcom/twitter/library/amplify/o;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Landroid/content/Context;)V

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->o()V

    return-void
.end method

.method public g()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/h;->g()V

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->k()V

    return-void
.end method

.method public h()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/h;->h()V

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->C()V

    return-void
.end method

.method public i()Lcom/twitter/library/amplify/AmplifyPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/o;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    return-object v0
.end method
